<?php

/**
 * Class AddSetofImpressionsOptionCommand
 */
class AddSetofImpressionsOptionCommand extends CConsoleCommand
{
    const DEFAULT_CITY_ID = 1;

    public function run($args)
    {
        $links = ItemSelfLink::model()->findAll();
        foreach ($links as $link) {
            /** @var ItemSelfLink $link */
            $link->city_id = self::DEFAULT_CITY_ID;
            if (empty($link->superItem) || empty($link->item)) {
                $link->delete();
                continue;
            }
            $prices = $link->item->prices_cache;
            if (!empty($prices)) {
                $prices = $prices[self::DEFAULT_CITY_ID];
                if (!is_array($prices)) {
                    $link->option_id = 0;
                } else {
                    $link->option_id = key($prices);
                }
            } else {
                $link->city_id = null;
                $link->option_id = 0;
            }
            $link->save();
        }
    }
}