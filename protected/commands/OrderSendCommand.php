<?php


date_default_timezone_set('Europe/Minsk');

class OrderSendCommand extends CConsoleCommand
{
    public function run($args)
    {
        if (isset ($args) && count($args))
        {
            foreach ($args as $arg)
            {
                $this->_runSend($arg);
            }
        }
        return;
    }


    private function _runSend($method)
    {
        $runMethod = 'sendOut' . $method;

        if (method_exists($this, $runMethod))
        {
            $this->$runMethod();
        }
        return;
    }


    public function sendMail($to, $subj, $body)
    {
        echo 'send - '.$to.' - '.$subj."\n\n";
        $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
        $emailer_config = Yii::app()->params['emailer'];
        email::EMailerSmtpSend($mailer, $to, $subj, $body, $emailer_config);
    }


     /**
     * function sends mail notifications after item is activated
      * Activation batton on index page
     */
    public function sendOutDeliveredItem()
    {
        $oCriteria=new CDbCriteria;
        //$oCriteria->select='id, name, recipient_firstname, email, recivier_email';
        $oCriteria->select='*';
        $oCriteria->condition='activated = '.Order::ACTIVE.
                ' AND activated_notify != 1 '.
                ' AND UNIX_TIMESTAMP(recivier_date) <= UNIX_TIMESTAMP(NOW())';
                //'AND (UNIX_TIMESTAMP(recivier_date) + 86400) <= UNIX_TIMESTAMP(NOW())';
        $oCriteria->params=array();
        $aOrders = Order::model()->with('item', 'user')->findAll($oCriteria);

        foreach ($aOrders as $oOrder)
        {
            $sTemplate = $this->renderFile(Yii::getPathOfAlias('application') . '/views/system/mailDeliveredItem.php',
            array
            (
                'oOrder' => $oOrder,
                'oUser'  => $oOrder->user,
                'oItem'  => $oOrder->item,
            ),
            true);

//            if (!empty ($oOrder->email))
//            {
//                $this->sendMail($oOrder->email, 'Добрый день, '.$oOrder->name.'!', $sTemplate, 'elpresent.by');
//                $oOrderItem = Order::model()->findByPk($oOrder->id);
//                $oOrderItem->delivered_notify = 1;
//                $oOrderItem->save();
//            }
            
            if (!empty ($oOrder->recivier_email))
            {
//                $this->sendMail($oOrder->recivier_email, 'Добрый день, '.$oOrder->recivier_name.'!', $sTemplate, 'elpresent.by');
                $this->sendMail($oOrder->recivier_email, 'Оставьте отзыв о подарке', $sTemplate);
                $oOrderItem = Order::model()->findByPk($oOrder->id);
                $oOrderItem->activated_notify = 1;
                $oOrderItem->save();
            }
        }
        return;
    }

     /**
     * function sends mail notifications after item is paid
     */
    public function sendOutIs_paidItem()
    {
        $oCriteria=new CDbCriteria;
        //$oCriteria->select='id, name, recipient_firstname, email, recivier_email';
        $oCriteria->select='*';
        $oCriteria->condition='is_paid = 1 '. 'AND is_paid_notify != 1 ';
               // 'AND (UNIX_TIMESTAMP(is_paid_date) + 86400) <= UNIX_TIMESTAMP(NOW())';
        $oCriteria->params=array();
        $aOrders = Order::model()->with('item', 'user')->findAll($oCriteria);

        foreach ($aOrders as $oOrder)
        {
            $sTemplate = $this->renderFile(Yii::getPathOfAlias('application') . '/views/system/mailPaidItem.php',
            array
            (
                'oOrder' => $oOrder,
                'oUser'  => $oOrder->user,
                'oItem'  => $oOrder->item,
            ),
            true);

            if (!empty ($oOrder->email))
            {
                //$this->sendMail($oOrder->email, 'Добрый день, '.$oOrder->name.'!', $sTemplate, 'elpresent.by');
                $this->sendMail($oOrder->email, 'Оставьте отзыв о сервисе', $sTemplate);
                $oOrderItem = Order::model()->findByPk($oOrder->id);
                $oOrderItem->is_paid_notify = 1;
                $oOrderItem->save();
            }
//            if (!empty ($oOrder->recivier_email))
//            {
//                $this->sendMail($oOrder->email, 'Добрый день, '.$oOrder->recipient_firstname.'!', $sTemplate, 'elpresent.by');
//                $oOrderItem->is_paid_notify = 1;
//                $oOrderItem->save();
//            }
        }

        return;
    }
  
}