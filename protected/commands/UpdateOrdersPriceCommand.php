<?php

class UpdateOrdersPriceCommand extends CConsoleCommand
{
    public function run($args)
    {
        $criteria = new CDbCriteria();
        $criteria->order = 'id DESC';
        $orderItems = OrderItems::model()->findAll($criteria);
        foreach ($orderItems as $k=>$orderItem) {
            /** @var OrderItems $orderItem */
            if (!empty($orderItem->group_item_price_data)) {
                if (null === $orderItem->itemObj) break;
                $itemObj = $orderItem->itemObj;
                $isGroup = $itemObj->type == Item::TYPE_GROUP;
                if($isGroup) {
                    if(empty($orderItem->activated_item_price)) {
                        if (null !== $orderItem->activatedItemObj) {
                            $activatedItemId = $orderItem->activatedItemObj->id;
                            if (!isset($orderItem->group_item_price_data[$activatedItemId])) {
                                echo "-----ERROR----\n";
                                continue;
                            }
                            $priceData = $orderItem->group_item_price_data[$activatedItemId];
                            $orderItem->activated_item_price = $priceData['price'];
                        }
                    }
                } else {
                    if(empty($orderItem->activated_item_price)) {
                        $price = $orderItem->item_price_with_discount;
                        if (!$price) {
                            $price = $itemObj->getRoundedPrice($orderItem->option_id, City::model()->findByPk(1));
                        }
                        $orderItem->activated_item_price = $price;
                    }
                }
                $orderItem->save();
            }
        }
    }


}