<?php

class SetGroupItemDataToOldOrderCommand extends CConsoleCommand
{


    const START_ID = 87967;
    public function run($args)
    {
        /** @var OrderItems[] $orderItems */
        $orderItems = OrderItems::model()->findAll('id >= '.self::START_ID);
        foreach ($orderItems as $orderItem) {
            $item = Item::model()->findByPk($orderItem->item_id);
            if ($item) {
                if ($item->type == Item::TYPE_ITEM) {
                    $city = $this->getCity($orderItem);
                    /** @var ItemPartnerLink $pricePartner */

                    $option = Option::model()->findByPk($orderItem->option_id);
                    $result = [
                        'id' => $item->id,
                        'title' => $item->title,
                        'option_id' => $orderItem->option_id,
                        'option_name' => $orderItem->option_id != 0 ? ($option?$option->name:null) : null,
                        'option_small_name' => $orderItem->option_id != 0 ? ($option?$option->small_name:null) : null,
                        'option_city_id' => $city->id,
                        'partner_id' => $orderItem->partner_id,
                        'partner_name' => $orderItem->partner ? $orderItem->partner->name : null,
                        'partner_price' => $orderItem->partner_price,

                    ];

                    $orderItem->group_item_price_data = $result;
                }
                if ($item->type == Item::TYPE_GROUP) {
                    $result = [];
                    foreach ($item->childs as $childItem) {
                        /** @var Item $childId */
                        $childId = $childItem->id;
                        $result[$childId]['id'] = $childId;
                        $result[$childId]['title'] = $childItem->title;
                        $selfLink = $item->getSelfLinkByChildren($childItem);
                        $result[$childId]['option_id'] = 0;
                        $result[$childId]['option_name'] = null;
                        $result[$childId]['option_small_name'] = null;
                        $result[$childId]['option_city_id'] = 1;
                        if (!empty($selfLink->option)) {
                            $result[$childId]['option_id'] = $selfLink->option->id;
                            $result[$childId]['option_name'] = $selfLink->option->name;
                            $result[$childId]['option_small_name'] = $selfLink->option->small_name;
                            $result[$childId]['option_city_id'] = $selfLink->city_id;
                        }
                        $option_id = $result[$childId]['option_id'];
                        $city_id = $result[$childId]['option_city_id'];

                        $partnerLink = array_filter($childItem->partners, function($v) use ($option_id) {
                            return $v->option_id == $option_id;
                        });
                        /** @var ItemPartnerLink $partnerLink */
                        $partnerLink = array_shift($partnerLink);
                        $result[$childId]['partner_id'] = null;
                        $result[$childId]['partner_name'] = null;
                        $result[$childId]['partner_price'] = null;
                        if ($partnerLink) {
                            $result[$childId]['partner_id'] = $partnerLink->partner_id;
                            $result[$childId]['partner_name'] = $partnerLink->partner->name;
                            $result[$childId]['partner_price'] = $partnerLink->partner_price;
                        }

                        $prices = array_filter($childItem->prices, function($v) use ($option_id, $city_id) {
                            return $v->option_id == $option_id && $v->city_id == $city_id;
                        });
                        $prices = array_shift($prices);
                        $result[$childId]['price'] = $prices->item_price;
                    }
                    $orderItem->group_item_price_data = $result;
                }
                $orderItem->save();

            }
        }

    }

    /**
     * @param OrderItems $orderItem
     * @return CActiveRecord|null
     */
    protected function getCity($orderItem)
    {

//        var_dump('----------');
//        var_dump($orderItem->order_id);
//        var_dump(Order::model()->findByPk($orderItem->order_id)->city_id);
//        var_dump('----------');
        $order = Order::model()->findByPk($orderItem->order_id);

        return $order ? City::getCity($order->city_id) : City::getCity();
    }
}