<?php

class AssetsDumpCommand extends CConsoleCommand
{

    public function run($args)
    {
        /** @var AssetManager $assetsManager */
        $assetsManager = Yii::app()->assetsManager;
        $assetsManager->dump(empty($args) ? null : $args[0]);
        echo "Done\n";
    }

}