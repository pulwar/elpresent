<?php

Yii::import('application.extensions.assist.AssistPayment');
Yii::import('application.extensions.curl.Curl');

class PaymentCommand extends CConsoleCommand
{
    public function run($args)
    {
        $oAssistPayment = new AssistPayment();
        $oCurl          = new Curl();
        $sResult        = $oCurl->getSSLUrl($oAssistPayment->getXmlResultUrl());
	    
        $oResultXml = simplexml_load_string($sResult);
        
        foreach ( $oResultXml->orders->order as $oOrder )
        {
            Order::model()->updateAll(array('is_paid' => 1), 'order_number = "'.$oOrder->ordernumber.'"');  
        }    
    }
}