<?php

class SitemapCommand extends CConsoleCommand
{
    public function run($args)
    {
        $siteMapFile = realpath(Yii::getPathOfAlias('application').'/../sitemap.xml');
        if (is_file($siteMapFile)) {
            unlink($siteMapFile);
        }

        // call rebuild of sitemap
        $siteMapUrl = Yii::app()->params['cronServer']['pathInfo'] . ltrim(Yii::app()->urlManager->createUrl('site/sitemap'), '.');
        file_get_contents($siteMapUrl);

        // compress sitemap.xml
        shell_exec(sprintf('gzip -c -5 %s > %s', $siteMapFile, $siteMapFile.'.gz'));
    }
}