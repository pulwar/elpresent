<?php

/**
 * Class GeneratepartnersuserCommand
 */
class GeneratepartnersuserCommand extends CConsoleCommand
{
    public function run($args)
    {
        $result = [];
        $usedEmails = [];
        $partners = Partner::model()->findAll();
        foreach ($partners as $partner) {
            /** @var Partner $partner */
            if (empty($partner->user_id)) {
                $partnerId = $partner->id;
                $password = $this->generatePassword();
                $name = $partner->name;
                $login = "partner-{$partner->id}";
                $email = !empty($partner->email) && !in_array($partner->email, $usedEmails)?$partner->email:"$login@elpresent.by";

                $user = new User();
                $user->user_role = User::ROLE_PARTNER;
                $user->user_firstname = $name;
                $user->user_lastname = $name;
                $user->user_email = $email;
                $user->user_login = $login;
                $user->user_password = md5($password);
                $user->save();
                //-------
                $partner->user_id = $user->id;
                $partner->save();

                if (!empty($partner->email) && !in_array($partner->email, $usedEmails)) {
                    $usedEmails[] = $partner->email;
                }
                echo 'Partner #'.$partnerId.". Generated!\n";

                $result[] = [
                    'partnerId' => $partnerId,
                    'password' => $password,
                    'name' => $name,
                    'login' => $login,
                    'email' => $email,
                ];
            }
        }
        $resultCsv = '';
        foreach ($result as $item) {
            $resultCsv .= sprintf("%s;%s;%s;%s;%s\n", $item['partnerId'], $item['name'], $item['login'], $item['password'], $item['email']);
        }
        file_put_contents('user.csv', $resultCsv);
    }


    protected function generatePassword($length = 8) {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $count = mb_strlen($chars);

        for ($i = 0, $result = ''; $i < $length; $i++) {
            $index = rand(0, $count - 1);
            $result .= mb_substr($chars, $index, 1);
        }

        return $result;
    }
}