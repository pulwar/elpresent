<?php

/*
 * 
 */

class GetInvoicesEasypayCommand extends CConsoleCommand
{

	public function run($args)
	{
		$criteria = new CDbCriteria;
		$criteria->select = '*';
		// Выбираем только за последние сутки
		$criteria->condition = 'payment_type_id= 4 AND is_paid = 0 AND order_date >= CURDATE()';
		$orders = Order::model()->findAll($criteria);
		foreach ($orders as $order) {
			$result = EasyPay::IsInvoicePaid($order->id);
			//html::pr('IsInvoicePaid: ' . $result . '\n');
			if ($result == 200) {
				Order::model()->updateByPk(intval($order->id), ['is_paid' => 1, 'is_paid_date' => date('Y-m-d')],
					'payment_type_id= 4 AND is_paid = 0');
				$body = "<h4>Счет № {$order->id} (соответствует № п/п заказа) оплачен в системе EasyPay</h4>";
				//$body .= "Cумма оплаты: {$invoice->sum} <br>";
				//$body .= "Дата оплаты: {$invoice->purch_date} <br>";
				//$body .= "Идентификатор кошелька: {$invoice->card} <br>";
				$this->emailOrdersNotification($body, $order->id);
			}
		}

		die('ok');
		//ob_start();
		/*$criteria=new CDbCriteria;
		$criteria->select='id';
		$criteria->condition='payment_type_id= 4 AND is_paid = 0';
		$criteria->order = 'id DESC';
		$criteria->limit = 1;

		$last_pained = Order::model()->find($criteria);
		if($last_pained){
			$last_id = $last_pained->id;
		}else{
			$last_id = 0;
		}
		$invoices = EasyPay::GetPaidInvoicesExt($last_id);

		if(empty ($invoices)){
			return;
		}
		print_r($invoices);
		$criteria=new CDbCriteria;
		$criteria->select='id';
		$criteria->condition='payment_type_id= 4 AND is_paid = 0';
		$orders = Order::model()->findAll($criteria);
		$order_ids = array();
		foreach($orders as $order){
			$order_ids[] = $order->id;
		}
		print_r($order_ids);
		foreach ($invoices as $invoice){
			if(in_array($invoice->order, $order_ids)){
				$this->changePaymentStatus($invoice);
				echo $invoice->order;
			}
		}*/
		//echo time()."\n";
		//$data = ob_get_clean();
		//file_put_contents('./protected/runtime/easypay.log', $data);
	}

	protected function changePaymentStatus($invoice)
	{
		$body = "<h4>Счет № {$invoice->order} (соответствует № п/п заказа) оплачен в системе EasyPay</h4>";
		$body .= "Cумма оплаты: {$invoice->sum} <br>";
		$body .= "Дата оплаты: {$invoice->purch_date} <br>";
		$body .= "Идентификатор кошелька: {$invoice->card} <br>";

		$this->emailOrdersNotification($body, $invoice->order);
		Order::model()->updateByPk(intval($invoice->order), array('is_paid' => 1, 'is_paid_date' => $invoice->purch_date),
			'payment_type_id= 4 AND is_paid = 0');
		echo $body;

	}

    protected function emailOrdersNotification($body, $orderId)
    {
        email::EMailerSmtpSend(
            Yii::createComponent('application.extensions.mailer.EMailer'),
            Settings::getValueByAlias('order_buy_notification_email'),
            "Счет № $orderId оплачен в системе EasyPay",
            $body,
            Yii::app()->params['emailer']
        );
    }
}

?>
