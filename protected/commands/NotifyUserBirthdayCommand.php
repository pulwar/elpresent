<?php

class NotifyUserBirthdayCommand extends CConsoleCommand
{
	/**
	 * @var string
	 */
	private $from = 'admin@elpresent.by';

	/**
	 * @var string
	 */
	private $to = 'info@elpresent.by';

	/**
	 * @var Mailer
	 */
	private $mailer;


	public function init()
	{
		$this->mailer = Yii::app()->mailer;
	}

	public function run($args)
	{
		foreach ($this->getUsersWhoseBirthdayNextWeek() as $user) {
			$this->sendNotify($user);
		}

		foreach ($this->getUsersWhoseBirthdayToday() as $user) {
			$this->sendNotify($user);
		}
	}

	private function sendNotify(User $user)
	{
		$message = $this->mailer->createMessage('Уведомление о дне рождения пользователя', sprintf('%s у пользователя <a href="http://%s/admin/user/manage/id/%d">%s</a> будет день рождения.',
			$user->user_birthday_date == date('m-d') ? 'Сегодня' : 'Через неделю',
			Yii::app()->params['cronServer']['pathInfo'],
			$user->id,
			$user->user_email
		));
		$message->setTo($this->to, 'Elpresent');
		$message->setFrom($this->from, 'Elpresent');
		$message->setContentType('text/html');
		$this->mailer->send($message);
	}

	private function getUsersWhoseBirthdayNextWeek()
	{
		$date = new \DateTime();
		$date->modify('+7 day');

		return $this->getUsersByBirthdayDate($date);
	}

	private function getUsersWhoseBirthdayToday()
	{
		return $this->getUsersByBirthdayDate(new \DateTime());
	}

	/**
	 * @param DateTime $date
	 * @return User[]
	 */
	private function getUsersByBirthdayDate(\DateTime $date)
	{
		$criteria = new CDbCriteria();
		$criteria->addCondition('user_birthday_date = :date');
		$criteria->params = [
			'date' => $date->format('m-d')
		];

		$dataProvider = new CActiveDataProvider(User::model(), [
			'criteria' => $criteria
		]);

		return new CDataProviderIterator($dataProvider, 5);
	}

}