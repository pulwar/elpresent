<?php

class m140916_143309_is_kit extends CDbMigration
{
    public function up()
    {
        $this->addColumn('el_item', 'is_kit', 'tinyint(1) DEFAULT 1');
    }

    public function down()
    {
        $this->dropColumn('el_item', 'is_kit');
        return false;
    }
}