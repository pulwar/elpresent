<?php

class m140407_214252_increase_settings_limits extends CDbMigration
{
	public function up()
	{
        $this->alterColumn('{{settings}}', 'setting_value', 'text NOT NULL');
	}

	public function down()
	{
		$this->alterColumn('{{settings}}', 'setting_value', 'string(300) NOT NULL');
	}
}