<?php

class m150117_074042_about_gift extends CDbMigration
{
	public function up()
	{
		$this->update('el_text_record',
			array(
				'text' => '
<div class="description-text">
	<div class="col-md-12">
		<h1 class="main-title">О подарке</h1>

		<h3>Что в подарке?</h3>

		<p>Из чего же, из чего же, из чего же, сделаны наши подарки? Мы дарим
			вашим близким возможность испытать удивительные эмоции. Для этого
			нужно лишь открыть подарочную коробку. Купленный вами подарок
			состоит из:</p>
	</div>
	<div class="description-item clearfix">
		<div class="col-md-7">
			<div class="description-item-title clearfix"><span>1</span><h4>
					Фирменного подарочного пакета</h4></div>

			<p>Яркая упаковка создает первое впечатление о вашем подарке, а
				также является удобным способом преподнести подарок дорогому
				человеку. В пакет Elpresent поместится не только сам подарок, но
				и такое дополнение, как открытка, и любая другая небольшая, но
				приятная вещь. При виде такого подарка точно захочется узнать,
				что внутри.</p>
		</div>
		<div class="col-md-5">
			<img src="/img/front_bootstrap/other/description-1.jpg" alt="">
		</div>
	</div>

	<div class="description-item clearfix">
		<div class="col-md-12">
			<div class="description-item-title clearfix"><span>2</span><h4>
					Подарочной коробки и ленты с бантом, которые вы выбираете
					сами</h4></div>
		</div>

		<div class="col-md-5">
			<img src="/img/front_bootstrap/other/description-2.jpg" alt="">
		</div>
		<div class="col-md-7">
			<p>Вашему вниманию представлены эксклюзивные дизайнерские коробки,
				которые призваны сделать ваш праздник ярким и незабываемым.
				Ленты и наполнение коробки вы также выбираете самостоятельно</p>
		</div>
	</div>

	<div class="description-item clearfix">
		<div class="col-md-12">
			<div class="description-item-title clearfix"><span>3</span><h4>
					Внутри: подробная инструкция с описанием и условиями
					активации подарка</h4></div>
		</div>

		<div class="col-md-7">
			<p>Красочная, дизайнерски оформленная инструкция расскажет вам,
				обладателям какого подарка вы стали, а также как, когда и при
				каких условиях вы сможете активировать и получить свой подарок.
				Наша задача - доставить вам эстетическое и эмоциональное
				удовольствие при получении подарка!</p>
		</div>
		<div class="col-md-5">
			<img src="/img/front_bootstrap/other/description-3.jpg" alt="">
		</div>
	</div>

	<div class="description-item clearfix">
		<div class="col-md-12">
			<div class="description-item-title clearfix"><span>4</span><h4>
					Подарочная пластиковая карта с эмбоссированным пин-кодом,
					который является индивидуальным номером вашего заказа и
					используется при активации подарка</h4></div>
		</div>

		<div class="col-md-5">
			<img src="/img/front_bootstrap/other/description-4.jpg" alt="">
		</div>
		<div class="col-md-7">
			<p>Подарочная карта - это и есть сертификат для активации подарка, а
				также красивый сувенир, который останется на память о нашем
				сервисе. На карточке будет эмбоссирован пин-код, индивидуальный
				номер вашего заказа, который является ключом к получению
				волшебных ощущений. Вы можете активировать подарок в течение
				трех месяцев с момента покупки подарка.</p>
		</div>
	</div>
	<div class="col-md-12">
		<hr>
	</div>
</div>

<div class="description-buy">
	<h3>Как купить?</h3>

	<div class="description-buy-item clearfix">
		<div class="col-md-4">
			<img src="/img/front_bootstrap/other/description-buy-1.jpg" alt="">
		</div>
		<div class="col-md-8">
			<div class="description-item-title clearfix"><h4>Выбираете подарок в
					Каталоге подарков, тщательно прочитав описание и условия
					активации подарка</h4></div>
			<p>В нашем каталоге вы найдете самые разнообразные подарки на любой
				вкус и кошелек. Вы покупаете незабываемые впечатления и эмоции,
				которые можете подарить своим близким или себе.</p>
		</div>
	</div>

	<div class="description-buy-item clearfix">
		<div class="col-md-4">
			<img src="/img/front_bootstrap/other/description-buy-2.jpg" alt="">
		</div>
		<div class="col-md-8">
			<div class="description-item-title clearfix"><h4>«Купить» и
					переходите на страницу упаковки</h4></div>
			<p>Вы можете выбрать дизайн подарочной коробки, тем самым решив, в
				каком виде вы хотите преподнести его дорогому человеку. Все
				остальное предоставьте нам!</p>
		</div>
	</div>

	<div class="description-buy-item clearfix">
		<div class="col-md-4">
			<img src="/img/front_bootstrap/other/description-buy-2.jpg" alt="">
		</div>
		<div class="col-md-8">
			<div class="description-item-title clearfix"><h4>Выбираете
					индивидуальную упаковку: подарочную коробку и ленту</h4>
			</div>
			<p>Мы постарались подобрать самые красивые, яркие и веселые решения,
				чтобы сделать ваш подарок и праздник красочными.</p>
		</div>
	</div>

	<div class="description-buy-item clearfix">
		<div class="col-md-4">
			<img src="/img/front_bootstrap/other/description-buy-2.jpg" alt="">
		</div>
		<div class="col-md-8">
			<div class="description-item-title clearfix"><h4>Попадаете на форму
					заказа подарка, где заполняете личные данные о себе,
					указывая контактный телефон и адрес доставки</h4></div>
			<p>Наша сила – оперативность и внимательное отношение к клиентам.
				Именно поэтому, как только вы становитесь нашим клиентом, мы
				окружаем вас всяческим вниманием и заботой.</p>
		</div>
	</div>

	<div class="description-buy-item clearfix">
		<div class="col-md-4">
			<img src="/img/front_bootstrap/other/description-buy-3.jpg" alt="">
		</div>
		<div class="col-md-8">
			<div class="description-item-title clearfix"><h4>Наш консультант
					связывается с вами в течение 10 минут и уточняет удобное для
					вас время получения подарка</h4></div>
			<p>Мы с удовольствием ответим на все ваши вопросы, поможем с выбором
				подарка, оперативно его доставим вам или человеку, которому он
				предназначен. Вы можете заказать подарок только на сайте через
				форму заказа подарка</p>
		</div>
	</div>

	<div class="col-md-12">
		<hr>
	</div>

</div>

<div class="description-activate">
	<h3>Как активировать?</h3>

	<p>Обладатель подарочной пластиковой карты может активировать подарок 2-мя
		способами:</p>

	<div class="description-activate-item">
		<div class="description-item-title clearfix"><span>1</span><h4>
				Самостоятельная связь с нашим консультантом по телефону, skype,
				icq или в он-лайн чате на сайте</h4></div>
		<ul>
			<li>Свяжитесь с консультантом и продиктуйте пин-код, который
				эмбоссирован на карте и необходимые личные данные
			</li>
			<li>Уточните возможную дату, время и место получения активированного
				подарка.
			</li>
		</ul>
	</div>

	<div class="description-activate-item">
		<div class="description-item-title clearfix"><span>2</span><h4>Активация
				подарка на сайте (более простой и удобный)</h4></div>
		<ul>
			<li>Заполните форму активации, вписав пин-код, необходимые личные
				данные и контактный телефон
			</li>
			<li>Далее, вы получите контактные данные партнера, место проведения
				и телефоны. Вы должны самостоятельно связаться с нашим
				партнером, чтобы забронировать время впечатления.
			</li>
		</ul>
	</div>

</div>'
			),
			'id=11'
		);

	}

	public function down()
	{
		echo "m150117_074042_about_gift does not support migration down.\n";
		return false;
	}

}