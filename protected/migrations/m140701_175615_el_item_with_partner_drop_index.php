<?php

class m140701_175615_el_item_with_partner_drop_index extends CDbMigration
{
	public function up()
	{
        $this->execute('ALTER TABLE el_item_with_partner DROP INDEX iwp_unq');
	}

	public function down()
	{
		echo "m140701_175615_el_item_with_partner_drop_index does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}