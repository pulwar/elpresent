<?php

class m151031_065147_wherebuy extends CDbMigration
{
	public function up()
	{
        $this->createTable('{{wherebuy}}', array(
            'id' => 'pk',
            'address' => 'text NOT NULL',
            'title' => 'string NOT NULL',
            'caption' => 'string NOT NULL',
            'coordinates' => 'string NOT NULL',
            'city' => 'string NOT NULL',
            'show' => 'boolean NOT NULL DEFAULT 1',
            'order' => 'integer NOT NULL'
        ));
	}

	public function down()
	{
        $this->dropTable('{{whereby}}');
	}
}