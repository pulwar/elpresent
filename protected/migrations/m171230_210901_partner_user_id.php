<?php

class m171230_210901_partner_user_id extends CDbMigration
{
	public function up()
	{
	    $this->addColumn('el_partner', 'user_id', 'INT DEFAULT NULL');
        return true;
	}

	public function down()
	{
		echo "m171230_210901_partner_user_id does not support migration down.\n";
        $this->dropColumn('el_partner', 'user_id');
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}