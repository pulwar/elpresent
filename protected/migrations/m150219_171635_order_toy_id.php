<?php

class m150219_171635_order_toy_id extends CDbMigration
{
	public function up()
	{
		$this->addColumn('el_order', 'toy_id', 'int(11) DEFAULT 0');
	}

	public function down()
	{
		$this->dropColumn('el_order', 'toy_id');
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}