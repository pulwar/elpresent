<?php

class m131224_233500_bind_orders_to_city extends CDbMigration
{
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
        $this->addColumn('{{order}}', 'city_id', 'integer not null');
        $this->execute('UPDATE {{order}} SET city_id = 1');
	}

	public function safeDown()
	{
        $this->dropColumn('{{order}}', 'city_id');
	}
}