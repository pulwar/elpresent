<?php

class m130213_231440_added_redirection_map extends CDbMigration
{
	public function safeUp()
	{
        $prefix = $this->getDbConnection()->tablePrefix;
        $this->createTable($prefix.'redirection', array(
            'id' => 'pk',
            'pattern' => 'string not null',
            'target' => 'string not null',
            'isRegexp' => 'boolean default 0'
        ));
    }

    public function safeDown()
	{
        $prefix = $this->getDbConnection()->tablePrefix;
        $this->dropTable($prefix.'redirection');
	}
}