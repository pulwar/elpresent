<?php

class m140204_221406_fix_city_bindings extends CDbMigration
{
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
        $this->addColumn('{{item_with_city}}', 'available', 'boolean default 0');
        $this->execute('UPDATE {{item_with_city}} SET available=0');

        $dataProvider = new CActiveDataProvider(Item::model()->resetScope());
        $dataProvider->setPagination(false);
        $dataIterator = new CDataProviderIterator($dataProvider);

        foreach($dataIterator as $item) {
            $this->processItem($item);
        }
    }

	public function safeDown()
	{
        $this->dropColumn('{{item_with_city}}', 'available');
	}

    protected function processItem($item)
    {
        $citiesAvailable = [];
        $partners = $item->partners;
        if (is_array($partners)) {
            foreach($partners as $partnerLink) {
                $city = $partnerLink->partner->city;
                if ($city) $citiesAvailable[] = $city->id;
            }
        }

        $cityLinks = $item->prices;
        foreach($cityLinks as $link) {
            if (in_array($link->city_id, $citiesAvailable)) {
                $link->available = !!$item->available;
                $link->save();
            }
        }

    }
}