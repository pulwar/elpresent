<?php

class m141027_182310_action_add_img extends CDbMigration
{
    public function up()
    {
        $this->addColumn('el_actions', 'img', 'VARCHAR(255) NOT NULL');
    }

    public function down()
    {
        $this->dropColumn('el_actions', 'img');
        return false;
    }
}