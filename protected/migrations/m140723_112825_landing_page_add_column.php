<?php

class m140723_112825_landing_page_add_column extends CDbMigration
{
	public function up()
	{
        $this->addColumn('el_landing', 'map_code', 'text NOT NULL');
	}

	public function down()
	{
        $this->dropColumn('el_landing', 'map_code');
		return false;
	}


}