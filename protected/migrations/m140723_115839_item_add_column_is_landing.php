<?php

class m140723_115839_item_add_column_is_landing extends CDbMigration
{
	public function up()
	{
        $this->addColumn('el_item', 'is_landing', 'tinyint(1) DEFAULT 0');
	}

	public function down()
	{
        $this->dropColumn('el_item', 'is_landing');
		return false;
	}

}