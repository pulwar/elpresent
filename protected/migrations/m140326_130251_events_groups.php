<?php

class m140326_130251_events_groups extends CDbMigration
{


	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
        $this->dropTable('{{events}}');
        $this->createTable('{{events}}', [
            'id' => 'pk',
            'title' => 'string NOT NULL',
            'description' => 'text NOT NULL',
            'enabled' => 'boolean DEFAULT 0'
        ]);

        $this->addColumn('{{item}}', 'event_id', 'integer DEFAULT NULL');
	}

	public function safeDown()
	{
        return true;
	}

}