<?php

class m160925_181601_yml extends CDbMigration
{
	public function up()
	{
		$this->addColumn('el_item', 'yml', 'tinyint(1) DEFAULT 1');
	}

	public function down()
	{
		$this->dropColumn('el_item', 'yml');
	}
}