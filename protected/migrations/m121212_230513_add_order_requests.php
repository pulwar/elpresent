<?php

class m121212_230513_add_order_requests extends CDbMigration
{
	public function up()
	{
        $prefix = $this->getDbConnection()->tablePrefix;
        $this->createTable($prefix . 'order_request', array(
            'id' => 'pk',
            'item_id' => 'integer NOT NULL',
            'name' => 'string NOT NULL',
            'phone' => 'string NOT NULL',
            'date' => 'datetime NOT NULL',
            'processed' => 'boolean NOT NULL',
        ));
    }

    public function down()
	{
        $prefix = $this->getDbConnection()->tablePrefix;
        $this->dropTable($prefix . 'order_request');
	}
}