<?php

class m121111_224553_multiple_categories_for_items extends CDbMigration
{
	public function up()
	{
        echo "Create reference table...\n";
        $this->createTable('el_category_items', array(
            'category_id' => 'integer',
            'item_id' => 'integer',
        ));

        echo "Moving data to new table\n";

        $sql = 'INSERT INTO el_category_items (category_id, item_id)'
            . ' SELECT i.category_id, i.id'
            . ' FROM el_item as i WHERE i.type="item"';

        $this->execute($sql);

	}

	public function down()
	{
        echo "Restore data";
        $this->dropTable('el_category_items');
	}
}