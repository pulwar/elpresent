<?php

class m131214_115115_move_base64_images_to_separate_files extends CDbMigration
{
	public function safeUp()
	{
        $sql = 'SELECT id, text FROM {{item}}';
        $command = $this->dbConnection->createCommand($sql);
        $dataReader = $command->query();

        while($row = $dataReader->read()) {
            $this->processDescription($row['id'], $row['text']);
            unset($row);
            $dataReader->next();
        }
	}

	public function safeDown()
	{
        return false;
	}

    private function processDescription($id, $description)
    {
        // extract images from description
        $regexp = '/src="data:image\/([a-z]+?);base64\,([^"]+)"/i';
        $picturesDirectory = realpath(Yii::getPathOfAlias('application') . '/../images/descr/') . '/';
        $description = preg_replace_callback($regexp, function ($matches) use ($picturesDirectory, $id) {
            $base64 = $matches[2];
            $type = $matches[1];

            // generate name
            $name = uniqid($id) . '.' . $type;
            // save image to disc
            file_put_contents($picturesDirectory . $name, base64_decode($base64));
            unset($base64);

            // replace uri of image
            return 'src="/images/descr/'.$name.'"';
        }, $description);

        // update in db
        $this->update('{{item}}', [
            'text' => $description,
        ], 'id=:id', ['id' => $id]);
    }
}