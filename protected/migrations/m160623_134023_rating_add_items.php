<?php

class m160623_134023_rating_add_items extends CDbMigration
{
	public function up()
	{
		$this->addColumn('el_item', 'rating', 'integer DEFAULT 0');
		$this->addColumn('el_item', 'vote_count', 'integer DEFAULT 0');
	}

	public function down()
	{
		$this->dropColumn('el_item', 'rating');
		$this->dropColumn('el_item', 'vote_count');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}