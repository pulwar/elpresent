<?php

class m180129_193825_new_column_for_otion_small_name extends CDbMigration
{
	public function up()
	{
	    $this->addColumn('{{option}}', 'small_name', 'VARCHAR (255) DEFAULT NULL');
	    $this->updateOptions();
	    return true;
	}

    private function updateOptions()
    {
        $options = $this->query('SELECT o.id, o.name FROM el_option o');
        foreach ($options as $option) {
            $this->update('el_option', ['small_name' => $option['name']], 'id='.$option['id']);
        }
    }

    private function query($sql)
    {
        return $this->getDbConnection()->createCommand($sql)->queryAll();
    }

	public function down()
	{
		echo "m180129_193825_new_column_for_otion_small_name does not support migration down.\n";
		$this->dropColumn('{{option}}', 'small_name');
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}