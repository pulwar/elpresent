<?php

class m171229_200118_add_role_partner extends CDbMigration
{
	public function up()
	{
        $this->dropColumn('el_user', 'user_role');
        $this->addColumn('el_user', 'user_role', 'ENUM(\'user\',\'admin\',\'partner\') default \'user\'');
        $this->update('el_user', ['user_role' => 'admin'], 'id=1');
        $this->update('el_user', ['user_role' => 'user'], 'id<>1');
        return true;
	}

	public function down()
	{
        echo "m171227_212154_add_role_partner does not support migration down.\n";
        $this->dropColumn('el_user', 'user_role');
        $this->addColumn('el_user', 'user_role', 'ENUM(\'user\',\'admin\') default \'user\'');
        $this->update('el_user', ['user_role' => 'admin'], 'id=1');
        $this->update('el_user', ['user_role' => 'user'], 'id<>1');
        return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}