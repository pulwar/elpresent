<?php

class m160406_122119_discounts_codes_table extends CDbMigration
{
    public function up()
    {
        $this->createTable('{{discounts_cods}}', array(
            'id' => 'pk',
            'discount_id' => 'integer NOT NULL',
            'code' =>  'VARCHAR(255) UNIQUE NOT NULL',

        ), 'ENGINE=InnoDB CHARSET=utf8');
    }

    public function down()
    {
        $this->dropTable('{{discounts_cods}}');
    }
}