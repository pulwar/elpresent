<?php

class m140506_100449_hot_impressions extends CDbMigration
{
	public function up()
	{
        $this->addColumn('{{item}}', 'is_hot', 'boolean');
	}

	public function down()
	{
		$this->dropColumn('{{item}}', 'is_hot');
	}
}