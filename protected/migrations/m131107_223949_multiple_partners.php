<?php

class m131107_223949_multiple_partners extends CDbMigration
{
	public function safeUp()
	{
        $this->createTable($this->getTableName('item_with_partner'), array(
            'id' => 'pk',
            'item_id' => 'integer not null',
            'partner_id' => 'integer not null',
            'partner_price' => 'integer DEFAULT NULL',
            'enabled' => 'boolean DEFAULT 0'
        ));

        $this->createTable($this->getTableName('item_with_city') , array(
            'id' => 'pk',
            'item_id' => 'integer not null',
            'city_id' => 'integer not null',
            'item_price' => 'integer not null',
        ));

        $this->createIndex('iwp_unq', $this->getTableName('item_with_partner'), 'partner_id, item_id', true);

        $this->execute('UPDATE el_partner SET city_id=1 WHERE city_id = \'\'');

        // fill table with data
        $sql = 'INSERT INTO el_item_with_partner (item_id, partner_id, partner_price) '
             . 'SELECT i.id, i.partner_id, i.partner_price FROM el_item i '
             . 'INNER JOIN el_partner p ON p.id = i.partner_id;';
        $this->execute($sql);

        $sql = 'INSERT INTO el_item_with_city (item_id, city_id, item_price) '
            . 'SELECT i.id, p.city_id, i.price FROM el_item i '
            . 'INNER JOIN el_partner p ON p.id = i.partner_id;';
        $this->execute($sql);

	}

	public function safeDown()
	{
        $this->dropTable($this->getTableName('item_with_partner'));
        $this->dropTable($this->getTableName('item_with_city'));
	}

    protected function getTableName($name)
    {
        return $this->dbConnection->tablePrefix . $name;
    }

}