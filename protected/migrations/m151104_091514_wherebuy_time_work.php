<?php

class m151104_091514_wherebuy_time_work extends CDbMigration
{
    public function up()
    {
        $this->addColumn('el_wherebuy', 'time_work', 'VARCHAR(100) NOT NULL');
    }

    public function down()
    {
        $this->dropColumn('el_wherebuy', 'time_work');
        return false;
    }
}