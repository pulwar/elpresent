<?php

class m160601_071423_discounts_add_fields extends CDbMigration
{
	public function up()
	{
		$this->addColumn('el_discounts', 'address', 'text');
		$this->addColumn('el_discounts', 'tel', 'text');
	}

	public function down()
	{
		$this->dropColumn('el_discounts', 'address');
		$this->addColumn('el_discounts', 'tel');

		return false;
	}
}