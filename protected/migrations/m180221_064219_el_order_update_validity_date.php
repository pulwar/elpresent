<?php

class m180221_064219_el_order_update_validity_date extends CDbMigration
{
	public function up()
	{

		echo "Update validity date\n";

		$sql = 'update el_order o '
			.'join el_order_items o_i '
			.'on o_i.order_id = o.id '
			.'join el_item i '
			.'on i.id = o_i.item_id '
			.'set o.`validity_date` = o.`order_date` + INTERVAL  \'3\' MONTH ;';

		$this->execute($sql);
	}

	public function down()
	{
	}
}