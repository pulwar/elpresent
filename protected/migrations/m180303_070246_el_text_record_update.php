<?php

class m180303_070246_el_text_record_update extends CDbMigration
{
	public function up()
	{
		$this->update('el_text_record',
			array(
				'alias' => 'where-buy'

			),
			'id=33'
		);
	}

	public function down()
	{
		echo "m180303_070246_el_text_record_update does not support migration down.\n";
		return false;
	}
}