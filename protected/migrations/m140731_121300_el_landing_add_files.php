<?php

class m140731_121300_el_landing_add_files extends CDbMigration
{
	public function up()
	{
        $this->addColumn('el_landing', 'meta_d', 'VARCHAR(255) NOT NULL');
        $this->addColumn('el_landing', 'meta_k', 'VARCHAR(255) NOT NULL');
        $this->addColumn('el_landing', 'delivery', 'text NOT NULL');
	}

	public function down()
	{
        $this->dropColumn('el_landing', 'meta_d');
        $this->dropColumn('el_landing', 'meta_k');
        $this->dropColumn('el_landing', 'delivery');
		return false;
	}


}