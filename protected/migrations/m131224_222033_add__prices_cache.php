<?php

class m131224_222033_add__prices_cache extends CDbMigration
{
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
        $this->addColumn('{{item}}', 'prices_cache', 'text default null');

        // fill data
        $sql = 'SELECT id FROM {{item}} ORDER BY id';
        $command = $this->dbConnection->createCommand($sql);
        $ids = $command->queryAll();

        $i = 0;
        foreach ($ids as $row) {
            $this->processPrice($row['id']);
            $i++;
        }

        echo "\nProcessed $i rows\n";
	}

	public function safeDown()
	{
        $this->dropColumn('{{item}}', 'prices_cache');
	}

    private function processPrice($id)
    {
        if (1182 == $id) {
            $aef = 4;
        }

        $raw = $this->dbConnection->createCommand('SELECT city_id, item_price as price FROM {{item_with_city}} WHERE item_id=:id')->queryAll(true, ['id' => $id]);
        $prices = [];
        foreach ($raw as $row) {
            $prices[$row['city_id']] = $row['price'];
        }

        $s = serialize($prices);

        if (!$s || count($prices) > 2) {
            $j = 4;
        }

        $this->update('{{item}}', [
            'prices_cache' => $s
        ], 'id=:id', ['id' => $id]);
    }
}