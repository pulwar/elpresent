<?php

class m161225_171759_update_user_cash_in_order_to_float extends CDbMigration
{
	public function up()
	{
		$this->alterColumn('el_order', 'user_cash', 'float DEFAULT 0.0');
	}

	public function down()
	{
		echo "m161225_171759_update_user_cash_in_order_to_float does not support migration down.\n";
		$this->alterColumn('el_order', 'user_cash', 'int DEFAULT 0');
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}