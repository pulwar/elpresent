<?php

class m180204_135701_order_items_activated_item_price extends CDbMigration
{
	public function up()
	{
        $this->addColumn('el_order_items', 'activated_item_price', 'FLOAT DEFAULT NULL');

        return true;
	}

	public function down()
	{
		echo "m180204_135701_order_items_activated_item_price does not support migration down.\n";

		$this->dropColumn('el_order_items', 'activated_item_price');

		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}