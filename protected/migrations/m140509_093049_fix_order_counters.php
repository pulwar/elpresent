<?php

class m140509_093049_fix_order_counters extends CDbMigration
{
	public function up()
	{
        $this->update('{{item}}', ['order_count' => 0]);
        $sql = 'SELECT t.id, COUNT(o.id) as order_count FROM {{item}} t INNER JOIN {{order}} o ON o.item_id = t.id GROUP BY t.id';
        $command = $this->dbConnection->createCommand($sql);
        $dataReader = $command->query();

        while(($row = $dataReader->read())) {
            $this->processCounter($row['id'], $row['order_count']);
            unset($row);
            $dataReader->next();
        }
	}

	public function down()
	{

	}

    protected function processCounter($id, $count) {
        $this->update('{{item}}', [
            'order_count' => $count
        ], 'id = :id', ['id' => $id]);
    }

}