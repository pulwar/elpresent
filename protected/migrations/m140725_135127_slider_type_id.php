<?php

class m140725_135127_slider_type_id extends CDbMigration
{
	public function up()
	{
        $this->addColumn('el_slider', 'type_id', 'int(11) DEFAULT 1');
	}

	public function down()
	{
        $this->dropColumn('el_slider', 'type_id');
		return false;
	}
}