<?php

class m140609_194528_user_birthday extends CDbMigration
{
	public function up()
	{
        $this->addColumn('el_user', 'user_birthday', 'int(11)');
	}

	public function down()
	{
        $this->dropColumn('el_user', 'user_birthday');
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}