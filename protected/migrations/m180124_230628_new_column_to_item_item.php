<?php

class m180124_230628_new_column_to_item_item extends CDbMigration
{
	public function up()
	{
	    try {
            $this->addColumn('{{item_item}}', 'city_id', 'INT DEFAULT NULL');
            $this->addColumn('{{item_item}}', 'option_id', 'INT DEFAULT NULL');
            $this->addColumn('{{item_item}}', 'id', 'pk');
            return true;
        } catch (\Exception $e) {
	        return false;
        }
	}

	public function down()
	{
		echo "m180124_230628_new_column_to_item_item does not support migration down.\n";
        $this->dropColumn('{{item_item}}', 'city_id');
        $this->dropColumn('{{item_item}}', 'option_id');
        $this->dropColumn('{{item_item}}', 'id');
        return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}