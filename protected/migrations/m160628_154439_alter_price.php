<?php

class m160628_154439_alter_price extends CDbMigration
{
	public function up()
	{
		$this->alterColumn('el_item_with_city', 'item_price', 'float DEFAULT NULL');
		$this->alterColumn('el_item_with_partner', 'partner_price', 'float DEFAULT NULL');
		$this->alterColumn('el_order', 'price', 'float DEFAULT NULL');
		$this->alterColumn('el_discounts', 'price', 'float DEFAULT NULL');
		$this->alterColumn('el_remote_sales_orders', 'sum', 'float DEFAULT NULL');
		$this->alterColumn('el_user', 'cash', 'float DEFAULT NULL');
		$this->alterColumn('el_webpay', 'wsb_total', 'float DEFAULT NULL');
	}

	public function down()
	{
		$this->alterColumn('el_item_with_city', 'item_price', 'integer DEFAULT NULL');
		$this->alterColumn('el_item_with_partner', 'partner_price', 'integer DEFAULT NULL');
		$this->alterColumn('el_order', 'price', 'integer DEFAULT NULL');
		$this->alterColumn('el_discounts', 'price', 'integer DEFAULT NULL');
		$this->alterColumn('el_remote_sales_orders', 'sum', 'integer DEFAULT NULL');
		$this->alterColumn('el_user', 'cash', 'integer DEFAULT NULL');
		$this->alterColumn('el_webpay', 'wsb_total', 'integer DEFAULT NULL');
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}