<?php

class m180221_063921_el_order_add_column extends CDbMigration
{
	public function up()
	{
		$this->addColumn('el_order', 'validity_date', 'DATE DEFAULT NULL');
	}

	public function down()
	{
		$this->dropColumn('el_order', 'validity_date');
		return false;
	}
}