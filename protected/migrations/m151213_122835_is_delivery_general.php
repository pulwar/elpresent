<?php

class m151213_122835_is_delivery_general extends CDbMigration
{
    public function up()
    {
        $this->addColumn('el_order', 'is_delivery_general', 'tinyint(1) DEFAULT 0');
    }

    public function down()
    {
        $this->dropColumn('el_order', 'is_delivery_general');
        return false;
    }
}