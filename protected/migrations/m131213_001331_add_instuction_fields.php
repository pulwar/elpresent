<?php

class m131213_001331_add_instuction_fields extends CDbMigration
{
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
        $this->addColumn('{{item}}', 'instuction_description', 'text');
        $this->addColumn('{{item}}', 'need_to_know', 'text');
	}

	public function safeDown()
	{
        $this->dropColumn('{{item}}', 'instuction_description');
        $this->dropColumn('{{item}}', 'need_to_know');
	}

}