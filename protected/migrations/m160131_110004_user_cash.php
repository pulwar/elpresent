<?php

class m160131_110004_user_cash extends CDbMigration
{
    public function up()
    {
        $this->addColumn('el_order', 'user_cash', 'int(11) DEFAULT 0');
    }

    public function down()
    {
        $this->dropColumn('el_order', 'user_cash');
        return false;
    }
}