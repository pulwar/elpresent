<?php

class m150207_074706_partners_updates extends CDbMigration
{
	public function up()
	{
		$this->update('el_text_record',
			array(
				'text' => '
            <div class="partners-list">
    <div class="partner-item clearfix">
        <div class="col-md-3">
            <a href="http://voditel.by/"><img src="/images/partners-voditel.gif" alt="" /></a>
        </div>
        <div class="col-md-9">
            <h4>Voditel.by</h4>
            <p>
            Школа экстремального вождения &ndash;
                ваша безопасность в сложной дорожной ситуации. Узнайте предельные возможности автомобиля и свои как водителя.
            </p>
        </div>
    </div>

    <div class="partner-item clearfix">
        <div class="col-md-3">
            <a rel="nofollow" href="http://taboo.by/"><img src="/images/partners-taboo.jpg" alt="" /></a>
        </div>
        <div class="col-md-9">
            <h4>Taboo.by</h4>
            <p>
            Наше &laquo;Табу&raquo; - это запрет на серость, скуку, безвкусицу. Наши принципы &ndash; это яркость, свобода и качество.

            </p>
        </div>
    </div>

    <div class="partner-item clearfix">
        <div class="col-md-3">
            <a rel="nofollow" href="http://arbyz.by/"><img src="/images/partners-arbyz.jpg" alt="" /></a>
        </div>
        <div class="col-md-9">
            <h4>Студия праздника Arbyz.by</h4>
            <p>
                Студия праздника Arbyz.by : самые интересные и необычные развлечения.
            </p>
        </div>
    </div>

    <div class="partner-item clearfix">
        <div class="col-md-3">
            <a rel="nofollow" href="http://magicsun.by/"><img src="/images/partners-magic-sun.jpg" alt="" /></a>
        </div>
        <div class="col-md-9">
            <h4>Студия красоты Magic Sun</h4>
            <p>
                Студия красоты Magic Sun &ndash; это атмосфера уюта и заботы в самом центре Минска.
            </p>
        </div>
    </div>

    <div class="partner-item clearfix">
        <div class="col-md-3">
            <a rel="nofollow" href="http://yoga-minsk.info"><img src="/images/partners-ega-hata.jpg" alt="" /></a>
        </div>
        <div class="col-md-9">
            <h4>Студия йоги &laquo;ЁГА-ХАТА&raquo;</h4>
            <p>
                Студия йоги &laquo;ЁГА-ХАТА&raquo;. Групповые и индивидуальные занятия. Необычные йога-туры и семинары.
            </p>
        </div>
    </div>

    <div class="partner-item clearfix">
        <div class="col-md-3">
            <a rel="nofollow" href="http://www.way-of-health.com/"><img src="/images/partners-put-zdorovia.jpg" alt="" /></a>
        </div>
        <div class="col-md-9">
            <h4>Путь Здоровья</h4>
            <p>
                "Главная цель клуба &laquo;Путь Здоровья&raquo; - отличное настроение и крепкое здоровье у Вас и Вашей семьи!"
            </p>
        </div>
    </div>

    <div class="partner-item clearfix">
        <div class="col-md-3">
            <a rel="nofollow" href="http://adrenalin.by/"><img src="/images/partners-adrenalin.jpg" alt="" /></a>
        </div>
        <div class="col-md-9">
            <h4>Адреналин</h4>
            <p>
                Адреналин. Самая большая площадка в Минске, возможность играть одновременно большим компаниям, детский день рождения, корпоративный отдых.
            </p>
        </div>
    </div>

    <div class="partner-item clearfix">
        <div class="col-md-3">
            <a rel="nofollow" href="http://www.jazz-hand.by/"><img src="/images/partners-jazz-hand.jpg" alt="" /></a>
        </div>
        <div class="col-md-9">
            <h4>Jazzhand</h4>
            <p>
                Jazzhand. Центр современного искусства.
            </p>
        </div>
    </div>

    <div class="partner-item clearfix">
        <div class="col-md-3">
            <img src="/images/partners-moika.gif" alt="" />
        </div>
        <div class="col-md-9">
            <h4>Сеть ручных автомоек</h4>
        </div>
    </div>

    <div class="partner-item clearfix">
        <div class="col-md-3">
            <img src="/images/partners-healthy-joy.gif" alt="" />
        </div>
        <div class="col-md-9">
            <h4>Салон красоты</h4>
        </div>
    </div>

    <div class="partner-item clearfix">
        <div class="col-md-3">
            <a rel="nofollow" href="http://www.vadimnardin.com/"><img src="/images/partners-vadim-nardin.gif" alt="" /></a>
        </div>
        <div class="col-md-9">
            <h4>Фотограф Вадим Нардин</h4>
        </div>
    </div>

    <div class="partner-item clearfix">
        <div class="col-md-3">
            <a rel="nofollow" href="http://megafit.by/"><img src="/images/partners-megafitnes.jpg" alt="" /></a>
        </div>
        <div class="col-md-9">
            <h4>Фитнес Клуб "МегаФитнес"</h4>
        </div>
    </div>

    <div class="partner-item clearfix">
        <div class="col-md-3">
            <img src="/images/partners-invino.jpg" alt="" />
        </div>
        <div class="col-md-9">
            <h4>Invino</h4>
            <p>
                Древние считали, что &laquo;истина - в вине&raquo;, по латыни - &laquo;In vino veritas&raquo;. В наше время, чтобы познать истину, не обязательно пить вино. Достаточно посетить новый концептуальный ресторан In Vino, и на вопрос: &laquo;Где же все-таки обитает истина?&raquo;, будет получен исчерпывающий ответ. Она - в Вашем сердце.
            </p>
        </div>
    </div>

    <div class="partner-item clearfix">
        <div class="col-md-3">
            <a rel="nofollow" href="http://orangeclub.by/"><img src="/images/partners-orangeclub.jpg" alt="" /></a>
        </div>
        <div class="col-md-9">
            <h4>ORANGE CLUB</h4>
            <p>
                Центр активного отдыха ORANGE CLUB - это настоящая находка для любителей активного отдыха и здорового образа жизни. Атмосфера заведения располагает к постоянному движению и активному досугу.
            </p>
        </div>
    </div>

    <div class="partner-item clearfix">
        <div class="col-md-3">
            <a rel="nofollow" href="http://tancuem.by/"><img src="/images/partners-hastl.jpg" alt="" /></a>
        </div>
        <div class="col-md-9">
            <h4>Tancuem.by</h4>
            <p>
            Мы очень разные: мужчины и женщины, от 20 и до 50, домохозяйки и директора компаний, парашютисты и бухгалтеры, но есть нечто, что сближает и объединяет всех нас - МЫ ТАНЦУЕМ! Присоединяйтесь к нам!
            </p>
        </div>
    </div>
</div>'
			),
			'id=14'
		);
	}

	public function down()
	{
		echo "m150202_180020_customers_page_update does not support migration down.\n";
		return false;
	}
}