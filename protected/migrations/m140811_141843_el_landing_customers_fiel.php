<?php

class m140811_141843_el_landing_customers_fiel extends CDbMigration
{
	public function up()
	{
        $this->addColumn('el_landing', 'customers', 'text NOT NULL');
	}

	public function down()
	{
        $this->dropColumn('el_landing', 'customers');
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}