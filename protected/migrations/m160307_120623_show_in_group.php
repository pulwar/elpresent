<?php

class m160307_120623_show_in_group extends CDbMigration
{
    public function up()
    {
        $this->addColumn('el_option', 'show_in_group', 'tinyint(1) DEFAULT 0');
    }

    public function down()
    {
        $this->dropColumn('el_option', 'show_in_group');
        return false;
    }
}