<?php

class m140913_163828_kit_ids extends CDbMigration
{
    public function up()
    {
        $this->addColumn('el_order', 'kit_ids', 'VARCHAR(255) NOT NULL');
    }

    public function down()
    {
        $this->dropColumn('el_order', 'kit_ids');
        return false;
    }
}