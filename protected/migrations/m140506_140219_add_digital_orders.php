<?php

class m140506_140219_add_digital_orders extends CDbMigration
{
	public function up()
	{
        $this->addColumn('{{order}}', 'type', 'integer NOT NULL');
	}

	public function down()
	{
		$this->dropColumn('{{order}}', 'type');
	}
}