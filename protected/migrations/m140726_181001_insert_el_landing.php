<?php

class m140726_181001_insert_el_landing extends CDbMigration
{
	public function up()
	{
        $this->insert('el_landing', array(
            'phones' => 'Content',
            'address' => 'Content',
            'time_work' => 'Content',
            'map_code' => 'Content',
        ));
	}

	public function down()
	{
		echo "m140726_181001_insert_el_landing does not support migration down.\n";
		return false;
	}
}