<?php

class m171229_181018_order_price_fields extends CDbMigration
{
	public function up()
	{
	    $this->addColumn('el_order', 'delivery_price', 'FLOAT DEFAULT NULL');
	    $this->addColumn('el_order', 'delivery_free_price', 'FLOAT DEFAULT NULL');

	    return true;
	}

	public function down()
	{
		echo "m171229_181018_order_price_fields does not support migration down.\n";
		$this->dropColumn('el_order', 'delivery_price');
		$this->dropColumn('el_order', 'delivery_free_price');

		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}