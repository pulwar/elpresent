<?php

class m140502_202831_database_refactoring extends CDbMigration
{
	public function up()
	{
        $this->dropTable('old_category');
        $this->dropTable('old_category_items');
        $this->dropTable('old_subcategory');
        $this->dropTable('old_subcategory_items');
        $this->dropTable('el_user_eldiscount');
        $this->dropTable('el_admin_user');
        $this->dropTable('el_db_patcher');
        $this->dropTable('el_holiday_birthday');
        $this->dropTable('el_holiday_day');

        // change mapping for webpay orders
        $this->addColumn('{{webpay}}', 'order_id', 'integer NOT NULL');
        // map all orders
        $command = $this->dbConnection->createCommand(
            'SELECT order_id, webpay_id FROM {{webpay_orders}}');
        $dataReader = $command->query();

        while($row = $dataReader->read()) {
            $this->update('{{webpay}}', [
                'order_id' => $row['order_id']
            ], 'id=:webpay_id', [
                'webpay_id' => $row['webpay_id']
            ]);
            unset($row);
            $dataReader->next();
        }

//        $this->dropTable('el_webpay_orders');
	}

	public function down()
	{
		echo "m140502_202831_database_refactoring does not support migration down.\n";
		return false;
	}
}