<?php

class m171221_215144_new_order_statuses extends CDbMigration
{
	public function up()
	{
        $this->addColumn('el_order', 'is_canceled', 'BOOLEAN NOT NULL DEFAULT 0');
        $this->addColumn('el_order', 'is_processed', 'BOOLEAN NOT NULL DEFAULT 0');
        return true;
	}

	public function down()
	{
		echo "m171221_215144_new_order_statuses does not support migration down.\n";
        $this->dropColumn('el_order', 'is_canceled');
        $this->dropColumn('el_order', 'is_processed');
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}