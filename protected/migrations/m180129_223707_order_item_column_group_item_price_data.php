<?php

class m180129_223707_order_item_column_group_item_price_data extends CDbMigration
{
	public function up()
	{
	    $this->addColumn('{{order_items}}', 'group_item_price_data', 'TEXT DEFAULT NULL');
	    return true;
	}

	public function down()
	{
		echo "m180129_223707_order_item_column_group_item_price_data does not support migration down.\n";
        $this->dropColumn('{{order_items}}', 'group_item_price_data');
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}