<?php

class m131113_170352_change_category_structure extends CDbMigration
{
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
//        return true;

        $prefix = $this->getDbConnection()->tablePrefix;
        foreach(array('category', 'subcategory', 'category_items', 'subcategory_items') as $tableName) {
            $this->renameTable($prefix.$tableName, 'old_'.$tableName);
        }

        $this->createTable($prefix.'category', array(
            'id' => 'pk',
            'parent_id' => 'int',
            'title' => 'string not null',
            'slug' => 'string not null',
            'icon' => 'string',
            'position' => 'int not null',
            'meta_description' => 'text',
            'meta_keywords' => 'text',
            'meta_title' => 'string',
            'seo_text' => 'text',
            'visible' => 'boolean not null default "0"'
        ));

        $this->createTable($prefix.'category_has_items', array(
            'category_id' => 'int not null',
            'item_id' => 'int not null'
        ));

        // remap entities
        $categories = $this
            ->getDbConnection()
            ->createCommand('select * from old_category')
            ->queryAll()
        ;

        $idsMapping = [];

        foreach($categories as $data) {
            $categoryId = $data['id'];

            $category = $this->createCategory($data);
            $category->save(false);

            $this
                ->getDbConnection()
                ->createCommand('INSERT INTO {{category_has_items}} '
                    .'SELECT ' . $category->id . ', l.item_id FROM old_category_items as l INNER JOIN el_item as i ON l.item_id=i.id AND l.category_id=:categoryId')
                ->execute(array(
                    ':categoryId' => $data['id']
                ))
            ;

            // add subcategories
            $sub = $this
                ->getDbConnection()
                ->createCommand('SELECT * FROM old_subcategory WHERE category_id=:categoryId')
                ->queryAll(true, array(
                    ':categoryId' => $categoryId,
                ))
            ;

            foreach($sub as $childrenData) {
                $children = $this->createCategory($childrenData, $category);
                $children->save(false);

                // link children's items
                $this
                    ->getDbConnection()
                    ->createCommand('INSERT INTO {{category_has_items}} (category_id, item_id) '
                        .'SELECT ' . $children->id . ', item_id FROM old_subcategory_items l INNER JOIN el_item i ON l.item_id=i.id AND l.subcategory_id=:childrenId')
                    ->execute(array(
                        ':childrenId' => $childrenData['id']
                    ))
                ;


            }

            // update mapping
            $idsMapping[$data['id']] = $category->id;
        }

        // save to runtime directory
        $path = Yii::app()->getRuntimePath();
        file_put_contents($path . '/categories_mapping.php', "<?php \n\n return " . var_export($idsMapping, true) . ';');

    }

	public function safeDown()
	{
        $prefix = $this->getDbConnection()->tablePrefix;
        $this->dropTable($prefix.'category');
        $this->dropTable($prefix.'category_has_items');

        foreach(array('category', 'subcategory', 'category_items', 'subcategory_items') as $tableName) {
            $this->renameTable('old_'.$tableName, $prefix.$tableName);
        }
	}

    /**
     * @param array $data
     * @param Category $parent
     * @return Category
     */
    private function createCategory(array $data, Category $parent = null)
    {
        $category = new Category();

        if ($parent) {
            $category->parent_id = $parent->id;
        }

        $category->title = $data['title'];
        $category->slug = $data['link'];
        $category->icon = $data['img'];
        $category->position = isset($data['order']) ? $data['order'] : 0;
        $category->meta_description = isset($data['description']) ? $data['description'] : '';
        $category->meta_keywords = isset($data['keywords']) ? $data['keywords'] : '';
        $category->meta_title = isset($data['seo_title']) ? $data['seo_title'] : '';
        $category->seo_text = isset($data['seo_text']) ? $data['seo_text'] : '';
        $category->visible = !isset($data['visible']) || $data['visible'];

        return $category;
    }


}