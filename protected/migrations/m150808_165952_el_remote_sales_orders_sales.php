<?php

class m150808_165952_el_remote_sales_orders_sales extends CDbMigration
{
	public function up()
	{
		$this->addColumn('el_remote_sales_orders', 'discont', 'int(11) DEFAULT 0');
	}

	public function down()
	{
		$this->dropColumn('el_remote_sales_orders', 'discont');
		return false;
	}
}