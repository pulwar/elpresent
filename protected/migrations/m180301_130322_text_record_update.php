<?php

class m180301_130322_text_record_update extends CDbMigration
{
	public function up()
	{

		$this->update('el_text_record',
			array(
				'text' => '<h1>О подарке</h1>
				<!-- NEW CONTENT HERE -->
				<div class="main-img-howtobuy"><img src="/images/main-howtobuy.jpg" alt="На нашем сайте вы найдете более 800 услуг" /></div>
				<h2 class="howtobuy relative-bottom">Как выглядят наши подарочные <br />сертификаты?</h2>
				<div class="certificates-howtoby"><img src="/images/certificates-howtoby.jpg" alt="Как выглядат наши подарочные сертификаты?" /></div>
				<h2 class="howtobuy margin-bottom">Как купить?</h2>
				<div class="howtoby-steps clearfix">
				<div class="clearfix">
				<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"><img src="/images/step1.jpg" alt="Шаг 1" /></div>
				<div class="col-lg-7 col-md-7 col-sm-7 col-xs-7"><img src="/images/step2.jpg" alt="Шаг 2" /></div>
				</div>
				<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5"><img src="/images/step4.jpg" alt="Шаг 3" /></div>
				<div class="col-lg-7 col-md-7 col-sm-7 col-xs-7"><img src="/images/step3.jpg" alt="Шаг 4" /></div>
				</div>
				<h2 class="howtobuy margin-bottom">Как воспользоваться подарком?</h2>
				<div class="howtouse clearfix"><img src="/images/howtouse-howtobuy-2.jpg" alt="Как воспользоваться подарком?" />
				<div class="col-md-6">
				<p>Записаться на услугу в любое удобное для Вас время, <br />используя контактную информацию, указанную на подарочном сертификате</p>
				</div>
				<div class="col-md-6">
				<p>Насладиться эмоциями <br /> и впечатлениями</p>
				</div>
				</div>
				<!-- / NEW CONTENT HERE -->'
			),
			'id=11'
		);
	}

	public function down()
	{
		echo "m180301_130322_text_record_update does not support migration down.\n";
		return false;
	}
}