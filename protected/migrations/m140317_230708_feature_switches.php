<?php

class m140317_230708_feature_switches extends CDbMigration
{

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
        $this->addColumn('{{user}}', 'feature_switches', 'text default null');
	}

	public function safeDown()
	{
        $this->dropColumn('{{user}}', 'feature_switches');
	}
}