<?php

class m130120_151046_add_teasers_managment extends CDbMigration
{
	public function up()
	{
        $prefix = $this->getDbConnection()->tablePrefix;
        $this->createTable($prefix . 'teasers', array(
            'id' => 'pk',
            'image' => 'string NOT NULL',
            'description' => 'string NOT NULL',
            'link' => 'string NOT NULL',
        ));

        // insert data into new table
        $teasers = array(
            array (
                'image' => 'superimpressions.jpg',
                'link' => '/superimpressions.html',
                'description' => 'Выбери одно из восьми',
            ),

            array (
                'image' => 'new-year.jpg',
                'link' => '/news/93.html',
                'description' => 'Новогодние подарки',
            ),

            array (
                'image' => 'corp-clients.jpg',
                'link' => '/pages/CorporateService.html',
                'description' => 'Корпоративным клиентам',
            )
        );

        // set data
        $teaser = new Teaser();
        foreach($teasers as $teaserData) {
            $teaser->image = $teaserData['image'];
            $teaser->description = $teaserData['description'];
            $teaser->link = $teaserData['link'];
            $teaser->save();
            $teaser->setIsNewRecord(true);
            $teaser->id = null;
        }

	}

	public function down()
	{
        $prefix = $this->getDbConnection()->tablePrefix;
        $this->dropTable($prefix . 'teasers');

		return false;
	}
}