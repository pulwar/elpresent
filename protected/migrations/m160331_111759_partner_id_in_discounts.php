<?php

class m160331_111759_partner_id_in_discounts extends CDbMigration
{
    public function up()
    {
        $this->addColumn('el_discounts', 'partner_id', 'int(11)');
    }

    public function down()
    {
        $this->dropColumn('el_discounts', 'partner_id');

        return false;
    }
}