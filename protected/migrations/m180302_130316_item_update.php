<?php

class m180302_130316_item_update extends CDbMigration
{
	public function up()
	{
		echo "Update need_to_know and instuction_description\n";

		$sql = "update el_item set need_to_know = substr(text, locate('Что нужно знать?', text)), instuction_description = `desc`";

		$this->execute($sql);

	}

	public function down()
	{
		echo "m180302_130316_item_update does not support migration down.\n";
		return false;
	}
}