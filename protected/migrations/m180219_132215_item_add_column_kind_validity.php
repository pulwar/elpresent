<?php

class m180219_132215_item_add_column_kind_validity extends CDbMigration
{
	public function up()
	{
		$this->addColumn('el_item', 'kind_validity', 'tinyint(1) DEFAULT 0');
	}

	public function down()
	{
		$this->dropColumn('el_item', 'kind_validity');
		return false;
	}
}