<?php

class m131119_110506_add_perm_link extends CDbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{item}}', 'perm_link', 'string not null');

        $normalizer = new TransliterationBehaviour();

        // generate perm links
        $query = $this->dbConnection->createCommand('SELECT i.id, i.title, c.slug, p.slug as pslug from {{item}} i INNER JOIN {{category_has_items}} chi ON chi.item_id=i.id INNER JOIN {{category}} c ON c.id = chi.category_id LEFT JOIN {{category}} p ON p.id = c.parent_id');
        $data = $query->queryAll();

        $links = [];
        foreach ($data as $row) {
            $link = $row['slug'];
            if (!empty($row['pslug'])) {
                $link = $row['pslug'] . '/' . $link;
            }

            $link .= '/' . $normalizer->normalizeUri($row['title']);
            if (!isset($links[$row['id']])) {
                $links[$row['id']] = [];
            }

            $links[$row['id']][] = $link;
        }

        $notSoImportantCategories = [
            '8-marta-podarki', 'leto', 'podarki-dlja-muzhchin', ''
        ];
        $notSoImportantCategories = array_map(function ($category) {
            return preg_quote($category);
        }, $notSoImportantCategories);

        // filter links
        $links = array_map(function ($options) use ($notSoImportantCategories) {
            if (1 == count($options)) {
                return $options[0];
            }
            // sort by nesting level
            usort($options, function ($a, $b) {
                return substr_count($b, '/') - substr_count($a, '/');
            });
            $maxNestingLevel = substr_count($options[0], '/');
            // filter all links with nesting level less than maxNestingLevel
            $options = array_filter($options, function ($option) use ($maxNestingLevel) {
                return $maxNestingLevel == substr_count($option, '/');
            });

            if (1 == count($options)) {
                return $options[0];
            }

            // try to filter not so important categories
            $regExp = '/^('.implode('|', $notSoImportantCategories).')\//U';
            $filtered = array_values(array_filter($options, function ($option) use ($regExp) {
                return !preg_match($regExp, $option);
            }));

            if (0 == count($filtered)) {
                $filtered = $options;
            }

            if (1 == count($filtered)) {
                return $filtered[0];
            }

            usort($filtered, function ($a, $b) {
                return strlen($a) - strlen($b);
            });

            return $filtered[0];
        }, $links);


        $processed = [];
        foreach ($links as $id => $link) {
            $suffix = '';$i=0;
            while(isset($processed[$link.$suffix])) {
                $suffix = $i++;
            }
            $processed[$link.$suffix] = $id;
        }

        foreach ($processed as $link => $id) {
            $this->update('{{item}}', [
                'perm_link' => $link
            ], 'id = :id', ['id' => $id]);
        }
    }

    public function safeDown()
    {
        $this->dropColumn('{{item}}', 'perm_link');
    }
}