<?php

class m180302_151212_item_update extends CDbMigration
{
	public function up()
	{
		echo "Update need_to_know and instuction_description\n";

		$sql = "update el_item i
join el_order_items o
on i.id = o.item_id
join `el_partner` p
on p.id = o.partner_id
set i.need_to_know = CONCAT(i.need_to_know, '<p><strong>Место проведения:</strong> ', p.place, '</p>', '<p><strong>Контакты:</strong> телефоны ', p.phone, '; время работы ', p.work_time, '</p>')
where p.place != ''
and  p.phone !=''
and p.work_time !=''
and i.need_to_know != '';";

		$this->execute($sql);

	}

	public function down()
	{
		echo "m180302_151212_item_update does not support migration down.\n";
		return false;
	}

}