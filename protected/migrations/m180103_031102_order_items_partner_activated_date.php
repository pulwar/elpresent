<?php

class m180103_031102_order_items_partner_activated_date extends CDbMigration
{
	public function up()
	{
	    $this->addColumn('el_order_items', 'partner_activated_date', 'DATE DEFAULT NULL');
	    return true;
	}

	public function down()
	{
		echo "m180103_031102_order_items_partner_activated_date does not support migration down.\n";
		$this->dropColumn('el_order_items', 'partner_activated_date');
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}