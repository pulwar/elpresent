<?php

class m190409_110809_el_feature_values_add_column extends CDbMigration
{
	public function up()
	{
        $this->addColumn('el_feature_values', 'available', 'BOOLEAN NOT NULL DEFAULT 0');
	}

	public function down()
	{
		echo "m190409_110809_el_feature_values_add_column does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}