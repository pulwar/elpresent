<?php

class m131218_090803_fix_static_price_and_partner_price_fields extends CDbMigration
{
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
        $this->alterColumn('{{item}}', 'partner_price', 'integer DEFAULT NULL');
        $this->alterColumn('{{item}}', 'price', 'integer DEFAULT NULL');
	}

	public function safeDown()
	{
        $this->alterColumn('{{item}}', 'partner_price', 'integer NOT NULL');
        $this->alterColumn('{{item}}', 'price', 'integer NOT NULL');
	}
}