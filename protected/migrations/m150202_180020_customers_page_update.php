<?php

class m150202_180020_customers_page_update extends CDbMigration
{
	public function up()
	{
		$this->update('el_text_record',
			array(
				'text' => '<h1>Наши партнеры</h1>
            <div class="partners-list">
                <div class="partner-item clearfix">
                  <div class="col-md-3">
                    <a href="#" target="_blank"><img src="/images/partners-taboo.jpg" alt="image"></a>
                  </div>
                  <div class="col-md-9">
                    <h4>Салон студия ТАБУ</h4>
                    <p>Наше «Табу» - это запрет на серость, скуку, безвкусицу. Наши принципы – это яркость, свобода и качество.</p>
                  </div>
                </div>

                <div class="partner-item clearfix">
                  <div class="col-md-3">
                    <a href="#" target="_blank"><img src="/images/partners-arbyz.jpg" alt="image"></a>
                  </div>
                  <div class="col-md-9">
                    <h4>Студия праздника Arbyz.by</h4>
                    <p>Студия праздника Arbyz.by : самые интересные и необычные развлечения.</p>
                  </div>
                </div>

                <div class="partner-item clearfix">
                  <div class="col-md-3">
                    <a href="#" target="_blank"><img src="/images/partners-magic-sun.jpg" alt="image"></a>
                  </div>
                  <div class="col-md-9">
                    <h4>Студия красоты Magic Sun</h4>
                    <p>Студия красоты Magic Sun – это атмосфера уюта и заботы в самом центре Минска.</p>
                  </div>
                </div>
             </div>'
			),
			'id=25'
		);
	}

	public function down()
	{
		echo "m150202_180020_customers_page_update does not support migration down.\n";
		return false;
	}

}