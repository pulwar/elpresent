<?php

class m140618_091433_el_item_with_partner_add_option_id extends CDbMigration
{
	public function up()
	{
        $this->addColumn('el_item_with_partner', 'option_id', 'int(11) DEFAULT 0');
	}

	public function down()
	{
        $this->dropColumn('el_item_with_partner', 'option_id');
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}