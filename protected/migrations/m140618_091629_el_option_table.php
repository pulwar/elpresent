<?php

class m140618_091629_el_option_table extends CDbMigration
{
	public function up()
	{
        $this->createTable('el_option', array(
            'id' => 'pk',
            'name' => 'VARCHAR(200) NOT NULL',
            'desc' => 'text NOT NULL',
        ), 'ENGINE=InnoDB CHARSET=utf8');
	}

	public function down()
	{
        dropTable('el_option');
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}