<?php

class m140309_231044_add_order_counter extends CDbMigration
{
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
        $this->addColumn('{{item}}', 'order_count', 'integer NOT NULL DEFAULT 0');
        $sql = 'SELECT t.id, COUNT(o.id) as order_count FROM {{item}} t INNER JOIN {{order}} o ON o.item_id = t.id GROUP BY t.id';
        $command = $this->dbConnection->createCommand($sql);
        $dataReader = $command->query();

        while(($row = $dataReader->read())) {
            $this->processCounter($row['id'], $row['order_count']);
            unset($row);
            $dataReader->next();
        }
	}

    protected function processCounter($id, $count) {
        $this->update('{{item}}', [
            'order_count' => round($count * 1.5)
        ], 'id = :id', ['id' => $id]);
    }

	public function safeDown()
	{
        $this->dropColumn('{{item}}', 'order_count');
	}

}