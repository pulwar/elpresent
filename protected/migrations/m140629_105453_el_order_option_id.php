<?php

class m140629_105453_el_order_option_id extends CDbMigration
{
	public function up()
	{
        $this->addColumn('el_order', 'option_id', 'int(11) DEFAULT 0');
	}

	public function down()
	{
        $this->dropColumn('el_order', 'option_id');
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}