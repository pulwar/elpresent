<?php

class m171223_221148_column_payment_type_to_bepaid extends CDbMigration
{
	public function up()
	{
        $this->addColumn('el_bepaid', 'type', 'INT DEFAULT NULL');
        $this->update('el_bepaid', ['type' => 6]);
        return true;
	}

	public function down()
	{
		echo "m171223_221148_column_payment_type_to_bepaid does not support migration down.\n";
        $this->dropColumn('el_bepaid', 'type');
        return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}