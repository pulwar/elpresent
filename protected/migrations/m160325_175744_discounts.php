<?php

class m160325_175744_discounts extends CDbMigration
{
    public function up()
    {
        $this->createTable('{{discounts}}', array(
            'id' => 'pk',
            'title' => 'VARCHAR(255) NOT NULL',
            'alias' => 'VARCHAR(255) NOT NULL',
            'tiser' => 'text',
            'text' => 'text',
            'meta_k' => 'VARCHAR(255)',
            'meta_d' => 'text',
            'category_id' => 'integer NOT NULL',
            'price' => 'integer',
            'date' => 'integer',
            'img_small' => 'VARCHAR(255)',
            'img_big' => 'VARCHAR(255)',
            'hide' => 'boolean DEFAULT 0',
            'order_items' => 'integer DEFAULT 0',

        ), 'ENGINE=InnoDB CHARSET=utf8');
    }

    public function down()
    {
        $this->dropTable('{{discounts}}');
    }
}