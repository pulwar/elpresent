<?php

class m151123_153750_order_message extends CDbMigration
{
	public function up()
	{
        $this->addColumn('el_order', 'message', 'text');
	}

	public function down()
	{
        $this->dropColumn('el_order', 'message');
		return false;
	}
}