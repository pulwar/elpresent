<?php

class m150908_163518_el_remote_sales_orders_fio extends CDbMigration
{
    public function up()
    {
        $this->addColumn('el_remote_sales_orders', 'fio', 'VARCHAR(100) NOT NULL');
    }

    public function down()
    {
        $this->dropColumn('el_remote_sales_orders', 'fio');
        return false;
    }
}