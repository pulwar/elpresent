<?php

class m171230_153305_new_fields_order_item extends CDbMigration
{
	public function up()
	{
	    $this->addColumn('el_order_items', 'partner_id', 'INT DEFAULT NULL');
	    $this->addColumn('el_order_items', 'partner_price', 'FLOAT DEFAULT NULL');
	    $this->addColumn('el_order_items', 'partner_activated', 'BOOLEAN NOT NULL DEFAULT 0');
	    $this->updateOrderItems();

        return true;
	}

	private function updateOrderItems()
    {
        $orderItems = $this->query('SELECT oi.id, oi.item_id, oi.option_id FROM el_order_items oi');
        foreach ($orderItems as $item) {
            $sql = 'SELECT ip.partner_id, ip.partner_price FROM el_item i LEFT JOIN el_item_with_partner ip ON i.id=ip.item_id WHERE ip.option_id='.$item['option_id'].' AND i.id ='.$item['item_id'];
            $price = $this->query($sql);
            if ($price) {
                $price = $price[0];
                $this->update('el_order_items', ['partner_id' => $price['partner_id']], 'id='.$item['id']);
                $this->update('el_order_items', ['partner_price' => $price['partner_price']], 'id='.$item['id']);
            }
        }
    }

    private function query($sql)
    {
        return $this->getDbConnection()->createCommand($sql)->queryAll();
    }

	public function down()
	{
		echo "m171230_153305_new_fields_order_item does not support migration down.\n";

		$this->dropColumn('el_order_items', 'partner_id');
		$this->dropColumn('el_order_items', 'partner_price');
		$this->dropColumn('el_order_items', 'partner_activated');

		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}