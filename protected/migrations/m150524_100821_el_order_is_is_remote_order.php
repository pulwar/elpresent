<?php

class m150524_100821_el_order_is_is_remote_order extends CDbMigration
{
	public function up()
	{
		$this->addColumn('el_order', 'is_remote_order', 'tinyint(1) DEFAULT 0');
	}

	public function down()
	{
		$this->dropColumn('el_order', 'is_remote_order');
		return false;
	}
}