<?php

class m131217_233349_move_prices_to_separate_table extends CDbMigration
{
	public function up()
	{
        $sql = 'SELECT id, partner_id, partner_price, price FROM {{item}} ORDER BY id DESC';
        $command = $this->dbConnection->createCommand($sql);
        $dataReader = $command->query();

        while(($row = $dataReader->read())) {
            $this->processPrice($row);
            unset($row);
            $dataReader->next();
        }
	}

	public function down()
	{
		return true;
	}

    private function processPrice($row)
    {
        // get city and partner prices
        $partnerPrices = ItemPartnerLink::model()->findByAttributes(['partner_id' => $row['partner_id'], 'item_id' => $row['id']]);
        if (empty($partnerPrices)) {
            $partnerPrices = new ItemPartnerLink();
            $partnerPrices->item_id = $row['id'];
            $partnerPrices->partner_id = $row['partner_id'];
            $partnerPrices->enabled = 1;
        }

        if ($partnerPrices->partner_price !== $row['partner_price']) {
            $partnerPrices->partner_price = $row['partner_price'];
        }

        if (!$partnerPrices->save()) {
            $i = 0;
        }

        $cityLink = ItemCityLink::model()->findByAttributes(['city_id' => 1, 'item_id' => $row['id']]);
        if (empty($cityLink)) {
            $cityLink = new ItemCityLink();
            $cityLink->city_id = 1;
            $cityLink->item_id = $row['id'];
        }

        if ($cityLink->item_price !== $row['price']) {
            $cityLink->item_price = $row['price'];
        }

        if (!$cityLink->save()) {
            $i = 5;
        }
    }
}