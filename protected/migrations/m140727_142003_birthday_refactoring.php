<?php

class m140727_142003_birthday_refactoring extends CDbMigration
{
	public function up()
	{
        $this->addColumn('{{user}}', 'user_birthday_date', 'string');
        $this->renameColumn('{{user}}', 'user_birthday', 'user_old_birthday');
        $this->addColumn('{{user}}', 'user_birthday', 'date');
        $this->migrateData(false);
        $this->dropColumn('{{user}}', 'user_old_birthday');
	}

	public function down()
	{
		$this->dropColumn('{{user}}', 'user_birthday_date');
        $this->renameColumn('{{user}}', 'user_birthday', 'user_old_birthday');
        $this->addColumn('{{user}}', 'user_birthday', 'int(11)');
        $this->migrateData(true);
        $this->dropColumn('{{user}}', 'user_old_birthday');
	}

    private function migrateData($down) {
        $sql = 'SELECT id, user_old_birthday FROM {{user}} WHERE user_old_birthday IS NOT NULL';
        $command = $this->dbConnection->createCommand($sql);
        $dataReader = $command->query();

        while(($row = $dataReader->read())) {

            $this->update(
                '{{user}}',
                $down ?
                    $this->downData($row['user_old_birthday']) : $this->upData($row['user_old_birthday']),
                'id=:id',
                ['id' => $row['id']]
            );

            unset($row);
            $dataReader->next();
        }
        $dataReader->close();
    }

    private function upData($birthday) {
        $birthday = intval($birthday, 10);
        if (!$birthday) {
            return array('user_birthday' => null);
        }

        return [
            'user_birthday' => date('Y-m-d', $birthday),
            'user_birthday_date' => date('m-d', $birthday),
        ];
    }

    private function downData($birthday) {
        if (!$birthday) {
            return array('user_birthday' => null);
        }

        $date = new \DateTime($birthday);

        return [
            'user_birthday' => $date->getTimestamp()
        ];
    }

    // disable output to STDOUT
    public function update($table, $columns, $conditions = '', $params = array())
    {

        try {
            $this->getDbConnection()->createCommand()->update($table, $columns, $conditions, $params);
        } catch (Exception $e) {
            $i = 0;$i++;
        }

    }


}