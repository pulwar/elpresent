<?php

class m131115_004018_fixed_sities extends CDbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{city}}', 'title', 'string not null');
        $this->addColumn('{{city}}', 'isDefault', 'boolean default 0');

        $cities = City::model()->findAll();
        foreach ($cities as $city) {
            $city->isDefault = 'минск' == strtolower($city->name);
            $city->title = $city->name;
            $city->name = 'минск' == strtolower($city->name) ? 'minsk' : 'brest';
            $city->save();
        }
    }

    public function safeDown()
    {
        $cities = City::model()->findAll();
        foreach ($cities as $city) {
            $city->name = $city->title;
            $city->save();
        }

        $this->dropColumn('{{city}}', 'title');
        $this->dropColumn('{{city}}', 'isDefault');
    }
}