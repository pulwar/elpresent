<?php

class m180220_065509_item_add_column_start_season_date extends CDbMigration
{
	public function up()
	{
		$this->addColumn('el_item', 'start_season_date', 'DATE DEFAULT NULL');
	}

	public function down()
	{
		$this->dropColumn('el_item', 'start_season_date');
		return false;
	}
}