<?php

class m160123_125211_user_cash extends CDbMigration
{
	public function up()
	{
        $this->addColumn('el_user', 'cash', 'int(11) DEFAULT 0');
	}

	public function down()
	{
        $this->dropColumn('el_user', 'cash');
        return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}