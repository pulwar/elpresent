<?php

class m180110_204609_partner_name_magnification_value extends CDbMigration
{
	public function up()
	{
		$this->execute('ALTER TABLE el_partner CHANGE COLUMN name name VARCHAR (100)');
        return true;
	}

	public function down()
	{
		echo "m180110_204609_partner_name_magnification_value does not support migration down.\n";
        $this->execute('ALTER TABLE el_partner CHANGE COLUMN name name VARCHAR (40)');
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}