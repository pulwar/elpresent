<?php

class m180419_084335_el_partner_add_commission extends CDbMigration
{
    public function up()
    {
        $this->addColumn('el_partner', 'commission', 'FLOAT DEFAULT 20');
    }

	public function down()
	{
		echo "m180419_084335_el_partner_add_commission does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}