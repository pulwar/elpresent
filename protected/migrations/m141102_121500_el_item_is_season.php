<?php

class m141102_121500_el_item_is_season extends CDbMigration
{
	public function up()
	{
		$this->addColumn('el_item', 'is_season', 'tinyint(1) DEFAULT 0');
	}

	public function down()
	{
		$this->dropColumn('el_item', 'is_season');
		return false;
	}
}