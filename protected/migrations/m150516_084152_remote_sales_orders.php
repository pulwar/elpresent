<?php

class m150516_084152_remote_sales_orders extends CDbMigration
{
	public function up()
	{
		$this->createTable('el_remote_sales_orders', array(
			'id' => 'pk',
			'user' => 'VARCHAR(255) NOT NULL',
			'company' => 'VARCHAR(255) NOT NULL',
			'item_id' => 'int(11) DEFAULT 0',
			'date' => 'VARCHAR(255) NOT NULL',
			'sum' => 'int(11) DEFAULT 0',
			'address' => 'VARCHAR(255) NOT NULL',
			'score' => 'int(11) DEFAULT 0',
			'code' => 'VARCHAR(12) NOT NULL',
			'activated' => 'tinyint(1) DEFAULT 0',
		),'ENGINE=InnoDB CHARSET=utf8');
	}

	public function down()
	{
		dropTable('el_remote_sales_orders');
		return false;
	}
}