<?php

class m141005_090622_el_actions extends CDbMigration
{
    public function up()
    {
        $this->createTable('el_actions', array(
            'id' => 'pk',
            'title' => 'VARCHAR(200) NOT NULL',
            'name' => 'VARCHAR(200) NOT NULL',
            'meta_k' => 'VARCHAR(255) NOT NULL',
            'meta_d' => 'text NOT NULL',
            'desc' => 'text NOT NULL',
            'txt' => 'text NOT NULL',
            'show' => 'tinyint(1) DEFAULT 1',
            'order' => 'int(10) DEFAULT 0',
            'date' => 'int(11) DEFAULT 0',
        ),'ENGINE=InnoDB CHARSET=utf8');
    }

    public function down()
    {
        dropTable('el_actions');
        return false;
    }
}