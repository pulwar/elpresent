<?php

class m161225_140107_added_price_and_another_info_to_order_items extends CDbMigration
{
	public function up()
	{
		$this->addColumn('el_order_items', 'item_title', 'varchar(255) DEFAULT NULL');
		$this->addColumn('el_order_items', 'discount_percent', 'float DEFAULT NULL');
		$this->addColumn('el_order_items', 'discount_price', 'float DEFAULT NULL');
		$this->addColumn('el_order_items', 'item_price', 'float DEFAULT NULL');
		$this->addColumn('el_order_items', 'item_price_with_discount', 'float DEFAULT NULL');
	}

	public function down()
	{
		echo "m161225_140107_added_price_and_another_info_to_order_items does not support migration down.\n";
		$this->dropColumn('el_order_items', 'item_title');
		$this->dropColumn('el_order_items', 'discount_percent');
		$this->dropColumn('el_order_items', 'discount_price');
		$this->dropColumn('el_order_items', 'item_price');
		$this->dropColumn('el_order_items', 'item_price_with_discount');
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}