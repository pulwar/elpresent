<?php

class m171225_102033_add_payment_types extends CDbMigration
{
	public function up()
	{
        $this->insert('el_payment_type', array(
            'payment_type_id' => '6',
            'payment_type_title' => 'Пластиковая карта (Bepaid)',
        ));

        $this->insert('el_payment_type', array(
            'payment_type_id' => '7',
            'payment_type_title' => 'Халва (Bepaid)',
        ));

        return true;
	}

	public function down()
	{
		echo "m171225_102033_add_payment_types does not support migration down.\n";
		$this->delete('el_payment_type', 'payment_type_id IN (6,7)');
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}