<?php

class m161221_213700_order_item_activated_item extends CDbMigration
{
	public function up()
	{
		$this->addColumn('el_order_items', 'activated_item', 'int NULL');
	}

	public function down()
	{
		echo "m161221_213700_order_item_activated_item does not support migration down.\n";
		$this->dropColumn('el_order_items', 'activated_item');
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}