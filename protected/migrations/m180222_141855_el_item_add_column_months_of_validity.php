<?php

class m180222_141855_el_item_add_column_months_of_validity extends CDbMigration
{
	public function up()
	{
		$this->addColumn('el_item', 'months_of_validity', 'integer DEFAULT 3');
	}

	public function down()
	{
		$this->dropColumn('el_item', 'months_of_validity');
	}
}