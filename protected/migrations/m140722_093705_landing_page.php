<?php

class m140722_093705_landing_page extends CDbMigration
{
	public function up()
	{
        $this->createTable('el_landing', array(
            'phones' => 'VARCHAR(255) NOT NULL',
            'address' => 'text NOT NULL',
            'time_work' => 'text NOT NULL',
        ), 'ENGINE=InnoDB CHARSET=utf8');
	}

	public function down()
	{
        dropTable('el_landing');
		return false;
	}
}