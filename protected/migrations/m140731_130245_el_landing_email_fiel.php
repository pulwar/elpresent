<?php

class m140731_130245_el_landing_email_fiel extends CDbMigration
{
	public function up()
	{
        $this->addColumn('el_landing', 'email', 'VARCHAR(255) NOT NULL');
	}

	public function down()
	{
        $this->dropColumn('el_landing', 'email');
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}