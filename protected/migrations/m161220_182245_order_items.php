<?php

class m161220_182245_order_items extends CDbMigration
{
	public function up()
	{
		$this->createTable(
			'el_order_items',
			array(
				'id' => 'pk',
				'order_id' => 'int NOT NULL',
				'item_id' => 'int NOT NULL',
				'option_id' => 'int NOT NULL',
				'code' => 'varchar(255) DEFAULT NULL',
				'recivier_name' => 'text',
				'recivier_lname' => 'varchar(255) DEFAULT NULL',
				'recivier_adr' => 'text NOT NULL',
				'recivier_phone' => 'text',
				'recivier_email' => 'text',
				'recivier_date' => 'date NOT NULL',
				'recivier_activation_date' => 'date NOT NULL',
				'activated' => 'tinyint(1) NOT NULL DEFAULT \'2\'',
				'activated_notify' => 'tinyint(1) NOT NULL DEFAULT \'0\'',
			),
			'ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC'
		);

		$this->addForeignKey('fk_order_id', 'el_order_items', 'order_id', 'el_order', 'id');
		$this->addForeignKey('fk_item_id', 'el_order_items', 'item_id', 'el_item', 'id');
	}

	public function down()
	{
		$this->dropForeignKey('fk_order_id', 'el_order_items');
		$this->dropTable('el_order_items_activation');
		echo "m161219_205236_activation_multipart_orders does not support migration down.\n";
		return false;
	}
}