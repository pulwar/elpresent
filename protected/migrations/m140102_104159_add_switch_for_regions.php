<?php

class m140102_104159_add_switch_for_regions extends CDbMigration
{
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
        $this->addColumn('{{city}}', 'enabled', 'boolean NOT NULL');
        $this->update('{{city}}', ['enabled' => true]);
	}

	public function safeDown()
	{
        $this->dropColumn('{{city}}', 'enabled');
	}
}