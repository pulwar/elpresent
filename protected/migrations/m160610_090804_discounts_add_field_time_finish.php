<?php

class m160610_090804_discounts_add_field_time_finish extends CDbMigration
{
	public function up()
	{
		$this->addColumn('el_discounts', 'time_finish', 'integer');
	}

	public function down()
	{
		$this->dropColumn('el_discounts', 'time_finish');
		return false;
	}
}