<?php

class m140610_071839_banner_bar extends CDbMigration
{
	public function up()
	{
        $this->createTable('el_slider', array(
            'id' => 'pk',
            'title' => 'VARCHAR(200) NOT NULL',
            'desc' => 'text NOT NULL',
            'link' => 'VARCHAR(200) NOT NULL',
            'img' => 'VARCHAR(200) NOT NULL',
            'show' => 'tinyint(1) DEFAULT 1',
            'order' => 'int(10) DEFAULT 0',
        ),'ENGINE=InnoDB CHARSET=utf8');
	}

	public function down()
	{
        dropTable('el_slider');
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}