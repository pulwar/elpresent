<?php

class m140413_075214_add_event_parent_field extends CDbMigration
{
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
        $this->addColumn('{{events}}', 'parent_id', 'int');
	}

	public function safeDown()
	{
        $this->dropColumn('{{events}}', 'parent_id');
	}

}