<?php

class m171206_195823_add_columnt_to_items extends CDbMigration
{
	public function up()
	{
        $this->addColumn('el_item', 'verified', 'BOOLEAN NOT NULL DEFAULT 0');
        $this->addColumn('el_item', 'updated_at', 'TIMESTAMP DEFAULT CURRENT_TIMESTAMP');
        return true;
	}

	public function down()
	{
		echo "m171206_195823_add_columnt_to_items does not support migration down.\n";
		$this->dropColumn('el_item', 'verified');
		$this->dropColumn('el_item', 'updated_at');
		return true;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}