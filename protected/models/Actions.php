<?php


class Actions extends CActiveRecord
{
    public $file;

    public function scopes()
    {
        return [
            'frontView' => ['order' => '`order` ASC, id DESC', 'condition'=>"`show`=1"],
        ];
    }

    public function init()
    {

    }
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{actions}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, title, desc, txt', 'required'),
            array('meta_k, meta_d, img', 'safe'),
            array('date', 'unsafe'),
            array('order, show', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, title, show', 'safe', 'on' => 'search'),
            array('img', 'file',
                'allowEmpty' => true,
                'maxFiles' => 1,
                'maxSize' => 1024 * 250 * 100,
                'minSize' => 1,
                'types' => 'jpg, jpeg, png, gif',
                'tooLarge' => 'Вы загружаете файл слишком большого размера',
                'tooSmall' => 'Вы загружаете файл слишком маленького размера',
                'tooMany' => 'Вы загружаете слишком большое количество файлов',
                'wrongType' => 'Неправильный тип файла',
                'wrongMimeType' => 'Неправильный MIME-тип файла'
            ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'date' => 'Дата',
            'title' => 'Тайтл (СЕО)',
            'name' => 'Название',
            'desc' => 'Тизер',
            'meta_k' => 'Мета слова (СЕО)',
            'meta_d' => 'Мета описание (СЕО)',
            'txt' => 'Основной текст',
            'show' => 'Отображать?',
            'order' => 'Порядок',
            'img' => 'Превью'
        );
    }


    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('name', $this->name, true);
        $criteria->order ='id DESC';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }


    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function beforeSave()
    {
        $this->date = time();
        return parent::beforeSave();
    }

	public function afterSave()
	{
		return parent::afterSave();
	}

    public function afterDelete()
    {
        return parent::afterDelete();
    }
}
