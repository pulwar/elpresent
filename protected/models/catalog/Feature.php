<?php

/**
 * Class Feature
 *
 * @property integer $feature_id
 * @property string $feature_value
 */
class Feature extends CActiveRecord
{
    public function primaryKey()
    {
        return 'feature_id';
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{features}}';
    }

    public function rules()
    {
        return array
        (
            array('feature_value', 'required', 'message' => 'Введите название характеристики.')
        );
    }

    public function relations()
    {
        return array
        (
            'feature_values' => array(self::HAS_MANY, 'FeatureValue', 'feature_id')
        );
    }


    public function attributeLabels()
    {
        return array
        (

        );
    }
}
