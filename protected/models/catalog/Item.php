<?php

/**
 * Class Item
 * @property integer           $id
 * @property string            $title
 * @property string            $type
 * @property string            $perm_link
 * @property boolean           $is_new
 * @property boolean           $is_hot
 * @property boolean           $available
 * @property boolean           $draft
 * @property ItemCityLink[]    $cities
 * @property ItemPartnerLink[] $partners
 * @property ItemSelfLink[]    $selfLinks
 * @property Item[]            $childs
 * @property ItemCityLink[]    $prices
 */
class Item extends CActiveRecord
{
	public $title;
    public $new;
    public $special;
    public $main;
	public $desc;
    public $is_season;
    public $is_new;
    public $is_special;
    public $is_main;
    public $yml;
    public $is_vkpost;
    public $preview_img;
    public $full_img;
	public $t_available;

    private $counterSeed;

    public $uploadImages = [];

    const SPECIAL_ITEM = 1;
    const NEW_ITEM = 2;
    const MAIN_ITEM = 5;

    const IN_CATALOG = 1;
    const OUT_CATALOG = 0;

    const TYPE_GROUP = 'group';
    const TYPE_ITEM = 'item';

    static public $ALLOWED_TYPES = [
        self::TYPE_GROUP,
        self::TYPE_ITEM,
    ];


    /**
     * @param string $className
     * @return Item
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{item}}';
    }

    public function behaviors()
    {
        return [
            ['class' => 'application.components.behaviors.TransliterationBehaviour'],
            ['class' => 'application.components.behaviors.UrlShortnerBehaviour', 'prefix' => 'i'],
            ['class' => 'application.components.behaviors.WysiwygTidyBehaviour'],
            [
                'class' => 'application.components.behaviors.CachePurger',
                'tags' => function (Item $changedItem) {
                    return array_reduce(City::model()->findAll(), function ($tags, City $city) {
                        $tags[] = sprintf('tree-%d-%d', 0, $city->id);
                        $tags[] = sprintf('tree-%d-%d', 1, $city->id);

                        return $tags;
                    }, []);
                },
            ],
            [
                'class' => 'application.components.behaviors.UploadableBehaviour',
                'saveToDirectory' => 'images/item',
                'fields' => [
                    'preview_img' => ['size' => 130],
                    'full_img' => ['size' => 472]
                ]
            ],
            [
                'class' => 'application.components.behaviors.SerializableFieldsBehaviour',
                'fields' => ['prices_cache']
            ],
            [
                'class' => 'application.components.behaviors.LinkSaverBehaviour',
                'binders' => [
                    'categories' => function (Item $owner, array $data) {
                        CategoryItems::addItem($data, $owner->id);
                    },
                    'features' => function (Item $owner, array $data) {
                        FeatureValueItem::model()->deleteAllByAttributes(['item_id' => $owner->id]);

                        foreach ($data as $featureValues)
                        {
                            foreach ($featureValues as $featureValue)
                            {
                                $oFeatureValueItem = new FeatureValueItem();
                                $oFeatureValueItem->item_id = $owner->id;
                                $oFeatureValueItem->feature_value_id = $featureValue;
                                $oFeatureValueItem->save(false);
                            }
                        }
                    },
                    'partners' => function (Item $owner, array $data) {
                        ItemPartnerLink::model()->deleteAllByAttributes(['item_id' => $owner->id]);

                        foreach ($data as $partnerId => $price) {
                            $link = new ItemPartnerLink();
                            $link->item_id = $owner->id;
                            $link->partner_id = $partnerId;
                            $link->partner_price = $price['price'];
                            $link->save(false);
                        }
                    },
                    'prices' => function (Item $owner, array $data) {
                        ItemCityLink::model()->deleteAllByAttributes(['item_id' => $owner->id]);

                        foreach ($data as $cityId => $cityData) {
                            $link = new ItemCityLink();
                            $link->item_id = $owner->id;
                            $link->city_id = $cityId;
                            $link->item_price = $cityData['price'];
                            $link->available = isset($cityData['available']);
                            $link->save(false);
                        }
                    },
                    'items' => function (Item $owner, array $data) {
                        $sql = "DELETE FROM {{item_item}} WHERE item_id=".$owner->id;
                        Yii::app()->db->createCommand($sql)->execute();

                        foreach ($data as $id) {
                            $sql = "INSERT INTO {{item_item}}  (`item_id`, `child_id`) VALUES (".$owner->id.", ".$id.")";
                            Yii::app()->db->createCommand($sql)->execute();
                        }
                    }
                ]
            ]
        ];
    }

    public function getOrderCount()
    {
        $orderCount = intval($this->order_count);
        return $orderCount;
    }

    public function rules()
    {
        return [
            ['title, text, desc', 'required', 'on'  => 'update, create'],
            ['rating', 'numerical', 'min' => 1, 'on' => 'rating'],
            ['vote_count', 'numerical', 'min' => 1, 'on' => 'rating'],
            ['order, available', 'numerical', 'integerOnly' => true],
            ['draft', 'numerical', 'integerOnly' => true, 'allowEmpty' => true],
            ['title, seo_title', 'length', 'max' => 255],
            ['desc, scenario', 'length', 'max' => 4096],
            ['instuction_description, need_to_know', 'length', 'max' => 4096, 'allowEmpty' => true],
            // Validator for image removed because we using js plugin for that
//            ['preview_img, full_img', 'file', 'types' => 'jpg, gif, png', 'on' => 'create'],
//            ['preview_img, full_img', 'file', 'types' => 'jpg, gif, png', 'allowEmpty' => true, 'on' => 'update'],
            ['description, keywords', 'length', 'max' => 1020]
        ];
    }

    public function relations()
    {
        return [
            'categories' => [self::MANY_MANY, 'Category', '{{category_has_items}}(item_id, category_id)'],
            'partner' => [self::BELONGS_TO, 'Partner', 'partner_id'],
            'partners' => [self::HAS_MANY, 'ItemPartnerLink', 'item_id', 'with' => 'partner'],
            'prices' => [self::HAS_MANY, 'ItemCityLink', 'item_id'],
            'images' => [self::HAS_MANY, 'ItemImage', 'item_id'],
            'comments' => [self::HAS_MANY, 'Comments', 'item_id', 'condition' => 'visibility = 1'],
            'photos' => [self::HAS_MANY, 'ItemPhoto', 'item_id'],
            'childs' => [self::MANY_MANY, 'Item', '{{item_item}}(item_id,child_id)'],
            'event' => [self::BELONGS_TO, 'Event', 'event_id'],
            'in_orders' => [self::HAS_MANY, 'OrderItems', 'item_id'],
            'in_orders_activated' => [self::HAS_MANY, 'OrderItems', 'activated_item'],
            'selfLinks' => [self::HAS_MANY, 'ItemSelfLink', 'item_id']
        ];
    }

    /**
     * @return array customized attribute labels (name=>label)
     * todo: remove from model, this is part of presentation
     */
    public function attributeLabels()
    {
        return [
            'id' => '№ п/п',
            'category_id' => 'Категория',
            'subcategory_id' => 'Подкатегория',
            'partner_id' => 'Партнер',
            'title' => 'Название',
            'text' => 'Текст',
            'desc' => 'Описание',
            'scenario' => 'Сценарий',
            'category' => 'Категория',
            'draft' => 'Только для супервпечатлений',
            'is_new' => 'Новый',
            'is_special' => 'Специальный',
            'is_main' => 'На главной',
            'yml' => 'Выгружать в yandex',
            'is_recommend' => 'Рекомендуем',
            'images' => 'Картинка',
            'price' => 'Цена',
            'off_price' => 'Скидка',
            'preview_img' => 'Превью',
            'available' => 'Наличие',
            'full_img' => 'Полное изображение',
            'description' => 'Description (используется при создании краткого описания страницы,
                            используется поисковыми системами для индексации,
                            а также при создании аннотации в выдаче по запросу)',
            'updated_at' => 'Дата обновления',
            'verified' => 'Проверено'
        ];
    }

    public function scopes()
    {
        $alias = $this->tableAlias;
        $alias = empty($alias) ? '' : ($alias . '.');
        return [
            'special' => [
                'condition' => $alias.'is_special = 1'
            ],
            'main' => [
                'condition' => $alias.'is_main = 1',
                'order' => $alias.'`order` ASC, '.$alias.'id DESC',
            ],
            'recommend' => [
                'condition' => $alias.'is_recommend = 1',
                'order' => $alias.'id DESC'
            ],
        ];
    }

    public function similar()
    {
        $c = new CDbCriteria();
        $c->condition = "id != ".$this->id . " AND `is_recommend` = 1  AND category_id = ".$this->category_id;
        $c->order = "RAND()";

        return $this->findAll($c);
    }

    // todo: move to form builder
    public function getFormData()
    {
        return [
            'category_id' => 'CategorySelectType',
            'title' => 'InputTextType',
            'new' => 'CheckboxType',
            'special' => 'CheckboxType',
            'main' => 'CheckboxType',
            'draft' => 'CheckboxType',
            'available' => 'CheckboxType',
            'desc' => 'TextareaType',
            'text' => 'EditorViewType',
            'price' => 'InputTextType',
            'scenario' => 'TextareaType',
            'images' => 'ImagesListType',
            'preview_img' => 'ImageType',
            'full_img' => 'ImageType',
            'description' => 'TextareaType',
        ];
    }

    // remove this method in future
    public function getViewData()
    {
        return [
            'title' => 'SimpleTextType',
            'category' => 'RelatedRecordTitleType',
            'desc' => 'SimpleTextType',
            'new' => 'BoolType',
            'special' => 'BoolType',
            'draft' => 'BoolType',
            'main' => 'BoolType',
            'available' => 'BoolType',
            'text' => 'SimpleTextType',
            'scenario' => 'nl2brTextType',
            'images' => 'PreviewImageType',
        ];
    }

    // todo: move to some SEO behaviour
    public function getText()
    {
        $search = 'alt=""';
        $text = str_replace($search, 'alt="' . htmlspecialchars($this->title) . '"', $this->text);

        return $text;
    }

    /**
     * Return price by optionId
     * @param string $option
     * @return float
     */
    public function getRoundedPrice($option = '', City $city = null)
    {
        if ($this->off_price) {
            $price = $this->getPrice($option, $city);
            $price = ($price - ($this->off_price / 100 * $price));
            $price = round($price, 2);
            return $price;
        }

        return $this->getPrice($option, $city);
    }

    /**
     * Returned Price for this item by option id and cityId
     * @param string $option
     * @param City|null $city
     * @return int|mixed
     */
    public function getPrice($option = '', City $city = null)
    {
        $city = !empty($city)?$city:$_GET['_city'];
        $prices = $this->prices_cache;

        if (isset($prices[$city->id])) {
            if (!empty($option)) {
                return is_array($prices[$city->id]) ? $prices[$city->id][$option] : $prices[$city->id];
            } else {
                return is_array($prices[$city->id]) ? array_shift($prices[$city->id]) : $prices[$city->id];
            }
        }

        return -1;
    }

    /**
     * Вывести все цены по городу
     * @param City $city
     * @return int
     */
    public function getAllPrice(City $city = null){
        $city = !empty($city) ? $city : $_GET['_city'];
        $prices = $this->prices_cache;

        return isset($prices[$city->id]) ? $prices[$city->id] : -1;
    }

    /**
     * @param array|City[] $cities
     * @return mixed
     */
    public function getFilteredPriceByCities($cities = [])
    {
        if (empty($cities)) {
            return $this->prices_cache;
        } else {
            $result = [];
            foreach ($cities as $city) {
                if (!empty($this->prices_cache[$city->id])) {
                    $result[$city->id] = $this->prices_cache[$city->id];
                }
            }

            return $result;
        }
    }

    public function getItemPriceSelection($cities = [])
    {
        $prices = $this->getFilteredPriceByCities($cities);
        if (empty($prices))
            return $prices;
        $result = [];
        foreach ($prices as $cityId => $price) {
            if (is_array($price)) {
                foreach ($price as $optionId => $priceVal) {
                    if ($optionId == 0) {
                        $result[$cityId][$optionId] = $priceVal.' р.';
                    } else {
                        /** @var Option $option */
                        $option = Option::model()->findByPk($optionId);
                        $result[$cityId][$optionId] = $priceVal.' р.'.' - '.$option->small_name;
                    }
                }
            } else {
                $result[$cityId] = $price.' р.';
            }
        }

        return $result;
    }


    /**
     * Получить диапазон цен для опций в шаблоне
     * @param City $city
     * @return string
     */
    public function getVariablePriceForView(City $city = null){

        $city = !empty($city) ? $city : $_GET['_city'];
        $prices = $this->prices_cache;

        $price =  isset($prices[$city->id]) ? $prices[$city->id] : -1;

        if(is_array($price)){
            $price_start = (min($price));
            $price_finish = count($price) > 1 ? ' - '. (max ($price)) : '';
            return $price_start.$price_finish. ' руб.';
        }
        else{
            if($price > 0){
                return ($price).' руб.';
            }
             else{
                return false;
            }
        }
    }

    /**
     * Получить диапазон цен для опций в шаблоне
     * @param City $city
     * @return string
     */
    public function getVariableWithoutDenominationPriceForView(City $city = null)
    {
        $city = !empty($city)?$city:$_GET['_city'];
        $prices = $this->prices_cache;

        $price =  isset($prices[$city->id]) ? $prices[$city->id] : -1;

        if(is_array($price)){
            $price_start = number_format((min($price)) * 10000);
            $price_finish = count($price) > 1 ? ' - '. number_format((max ($price)) * 10000) : '';
            return $price_start.$price_finish. ' руб.';
        }
        else{
            if($price > 0){
                return number_format($price * 10000).' руб.';
            }
            else{
                return false;
            }
        }
    }

    public static function getOption($id){
        return Option::model()->findByPk($id);
    }

    public static function getOptionInGroup($all_prices)
    {
        foreach($all_prices as $k => $v){
            $option_in_group = Option::model()->find('id=:id AND show_in_group=:show_in_group', [':id' => $k, ':show_in_group' => 1]);
            if(!empty($option_in_group)){
                return $option_in_group->name;
            }
        }
        return false;
    }

    /**
     * @param City|null $city
     * @return null|Partner
     */
    public function getPartner(City $city = null)
    {
        $city = !empty($city) ? $city : $_GET['_city'];
        foreach ($this->partners as $partnerLink) {
            $partner = $partnerLink->partner;
            if ($partner->city_id == $city->id) {
                return $partner;
            }
        }

        return null;
    }

    public function getPartnerPrice(City $city = null)
    {
        $prices = array_map(function (ItemPartnerLink $link) {
            return $link->partner_price;
        }, $this->partners);

        sort($prices);
        if (!count($prices)) {
            return -1;
        }

        if (count($prices) >= 2) {
            return sprintf('%d %d', ($prices[0]), ($prices[count($prices)-1]));
        }

        return ($prices[0]);
    }

    public function getPartnersForCity(City $city)
    {
        $partners = $this->partners;
        return array_filter($partners, function (ItemPartnerLink $link) use ($city) {
            return $link->partner->city_id == $city->id;
        });
    }

    public function getPartnerPriceByOption(City $city, $option_id)
    {
        $partners = $this->partners;
        if (count($partners) === 1) {
            return $partners;
        } else {
            return array_filter($partners, function (ItemPartnerLink $link) use ($city, $option_id) {
                return $link->partner->city_id == $city->id && $option_id == $link->option_id;
            });
        }
    }

    /**
     * Returned available information by Item. If price <=, then unavailable
     * @param City|null $city
     * @return bool
     */
    public function isAvailable(City $city = null)
    {
        $city = is_null($city) ? Yii::app()->request->getQuery('_city'): $city;

        return $this->getPrice('',  $city) >= 0;
    }

    /**
     * @param City $city
     * @return bool
     */
    public function isDigitalCertificateAvailable(City $city = null)
    {
        return in_array($this->type, self::$ALLOWED_TYPES);
    }

    /**
     * @return array
     */
    public function getItemScenario()
    {
        $scenario = [];
        foreach (explode("\n", $this->scenario) as $line) {
            $line = trim($line);
            if (!empty($line)) {
                $scenario[] = $line;
            }
        }

        return $scenario;
    }

    /**
     * @return array
     */
    public function whatShouldYouKnow()
    {
        if (empty($this->need_to_know)) {
            return [];
        }

        return array_reduce(explode("\n", $this->need_to_know), function ($result, $line) {
            $line = trim($line);
            if (empty($line) || strpos($line, ':') === false) {
                return $result;
            }

            if (strpos('Срок действия', $line) !== false) {
                return $result;
            }

            if (strpos('Подарок необходимо активировать', $line) !== false) {
                return $result;
            }

            $data = explode(':', $line);
            if (trim($data[0]) === 'Срок действия подарка') {
                return $result;
            }
            $result[] = [
                'key' => trim($data[0]),
                'value' => trim($data[1])
            ];

            return $result;
        }, []);
    }

    /**
     * @param Category $category
     * @param City|null $city
     * @param int $perPage
     * @return CActiveDataProvider
     */
    public static function searchByCategory($category, $city = null, $perPage = 10)
    {
        $model = static::model();
        $criteria = $model->resetScope()->getDbCriteria(true);
        $criteria->join = 'INNER JOIN el_category_has_items ci ON ci.item_id=t.id AND ci.category_id=:categoryId';
        $criteria->order = 't.available DESC, t.is_new DESC';
        $criteria->condition .= ' t.available=1';

        $criteria->params = [
            ':categoryId' => $category->id,
        ];

        return self::getProvider($criteria, $city, $perPage);
    }

    public static function getItemsForMainPage(City $city = null, $perPage=10)
    {

        $criteria = new CDbCriteria();
        $criteria->condition = 't.is_main=1';
        $criteria->condition .= ' AND t.available=1';


        return self::getProvider($criteria, $city, $perPage);
    }

    /**
     * Для вывода в яндекс yml
     * 
     * @param $city
     * @return CActiveDataProvider
     */
    public static function getItemsYml($city)
    {
        $criteria = [
            'condition' => 'yml=1',
        ];

        return self::getProvider(new CDbCriteria($criteria), $city, 1000000);
    }

    /**
     * @param City $city
     * @param int $perPage
     * @param $criteria
     * @return CActiveDataProvider
     */
    public static function getItemsForKit(City $city = null, $perPage = 10, $criteria)
    {
        return self::getProvider(new CDbCriteria($criteria), $city, $perPage);
    }

    /**
     * Отображать на странице landing
     * @param City $city
     * @param int $perPage
     * @return CActiveDataProvider
     */
    public static function getItemsForLanding(City $city = null, $perPage=6){
        $criteria = [
            'condition' => 'is_landing=1',
        ];

        return self::getProvider(new CDbCriteria($criteria), $city, $perPage);
    }

    public static function getAjaxList(array $params, City $city = null, $perPage, $sortBy = 't.`order` ASC')
    {
        $criteria = new CDbCriteria();

        if (!empty($params['q']) && mb_strlen($q = trim($params['q']), 'UTF-8') > 2) {
            $criteriaSearch = new CDbCriteria();
            $criteriaSearch->addSearchCondition('title', $q, true, 'AND');
            $criteriaSearch->addSearchCondition('text', $q, true, 'OR');
            $criteriaSearch->addSearchCondition('`desc`', $q, true, 'OR');
            $criteria->mergeWith($criteriaSearch, 'AND');
        }

        $betweens = [];
        if (!empty($params['prices'])) {
            if(!$params['prices'][0])
                $params['prices'][0] = 0;
            if(!$params['prices'][1])
                $params['prices'][1] = 1000000;
            $betweens[] = "(ic.item_price >= {$params['prices'][0]} AND  ic.item_price <= {$params['prices'][1]})";
        }

        if (!empty ($betweens)) {
            $criteria->addCondition('(' . (implode(' OR ', $betweens)) . ')');
        }



        //$criteria->addCondition('option_id = 0');

        if (!empty($params['features']) && $params['features'][0] != 0) {
            $ids = array_unique(array_map(function ($feature) {
                return intval($feature);
            }, $params['features']));
            $criteria->join = 'INNER JOIN {{features_values_items}} f ON f.item_id = t.id AND f.feature_value_id IN (' . implode(', ', $ids) . ')';
        }


        if (!empty($params['options']) && $params['options'][0] != 0) {
            $ids = array_unique(array_map(function ($option) {
                return intval($option);
            }, $params['options']));
            $criteria->join = 'INNER JOIN {{features_values_items}} f ON f.item_id = t.id AND f.feature_value_id IN (' . implode(', ', $ids) . ')';
        }

        if(!empty($params['category_id'])) {
            $criteria->join = 'INNER JOIN el_category_has_items ci ON ci.item_id=t.id AND ci.category_id=:categoryId';
            $criteria->params = is_array($criteria->params) ? $criteria->params : [];
            $criteria->params['categoryId'] =  $params['category_id'];
        }

        // for group by
        $criteria->select = 'ic.item_id, ic.city_id, ic.available, ic.option_id, ic.item_price,
        t.id, t.title, t.desc, t.preview_img, t.full_img, t.perm_link, t.prices_cache, t.is_new, t.is_season,
        t.is_main, t.is_landing, t.is_kit, t.draft, t.is_special, t.is_weekend, t.is_recommend, t.is_in_group, t.is_hot,
        t.is_catalog, t.yml, t.order_count, t.scenario, t.off_price, t.opts, t.available as t_available, t.rating, t.vote_count';

        if (!$criteria->join) $criteria->join = '';
        $criteria->join .= ' INNER JOIN {{item_with_city}} ic ON ic.available = 1 AND ic.city_id=:city AND t.id=ic.item_id';
        $criteria->params = is_array($criteria->params) ? $criteria->params : [];
        $criteria->params['city'] = $city->id;
        // todo: move to controller
        //$criteria->order = 'ic.available DESC, t.is_hot DESC';
        //$criteria->condition .= 't.is_main=1';

        $criteria->condition .= ' AND t.available=1';

        if(!empty($params['is_new'])) {
            $criteria->condition .= ' AND t.is_new=1';
        }

        if(!empty($params['off_price'])) {
            $criteria->condition .= ' AND t.off_price > 0';
        }

        $criteria->group = 'ic.item_id';

        $criteria->order = $sortBy;




        $countCriteria = clone $criteria;
        $countCriteria->order = '';
//        echo "<pre>";


        $dataProvider = new CActiveDataProvider('Item', [
            'criteria' => $criteria,
            'countCriteria' => $countCriteria,
            'pagination'=>[
                'pageSize'=> $perPage,
                'pageVar' => 'page'

            ],
        ]);


        if (0 === intval($perPage)) {
            $dataProvider->setPagination(false);
        }

        return $dataProvider;
    }

    public static function searchByCriteria(array $params, City $city = null, $perPage = 12, $sortBy = 't.`order` ASC')
    {
        $criteria = new CDbCriteria([
            'condition' => 'draft = 0 AND t.`available` = 1'
        ]);

        if (!empty($params['q'])) {
            $criteriaSearch = new CDbCriteria();
            $params['q'] = htmlentities($params['q'], ENT_QUOTES);
            $params['q'] = str_replace(
                ['&#039;', '&laquo;', '&raquo;', '&quot;', ':', "&ldquo;", "&rdquo;", "&lsquo;", "&rsquo;", "&bdquo;", "&rdquo;"],
                '',
                $params['q']
            );
            $titleCriteria = '';
            $descCriteria = '';
            foreach (explode(' ', $params['q']) as $key => $queryParam) {
                $titleCriteria = !empty($titleCriteria)
                    ? $titleCriteria .= " AND t.`title` LIKE '%{$queryParam}%'"
                    : $titleCriteria = "(t.`title` LIKE '%{$queryParam}%'";
                $descCriteria = !empty($descCriteria)
                    ? $descCriteria .= " AND t.`text` LIKE '%{$queryParam}%'"
                    : $descCriteria = "(t.`text` LIKE '%{$queryParam}%'";
            }
            $titleCriteria .= ')';
            $descCriteria .= ')';
            $criteriaSearch->condition = $titleCriteria . " OR " . $descCriteria . " OR t.`title` LIKE '%{$params['q']}%' OR t.`text` LIKE '%{$params['q']}%' OR t.`desc` LIKE '%{$params['q']}%'";
            $criteria->mergeWith($criteriaSearch, 'AND');
        }

        $betweens = [];
        if (!empty($params['prices'])) {
            if(!$params['prices'][0])
                $params['prices'][0] = 0;
            if(!$params['prices'][1])
                $params['prices'][1] = 1000000;
            $betweens[] = "(ic.item_price >= {$params['prices'][0]} AND  ic.item_price <= {$params['prices'][1]})";
        }

        if (!empty ($betweens)) {
            $criteria->addCondition('(' . (implode(' OR ', $betweens)) . ')');
        }

        //$criteria->addCondition('option_id = 0');

        if (!empty($params['features']) && $params['features'][0] != 0) {
            $ids = array_unique(array_map(function ($feature) {
                return intval($feature);
            }, $params['features']));
            $criteria->join = 'INNER JOIN {{features_values_items}} f ON f.item_id = t.id AND f.feature_value_id IN (' . implode(', ', $ids) . ')';
        }


        if (!empty($params['options']) && $params['options'][0] != 0) {
            $ids = array_unique(array_map(function ($option) {
                return intval($option);
            }, $params['options']));
            $criteria->join = 'INNER JOIN {{features_values_items}} f ON f.item_id = t.id AND f.feature_value_id IN (' . implode(', ', $ids) . ')';
        }

        $criteria->condition .= ' AND t.available=1';


        if(!empty($params['is_new'])) {
            $criteria->condition .= ' AND t.is_new=1';
        }

        if(!empty($params['off_price'])) {
            $criteria->condition .= ' AND t.off_price > 0';
        }

        $criteria->order = $sortBy;

        return self::getProvider($criteria, $city, $perPage);
    }

    /**
     * @param CDbCriteria $criteria
     * @param City $city
     * @param int $perPage
     * @return CActiveDataProvider
     */
    private static function getProvider($criteria, City $city = null, $perPage = 12)
     {

        // for group by
        $criteria->select = 'ic.item_id, ic.city_id, ic.available, ic.option_id, ic.item_price,
        t.id, t.title, t.desc, t.preview_img, t.full_img, t.perm_link, t.prices_cache, t.is_new, t.is_season,
        t.is_main, t.is_landing, t.is_kit, t.draft, t.is_special, t.is_weekend, t.is_recommend, t.is_in_group, t.is_hot,
        t.is_catalog, t.yml, t.order_count, t.scenario, t.off_price, t.opts, t.available as t_available, t.rating, t.vote_count';

        if (!$criteria->join) $criteria->join = '';
        $criteria->join .= ' INNER JOIN {{item_with_city}} ic ON ic.available = 1 AND ic.city_id=:city AND t.id=ic.item_id';
        $criteria->params = is_array($criteria->params) ? $criteria->params : [];
        $criteria->params['city'] = $city->id;
        // todo: move to controller
        //$criteria->order = 'ic.available DESC, t.is_hot DESC';

        $criteria->group = 'ic.item_id';

         $criteria->order = 'ic.available DESC';
         if (($sort = Yii::app()->request->getQuery('sort'))) {
             $criteria->order .= ', ic.item_price ' . (strtolower($sort) == 'desc' ? 'DESC' : 'ASC');
         } else {
             //$criteria->order .= ', ic.item_price ASC';
             $criteria->order .= ', t.`order`';
         }



        $countCriteria = clone $criteria;
        $countCriteria->order = '';
        $dataProvider = new CActiveDataProvider('Item', [
            'criteria' => $criteria,
            'countCriteria' => $countCriteria,
            'pagination'=>[
                'pageSize'=> $perPage,
                'pageVar' => 'page'
            ],
        ]);

        if (0 === intval($perPage)) {
            $dataProvider->setPagination(false);
        }

        return $dataProvider;
    }

    /**
     * todo: move this logic to admin cp and restrict save without it
     * @return string
     */
    public function generatePermLink()
    {
        $notSoImportantCategories = [
            '8-marta-podarki', 'leto', 'podarki-dlja-muzhchin', ''
        ];
        $notSoImportantCategories = array_map(function ($category) {
            return preg_quote($category);
        }, $notSoImportantCategories);

        // get categories of item
        $categories = $this->categories;
        $links = [];
        foreach ($categories as $category) {
            $link = '';
            if ($category->parent_id) {
                $link .= $category->parent->slug . '/';
            }

            $links[] =  $link . $category->slug . '/' . $this->normalizeUri($this->title);
        }
        // sort by nesting level

        if (1 == count($links)) {
            return $this->uniqPermLink($links[0]);
        }
        // sort by nesting level
        usort($links, function ($a, $b) {
            return substr_count($b, '/') - substr_count($a, '/');
        });

        $maxNestingLevel = substr_count($links[0], '/');
        // filter all links with nesting level less than maxNestingLevel
        $links = array_filter($links, function ($option) use ($maxNestingLevel) {
            return $maxNestingLevel == substr_count($option, '/');
        });

        if (1 == count($links)) {
            return $this->uniqPermLink($links[0]);
        }

        // try to filter not so important categories
        $regExp = '/^('.implode('|', $notSoImportantCategories).')\//U';
        $filtered = array_values(array_filter($links, function ($option) use ($regExp) {
            return !preg_match($regExp, $option);
        }));


        if (0 == count($filtered)) {
            $filtered = $links;
        }

        if (1 == count($filtered)) {
            return $this->uniqPermLink($filtered[0]);
        }

        usort($filtered, function ($a, $b) {
            return strlen($a) - strlen($b);
        });

        return $this->uniqPermLink($filtered[0]);
    }

    /**
     * todo: todo: move this logic to admin cp and restrict save without it
     */
    private function uniqPermLink($link) {
        $i = 2;
        while(Item::model()->findByAttributes(['perm_link' => $link])) {
            $link = $link . $i++;
        }

        return $link;
    }

    protected function deleteImages()
    {
        foreach ($this->images as $each) {
            $each->delete();
        }
    }

    /**
     * @param array $ids
     * @return Item[]
     */
    public static function getItemsById($ids)
    {
        $criteria = new CDbCriteria();
        $criteria->addInCondition('t.id', $ids);

        return Item::model()->findAll($criteria);
    }

    /**
     * Получить итемы из тематической доставки
     * @param City $city
     * @return array()
     */
    public static function getItemsFromThematicCategory(City $city)
    {
        //if(!$items = Yii::app()->cache->get('items_thematic')) {
            $category = Category::model()->findByPk(66);
            $items = Item::searchByCategory($category, $city);
            $items = $items->getData();
            //Yii::app()->cache->set('items_thematic', $items, 43200);
        //}

        return $items;
    }

    /**
     * Относится ли впечатление к тематической доставке?
     * @param City $city
     * @param int $id
     * @return bool
     */
    public static function isThematicItem(City $city, $id)
    {
        $items = self::getItemsFromThematicCategory($city);

        foreach ($items as $item){
            if($item->id == $id){
                return true;
            }
        }

        return false;
    }

    /**
     * @param array $ids
     * @return array
     */
    public function getItemsTitleByIds(array $ids)
    {
        $criteria = (new CDbCriteria())->addInCondition('t.id', $ids);
        $items = Item::model()->findAll($criteria);
        $titles = [];
        foreach($items as $item) {
            $titles[] = $item->title;
        }

        return $titles;
    }

    protected function beforeSave()
    {
        if (!parent::beforeSave()) {
            return false;
        }
        $this->updated_at = date('Y-m-d H:i:s');
        return true;
    }

    /**
     * @param Item $cItem
     * @return ItemSelfLink
     */
    public function getSelfLinkByChildren(Item $cItem)
    {
        $childId = $cItem->id;
        $childs = array_filter(
            $this->selfLinks,
            function ($val) use ($childId){
                /** @var ItemSelfLink $val */
                return $val->child_id == $childId;
            }
        );

        return array_shift($childs);
    }

    public static function getAll()
    {
        $items = [];
        foreach (self::model()->findAll(['order' => 'title ASC']) as $item) {
            //if($item->partner_id!=0) {
                $items[$item->getPartner()->id][] = array(
                    'id' => $item->id,
                    'title' => $item->title
                );
            //}
        }

        return $items;
    }
}
