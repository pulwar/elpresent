<?php

/**
 * Class Partner
 * @property int $id
 * @property string $name
 * @property int $user_id
 * @property string $email
 */
class Partner extends CActiveRecord
{
    static public function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    public function tableName()
    {
        return '{{partner}}';
    }

    public function rules()
    {
        return array
        (
            array('name, phone, adress, work_time, contact_person, agreement_terms ', 'required'),
            array('place,lat,lon', 'safe'),
            array('city_id', 'numerical', 'integerOnly' => true),
            array('email', 'email', 'message' => 'Введённый E-mail не является правильным e-mail адресом'),
        );
    }
    
    public function relations()
    {
        return array(            
            'city' => array(self::BELONGS_TO, 'City', 'city_id')           
        );
    }

    public function attributeLabels()
    {
        return array
        (
            'id' => _('#'),
            'name' => _('Компания'),
            'city_id' => _('Город'),
            'adress' => _('Адрес'),
            'work_time' => _('Время работы'),
            'contact_person' => _('Контактное лицо'),
            'phone' => _('Телефон'),
            'agreement_terms' => _('Условия сотрудничества'),
            'commission' => _('Процент комиссии'),
            'reg_date' => _('Дата регистрации'),
            'email' => 'Email',
        );
    }

    public static function getPartnerList()
    {
        return self::model()->findAll();
    }

    /**
     * Вывод партнеров по городу
     * @param $city_id
     * @return array|CActiveRecord|CActiveRecord[]|mixed|null
     */
    public static function getPartnerListByCityId($city_id){
        return Partner::model()->findAll([
            'condition'=>'city_id=:city_id',
            'params'=>array(':city_id'=>$city_id),
            'order' => 'name ASC'
        ]);
    }
	
	public static function getPartnersList()
    {
        $parnters = [];
        foreach (self::model()->findAll(['order' => 'name ASC']) as $partner) {
            $parnters[$partner->id] = $partner->name;
        }

        return $parnters;
    }

}