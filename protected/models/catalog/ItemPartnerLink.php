<?php

/**
 * Class ItemPartnerLink
 *
 * This model used as link model
 * between partners and items
 *
 * @property int $item_id
 * @property int $partner_id
 * @property int $partner_price
 * @property int $city_id
 * @property int $option_id
 * @property Partner $partner
 */
class ItemPartnerLink extends CActiveRecord
{

    /**
     * @param string $className
     * @return ItemPartnerLink
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function relations()
    {
        return array(
            'partner' => array(self::BELONGS_TO, 'Partner', 'partner_id', 'joinType' => 'INNER JOIN')
        );
    }


    public function tableName()
    {
        return '{{item_with_partner}}';
    }

    public static function updateItemPartnerLinkPrices($item_id, $partner_id, $option_id, $partner_price){
        $itemPartnerLink = ItemPartnerLink::model()->find(array(
            'condition'=>'item_id=:item_id AND partner_id=:partner_id AND option_id=:option_id',
            'params'=>array(':item_id' => $item_id, ':partner_id' => $partner_id, ':option_id' => $option_id),
        ));

        $itemPartnerLink->partner_price = $partner_price;
        //$ItemCityLink->available = 1;
        $itemPartnerLink->save();

        return true;
    }

    public static function saveItemPartnerLinkPrices($item_id, $partner_id, $option_id, $partner_price){
        $itemPartnerLink = new ItemPartnerLink;
        $itemPartnerLink->item_id = $item_id;
        $itemPartnerLink->partner_id = $partner_id;
        $itemPartnerLink->option_id = $option_id;
        $itemPartnerLink->partner_price = $partner_price;
        $itemPartnerLink->save();
        return true;
    }


}