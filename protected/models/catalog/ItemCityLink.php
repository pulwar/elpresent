<?php

/**
 * Class ItemPartnerLink
 *
 * This model used as link model
 * between partners and items
 *
 * @property int item_id
 * @property int item_price
 * @property int city_id
 */
class ItemCityLink extends CActiveRecord
{
    public $max_price;
    public $min_price;

    /**
     * @param string $className
     * @return ItemPartnerLink
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function relations()
    {
        return array(
            'city' => array(self::BELONGS_TO, 'City', 'city_id'),
            'item' => array(self::HAS_ONE, 'item', 'item_id'),
            'option' => array(self::BELONGS_TO, 'Option', 'option_id'),
        );
    }


    public function tableName()
    {
        return '{{item_with_city}}';
    }


    public static function updateItemCityLinkPrices($item_id, $citiy_id, $option_id, $price){
        $itemCityLink = ItemCityLink::model()->find(array(
            'condition'=>'item_id=:item_id AND city_id=:city_id AND option_id=:option_id',
            'params'=>array(':item_id' => $item_id, ':city_id' => $citiy_id, ':option_id' => $option_id),
        ));

        $itemCityLink->item_price = $price;
        $itemCityLink->available = 1;
        $itemCityLink->save();
        return true;
    }

    public static function saveItemCityLinkPrices($item_id, $citiy_id, $option_id, $price){
        $itemCityLink = new ItemCityLink;
        $itemCityLink->item_id = $item_id;
        $itemCityLink->city_id = $citiy_id;
        $itemCityLink->option_id = $option_id;
        $itemCityLink->item_price = $price;
        $itemCityLink->available = 1;
        $itemCityLink->save();
        return true;
    }

    /**
     * Максимальная цена подарка
     * @return int
     */
    public static function getMaxPrice(){
        if(! $max_price = Yii::app()->cache->get('max_price')){
            $criteria = new CDbCriteria();
            $criteria->select='MAX(`item_price`) as `max_price`';
            $criteria->condition = 'available = 1';
            $row = ItemCityLink::model()->find($criteria);
            $max_price = $row->max_price;
            Yii::app()->cache->set('max_price' ,$max_price, 24*3600);
        }
        return (int)$max_price;
    }

    public static function getMinPrice(){
        if(! $min_price = Yii::app()->cache->get('min_price')){
            $criteria = new CDbCriteria();
            $criteria->select='MIN(`item_price`) as `min_price`';
            $criteria->condition = 'available = 1';
            $row = ItemCityLink::model()->find($criteria);
            $min_price = $row->min_price;
            Yii::app()->cache->set('min_price' ,$min_price, 24*3600);
        }
        return (int)$min_price;
    }


}