<?php

/**
 * Class Category
 *
 * @property int $id
 * @property int $parent_id
 * @property string $title
 * @property string $icon
 * @property string $slug
 * @property int $position
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keywords
 * @property string $seo_text
 * @property int $visible
 *
 * // related
 * @property Item[] $items
 * @property Category[] $children
 * @property Category $parent
 *
 * // scopes
 * @method $this onlyRoots() onlyRoots()
 *
 * // behaviours
 * @method string transliterate(string $str) transliterate()
 * @method string normalizeUri(string $str) normalizeUri()
 */
class Category extends CActiveRecord
{

    /**
     * @param string $className
     * @return Category
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        return array(
            array(
                'class' => 'application.components.behaviors.TransliterationBehaviour'
            ),
            array(
                'class' => 'application.components.behaviors.CachePurger',
                'tags' => function (Category $category) {

                    return array_reduce(City::model()->findAll(), function ($tags, City $city) {
                        $tags[] = sprintf('tree-%d-%d', 0, $city->id);
                        $tags[] = sprintf('tree-%d-%d', 1, $city->id);

                        return $tags;
                    }, []);
                }
            )
        );
    }

    public function tableName()
    {
        return '{{category}}';
    }

    public function rules()
    {
        return array(
            array('title', 'required'),
            array('title', 'length', 'max' => 255),
            array('icon', 'length', 'max' => 100),
            array('visible', 'safe'),
        );
    }

    public function scopes()
    {
        return array(
            'onlyRoots' => array(
                'condition' => 't.parent_id is null',
            ),
            'visible' => array(
                'condition' => 't.visible=1'
            ),
            'onlyChild' => array(
                'condition' => 't.parent_id is not null',
            ),
        );
    }

    public function defaultScope()
    {
        $alias = $this->getTableAlias(false, false);
        $alias = empty($alias) ? '' : $alias . '.';
        return array(
            'order' => $alias.'parent_id, '.$alias.'position',
        );
    }

    public function relations()
    {
        return array(
            'items' => array(self::HAS_MANY, 'Item', 'category_id'),
            'parent' => array(self::BELONGS_TO, 'Category', 'parent_id'),
            'children' => array(self::HAS_MANY, 'Category', 'parent_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'Id',
            'title' => 'Title',
            'icon' => 'Icon',
            'parent_id' => 'Категория',
            'meta_description' => 'Description (используется при создании краткого описания страницы,
                            используется поисковыми системами для индексации,
                            а также при создании аннотации в выдаче по запросу)',
            'slug' => 'Название латиницей для URL',
            'meta_keywords' => 'Ключевые слова (через запятую).
                            При формировании данного тега необходимо использовать только те слова,
                            которые содержатся в самом документе. Использование тех слов, которых нет на странице,
                            не рекомендуется. Рекомендованное количество слов в данном теге — не более десяти. ',
            'meta_title' => 'Title',
            'seo_text' => 'SEO text',
            'visible' => 'Показывать в меню',
        );
    }

    public function getFormData()
    {
        // todo: this is an hotfix
        if ('subcategory' == Yii::app()->controller->id) {
            return array(
                'title' => 'InputTextType',
                'parent_id' => 'CategorySelectType',
                'icon' => 'IconType',
                'meta_keywords' => 'TextareaType',
                'meta_description' => 'TextareaType',
                'slug' => 'InputTextType',
                'meta_title' => 'InputTextType',
                'seo_text' => 'TextareaType',
                'visible' => 'CheckboxType',

            );
        }

        return array(
            'title' => 'InputTextType',
            'icon' => 'IconType',
            'meta_keywords' => 'TextareaType',
            'meta_description' => 'TextareaType',
            'slug' => 'InputTextType',
            'meta_title' => 'InputTextType',
            'seo_text' => 'TextareaType',
            'visible' => 'CheckboxType',
        );
    }

    public function getViewData()
    {
        return array(
            'title' => 'SimpleTextType',
            'icon' => 'ViewIcon'
        );
    }

    public function updateOrders($orders)
    {

        foreach ($orders as $orderId => $order)
        {
            $categoryItem = Category::model()->findByPk($orderId);
            $categoryItem->position = $order[0];
            $categoryItem->save();
        }
    }

    /**
     * @param string $slug
     * @param string|null $parent
     * @return Category
     */
    public static function findBySlug($slug, $parent=null)
    {
        $criteria = new CDbCriteria();
        $criteria->addCondition('t.slug=:slug');
        $criteria->params[':slug'] = $slug;
//        if ($parent) {
//            $criteria->join = 'INNER JOIN ' . $this->getTableSchema()->name . ' p ON t.parent_id=p.id AND p.slug=:parent';
//            $criteria->params['parent'] = $parent;
//        }

        return self::model()->find($criteria);
    }

    /**
     * @param bool $useCache
     * @param bool $onlyVisible
     * @return Category[]
     */
    public static function getTree($useCache = true, $onlyVisible = false)
    {
        $city = Yii::app()->request->getQuery('_city');


        $cache = Yii::app()->cache;

        //$cache->flush();

        $cacheKey = sprintf('tree-%d-%d', $onlyVisible ? 1: 0, $city->id);
        if ($useCache && $tree = $cache->get($cacheKey)) {
            return $tree;
        }

        if ($onlyVisible) {
            $rawTree = Category::model()->onlyRoots()->visible()->with('children')->findAll();
        } else {
            $rawTree = Category::model()->onlyRoots()->with('children')->findAll();
        }

//        print_r($rawTree);
//        die;

        $tree = [];
        foreach($rawTree as $node) {
            $tree[] = self::nodeToArray($node, $onlyVisible);
        }

        $cache->set($cacheKey, $tree);

        return $tree;
    }

    /**
     * @param Category $node
     * @return array
     */
    private static function nodeToArray($node, $onlyVisible)
    {
        $result = array(
            'id' => $node->id,
            'title' => $node->title,
            'slug' => $node->slug,
            'icon' => $node->icon,
            'children' => array(),
            'hasParent' => $node->parent_id !== null
        );

        if (!$result['hasParent']) {
            $result['itemsCount'] = Category::getItemsCount($node);
        }

        if (empty($node->children)) {
            return $result;
        }

        foreach($node->children as $children) {
            if ($children->visible) {
                $result['children'][] = self::nodeToArray($children, $onlyVisible);
            }
        }

        return $result;
    }


    /**
     * @param Category $category
     * @return int
     */
    public static function getItemsCount(Category $category)
    {

        static $countMap;
        static $city;

        if (empty($city)) {
            $city = Yii::app()->request->getQuery('_city');
        }


        $db = Yii::app()->db;
        //. ' INNER JOIN '.$prefix.'item_with_city cp ON cp.item_id=i.id AND cp.city_id=:city_id '
        if (empty($countMap)) {
            $prefix = $db->tablePrefix;
            $rawCountMap = $db->createCommand(
                'SELECT ci.category_id, i.id '
                . 'FROM '.$prefix.'category_has_items ci '
                . 'INNER JOIN '.$prefix.'category c ON c.id=ci.category_id AND c.parent_id is null '
                . 'INNER JOIN '.$prefix.'item i ON i.id=ci.item_id AND i.available=1 '
                . 'INNER JOIN '.$prefix.'item_with_city cp ON cp.item_id=i.id AND cp.city_id=:city_id AND cp.available = 1 '
                //. 'GROUP BY i.id '
                . 'order by ci.category_id, i.id'
            )->queryAll(true, [
                ':city_id' => $city->id
            ]);


            $countMap = [];
            $temp = [];
            foreach($rawCountMap as $row) {
                //var_export($row);
                if(isset($countMap[$row['category_id']])) {
                    if(!in_array($row['id'], $temp[$row['category_id']])){
                        $countMap[$row['category_id']] = $countMap[$row['category_id']] + 1;
                        $temp[$row['category_id']][$row['id']] = $row['id'];
                    }

                } else {
                    $countMap[$row['category_id']] = 1;
                    $temp[$row['category_id']][$row['id']] = $row['id'];
                }

            }


        }

        return isset($countMap[$category->id]) ? $countMap[$category->id]:0;
    }

    public function beforeSave()
    {
        if ($this->slug == '') {
            $this->slug = $this->normalizeUri($this->title);
        }

        return parent::beforeSave();
    }

    public function afterDelete()
    {
        $db = Yii::app()->db;
        $prefix = $db->tablePrefix;
        $db
            ->createCommand('DELETE FROM '.$prefix.'category_has_item WHERE category_id=:id')
            ->execute(array(
                ':id' => $this->id
            ))
        ;
    }
}
