<?php

class CategoryItems extends CActiveRecord
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{category_has_items}}';
    }

    // Fucking shame... How I miss the Doctrine...
    public static function addItem($categoryIds, $itemId)
    {
        if (!is_array($categoryIds)) {
            $categoryIds = array($categoryIds);
        }

        CategoryItems::model()->deleteAllByAttributes(array('item_id' => $itemId));

        $sub = new CategoryItems();
        foreach ($categoryIds as $id) {
            $sub->category_id = $id;
            $sub->item_id = $itemId;
            $sub->save();
            $sub->isNewRecord = true;
            $sub->primaryKey = NULL;
        }
    }

}