<? class ItemImage extends CActiveRecord
{
    private function getExt($filename)
    {
        return pathinfo($filename, PATHINFO_EXT);
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    public function tableName()
    {
        return '{{item_image}}';
    }


    public function rules()
    {

        return array(
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
        );
    }

    public function delete()
    {
        @unlink($this->filepath);
        @unlink(pathinfo($this->filepath, PATHINFO_FILENAME));
        return parent::delete();
    }

    public function toWrapper()
    {
        return ImageWrapper::wrap($this->filepath);
    }
}
