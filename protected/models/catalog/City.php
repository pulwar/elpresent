<?php

/**
 * Class City
 *
 * @property integer $id
 * @property string $name used by system
 * @property string $title show to user
 * @property boolean $isDefault
 * @property boolean $enabled
 */
class City extends CActiveRecord
{
    /**
     * @param string $class
     * @return City
     */
    static public function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    public function tableName()
    {
        return '{{city}}';
    }

    public function rules()
    {
        return array (
            array('name, title', 'required'),
            array('enabled', 'default', 'value'=>'0', 'setOnEmpty' => true),
            array('enabled', 'safe'),
        );
    }

    public function attributeLabels()
    {
        return array (
            'id' => _('№'),
            'name' => _('Системное название'),
            'title' => _('Название'),
        );
    }

    /**
     * @param bool $disabled
     * @return City[]|array
     */
    static public function getCities($disabled = false)
    {
        if (!$disabled) {
            $criteria = new CDbCriteria();
            $criteria->condition = 'enabled = 1';
            $criteria->order = 'isDefault DESC, name ASC';
            $cityList = static::model()->findAll($criteria);
        } else {
            $cityList = static::model()->findAll();
        }

        return $cityList;
    }

    /**
     * @param City[] $cityList
     * @return array
     */
    static public function cityListToArray($cityList)
    {
        $result = [];
        foreach ($cityList as $city) {
            /** @var self $city */
            $result[] = ['id' => $city->id, 'title' => $city->title, 'default' => $city->isDefault];
        }

        return $result;
    }

    public static function getCityList()
    {
        $criteria = new CDbCriteria();
        $criteria->condition = 'enabled = 1';
        $criteria->order = 'isDefault DESC, name ASC';

        return self::model()->findAll($criteria);
    }

    // todo: move to cache
    public static function getAvailable()
    {
        $cities = static::model()->findAll();
        $result = [];
        foreach ($cities as $city) {
            $result[$city->name] = $city;
        }
        
        return $result;
    }

    public static function getCity($id = 0)
    {
        $city = null;
        if ($id) {
            $city = static::model()->findByPk($id);
        }

        if (!$city) {
            $city = static::model()->findByAttributes(['isDefault'=>1]);
        }

        return $city;
    }

    public function __toString()
    {
        return $this->name;
    }

}