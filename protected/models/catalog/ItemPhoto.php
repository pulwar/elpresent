<?php

class ItemPhoto extends CActiveRecord
{
    public $verifyCode;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{item_photo}}';
    }

    public function rules()
    {
        return [];
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return [
            'item' => [self::BELONGS_TO, 'Item', 'item_id'],
            'user' => [self::BELONGS_TO, 'User', 'user_id']
        ];
    }


    public function delete()
    {
        $path = [
            'images/item/photo/original/',
            'images/item/photo/thumb/',
            'images/item/photo/'
        ];

        foreach ($path as $link) {
            if (file_exists($link . $this->filename)) {
                @unlink($link . $this->filename);
            }
        }
        return parent::delete();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return [
            'filename' => 'Название файла',
            'comment' => 'Комментарий',
            'moderated' => 'Выбран',
            'order' => 'Порядок',
            'user_id' => 'Пользователь',
            'item_id' => 'Товар'
        ];
    }

    public static function getPhotosForItem(Item $item)
    {
        return static::model()->findAll([
            'condition' => 'item_id=:item_id AND moderated=1',
            'limit' => 3,
            'order' => '`order` ASC',
            'params' => [
                'item_id' => $item->id
            ]
        ]);
    }

}
