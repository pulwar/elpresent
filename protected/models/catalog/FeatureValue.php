<?php

/**
 * Class FeatureValue
 *
 * @property integer $feature_value_id
 * @property string $feature_value_value
 * @property integer $feature_id
 */
class FeatureValue extends CActiveRecord
{
    public function primaryKey()
    {
        return 'feature_value_id';
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    public function tableName()
    {
        return '{{feature_values}}';
    }


    public function rules()
    {
        return array
        (
            array('feature_id', 'required', 'message' => 'Выберите характеристику'),
            array('feature_value_value', 'required', 'message' => 'Введите значение характеристики')
        );
    }


    public function relations()
    {
        return array
        (
        );
    }


    public function attributeLabels()
    {
        return array
        (
        );
    }
}
