<?php

/**
 * Class Option
 * @property int $id
 * @property string $name
 * @property string $small_name
 * @property string $desc
 * @property int $show_in_group 0 or 1 values
 */
class Option extends CActiveRecord
{

    public function tableName()
    {
        return '{{option}}';
    }

    public function relations()
    {
        return [
            'in_order_items' => [self::HAS_MANY, 'OrderItems', 'option_id'],
        ];
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    public function rules()
    {
        return [
            ['name', 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'id',
            'name' => 'Название',
            'small_name' => 'Короткое Название',
            'desc' => 'Описание',
        ];
    }

    public static function getAll()
    {
        $options = [];
        foreach (self::model()->findAll(['order' => 'name ASC']) as $option) {
            $options[$option->id] = $option->name;
        }

        return $options;
    }
}