<?php

/**
 * @deprecated This Class unused in this App.
 * Class Kit
 */
class Kit extends CActiveRecord{

    public static function getCart($kits_id){
        $kits_id = json_decode($kits_id);
        $criteria = new CDbCriteria();
        $criteria->addInCondition('t.id',$kits_id);
        return Item::model()->findAll($criteria);
    }

     public static function getCartByCookie(){
        $kit_ids = (string)Yii::app()->request->cookies['kit_ids'];
        if(empty($kit_ids)){
            return false;
        }
        else{
            return self::getCart($kit_ids);
        }
    }
}