<?php


class FeatureValueItem extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{features_values_items}}';
    }

    public function rules()
    {
        return array
        (
        );
    }

    public function relations()
    {
        return array
        (
            'feature_value' => array(self::HAS_MANY, 'FeatureValue', 'feature_value_id')
        );
    }


    public function attributeLabels()
    {
        return array
        (
        );
    }
}
