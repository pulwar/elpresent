<?php

/**
 * Class ItemSelfLink
 * @property integer $item_id
 * @property integer $child_id
 * @property integer $city_id
 * @property integer $option_id
 * @property Item    $superItem
 * @property Item    $item
 * @property Option  $option
 * @property City    $city
 */
class ItemSelfLink extends CActiveRecord
{
    /**
     * @param string $className
     * @return Item
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{item_item}}';
    }

    public function relations()
    {
        return [
            'superItem' => [self::BELONGS_TO, 'Item', 'item_id'],
            'item' => [self::BELONGS_TO, 'Item', 'child_id'],
            'option' => [self::BELONGS_TO, 'Option', 'option_id'],
            'city' => [self::BELONGS_TO, 'City', 'city_id'],
        ];
    }

}