<?php

class Superimpression extends Item
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    // filter by type
    public function defaultScope()
    {
        return array(
            'condition'=>"t.type=:type",
            'params' => array(':type'=>'group'),
            'with' => array('childs')
        );
    }

    public function scopes()
    {
        return array(
            'published' => array(
                'condition' => 't.draft=0',
            )
        );
    }

    public function afterFind()
    {

    }

    public function beforeValidate()
    {
        $this->partner_price = 666;
        //$this->text = "no";
        $this->category_id = 0;
        return parent::beforeValidate();
    }

    public function beforeSave()
    {
        $this->type = "group";
        return parent::beforeSave();
    }

}