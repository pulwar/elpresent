<?php

/**
 * Модель User
 *
 * @property integer $id
 * @property string $user_login
 * @property string $user_password
 * @property string $user_email
 * @property string $user_cdate
 * @property string $user_role
 * @property string $user_firstname
 * @property string $user_lastname
 * @property string $user_phone
 * @property string $user_address
 * @property string $user_sex
 * @property string $user_age
 * @property string $user_fullname
 * @property string $user_event_settings
 * @property string $user_forgot_password_hash
 * @property string $user_subscribe_hash
 * @property boolean $user_subscribe_status
 * @property string $feature_switches
 * @property string $user_birthday
 * @property string $user_birthday_date
 *
 * @property Order[] $orders
 * @property Partner|null $partner
 */
class User extends CActiveRecord
{
    const ROLE_ADMIN = 'admin';
    const ROLE_USER = 'user';
    const ROLE_GUEST = 'guest';
    const ROLE_PARTNER = 'partner';

    public $user_password_current;
    public $user_password_new;
    public $user_password_confirm;
    public $verifyCode;

    /**
     * @param string $className
     * @return User
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        return [
            [
                'class' => 'application.components.behaviors.SerializableFieldsBehaviour',
                'fields' => ['feature_switches']
            ],
        ];
    }


    public function tableName()
    {
        return '{{user}}';
    }


    public function rules()
    {
        return array(
            array('user_firstname, user_lastname, user_birthday', 'required', 'on' => 'register,update_profile'),

            array('cash', 'unsafe'),

            //array('user_firstname,user_lastname,user_birthday', 'required','on' => 'update_profile');

            array('user_phone', 'safe', 'on' => 'update_profile'),
            array(
                'user_password',
                'required',
                'message' => 'Поле Введите пароль не должно быть пустым',
                'on' => 'register,'
            ),
            array(
                'user_password_confirm',
                'required',
                'message' => 'Поле Повторите пароль не должно быть пустым',
                'on' => 'register'
            ),
            array(
                'user_password_current, user_password_new, user_password_confirm',
                'changePasswordValidator',
                'on' => 'update_profile'
            ),
            array('user_email', 'unique', 'message' => 'Такой e-mail уже занят'),
            array('user_email', 'email', 'message' => 'Введённый E-mail не является правильным e-mail адресом'),
            array(
                'user_password',
                'compare',
                'compareAttribute' => 'user_password_confirm',
                'message' => 'Пароли не совпадают',
                'on' => 'register'
            ),
        );
    }

    public function changePasswordValidator()
    {

        if (empty($this->user_password_current) && empty($this->user_password_new)) {
            return;
        }

        if (empty($this->user_password_current)) {
            $this->addError('user_password_current', 'Для того что бы сменить пароль, введите текущий');
        } else if (md5($this->user_password_current) !== $this->user_password) {
            $this->addError('user_password_current', 'Неверный пароль');
        }

        if (empty($this->user_password_new)) {
            $this->addError('user_password_new', 'Введите новый пароль');
        }
        if ($this->user_password_new !== $this->user_password_confirm) {
            $this->addError('user_password_new', 'Пароли не совпадают');
        }


    }

    public function beforeSave()
    {
        if ($this->user_birthday) {
            $bd = new \DateTime($this->user_birthday);
            $this->user_birthday = $bd->format('Y-m-d');
            $this->user_birthday_date = $bd->format('m-d');
        }

        if ($this->isNewRecord && $this->getScenario() === 'register') {
            $this->user_password = md5($this->user_password);
        }

        if (empty($this->user_password_current) && empty($this->user_password_new)) {
            return parent::beforeSave();
        }

        if (empty($this->user_password_current)) {
            $this->addError('user_password_current', 'Для того что бы сменить пароль, введите текущий');
            return false;
        } else if (md5($this->user_password_current) !== $this->user_password) {
            $this->addError('user_password_current', 'Неверный пароль');
            return false;
        }

        if (empty($this->user_password_new)) {
            $this->addError('user_password_new', 'Введите новый пароль');
            return false;
        }
        if ($this->user_password_new !== $this->user_password_confirm) {
            $this->addError('user_password_new', 'Пароли не совпадают');
            return false;
        }

        $this->user_password = md5($this->user_password_new);

        return parent::beforeSave();
    }


    public function relations()
    {
        return array(
            'orders' => [self::HAS_MANY, 'Order', 'user_id'],
            'partner' => [self::HAS_ONE, 'Partner', 'user_id'],
        );
    }

    /**
     * @return null|Partner
     */
    public function getPartner()
    {
        if ($this->user_role == self::ROLE_PARTNER) {
            return $this->partner;
        }
    }


    public function attributeLabels()
    {
        return array
        (
            'id' => '#',
            'user_password' => 'Пароль',
            'user_email' => 'EMail',
            'user_cdate' => 'Дата регистрации',
            'user_role' => 'Тип',
            'user_phone' => 'Телефон',
            'user_address' => 'Адрес',
            'user_sex' => 'Пол',
            'user_age' => 'Возраст',
            'user_fullname' => 'ФИО (устаревшее)',
            'user_firstname' => 'Имя',
            'user_lastname' => 'Фамилия',
            'user_event_settings' => 'Настройки эвентов',
            'user_forgot_password_hash' => 'Хеш восстановления пароля',
            'user_birthday' => 'День рождения',
        );
    }


    public static function createUserFromOrder(Order $order)
    {
        $user = static::model()->findByAttributes(['user_email' => $order->email]);
        if ($user) {
            return $user;
        }

        $password = static::generateRandomPass();

        $user = new User();
        $user->user_email = $order->email;
        $user->user_phone = $order->phone;
        $user->user_address = $order->adr;
        $user->user_cdate = date('Y-m-d H:i:s');
        $user->user_firstname = $order->name;
        $user->user_lastname = $order->lname;
        // whe should use SHA512 with multiple iterations and salt
        // but who cares?
        $user->user_password = md5($password);

        $user->user_role = User::ROLE_USER;

        if ($user->save()) {
            static::sendRegistrationNotification($user, $password);
        }

        return $user;
    }

    public static function sendRegistrationNotification(User $user, $password)
    {
        /** @var Mailer $mailer */
        $mailer = Yii::app()->mailer;

        $message = $mailer->createMessageFromView('Регистрация на сайте Elpresent.by',
            Yii::getPathOfAlias('application.views.email') . '/registration.php', [
                'user' => $user,
                'password' => $password
            ]
        );
        $message->setTo($user->user_email);

        $mailer->send($message);
    }


    /**
     * @param int $length
     * @return string
     */
    public static function generateRandomPass($length=8)
    {
        $dict = range(0, 9);
        $password = '';
        for($i=0;$i<8;$i++) {
            $password .= $dict[array_rand($dict)];
        }

        return $password;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        if ($this->user_firstname && $this->user_lastname) {
            return sprintf('%s %s', $this->user_firstname, $this->user_lastname);
        }

        if ($this->user_firstname) {
            return $this->user_firstname;
        }

        if ($this->user_lastname) {
            return $this->user_lastname;
        }

        return '';
    }

    public function getAge()
    {
        if (!$this->user_age || $this->user_age == '000-00-00' || $this->user_age > date('Y-m-d')) {
            return '--';
        }
        $age = floor((time() - strtotime($this->user_age)) / (365 * 60 * 60 * 24)) + 1;
        return $age;
    }

    public function getForm()
    {
        $this->setScenario('update_profile');

        return [
			'activeForm' => array(
				'class' => 'CActiveForm',
				'enableClientValidation' => true,
				'clientOptions' => array(
					'validateOnSubmit' => true
				)
			),

            'type'=>'form',
            'model' => $this,
            'elements'=>[
                'user_firstname' => [
                    'type' => 'text',
					'class' =>'form-control',
                ],
                'user_lastname' => [
                    'type' => 'text',
					'class' =>'form-control',
                ],
                'user_phone' => [
                    'type' => 'text',
					'class' =>'form-control',
                ],
                'user_birthday' => [
                    'type' => 'text',
					'class' => 'datepicker',
                    //'value' => $this->user_birthday ? date('Y-m-d', strtotime($this->user_birthday)) : '',
                ],
                '<h3>Смена пароля</h3>',
                'user_password_current' => [
					'type' => 'password',
					'label' => 'Текущий пароль',
				],
                'user_password_new' => [
					'type' => 'password',
					'label' => 'Новый пароль',
				],
                'user_password_confirm' => [
					'type' => 'password',
					'label' => 'Повторите новый пароль',
				],
            ],

            'buttons'=>[
                'save'=>[
                    'type'=>'submit',
                    'label'=>'Сохранить',
                    'class' => 'btn btn-default',
					'style' => 'width:107px; height:41; border-color:#ec3e06'
                ],
            ],
        ];
    }
}