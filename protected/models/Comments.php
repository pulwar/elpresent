<?php


class Comments extends CActiveRecord
{

    public $verifyCode;
    public $visibility;
    protected $message = false;


    static public function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    public function tableName()
    {
        return '{{comments}}';
    }

    public function rules()
    {
        return array
        (
            array('sender_name, text', 'required'),
            array('text', 'htmlTagsValidator'),
            array('rating', 'required', 'on' => 'front'),
            array('verifyCode', 'captcha', 'allowEmpty' => !extension_loaded('gd'), 'on' => 'front'),
            array('parent_id', 'safe')

        );
    }

    public function attributeLabels()
    {
        return array
        (
            'sender_name' => _('Имя отправителя'),
            'text' => _('Текст'),
            'rating' => _('Рейтинг'),
            'cdate' => _('Дата и время отзыва'),
            'item_id' => _('Название товара')
        );
    }

    public function getViewData()
    {
        return array
        (
            'sender_name' => 'SimpleTextType',
            'text' => 'SimpleTextType',
            'rating' => 'SimpleTextType'
        );
    }

    public function getFormData()
    {
        return array
        (
            'sender_name' => 'InputTextType',
            'text' => 'TextareaType',
            'rating' => 'InputTextType'
        );
    }

    public function relations()
    {
        return array
        (
            'item' => array(self::BELONGS_TO, 'Item', 'item_id'),
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
            //'childs' => array(self::HAS_MANY, 'Comments', 'parent_id', ),
            'childs' => array(self::HAS_MANY, 'Comments', 'parent_id', 'condition' => 'visibility = 1', 'order' => 'id ASC'),
        );
    }

    // todo: maybe replace on strip_tags?
    public function htmlTagsValidator($attribute, $params)
    {
        if (Yii::app()->user->getId())
        {
            return;
        }
        if (preg_match('/href\s*=/', $this->$attribute))
        {
            $this->addError($attribute, 'В теле сообщения запрещены html теги');
        }
    }

    public static function humanPluralForm($number, $titles = array('комментарий', 'комментария', 'комментариев'))
    {
        $cases = array(2, 0, 1, 1, 1, 2);
        return $number . " " . $titles[($number % 100 > 4 && $number % 100 < 20) ? 2 : $cases[min($number % 10, 5)]];
    }

    // templating...
    public static function showSubComments($parentComment)
    {
        echo '<div class="sub-comment">';
        foreach ($parentComment->childs as $child)
        {
            echo '<div>' . htmlspecialchars($child->sender_name) . '</div>';
            echo '<div class="fquote">';
            echo nl2br(htmlspecialchars($child->text));
            echo '</div>';
            echo '<div class="clear"><!-- --></div>';

            if (Yii::app()->user->id)
            {
                echo '<div class="action-comment">';
                echo '<a href="javascript:void(0)" class="reply _reply_comment" rcomment="' . $child->id . '">Ответить</a>';
                if ($child->user_id && $child->user_id == Yii::app()->user->id)
                {
                    echo '<a href="javascript:void(0)" class="delete-comment _delete_comment" dcomment="' . $child->id . '" >Удалить</a>';
                }
                echo '</div>';
                echo '<div class="clear"><!-- --></div>';
            }

            if (count($child->childs))
            {
                Comments::showSubComments($child);
            }
        }
        echo '</div><br />';
    }

    public function getMessage()
    {
        return $this->message ? $this->message : false;
    }

    public function addMessage($message)
    {
        $this->message = $message;
    }
}