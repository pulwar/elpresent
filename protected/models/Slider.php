<?php


class Slider extends CActiveRecord
{

    private $dir;

    public function init()
    {
        $this->dir = Yii::getPathOfAlias('webroot.images.slider');
    }
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{slider}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('title, type_id', 'required'),
            array('link', 'match', 'pattern' => '/^[^А-я]+$/'),
            array('desc, img', 'safe'),
            array('order, show, type_id', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, title, show', 'safe', 'on' => 'search'),

            array('img', 'file',
                'allowEmpty' => true,
                'maxFiles' => 10,
                'maxSize' => 1024 * 250 * 100,
                'minSize' => 1,
                'types' => 'jpg, jpeg, png, gif',
                'tooLarge' => 'Вы загружаете файл слишком большого размера',
                'tooSmall' => 'Вы загружаете файл слишком маленького размера',
                'tooMany' => 'Вы загружаете слишком большое количество файлов',
                'wrongType' => 'Неправильный тип файла',
                'wrongMimeType' => 'Неправильный MIME-тип файла',
            ),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'title' => 'Тайтл',
            'desc' => 'Описание',
            'link' => 'Ссылка',
            'img' => 'Картинка',
            'show' => 'Отображать?',
            'order' => 'Порядок',
            'type_id' => 'Тип слайдера',
        );
    }


    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('type_id', $this->type_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }


    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function beforeSave()
    {
        $file_path = $this->files_uploader('img');
        if (!empty($file_path)) {
            $this->img = implode('|', $file_path);
        }
        return parent::beforeSave();
    }

    public function afterDelete()
    {
        $this->filesRemove($this->img);
        return parent::afterDelete();
    }

    public function filesRemove($img_str){
        if (!empty($img_str)) {
            $file_paths = explode('|', $img_str);
            foreach ($file_paths as $file_path) {
                if (is_file($this->dir . DIRECTORY_SEPARATOR . $file_path)) {
                    unlink($this->dir . DIRECTORY_SEPARATOR . $file_path);
                }
            }
        }
        return true;
    }

    /**
     * Загрузка файлов
     * @param $input_name название поля загрузчика
     * @return null|имя загруженного файла
     */
    private function files_uploader($input_name)
    {
        $files = CUploadedFile::getInstances($this, $input_name);

        foreach ($files as $k => $file) {

            if (!is_dir($this->dir)) {
                mkdir($this->dir, 0777);
            }

            $ext = pathinfo($file->getName(), PATHINFO_EXTENSION);


            // транслит на всякий случай
            //$fileName = text::getAliasByStr($file->getName());

            $nameFile[$k] = md5(time() . rand(1, 9)) . '.'.$ext;

            $filePath[$k] = $this->dir . DIRECTORY_SEPARATOR . $nameFile[$k];
            $file->saveAs($filePath[$k]);
            chmod($filePath[$k], 0755);
        }

        return empty($nameFile) ?  null : $nameFile;
    }

}
