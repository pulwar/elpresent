<?php

class Partner extends CActiveRecord
{
     public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function behaviors()
    {
        return [
            [
                'class' => 'application.components.behaviors.SerializableFieldsBehaviour',
                'fields' => ['feature_switches']
            ],
        ];
    }


    public function tableName()
    {
        return '{{partner}}';
    }
    
    
    

}
