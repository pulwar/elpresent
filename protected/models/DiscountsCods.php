<?php
/**
 * @author: Alexander Zhaliazouski <goper@tut.by>
 * @date: 06.04.2016 15:26
 */
class DiscountsCods extends CActiveRecord{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{discounts_cods}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('code, discount_id', 'required')
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array
        (
            'id' => 'Id',
            'code' => 'Код',
            'discount_id' => 'Скидка',
        );
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}