<?php

class TextRecord extends CActiveRecord
{
    static public function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    public function tableName()
    {
        return '{{text_record}}';
    }

    public function attributeLabels()
    {
        return array(
            'alias' => 'URI страницы',
            'enabled' => 'Включить страницу',
            'text' => 'Содержимое',
            'title' => 'Заголовок',
			'meta_title' => 'Мета заголовок',
			'meta_description' => 'Мета Описание',
			'meta_keywords' => 'Мета ключевые слова',
        );
    }

    public function rules()
    {
        return array(
            array('alias,text,title', 'required'),
        );
    }

    public function getViewData()
    {
        return array('title' => 'SimpleTextType');
    }

    public function getFormData()
    {
        return array(
            'title' => 'InputTextType',
            'alias' => 'InputTextType',
            'text' => 'TextareaType',
            'enabled' => 'CheckboxType',
			'meta_title' => 'InputTextType',
			'meta_description' => 'InputTextType',
			'meta_keywords' => 'InputTextType',
        );
    }

    static public function getAliasedText($alias)
    {
        return self::model()->findByAttributes(array('alias' => $alias))->text;
    }

    public function isEnabled()
    {
        return $this->enabled > 0;
    }

    public function getLink($noFollow = false)
    {
		return '<a'.($noFollow ? ' rel="nofollow"':'').' href="'.Yii::app()->createUrl('pages/show', ['alias'=>$this->alias]).'">'.$this->title.'</a>';
    }

    public static function getAliases()
    {
        return array_map(function (TextRecord $record) {
            return $record->alias;
        }, static::model()->findAll('enabled=1'));
    }

    public function afterFind()
    {
        $GLOBALS['meta_title'] = $this->meta_title;
        $GLOBALS['meta_description'] = $this->meta_description;
        $GLOBALS['meta_keywords'] = $this->meta_keywords;
    }
}
