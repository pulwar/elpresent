<?php


class Landing extends CActiveRecord
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{landing}}';
    }

    public function primaryKey() {
        return ['map_code', 'phones', 'address', 'time_work', 'meta_d', 'meta_k', 'delivery', 'email', 'customers'];
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            ['map_code, phones, address, time_work, meta_d, meta_k, delivery, email, customers', 'required'],
            ['email', 'email']
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'phones' => 'Телефоны через запятую',
            'address' => 'Адрес',
            'time_work' => 'Время работы',
            'map_code' => 'Код карты',
            'meta_d' => 'Мета текст',
            'meta_k' => 'Мета слова',
            'delivery' => 'Оплата и доставка',
            'email' => 'Куда будут приходить письма',
            'customers' => 'Клиенты',
        );
    }


    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        //$criteria->compare('id', $this->id);
        //$criteria->compare('title', $this->title, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }


    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function beforeSave()
    {
        return parent::beforeSave();
    }

    public function afterDelete()
    {
        return parent::afterDelete();
    }

}
