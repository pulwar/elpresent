<?php

class Contact extends CActiveRecord
{
    public $verifyCode;

    static public function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    public function tableName()
    {
        return '{{contact}}';
    }

    public function  onBeforeValidate($event)
    {
        $this->contact_date = date('Y-m-d H:i:s');
        parent::onBeforeValidate($event);
    }

    public function rules()
    {
        return array
        (
            array('contact_name, contact_subject, contact_body', 'required'),
            array('contact_email', 'email'),
            array('contact_rating', 'numerical', 'min' => 1, 'max' => 5),
            array('contact_date', 'safe'),
        );
    }

    public function attributeLabels()
    {
        return array
        (
            'contact_name' => _('Имя'),
            'contact_email' => _('Email'),
            'contact_subject' => _('Тема'),
            'contact_body' => _('Отзыв'),
            'contact_date' => _('Дата'),
            'contact_status' => _('Статус'),
            'contact_rating' => _('Рейтинг'),
        );
    }

    public function relations()
    {
        return array();
    }
}