<?
class Mood extends CActiveRecord
{

    static public function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    public function tableName()
    {
        return '{{moods}}';
    }

    public function relations()
    {
        return array(
            'profile' => array(self::BELONGS_TO, 'MoodProfile', 'profile_id'),
            'items' => array(self::HAS_MANY, 'MoodItems', 'mood_id'),
        );
    }

    public function rules()
    {
        return array(
            array('profile_id,image,mood_order', 'required'),
            array('mood', 'safe'),
        );
    }


    public function attributeLabels()
    {
        return array(
            'desc' => _('Text'),
            'is_calendar' => _('Добавить в календарь (Напоминалка за день)'),
            'is_important' => _('Важный праздник (Напоминалка за две недели)')
        );
    }

    public function getViewData()
    {
        return array
        (
            'title' => 'SimpleTextType',
        );
    }

    public function getFormData()
    {
        return array
        (
            'title' => 'InputTextType',

        );
    }

    public function generateRandomName()
    {
        $length = 10;
        $characters = "0123456789abcdefghijklmnopqrstuvwxyz";
        $string = "";

        for ($p = 0; $p < $length; $p++)
        {
            $string .= $characters[mt_rand(0, (strlen($characters) - 1))];
        }

        return $string;
    }

    public function getItemsBlock()
    {
        $items = $this->items;
        $rItems = array();

        if (!empty($items))
        {
            $id = mt_rand(0, count($items) - 1);
            $rItems[] = $items[$id];
            unset($items[$id]);
            $items = array_values($items);
        }
        if (!empty($items))
        {
            $id = mt_rand(0, count($items) - 1);
            $rItems[] = $items[$id];
            unset($items[$id]);
            $items = array_values($items);
        }
        if (!empty($items))
        {
            $id = mt_rand(0, count($items) - 1);
            $rItems[] = $items[$id];
            unset($items[$id]);
            $items = array_values($items);
        }
        if (empty($rItems))
        {
            return '';
        }
        $html = '<table width="100%">';
        foreach ($rItems as $item)
        {
            $html .= '<tr>';
            $html .= '<td><img src="/' . $item->item->preview_img . '" width="35" /></td>';
            $html .= '<td><a href="'.Yii::app()->createUrl('catalog/item', ['perm_link' => $item->perm_link]).'">' . $item->title . '</a></td>';
            $html .= '</tr>';
        }
        $html .= '</table>';
        return $html;

    }

}