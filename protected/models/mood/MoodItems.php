<?
class MoodItems extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{mood_items}}';
    }

    public function rules()
    {
        return array
        (
        );
    }

    public function relations()
    {
        return array
        (
            'item' => array(self::BELONGS_TO, 'Item', 'item_id'),
        );
    }


    public function attributeLabels()
    {
        return array
        (
        );
    }

    public static function addItems($items, $mood_id)
    {

        MoodItems::model()->deleteAllByAttributes(array('mood_id' => $mood_id));
        foreach ($items as $k => $v)
        {

            $moodItem = new MoodItems();
            $moodItem->item_id = $k;
            $moodItem->title = mb_substr($v, 0, 32, 'UTF-8');
            $moodItem->mood_id = $mood_id;
            $moodItem->save();
        }

    }
}
