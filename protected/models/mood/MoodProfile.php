<?
class MoodProfile extends CActiveRecord
{

    static public function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    public function tableName()
    {
        return '{{mood_profiles}}';
    }

    public function rules()
    {
        return array(
            array('profile,profile_order,name', 'required'),
        );
    }

    public function scopes()
    {
        return array(
            'sorted' => array(
                'order' => 'profile_order',
                'condition' => 't.visible=1'
            ),
        );

    }

    public function getViewData()
    {
        return array
        (
            'name' => 'SimpleTextType',
            'profile' => 'SimpleTextType',
            'profile_order' => 'SimpleTextType',
        );
    }

    public function attributeLabels()
    {
        return array
        (
            'name' => _('Name'),
            'profile' => _('Profile'),
            'profile_order' => _('Order'),
        );
    }


    public function relations()
    {
        return array(
            'moods' => array(self::HAS_MANY, 'Mood', 'profile_id'),
        );
    }

}