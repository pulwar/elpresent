<?php


class News extends CActiveRecord
{
    /**
     * The followings are the available columns in table '{{News}}':
     * @var integer $id
     * @var string $title
     * @var string $text
     * @var string $date
     */


    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{news}}';
    }

    public function behaviors()
    {
        return [
            array(
                'class' => 'application.components.behaviors.CachePurger',
                'tags' => array('news-widget')
            )
        ];
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        if (!$this->isNewRecord)
        {
            return array
            (
                array('title, text', 'required'),
                array('title', 'length', 'max' => 255)
            );
        }

        return array(
            array('title, text', 'required'),
            array('title', 'length', 'max' => 255),
            //array('preview_img', 'file', 'types' => 'jpg, gif, png'),
            //array('full_img',    'file', 'types' => 'jpg, gif, png')
        );
    }


    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array
        (
            'id' => _('Id'),
            'title' => 'Заголовок',
            'text' => 'Текст',
            'date' => 'Дата',
        );
    }

    public function ordered()
    {
        $this->getDbCriteria()->order = '`date` desc';
        return $this;
    }

    public function getDateFormated()
    {
        $d = new DateTime($this->date);
        return date_format($d, "Y/m/d");
    }
}
