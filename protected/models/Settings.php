<?php

/**
 * Class Settings
 * @property int setting_id
 * @property string setting_alias
 * @property string setting_value
 */
class Settings extends CActiveRecord
{
    public static function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    public function tableName()
    {
        return '{{settings}}';
    }

    public static function getValueByAlias($alias)
    {
        $s = Settings::model()->findByAttributes(array('setting_alias' => $alias));

        return $s ? $s->setting_value : null;
    }

    public function rules()
    {
        return [
            ['setting_alias, setting_value', 'required']
        ];
    }
}
