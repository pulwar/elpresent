<?
class ForgotPasswordForm extends CFormModel
{
    public $email;

    public function rules()
    {
        return array
        (
            array('email', 'required', 'message' => 'Поле E-mail не должно быть пустым'),
            array('email', 'email', 'message' => 'Неверный формат E-mail')
        );
    }
}