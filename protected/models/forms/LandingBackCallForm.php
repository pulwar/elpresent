<?
class LandingBackCallForm extends CFormModel
{
    public $email;
    public $name;
    public $phone;

    public function rules()
    {
        return array
        (
            array('name, phone, email', 'required', 'message' => 'Не заполнены поля {attribute}'),
            array('email', 'email', 'message' => 'Неверный E-mail'),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array
        (
            'name' => '*Введите имя:',
            'phone' => '*Введите номер телефона:',
            'email' => '*Введите e-mail:'
        );
    }

}
