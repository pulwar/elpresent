<?
/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class LoginForm extends CFormModel
{

    public $user_email;
    public $user_password;

    //public $remember_me;

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules()
    {
        return array
        (
            array('user_email, user_password', 'required', 'message' => 'Не заполнены все поля'),
            array('user_email', 'email', 'message' => 'Неверный E-mail'),
            //array('remember_me', 'boolean'),
            array('user_password', 'authenticate', 'message' => 'Ошибка авторизации')
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array
        (
            // 'remember_me' => 'Запомнить меня'
        );
    }

    /**
     * Authenticates the password.
     * This is the 'authenticate' validator as declared in rules().
     */
    public function authenticate()
    {
        if (!$this->hasErrors()) // we only want to authenticate when no input errors
        {
            $identity = new UserIdentity($this->user_email, $this->user_password);

            if ($identity->authenticate())
            {
                //$duration = $this->remember_me ? 3600 * 24 * 30 : 0;
                $duration = 3600 * 24 * 30;
                Yii::app()->user->login($identity, $duration);
            }
            else
            {
                $this->addError('user_email', 'Неверный E-mail либо пароль.');
            }
        }
    }

}
