<?php

/**
 * Refactor
 */
class BuyForm extends CFormModel
{
    public $recipient_firstname;
    public $recipient_lastname;
    public $recipient_address;
    public $recipient_gift_date;
    public $firstname;
    public $lastname;
    public $phone;
    public $email;
    public $adr;
    public $payer_adr;
    public $comment;
    public $knowwhere;
    public $is_express_delivery_recipient;

    public $recipient_desired_date;
    public $recivier_pay_date;
    public $payer_date;
    public $gift_date;

    public $recipient_desired_hour_from;
    public $recipient_desired_minute_from;
    public $recipient_desired_hour_to;
    public $recipient_desired_minute_to;
    public $recipient_phone;
    public $payer_hour;
    public $payer_minute;
    public $payer_hour_from;
    public $payer_minute_from;
    public $payer_hour_to;
    public $payer_minute_to;
    public $gift_hour_from;
    public $gift_hour_to;
    public $gift_minute_from;
    public $gift_minute_to;
    public $delivery_id;
    public $payment_type_id;
    //public $easypay_card;
    public $divaby_discount;
    public $divaby_card_id;
    public $svadbaby_discount;
    public $svadbaby_card_id;

    public $is_thematic_delivery;
    public $thematic_delivery_price;

    public $expr_deliv = array();

    public static $aKnowWhere = array
    (
        'Я ваш постоянный клиент',
        'нашел(ла) карточку "Если мешает авто, позвоните..."',
        'Интернет' => array(
            'Поисковик',
            'Другой сайт',
            'Социальная сеть',
            'Форум',
            'E-mail рассылка',
            'IT-CUP',
            'Goals.by'
        ),
        'Радио' => array(
            'Unistar',
            'Русское радио',

        ),
        'Журналы' => array(
            'Большой',
            'Minsk Where',
            'Женский Журнал',
            'Конфитюр',
            'Yoga+life',
            'Газета Ва-банк',
        ),
        'Наружная реклама' => array(
            'Бизнес и торговые центры',
            'Информационный плакат',
            'Листовка'
        ),
        'Другие варианты' => array(
            'От знакомых',
            'От поставщика услуг',
            'Рассылка почтовая',
            'Подарили подарок',
            'Кинотеатр "Беларусь"',
            'Затрудняюсь ответить',
            'Выставка',
        ),


    );
    public static $delivery = array(
        array(

            'price' => 40000,
            'label' => '40 000 руб.',
            'title' => 'Обычная доставка',
            'desc' => 'При заказе до 14.00 - доставка в тот же день, при заказе после 14.00 - на следующий день.'
        ),
        array(

            'price' => 0,
            'label' => '0 руб.',
            'title' => 'Самовывоз',
            'desc' => 'с 10.30 до 19.30 по адресу Пр. Независимости 58 - ТЦ Московско-Венский, 3 этаж, павильон 319. Выходной день - воскресенье.'
        ),
    );


    public static $aThematicDelivery = array(

        'default' => array(
            'alias' => 'default',
            'title' => 'Выберите',
            'price' => 0,
        ),
        'clowns' => array(
            'alias' => 'clowns',
            'title' => 'Клоун',
            'price' => 200000,
        ),
        'superman' => array(
            'alias' => 'superman',
            'title' => 'Супермен',
            'price' => 200000,
        ),
    );


    public function  onBeforeValidate($event)
    {
        if (isset($_POST['is_express_delivery_recipient']) && count($_POST['is_express_delivery_recipient']))
        {
            $this->expr_deliv = array_keys($_POST['is_express_delivery_recipient']);
        }
        parent::onBeforeValidate($event);
    }

    public function rules()
    {



        // todo: add validation group
        if(!empty($_POST['delivery_id_1'])) {
            return array(
                array('recipient_firstname', 'arrayValidator', 'type' => 'req', 'message' => 'Поле Имя получателя подарка не должно быть пустым', 'on' => 'both, recipient'),
                array('recipient_lastname', 'arrayValidator', 'type' => 'req', 'message' => 'Поле Фамилия получателя подарка не должно быть пустым', 'on' => 'both, recipient'),
                array('recipient_address', 'arrayValidator', 'type' => 'req', 'message' => 'Поле Адрес вручения подарка не должно быть пустым', 'on' => 'both, recipient'),

                array('recipient_desired_date', 'arrayValidator', 'type' => 'req', 'message' => 'Поле Желаемая дата вручения подарка не должно быть пустым', 'on' => 'both, recipient'),
                array('recipient_desired_hour_from, recipient_desired_hour_to', 'arrayValidator', 'type' => 'hour', 'message' => 'Неверное значение часа поля Желаемое время вручения подарка', 'on' => 'both, recipient'),
                array('recipient_desired_minute_from, recipient_desired_minute_to', 'arrayValidator', 'type' => 'minute', 'message' => 'Неверное значение минут поля Желаемое время вручения подарка', 'on' => 'both, recipient'),
                array('recipient_phone', 'required', 'message' => 'Поле Контактный телефон получателя подарка не должно быть пустым', 'on' => 'both, recipient'),

                array('firstname', 'required', 'message' => 'Поле Ваше Имя не должно быть пустым'),
                array('lastname', 'required', 'message' => 'Поле Ваша Фамилия не должно быть пустым'),
                array('phone', 'required', 'message' => 'Поле Контактный телефон не должно быть пустым'),
                array('email', 'required', 'message' => 'Поле E-mail не должно быть пустым'),

//                array('email', 'emailUniqueValidator'
//                , 'message' => 'Такой E-mail уже существует в базе. Авторизируйтесь либо используйте другой E-mail.'
//                , 'caseSensitive' => false, 'allowEmpty' => false, 'className' => 'User', 'attributeName' => 'user_email'),

                array('payer_date', 'required', 'message' => 'Поле Желаемая дата оплаты не должно быть пустым', 'on' => 'both, recipient'),

                array('payment_type_id', 'numerical', 'message' => 'Некорректный способ оплаты'),
                //array('easypay_card', 'easyPayValidator', 'message' => 'Идентификатор электронного кошелька EasyPay должен состоять из 8 цифр'),
                //array('easypay_card', 'numerical', 'message' => 'Идентификатор электронного кошелька EasyPay должен состоять из цифр'),

                array('payer_hour_from, payer_hour_to', 'validateHourItm', 'message' => 'Неверное значение часа поля Желаемое время оплаты', 'on' => 'both, recipient'),
                array('payer_minute_from, payer_minute_to', 'validateMinuteItm', 'message' => 'Неверное значение минут поля Желаемое время оплаты', 'on' => 'both, recipient'),
                array('gift_hour_from, gift_hour_to', 'validateHourItm', 'message' => 'Неверное значение часа поля Желаемое время доставки', 'on' => 'both, user'),
                array('gift_minute_from, gift_minute_to', 'validateMinuteItm', 'message' => 'Неверное значение минут поля Желаемое время доставки', 'on' => 'both, user'),

                //diva.by
                array('divaby_discount', 'safe'),
                array('divaby_card_id', 'divabyCardId', 'length' => 9),
            );
        }

        return array
        (
            //recipient
            array('recipient_firstname', 'arrayValidator', 'type' => 'req', 'message' => 'Поле Имя получателя подарка не должно быть пустым', 'on' => 'both, recipient'),
            array('recipient_lastname', 'arrayValidator', 'type' => 'req', 'message' => 'Поле Фамилия получателя подарка не должно быть пустым', 'on' => 'both, recipient'),
            array('recipient_address', 'arrayValidator', 'type' => 'req', 'message' => 'Поле Адрес вручения подарка не должно быть пустым', 'on' => 'both, recipient'),

            array('recipient_desired_date', 'arrayValidator', 'type' => 'req', 'message' => 'Поле Желаемая дата вручения подарка не должно быть пустым', 'on' => 'both, recipient'),
            array('recipient_desired_hour_from, recipient_desired_hour_to', 'arrayValidator', 'type' => 'hour', 'message' => 'Неверное значение часа поля Желаемое время вручения подарка', 'on' => 'both, recipient'),
            array('recipient_desired_minute_from, recipient_desired_minute_to', 'arrayValidator', 'type' => 'minute', 'message' => 'Неверное значение минут поля Желаемое время вручения подарка', 'on' => 'both, recipient'),
            array('recipient_phone', 'required', 'message' => 'Поле Контактный телефон получателя подарка не должно быть пустым', 'on' => 'both, recipient'),

            //recivier
            array('firstname', 'required', 'message' => 'Поле Ваше Имя не должно быть пустым'),
            array('lastname', 'required', 'message' => 'Поле Ваша Фамилия не должно быть пустым'),
            array('phone', 'required', 'message' => 'Поле Контактный телефон не должно быть пустым'),
            array('email', 'required', 'message' => 'Поле E-mail не должно быть пустым'),
            array('adr', 'required', 'message' => 'Поле Адрес доставки не должно быть пустым', 'on' => 'both, user'),
            array('payer_adr', 'required', 'message' => 'Поле Адрес оплаты не должно быть пустым', 'on' => 'both, recipient'),
            // array('knowwhere', 'required', 'message' => 'Поле Откуда вы узнали о нас не должно быть пустым'),
            array('email', 'email', 'message' => 'Неверный формат E-mail'),
//            array('email', 'emailUniqueValidator'
//            , 'message' => 'Такой E-mail уже существует в базе. Авторизируйтесь либо используйте другой E-mail.'
//            , 'caseSensitive' => false, 'allowEmpty' => false, 'className' => 'User', 'attributeName' => 'user_email'),

            array('payer_date', 'required', 'message' => 'Поле Желаемая дата оплаты не должно быть пустым', 'on' => 'both, recipient'),
            array('gift_date', 'required', 'message' => 'Поле Желаемая дата доставки не должно быть пустым', 'on' => 'both, user'),

            array('payment_type_id', 'numerical', 'message' => 'Некорректный способ оплаты'),

            array('payer_hour_from, payer_hour_to', 'validateHourItm', 'message' => 'Неверное значение часа поля Желаемое время оплаты', 'on' => 'both, recipient'),
            array('payer_minute_from, payer_minute_to', 'validateMinuteItm', 'message' => 'Неверное значение минут поля Желаемое время оплаты', 'on' => 'both, recipient'),
            array('gift_hour_from, gift_hour_to', 'validateHourItm', 'message' => 'Неверное значение часа поля Желаемое время доставки', 'on' => 'both, user'),
            array('gift_minute_from, gift_minute_to', 'validateMinuteItm', 'message' => 'Неверное значение минут поля Желаемое время доставки', 'on' => 'both, user'),

            //diva.by
            array('divaby_discount', 'safe'),
            array('divaby_card_id', 'divabyCardId', 'length' => 9),

        );
    }

    /*
     public function easyPayValidator($attribute, $params)
    {
        if (4 != $this->payment_type_id)
        {
            return;
        }
        if (8 != strlen($this->$attribute))
        {
            $this->addError($attribute, $params['message']);
            return;
        }

    }
    */

    public function emailUniqueValidator($sAttribute, $aParams)
    {
        if (Yii::app()->user->getId())
        {
            return;
        }

        $validator = new CUniqueValidator();
        foreach ($aParams as $k => $v)
        {
            $validator->$k = $v;
        }
        $validator->attributes = array($sAttribute);

        $validator->validate($this);

    }

    public function arrayValidator($attribute, $params)
    {
        if (strpos($params['on'], $this->getScenario()) === FALSE)
        {
            return;
        }

        $aAttributes = $this->$attribute;
        if (count($aAttributes))
        {
            $sValidate = 'validate' . ucfirst($params['type']);
            if (!method_exists($this, $sValidate))
            {
                return;
            }
            foreach ($aAttributes as $iK => $sAttribute)
            {
                if (!in_array($iK, $this->expr_deliv))
                {
                    continue;
                }
                if (!$this->$sValidate($sAttribute))
                {
                    $this->addError($attribute, $params['message']);
                    return;
                }
            }
            return;
        }
        $this->addError($attribute, $params['message']);
    }

    public function validateReq($sAttribute)
    {
        return (bool) strlen($sAttribute);
    }


    public function  validateHourItm($sAttribute, $aParams)
    {
        if (!$this->validateHour($this->$sAttribute))
        {
            $this->addError('hour', $aParams['message']);
        }
    }

    public function  validateMinuteItm($sAttribute, $aParams)
    {
        if (!$this->validateMinute($this->$sAttribute))
        {
            $this->addError('minute', $aParams['message']);
        }
    }


    public function validateHour($sAttribute)
    {
        $sAttribute = trim($sAttribute);

        return strlen($sAttribute) && is_numeric($sAttribute) && in_array(intval($sAttribute), range(0, 23));
    }

    public function validateMinute($sAttribute)
    {
        $sAttribute = trim($sAttribute);

        return strlen($sAttribute) && is_numeric($sAttribute) && in_array(intval($sAttribute), range(0, 60));
    }


    public static function hourRange()
    {
        return array_map(function ($hour) {
            // add leading zero
            return str_pad($hour, 2, '0', STR_PAD_LEFT);
        }, range(0, 24, 1));
    }


    public static function minuteRange()
    {
        return array_map(function ($minute) {
            // add leading zero
            return str_pad($minute, 2, '0', STR_PAD_LEFT);
        }, range(0, 60, 5));
    }


    public static function getThematicDeliveryPrice($alias)
    {
        return self::$aThematicDelivery[$alias]['price'];
    }


    public static function getThematicDeliveryName($iNumber)
    {
        return self::$aThematicDelivery[intval($iNumber)]['title'];
    }
}