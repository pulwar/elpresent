<?

class OptionUpdateForm extends CFormModel
{
    public $citiy_id;
    public $partner_price;
    public $option;

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules()
    {
        return array
        (
            array('citiy_id', 'required', 'message' => 'Не заполнены поля {attribute}'),
            array('partner_price', 'is_empty_partner_price'),
            array('option', 'option_validate'),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array
        (
            'citiy_id' => 'Id города',
            'partner_price' => 'Цена партнера'
        );
    }

    public function is_empty_partner_price($attr, $params){
        foreach ($this->partner_price as $partner_price){
            //html::pr($partner_price,1);
            if(is_array($partner_price)){
                foreach ($partner_price as $v){
                    if (empty($v) || !is_numeric($v)){
                        $this->addError('partner_price', 'Было не заполнено или заполнено неправильно поле Цена партнера');
                    }
                }
            }
            else{
                if (empty($partner_price) || !is_numeric($partner_price)){
                    $this->addError('partner_price', 'Было не заполнено или заполнено неправильно поле Цена партнера');
                }
            }

        }
    }

    public function option_validate($attr, $params){
        foreach($this->option['option_id'] as $option_id){
            if(empty($option_id) || !is_numeric($option_id)){
                $this->addError('option_id', 'Было не заполнено или заполнено неправильно поле option_id');
            }
        }

        foreach($this->option['price'] as $price){
            if(empty($price) || !is_numeric($price)){
                $this->addError('price', 'Было не заполнено или заполнено неправильно поле Цена');
            }
        }

        foreach($this->option['option_name'] as $option_name){
            if(empty($option_name)){
                $this->addError('option_name', 'Не заполнено поле Название опции');
            }
        }

        foreach($this->option['option_small_name'] as $option_small_name){
            if(empty($option_small_name)){
                $this->addError('option_small_name', 'Не заполнено поле Короткое название опции');
            }
        }


        foreach($this->option['option_desc'] as $option_desc){
            if(empty($option_desc)){
                $this->addError('option_desc', 'Не заполнено поле Описание опции');
            }
        }
    }
}
