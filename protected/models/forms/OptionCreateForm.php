<?

class OptionCreateForm extends CFormModel
{
    public $citiy_id;
    public $option_name;
    public $option_small_name;
    public $option_desc;
    public $price;
    public $partner_price;

    public function rules()
    {
        return array
        (
            array('option_name, option_small_name, option_desc, price, partner_price, citiy_id', 'required', 'message' => 'Не заполнены поля {attribute}'),
            array('price, citiy_id', 'numerical', 'integerOnly'=>false),
            array('partner_price', 'is_empty_partner_price'),
            array('option_name, option_small_name, option_desc', 'filter', 'filter' => 'trim'),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array
        (
            'option_name' => 'Название опции',
            'option_small_name' => 'Краткое название опции',
            'option_desc' => 'Описание опции',
            'partner_price' => 'Цена партнера',
            'price' => 'Цена по городу'
        );
    }

    public function is_empty_partner_price($attr, $params){
        foreach ($this->partner_price as $partner_price){
            if (empty($partner_price) || !is_numeric($partner_price)){
                $this->addError('partner_price', 'Было не заполнено или заполнено неправильно поле Цена партнера');
            }
        }
    }

}
