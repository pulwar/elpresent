<?
class LandingHelpForm extends CFormModel
{
    public $name;
    public $phone;
    public $time_from;
    public $time_to;
    public $comment;

    public function rules()
    {
        return array
        (
            array('name, phone, time_from, time_to, comment', 'required', 'message' => 'Не заполнены поля {attribute}'),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array
        (
            'name' => '*Введите имя:',
            'phone' => '*Введите номер телефона:',
            'comment' => '*Добавьте комментарий:',
            'time_from'  => '*Введите время:',
            'time_to' => '*Введите время:'
        );
    }

}
