<?

class OptionDeleteForm extends CFormModel
{
    public $citiy_id;
    public $partner_id;
    public $partner_price;
    public $price;


    public function rules()
    {
        return array
        (
            array('citiy_id, partner_id, partner_price, price', 'required', 'message' => 'Не заполнены поля {attribute}'),
            array('citiy_id, partner_id, partner_price, price', 'numerical', 'integerOnly'=>true),
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array
        (
            'citiy_id' => 'Id города',
            'partner_id' => 'Id партнера',
            'partner_price' => 'Цена партнера',
            'price' => 'Цена по городу'
        );
    }

}
