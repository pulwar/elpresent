<?
/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class RestoreForm extends CFormModel
{

    public $user_email;

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules()
    {
        return array
        (
            array('user_email', 'required', 'message' => 'Не заполнены все поля'),
            array('user_email', 'email', 'message' => 'Неверный E-mail'),
            array('user_email', 'authenticate', 'message' => 'Ошибка авторизации')
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array
        (
            'user_email' => 'Email для восстановления'
        );
    }

    public function authenticate()
    {
        $user = User::model()->find('user_email=:user_email', array(':user_email' => $this->user_email));

        if (empty($user)) {
            $this->addError('user_email', 'Данный E-mail не зарегестрирован в системе.');
        }

    }
}
