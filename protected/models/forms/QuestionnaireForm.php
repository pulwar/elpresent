<?
class QuestionnaireForm extends CFormModel
{
    public $company_name;
    public $company_activities;
    public $employees_num;
    public $contact;
    public $phone;
    public $source;
    public $employees_bd;
    public $presents;
    public $other_shops;
    public $wishes;
    public $other_companies;


    public function rules()
    {
        return array(
            array('company_name, company_activities, employees_num, contact, phone, source, employees_bd, presents, other_shops', 'required'),
            array('wishes, other_companies', 'safe'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'company_name' => 'Название компании',
            'company_activities' => 'Направление деятельности компании',
            'employees_num' => 'Количество сотрудников',
            'contact' => 'Контактное лицо',
            'phone' => 'Контактный телефон',
            'source' => 'Откуда вы о нас узнали?',
            'employees_bd' => 'Принято ли в вашей компании поздравлять сотрудников с днями рождения и с другими памятными датами?',
            'presents' => 'Что вы обычно дарите вашим сотрудникам?',
            'other_shops' => 'Работали ли раньше со службами по организации корпоративных мероприятий или магазинами?',
            'wishes' => 'Пожелания по работе с нами',
            'other_companies' => 'С какими?',
        );
    }
}