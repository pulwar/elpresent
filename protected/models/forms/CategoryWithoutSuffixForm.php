<?

class CategoryWithoutSuffixForm extends CFormModel
{
    public $txt;

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(//array('name, email', 'required'),
        );
    }

    public function attributeLabels()
    {
        return array
        (
            'txt' => 'Введите url (значимую часть) категорий через запятую, в которых не надо выводить суффикс',
        );
    }
}