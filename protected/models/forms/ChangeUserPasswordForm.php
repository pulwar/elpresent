<?php


class ChangeUserPasswordForm extends CFormModel
{
    public $new_password;

    public function rules()
    {
        return array (
            array('new_password', 'required', 'message' => 'Поле Новый пароль не должно быть пустым'),
        );
    }
}