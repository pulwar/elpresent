<?php

/**
 * Class Flower
 *
 * @property string $flower_title
 * @property integer $flower_price
 */
class Flower extends CActiveRecord
{
    public $flower_image;

    public static function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    public function tableName()
    {
        return '{{flowers}}';
    }

    public function rules()
    {
        return array
        (
            array('flower_title', 'required')
        );
    }

    public function attributeLabels()
    {
        return array
        (
            'flower_id' => '#',
            'flower_title' => 'Название',
            'flower_price' => 'Цена',
            'flower_image' => 'Изображение'
        );
    }

    public function generateRandomName()
    {
        $length = 10;
        $characters = "0123456789abcdefghijklmnopqrstuvwxyz";
        $string = "";

        for ($p = 0; $p < $length; $p++)
        {
            $string .= $characters[mt_rand(0, (strlen($characters) - 1))];
        }

        return $string;
    }

    public static function getProvider($pageSize = 100)
    {
        return new CActiveDataProvider('Flower', array(
            'pagination' => array(
                'pageSize' => $pageSize
            )
        ));
    }
}
