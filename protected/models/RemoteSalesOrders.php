<?php


class RemoteSalesOrders extends CActiveRecord
{
	const ADMIN = 'elpresent';

	/**
	 * Путь к вайлу баллов
	 */
	const JSON_SCORE_FILE_PATH = 'assets/score.json';

    /**
     * Путь к вайлу баллов
     */
    const JSON_USERS_FILE_PATH = 'assets/users.json';

	/**
	 * Операторы для поиска передаваемые extjs
	 *
	 * @var array
	 */
	public static $operators = [
		'lt' => '<',
		'gt' => '>',
		'eq' => '='
	];

	/**
	 * Скидки
	 * @var array
	 */
	public static $sales = [
		['val' => 0], ['val' => 5], ['val' => 7], ['val' => 10], ['val' => 15],['val' => 20]
	];

	/**
	 * Не показывать подарки
	 * @var array
	 */
	public static $exclusionGifts = [1641, 1642];

	public $option_name;

	public function scopes()
	{
		return [

		];
	}

	public function init()
	{

	}
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{remote_sales_orders}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return [
			['id, item_id, date, user, company, sum, address, score, code, activated, discont, fio', 'safe']
		];
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return [
			'item' => array(self::BELONGS_TO, 'Item', 'item_id')
		];
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'date' => 'Дата',
			'item_id' => 'item_id',
			'user' => 'Юзер',
			'company' => 'Компания',
			'sum' => 'Сумма',
			'address' => 'Адрес',
			'score' => 'Очки',
			'code' => 'Код',
			'activated' => 'Активирован?',
			'discont' => 'Номер дисконтной карты',
            'fio' => 'ФИО'
		);
	}

	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * Очки продавцов
	 *
	 * @param $sum
	 * @return int
	 */
	public function getScores($sum)
	{
		$json = file_get_contents(self::JSON_SCORE_FILE_PATH);
		$arr = json_decode($json, true);
		foreach($arr as $key => $value){
			if($sum > $value[0] && $sum <= $value[1]){
				return (int)$key;
			}
		}
		return false;
	}

	/**
	 * Возвращает уникальный код заказа
	 *
	 * @return string
	 */
	public function getCode()
	{
		return rand(10,99) . rand(10,99) . substr(time(), -8);
	}

}
