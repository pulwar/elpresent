<?php

/**
 * Class Event
 * @property int $id
 * @property int $parent_id
 * @property int $enabled
 * @property string $title
 * @property string $description
 * @property Item[] $items
 * @property Event[] $child
 * @method Event with() with() prepares join
 * @method Event enabled() enabled() returns only enabled items
 * @method Event[] findAll() findAll() returns items
 */
class Event extends CActiveRecord
{
    /**
     * @param string $class
     * @return Event
     */
    public static function model($class = __CLASS__)
    {
        return parent::model($class);
    }


    public function tableName()
    {
        return '{{events}}';
    }

    public function scopes()
    {
        return [
            'enabled' => [
                'condition' => 'enabled=1'
            ]
        ];
    }

    public function rules()
    {
        return [
            ['title, description', 'required'],
            ['parent_id', 'numerical', 'allowEmpty' => true],
            ['enabled', 'in', 'range' => [0, 1]]
        ];
    }

    public function relations()
    {
        return [
            'items' => [self::HAS_MANY, 'Item', 'event_id'],
            'child' => [self::HAS_MANY, 'Event', 'parent_id'],
            'parent' => [self::BELONGS_TO, 'Event', 'parent_id'],
        ];
    }

    public function search()
    {
        $this->setScenario('search');
        $this->attributes = $_GET['Event'];

        return new CActiveDataProvider($this);
    }

    public function attributeLabels()
    {
        return [
            'title' => 'Наименование события',
            'description' => 'Описание',
            'parent_id' => 'Родительский раздел',
            'enabled' => 'Показывать в списке',
        ];
    }

    public function __toString()
    {
        return $this->title;
    }


    public static function formSettings()
    {
        return [
            'title'=>'',

            'elements'=>[
                'title' => [
                    'type' => 'text',
                ],
                'parent_id' => [
                    'type' => 'dropdownlist',
                    'items' => CHtml::listData(Event::model()->findAll(), 'id', 'title'),
                    'prompt'=>'Родительский раздел:',
                ],
                'description' => [
                    'type' => 'ext.tinymce.TinyMce',
                    'fileManager' => [
                        'class' => 'ext.elFinder.TinyMceElFinder',
                        'connectorRoute'=>'admin/elfinder/connector',
                    ],
                ],
                'enabled'=> [
                    'type'=>'checkbox',
                ]
            ],

            'buttons'=>[
                'save'=>[
                    'type'=>'submit',
                    'label'=>'Сохранить',
                ],
            ],
        ];
    }
}
