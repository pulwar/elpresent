<?php

/**
 * Class GalleryAlbum
 * @property int $id
 * @property int $title
 * @property string $date
 */
class GalleryAlbum extends CActiveRecord
{

    public function tableName()
    {
        return '{{gallery_album}}';
    }

}