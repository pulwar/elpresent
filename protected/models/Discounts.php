<?php


class Discounts extends CActiveRecord
{
    public $date;
    public $alias;
    public $title;
    public $count_cods;
    public $time_finish;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{discounts}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('title, text, address, tel, price, tiser, hide, order_items, category_id, partner_id, time_finish', 'required'),
            array('count_cods', 'required', 'on' => 'create'),
            array('count_cods', 'safe', 'on' => 'update'),
            array('alias', 'required', 'on' => 'update'),
            array('count_cods', 'numerical', 'integerOnly' => true),
            array('price', 'numerical', 'integerOnly' => false),
            array('category_id', 'numerical', 'integerOnly' => true),
            array('partner_id', 'numerical', 'integerOnly' => true),
            array('hide', 'numerical', 'integerOnly' => true),
            array('order_items', 'numerical', 'integerOnly' => true),
            array('alias', 'match', 'pattern' => '/^[A-z\-0-9]+$/'),
            array('title', 'length', 'max' => 255),
            array('title, alias', 'safe', 'on' => 'search'),
            array('img_small', 'file',
                'allowEmpty' => true,
                'maxFiles' => 1,
                'maxSize' => 1024 * 250 * 100,
                'minSize' => 1,
                'types' => 'jpg, jpeg, png, gif',
                'tooLarge' => 'Вы загружаете файл слишком большого размера',
                'tooSmall' => 'Вы загружаете файл слишком маленького размера',
                'tooMany' => 'Вы загружаете слишком большое количество файлов',
                'wrongType' => 'Неправильный тип файла',
                'wrongMimeType' => 'Неправильный MIME-тип файла',
            ),
            array('img_big', 'file',
                'allowEmpty' => true,
                'maxFiles' => 1,
                'maxSize' => 1024 * 250 * 100,
                'minSize' => 1,
                'types' => 'jpg, jpeg, png, gif',
                'tooLarge' => 'Вы загружаете файл слишком большого размера',
                'tooSmall' => 'Вы загружаете файл слишком маленького размера',
                'tooMany' => 'Вы загружаете слишком большое количество файлов',
                'wrongType' => 'Неправильный тип файла',
                'wrongMimeType' => 'Неправильный MIME-тип файла',
            ),
        );
    }

    public function relations()
    {
        return array(
            'category' => array(self::BELONGS_TO, 'Category', 'category_id'),
            'partner' => array(self::BELONGS_TO, 'Partner', 'partner_id'),
            'code' => array(self::HAS_MANY, 'DiscountsCods', 'discount_id')
        );
    }


    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array
        (
            'id' => 'Id',
            'title' => 'Заголовок',
            'text' => 'Текст',
            'category_id' => 'Категории',
            'partner_id' => 'Партнер',
            'price' => 'Размер скидки',
            'date' => 'Дата',
            'alias' => 'Алиас',
            'tiser' => 'Тизер',
            'img_small' => 'Картинка превью',
            'img_big' => 'Основная картика',
            'hide' => 'Скрывать?',
            'order_items' => 'Порядок',
            'count_cods' => 'Количество кодов',
            'address' => 'Адрес проведения',
            'tel' => 'Телефоны',
            'time_finish' => 'Воспользоваться купоном до',
        );
    }

    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('alias', $this->title, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function beforeSave()
    {
        if(empty($this->alias)){
            $this->alias = text::getAliasByStr($this->title);
        }

        $this->date = time();
        $this->time_finish = strtotime($this->time_finish);
        return parent::beforeSave();
    }


    public function afterSave()
    {
        if(!empty($this->count_cods)){
            DiscountsCods::model()->deleteAll('discount_id=:discount_id', [':discount_id' => $this->id]);
            for($i = 0; $i < $this->count_cods; $i++){
                $model= new DiscountsCods();
                $model->code = time() . mt_rand(1,9) . $i;
                $model->discount_id = $this->id;
                $model->save();
            }
            Yii::app()->cache->flush();
        }
        return parent::afterSave();
    }

    public function beforeDelete()
    {
        DiscountsCods::model()->deleteAll('discount_id=:discount_id', [':discount_id' => $this->id]);
        return parent::beforeDelete();
    }

    public function filesRemove($img_str, $path){
        if (!empty($img_str)) {
            $file_paths = explode('|', $img_str);
            foreach ($file_paths as $file_path) {
                if (is_file($path . DIRECTORY_SEPARATOR . $file_path)) {
                    @unlink($path . DIRECTORY_SEPARATOR . $file_path);
                }
            }
        }
        return true;
    }

    public function getIdsCategory()
    {
        $parent_ids_array = [];
        $category = Category::model()->findBySlug('discounts');
        $category_id = $category->id;
        $parent_ids = Category::model()->findAll(
            [
                'select'=>'id',
                'condition'=>'parent_id=:parent_id',
                'params'=> [':parent_id' => $category_id],
            ]
        );

        foreach($parent_ids as $parent_id){
            $parent_ids_array[] = $parent_id->id;
        }

        return (array)$parent_ids_array;
    }

    public function countCods($id)
    {
        $key_cash = 'count_cods_' . $id;
        if(!$result = Yii::app()->cache->get($key_cash)){
            $result = DiscountsCods::model()->count(array(
                'select' => 'id',
                'condition' => 'discount_id=:discount_id',
                'params' => array(':discount_id' => $id),
            ));
            Yii::app()->cache->set($key_cash, $result);
        }

        return $result;
    }

    public function getCategoriesByIds($view = 'array')
    {
        $categories = null;
        $root = Category::model()->findBySlug('discounts');
        $category_id = $root->id;
        $parents_categories =  Category::model()->findAll(
            [
                'select'=>'*',
                'condition'=>'parent_id=:parent_id',
                'params'=> [':parent_id' => $category_id],
            ]
        );

        if($view == 'array'){
            foreach ($parents_categories as $category){
                $categories[$category->id] = $category->title;
            }
        }else{
            $categories = $parents_categories;
        }


        return $categories;
    }

    public function getPartnersInMinsk()
    {
        $partners_list =[];
        $partners = Partner::getPartnerListByCityId(1);
        foreach($partners as $partner){
            $partners_list[$partner->id] = $partner->name;
        }
        return (array)$partners_list;
    }

}
