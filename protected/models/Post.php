<?php

class Post extends CActiveRecord
{

    public static function model($className = __CLASS__)
    {
        $model = parent::model($className);
        return $model;
    }

    public function tableName()
    {
        return '{{post}}';
    }

    public function rules()
    {
        return array(
            array('body', 'absoluteLinksValidator')
        );
    }

    public function relations()
    {
        return array
        (
        );
    }

    public function attributeLabels()
    {
        return array
        (
            'id' => _('#'),
            'subject' => _('Тема письма'),
            'body' => _('Содержание письма'),
            'schedule' => _('Дата рассылки'),
            'sent' => _('Состояние отправки'),
            'enabled' => _('Включена?'),
            'actions' => _('Действия')
        );
    }

    public function absoluteLinksValidator($attribute)
    {
        $body = "" . $this->$attribute;


        $search = array('"', "'", '&quot;', '&#39;', '&quot', '&#39');
        $body = str_replace($search, '', $body);
        $body = preg_replace('/\s+|&nbsp;|&#160;|&nbsp|&#160/', '', $body);
        $patternHref = 'href=[^h]';
        $patternSrc = 'src=[^h]';
        if (preg_match("#$patternHref|$patternSrc#i", $body))
        {
            $this->addError($attribute, 'В теле сообщения обнаружены относительные либо неккоректные ссылки, которые не будут работать у пользователей при просмотре почты');
            return;
        }
        if (mb_strlen($body, 'UTF-8') > 50000)
        {
            $this->addError($attribute, 'Недопустимое количество символов в теле сообщения');
            return;
        }

    }

}
