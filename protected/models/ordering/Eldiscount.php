<?php


class Eldiscount extends CActiveRecord
{
    const DIVABY = 1;
    const SVADBABY = 2;

    static public function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    public function tableName()
    {
        return '{{eldiscount}}';
    }

    public function rules()
    {
        return array();
    }

    public function attributeLabels()
    {
        return array();
    }


    public function relations()
    {
        return array(
            'users' => array(self::MANY_MANY, 'User',
            '{{user_eldiscount}}(user_id, eldiscount_id)'),
        );
    }

    public static function getEldiscountId($discountKey)
    {
        $eldiscount = Eldiscount::model()->find('discount_key=:discountKey', array(':discountKey' => $discountKey));

        return $eldiscount ? $eldiscount->id:0;
    }

    public static function getElPercent($discountKey)
    {
        $eldiscount = Eldiscount::model()->find('discount_key=:discountKey', array(':discountKey' => $discountKey));

        return $eldiscount ? $eldiscount->percent:0;
    }
}