<?php

/**
 * Class Cart
 */
class Cart
{
    /** @var Cart|null $instance */
    private static $instance = null;
    /** @var Item[] $items */
    private $items;
    /** @var  array $options */
    private $options;

    private $maxPosNum = 0;

    /**
     * Cart constructor.
     */
    private function __construct()
    {
        $session = Yii::app()->getSession();
        /** @var self $cart */
        $cart = $session->get('cart');
        if(!empty($cart)) {
            /** @var array $criteria */
            $criteria = new CDbCriteria();
            $criteria->addInCondition('t.id', array_keys($cart));
            $items = Item::model()->resetScope(true)->findAll($criteria);
            foreach($items as $item) {
                $this->items[$item->id] = $item;
                $this->options[$item->id] = $cart[$item->id];
                $this->maxPosNum = $this->maxPosNum < $cart[$item->id]['position_num'] ? $cart[$item->id]['position_num'] : $this->maxPosNum;
            }
        } else {
            $session->add('cart', []);
        }
        self::$instance = $this;
    }


    /**
     * Init method
     * @return Cart|null
     */
    public static function getCart()
    {
        if (self::$instance === null) {
            return new self();
        }

        return self::$instance;
    }

    /**
     * @param int  $itemId
     * @param int  $optionId
     * @param bool $type
     * @throws \Exception
     */
    public function addItemById($itemId, $optionId, $type = false)
    {
        if ($this->checkDigitalCertificate()) {
            $this->removeAll();
        }
        /** @var CHttpSession $session */
        $session = Yii::app()->getSession();
        $item = Item::model()->findByPk($itemId);
        if(!empty($item) && $item instanceof Item) {
            $this->items[$item->id] = $item;
            $this->options[$item->id] = ['optionId' => $optionId, 'type' => $type, 'position_num' => ++$this->maxPosNum];

            $sessionCart = $session->get('cart');
            $sessionCart[$item->id] = $this->options[$item->id];
            $session->add('cart', $sessionCart);
        } else {
            throw new \Exception("This id - $itemId is invalid.");
        }
    }

    /**
     * Remove item from cart.
     * @throws \Exception
     */
    public function removeItemById($itemId)
    {
        /** @var CHttpSession $session */
        $session = Yii::app()->getSession();
        if(isset($this->items[$itemId])) {
            unset($this->items[$itemId]);
            unset($this->options[$itemId]);

            $sessionCart = $session->get('cart');
            unset($sessionCart[$itemId]);
            $session->add('cart', $sessionCart);
        } else {
            throw new Exception("Id $itemId, not found in cart!");
        }
    }

    /**
     * Return cart in array format
     */
    public function toArray()
    {
        $idToOptions = $this->getIdToOptions();
        return empty($idToOptions)?[]:$idToOptions;
    }

    /**
     * Return cart to Json format
     * @return string
     */
    public function toJson()
    {
        return json_encode($this->toArray());
    }

    /**
     * Get map itemIs => optionId
     * @return array
     */
    public function getIdToOptions()
    {
        $idToOption = [];
        foreach ($this->options as $id => $option) {
            $idToOption[$id] = $option['optionId'];
        }

        return $idToOption;
    }

    /**
     * @return bool
     */
    public function checkDigitalOrder()
    {
        if ($this->getCountItems() == 1 && $this->checkDigitalCertificate()) {
            return true;
        }

        return false;
    }

    protected function checkDigitalCertificate()
    {
        foreach ($this->options as $id => $option) {
            if ($option['type'] == 'digital') {
                return true;
            }
        }

        return false;
    }

    /**
     * Return all items in cart.
     * @return mixed
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param int $itemId
     * @return mixed
     */
    public function getOptionByItemId($itemId)
    {
        if (isset($this->options[$itemId])) {
            return $this->options[$itemId]['optionId'];
        }
    }

    /**
     * @param int $itemId
     * @return mixed
     */
    public function geTypeByItemId($itemId)
    {
        if (isset($this->options[$itemId])) {
            return $this->options[$itemId]['type'];
        }
    }

    /**
     * Calculate Sum Prices from cart
     * @return float
     */
    public function calcCartPriceSum()
    {
        $sum = 0.0;
        foreach ($this->items as $item) {
            /** @var Item $item */
            $sum += $item->getPrice($this->getOptionByItemId($item->id));
        }

        return $sum;
    }

    /**
     * Calculation sum price by discount by each product.
     * @return float
     */
    public function calcDiscountPriceSum()
    {
        $sum = 0.0;
        foreach ($this->items as $item) {
            /** @var Item $item */
            $sum += $item->getRoundedPrice($this->getOptionByItemId($item->id));
        }

        return $sum;
    }

    /**
     * Get Item type. Using for one-buy.
     * @param int $itemId
     * @return string
     */
    public function getTypeItemById($itemId)
    {
        if (isset($this->options[$itemId])) {
           return  empty($this->options[$itemId]['optionId'])?'general':$this->options[$itemId]['optionId'];
        }

    }

    /**
     * @return int
     */
    public function getCountItems()
    {
        return count($this->items);
    }

    /**
     * Static interface. Delegate on removeAll.
     */
    public static function clearCart()
    {
        self::getCart()->removeAll();
    }

    /**
     * Remove All items and Options
     */
    private function removeAll()
    {
        $this->items = [];
        $this->options = [];
        Yii::app()->getSession()->remove('cart');
    }

    /**
     * @return int
     */
    public function getMaxPosNum()
    {
        return $this->maxPosNum;
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    public function __sleep()
    {
    }

    public function __wakeup()
    {
    }

    private function __clone()
    {
    }
}
