<?
class Discount extends CActiveRecord
{
    const STATUS_ACTIVE = 'active';
    const STATUS_NOTACTIVE = 'not active';
    private static $elDiscountPercent = 3;


    static public function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    public function tableName()
    {
        return '{{discount}}';
    }

    public function rules()
    {
        return array
        (
            array('code', 'required'),
            array('code', 'unique')
        );
    }

    public function attributeLabels()
    {
        return array
        (
            'code' => _('Код'),
            'percent' => _('Процент'),
            'status' => _('Статус')
        );
    }

    public function getFormData()
    {
        return array
        (
            'code' => 'InputTextType',
            'percent' => 'InputTextType'
        );
    }

    public function getViewData()
    {
        return array
        (
            'code' => 'SimpleTextType',
            'percent' => 'SimpleTextType',
            'status' => 'SimpleTextType'
        );
    }

    public static function searchCode($code)
    {
        return Discount::model()->count(array(
             'condition' => 'code=:code',
             'params' => array(':code' => $code)
        ));
    }

    public static function getUserDiscount($userId)
    {
        return (self::getUserDiscountElpresent($userId) > User::getMaxElDiscount($userId)) ? self::getUserDiscountElpresent($userId) : User::getMaxElDiscount($userId);
    }

    public static function getPercent()
    {
        return self::$elDiscountPercent;
    }

    public static function getUserDiscountElpresent($userId)
    {
        if (!$userId)
            return 0;

        $criteria = new CDbCriteria;
        $criteria->condition = 'user_id = ' . $userId . ' AND is_paid = 1';
        $orderCount = Order::model()->count($criteria);

        if ($orderCount >= 6 && $orderCount < 12)
        {
            return 3;
        }

        if ($orderCount >= 12 && $orderCount < 24)
        {
            return 5;
        }

        if ($orderCount >= 24 && $orderCount < 48)
        {
            return 7;
        }

        if ($orderCount >= 48 && $orderCount < 96)
        {
            return 10;
        }

        if ($orderCount >= 96)
        {
            return 12;
        }
    }
}