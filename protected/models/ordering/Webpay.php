<?php

/**
 * Class Webpay
 *
 * @property integer $id
 * @property string $wsb_order_num
 * @property integer $wsb_seed
 * @property string $wsb_signature
 * @property integer $wsb_total
 * @property integer $wsb_tid
 *
 * @property Order $order
 */
class Webpay extends CActiveRecord
{

    const REJECTED = 2;
    const PENDING = 3;
    const RETURNED = 5;
    const SYSTEM = 6;
    const AUTHORIZATION_TIMEOUT = 7;

    public static $wsb_storeid = 787373493;
    public static $wsb_test = 0;
    public static $wsb_currency_id = 'BYR';
    public static $SecretKey = 'df4b8c0b4bb1f18dsd4d';
    public static $url = 'https://billing.webpay.by';
    public static $wsb_version = 2;
    public static $wsb_store = 'elpresent.by';

    public static $username = 'elpresent.by';
    public static $password = 'a0c48f060ee22ea16ca710d9af50f1f9';
    public static $status = array(
        2 => 'Отклоненная',
        3 => 'В обработке',
        5 => 'Возвращенная',
        6 => 'Системная',
        7 => 'Сброшенная после авторизации',
    );

    public $wsb_return_url;
    public $wsb_cancel_return_url;
    public $wsb_notify_url;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{webpay}}';
    }

    public function rules()
    {
        return array(
            array('wsb_order_num', 'unique'),
            array('wsb_order_num', 'length', 'max' => 32),
            array('wsb_signature', 'length', 'max' => 40),
        );
    }

    /**
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }


    public function relations()
    {
        return array(
            'order' => array(self::BELONGS_TO, 'Order', 'order_id'),
        );
    }

}
