<?php

/**
 * Class Order
 * TODO: this is very fat model... need to be splited in future
 *
 * @property integer      $id
 * @property integer      $item_id
 * @property integer      $activated_item
 * @property string       $name
 * @property string       $lname
 * @property string       $phone
 * @property boolean      $sended
 * @property string       $sended_date
 * @property boolean      $delivered
 * @property string       $delivered_date
 * @property string       $code
 * @property string       $adr
 * @property string       $texture
 * @property string       $message
 * @property string       $bow
 * @property string       $comment
 * @property integer      $price
 * @property integer      $saled_price
 * @property string       $email
 * @property string       $recivier_name
 * @property string       $recivier_lname
 * @property string       $recivier_adr
 * @property string       $recivier_phone
 * @property string       $recivier_email
 * @property string       $recivier_date
 * @property string       $recivier_activation_date
 * @property string       $order_date
 * @property integer      $discount
 * @property boolean      $delivery
 * @property integer      $user_id
 * @property integer      $is_delivery_general
 * @property boolean      $is_express_delivery
 * @property boolean      $is_express_delivery_recipient
 * @property boolean      $is_thematic_delivery
 * @property integer      $thematic_delivery_price
 * @property string       $thematic_delivery_name
 * @property integer      $flower_id
 * @property integer      $card_id
 * @property integer      $payment_type_id
 * @property integer      $item_quantity
 * @property string       $fullname
 * @property string       $order_number
 * @property integer      $is_paid
 * @property string       $is_paid_date
 * @property boolean      $is_paid_notify
 * @property string       $order_knowwhere
 * @property string       $recipient_firstname
 * @property string       $recipient_lastname
 * @property string       $recipient_address
 * @property string       $recipient_gift_date
 * @property string       $recipient_phone
 * @property string       $recipient_desired_date
 * @property string       $recipient_desired_time_from
 * @property string       $recipient_desired_time_to
 * @property string       $gift_date
 * @property string       $gift_time_from
 * @property string       $gift_time_to
 * @property string       $payer_adr
 * @property string       $payer_date
 * @property string       $payer_time_from
 * @property string       $payer_time_to
 * @property integer      $flower_quantity
 * @property integer      $card_quantity
 * @property boolean      $visibility
 * @property boolean      $activated
 * @property boolean      $activated_notify
 * @property string       $referer
 * @property string       $keywords
 * @property integer      $is_fast_order
 * @property integer      $is_remote_order
 * @property integer      $city_id
 * @property boolean      $is_canceled
 * @property boolean      $is_processed
 * @property float        $delivery_price
 * @property float        $delivery_free_price
 * @property Item         $item
 * @property User         $user
 * @property Flower       $flower
 * @property Card         $card
 * @property Webpay       $webpay
 * @property Bepaid       $bepaid
 * @property PaymentType  $payment_type
 * @property City         $city
 * @property City         $user_cash
 * @property OrderItems[] $orderItems
 */
class Order extends CActiveRecord
{
    public $message;

    public $discounted_price = null;
    public $discount_describe = null;

    public $totalOrderPrice = 0;
    public $totalOrderPriceDisc = 0;

    public $giftPriceDisc = 0;
    public $flowerPriceDisc = 0;
    public $cardPriceDisc = 0;

    public $deliveryType = 1;
    public $sendToRecipient = false;
    public $paymentType = 1;

    public $easypay_card;

    public $toy_price;
    public $toy_title;

    const VISIBLE = 1;
    const HIDDEN = 2;

    const ACTIVE = 1;
    const INACTIVE = 2;

    const DELIVERY_NONE = 0;
    const DELIVERY_GENERAL = 1;
    const DELIVERY_EXPRESS = 2;

    /**
     * Цена обычной достввки
     */
    const DELIVERY_GENERAL_PRICE = 6;

    /**
     * Цена экспресс достввки
     */
    const DELIVERY_EXPRESS_PRICE = 9;

    const DELIVERY_FREE_PRICE = 0;

    const PAYMENT_CASH = 1;
    const PAYMENT_EASYPAY = 2;
    const PAYMENT_WEBPAY = 2;
    const PAYMENT_BEPAID = 6;

    const TYPE_BASIC = 0;
    const TYPE_DIGITAL = 1;

    const PRICE_FOR_FREE_DELIVERY = 120;
    const PRICE_FOR_FREE_EXPRESS_DELIVERY = 350;

    /**
     * Pseudo constuctor
     */
    public function init()
    {
        if ($this->isNewRecord) {
            $this->city_id = Yii::app()->request->getQuery('_city')->id;
        }
    }

    /**
     * @param string $className
     *
     * @return Order
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return '{{order}}';
    }

    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->condition = 'email != "" AND  Length (email) > 8';
        $criteria->group = 'email';
        $criteria->order = 'id';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => 100,
            ),
        ));
    }

    /**
     * @return array of validation rules
     */
    public function rules()
    {
        return [
            ['email, recivier_email', 'email', 'allowEmpty' => true],
            ['phone,email,paymentType', 'required', 'on' => ['insert', 'bind', 'delivery', 'recipient_delivery', 'digital']],
            ['name,phone,email', 'required', 'on' => 'fast_order'],
            ['adr,gift_date', 'required', 'on' => 'delivery'],
            ['recivier_name, recivier_email, recivier_phone', 'required', 'on' => 'activation'],
            ['recipient_address, recipient_gift_date', 'required', 'on' => 'recipient_delivery'],
            ['code', 'unique', 'allowEmpty' => true],
            //['phone', 'length', 'min'=>12, 'max'=>25],
            // specify all safe-on-bind properties
            ['name,fname,email,adr', 'safe', 'on' => 'bind_session'],
            ['name,fname,email,adr,gift_date,comment,paymentType,deliveryType,sendToRecipient', 'safe', 'on' => 'bind'],
            ['thematic_delivery_name,flower_id,card_id,toy_id,item_id,bow,texture,message', 'safe', 'on' => 'bind'],
            ['recipient_firstname,recipient_lastname,recipient_address,recipient_phone,recipient_gift_date', 'safe', 'on' => 'bind'],
            ['option_id, kit_ids', 'unsafe'],
            ['phone', 'checkPhone']
        ];
    }

    /**
     * Метод проверяет силу пароля
     * Этот метод является валидатором 'passwordStrength' для метода rules().
     */
    public function checkPhone($attribute,$params)
    {
        if((strpos($this->$attribute, '+375 ') !== false) && (strlen($this->$attribute)!=14))
            $this->addError($attribute, 'Телефон заказавшего слишком короткий (Минимум: 12 симв.).');
    }


    public function getVerboseStatus()
    {
        if (!$this->getIsCorrectDate()) {
            return 'Срок действия истек';
        }
		
		if($this->payment_type_id == 1) {
            return 'В обработке';
        }


        if (!$this->is_paid && !$this->delivered) {
            return 'Ожидает оплаты';
        }

        return $this->activationStatus();
    }

    /**
     * Return generally activation status for this order.
     * @return string
     */
    public function activationStatus()
    {
        $arrayStatus = [];
        foreach ($this->orderItems as $orderItem) { 
            /** @var OrderItems $orderItem */
            if($orderItem->getIsActivated()) {
                $arrayStatus[] = true;
            } else {
                $arrayStatus[] = false;
            }
        }
        $uniqArr = array_unique($arrayStatus);

        return count($uniqArr) == 1 ? (array_shift($uniqArr) == true ? 'Все позиции активирована' : 'Ни одна из позиций не активирована') : 'Есть неактивированные позиции'; 
    }

    public function getIsCorrectDate()
    {
        return !(strtotime('- 3 MONTH') - 86400 > strtotime($this->order_date));
    }

    /**
     *
     * @return array of relations
     */
    public function relations()
    {
        return array(
            'item' => array(self::BELONGS_TO, 'Item', 'item_id'),
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
            'flower' => array(self::BELONGS_TO, 'Flower', 'flower_id'),
            'card' => array(self::BELONGS_TO, 'Card', 'card_id'),
            'payment_type' => array(self::BELONGS_TO, 'PaymentType', 'payment_type_id'),
            'webpay' => array(self::HAS_ONE, 'Webpay', 'order_id'),
            'bepaid' => array(self::HAS_ONE, 'Bepaid', 'order_id'),
            'city' => [self::BELONGS_TO, 'City', 'city_id'],
            'option' => array(self::BELONGS_TO, 'Option', 'option_id'),
            'orderItems' => array(self::HAS_MANY, 'OrderItems', 'order_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => '№ п/п',
            'order_number' => 'Номер заказа',
            'order_date' => 'Дата заказа',
            'code' => '№ активационной карты',
            'item_id' => 'Наименование подарка',
            'discount' => 'Скидка',
            'delivery' => 'Доставка',
            'is_delivery_general' => 'Обычная доставка',
            'is_express_delivery' => 'Экспресс доставка',
            'is_thematic_delivery' => 'Тематическая доставка',
            'is_express_delivery_recipient' => 'Вручить лично',
            'price' => 'Цена',
            'saled_price' => 'Стоимость заказа со скидкой',
            'name' => 'Имя заказавшего',
            'lname' => 'Фамилия заказавшего',
            'phone' => 'Телефон заказавшего',
            'comment' => 'Комментарий',
            'gift_time_from' => 'Желаемое время доставки',
            'gift_time_to' => 'Желаемое время доставки',
            'payer_adr' => 'Адрес оплаты заказа',
            'payer_date' => 'Желаемая дата оплаты',
            'payer_time_from' => 'Желаемое время оплаты',
            'payer_time_to' => 'Желаемое время оплаты',
            'flower_id' => 'Цветы',
            'card_id' => 'Открытка',
            'email' => 'Email заказавшего',
            'paymentType' => 'Способ оплаты',
            'user_id' => 'Юзер',
            'payment_type_id' => 'Способ оплаты',
            'texture_img' => 'Текстура',
            'bow_img' => 'Лента',
            'recivier_activation_date' => 'Дата активации',
            'recivier_name' => 'Имя активировавшего',
            'recivier_lname' => 'Фамилия активировавшего',
            'recivier_phone' => 'Телефон активировавшего',
            'recivier_email' => 'Email активировавшего',
            'recivier_date' => 'Дата указанная при активации',
            'adr' => 'Адрес доставки при заказе',
            'item' => 'Подарок',
            'recipient_desired_date' => 'Желаемая дата вручения подарка',
            'recipient_desired_time_from' => 'Желаемое время вручения подарка',
            'recipient_desired_time_to' => 'Желаемое время вручения подарка',
            'is_paid' => 'Оплачено',
            'delivered' => 'Получено',
            'sended' => 'Отправлено',
            'recipient_firstname' => 'Имя получателя подарка',
            'recipient_lastname' => 'Фамилия получателя подарка',
            'recipient_address' => 'Адрес получателя подарка',
            'recipient_phone' => 'Контактный телефон получателя подарка',
            'recipient_gift_date' => 'Желаемая дата вручения подарка',
            'gift_date' => 'Дата доставки',
            'flower_quantity' => 'Количество цветов',
            'card_quantity' => 'Количество открыток',
            'order_knowwhere' => 'Источник',
            'desired_deliv_time' => 'Желаемое время доставки',
            'desired_pay_time' => 'Желаемое время оплаты',
            'user_login' => 'Логин',
        );
    }

    public function behaviors()
    {
        return [
            ['class' => 'application.components.payment.PaymentNotifierBehavior'],
        ];
    }

    public function onOrderPaid($event)
    {
        $this->raiseEvent('onOrderPaid', $event);
    }

    /**
     * @return $this
     */
    public function setPaid()
    {
        $this->is_paid = true;
        //$this->is_paid_date = date('Y-m-d H:i:s');

        // create event
        $event = new CModelEvent($this);
        $this->onOrderPaid($event);

        return $this;
    }

    /**
     * Binds properties
     *
     * @param CHttpRequest $request
     *
     * @return $this
     */
    public function bindRequest(CHttpRequest $request)
    {
        if (!$request->isPostRequest) {
            return $this;
        }

        $prevScenario = $this->getScenario();
        $this->setScenario('bind');
        $this->attributes = $request->getParam('Order');
        $this->setScenario($prevScenario);

        // bind delivery info
        if ($this->thematic_delivery_name) {
            $item = Item::model()->resetScope()->findByPk($this->thematic_delivery_name);
            if ($item) {
                $this->thematic_delivery_price = $item->getPrice('', $this->city);
            }
        }
        $this->flower_quantity = $this->flower_id ? 1 : 0;

        return $this;
    }

    /**
     * @return $this
     */
    public function detectScenario()
    {
        if ($this->type == self::TYPE_DIGITAL) {
            $this->setScenario('digital');

            return $this;
        }

        $city = City::getCity(isset($_SESSION['_city']) ? $_SESSION['_city'] : 0);
        if ($this->sendToRecipient || Item::isThematicItem($city, $this->deliveryType)) {
            $this->setScenario('recipient_delivery');
        } else {
            if (in_array(intval($this->deliveryType), [self::DELIVERY_GENERAL, self::DELIVERY_EXPRESS], false)) {
                $this->setScenario('delivery');
            }
        }

        return $this;
    }

    /**
     * Return total price by this order
     * @param int $sale
     * @return $this
     */
    public function calcTotalPrice($sale = 0)
    {
        $packagePrice = (float) ($this->flower_id ? $this->flower->flower_price : 0);
        $packagePrice += ($this->card_id) ? $this->card->card_price : 0;
        $packagePrice += !empty($this->toy_id) ? $this->toy_price : 0;
        $totalCartSum = (float) Cart::getCart()->calcDiscountPriceSum();
        $deliveryPrice = (float) $this->getPriceForDelivery($this->deliveryType);

        $this->price = ($totalCartSum + $deliveryPrice + $packagePrice);// - (float) $sale;

        if($sale >= $this->price) {
            // вся сумма заказа оплачивается с  бонусного счета, с бонусного счета вычитается сумма заказа
            $this->user_cash = $this->price;
            $this->price = 0;
            // заказ отмечается как оплачен
            $this->setPaid();
        } else {
            // весь бонусный счет обнуляется
            $this->user_cash = (float) $sale;
            $this->price  = $this->price - (float) $sale;
        }

        return $this;
    }

    /**
     * @param array $packing
     *
     * @return $this
     */
    public function preparePacking(array $packing)
    {
        if (!empty($packing['toy'])) {
            $toy = explode('|', $packing['toy']);
        }

        $this->flower_id = empty($packing['flower']) ? null : $packing['flower'];
        $this->card_id = empty($packing['card']) ? null : $packing['card'];
        $this->toy_id = empty($toy[0]) ? null : $toy[0];
        $this->toy_price = empty($toy[1]) ? null : $toy[1];
        $this->toy_title = empty($toy[2]) ? null : $toy[2];
        $this->flower_quantity = 1;
        $this->card_quantity = 1;
        //$this->toy_quantity = 1 ;
        $this->bow = empty($packing['bow']) ? null : ($packing['bow'] . '.png');
        $this->texture = empty($packing['texture']) ? null : ($packing['texture'] . '.png');
        $this->message = empty($packing['message']) ? null : ($packing['message']);

        return $this;
    }

    public function getFullName()
    {
        if (!empty($this->lname) && !empty($this->name)) {
            return $this->lname . ' ' . $this->name;
        }

        if (!empty($this->name)) {
            return $this->name;
        }

        return null;
    }

    public function afterDelete()
    {
        $ip = $_SERVER["REMOTE_ADDR"];
        $time = date('d.m.Y H:i');
        $order_name = $this->name;
        $order_price = $this->price;
        $city_id = $this->city_id;
        $item_id = $this->item_id;
        $order_number = $this->order_number;
        $txt = "$time был удален заказ №: $order_number ($order_name), цена: $order_price, id города: $city_id, id подарка: $item_id, ip: $ip \n";
        $log_file = Yii::getPathOfAlias('webroot.assets') . '/order-delete.log';
        error_log($txt, 3, $log_file);
        return parent::afterDelete();

    }

    //code means for correct saving date in database
    public function beforeSave()
    {
        if ($this->getIsNewRecord()) {
            $city = City::getCity(isset($_SESSION['_city']) ? $_SESSION['_city'] : 0);
            $this->delivery_price = (float) $this->getPriceForDelivery($this->deliveryType);
            $this->delivery_free_price = self::PRICE_FOR_FREE_DELIVERY;
            // set order details
            $this->order_number = $this->generateUniqueOrderNumber();
            $this->order_date = date('Y-m-d H:i:s');
            if ('fast_order' !== $this->getScenario()) {
                $this->paymentType = intval($this->paymentType);
                $this->deliveryType = intval($this->deliveryType);
                // set payment details
                $this->payment_type_id = $this->paymentType;

                // prepare delivery info
                $this->is_delivery_general = $this->deliveryType == self::DELIVERY_GENERAL;
                $this->is_express_delivery = $this->deliveryType == self::DELIVERY_EXPRESS;
                if($this->deliveryType == self::DELIVERY_EXPRESS) {
                    $this->delivery_free_price = self::PRICE_FOR_FREE_EXPRESS_DELIVERY;
                }
                $this->is_express_delivery_recipient = $this->deliveryType == self::DELIVERY_EXPRESS && $this->sendToRecipient;
                $this->is_thematic_delivery = Item::isThematicItem($city, $this->deliveryType);

                // prepare dates
                // todo: replace with filters
                foreach (array('recipient_desired_date', 'gift_date', 'payer_date', 'recipient_gift_date') as $field) {
                    if (preg_match('#\d{2}\.\d{2}\.\d{4}#', $this->$field)) {
                        $this->$field = (new \DateTime($this->$field))->format('Y-m-d');
                    }
                }
            } else {
                $this->lname = '';
            }

            $this->item_quantity = $this->item_quantity ?: 1;
        }
        return parent::beforeSave();
    }

    /**
     * Return price delivery by type
     * @param $deliveryType
     * @return int
     * @throws CHttpException
     */
    public function getPriceForDelivery($deliveryType)
    {
        if ($this->type == self::TYPE_DIGITAL) {
            return 0;
        }

        $prices = [
            self::DELIVERY_FREE_PRICE,
            // если цена товара > PRICE_FOR_FREE_DELIVERY цена доставки = 0
            (Cart::getCart()->calcDiscountPriceSum() >= self::PRICE_FOR_FREE_DELIVERY) ? self::DELIVERY_FREE_PRICE : self::DELIVERY_GENERAL_PRICE,
            (Cart::getCart()->calcDiscountPriceSum() >= self::PRICE_FOR_FREE_EXPRESS_DELIVERY) ? self::DELIVERY_FREE_PRICE : self::DELIVERY_EXPRESS_PRICE,
        ];

        $prices_thematic = self::getPricesThematic();

        if(!empty($prices_thematic) && is_array($prices_thematic)){
            $prices = $prices + $prices_thematic;
        }

        return isset($prices[$deliveryType]) ? $prices[$deliveryType] : -1;
    }

    public function findPreviousOrder()
    {
        if (!$this->email) {
            return null;
        }
        if (!$this->order_date) {
            $this->order_date = date('Y-m-d H:i:s');
        }
        $orderDate = new \DateTime($this->order_date);

        return Order::model()->find('email=:email AND order_date < :current_date', [
            'email' => $this->email,
            'current_date' => $orderDate->format('Y-m-d H:i:s')
        ]);
    }

    public function getTexture_img()
    {
        return $this->texture;
    }

    public function getBow_img()
    {
        return $this->bow;
    }

    public function isDelivered()
    {
        return $this->delivered == 1;
    }

    public function isSent()
    {
        return $this->sended == 1;
    }

    public function generateUniqCode($return = false)
    {
        do {
            $code = implode('', array_map(function () {
                return mt_rand(0, 9);
            }, range(0, 11)));
        } while (0 != Order::model()->countByAttributes(array('code' => $code)));

        $this->code = $code;

        if ($return) {
            return $code;
        }
    }

    public static function generateUniqueOrderNumber()
    {
        do {
            $randOrderNumber = mt_rand(100000, 100000000);
        } while (Order::model()->findByAttributes(array('order_number' => $randOrderNumber)));

        return $randOrderNumber;
    }

    // todo: we can use T() for this stuff
    public static function humanPluralForm($number, $titles = array('заказ', 'заказа', 'заказов'))
    {
        $cases = array(2, 0, 1, 1, 1, 2);
        return $number . " " . $titles[($number % 100 > 4 && $number % 100 < 20) ? 2 : $cases[min($number % 10, 5)]];
    }

    public function formatDate($columnName)
    {
        $timestamp = strtotime($this->$columnName);

        $month = array(
            'January' => "Января",
            'February' => "Февраля",
            'March' => "Марта",
            'April' => "Апреля",
            'May' => "Мая",
            'June' => "Июня",
            'July' => "Июля",
            'August' => "Августа",
            'September' => "Сентября",
            'October' => "Октября",
            'November' => "Ноября",
            'December' => "Декабря"
        );
        $month = $month[date('F', $timestamp)];

        return date('j ' . $month . ' Y, в H:i', $timestamp);
    }

    public static function getNameOption($id)
    {
        $option = Option::model()->findByPk($id);
        if ($option == null) {
            return null;
        } else {
            return $option->name;
        }
    }

    /**
     * Нужно ли выдавать текст о том что магаз не работает
     *
     * @return bool
     */
    public static function isTimeAlert()
    {
        $w = date('w');
        $h = date('G');
        $i = date('i');
        $t = (int)$h . $i;
        // суббота после 17.00 и воскресенье
        if (($w == 6 && $t > 1700) || ($w == 0) || $t > 1800) {
            return true;
        }

        return false;
    }

    /**
     * @return array
     */
    public static function getPricesThematic()
    {
        $prices_thematic = [];
        $city = City::getCity(isset($_SESSION['_city']) ? $_SESSION['_city'] : 0);
        $thematic_delivery_items = PaymentType::getThematicDeliveryOptions($city);
        foreach($thematic_delivery_items as $key => $item){
            $prices_thematic[$item->id] = $item->prices_cache[$city->id];
        }

        return $prices_thematic;
    }

    /**
     * Какой должен быть накопительный балл в зависимости от суммы подарка
     *
     * @param $price
     * @return int
     */
    public static function getCountPoint($price)
    {
        if ($price >= 0 && $price <= 300) {
            $result = $price*0.05;
        } elseif ($price > 300 && $price <= 1000) {
            $result =  $price*0.03;
        } elseif ($price > 1000 ) {
            $result = $price*0.02;
        } 
        return round($result,2);
    }

    /**
     * Save related OrderItems
     * @return bool
     */
    public function saveOrderItems()
    {
        $error = false;
        foreach ($this->orderItems as $orderItem) {
            /** @var OrderItems $orderItem */
            if ($orderItem->save()) {
                $error = true;
            }
        }

        return $error;
    }

    public function calcSumOrderWithoutDiscount()
    {
        $totalSum = 0;
        foreach ($this->orderItems as $orderItem) {
            $totalSum += $orderItem->item_price;
        }

        return $totalSum;
    }

    public function calcSumOrderWithDiscount()
    {
        $totalSum = 0;
        foreach ($this->orderItems as $orderItem) {
            $totalSum += $orderItem->item_price_with_discount;
        }

        return $totalSum;
    }
}
