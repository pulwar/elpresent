<?php

/**
 * @property $id int
 * @property $item_id int
 * @property $item Item
 * @property $name string
 * @property $phone string
 * @property $date string
 * @property $processed bool
 */
class OrderRequest extends CActiveRecord
{

    public static function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    public function tableName()
    {
        return '{{order_request}}';
    }

    public function rules()
    {
        return array(
            array('item_id, phone, name', 'required'),
        );
    }

    public function relations()
    {
        return array(
            'item' => array(self::BELONGS_TO, 'Item', 'item_id'),
        );
    }

    public function init()
    {
        parent::init();
        $this->date = date('Y-m-d H:i:s');
        $this->processed = 0;
    }

    public function afterSave()
    {
        unset(Yii::app()->request->cookies['option_by_city_value']);
        return parent::afterSave();
    }
}