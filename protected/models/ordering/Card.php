<?php

/**
 * Refactor
 * @property integer card_id
 * @property string card_title
 * @property string card_image
 * @property integer card_price
 */
class Card extends CActiveRecord
{
    public static function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    public function tableName()
    {
        return '{{cards}}';
    }

    public function attributeLabels()
    {
        return array
        (
            'card_id' => '#',
            'card_title' => 'Название',
            'card_price' => 'Цена',
            'card_image' => 'Изображение'
        );
    }

    /**
     * @param int $length
     * @return string
     */
    public function generateRandomName($length = 10)
    {
        $characters = "0123456789abcdefghijklmnopqrstuvwxyz";
        $string = "";

        for ($p = 0; $p < $length; $p++)
        {
            $string .= $characters[mt_rand(0, (strlen($characters) - 1))];
        }

        return $string;
    }

    public static function getProvider($pageSize = 12)
    {
        return new CActiveDataProvider('Card', array(
            'pagination' => array(
                'pageSize' => $pageSize
            )
        ));
    }
}
