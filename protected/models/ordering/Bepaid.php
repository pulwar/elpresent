<?php

/**
 * Class Bepaid
 *
 * @property int    $id
 * @property string $tracking_id
 * @property string $token
 * @property string $uid
 * @property string $status
 * @property int    $total
 * @property int    $order_id
 * @property Order  $order
 * @property int    $type
 */
class Bepaid extends CActiveRecord
{
    const TYPE_CARD = PaymentType::BEPAID_CARD;
    const TYPE_HALVA = PaymentType::BEPAID_HALVA;
    public static $ALLOWED_TYPES = [
        self::TYPE_CARD,
        self::TYPE_HALVA,
        //etc..
    ];

    const SUCCESSFUL = 2;
    const FAILED = 3;
    const INCOMPLETE = 5;
    const EXPIRED = 6;   

    public $be_total;
    public $be_signature;

    public static $status = array(
        self::SUCCESSFUL => 'транзакция была обработана успешно',
        self::FAILED => 'транзакция была обработана и отклонена шлюзом',
        self::INCOMPLETE => 'транзакция не завершена, требуется участие торговца',
        self::EXPIRED => 'время обработки транзакции истекло',
    );

    /**
     * @param string $className
     * @return CActiveRecord|mixed
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string
     */
    public function tableName()
    {
        return '{{bepaid}}';
    }

    /**
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    public function relations()
    {
        return array(
            'order' => array(self::BELONGS_TO, 'Order', 'order_id'),
        );
    }
}
