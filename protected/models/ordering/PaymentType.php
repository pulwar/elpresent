<?php


class PaymentType extends CActiveRecord
{
    const COURIER = 1;
    const ASSIST = 2;
    const SETTLEMENT = 3;
    const EASYPAY = 4;
    const WEBPAY = 5;
    const BEPAID_CARD = 6;
    const BEPAID_HALVA = 7;

    /** php5.4 limitation. */
    public static $DIGITAL_PAYMENTS = [
        self::EASYPAY,
        self::WEBPAY,
        self::BEPAID_CARD,
        self::BEPAID_HALVA,
    ];

    public static function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    public function tableName()
    {
        return '{{payment_type}}';
    }

    public static function getByType($type)
    {
        return self::model()->findByPk($type);
    }

    // todo: move to DeliveryType model
    public static function getThematicDeliveryOptions(City $city)
    {
        // id впечатлений, которые не годятся в качестве тематичесской доставки. На данный момент исключаем "Муж на час"
        $excludes = [1801];

        $items = Item::getItemsFromThematicCategory($city);

        foreach($items as $k => $item){
            if(in_array($item->id, $excludes)){
                unset($items[$k]);
            }
        }

        return $items;
    }

    public static function isThematicDeliveryAvailable(City $city = null)
    {
        return $city && $city->isDefault;
    }

    // todo: move to DeliveryType model
    public static function getAvailableDeliveryTypes(City $city = null)
    {
        if ($city && $city->isDefault) {
            return ['Самовывоз', 'Обычная доставка', 'Экспресс доставка', 'Тематическая доставка'];
        }

        return ['Самовывоз', 'Обычная доставка', 'Экспресс доставка'];
    }

    /**
     * @param int|string $paymentTypeId
     * @return bool
     */
    public static function isDigitalPayment($paymentTypeId)
    {
        return in_array(intval($paymentTypeId), self::$DIGITAL_PAYMENTS);
    }
}
