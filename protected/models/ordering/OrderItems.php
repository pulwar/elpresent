<?php

/**
 * Class OrderItemsActivation
 * @property int     $id
 * @property int     $order_id
 * @property int     $item_id
 * @property int     $option_id
 * @property string  $code
 * @property string  $recivier_name
 * @property string  $recivier_lname
 * @property string  $recivier_adr
 * @property string  $recivier_phone
 * @property string  $recivier_email
 * @property string  $recivier_date
 * @property string  $recivier_activation_date
 * @property string  $activated
 * @property string  $activated_notify
 * @property string  $item_title
 * @property float   $discount_percent
 * @property int     $activated_item
 * @property float   $discount_price
 * @property float   $item_price
 * @property float   $item_price_with_discount
 * @property int     $partner_id
 * @property float   $partner_price
 * @property bool    $partner_activated
 * @property string  $partner_activated_date
 * @property Item    $itemObj
 * @property Order   $orderObj
 * @property Item    $activatedItemObj
 * @property Partner $partner
 * @property Option  $optionObj
 * @property array   $group_item_price_data
 * @property float   $activated_item_price
 */
class OrderItems extends CActiveRecord
{
    const ACTIVE = 1;
    const INACTIVE = 2;

    /**
     * @param string $className
     * @return CActiveRecord
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string
     */
    public function tableName()
    {
        return '{{order_items}}';
    }

    /**
     * Added relations
     * @return array
     */
    public function relations()
    {
        return [
            'itemObj' => [self::BELONGS_TO, 'Item', 'item_id'],
            'activatedItemObj' => [self::BELONGS_TO, 'Item', 'activated_item'],
            'orderObj' => [self::BELONGS_TO, 'Order', 'order_id'],
            'optionObj' => [self::BELONGS_TO, 'Option', 'option_id'],
            'partner' => [self::BELONGS_TO, 'Partner', 'partner_id'],
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => 'application.components.behaviors.SerializableFieldsBehaviour',
                'fields' => ['group_item_price_data']
            ],
        ];
    }

    /**
     * @return array of validation rules
     */
    public function rules()
    {
        return [
            ['recivier_name, recivier_email, recivier_phone', 'required', 'on' => 'activation'],
        ];
    }

    public function generateUniqCode($return = false)
    {
        do {
            $code = implode('', array_map(function () {
                return mt_rand(0, 9);
            }, range(0, 11)));
        } while (0 != OrderItems::model()->countByAttributes(array('code' => $code)));

        $this->code = $code;

        if ($return) {
            return $code;
        }
    }

    public static function getUniqCode()
    {
        $orderItem = new self();
        return $orderItem->generateUniqCode(true);
    }

    public function getIsActivated()
    {
        return !empty($this->recivier_phone);
    }

    public function getIsCorrectDate()
    {
        return !(strtotime('- 3 MONTH') - 86400 > strtotime($this->orderObj->order_date));
    }


    public function beforeSave()
    {
        if ($this->getIsNewRecord()) {
            /** @var Item $item */
            $item = Item::model()->resetScope(true)->findByPk($this->item_id);
            if ($item->type == Item::TYPE_ITEM) {
                $city = $this->getCity();
                $pricePartner = $item->getPartnerPriceByOption($city, $this->option_id);
                $pricePartner = array_shift($pricePartner);
                /** @var ItemPartnerLink $pricePartner */
                $this->partner_id = $pricePartner->partner_id;
                $this->partner_price = $pricePartner->partner_price;
                $this->activated_item_price = $this->discount_percent ? $this->item_price_with_discount : $this->item_price;

                $result = [
                    'id' => $item->id,
                    'title' => $item->title,
                    'option_id' => $this->option_id,
                    'option_name' => $this->option_id != 0 ? Option::model()->findByPk($this->option_id)->name : null,
                    'option_small_name' => $this->option_id != 0 ? Option::model()->findByPk($this->option_id)->small_name : null,
                    'option_city_id' => $city->id,
                    'partner_id' => $this->partner_id,
                    'partner_name' => $this->partner->name,
                    'partner_price' => $this->partner_price,

                ];

                $this->group_item_price_data = $result;
            }
            if ($item->type == Item::TYPE_GROUP) {
                $result = [];
                foreach ($item->childs as $childItem) {
                    /** @var Item $childId */
                    $childId = $childItem->id;
                    $result[$childId]['id'] = $childId;
                    $result[$childId]['title'] = $childItem->title;
                    $selfLink = $item->getSelfLinkByChildren($childItem);
                    $result[$childId]['option_id'] = 0;
                    $result[$childId]['option_name'] = null;
                    $result[$childId]['option_small_name'] = null;
                    $result[$childId]['option_city_id'] = 1;
                    if (!empty($selfLink->option)) {
                        $result[$childId]['option_id'] = $selfLink->option->id;
                        $result[$childId]['option_name'] = $selfLink->option->name;
                        $result[$childId]['option_small_name'] = $selfLink->option->small_name;
                        $result[$childId]['option_city_id'] = $selfLink->city_id;
                    }
                    $option_id = $result[$childId]['option_id'];
                    $city_id = $result[$childId]['option_city_id'];

                    $partnerLink = array_filter($childItem->partners, function($v) use ($option_id) {
                        return $v->option_id == $option_id;
                    });
                    /** @var ItemPartnerLink $partnerLink */
                    $partnerLink = array_shift($partnerLink);
                    $result[$childId]['partner_id'] = $partnerLink->partner_id;
                    $result[$childId]['partner_name'] = $partnerLink->partner->name;
                    $result[$childId]['partner_price'] = $partnerLink->partner_price;

                    $prices = array_filter($childItem->prices, function($v) use ($option_id, $city_id) {
                        return $v->option_id == $option_id && $v->city_id == $city_id;
                    });
                    $prices = array_shift($prices);
                    $result[$childId]['price'] = $prices->item_price;
                }
                $this->group_item_price_data = $result;
            }
        }
        return parent::beforeSave();
    }

    public function savePartnerDataForGroup()
    {
        /** @var Item $item */
        $item = Item::model()->resetScope(true)->findByPk($this->activated_item);
        $city = $this->getCity();
        $partner = $item->getPartnersForCity($city);
        /** @var ItemPartnerLink $partner */
        $partner = array_shift($partner);
        $this->partner_id = $partner->partner_id;
        $this->partner_price = $partner->partner_price;
    }

    /**
     * @return City
     */
    protected function getCity()
    {
        return City::getCity(Order::model()->findByPk($this->order_id)->city_id);
    }

    /**
     * @return bool
     */
    function isOverdue()
    {
        $overdueDate = $this->getOverdueTime();

        //echo $overdueDate; die;
        
        return $overdueDate < date("Y-m-d");
    }

    /**
     * @param bool $strDate
     * @return string|DateTime
     */
    function getOverdueTime($strDate = true)
    {
        $orderDate = $this->orderObj->validity_date;
        $date = new DateTime($orderDate);
        return $strDate ? $date->format('Y-m-d') : $date;
    }


}