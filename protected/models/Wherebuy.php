<?php
class Wherebuy extends CActiveRecord{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{wherebuy}}';
    }

    public function defaultScope()
    {
        return array(
            'order'=>'`order` ASC'

        );
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('city, address, title, coordinates, caption, time_work', 'required'),
            array('order, show', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, city, show', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('city', $this->city, true);
        $criteria->order ='id DESC';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'title' => 'Содержимое балуна',
            'caption' => 'Метка',
            'city' => 'Город',
            'coordinates' => 'Координаты (например: 55.831903, 37.411961)',
            'address' => 'Адрес',
            'time_work' => 'Время работы',
            'show' => 'Отображать?',
            'order' => 'Порядок',
        );
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}