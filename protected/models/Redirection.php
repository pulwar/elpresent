<?php

/**
 * Class Redirection
 *
 * @property int $id
 * @property string $pattern
 * @property string $target
 * @property bool $isRegexp
 */
class Redirection extends CActiveRecord
{

    /**
     * @param string $className
     * @return Redirection
     */
    public static  function model($className=__CLASS__)
    {
        return parent::model($className);
    }


    public function tableName()
    {
        return '{{redirection}}';
    }

    public function getRegexp()
    {
        $pattern = $this->pattern;
        if (!$this->isRegexp) {
            $pattern = sprintf('^%s$', preg_quote($pattern, '/'));
        }

        return sprintf('/%s/Ui', $pattern);
    }

}