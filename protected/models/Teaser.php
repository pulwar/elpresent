<?php

/**
 * Teaser model
 *
 * @property int $id
 * @property string $image
 * @property string $description
 * @property string $link
 * @property int $order
 * @property bool $noIndex
 */
class Teaser extends CActiveRecord
{

    /**
     * @var CUploadedFile|null
     */
    public $imageFile;

    public $noIndex = true;

    /**
     * @return string
     */
    public function tableName()
    {
        return '{{teasers}}';
    }

    public function behaviors()
    {
        return [
            array(
                'class' => 'application.components.behaviors.CachePurger',
                'tags' => array('teasers-cache')
            )
        ];
    }

    /**
     * @param string $className
     * @return Teaser
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function beforeSave()
    {
        // upload file
        if ($this->imageFile instanceof CUploadedFile) {
            $fileName = uniqid('teather_') . '.' . $this->imageFile->getExtensionName();
            $path = Yii::getPathOfAlias('webroot.images.teasers') . DIRECTORY_SEPARATOR . $fileName;
            if ($this->imageFile->saveAs($path)) {
                $this->image = $fileName;
            }
        }

        return parent::beforeSave();
    }

    public function getImageUri()
    {
        return 'images/teasers/' . $this->image;
    }

}