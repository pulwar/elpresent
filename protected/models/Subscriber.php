<?
/**
 * Модель Subscriber
 *
 * @property integer $subscriber_id
 * @property string $subscriber_email
 * @property string $subscriber_ip
 * @property string $subscriber_date
 * @property string $subscriber_hash
 *
 */
class Subscriber extends CActiveRecord
{
    const ACTIVE = 1;
    const INACTIVE = 2;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    public function tableName()
    {
        return '{{subscriber}}';
    }


    public function rules()
    {
        return array
        (
            array('subscriber_email, subscriber_ip, subscriber_date', 'required'),
        );
    }


    public function relations()
    {
        return array();
    }


    public function attributeLabels()
    {
        return array
        (
            'id' => _('#'),
            'subscriber_email' => _('Email'),
            'subscriber_ip' => _('Ip'),
            'subscriber_date' => _('Дата подписки'),
        );
    }
}