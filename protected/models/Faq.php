<?php

class Faq extends CActiveRecord
{

    static public function model($class = __CLASS__)
    {
        return parent::model($class);
    }

    public function tableName()
    {
        return '{{faq}}';
    }

    public function rules()
    {
        return [
            ['name, email, question', 'required'],
            ['email', 'email'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => _('Имя'),
            'email' => _('E-mail'),
            'question' => _('Ваш вопрос')
        ];
    }

    public function getFormData()
    {
        return [
            'name' => 'InputTextType',
            'email' => 'InputTextType',
            'question' => 'TextareaType',
        ];
    }


    public function getViewData()
    {
        return [
            'name' => 'SimpleTextType',
            'email' => 'SimpleTextType',
            'question' => 'SimpleTextType'
        ];
    }

    public function checkSpam($attribute, $params)
    {
        return false;
    }

    public function afterSave()
    {
        // notify administrators about new questions
        if (!$this->isNewRecord) {
            return true;
        }
        // prepare data
        $fromEmail = $this->email;
        $toEmail = 'info@elpresent.by';
        $subject = 'Вопрос от ' . $fromEmail;
        $question = $this->question;

        $mailer = Yii::createComponent('ext.mailer.EMailer');
        $mailer->isMail(); // set mail function as transport
        $mailer->CharSet = 'UTF-8';
        $mailer->SetFrom($fromEmail, $subject);
        $mailer->AddAddress($toEmail);
        $mailer->Subject = $subject;
        $mailer->MsgHtml($question);

        $mailer->Send();

        $sFromEmail = $this->email;
        $sToEmail = 'info@elpresent.by';
        $sSubject = 'Вопрос от ' . $sFromEmail;
        $sMessage = $_POST['question'];
    }
}