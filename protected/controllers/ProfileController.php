<?php


class ProfileController extends Controller
{

    public $defaultAction = 'profile';
	private static $_auth_social_time = 3600;

    public function actionLogin()
    {
        $request = $this->getRequest();
        $identity = new UserIdentity($request->getParam('email'), $request->getParam('password'));
        if($identity->authenticate()) {
            Yii::app()->user->login($identity);
            $this->redirect($this->createUrl('profile/profile'));
        }else{
            Yii::app()->user->setFlash('main_error', 'Ошибка авторизации');
            $this->redirect($this->createUrl('catalog/main'));
        }
    }

    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect($this->createUrl('catalog/main'));
    }

    public function actionRestore()
    {
        $form = new RestoreForm();

        if(!empty($_POST['RestoreForm'])){
            $form->attributes = $_POST['RestoreForm'];

            if ($form->validate())
            {
                $email = trim($form->attributes['user_email']);
                $pass = self::generatePassword(5);

                $user = User::model()->find('user_email=:user_email', array(':user_email' => $email));
                $user->user_password = md5($pass);
                if($user->save()){
                    email::EMailerSmtpSend(
                        Yii::createComponent('application.extensions.mailer.EMailer'),
                        $email,
                        'Восстановление пароля на сайте ' . $_SERVER['HTTP_HOST'],
                        'Ваш новый пароль для входа: ' . $pass . ' . Вы можете сменить пароль здесь: ' . 'http://'.$_SERVER['HTTP_HOST'] . '/profile/profile',
                        Yii::app()->params['emailer']
                    );
                    Yii::app()->user->setFlash('restore_password', 'Новый пароль выслан на ' . $email);
                    $this->redirect(['profile/restore']);
                }else{
                    html::pr($user->getErrors(),1);
                }


            }
        }
        
        $this->render('restore_pass', array('form' => $form));
    }

    public function actionRegister()
    {
        $request = $this->getRequest();

        $user = new User('register');
        $user->user_password = $request->getParam('password_confirm');
        $user->user_password_confirm = $request->getParam('password_confirm');
        $user->user_lastname = $request->getParam('last_name');
        $user->user_firstname = $request->getParam('first_name');
        $user->user_email = $request->getParam('email');
        $user->user_birthday = $request->getParam('birthday');

        if ($user->validate() && $user->save()) {
            $identity = new UserIdentity($user->user_email, $user->user_password, true);
            if($identity->authenticate()) {
                Yii::app()->user->login($identity);
                $this->redirect($this->createUrl('profile/profile'));
            }
        } else {
            $arr_errors = [];
            foreach($user->getErrors() as $value){
                $arr_errors[] = $value[0];
            }
            Yii::app()->user->setFlash('main_error', implode('<p>', $arr_errors));
            $this->redirect($this->createUrl('catalog/main'));
        }
    }

	public function actionVkauth()
	{
		Yii::app()->user->logout();
		$user_info = Yii::app()->vkAuth->getUserInfo();
		$uid = $user_info['uid'];
		$user_login = empty($user_info['screen_name']) ? '' : $user_info['screen_name'];
		$first_name = $user_info['first_name'];
		$last_name = $user_info['last_name'];
		$sex = $user_info['sex'] == 2 ? 'm' : 'f';
		$user_birthday_time = strtotime($user_info['bdate']);
		$user_birthday = date('Y-m-d', $user_birthday_time);
		$email = empty($user_info['email']) ? '' : $user_info['email'];

		$user_identity = new UserSocialIdentity($uid, $first_name, $email);

		if($user_identity->authenticate()) {
			Yii::app()->user->login($user_identity, self::$_auth_social_time);
		}else{
			$user = new User('register');
			$user->user_login = $user_login;
			$user->user_password = $uid;
			$user->user_password_confirm = $uid;
			$user->user_birthday = $user_birthday;
			$user->user_lastname = $last_name;
			$user->user_firstname = $first_name;
			$user->user_sex = $sex;
			$user->user_email = $email;
			if ($user->validate() && $user->save()) {
				$user_identity = new UserSocialIdentity($uid, $first_name);
				if($user_identity->authenticate()) {
					Yii::app()->user->login($user_identity, self::$_auth_social_time);
				}
			}else{
				die('Не удалось создать данные в базе, взятые из соцсети');
			}
		}
		if (!empty($_GET['code'])) {
			header("Location: http://$_SERVER[HTTP_HOST]");
		}
		die;
	}

	public function actionFsauth()
	{
		Yii::app()->user->logout();
		$user_info = Yii::app()->fsAuth->getUserInfo();
		$id = $user_info['id'];
		$first_name = $user_info['first_name'];
		$last_name = $user_info['last_name'];
		$sex = $user_info['gender'] == 'male' ? 'm' : 'f';
		if(!empty($user_info['birthday'])){
			$user_birthday_time = strtotime($user_info['birthday']);
			$user_birthday = date('Y-m-d', $user_birthday_time);
		}
		$email = empty($user_info['email']) ? '' : $user_info['email'];

		$user_identity = new UserSocialIdentity($id, $first_name, $email);

		if($user_identity->authenticate()) {
			Yii::app()->user->login($user_identity, self::$_auth_social_time);
		}else{
			$user = new User('register');
			$user->user_password = $id;
			$user->user_password_confirm = $id;
			if(!empty($user_birthday)){
				$user->user_birthday = $user_birthday;
			}
			$user->user_lastname = $last_name;
			$user->user_firstname = $first_name;
			$user->user_sex = $sex;

			if(!empty($email)){
				$user->user_email = $email;
				$user->user_login = $email;
			}

			if ($user->validate() && $user->save()) {
				$user_identity = new UserSocialIdentity($id, $first_name);
				if($user_identity->authenticate()) {
					Yii::app()->user->login($user_identity, self::$_auth_social_time);
				}
			}else{
				header('Content-type: text/html; charset=UTF-8');
				echo '<p>В вашем социальном профиле недостаточно данных для создания профиля на elpresent</p>';
				foreach($user->getErrors() as $value){
					foreach($value as $v){
						echo $v . '<br>';
					}
				}
				exit;
			}
		}
		if (!empty($_GET['code'])) {
			header("Location: http://$_SERVER[HTTP_HOST]");
		}
		die;
	}

    public function actionHistory()
    {
        /** @var WebUser $user */
        $user = Yii::app()->user;

        if(!$user->getId())
            throw new CHttpException('404');
        $user_all_info = User::model()->findByPk($user->getId());

        // return only activated or already expired orders
        $expirationDate = new \DateTime('-3 month');
        $criteria = new CDbCriteria(
            ['condition' => '(order_date < :expiration_date) AND user_id=:user',
            'order' => 'order_date DESC',
            'params' => [
                'expiration_date' => $expirationDate->format('Y-m-d 00:00:00'),
                'user' => $user->id,
                'cash' => PaymentType::COURIER
            ]
        ]);
        $criteria->addCondition('(is_paid=1 AND payment_type_id!=:cash) OR (is_paid=0 AND payment_type_id=:cash)');
        $activeOrderProvider = new CActiveDataProvider('Order', [
            'criteria' => $criteria
        ]);

        $this->renderResponse('history', [
            'provider' => $activeOrderProvider,
            'user_all_info' => $user_all_info
        ]);
    }

    public function actionProfile()
    {
        /** @var WebUser $user */
        $user = Yii::app()->user;
        if(!$user->getId())
            throw new CHttpException('404');
        $user_all_info = User::model()->findByPk($user->getId());

        $u = $user->getUser();
        $form = new CForm($u->getForm());

        if ($form->submitted('save') && $form->validate()) {
            /** @var User $u */
            $u = $form->getModel();
            $u->save();
            $user->setFlash('success', "Данные сохранены успешно!");
            $this->redirect($this->createUrl('profile/profile'));
        }

        $form = str_replace('row ', 'form-group ', $form);
        $this->renderResponse('profile', [
            'form' => $form,
            'user_all_info' => $user_all_info
        ]);
    }

    public function actionResetCash()
    {
        /** @var WebUser $user */
        $user = Yii::app()->user;
        $model = User::model()->findByPk($user->getId());
        $model->cash = 0;
        $model->save();
        $this->redirect('/profile/profile');
    }

    public function actionOrder()
    {
        /** @var WebUser $user */
        $user = Yii::app()->user;
        if(!$user->getId())
            throw new CHttpException('404');
        $user_all_info = User::model()->findByPk($user->getId());
        // return only not activated and not expired orders

        $expirationDate = new \DateTime('-3 month');
        $criteria = new CDbCriteria(['condition' => '(order_date > :expiration_date) AND user_id=:user',
            'order' => 'order_date DESC',
            'params' => [
                'expiration_date' => $expirationDate->format('Y-m-d 00:00:00'),
                'user' => $user->id,
                'cash' => PaymentType::COURIER
            ]
        ]);
        $criteria->addCondition('((is_paid=1 AND payment_type_id!=:cash) OR (is_paid=0 AND payment_type_id=:cash))');
        $activeOrderProvider = new CActiveDataProvider('Order', [
            'criteria' => $criteria
        ]);

        $this->renderResponse('active', [
            'provider' => $activeOrderProvider,
            'user_all_info' => $user_all_info
        ]);

    }

    public function actionPending()
    {
        /** @var WebUser $user */
        $user = Yii::app()->user;

        if(!$user->getId())
            throw new CHttpException('404');
        $user_all_info = User::model()->findByPk($user->getId());
        // return only not activated and not expired orders

        $expirationDate = new \DateTime('-3 month');
        $criteria = new CDbCriteria(['condition' => 'order_date > :expiration_date AND user_id=:user',
            'order' => 'order_date DESC',
            'params' => [
                'expiration_date' => $expirationDate->format('Y-m-d 00:00:00'),
                'user' => $user->id,
                'cash' => PaymentType::COURIER
            ]
        ]);
        $criteria->addCondition('is_paid=0 AND payment_type_id!=:cash');
        $activeOrderProvider = new CActiveDataProvider('Order', [
            'criteria' => $criteria
        ]);

        $this->renderResponse('pending', [
            'provider' => $activeOrderProvider,
            'user_all_info' => $user_all_info
        ]);
    }

    public function renderResponse($view, $data=null)
    {
        $this->render($view, $data);
    }

    public function getLeftMenuWidget()
    {
        return ['CategoriesWidget', 'SocialWidget'];
    }

    /**
     * Выгрузка номеров юзеров для sms рассыльщика
     */
    public function actionUserPhones()
    {
        /** @var WebUser $user */
        $user = Yii::app()->user;
        $data = [];

        if($user->getId() == 1){
            $criteria = new CDbCriteria;
            $criteria->select='user_phone';
            $criteria->condition='user_phone!=""';
            $model = User::model()->findAll($criteria);

            foreach($model as $value){
                $item = trim($value->user_phone);
                $rest = substr($item, 0, 1);
                if($rest == '8' || $rest == '+'){
                    $data[] = $item;
                }
            }

            $data = array_unique($data);

            $output = fopen("php://output",'w') or die("Can't open php://output");
            header("Content-Type:application/csv");
            header("Content-Disposition:attachment;filename=phones.csv");
            fputcsv($output, $data, "\n");
            fclose($output) or die("Can't close php://output");
        } else {
            throw new CHttpException('404');
        }
    }

    /**
     * Выгрузка emails юзеров для sms рассыльщика
     */
    public function actionUserEmails()
    {
        /** @var WebUser $user */
        $user = Yii::app()->user;
        $data = [];

        if($user->getId() == 1){
            $criteria = new CDbCriteria;
            $criteria->select='user_email';
            $criteria->condition='user_email!=""';
            $model = User::model()->findAll($criteria);

            foreach($model as $value){
                $item = trim($value->user_email);
                $data[] = $item;
            }

            $data = array_unique($data);

            $output = fopen("php://output",'w') or die("Can't open php://output");
            header("Content-Type:application/csv");
            header("Content-Disposition:attachment;filename=emails.csv");
            fputcsv($output, $data, "\n");
            fclose($output) or die("Can't close php://output");
        } else {
            throw new CHttpException('404');
        }
    }

    public static function generatePassword($number)
    {
        $arr = array('a','b','c','d','e','f',
            'g','h','i','j','k','l',
            'm','n','o','p','r','s',
            't','u','v','x','y','z',
            'A','B','C','D','E','F',
            'G','H','I','J','K','L',
            'M','N','O','P','R','S',
            'T','U','V','X','Y','Z',
            '1','2','3','4','5','6',
            '7','8','9','0','.',',',
            '(',')','[',']','!','?',
            '&','^','%','@','*','$',
            '<','>','/','|','+','-',
            '{','}','`','~');
        // Генерируем пароль
        $pass = "";
        for($i = 0; $i < $number; $i++)
        {
            // Вычисляем случайный индекс массива
            $index = rand(0, count($arr) - 1);
            $pass .= $arr[$index];
        }
        return $pass;
    }

}