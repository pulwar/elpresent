<?php
class SkidkiController extends ElPresentController
{
    const COUNT_ITEMS = 10;
    public $itemTitle;
    public $description;
    public $keywords;

    public function actionIndex()
    {
        $params = Yii::app()->request->getParam('params');

        if($params == 'index'){
            $params_array = [];
        }else{
            $params_array = self::_paramsToArray($params);
        }

        if(count($params_array) == 0){
            $this->_getAll();
        }elseif(count($params_array) == 1){
           $this->_getCategory($params_array);
        }elseif(count($params_array) == 2){
            $this->_getItem($params_array);
        }else{
            throw new CHttpException(404, 'Страница не найдена');
        }
    }

    protected function _getAll()
    {
        $this->itemTitle = 'Акции и скидки большого города';
        $this->description = 'Акции и скидки большого города';
        $this->keywords = 'Акции,скидки';

        // todo делаем кеширование
        $parent_ids_array = Discounts::model()->getIdsCategory();

        $criteria = new CDbCriteria();
        $criteria->condition = 'hide = :hide';
        $criteria->params = [':hide' => 0];
        $criteria->addInCondition('category_id', $parent_ids_array);
        $criteria->order = 'order_items ASC, id DESC';

        $count = Discounts::model()->count($criteria);
        $pages = new CPagination($count);
        $pages->pageSize = self::COUNT_ITEMS;
        $pages->applyLimit($criteria);
        $models = Discounts::model()->findAll($criteria);

        $this->render('category', array(
            'models' => $models,
            'pages' => $pages,
            'count' => $count
        ));
    }

    protected function _getCategory($params_array)
    {
        $category_alias = $params_array[0];
        $category = Category::model()->findBySlug($category_alias);

        if(empty($category)){
            throw new CHttpException(404, 'Страница не найдена');
        }

        $category_id = $category->id;

        $criteria = new CDbCriteria();
        $criteria->condition = 'category_id= :category_id AND hide = :hide';
        $criteria->params = [':category_id' => $category_id, ':hide' => 0];
        $criteria->order = 'order_items ASC, id DESC';

        $count = Discounts::model()->count($criteria);
        $pages = new CPagination($count);
        $pages->pageSize = self::COUNT_ITEMS;
        $pages->applyLimit($criteria);
        $models = Discounts::model()->findAll($criteria);

        $this->itemTitle = $category->title;
        $this->description = $category->meta_description;
        $this->keywords = $category->meta_keywords;

        $this->render('category', array(
            'models' => $models,
            'pages' => $pages,
            'count' => $count,
            'category' => $category,
        ));
    }

    protected function _getItem($params_array)
    {

        $item_alias = $params_array[1];
        $model = Discounts::model()->find(
            [
                'select' => '*',
                'condition' => 'alias=:alias AND hide = :hide',
                'params' => [':alias' => $item_alias, ':hide' => 0],
            ]
        );

        if(!empty($_POST['submit']) && !empty($_POST['email'])){
            $result = null;
            $discount_id = $model->id;
            $discounts_cods = DiscountsCods::model()->find('discount_id=:discount_id', [':discount_id' => $discount_id]);

            if(empty($discounts_cods)){
                $result = 'Кода для вашей скидки нет';
            }else{
                $code = $discounts_cods->code;
                $email = trim(htmlspecialchars($_POST['email']));
                $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
                $emailer_config = Yii::app()->params['emailer'];

                /** @var PdfGenerator $pdfGenerator */
                $pdfGenerator = Yii::app()->pdfGenerator;
                $tmp_dir = Yii::getPathOfAlias('webroot.assets.pdf_tmp');
                $htmlFile = $tmp_dir . '/code_' . $code . '.html';

                $html = Yii::app()->controller->renderPartial(
                    '//pdf/code_certificate',
                    [
                        'model' => $model,
                        'rootDir' => Yii::getPathOfAlias('webroot'),
                        'code' => $code
                    ],
                    true
                );

                file_put_contents($htmlFile, $html);
                $pdfFile = $tmp_dir . '/code_' . $code . '.pdf';
                $pdfGenerator->generatePdf($htmlFile, $pdfFile);

                $emailer_result = email::EMailerSmtpSend(
                    $mailer,
                    $email,
                    'Ваш сертификат для получения скидки',
                    'Ваш код: ' . $code,
                    $emailer_config,
                    [$pdfFile, 'code_certificate.pdf']
                );

                if ($emailer_result){
                    Yii::app()->cache->flush();
                    $result = 'Ваш код для получения скидки отправлен на ' . $email;
                    $discounts_cods->delete();
                }else{
                    $result = 'Ошибка отправки письма!';
                }
            }

            $this->render('result', array(
                'result' => $result,
            ));
        }else{
            if(empty($model)){
                throw new CHttpException(404, 'Страница не найдена');
            }

//            echo $html = Yii::app()->controller->renderPartial(
//                '//pdf/code_certificate',
//                [
//                    'model' => $model,
//                    'rootDir' => Yii::getPathOfAlias('webroot'),
//                    'code' => 333
//                ],
//                true
//            );
//            die;

            $this->itemTitle = $model->title;
            $this->description = $model->meta_d;
            $this->keywords = $model->meta_k;

            $this->render('item', array(
                'model' => $model,
            ));
        }
    }

    protected static function _paramsToArray($params_str, $separator = '/')
    {
        $params_str = trim($params_str);
        return empty($params_str) ? [] : explode($separator, $params_str);
    }


}