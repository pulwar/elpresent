<?php

class NewsController extends ElPresentController
{
    public $itemTitle;

    public function actionIndex()
    {
        $list = News::model()->ordered()->findAll();
        $this->render('index', array('list' => $list));
    }


    public function actionView()
    {
        $newsId = Yii::app()->request->getParam('id');
        $oNews = News::model()->findByPk($newsId);
        $this->itemTitle = $oNews->title;
        $this->render('view', array (
            'oNews' => $oNews
        ));
    }
}