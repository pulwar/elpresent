<?php

class ReportController extends Controller
{
    private $token = 'E537JwzT7EVLArwJBGHQh6hNXND2nRcEU7NMZc8z8q2B5Xx9SSRZ';

    public function actionOrders()
    {
        /** @var CHttpRequest $request */
        $request = Yii::app()->request;
        $token = $request->getParam('auth_token');

        if ($token == $this->token) {
            $endDate = new DateTime();
            $endDate = $endDate->format('Y-m-d');
            $startDate = new DateTime('midnight');
            $startDate->modify('-3 month');
            $startDate = $startDate->format('Y-m-d');

            $criteria = new CDbCriteria();
            $criteria->order = 't.partner_activated_date DESC';
            $criteria->condition = 't.partner_activated=TRUE';
            $criteria->condition .= " AND (t.partner_activated_date>='$startDate' AND t.partner_activated_date<='$endDate')";
            $orderItems = OrderItems::model()->findAll($criteria);

            if (count($orderItems) > 0) {
                header('Content-Type: text/csv; charset=utf-8');
                header('Content-Disposition: attachment; filename=partner_report.csv');
                $output = fopen('php://output', 'w');
                fputcsv($output, [
                    'Id Партнёра',
                    'Партнёр',
                    'Дата погашения',
                    'Код',
                    'Id впечетления',
                    'Впечетление',
                    'Опция',
                    'Цена',
                    'Цена партнёра',
                    'Наименование проданного впечетления',
                    'Id проданного впечетления'

                ], ';');
                foreach ($orderItems as $key => $orderItem) {
                    /** @var OrderItems $orderItem */
                    $isGroup = $orderItem->itemObj->type == Item::TYPE_GROUP;

                    $idItem = $isGroup ? $orderItem->activatedItemObj->id : $orderItem->itemObj->id;
                    $itemTitle = $isGroup ? $orderItem->activatedItemObj->title : $orderItem->itemObj->title;
                    $optionName = $isGroup
                        ? ($orderItem->option_id === 0 ? '' : $orderItem->group_item_price_data[$idItem]['option_name'])
                        : ($orderItem->option_id === 0 ? '' : $orderItem->group_item_price_data['option_name']);

                    fputcsv($output, [
                        $orderItem->partner_id,
                        $orderItem->partner->name,
                        $orderItem->partner_activated_date,
                        $orderItem->code,
                        $idItem,
                        $itemTitle,
                        $optionName,
                        number_format($orderItem->activated_item_price, 2, ',', ''),
                        number_format($orderItem->partner_price, 2, ',', ''),
                        $isGroup ? $orderItem->itemObj->title : '',
                        $isGroup ? $orderItem->itemObj->id : ''


                    ], ';');
                }
            } else {
                header("HTTP/1.0 204 No Response");
            }
        } else {
            header("Location: /",TRUE,301);
        }
    }

}