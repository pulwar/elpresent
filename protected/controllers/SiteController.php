<?php

class SiteController extends ElPresentController
{
    protected $_error;
    public $enNews = false;
    public $itemTitle;

    public static $landingSubject = 'Заявка с продающей страницы Elpresent.by';

    public function actions()
    {
        return [
            'sitemap' => 'application.actions.SiteMapAction'
        ];
    }


    public function getLeftMenuWidget()
    {
        return array('CategoriesWidget', 'SocialWidget');
    }

    /**
     * This is discount page
     */
    public function actionDiscount()
    {
        $oFaqModel = new Faq;
        $oUser = new User;
        $userId = Yii::app()->user->getId();

        if ($userId) {
            $oUser = User::model()->findByPk($userId);
        }

        $oFaqModel->name = $oUser->user_firstname;
        $oFaqModel->email = $oUser->user_email;

        if ($this->getRequest()->isPostRequest && !empty($_POST)) {
            $oFaqModel->attributes = $_POST;

            if ($oFaqModel->save()) {
                Yii::app()->user->setFlash('faq_success', "Вопрос успешно отправлен!");
                Yii::app()->request->redirect($this->createUrl('site/discount'));
            }
        }

        $this->render('discount', [
          'oFaqModel' => $oFaqModel
        ]);
    }

    /**
     * This is faq page
     */
    public function actionFaq()
    {
        $oFaqModel = new Faq;
        $oUser = new User;
        $userId = Yii::app()->user->getId();

        if ($userId) {
            $oUser = User::model()->findByPk($userId);
        }

        $oFaqModel->name = $oUser->user_firstname;
        $oFaqModel->email = $oUser->user_email;

        if ($this->getRequest()->isPostRequest && !empty($_POST)) {
            $oFaqModel->attributes = $_POST;

            if ($oFaqModel->save()) {
                Yii::app()->user->setFlash('faq_success', "Вопрос успешно отправлен!");
                $this->redirect($this->createUrl('site/faq'));
            }
        }

        $this->render('faq', [
            'oFaqModel' => $oFaqModel
        ]);
    }

    /**
     * Displays the contact page
     */
    public function actionContact()
    {
        $contact = new Contact;
        if (isset($_POST['Contact']) && empty($_POST['Contact']['gobot'])) {

            $contact->attributes = $_POST['Contact'];
            $contact->visibility = 0;

            if ($contact->save())
            {
                email::EMailerSmtpSend(
                    Yii::createComponent('application.extensions.mailer.EMailer'),
                    Yii::app()->getParams()->review['emailTo'],
                    'Оставлен отзыв на сайте Elpresent.by',
                    'Оставлен отзыв на сайте Elpresent.by: <br>' . $contact->contact_body,
                    Yii::app()->params['emailer']
                );

                Yii::app()->user->setFlash('contact', 'Ваш отзыв успешно сохранен и будет добавлен после просмотра администрацией.'); //Ваш отзыв успешно сохранен.
                $this->refresh();
            }
        }

        $this->render('contact', array(
            'oContact' => $contact,
            'oMessages' => $contact->model()->findAllByAttributes(array(
                'visibility' => 1
            ))
        ));
    }

    public function actionQuestionnaire()
    {
        $questionnaire = new QuestionnaireForm();

        if (isset($_POST['Questionnaire']))
        {
            $questionnaire->attributes = $_POST['Questionnaire'];

            if ($questionnaire->validate())
            {
                email::EMailerSmtpSend(
                    Yii::createComponent('application.extensions.mailer.EMailer'),
                    Yii::app()->getParams()->questionnaire['emailTo'],
                    'Анкета корпоративного клиента Elpresent.by',
                    $this->renderFile(Yii::getPathOfAlias('application') . '/views/site/questionnaireMail.php', array('data' => $_POST['Questionnaire']), true),
                    Yii::app()->params['emailer']
                );

                Yii::app()->user->setFlash('questionnaire', 'Ваша анкета успешно отправлена.');
                $this->refresh();
            }
        }

        $this->render('questionnaire', array('questionnaire' => $questionnaire));
    }

    public function actionHorosheenastroenie()
    {
        Yii::app()->clientScript->registerCssFile('/css/moods.css');
        $moods = MoodProfile::model()->sorted()->with(array('moods' => array(
            'condition' => 'moods.visible=1',
            'order' => 'mood_order DESC, moods.id DESC',
            'with' => array('items' => array('with' => 'item')),
        )))->findAll();

        $this->render('moods', array(
            'moods' => $moods,
       ));
    }

    public function actionTest()
    {
        echo $_SERVER['REMOTE_ADDR'];
    }

    public function actionWish()
    {
        $request = $this->getRequest();
        if (!$request->isPostRequest) {
            throw new CHttpException(405);
        }

        // todo: create model for fields validation
        if (empty($_POST['item_id']) || empty($_POST['your_name']) || empty($_POST['recipient_name']) || empty($_POST['your_email']) || empty($_POST['recipient_email'])) {
            throw new CHttpException(400);
        }

        $item = Item::model()->findByPk($request->getParam('item_id'));
        if (!$item) {
            throw new CHttpException(404);
        }

        $wishHtml = $this->renderFile(
            Yii::getPathOfAlias('application') . '/views/system/wish.php', [
                'item' => $item,
                'your_name' => $request->getParam('your_name'),
                'recipient_name' => $request->getParam('recipient_name'),
            ], true);

        email::EMailerSmtpSend(
            Yii::createComponent('application.extensions.mailer.EMailer'),
            $_POST['recipient_email'],
            'Хочу в подарок',
            $wishHtml,
            Yii::app()->params['emailer']
        );
    }

    public function actionLanding(){

        $model = Landing::model()->find();

        $landingBackCallForm = new LandingBackCallForm();
        $landingOrderForm = new LandingOrderForm();
        $landingHelpForm = new LandingHelpForm();

        $city = $this->getRequest()->getQuery('_city');

        $list = Item::getItemsForLanding($city, 6);

        if(isset($_POST['ajax']) && Yii::app()->request->isAjaxRequest && ($_POST['ajax']==='yw0' || $_POST['ajax']==='yw1' || $_POST['ajax']==='yw2'))
        {
            echo CActiveForm::validate($landingBackCallForm);
            Yii::app()->end();
        }

        if(isset($_POST['ajax']) && Yii::app()->request->isAjaxRequest && $_POST['ajax']==='yw3')
        {
            echo CActiveForm::validate($landingOrderForm);
            Yii::app()->end();
        }


        if(isset($_POST['LandingBackCallForm']) && Yii::app()->request->isAjaxRequest)
        {
            $landingBackCallForm->attributes = $_POST['LandingBackCallForm'];

            if($landingBackCallForm->validate())
            {
                $str = "Пользователь заполнил следующие данные: имя: %s; телефон: %s; e-mail: %s";
                $body = sprintf($str, $landingBackCallForm->name, $landingBackCallForm->phone, $landingBackCallForm->email);

                email::EMailerSmtpSend(
                    Yii::createComponent('application.extensions.mailer.EMailer'),
                    $model->email,
                    self::$landingSubject,
                    $body,
                    Yii::app()->params['emailer']
                );

                //Здесь обрабатываем данные формы (сохранение, обновление ...)
                echo CJSON::encode(array(
                    'status'=>true
                ));
                Yii::app()->end();
            } else {
                $error = CActiveForm::validate($landingBackCallForm);
                if($error!='[]')
                    echo $error;
                Yii::app()->end();
            }
        }

        if(isset($_POST['LandingOrderForm']) && Yii::app()->request->isAjaxRequest)
        {
            $landingOrderForm->attributes = $_POST['LandingOrderForm'];

            if($landingOrderForm->validate())
            {
                $str = "Пользователь заполнил следующие данные: имя: %s; телефон: %s; e-mail: %s; сообщение: %s";
                $body = sprintf($str, $landingBackCallForm->name, $landingBackCallForm->phone, $landingBackCallForm->email, $landingBackCallForm->message);

                email::EMailerSmtpSend(
                    Yii::createComponent('application.extensions.mailer.EMailer'),
                    $model->email,
                    self::$landingSubject,
                    $body,
                    Yii::app()->params['emailer']
                );

                //Здесь обрабатываем данные формы (сохранение, обновление ...)
                echo CJSON::encode(array(
                    'status'=>true
                ));
                Yii::app()->end();
            }
            else
            {
                $error = CActiveForm::validate($landingOrderForm);
                if($error!='[]')
                    echo $error;
                Yii::app()->end();
            }
        }

        if(isset($_POST['LandingHelpForm']) && Yii::app()->request->isAjaxRequest)
        {
            $landingHelpForm->attributes = $_POST['LandingHelpForm'];

            if($landingHelpForm->validate())
            {
                //**********
                $postdata = http_build_query(
                    array(
                        'secret' => '6Ld6L50UAAAAAOgjiGM-_XW2BFwdYCUzoyoRm05n',
                        'response' => $_POST['g-recaptcha-response']
                    )
                );

                $opts = array('http' =>
                    array(
                        'method'  => 'POST',
                        'header'  => 'Content-Type: application/x-www-form-urlencoded',
                        'content' => $postdata
                    )
                );

                $context  = stream_context_create($opts);

                $result = json_decode(file_get_contents('https://www.google.com/recaptcha/api/siteverify', true, $context),true);

                if($result['success']!= true) {
                    echo CJSON::encode(array(
                        'status'=>false,
                        'code'  => 2
                    ));
                    Yii::app()->end();
                }
                //***********
                $str = "Пользователь заполнил следующие данные:\n Имя: %s\n Телефон: %s\n Звонить с %s по %s\n Комментарий: %s";
                $body = sprintf($str, $landingHelpForm->name, $landingHelpForm->phone, $landingHelpForm->time_from, $landingHelpForm->time_to, $landingHelpForm->comment);

                email::EMailerSmtpSend(
                    Yii::createComponent('application.extensions.mailer.EMailer'),
                    $model->email,
                    self::$landingSubject,
                    $body,
                    Yii::app()->params['emailer']
                );

                //Здесь обрабатываем данные формы (сохранение, обновление ...)
                echo CJSON::encode(array(
                    'status'=>true
                ));
                Yii::app()->end();
            } else {
                echo CJSON::encode(array(
                    'status'=>false,
                    'code'  => 1
                ));
                Yii::app()->end();
            }
        }

        // формируем метатеги и подключение jquery
        $cs = Yii::app()->getClientScript();
        $cs->registerCoreScript('jquery');
        $cs->registerMetaTag($model->meta_d,'description');
        $cs->registerMetaTag($model->meta_k,'keywords');

        $this->render('landing', array(
            'model' => $model,
            'phones' => explode(',', $model->phones),
            'list' => $list,
        ));
    }

    public function ActionError()
    {
        header('HTTP/1.0 404 Not Found');
        if( Yii::app()->errorHandler->getError() !== null ) {
            $this->_error = Yii::app()->errorHandler->getError();
            if ($this->_error['code'] == 404) {
                $this->render('404', []);
            }else{
                html::pr(Yii::app()->errorHandler->getError());
                //$this->render('500', []);
            }
        }

    }
}
