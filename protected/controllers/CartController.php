<?php

/**
 * Class CartController
 */
class CartController extends ElPresentController
{
    public $itemTitle;
    public $description;
    public $keywords;
    public $enNews = false;
    public $og_image = false;
    public $seoText = '';
    private $_dir;

    public function init()
    {
        $this->_dir = Yii::getPathOfAlias('webroot.assets');
    }


    public function actionIndex()
    {
        $cart = Cart::getCart();

        $items = $cart->getItems();
        $options = $cart->getOptions();
        $sortItems = [];
        foreach($items as $item) {
            $sortItems[$options[$item->id]['position_num']] = $item;
        }
        ksort($sortItems);
        $cart = $this->renderPartial('/cart/_list', ['items' => $sortItems], true, false);
        $this->render('/cart/index', compact('cart'));
    }

    public function actionAdd()
    {
        /** @var CApplication $APP */
        $APP = Yii::app();
        $addItem = $APP->request->getPost('addItem');
        if (!empty($addItem) && $APP->request->isAjaxRequest) {
            /** @var Cart $cart */
            try {
                $cart = Cart::getCart();
                $cart->addItemById($addItem['id'], $addItem['option']);
                $response = ['success' => true, 'countItems' => $cart->getCountItems(), 'countPrice' => $cart->calcCartPriceSum()];
                echo CJSON::encode($response);
            } catch (\Exception $e) {
                echo CJSON::encode(['success' => false]);
            }
            $APP->end();
        }
    }

    /**
     * Remove item from cart by id.
     * @return string
     */
    public function actionDel()
    {

        $request = Yii::app()->request;
        $removeItemId = $request->getPost('removeItemId');
        if (!empty($removeItemId) && $request->isAjaxRequest) {
            $removeItemId = (int) $removeItemId;
            try {
                $cart = Cart::getCart();
                $cart->removeItemById($removeItemId);
                $response = [
                    'success' => true,
                    'countItems' => $cart->getCountItems(),
                    'sumPrice' => $cart->calcCartPriceSum(),
                    'cart' => $this->renderPartial('/cart/_list', ['items' => $cart->getItems()], true, false)
                ];
                echo CJSON::encode($response);
            } catch (\Exception $e) {
                echo CJSON::encode(['success' => false]);
            }
        }
        Yii::app()->end();
    }

    protected function _getTitle($category)
    {
        $file_path = $this->_dir . '/categorywithoutsuffix.txt';
        $result = false;
        if (is_file($file_path)) {
            $urls_txt = trim(file_get_contents($file_path));
            $urls = explode(',', $urls_txt);
            foreach ($urls as $url) {
                if (url::is_page(trim($url))) {
                    $result = true;
                }
            }
        }

        return $result ? $category->meta_title : $category->meta_title . ' | Купить подарки в Минске | Подарки для мужчин и женщин';
    }

}