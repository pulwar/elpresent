<?php

class GalleryController extends ElPresentController
{

    public function actionIndex()
    {
        $galleries = Gallery::model()->with([
            'galleryPhotos' => ['joinType'=>'INNER JOIN',]
        ])->findAll(['order' => 't.id']);

        $this->render('list', compact('galleries'));
    }

    public function actionView($id)
    {
        $gallery = Gallery::model()->findByPk($id);
        if (!$gallery) {
            throw new CHttpException(404, 'Страница не найдена');
        }

        $this->render('view', compact('gallery'));
    }

}