<?php

class CatalogController extends ElPresentController
{
    public $itemTitle;
    public $description;
    public $keywords;
    public $enNews = false;
    public $og_image = false;
    public $seoText = '';
    private $_dir;

    public function init()
    {
        $this->_dir = Yii::getPathOfAlias('webroot.assets');
    }

    public function actionWant($hash)
    {
        $itemId = Yii::app()->urlShortner->decode($hash, 'i');

        $item = Item::model()->findByPk($itemId);
        if (!$item) {
            throw new CHttpException(404);
        }

        $this->redirect(Yii::app()->urlManager->createUrl('catalog/item', ['perm_link' => $item->perm_link]));
    }


    public function actionIndex()
    {
        $this->itemTitle = 'Интернет-магазин подарочных сертификатов l Купить подарки в Минске l Подарки для мужчин и женщин';
        $this->keywords = Yii::app()->params['catalog_default_meta_k'];
        $this->description = Yii::app()->params['catalog_default_meta_d'];

        $request = $this->getRequest();

        $list = Item::getItemsForMainPage(
            $request->getQuery('_city'),
            $request->getQuery('perPage', Yii::app()->params['count_gifts_in_main_per_page'])
        );

        $seoText = 1 === $request->getQuery('page', 1);

        if (1 == $request->getQuery('page', 1) && isFeatureEnabled('side-seo-text')) {
            $this->seoText = Settings::getValueByAlias('main_page_seo_text');
        }

        $this->enNews = true;

        $this->render('main', compact('list', 'seoText', 'cart'));
    }

    public function actionList()
    {


        $request = $this->getRequest();

        //var_export($request->getQuery('page')); die;

        $sortBy = $_POST['sortBy'];

        $list = Item::getAjaxList(
            [
                'category_id' =>  $_POST['category_id'],
                'q' => $_POST['q'],
                'prices' => array(0 => $_POST['priceMin'], 1 => $_POST['priceMax']),
                'features' => array(0 => $_POST['features']),
                'options' => array(0 => $_POST['options']),
                'is_new' =>  $_POST['is_new'],
                'off_price' =>  $_POST['off_price']
            ],
            $request->getQuery('_city'),
            $request->getQuery('perPage', Yii::app()->params['count_gifts_in_main_per_page']),
            $sortBy
        );

        $this->renderPartial('_lists', compact('list'));

    }

    public function actionSearch()
    {

        $request = $this->getRequest();

        $list = Item::searchByCriteria([
            'q' => $request->getQuery('q'),
            'prices' => $request->getQuery('prices', []),
            'features' => $request->getQuery('features', []),
            'options' => $request->getQuery('options', []),
            'is_new' =>  $request->getQuery('is_new'),
            'off_price' =>  $request->getQuery('off_price')
        ], $request->getQuery('_city'), $request->getQuery('perPage', 12));

        $this->layout = 'search';

        return $this->render('search', compact('list'));
    }

    public function actionCategory()
    {
        $request = $this->getRequest();
        $city = $request->getQuery('_city');

        $category = Category::model()->findBySlug($request->getQuery('slug'));

        if (!$category) {
            throw new CHttpException(404, 'Страница не найдена :(');
        }

        $list = Item::searchByCategory($category, $city,
            $request->getQuery('perPage', Yii::app()->params['count_gifts_per_page'])
        );
        $this->itemTitle = self::_getTitle($category);

        $this->description = $category->meta_description;
        $this->keywords = $category->meta_keywords;

        $this->layout = 'search';

        $this->render('list', compact('list', 'category'));
    }

    /**
     * fixme: add something like params converter
     * @param Item $item
     */
    public function actionItem(Item $item)
    {
        // todo: maybe move this to behaviour of model?
        $item->seo_title ? $this->itemTitle = $item->seo_title : $this->itemTitle = $item->title;
        $this->description = $item->description;
        $this->keywords = $item->keywords;

        $photos = ItemPhoto::getPhotosForItem($item);

        $this->render('item', [
            'item' => $item,
            'oPhotos' => $photos,
            'likeItems' => $item->similar(),
        ]);
    }

    public function actionRating()
    {
        $result = [
            'result' => false,
            'value' => 0,
            'error' => ''
        ];

        $rating_item_id = $_COOKIE["rating"];
        $id = (int)$_POST['item_id'];

        $val = (int)$_POST['val'];
        $model = Item::model()->findByPk($id);

        if ($rating_item_id == $id) {
            $result['error'] = 'Вы уже голосовали!';
            $result['value'] = $model->rating / $model->vote_count;
            echo CJSON::encode($result);
            die;
        }

        $model->rating = $model->rating + $val;
        $model->vote_count++;

        if (!$model->save()) {
            d::pr($model->getErrors(), 1);
        }

        $result['result'] = true;
        $result['value'] = $model->rating / $model->vote_count;
        echo CJSON::encode($result);
        die;
    }



    public function actionAjaxsearch()
    {
        $request = $this->getRequest();
        $params = [
            'prices' => $request->getParam('prices', []),
            'features' => $request->getParam('features', []),
            'options' => $request->getParam('options', [])
        ];

        $result = [
            'header' => '',
            'content' => ''
        ];
        if (!empty($params['prices']) || !empty($params['features']) || !empty($params['options'])) {
            $provider = Item::searchByCriteria($params, $request->getQuery('_city'), 3);

            $data = $provider->getData();
            if (!empty($data)) {
                $result['content'] = $this->renderPartial('search-results', [
                    'items' => $provider->getData(),
                    'params' => $params
                ], true);
                $result['header'] = sprintf('Найдено <span style="text-decoration:underline">%d подарка</span>:', $provider->getPagination()->itemCount);
            }
        }

        echo json_encode($result);
        Yii::app()->end();
    }


    protected function _getTitle($category)
    {
        $file_path = $this->_dir . '/categorywithoutsuffix.txt';
        $result = false;
        if (is_file($file_path)) {
            $urls_txt = trim(file_get_contents($file_path));
            $urls = explode(',', $urls_txt);
            foreach ($urls as $url) {
                if (url::is_page(trim($url))) {
                    $result = true;
                }
            }
        }

        return $result ? $category->meta_title : $category->meta_title . ' | Купить подарки в Минске | Подарки для мужчин и женщин';
    }

}
