<?php

class ActionsController extends ElPresentController
{
    public $itemTitle = 'Новости и акции сайта';
    public $description = 'Новорсти и акции сайта';
    public $keywords = 'Новорсти, акции';

    public function actionIndex()
    {
        $criteria = new CDbCriteria();
        $count = Actions::model()->frontView()->count($criteria);

        $pages = new CPagination($count);
        $pages->pageSize = 10;
        $pages->applyLimit($criteria);

        $items = Actions::model()->frontView()->findAll($criteria);

        $this->render('index', compact('items', 'pages'));
    }

    public function actionView()
    {
        $id = Yii::app()->request->getParam('id');
        $item = Actions::model()->findByPk($id);
        $this->itemTitle = $item->title;
        $this->keywords = $item->meta_k;
        $this->description = $item->meta_d;
        $this->render('view', array(
            'item' => $item
        ));
    }
}