<?php

class PagesController extends ElPresentController
{
    public function actionShow()
    {
        $alias = $this->getRequest()->getParam('alias', null);

        $page = TextRecord::model()->findByAttributes([
            'alias'=>$alias
        ]);

        if (!$page || !$page->isEnabled()) {
            throw new CHttpException(404, 'Страница не найдена :(');
        }

        // todo: remove
        $GLOBALS['meta_title'] = $page->meta_title;
        $GLOBALS['meta_description'] = $page->meta_description;
        $GLOBALS['meta_keywords'] = $page->meta_keywords;
//        $this->itemTitle = $page->meta_title;
//        $this->description = $page->meta_description;
//        $this->keywords = $page->meta_keywords;

        $customTemplate = Yii::getPathOfAlias('application.views.pages') . '/' . $alias . '.php';
        if(file_exists($customTemplate)) {
            return $this->render($alias, compact('page'));
        }

        return $this->render('default', compact('page', 'alias'));
    }


}
