<?php
class WherebuyController extends ElPresentController
{
    public $itemTitle = 'Где купить?';
    public $description = 'Где купить?';
    public $keywords = 'Где купить?' ;

    public function actionIndex()
    {
        $city = Yii::app()->request->getParam('city');

        $points = [];

        $cities = City::getCityList();

        if(empty($city)){
            $this->redirect('wherebuy/minsk');
        }

        $items = Wherebuy::model()->findAll([
            'condition' => 'city=:city AND `show`=:show',
            'params' => [':city'=> $city, ':show' => 1],
        ]);

        foreach($items as $k => $item){
            $coordinates = $item->coordinates;
            $coordinates = explode(',', $coordinates);
            $points[] = [
                "type" => "Feature",
                "id" => $item->id,
                "geometry" => [
                    "type" => "Point",
                    "coordinates" => $coordinates,
                ],
                "properties" => [
                    "balloonContent" => $item->title,
                    "clusterCaption" => $item->title,
                    "hintContent" => $item->caption
                ]
            ];
        }

        $data = [
            "type" => "FeatureCollection",
            "features" => $points
        ];

        $json = json_encode($data);

        $this->render('index', compact('items', 'cities', 'json' ,  'wherebuy'));
    }


}