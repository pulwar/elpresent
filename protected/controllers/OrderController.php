<?php

Yii::import('application.extensions.assist.AssistPayment');
Yii::import('application.extensions.curl.Curl');

class OrderController extends ElPresentController {

    public $itemTitle;
    public $option_id;

    protected function getRequstedCode() {
        return implode($this->getRequest()->getQuery('code', array()));
    }

    public function actionStat(){

        $err = '';
        $orderItems = [];
        $partner_name = '';
        $date_start = '';
        $date_end = '';

         $request = $this->getRequest();

         if ($is_submit = $request->getPost('submit', false) && $request->getPost('pass') !== '') {

            $password = mysql_escape_string($request->getPost('pass'));
            $password = md5($password);

            if ($partner = Partner::model()->findByAttributes(['password' => $password])) {
                $partner_name = $partner->name;

                $date_start = $request->getPost('date_start');
                $date_end = $request->getPost('date_end');

                 $criteria = new CDbCriteria();
                 $dateField = 'recivier_activation_date';

                if($date_start) {
                     $date = CDateTimeParser::parse($date_start, 'yyyy-MM-dd');
                     $criteria->addCondition($dateField . " > '" . date('Y-m-d', $date) . "'" );
                 }else{
                     $err = 'Дата не заполнена';

                 }
                if($date_end) {
                    $date = CDateTimeParser::parse($date_end, 'yyyy-MM-dd');
                    $criteria->addCondition($dateField . " < '" . date('Y-m-d', $date) . "'" );
                }else{
                     $err = 'Дата не заполнена';

                }
            if(!$err){
                $criteria->addCondition("activation_partner_id ='".$partner->id."'");
                     $criteria->order = 'id ASC';

              $orderItems = OrderItems::model()->findAll($criteria);

//                        $orderItems = new CActiveDataProvider('OrderItems', array(
//                             'criteria' => $criteria,
//
//                        ));
            }

            }else {
                $err = 'Введен неправильный пароль';
            }
         }

        $this->render('stat', [

            'err' => $err,
            'orderItems' => $orderItems,
            'partner_name' => $partner_name,
            'date_start' => $date_start,
            'date_end' => $date_end,

        ]);
    }

    public function actionInfo() {
        $in_6_month = 3600 * 24 * 30 * 6;
        $in_3_month = 3600 * 24 * 30 * 3;
        $items_for_6_month = [
            'Двухэтапный мастер-класс «Город»',
            'Курс «Штурман: боевое крещение»',
            'Курс «Штурман: раллийный драйв»',
            'Курс начинающих водителей',
            'Урок вождения: маневрирование',
            'Мастер-класс вождения на выбор',
            'Парковочный курс',
            'Правильная парковка',
            'Программа контраварийного вождения',
            'Раллийный день',
            'Раллийный день  для двоих',
            'Раллийный день для компании',
            'Урок контраварийного вождения',
            'Урок контраварийного вождения для двоих',
            'Универсальная подарочная карта'
        ];

        $title = null;
        $options_info = [];
        $option_id = null;
        $option_name = null;
        $activation_date = null;
        $end_date = '';
        $err = null;
        $price_order = null;
        $is_activated = null;
        $code = null;
        $company_name = null;
        $activated = false;
        $how_activated = '';
        $partner_name = '';

        $request = $this->getRequest();

        if ($is_submit = $request->getPost('submit', false) && $request->getPost('pass') !== '') {
            $password = mysql_escape_string($request->getPost('pass'));
            $password = md5($password);

            if ($partner_name = Partner::model()->findByAttributes(['password' => $password])->name) {


                if ($code = trim($request->getPost('code'))) {
                    /** @var OrderItems $model */
                    if ($order_item = OrderItems::model()->findByAttributes(['code' => $code])) {


                        $item = Item::model()->findByPk($order_item->id);

                        $company = Partner::model()->findByPk($item->partner_id);
                        $company_name = $company->name;
                        $options_info = [];
                        $order_item_date = strtotime($order_item->orderObj->order_date);
                        $title = $order_item->itemObj->title;

                        if (in_array($title, $items_for_6_month)) {
                            $end_date = $order_item_date + $in_6_month;
                        } else {
                            $end_date = $order_item_date + $in_3_month;
                        }

                        $today = time();

                        if ($end_date >= $today && $order_item->activated == Order::INACTIVE) {
                            $activation = true;
                        }



                        if($order_item->activated == Order::ACTIVE){

                             $is_activated = 'Да';

                             if($order_item->activation_partner_id){
                                 $how_activated = 'Компанией: '. Partner::model()->findByPk($order_item->activation_partner_id)->name;
                             } else {
                                $how_activated = 'Клиентом: '.$order_item->recivier_name.' '.$order_item->recivier_phone;
                             }

                        } else {
                            $is_activated = 'Нет';
                        }

                        $activation_date = $order_item->recivier_activation_date;
                        $city_id = $order_item->orderObj->city_id;
                        $options = $order_item->itemObj->prices_cache[$city_id];
                        $option_id = $order_item->option_id;
                        $price_order = $order_item->orderObj->price;

                        $option_name = Option::model()->findByPk($option_id)->name;

                        foreach ($options as $option_id => $price) {
                            $options_info[] = Item::getOption($option_id);
                        }
                    } else {
                        $err = 'Введен неправильный код';
                    }
                } else {
                    $err = 'Код пуст';
                }
            } else {
                $err = 'Введен неправильный пароль';
            }
        } elseif ($request->getPost('repay', false) && $request->getPost('pass') !== '') {

            $password = mysql_escape_string(intval($request->getPost('pass')));
            $password = md5($password);

            if ($partner = Partner::model()->findByAttributes(['password' => $password])) {

                $code = trim($request->getPost('code'));
                $order_item = OrderItems::model()->findByAttributes(['code' => $code]);

                $orderItem = OrderItems::model()->findByAttributes(array('code' => $code));

                $orderItem->activated = OrderItems::ACTIVE;
                $orderItem->recivier_activation_date = (string) date('Y-m-d');
                $orderItem->activation_partner_id = $partner->id;

                    $isGroupItem = $orderItem->itemObj->type == 'group';
                            if ($isGroupItem) {
                                $activationItem = (string) $request->getParam('activated_item');
                                foreach ($orderItem->itemObj->childs as $child) {
                                    if ($child->id == $activationItem) {
                                        $orderItem->activated_item = $activationItem;
                                        break;
                                    }
                                }
                                if(empty($orderItem->activated_item)){
                                    throw new CHttpException('Error 404. Not Fount this Activation Item!');
                                }
                            }

                if ($orderItem->save()) {
                    $email = $this->sendEmailPresentActivated($orderItem);
                    $is_activated = 'Успешно активирован';
                }else{
                    $err = 'При активации произошла ошибка';
                }
            }else {
                        $err = 'Введен неправильный пароль';
            }
        }


        $this->render('info', [
            'code' => $code,
            'activation' => $activation,
            'is_activated' => $is_activated,
            'title' => $title,
            'price_order' => $price_order,
            'company_name' => $company_name,
            'options_info' => $options_info,
            'option_id' => $option_id,
            'option_name' => $option_name,
            'end_date' => date('d.m.Y', $end_date),
            'activation_date' =>  $activation_date,
            'err' => $err,
            'partner_name' => $partner_name,
            'how_activated' => $how_activated,
        ]);
    }

    /** todo: вернуться */
    protected function checkCode(OrderItems $model = null) {
        $error = false;
        $message = '';

        if (is_null($model) || $model->code == '000000000000') {
            $error = true;
            $message = 'Неверный код активации';
        }

        if (!$error && $model->getIsActivated()) {
            $error = true;
            $message = 'Подарок уже активирован';
        }
        if (!$error && $model->isOverdue()) {
            $error = true;
            $message = 'Срок активации истек';
        }

        if (!$error && $model->orderObj->is_canceled == 1) {
            $error = true;
            $message = 'Сертификат отменен';
        }

        if ($error) {
            echo json_encode(array('error' => $message));
            exit();
        }
    }

    public function actionCode() {
        $code = $this->getRequstedCode();
        /** @var OrderItems $orderItem */
        $orderItem = OrderItems::model()->findByAttributes(array('code' => $code));
        $this->checkCode($orderItem);

        $data = array();
        $data['rawCode'] = $code;

        $data['title'] = $orderItem->itemObj->title;
        if ($orderItem->itemObj->type == Item::TYPE_ITEM) {
            if(!empty($orderItem->optionObj)) {
                $data['title'] .= ' ('. $orderItem->optionObj->small_name.')';
            }
        }

        if ($orderItem->itemObj->type == Item::TYPE_GROUP) {
            $item = Superimpression::model()->findByPk($orderItem->itemObj->id);
            $data['items'] = array();
            /** @var Item $child */
            $itemsData = $orderItem->group_item_price_data;
            foreach ($itemsData as $itemId => $itemData) {
                $optionTitle = '';
                if (!empty($itemData['option_name'])) {
                    $optionTitle = ' ('.$itemData['option_small_name'].')';
                }

                $data['items'][$itemId] = $itemData['title'].$optionTitle;
            }
        }
        $data['code'] = implode('-', str_split($code, 4));
        $data['href'] = $this->createUrl('catalog/item', array('perm_link' => $orderItem->itemObj->perm_link));

        echo json_encode($data);
    }

    /**
     * Present activation
     */
    public function actionActivation() {
        $request = $this->getRequest();

        $code = $request->getParam('code', 'string');

        /** @var OrderItems $orderItem */
        $orderItem = OrderItems::model()->findByAttributes(array('code' => $code));
        $this->checkCode($orderItem);

        $orderItem->setScenario('activation');
        // todo: replace with setAttributes
        $orderItem->recivier_name = (string) $request->getParam('recivier_name');
        $orderItem->recivier_lname = (string) $request->getParam('recivier_lname');
        $orderItem->recivier_phone = (string) $request->getParam('recivier_phone', 'temp');
        $orderItem->recivier_email = (string) $request->getParam('recivier_email');
        $orderItem->recivier_date = (string) $request->getParam('recivier_date');
        $orderItem->recivier_activation_date = (string) date('Y-m-d');
        $orderItem->activated = OrderItems::ACTIVE;

        $isGroupItem = $orderItem->itemObj->type == 'group';
        if ($isGroupItem) {
            $activationItem = (string) $request->getParam('activated_item');
            if (!empty($orderItem->group_item_price_data[$activationItem])) {
                $orderItem->activated_item = $activationItem;
                $itemData = $orderItem->group_item_price_data[$activationItem];
                $orderItem->partner_id = $itemData['partner_id'];
                $orderItem->partner_price = $itemData['partner_price'];
                $orderItem->option_id = $itemData['option_id'];
                $orderItem->activated_item_price = $itemData['price'];
            }
            if (empty($orderItem->activated_item)) {
                throw new CHttpException('Error 404. Not Fount this Activation Item!');
            }
        }

        if ($orderItem->save()) {
            $email = $this->sendEmailPresentActivated($orderItem);
            $results = [
                'ok' => true,
                'email' => $email
            ];

            /** @var Item $item */
            $item = ($isGroupItem && !empty($orderItem->activatedItemObj)) ? $orderItem->activatedItemObj : $orderItem->itemObj;
            /** Todo: default city_id=1, change it.*/
            $partner = Partner::model()->find('city_id=:city AND id=:id', [':city'=> 1, ':id' => $orderItem->partner_id]);

            foreach (['contact_person', 'adress', 'place', 'lat', 'lon', 'phone'] as $key) {
                $results[$key] = $partner->$key;
            }

            echo json_encode($results);
            if ($orderItem->recivier_email) {
//                $this->emailUserActivationToPartner($partner, $orderItem, $item);
                $this->emailUserExplanation($partner, $orderItem, $item);
            }

            exit();
        }

        //Make list of errors
        // todo: clear that trash
        $errors = $orderItem->getErrors();
        $error = call_user_func_array('array_merge', $errors);
        $error = implode('<br />', $error);
        echo json_encode(array('error' => $error));
    }

    /**
     * This action handle
     * @throws Exception
     */
    public function actionBuyone() {
        $rq = $this->getRequest();
        $cart = Cart::getCart();
        
        //Clean cart, before buy one item
        Cart::clearCart();
        $cart->addItemById($rq->getQuery('itemId'), $rq->getQuery('optionId'), $rq->getQuery('type', 'general'));
        $this->redirect($this->createUrl('order/buy'));
    }

    /**
     * Process buy
     * @throws CHttpException
     */
    public function actionBuy()
    {
        $request = $this->getRequest();
        $cartItems = Cart::getCart()->getItems();

        if (! count($cartItems)) {
            $this->redirect('/');
        }

        $city = $request->getQuery('_city');

        /** @var Item[] $thematicDelivery */
        // todo: make delivery manager service or something...

        /** AR bug from this method!!!! Reset scope after???*/
        $thematicDelivery = PaymentType::getThematicDeliveryOptions($city);

        $order = new Order();
        $order->city_id = $city->id;
        $order->type = Order::TYPE_BASIC;

        $isDigitalCert = Cart::getCart()->checkDigitalOrder();
        if ($isDigitalCert) {
            $order->type = Order::TYPE_DIGITAL;
            $items = Cart::getCart()->getItems();
            /** @var Item  $item */
            $item = array_shift($items);
            if (!$item->isDigitalCertificateAvailable($city)) {
                throw new CHttpException(404);
            }
        }

        if ($request->getIsPostRequest()) {


            if ($order->bindRequest($request)->detectScenario()->validate()) {
                // attempt to create new user
                $user = User::createUserFromOrder($order);
                $order->user_id = $user->id;
                /** Check user discount */
                //$order->user_cash = $request->getParam('Order')['cash'];
                //$order->user_cash = ($order->user_cash > $user->cash) ? $user->cash : $order->user_cash;
                //$order->user_cash = empty($order->user_cash) ? 0 : (float) $order->user_cash;

                // просчитываем списание с бонусного. отмечен чекбокс
                if($request->getParam('Order')['cash_check'] && ($user->cash > 0 )) {
                    $order->calcTotalPrice($user->cash);
                    $user->cash = $user->cash - $order->user_cash;

                } else {
                    $order->calcTotalPrice(0);
                }



                if ($order->save()) {
                    $user->update();
                    foreach (Cart::getCart()->toArray() as $itemId => $optionId) {
                        /** @var Item $currentItem */
                        $currentItem = $cartItems[$itemId];
                        $orderItem = new OrderItems();
                        $orderItem->order_id = $order->id;
                        $orderItem->item_id = $itemId;
                        $orderItem->option_id = (int) $optionId;
                        $orderItem->item_title = $currentItem->title;
                        $orderItem->item_price = $currentItem->getPrice($optionId, $city);
                        $orderItem->item_price_with_discount = $currentItem->getRoundedPrice($optionId, $city);
                        $orderItem->discount_percent = empty($currentItem->off_price) ? 0 : $currentItem->off_price;
                        $orderItem->discount_price = $orderItem->item_price * ($orderItem->discount_percent / 100);
                        $orderItem->code = OrderItems::model()->generateUniqCode(true);
                        $orderItem->save();
                        // set date expirience
                        if($currentItem->start_season_date != null) {
                            $date = new DateTime($currentItem->start_season_date);
                        } else {
                            $date = new DateTime($order->order_date);
                        }
                        date_modify($date, '+' . $currentItem->months_of_validity . ' month');
                        $order->validity_date = $date->format('Y-m-d');
                        $order->save();
                    }

                    switch ($order->paymentType) {
                        case PaymentType::EASYPAY:
                        case PaymentType::WEBPAY:
                            $this->redirect($this->createUrl('order/invoice', ['orderNumber' => $order->order_number]));
                            break;
                        case PaymentType::BEPAID_CARD:
                        case PaymentType::BEPAID_HALVA:
                            $this->redirect($this->createUrl('order/invoice', ['orderNumber' => $order->order_number]));
                            break;
                        case PaymentType::COURIER:
                            $this->redirect($this->createUrl('order/thanks', ['orderNumber' => $order->order_number]));
                            break;
                        default:
                            throw new InvalidArgumentException('PaymentType is Invalid!!!');
                    }
                }
            }
        }

        $this->render(
                $isDigitalCert ? 'digital_form' : 'buy_form', [
            'order' => $order,
            'thematicDelivery' => $thematicDelivery,
            'cart_items' => $cartItems,
            'city_id' => $city->id,
            'prices_thematic' => Order::getPricesThematic(),
            'user' => User::model()->findByPk(Yii::app()->user->getId())
                ]
        );
    }

    /**
     * Render Thank you page
     * @param int $orderNumber
     */
    public function actionThanks($orderNumber) {
        /** @var Order $order */
        $order = Order::model()->findByAttributes(['order_number' => $orderNumber]);
        $session = $this->get('session');
        $pack = $session['pack'];
        // try to create from session
        if (is_array($pack)) {
            $order->preparePacking($pack);
        }

        $this->render('thanks', [
            'order' => $order,
        ]);
    }

    /**
     * @param string $view
     * @return true
     */
    public function beforeRender($view) {
        switch ($view) {
            case 'webpay_complete':
            case 'bepaid_complete':
                Cart::getCart()->clearCart();
                break;
            case 'thanks':
                Cart::getCart()->clearCart();
                break;
            default:
                break;
        }

        return true;
    }

    public function actionInvoice($orderNumber) {
        $result = null;
        /** @var Order $order */
        $order = Order::model()->findByAttributes(['order_number' => $orderNumber]);
        $session = $this->get('session');
        $pack = $session['pack'];
        // try to create from session
        if (is_array($pack)) {
            $order->preparePacking($pack);
        }

        if (!$order->is_paid) {
            switch ($order->payment_type_id) {
                case PaymentType::EASYPAY:
                    if (!empty($_POST['card'])) {
                        $card = (string) trim($_POST['card']);
                        $result = EasyPay::CreateInvoice($order, $card);
                        if ($result == 200) {
                            $result = '<div class="alert alert-success">Счет выписан. Зайдите в кабинет EasyPay и оплатите счет</div>';
                        } else {
                            $result = '<div class="alert alert-danger">' . $result . '</div>';
                        }
                    }
                    if (!empty($_POST['get_sertificat'])) {
                        $result = EasyPay::IsInvoicePaid($order->id);
                        if ($result == 200) {
                            $order->setPaid()->save();
                            $_SESSION['certificate_id'] = $order->id;
                            $result = '<div class="alert alert-success">Сертификат выслан</div>';
                        } else {
                            $result = '<div class="alert alert-danger">Ваш счет не оплачен! Сертификат не может быть выслан!</div>';
                        }
                    }
                    break;
                case PaymentType::WEBPAY:
                    /** @var WebPayManager $paymentManager */
                    $paymentManager = $this->get('webpay');
                    $paymentManager->createInvoice($order);
                    break;
                case PaymentType::BEPAID_CARD:
                    /** @var AbstractBepaidManager $paymentManager */
                    $paymentManager = $this->get('manager.bepaid.card');
                    $paymentManager->createInvoice($order);
                    break;
                case PaymentType::BEPAID_HALVA:
                    /** @var AbstractBepaidManager $paymentManager */
                    $paymentManager = $this->get('manager.bepaid.halva');
                    $paymentManager->createInvoice($order);
                    break;

                case PaymentType::COURIER:
                default:
                    throw new CHttpException('Error 404.');
            }
            $this->render('invoice', [
                'order' => $order,
                'result' => $result
            ]);
        } else {
            $this->render('payment/already_paid');
        }
    }

    /**
     * Webpay success
     *
     * @throws CHttpException
     */
    public function actionWsbcomplete() {
        $request = $this->getRequest();
        $wsb_order_num = $request->getParam('wsb_order_num');
        $wsb_tid = $request->getParam('wsb_tid');

        if (empty($wsb_order_num) || empty($wsb_tid)) {
            throw new CHttpException('404');
        }

        $webpay = Webpay::model()->findByAttributes(array('wsb_order_num' => $wsb_order_num));

        $webpay->wsb_tid = $wsb_tid;
        $webpay->save();

        $_SESSION['certificate_id'] = $webpay->order_id;

        $order = Order::model()->findByPk($webpay->order_id);

        $this->render(
                'webpay_complete', [
            'webpay_complete_message' => Yii::app()->params['webpay_complete_message'],
            'wsb_order_num' => $wsb_order_num,
            'wsb_tid' => $wsb_tid,
            'order_id' => $webpay->order_id,
            'type' => (!empty($order->type) && $order->type == Order::TYPE_DIGITAL) ? $order->type : null,
        ]);
    }

    /**
     * Bepaid success
     *
     * @throws CHttpException
     */
    public function actionBepaidcomplete() {
        /** @var CHttpRequest $request */
        $request = Yii::app()->request;
        $token = $request->getQuery('token');
        $uid = $request->getQuery('uid');
        $status = $request->getQuery('status');
        if (empty($token) || empty($uid) || $status !== 'successful') {
            throw new CHttpException('404');
        }

        $manager = FactoryBepaidManager::getByToken($token);
        if ($result = $manager->getInvoice($token, $uid)) {
            $order = Order::model()->findByPk($result->order_id);
            if ($order->is_paid == '1') {
                $_SESSION['certificate_id'] = $order->id;
                $order->setPaid()->save();
                $this->render(
                    'bepaid_complete',
                    [
                        'bepaid_complete_message' => Yii::app()->params['bepaid_complete_message'],
                        'order_id' => $order->id,
                        'type' => (!empty($order->type) && $order->type == Order::TYPE_DIGITAL) ? $order->type : null,
                    ]
                );
                header("HTTP/1.0 200 Ok");
                Yii::app()->end();
            }
        } else {
            throw new CHttpException('404');
        }
    }

    /**
     * Bepaid fail
     *
     * @throws CHttpException
     */
    public function actionBepaidfail() {
        /** @var CHttpRequest $request */
        $request = Yii::app()->request;
        $token = $request->getQuery('token');
        $uid = $request->getQuery('uid');
        $status = $request->getQuery('status');

        if (empty($token) || empty($uid) || $status !== 'failed') {
            throw new CHttpException('404');
        }

        $manager = FactoryBepaidManager::getByToken($token);
        if ($result = $manager->getInvoice($token, $uid)) {
            $this->render(
                'bepaid_cancel', [
                    'bepaid_fail_message' => Yii::app()->params['bepaid_fail_message']
                ]
            );
        } else {
            throw new CHttpException('404');
        }
    }

    /**
     * Bepaid cancel
     *
     * @throws CHttpException
     */
    public function actionBepaidcancel() {
        /** @var CHttpRequest $request */
        $request = Yii::app()->request;
        $token = $request->getQuery('token');
        $uid = $request->getQuery('uid');
        $status = $request->getQuery('status');


        if (empty($token) || empty($uid) || $status == '') {
            throw new CHttpException('404');
        }

        $manager = FactoryBepaidManager::getByToken($token);
        if ($result = $manager->getInvoice($token, $uid)) {
            $this->render(
                'bepaid_cancel', [
                    'bepaid_cancel_message' => Yii::app()->params['bepaid_cancel_message']
                ]
            );
        } else {
            throw new CHttpException('404');
        }
    }

    /**
     * Bepaid decline
     *
     * @throws CHttpException
     */
    public function actionBepaiddecline() {
        /** @var CHttpRequest $request */
        $request = Yii::app()->request;
        $token = $request->getQuery('token');
        $uid = $request->getQuery('uid');
        $status = $request->getQuery('status');


        if (empty($token) || empty($uid) || $status == '') {
            throw new CHttpException('404');
        }

        $manager = FactoryBepaidManager::getByToken($token);
        if ($manager->getInvoice($token, $uid)) {
            $this->render(
                'bepaid_cancel', [
                    'bepaid_decline_message' => Yii::app()->params['bepaid_decline_message']
                ]
            );
        } else {
            throw new CHttpException('404');
        }
    }

    public function actionIspdfexist() {
        $file = Yii::getPathOfAlias('webroot.assets') . '/pdf_tmp/' . $_SESSION['certificate_id'] . '.pdf';

        if (is_file($file)) {
            echo 1;
        } else {
            echo 0;
        }
        exit;
    }

    public function actionDownloadcertificate() {
        if (empty($_SESSION['certificate_id'])) {
            throw new CHttpException('404');
        } else {
            $file = Yii::getPathOfAlias('webroot.assets') . '/pdf_tmp/' . $_SESSION['certificate_id'] . '.pdf';
            $file_html = Yii::getPathOfAlias('webroot.assets') . '/pdf_tmp/' . $_SESSION['certificate_id'] . '.html';
            if (is_file($file)) {
                // сбрасываем буфер вывода PHP, чтобы избежать переполнения памяти выделенной под скрипт
                // если этого не сделать файл будет читаться в память полностью!
                if (ob_get_level()) {
                    ob_end_clean();
                }
                // заставляем браузер показать окно сохранения файла
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename=' . basename($file));
                header('Content-Transfer-Encoding: binary');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($file));
                // читаем файл и отправляем его пользователю
                readfile($file);
                unset($_SESSION['certificate_id']);
                @unlink($file);
                @unlink($file_html);
                exit;
            } else {
                throw new CHttpException('404');
            }
        }
    }

    public function actionWsbcancel() {
        $request = $this->getRequest();
        $wsb_order_num = $request->getParam('wsb_order_num');

        if (empty($wsb_order_num)) {
            // todo: we also doesn't have history anymore
            $this->redirect('/profile/history', true);
        }

        $this->render('webpay_cancel', array(
            'webpay_cancel_message' => Yii::app()->params['webpay_cancel_message'],
        ));
    }

    public function actionWsbnotify() {
        $request = $this->getRequest();

        /** @var WebPayManager $webpayManager */
        $webpayManager = $this->get('webpay');

        if (!$webpayManager->validateRequest($request)) {
            throw new CHttpException('404 Invalid Request1');
        }

        $invoice = $webpayManager->getInvoice($request->getParam('site_order_id'));
        if (!$webpayManager->processInvoice($invoice, $request->getParam('transaction_id'))) {
            throw new CHttpException('404 process crash processInvoice!');
        }

        // order is paid, so...
        $order = $invoice->getOrder();
        $order->setPaid()->save();
        header("HTTP/1.0 200 Ok");
        Yii::app()->end();
    }

    public function actionFlowers() {
        $flowers = Flower::getProvider(100);
        $this->render('pack/flowers', ['flowers' => $flowers->getData()]);
    }

    public function actionMakeRequest() {
        $request = $this->getRequest();
        $city = $request->getQuery('_city');
        $attributes = $request->getPost('OrderRequest');
        if ($request->getIsPostRequest() && !empty($attributes)) {
            $orderRequest = new Order('fast_order');
            $orderRequest->type = Order::TYPE_BASIC;
            $orderRequest->name = $attributes['name'];
            $orderRequest->lname = '';
//            $name = preg_split('/\s+/', $attributes['name']);
//            if (count($name) > 1) {
//                $orderRequest->name = $name[0];
//                $orderRequest->lname = $name[1];
//            }
            $orderRequest->phone = $attributes['phone'];
            $orderRequest->email = $attributes['email'] ?? 'not-email@elpresent.by';

            $itemId = (int) $attributes['item_id'];
            /** @var Item $item */
            $item = Item::model()->findByPk($itemId);

            if ($item) {
                $orderRequest->price = $item->getRoundedPrice($request->getPost('optionId'), $city);
            }

            $orderRequest->order_number = Order::generateUniqueOrderNumber();
            $orderRequest->is_fast_order = 1;

            /** Todo: This invalid feture! Check! Get otion from GET or POST? Not from cookie. */
//            $orderRequest = $this->getOptionIdByCookies($orderRequest, $item);

            $ok = $orderRequest->save(true);

            $orderItem = new OrderItems();
            $orderItem->order_id = $orderRequest->id;
            $orderItem->item_id = $itemId;
            $orderItem->option_id = $request->getPost('optionId');
            $orderItem->item_title = $item->title;
            $orderItem->item_price = $item->getPrice($orderItem->option_id, $city);
            $orderItem->item_price_with_discount = $item->getRoundedPrice($orderItem->option_id, $city);
            $orderItem->discount_percent = empty($item->off_price) ? 0 : $item->off_price;
            $orderItem->discount_price = $orderItem->item_price * ($orderItem->discount_percent / 100);
            $orderItem->code = OrderItems::model()->generateUniqCode(true);
            $orderItem->save();

            // set date expirience
            if($item->start_season_date != null) {
                $date = new DateTime($item->start_season_date);
            } else {
                $date = new DateTime($orderRequest->order_date);
            }
            date_modify($date, '+' .$item->months_of_validity . ' month');
            $orderRequest->validity_date = $date->format('Y-m-d');
            $orderRequest->save();

            echo json_encode(compact('ok'));
        } else {
            throw new CHttpException(405, 'Method not allowed');
        }
    }

    public function getLeftMenuWidget() {
        return array('ProfileWidget', 'CategoriesWidget');
    }

    // todo: move to event listener
    protected function emailUserActivation(Order $order) {
        /** @var Mailer $mailer */
        $mailer = Yii::app()->mailer;
        $message = $mailer->createMessageFromView(
                sprintf('Активация подарка %s', $order->item->title), Yii::getPathOfAlias('application.views.system') . '/userActivationNotification.php', [
            'oModel' => $order,
            'serverName' => Yii::app()->params['cronServer']['pathInfo']
                ]
        );

        $message->setTo($order->email, $order->getFullName());
        $mailer->send($message);
    }

    // todo: move to event listener
    protected function emailUserOrdersNotification(Order $order, Order $previousOrder = null) {
        /** @var Mailer $mailer */
        $mailer = Yii::app()->mailer;
        $message = $mailer->createMessageFromView(
                sprintf('Ваш заказ #%s', $order->order_number), Yii::getPathOfAlias('application.views.system') . '/userOrderNotification.php', [
            'order' => $order,
            'previousOrder' => $previousOrder,
            'serverName' => Yii::app()->params['cronServer']['pathInfo']
                ]
        );

        $message->setTo($order->email, $order->getFullName());
        $mailer->send($message);
    }

    /**
     * @param Partner    $partner
     * @param OrderItems $orderItem
     * @param Item       $activeItem
     */
    protected function emailUserExplanation(Partner $partner, OrderItems $orderItem, Item $activeItem) {
        /** @var Mailer $mailer */
        $mailer = Yii::app()->mailer;
        $message = $mailer->createMessageFromView(
                'Информация об активированном подарке', Yii::getPathOfAlias('application.views.system') . '/userExplanation.php', [
            'partner' => $partner,
            'orderItem' => $orderItem,
            'activeItem' => $activeItem,
                ]
        );
        $message->setTo($orderItem->recivier_email);
        $mailer->send($message);
    }

    /**
     * todo: move to event listener
     * @param Partner    $partner
     * @param OrderItems $orderItem
     * @param Item       $activeItem
     */
    protected function emailUserActivationToPartner(Partner $partner, OrderItems $orderItem, Item $activeItem) {
        $toEmail = defined('EL_ENV') && EL_ENV === 'dev' ? Settings::getValueByAlias('order_buy_notification_email') : $partner->email;
        if (!empty($toEmail)) {
            /** @var Mailer $mailer */
            $mailer = Yii::app()->mailer;
            $message = $mailer->createMessageFromView(
                    'Активация подарка', Yii::getPathOfAlias('application.views.system') . '/partnerActivationNotification.php', [
                'orderItem' => $orderItem,
                'activeItem' => $activeItem,
                'serverName' => Yii::app()->params['cronServer']['pathInfo']
                    ]
            );
            $message->setTo($toEmail);
            $mailer->send($message);
        }
    }

    // todo: move to notifications service
    protected function emailOrdersNotification(Order $order) {
        /** @var Mailer $mailer */
        $mailer = Yii::app()->mailer;
        $message = $mailer->createMessageFromView(
                sprintf('Заказ #%s', $order->order_number), Yii::getPathOfAlias('application.views.order') . '/orderNotification.php', [
            'order' => $order,
            'serverName' => Yii::app()->params['cronServer']['pathInfo']
                ]
        );

        $message->setTo(Settings::getValueByAlias('order_buy_notification_email'));
        $mailer->send($message);
    }

    // todo: move to notifications service
    /**
     * @deprecated
     *
     * @param $model
     *
     * @return bool
     * @throws CException
     */
    protected function sendEmailBuyNotification($model) {
        return $this->sendAdminEmail(_('Present offered'), $this->renderPartial('buyNotification', array('model' => $model), true));
    }

    /**
     * @param OrderItems $model
     * @return bool
     */
    protected function sendEmailPresentActivated($model) {
        return $this->sendAdminEmail(
                        'Подарок активирован', $this->renderPartial('activatedNotification', ['model' => $model], true)
        );
    }

    // todo: move to notifications service
    /**
     * @deprecated
     *
     * @param $subj
     * @param $body
     *
     * @return bool
     */
    protected function sendAdminEmail($subj, $body) {
        email::EMailerSmtpSend(
                Yii::createComponent('application.extensions.mailer.EMailer'), Yii::app()->getParams()->review['emailTo'], $subj, $body, Yii::app()->params['emailer']
        );

        //return mail('info@elpresent.by', $subj, $body, "From: robot@elpresent.by\r\nContent-type: text/html;charset=utf-8\r\n\r\n");
    }

    /**
     * Print certificate
     * @param int $id
     * @param int $site
     * @throws CHttpException
     */
    public function actionPrint($id, $site = null) {

        if (empty($site)) {
            $model = RemoteSalesOrders::model()->findByPk($id);
            $option = ItemCityLink::model()->find('item_id=:item_id AND item_price=:item_price AND city_id=1 AND available=1', [
                ':item_id' => $model->item_id,
                ':item_price' => $model->sum
            ]);
            $model->option_name = Option::model()->findByPk($option->option_id)->name;

            $orderItem = OrderItems::model()->findByAttributes(array('code' => $model->code));

            $this->render(
                'print/print-items', [
                    'order' => $orderItem->orderObj,
                ]
            );
        } else {
            $order = Order::model()->findByPk($site);
            if (!empty($order)) {
                $this->render(
                        'print/print-items', [
                    'order' => $order,
                        ]
                );
                exit();
            } elseif ($orderItem = OrderItems::model()->findByPk($site)) {
                $this->render(
                        'print/print-items', [
                    'order' => $orderItem->orderObj,
                        ]
                );
                exit();
            } else {
                throw new CHttpException(404);
            }
        }
    }

    public function actionWord($site) {

        //$order = Order::model()->findByPk($site);

        //$orderItem = $order->orderItems[0];

        $orderItem = OrderItems::model()->findByPk($site);

        //echo date('H:i:s'), ' Creating new TemplateProcessor instance...' . "<br>";
        if($orderItem->itemObj->type == 'group') {
            $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor('upload/resources/many.docx');
        } else {
            $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor('upload/resources/one.docx');

        }
        // Variables on different parts of document
        $templateProcessor->setValue('weekday', date('l'));            // On section/content
        $templateProcessor->setValue('time', date('H:i'));             // On footer
        $templateProcessor->setValue('serverName', realpath(__DIR__)); // On header



        # заполняем
        if($orderItem->itemObj->type == 'group') {
            $templateProcessor->setValue('type', 'Комплект впечатлений');
        } else {
            $templateProcessor->setValue('type', 'Впечатлениe');
        }

        $templateProcessor->setValue('title', $orderItem->itemObj->title);

        # код активации
        if(!empty($orderItem->code)){
            $templateProcessor->setValue('code', $orderItem->code);
        } else {
            $templateProcessor->setValue('code', 'Код не сгененирован');
        }

        # использовать до
        $templateProcessor->setValue('date', date('d.m.Y', strtotime($orderItem->orderObj->validity_date)));

        # вариант
        if(!empty($orderItem->optionObj)) {
            $templateProcessor->setValue('variant-name', 'Вариант:');
            $templateProcessor->setValue('variant', $orderItem->optionObj->name);
        } else {
            $templateProcessor->setValue('variant-name', '');
            $templateProcessor->setValue('variant', '');
        }

        $str = '';
        if ($orderItem->itemObj->type == 'group') {
            foreach ($orderItem->itemObj->childs as $key => $child){
                /** @var Item $child */
                $selfLink = $orderItem->itemObj->getSelfLinkByChildren($child);
                $optionTitle = '';
                if (!empty($selfLink->option)) {
                    $optionTitle = '('.$selfLink->option->small_name.')';
                }
                if($key>0){
                    $str .= "<w:br/>";
                }
                $str .= $key + 1 . '. '. $child->title . $optionTitle;
            }
            $templateProcessor->setValue('choose', 'Выберите одну из услуг:');
            $templateProcessor->setValue('userName', $str);
        } else {
            $templateProcessor->setValue('choose', preg_replace('/&laquo;|&raquo;|«|»/', '"', (trim(strip_tags($orderItem->itemObj->instuction_description)))));
            $str = $orderItem->itemObj->need_to_know;
            $str = str_replace('&nbsp;', ' ', $str);
            $myArray = preg_split('/<strong>/i', $str);
            
            $i = 1;
            foreach ($myArray as  $value) {
                $tmp = explode(':', $value, 2);
                if($i != 1) {
                    $first =  preg_replace('/&laquo;|&raquo;|«|»/', '"', str_replace('&ndash;', '', trim(strip_tags(htmlspecialchars ($tmp[0]))))) . ':';
                    $sechond = preg_replace('/&laquo;|&raquo;|«|»/', '"', str_replace('&ndash;', '', trim(strip_tags($tmp[1]))));
                } else {
                    $first = preg_replace('/&laquo;|&raquo;|«|»/', '"', str_replace('&ndash;', '', trim(strip_tags($tmp[0]))));
                    $sechond = '';
                }
                //if(!in_array($i, [6,7])) {
                    $templateProcessor->setValue("key#$i", $first);
                    $templateProcessor->setValue("val#$i", $sechond);
                //}
                $i++;
            }
            for ($j = $i; $j <= 10; $j++) {
                $templateProcessor->setValue("key#$j", '');
                $templateProcessor->setValue("val#$j", '');
            }
        }

        $templateProcessor->setValue('present', preg_replace("/<br\W*?\/>/", "<w:br/>", $orderItem->orderObj->message));

        //echo date('H:i:s'), ' Saving the result document...' . "<br>";
        $templateProcessor->saveAs('upload/results/word_' . $site . '.docx');

        $Filename = 'word_' . $site;
        $FilePath = 'upload/results/word_' . $site;
        $EXTENTION = 'docx';

        $contentType = 'Content-type: application/vnd.openxmlformats-officedocument.wordprocessingml.document;';
        header ( "Expires: Mon, 1 Apr 1974 05:00:00 GMT" );
        header ( "Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT" );
        header ( "Cache-Control: no-cache, must-revalidate" );
        header ( "Pragma: no-cache" );
        header ( $contentType );
        header ( "Content-Disposition: attachment; filename=".$Filename.'.'.$EXTENTION );
        readfile($FilePath.'.'.$EXTENTION);


        //echo "<a href=\"/upload/results/word_$site.docx\">Download</a>";


    }


    public function actionPrintpdf($id) {
        if(Yii::app()->user->getUser()->user_role == 'admin') {
            $orderItem = OrderItems::model()->findByPk($id);
            if (!empty($orderItem)) {
                switch ($orderItem->itemObj->type) {
                    case 'group':
                        return $this->renderPartial(
                                        '/order/print/print-set-pdf', ['orderItem' => $orderItem]
                        );
                    default:
                        return $this->renderPartial(
                                        '/order/print/print-pdf', ['orderItem' => $orderItem]
                        );
                }
            }
        }
        throw new CHttpException(404);
    }

}
