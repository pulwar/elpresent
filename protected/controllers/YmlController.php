<?php
class YmlController extends ElPresentController
{
    public function actionIndex()
    {
        Yii::app()->clientScript->scriptMap['jquery.js'] = false;
        Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
        Yii::app()->clientScript->scriptMap['jquery.cookie.js'] = false;

        $tree = static::filterTree(Category::getTree(true, true));
        $city = City::getCity();
        $list = Item::getItemsYml($city);
        $offers = $list->getData();

        //html::pr(array_shift($offers[0]->prices_cache[1]),1);
        foreach($offers as $offer){
            if (!is_array($offer->prices_cache[1])){
               //echo $offer->prices_cache[1];
            }
        }
        //die('1');

        $this->render('yml', [
            'url' => 'http://'.$_SERVER['HTTP_HOST'],
            'name' => '«El Present» — оригинальные подарки. Впечатления в подарок.',
            'company' => 'ООО "Интеллектсофт Медиа"',
            'categories' => self::getCategories($tree),
            'delivery_general_price' => Order::DELIVERY_GENERAL_PRICE,
            'offers' => $offers
        ]);
    }

    protected static function filterTree($tree)
    {
        return array_filter($tree, function ($node) {
            return $node['itemsCount'] > 0;
        });
    }

    protected static function getCategories($tree)
    {
        $categories = [];
        $i = 0;
        foreach($tree as $key => $category){
            $categories[$i]['title'] = $category['title'];
            $categories[$i]['id'] = $category['id'];
            foreach($category['children'] as $children){
                $i++;
                $categories[$i]['title'] = $children['title'];
                $categories[$i]['id'] = $children['id'];
                $categories[$i]['parentId'] = $category['id'];
            }
            $i++;
        }

        return (array)$categories;
    }

}