<?php

return array(
    // static section
    'sitemap.xml' => 'site/sitemap',
    'questionnaire' => 'site/questionnaire',
    'admin' => 'admin/order',
    'partner' => 'partner/certificate',
    'admin/user/feature_switch' => 'admin/user/featureSwitch',
    'faq' => 'site/faq',
    '_info' => 'site/info',
    'contacts' => 'site/contact',
    'discount' => 'site/discount',
    // catalog common pages
    'want/<hash:\w+>' => 'catalog/want',
    'gallery' => 'gallery/index',
    'gallery/<id:\d+>' => 'gallery/view',
    'catalog/ajaxsearch' => 'catalog/ajaxsearch',
    'news/<id:\d+>' => 'news/view',
    'photo_upload' => 'site/photo_upload',
    'wherebuy/<city:\w+>' => 'wherebuy',
    'skidki/<params:\S+\/?\S+>' => 'skidki/index',
    // order
    'order/pack' => 'order/pack',

    // static pages
    array(
        'class' => 'application.components.routes.PagesRoute'
    ),
    // catalog
    array(
        'class' => 'application.components.routes.CatalogRoute'
    ),

    'login' => 'site/login',
    'logout' => 'site/logout',
    'forgot_password' => 'site/forgot_password',
    'change_user_password' => 'site/change_user_password',
    'register' => 'site/register',
    'ajax_login' => 'site/ajax_login',
    'ajax_register' => 'site/ajax_register',
    'profile/settings' => 'profile/profile/settings',
    'profile/orders' => 'profile/profile/orders',
    'profile/calendar' => 'profile/profile/calendar',
    'profile/recommend' => 'profile/profile/recommend',
    'profile/event_manage' => 'profile/profile/event_manage',
    'profile/event_delete' => 'profile/profile/event_delete',
    'profile/delete_cart_order/<id:\d+>' => 'profile/profile/delete_cart_order',
    'profile/delete_history_order/<id:\d+>' => 'profile/profile/delete_history_order',
    'profile/order/code' => 'order/code',
    'profile/order/activation' => 'order/activation',
    'cart' => 'cart',
);