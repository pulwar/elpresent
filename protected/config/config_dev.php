<?php
return [
    'components' => [
        'log' => [
            'class' => 'CLogRouter',
//            'routes' => [
//                [
//                    'class' => 'CProfileLogRoute',
//                    'levels' => 'profile',
//                ],
//                [
//                    'class' => 'CFileLogRoute',
//                    'levels' => 'error, warning',
//                ],
//                [
//                    'class' => 'CWebLogRoute',
//                    'levels' => 'error, warning',
//                ],
//            ],
        ],
        'db' => [
            'schemaCachingDuration' => 0,
            //'enableParamLogging' => true,
            //'enableProfiling' => true,
        ],

        'cache' => [
            'class' => 'CDummyCache'
        ],
    ]
];