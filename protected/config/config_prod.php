<?php

return [
    'components' => [
        'log' => [
            'class' => 'CLogRouter',
            'routes' => [
                [
                    'class'=>'CFileLogRoute',
                    'levels'=>'error, warning',
                ],
            ],
        ],
        'db' => [
            'schemaCachingDuration' => 0,
            'enableParamLogging'=> false,
            'enableProfiling'=> false,
        ],

        'cache' => [
            'class' => 'CDummyCache'
        ],
    ]
];