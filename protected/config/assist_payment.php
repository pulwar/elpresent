<? 
return array
(
    'procuction' => array
    (
        'type' => array
        (
            1 => array
            (
                'url' => 'https://secure.assist.ru/shops/purchase.cfm'
            ),
            
            2 => array
            (
                'url' => 'https://secure.assist.ru/shops/cardpayment.cfm'
            )
        ),
        
        'auth_result_url' => 'https://secure.assist.ru/results/results.cfm'
    ),
    
    'test' => array
    (
        'type' => array
        (
            1 => array
            (
                'url' => 'https://test.assist.ru/shops/purchase.cfm'
            ),
            
            2 => array
            (
                'url' => 'https://test.assist.ru/shops/cardpayment.cfm'
            )
        ),
        
        'auth_result_url' => 'https://test.assist.ru/results/results.cfm'
    ),
    'shop_idp'      => 382547,
    'currency'      => 'BYR',
    'status'        => 'test',
    'type'          => 1,
    'login'         => 'Elprezent',
    'password'      => 'o4o42o1o',
    'url_return'    => 'dev.elpresent.by',
    'url_return_ok' => 'dev.elpresent.by',
    'url_return_no' => 'dev.elpresent.by'
);