<?php 

	// base yii path
	defined('YII_PATH') or define('YII_PATH',dirname(__FILE__).DIRECTORY_SEPARATOR.'../../framework');
	defined('YII_DEBUG') or define('YII_DEBUG',true);

	/**
	 * @internal
	 */
	class sDbConfig {
		/** @var string */
		public $driver='mysql';
		/** @var string */
		public $host='127.0.0.1';
		/** @var string */
		public $dbname='c0_elpresentby';
		/** @var string */
		public $user='c0_elpresentby';
		/** @var string */
		public $password='GPZw5@4UgmCoz';

		/**
		 * @param string $name
		 * @param array $arguments
		 * @return mixed
		 */
		public function __call($name, $arguments=null) {
			$access=substr($name,0,3);
			$name=strtolower(substr($name, 3));

			if ('get'==$access) {
				return $this->{$name};
			} else if('set'==$access) {
				$this->{$name}=$arguments[0];
				return $this;
			} else {
				return call_user_func_array(array($this,$name), $arguments);
			}
		}

		/**
		 * @var CDbConfig
		 */
		protected static $instance;
		/**
		 * @return CDbConfig
		 */
		static public function getInstance() {
			return (null===self::$instance) ?
				(self::$instance = new self()) :
				self::$instance;
		}
	}
