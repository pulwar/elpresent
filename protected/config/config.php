<?php

return [
    'basePath'=>dirname(__DIR__),
    'name'=>'Elpresent',

    'import' => [
        'application.models.*',
        'application.models.catalog.*',
        'application.models.elevent.*',
        'application.models.forms.*',
        'application.models.mood.*',
        'application.models.ordering.*',
        'application.components.*',
        'application.components.auth.*',
        'application.components.ordering.*',
        'application.widgets.*',
        'application.extensions.format.*',
        'application.extensions.helpers.*',
        'application.extensions.image.*',
        'application.extensions.utils.*',
        'application.extensions.payment.*',
        'application.extensions.gallery.*',
        'application.extensions.gallery.models.*',
        'application.extensions.renderers.*',
        'application.components.CustomFields.*',
        'application.components.CustomFields.fields.*',
        'application.controllers.*',
		'application.helpers.*',
        'application.components.payment.Bepaid.AbstractBepaidManager',
        'application.components.payment.Bepaid.FactoryBepaidManager',
    ],

    'behaviors' => [
        [
            'class' => 'application.components.behaviors.FatalErrorHandlerBehaviour',
        ]
    ],

    'components' => [

        'errorHandler' => array(
            'errorAction' => 'site/error',
            'adminInfo'=>'info@elpresent.by'
        ),
        'appStrategy' => [
            'class'=>'application.extensions.ApplicationStrategy.ApplicationStrategy',

            'behaviors'=>array(
                'browser'=>array(
                    'class'=>'application.components.behaviors.Browser',
                ),
            ),

        ],

        'featureSwitch' => [
            'class' => 'application.components.FeatureSwitch',
            'features' => [
                'digital-order' => [
                    'title' => 'Электронные сертификаты',
                    'Если у подарка заполнено поле Что нужно знать, будет отображаться кнопка заказа электронных сертификатов'
                ],
                'side-seo-text' => [
                    'title' => 'SEO текст с боку страницы',
                    'hint' => 'При включении, seo текст с главной страницы будет <a href="/admin/settings/seo" target="_blank">доступен для редактирования</a> и будет отображаться сбоку.'
                ],
            ]
        ],
        'image'=>array(
            'class'=>'application.extensions.image.CImageComponent',
            'driver'=>'GD',
        ),
        'clientScript' => [
        ],
        'assetsManager' => [
            'class' => 'application.components.AssetManager',
            'cwd' => dirname(dirname(__DIR__)),
            'map' => require __DIR__ . '/assets_map.php'
        ],
        'dateFormatter' => [
            'class' => 'CDateFormatter',
            'params' => ['ru'],
        ],
        'cart' => [
            'class' => 'CCustomCart'
        ],
        'currency' => [
            'class' => 'CCustomCurrency'
        ],
        'custom_ajax' => [
            'class' => 'CCustomAjax',
            'preload' => [
                'modules' => [],
                'urls' => [],
                'values' => [],
                'templates' => []
            ],
        ],
        'log' => [
            'class' => 'CLogRouter',
            'routes' => [],
        ],
        'messages' => [
            'class' => 'CPhpMessageSource',
        ],
        'navigator' => [
            'class' => 'UserItemsNavigator'
        ],
        'urlShortner' => [
            'class' => 'UrlShortner',
        ],
        'pagesManager' => [
            'class' => 'PagesManager'
        ],
        'db' => [
            'class'=>'system.db.CDbConnection',
            'tablePrefix' => 'el_',
            'connectionString' => sprintf('mysql:host=%s;dbname=%s', $dbhost, $dbname),
            'username'=> $dbuser,
            'password'=> $dbpassword,
            'charset'=>'utf8',
            'schemaCachingDuration' => 3600 * 8, // 8 hours
            'schemaCachingExclude' => array(
                '{{order_item}}'
            ),
            'enableParamLogging'=>YII_DEBUG,
            'enableProfiling'=>YII_DEBUG,
        ],
        'user' => [
            'class' => 'WebUser',
            // enable cookie-based authentication
            'allowAutoLogin' => true,
            'loginUrl' => ['profile/login'],
        ],
        'authManager' => [
            'class' => 'PhpAuthManager',
            'defaultRoles' => ['user', 'guest', 'admin', 'partner'],
        ],
        'webpay' => [
            'class' => 'application.components.payment.WebPayManager',
            'storeId' => 787373493,
            'store' => 'elpresent.by',
            'currencyId' => 'BYN',
            'debug' => false,
            'secretKey' => 'df4b8c0b4bb1f18dsd4d',
            'username' => 'elpresent.by',
            'password' => '42ff9ef4d4f04148bda496b22be3f796',
        ],
        'client.bepaid.credit_card' => [
            'class' => 'application.components.payment.Bepaid.BepaidClient',
            'checkoutUrl' => $bepaid['checkoutUrl'],
            'shopId' => $bepaid['credit_card']['shopId'],
            'shopKey' => $bepaid['credit_card']['shopKey'],
        ],
        'client.bepaid.halva' => [
            'class' => 'application.components.payment.Bepaid.BepaidClient',
            'checkoutUrl' => $bepaid['checkoutUrl'],
            'shopId' => $bepaid['halva_card']['shopId'],
            'shopKey' => $bepaid['halva_card']['shopKey'],
        ],
        'manager.bepaid.card' => [
            'class' => 'application.components.payment.Bepaid.CardBepaidManager',
            'version' => $bepaid['version'],
            'currency' => $bepaid['credit_card']['currency']

        ],
        'manager.bepaid.halva' => [
            'class' => 'application.components.payment.Bepaid.HalvaBepaidManager',
            'version' => $bepaid['version'],
            'currency' => $bepaid['halva_card']['currency']
        ],

        'pdfGenerator' => [
            'class' => 'application.components.PdfGenerator',
        ],
	    'vkAuth' => [
			'class' => 'application.components.VkAuth',
	    ],
	    'fsAuth' => [
		    'class' => 'application.components.FsAuth',
	    ],
        'mailer' => [
            'class' => 'application.components.Mailer',
            'debug' => false,
            'spoolPath' => __DIR__ .'/../runtime/spool'
        ],
	    'EZip' => [
		    'class' => 'application.components.EZip',
	    ]
    ],

    'params' => require_once(__DIR__ . '/params.php'),

];