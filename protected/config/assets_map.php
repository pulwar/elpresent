<?php

// todo: build assets with gulp/grunt
return [
    'assets/jquery.plugins.js' => [
        'js/fallbacks.js',
        'js/masonry.pkgd.min.js',
        'js/jquery.*.js',
        '!js/jquery-ui',
        'js/fixto.min.js'
    ],
    'assets/app.js' => [
        'js/common.js',
        'js/elevents.js',
        'js/modal.js',
        'js/rollover-rotator.js',
        'js/jquery-ui-1.8.2.custom.min.js',
        'js/ui.datepicker-ru.js',
        'js/ext/ga.js'
    ],
    'assets/elpresent.css' => ['css/elpresent.less']
];
