<?php

return [
	'language' => 'ru',
	'sourceLanguage' => 'en_gb',
	'defaultController' => 'catalog',
	'preload' => [
		'log',
		'user',
	],
	'behaviors' => [
		'redirections' => [
			'class' => 'application.components.behaviors.RedirectHandler',
		]
	],
	'modules' => [
		'admin' => [
			'user' => 'NO'
		],
		'extjs',
        'partner' => [
            'user' => 'NO'
        ]
	],
	'components' => [
		'urlManager' => [
			'class' => 'CCustomUrlManager',
			'showScriptName' => YII_DEBUG,
			'appendParams' => false,
			'urlFormat' => 'path',
			'rules' => require __DIR__ . '/routing.php',
		],
	]
];