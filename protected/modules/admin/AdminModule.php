<? class AdminModule extends CWebModule
{
    public $user;

    public function preinit()
    {
        $this->importMe();
        //checks user permissions
        Yii::app()->user->loginUrl = Yii::app()->urlManager->createUrl('admin/login/');

        Yii::app()->urlManager->appendParams = false;

        //change db timezone for admins convinience
        self::changeTimezone();

        // set jQuery to 1.8.1]
        Yii::app()->setComponent('clientScript', new CClientScript());
        Yii::app()->clientScript->scriptMap = array(
            'jquery.js' => '/js/jquery-1.8.1.min.js',
            'jquery.min.js' => '/js/jquery-1.8.1.min.js',
            'jquery-ui.js' => '/js/jquery-ui-1.9.1.custom.min.js',
            'jquery-ui.min.js' => '/js/jquery-ui-1.9.1.custom.min.js'
        );
        Yii::app()->clientScript->registerCoreScript('jquery');

        yii::import('ext.bootstrap.components.Bootstrap');
        yii::import('ext.ext.tinymce.*');
        Yii::app()->setComponent('bootstrap', new Bootstrap());
    }

    private static function changeTimezone()
    {
        $sql = 'SET time_zone = "' . Yii::app()->getParams()->elTimezone['timezone'] . '"';

        Yii::app()->db->createCommand($sql)->query();
    }


    private function checkPerms($controller)
    {
        return $this->isUserNotGuest() && $this->isUserHasPerms($controller);
    }

    private function isCurrentRequestIsLoginPageRequest($controller)
    {
        return $controller->id == 'login';
    }

    private function isUserNotGuest()
    {
        return (bool)Yii::app()->user->getId();
    }

    private function isUserHasPerms($controller)
    {
        $model = User::model()->findByPk(Yii::app()->user->getId());

        if ($model->user_role == 'admin')
        {
            return true;
        }

        $controller->redirect('/');

        //throw new CHttpException(401,'You has not enought permssions to see this page');
    }

    private function importMe()
    {
        $this->setImport(array(
                              'admin.controllers.*',
                              'admin.models.*',
                              'admin.components.*',
                              'application.models.*',
                              'admin.widgets.*',
                              //'application.widgets.*',
                              'zii.widgets.*',
                              'admin.modelView.*',
                              'admin.modelView.types.*'
                         ));
    }


    public function init()
    {
        // this method is called when the module is being created
        // you may place code here to customize the module or the application

        // import the module-level models and components
        $this->layoutPath = dirname(__FILE__) . '/views/layouts';
        $this->viewPath = dirname(__FILE__) . '/views';
        $this->layout = 'main';
    }

    public function beforeControllerAction($controller, $action)
    {
        $this->switchToAdmin();
        $this->checkPerms($controller);
        if (parent::beforeControllerAction($controller, $action))
        {
            if (!$this->isCurrentRequestIsLoginPageRequest($controller) && !$this->checkPerms($controller))
            {
                $controller->redirect('/admin/login');
            }
            // this method is called before any module controller action is performed
            // you may place customized code here
            return true;
        }
        else
        {
            return false;
        }
    }

    protected function switchToAdmin()
    {
        $session = Yii::app()->getSession();
        if (!empty($session['admin_id']))
        {
            $user = User::model()->findByPk($session['admin_id']);
            $identity = new UserIdentity($user->user_email, $user->user_password, true);

            if ($identity->authenticate())
            {
                Yii::app()->user->login($identity);
            }
            else
            {
                $this->redirect('/', true);
            }

            $session['admin_id'] = '';
        }
    }
}
