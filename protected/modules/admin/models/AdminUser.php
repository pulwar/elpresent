<? class AdminUser extends CActiveRecord
{
	public $pass2;
	/**
	 * The followings are the available columns in table '{{AdminUser}}':
	 */

	/**
	 * Returns the static model of the specified AR class.
	 * @return CActiveRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{admin_user}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('name,type','required'),
			array('pass,username','required','on' => 'register'),
			array('username','checkIsUnique'),
			array('type','numerical','integerOnly' => true),
			array('username','length','max' => 255),
			array('pass','confirmPass'),
			array('pass','makeHash')
		);
	}
	
	public function checkIsUnique()
	{
		$model = self::model()->findByAttributes(array('username' => $this->username));
		$this->addErrorIfNot(is_null($model),'username',"user with the same username (login) already exists");
	}
	
	public function makeHash()
	{
		$this->pass = md5($this->pass);
	}
	
	private function addErrorIfNot($bool,$attr,$errorMsg)
	{
		if(!$bool)
			$this->addError($attr,$errorMsg);
		return $bool;
	}
	
	public function confirmPass()
	{
		return $this->addErrorIfNot($this->pass2 == $this->pass,'pass','password confirmation failed');
	}
	
	public function getUserType()
	{
		return AdminUserType::Factory($this);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}
	
	public function getUserTypeTitle()
	{
		return $this->getUserType()->getTitle();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'pass' 		=> 'Password',
			'type' 		=> 'User type',
			'username' 	=> 'User login',
			'name' 	   	=> 'User name'
		);
	}
	
	public function setLastLogin()
	{
		$id = (int)$this->id;
		return Yii::app()->db->createCommand("UPDATE {{admin_user}} SET `last_login` = NOW() WHERE `id` = {$id}")->execute();
	}
}