<?php

/**
 * Class UserPartnerForm
 */
class UserPartnerForm extends CFormModel
{
    const TYPE_CREATE = 'create';
    const TYPE_EDIT = 'edit';

    public $login;
    public $password;
    public $email;

    public function __construct($scenario = self::TYPE_CREATE)
    {
        parent::__construct($scenario);
    }

    public function rules()
    {
        switch ($this->getScenario()) {
            case self::TYPE_EDIT:
                return [
                    ['login, email', 'required'],
                ];
            case self::TYPE_CREATE:
                return [
                    ['login, password, email', 'required'],
                ];
        }
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return [
            'login' => 'Логин',
            'password' => 'Пароль',
            'email' => 'Email',
        ];
    }
}