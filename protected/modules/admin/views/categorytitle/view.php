<div class="anoncer">
    <h2>Исключение суффикса из tile</h2>

    <div class="landing-form-order">
        <? $form = $this->beginWidget('CActiveForm', array(
            'enableAjaxValidation' => false,
            'action' => CHtml::normalizeUrl(array("/admin/categorytitle")),
            'enableClientValidation' => false,
            'clientOptions' => array(
                'validateOnChange' => true,
                'validateOnSubmit' => true,
            )
        ));
        ?>

        <div>
            <hr>
            <?=$form->labelEx($model, 'txt'); ?>
            <?=$form->textArea($model, 'txt', ['value' => $txt, 'style' => 'width:800px']); ?>
            <?=$form->error($model, 'txt'); ?>
        </div>
        <div>
            <br>
            <?=CHtml::submitButton('Сохранить'); ?>
        </div>
        <? $this->endWidget(); ?>
    </div>
</div>
