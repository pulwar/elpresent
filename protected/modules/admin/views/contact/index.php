<div id="user-table">
    <?php
    $this->widget('ext.bootstrap.widgets.TbGridView', array (
    'type' => array('condensed', 'striped'),
    'dataProvider' => $oContact,
    'columns'      => array
    (
        array
        (
        'name' => 'contact_name',
        'value' => 'htmlspecialchars($data->contact_name)'
        ),
        'contact_email',
        array
        (
        'name' => 'contact_subject',
        'value' => 'htmlspecialchars($data->contact_subject)'

        ),
        array
        (
           'name' => 'contact_body',
           'value' => 'htmlspecialchars(substr($data->contact_body, 0, 50))'
        ),
        'contact_rating',
        'contact_date',

        array
        (
            'type'  => 'html',
            'value' => 'CHtml::link("[просмотреть]", "/admin/contact/view/id/".$data->contact_id."/page/".(Yii::app()->request->getParam("User_page")))."<br />".CHtml::link("[удалить]", "/admin/contact/delete/id/".$data->contact_id,array("class" => "delete"))."<br />".($data->visibility ? "" : CHtml::link("[опубликовать]", "/admin/contact/approve/id/".$data->contact_id))'
        )
    )
));
?>
</div>