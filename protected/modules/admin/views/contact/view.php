<div class="b-admin-errors">
    <?= CHtml::errorSummary($oContact); ?>
</div>
<div class="b-admin-wrapper">
    <form class="b-admin-form b-item">
        <table class="user-manage-table" cellspacing="0" cellpadding="0" border="0">
            <tr>
            <tr><td>Рейтинг:</td><td><?=$oContact->contact_rating; ?></td></tr>
            <tr><td>Имя:</td><td><?=htmlspecialchars($oContact->contact_name); ?></td></tr>
            <tr><td>E-mail:</td><td><?=htmlspecialchars($oContact->contact_email); ?></td></tr>
            <tr><td>Тема:</td><td><?=htmlspecialchars($oContact->contact_subject); ?></td></tr>
            <tr><td>Отзыв:</td><td><?=htmlspecialchars($oContact->contact_body); ?></td></tr>
            <tr><td>Дата:</td><td><?=$oContact->contact_date; ?></td></tr>
            </tr>
        </table>
    </form>
    <br />
    <a href="<?= $fromPage; ?>">Назад</a>
</div>