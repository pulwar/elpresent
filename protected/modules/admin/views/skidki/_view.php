<div class="view">

    <b><?=CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
    <?=CHtml::link(CHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
    <br/>

    <b><?=CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
    <?=CHtml::encode($data->title); ?>
    <br/>

    <b><?=CHtml::encode($data->getAttributeLabel('alias')); ?>:</b>
    <?=CHtml::encode($data->alias); ?>
    <br/>

    <b><?=CHtml::encode($data->getAttributeLabel('tiser')); ?>:</b>
    <?=CHtml::encode($data->tiser); ?>
    <br/>

    <b><?=CHtml::encode($data->getAttributeLabel('address')); ?>:</b>
    <?=CHtml::encode($data->address); ?>
    <br/>

    <b><?=CHtml::encode($data->getAttributeLabel('tel')); ?>:</b>
    <?=CHtml::encode($data->tel); ?>
    <br/>

    <b><?=CHtml::encode($data->getAttributeLabel('price')); ?>:</b>
    <?=CHtml::encode($data->price); ?>
    <br/>

    <b><?=CHtml::encode($data->getAttributeLabel('date')); ?>:</b>
    <?=CHtml::encode(date('d.m.Y',$data->date)); ?>
    <br/>

    <b><?=CHtml::encode($data->getAttributeLabel('time_finish')); ?>:</b>
    <?=CHtml::encode(date('d.m.Y',$data->time_finish)); ?>
    <br/>

    <b><?=CHtml::encode($data->getAttributeLabel('hide')); ?>:</b>
    <?=$data->hide == 1 ? 'Да' : 'Нет'; ?>
    <br/>
</div>