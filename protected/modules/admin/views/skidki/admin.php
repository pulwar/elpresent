<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#page-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Менеджер скидок</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'page-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id' => array(
            'name' => 'id',
            'filter' => false,
        ),
        'title',
        'alias',
        'partner_id' => array(
            'name' => 'partner_id',
            'value' => '$data->partner->name'
        ),
        'category_id' => array(
            'name' => 'category_id',
            'value' => '$data->category->title'
        ),
        'date' => array(
            'name' => 'date',
            'value' => '(date("d.m.Y", $data->date))',
            'filter' => false,
        ),
        'hide' => array(
            'name' => 'hide',
            'value' => '($data->hide == 1 ? "Да" : "Нет")',
            'filter' => false,
        ),
        'order_items' => array(
            'name' => 'order_items',
            'filter' => false,
        ),
        array(
            'class' => 'CButtonColumn',
        ),
    ),
)); ?>

<?php echo CHtml::link('Создать',array('/admin/skidki/create'), ['class' => 'btn btn-primary']); ?>
