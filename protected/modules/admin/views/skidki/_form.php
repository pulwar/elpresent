<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'page-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    )); ?>

    <p class="note">Обязательны для заполнения <span class="required">*</span></p>

    <div class="required">
        <?=$form->errorSummary($model); ?>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'title'); ?>
        <?=$form->textField($model, 'title'); ?>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'alias'); ?>
        <?=$form->textField($model, 'alias'); ?>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'price'); ?>
        <?=$form->numberField($model, 'price', ['step' => 'any']); ?>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'category_id'); ?>
        <?=$form->dropDownList($model, 'category_id', $model->getCategoriesByIds()); ?>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'partner_id'); ?>
        <?=$form->dropDownList($model, 'partner_id', $model->getPartnersInMinsk()); ?>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'img_small'); ?>
        <? if (empty($model->img_small)): ?>
            <?=$form->fileField($model, 'img_small', array('accept' => 'image')); ?>
            <?=$form->error($model, 'img_small'); ?>
        <? else: ?>
            <img src="/images/discounts/<?=$model->img_small?>" alt="удалить" style="display: block;height: 200px">
            <a href="<?=Yii::app()->createUrl('/admin/skidki/delsmall', array('id'=>$model->id))?>">Удалить</a>
        <? endif; ?>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'img_big'); ?>
        <? if (empty($model->img_big)): ?>
            <?=$form->fileField($model, 'img_big', array('accept' => 'image')); ?>
            <?=$form->error($model, 'img_big'); ?>
        <? else: ?>
            <img src="/images/discounts/<?=$model->img_big?>" alt="удалить" style="display: block;height: 200px">
            <a href="<?=Yii::app()->createUrl('/admin/skidki/delbig', array('id'=>$model->id))?>">Удалить</a>
        <? endif; ?>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'tiser'); ?>
        <? $this->widget('ext.tinymce.TinyMce', array(
            'model' => $model,
            'attribute' => 'tiser',
            'fileManager' => array(
                'class' => 'ext.elFinder.TinyMceElFinder',
                'connectorRoute'=>'admin/elfinder/connector',
            ),
        ));?>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'text'); ?>
        <? $this->widget('ext.tinymce.TinyMce', array(
            'model' => $model,
            'attribute' => 'text',
            'fileManager' => array(
                'class' => 'ext.elFinder.TinyMceElFinder',
                'connectorRoute'=>'admin/elfinder/connector',
            ),
        ));?>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'hide'); ?>
        <?=$form->dropDownList($model, 'hide', array(0 => 'Нет', 1 => 'Да')); ?>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'order_items'); ?>
        <?=$form->numberField($model, 'order_items', array('min' => 0)); ?>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'count_cods'); ?>
        <?=$form->numberField($model, 'count_cods', array('min' => 1)); ?>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'address'); ?>
        <?=$form->textArea($model, 'address'); ?>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'tel'); ?>
        <?=$form->textArea($model, 'tel'); ?>
    </div>

    <div class="form-group">
        <?=$form->labelEx($model, 'time_finish'); ?>
        <?=$form->dateField($model, 'time_finish', ['min'=> date('Y-m-d')]); ?>
    </div>

    <br>
    <div class="form-group buttons">
        <?=CHtml::link('К списку',['/admin/skidki'], ['class' => 'btn btn-primary']); ?>
        <?=CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', ['class' => 'btn btn-primary']); ?>
    </div>

    <?$this->endWidget(); ?>

</div><!-- form -->