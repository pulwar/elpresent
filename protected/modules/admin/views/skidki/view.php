<h1>Скидка #<?php echo $model->id; ?></h1>
<? foreach($model->code as $code):?>
    <? $codes[] = $code->code?>
<? endforeach;?>
<? $this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'id',
        'category_id' => [
            'name' => 'category_id',
            'value' => $model->category->title,
        ],
        'partner_id' => [
            'name' => 'partner_id',
            'value' => $model->partner->name,
        ],

        'title',
        'alias',
        'price',
        'img_small' => [
            'name' => 'img_small',
            'type' => 'html',
            'value' => $model->img_small ? "<img src='/images/discounts/$model->img_small' style='height: 100px'/>" : ''
        ],
        'img_big' => [
            'name' => 'img_big',
            'type' => 'html',
            'value' => $model->img_big ? "<img src='/images/discounts/$model->img_big' style='height: 100px'/>" : ''
        ],
        'tiser' => [
            'name' => 'tiser',
            'type' => 'html'
        ],
        'text' => [
            'name' => 'text',
            'type' => 'html'
        ],
        'address' => [
            'name' => 'address',
            'type' => 'html'
        ],
        'tel' => [
            'name' => 'tel',
            'type' => 'html'
        ],
        'date' => array(
            'name' => 'date',
            'value' => date("d.m.Y", $model->date),
        ),
        'time_finish' => array(
            'name' => 'time_finish',
            'value' => date("d.m.Y", $model->time_finish),
        ),
        'hide' => array(
            'name' => 'hide',
            'value' => $model->hide == 1 ? 'Да' : 'Нет',
        ),
        'order_items',
        'count_cods' => array(
            'name' => 'count_cods',
            'value' => implode(',',$codes)
        )
    ),
)); ?>
<br>
<?=CHtml::link('К списку',['/admin/skidki'], ['class' => 'btn btn-primary']); ?>
