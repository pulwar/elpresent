<div class="filterAdmin <?=($aFilter) ? 'active' : 'closed'
?>"><div id="label">[<span id="collapse"><?=($aFilter) ? '-' : '+'
?></span>] Фильтр <?=($aFilter) ? '<sup id="red">(активен)</sup>' : ''
?></div>
    <form method="POST">
        <table class="filterTable">
            <tr>
                <td colspan="2" class="head"></td>
            </tr>
            <tr>
                <td>По дате</td>
                <td>
                    <select name="aFilter[dttype]">
                        <option value="order_date" <?=($aFilter && $aFilter['dttype'] == 'order_date') ? 'selected' : ''
?> >заказа</option>
                        <option value="recipient_gift_date" <?=($aFilter && $aFilter['dttype'] == 'recipient_gift_date') ? 'selected' : ''
?> >желаемой доставки</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Не раньше, чем</td>
                <td><input type="text" name="aFilter[dtfrom]" value="<?=($aFilter && $aFilter['dtfrom']) ? $aFilter['dtfrom'] : ''
?>" /></td>
            </tr>
            <tr>
                <td>Не позже, чем</td>
                <td><input type="text" name="aFilter[dtto]" value="<?=($aFilter && $aFilter['dtto']) ? $aFilter['dtto'] : ''
?>" /></td>
            </tr>
            <tr>
                <td>Номер заказа</td>
                <td><input type="text" name="aFilter[order_number]" value="<?=($aFilter && $aFilter['order_number']) ? $aFilter['order_number'] : ''?>" /></td>
            </tr>
            <tr>
                <td>Номер карты</td>
                <td><input type="text" name="aFilter[code]" value="<?=($aFilter && $aFilter['code']) ? $aFilter['code'] : '' ?>" /></td>
            </tr>
            <tr>
                <td>Товар</td>
                <td>
                    <select name="aFilter[item_id]">
                        <option value="">---</option>
                        <?
                           foreach ($oItems as $row)
                           {
                               $sel = ($aFilter && $aFilter['item_id'] == $row->id) ? 'selected' : '';
                               echo '<option value="' . $row->id . '" ' . $sel . ' >' . $row->title . '</option>';
                           };
                        ?>
                       </select>
                   </td>
               </tr>
               <tr>
                   <td colspan="2" class="head"></td>
               </tr>
               <tr>
                   <td>
                       <input type="submit" value="Применить">
                   </td>
                   <td>
                       <input type="reset" value="Очистить" onclick="window.location = window.location.href">
                   </td>
               </tr>
           </table>
       </form>
   </div>
   <div style="clear:both"></div>

   <div id="order-table">
    <?                            $this->widget('zii.widgets.grid.CGridView', array
                               (
                               'dataProvider' => $oOrders,
                               'columns' => array
                                   (
                                   'id',
                                   'order_number',
                                   array
                                       (
                                       'name' => 'order_date',
                                       'value' => 'substr($data->order_date, 0, 16)'
                                   ),
                                   'code',
                                   array
                                   (
                                       'name' => 'item_id',
                                       'type' => 'raw',
                                       'value' => '$data->item ? "<a target=\"_blank\" href=\"/admin/".( $data->item->type == "group" ? "superitem/edit/":"item.html")."?id=" . $data->item->id . "\" >" . $data->item->title . "</a>" : ""'
                                   ),
                                   'discount',
                                   array(
                                       'name'=>'delivery',
                                       'type' => 'raw',
                                       'value' => '!$data->delivery ? "Обычная доставка":"Самовывоз"'
                                   ),
                                   array
                                   (
                                       'name' => 'is_express_delivery',
                                       'value' => '$data->is_express_delivery ? number_format(ShopOrder::getDelivPrice()) : ""'
                                   ),
                                   array
                                   (
                                       'name' => 'is_express_delivery_recipient',
                                       'value' => '$data->is_express_delivery_recipient ? number_format(ShopOrder::getGiftDelivPrice()) : ""'
                                   ),
                                   array
                                   (
                                       'name' => 'is_thematic_delivery',
                                       'value' => '$data->is_thematic_delivery ? number_format($data->thematic_delivery_price)." (".$data->thematic_delivery_name.")" : ""'
                                   ),
                                   array(
                                       'name' => 'saled_price',
                                       'value' => 'number_format($data->saled_price)'
                                   ),
                                   'name',
                                   'lname',
                                   'phone',
                                   'comment',
                                   'adr',
                                   'payer_adr',
                                   array(
                                       'name' => 'desired_deliv_time',
                                       'value' => '(strtotime($data->gift_date) > 0) ? $data->gift_date." ".$data->gift_time_from." - ".$data->gift_time_to : ""',
                                   ),
                                   array(
                                       'name' => 'desired_pay_time',
                                       'value' => '(strtotime($data->payer_date) > 0) ? $data->payer_date." ".$data->payer_time_from." - ".$data->payer_time_to : ""',
                                   ),
                                   array(
                                       'name' => 'user_login',
                                       'value' => 'user.user_login'
                                   ),
                                   'email',
                                   'order_knowwhere',
                                   array(
                                       'name' => 'REFERER',
                                       'type' => 'html',
                                       'value' => '"<div class=\"referer\">".htmlspecialchars($data->referer)."</div>"'
                                   ),
                                   array(
                                       'name' => 'Keywords',
                                       'value' => 'htmlspecialchars($data->keywords)'
                                   ),

                                   array
                                       (
                                       'name' => 'texture_img',
                                       'type' => 'html',
                                       'value' => '($data->texture && array_key_exists($data->texture, Order::$itemImages)) ? CHtml::image("/images/".Order::$itemImages[$data->texture], "", array("width" => "130")) : ""'
                                   ),
                                   array
                                       (
                                       'name' => 'bow_img',
                                       'type' => 'html',
                                       'value' => '($data->bow && array_key_exists($data->bow, Order::$itemImages)) ? CHtml::image("/images/".Order::$itemImages[$data->bow], "", array("width" => "130")) : "Нет"'
                                   ),
                                   array
                                       (
                                       'name' => 'flower_id',
                                       'type' => 'html',
                                       'value' => '$data->flower ? CHtml::image("/upload/flowers/".$data->flower->flower_image."_48.png", "") : "Нет"'
                                   ),
                                   array
                                       (
                                       'name' => 'card_id',
                                       'type' => 'html',
                                       'value' => '$data->card ? CHtml::image("/upload/cards/".$data->card->card_image."_48.png", "") : "Нет"'
                                   ),
                                   array(
                                       'name' => 'payment_type_id',
                                       'value' => '(4 == $data->payment_type_id) ? "EasyPay" : ((1 == $data->payment_type_id) ? "оплата курьеру" : ((5 == $data->payment_type_id) ? "Webpay" :"неизвестно"))'
                                   ),
                                   array(
                                       'name' => 'is_paid',
                                       'value' => '($data->is_paid) ? "Да" : "Нет"',
                                   ),
                                   array(
                                       'name' => 'delivered',
                                       'value' => '($data->delivered) ? "Да" : "Нет"',
                                   ),
                                   array(
                                       'name' => 'sended',
                                       'value' => '($data->sended) ? "Да" : "Нет"',
                                   ),
                                    'recipient_firstname',
                                    'recipient_lastname',
                                    'recipient_phone',
                                    'recipient_address',
                                   array(
                                       'name' => 'Желаемое время вручения подарка',
                                       'value' => '(strtotime($data->recipient_desired_date) > 0) ? $data->recipient_desired_date." ".$data->recipient_desired_time_from." - ".$data->recipient_desired_time_to : ""',
                                   ),

                                   array(
                                       'name' => 'Цена открытки',
                                       'value' => '($data->card_quantity) ? number_format($data->card->card_price) : ""'
                                   ),
                                   array(
                                       'name' => 'Цена открытки со скидкой',
                                       'value' => '($data->card_quantity) ? number_format($data->card->card_price - ceil($data->card->card_price / 100 * $data->discount)) : ""'
                                   ),

                                   array(
                                       'name' => 'Цена цветов',
                                       'value' => '($data->flower_quantity) ? number_format($data->flower->flower_price) : ""'
                                   ),
                                   array(
                                       'name' => 'Цена цветов со скидкой',
                                       'value' => '($data->flower_quantity) ? number_format($data->flower->flower_price - ceil($data->flower->flower_price / 100 * $data->discount)) : ""'
                                   ),
                                   array(
                                       'name' => 'Цена подарка',
                                       'value' => 'number_format($data->item->price)',
                                   ),
                                   array(
                                       'name' => 'Цена подарка со скидкой',
                                       'value' => 'number_format($data->item->price - ceil($data->item->price / 100 * $data->discount))',
                                   ),
                                   'recivier_activation_date',
                                   'recivier_name',
                                   'recivier_lname',
                                   'recivier_phone',
                                   'recivier_email',
                                   'recivier_date',
                                   array(
                                       'name' => 'Партнер',
                                       'value' => '$data->item->partner->name'
                                   ),
                                   array(
                                       'name' => 'Сумма за подарок партнеру',
                                       'value' => 'number_format($data->item->partner_price)'
                                   ),
                                   array
                                   (
                                       'type' => 'html',
                                       'value' => 'CHtml::link("[редактировать]", "/admin/order/manage/id/".$data->id."/page/".(Yii::app()->request->getParam("Order_page")))."<br />".CHtml::link("[удалить]", "/admin/order/delete/id/".$data->id,array("class" => "delete"))
                                           ."<br /><br />".(4 == $data->payment_type_id ? CHtml::link("[проверить счет]", "/admin/order/checkinvoice/id/".$data->id) : "")'
                                   )
                               )
                           ));
    ?>

</div