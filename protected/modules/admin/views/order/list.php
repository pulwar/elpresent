<div class="admin-main">
    <div class="title">
        <span><?= _('Orders'); ?></span>
    </div>
    <div class="inner">

        <? $this->widget('AdminTableView',array('list' => $list)); ?>
        <?
        if($list instanceof AbstractPager)
            $this->widget('AdminPagerWidget',array('pager' => $list));
        ?>

    </div>
</div>
