<div class="row">
    <div class="span7">
        <h1>Список заказов</h1>
        <div class="order-list" data-spy="affix">
            <div class="item first pending">
                <p class="name">Индивидуальный сеанс даосской йоги </p>
                <p class="price">272 100 руб.</p>
                <p class="customer">Протько Сергей</p>
            </div>
            <div class="item active">
                <p class="name">Индивидуальный сеанс даосской йоги <span class="label label-info">оплачено</span> </p>
                <p class="price">272 100 руб.</p>
                <p class="customer">Протько Сергей</p>
            </div>
            <div class="item">
                <p class="name">Индивидуальный сеанс даосской йоги <span class="label label-info">оплачено</span> <span class="label label-success">доставлено</span> </p>
                <p class="price">272 100 руб.</p>
                <p class="customer">Протько Сергей</p>
            </div>
            <div class="item pending">
                <p class="name">Индивидуальный сеанс даосской йоги </p>
                <p class="price">272 100 руб.</p>
                <p class="customer">Протько Сергей</p>
            </div>
            <div class="item">
                <p class="name">Индивидуальный сеанс даосской йоги <span class="label label-info">оплачено</span> <span class="label label-success">доставлено</span> </p>
                <p class="price">272 100 руб.</p>
                <p class="customer">Протько Сергей</p>
            </div>
            <div class="item">
                <p class="name">Индивидуальный сеанс даосской йоги <span class="label label-info">оплачено</span> </p>
                <p class="price">272 100 руб.</p>
                <p class="customer">Протько Сергей</p>
            </div>
            <div class="item">
                <p class="name">Индивидуальный сеанс даосской йоги <span class="label label-info">оплачено</span> <span class="label label-success">доставлено</span> </p>
                <p class="price">272 100 руб.</p>
                <p class="customer">Протько Сергей</p>
            </div>
            <div class="item">
                <p class="name">Индивидуальный сеанс даосской йоги <span class="label label-info">оплачено</span> <span class="label label-success">доставлено</span> </p>
                <p class="price">272 100 руб.</p>
                <p class="customer">Протько Сергей</p>
            </div>
            <div class="item">
                <p class="name">Индивидуальный сеанс даосской йоги <span class="label label-info">оплачено</span> </p>
                <p class="price">272 100 руб.</p>
                <p class="customer">Протько Сергей</p>
            </div>
            <div class="item last">
                <p class="name">Индивидуальный сеанс даосской йоги <span class="label label-info">оплачено</span> </p>
                <p class="price">272 100 руб.</p>
                <p class="customer">Протько Сергей</p>
            </div>
        </div>
        <div class="pagination">
            <ul>
                <li class="disabled"><a href="#">Предыдущая</a></li>
                <li class="active"><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">Следующая</a></li>
            </ul>
        </div>
    </div>
    <div class="span5 order-info">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#impression">Впечатление</a>
            </li>
            <li><a href="#delivery">Доставка</a></li>
            <li><a href="#payment">Оплата</a></li>
            <li><a href="#activation">Активация</a></li>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="impression">
                Информация о впечатлении
            </div>
            <div class="tab-pane" id="delivery">
                Информация о доставке
            </div>
            <div class="tab-pane" id="payment">
                Информация о платежах
            </div>
            <div class="tab-pane" id="activation">
                Информация о активации
            </div>
        </div>
    </div>
    <script>
        $(function(){
            $('.nav-tabs a').on('click', function (e) {
                e.preventDefault();
                $(this).tab('show');
            });
            $('.order-info').affix();
        });
    </script>
</div>