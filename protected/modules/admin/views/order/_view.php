<? // empty($order->kit_ids) ? '' : d::pr($order->kit_ids,1)?>
<form action="/admin/order/edit/id/<?php echo $order->id; ?>" method="POST" class="form-horizontal" id="formOrderEdit">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#description">Описание</a></li>
<!--        --><?php //if ($order->item->type != 'group'){ ?><!--<li><a href="#packing">Оформление</a></li>--><?php //} ?>
        <li><a href="#delivery">Доставка</a></li>
        <li><a href="#activation">Состояние</a></li>
        <li><a href="#lkp">ЛКП</a></li>
    <!--    <li><a href="#stats">Статистика</a></li>-->
    </ul>
    <div class="tab-content " style="max-height: 10000px;">
        <div class="tab-pane active form-horizontal" id="description">
            <div class="control-group">
                <strong class="control-label">№ заказа:</strong>
                <div> <?php echo $order->order_number; ?></div>
            </div>
    <!--        <div class="control-group">-->
    <!--            <strong class="control-label">Тип: </strong>-->
    <!--            <div> --><?//echo $order->item->type == 'group' ? 'Супервпечатление':'Впечатление'; ?><!--</div>-->
    <!--        </div>-->
            <div class="control-group">
                <strong class="control-label">Позиции заказа: </strong>
                <br>
                <div style="margin-left: 109px;">
                    <? foreach ($order->orderItems as $k=>$orderItem){?>
                            <a href="<?php echo '/admin/' . ($orderItem->itemObj->type == 'group' ? 'superitem/edit/id/':'item/manage/id/').$orderItem->itemObj->id; ?>">
                                    <?=$orderItem->itemObj->title; ?>
                                </a>
                        <br>
                    <? }?>
                </div>
            </div>

            <? if(!empty($order->option->name)):?>
                <div class="control-group">
                    <strong class="control-label">Опция: </strong>
                    <div> <?=$order->option->name?></div>
                </div>
            <? endif;?>
            <div class="control-group">
                <strong class="control-label">Срок действия:</strong>
                <div class="input-append date" id="payer_date_validity">
                    <input data-format="yyyy-MM-dd" type="text" name="OrderInfo[validity_date]" value="<?php echo $order->validity_date; ?>" />
                    <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="icon-calendar"></i>
                    </span>
                </div>
            </div>

            <div class="control-group">
                <strong class="control-label">Скидка в %:</strong>
                <div><input type="text" name="OrderInfo[discount]" value="<?php echo $order->discount; ?>" /> %</div>
            </div>

            <div class="control-group">
                <strong class="control-label">ФИО: </strong>
                <div> <input type="text" name="OrderInfo[name]" value="<?php echo ($order->lname ? $order->lname.' ': ''), $order->name; ?>"/></div>
            </div>
            <div class="control-group">
                <strong class="control-label">Телефон: </strong>
                <div> <input type="text" name="OrderInfo[phone]" value="<?php echo $order->phone; ?>"/></div>
            </div>
            <div class="control-group">
                <strong class="control-label">Email: </strong>
                <div> <input type="text" name="OrderInfo[email]" value="<?php echo $order->email; ?>"/></div>
            </div>
            <?//if(trim($order->comment)): ?>
            <div class="control-group">
                <strong class="control-label">Комментарий: </strong>
                <div> <textarea name="OrderInfo[comment]"><?php echo nl2br(strip_tags($order->comment)); ?></textarea></div>
            </div>
            <?// endif ?>
            <? if(1/*trim($order->message)*/): ?>
                <div class="control-group">
                    <strong class="control-label">Поздравление: </strong>
                    <div> <textarea name="OrderInfo[message]"><?php echo nl2br(strip_tags($order->message)); ?></textarea></div>
                </div>
            <? endif ?>
            <div class="control-group">
                <strong class="control-label">Способ оплаты: </strong>
                <div>
                    <select name="OrderInfo[payment_type_id]">
                        <option value="0">неизвестно</option>
                        <option value="1" <?php echo (1 == $order->payment_type_id) ? 'selected="selected"' : "" ;?> >Наличными</option>
                        <option value="4" <?php echo (4 == $order->payment_type_id) ? 'selected="selected"' : "" ;?> >EasyPay</option>
                        <option value="5" <?php echo (5 == $order->payment_type_id) ? 'selected="selected"' : "" ;?> >Webpay</option>
                        <option value="6" <?php echo (6 == $order->payment_type_id) ? 'selected="selected"' : "" ;?> >Bepaid (Viza, MasterCard, Maestro) онлайн</option>
                        <option value="7" <?php echo (7 == $order->payment_type_id) ? 'selected="selected"' : "" ;?> >Bepaid Халва (2 месяца рассрочки)</option>
                    </select>
                </div>
            </div>
            <div class="control-group">
                <strong class="control-label">Сумма в руб.: </strong>
                <div>
                    <input type="text" id="orderInfoPrice" name="OrderInfo[price]" value="<?php echo $order->price; ?>" /> Руб.
                    <br />
                    <?php if($order->flower_id) { ?> <span class="label label-info">Цветы</span> <?php } ?>
                    <?php if($order->card_id) { ?> <span class="label label-info">Открытка</span> <?php } ?>
					<?php if($order->toy_id) { ?> <span class="label label-info">Игрушки</span> <?php } ?>
                </div>
            </div>
            <div class="control-group">
                <strong class="control-label">Сумма со скидкой в руб.: </strong> <br />
                <?php echo ($order->price); ?> Руб.
            </div>
            <div class="control-group">
                <strong class="control-label">Оплачено баллами: </strong> <br />
                <?php echo ($order->user_cash); ?> Руб.
            </div>


        </div>
        <?php if($order->item->type != 'group') { ?>
        <div class="tab-pane packing" id="packing">
            <!-- Блок описания упаковки -->
            <div class="control-group design">
                <div class="control-label">Оформление: </div>
                <div class="controls packing">
                    <?php
//                    $this->widget('PackWidget');
                    ?>
                </div>
                <div class="controls packing">
                    <img id="texture-view" class="texture" src="<?php echo ($order->texture) ? "/images/".$order->texture:""; ?>" alt="" />
                    <img id="bow-view" class="bow" src="<?php echo ($order->bow) ? "/images/".$order->bow:""; ?>" alt="" />
                </div>
            </div>
            <div id="flower-view">
                <?php if($order->flower) { ?>
				<hr>
                <div class="control-group card">
                    <div class="control-label">Цветы: </div>
                    <div class="controls">
                        <a href="#" style="font-weight: bold;"><?= $order->flower->flower_title; ?></a><br />
                        <span class=" price"><?php echo ($order->flower->flower_price); ?> Руб.</span><br />
                        <img src="/upload/flowers/<?= $order->flower->flower_image; ?>_297.png" alt="" />
                    </div>
                </div>
                <?php } ?>
            </div>
            <div id="card-view">
                <?php if($order->card) { ?>
				<hr>
                <div class="control-group card">
                    <div class="control-label">Открытка: </div>
                    <div class="controls">
                        <a href="#" style="font-weight: bold;"><?= $order->card->card_title; ?></a><br />
                        <span class=" price"><?php echo ($order->card->card_price); ?> Руб.</span><br />
                        <img src="/upload/cards/<?= $order->card->card_image; ?>_297.png" alt="" />

                    </div>
                </div>
                <?php } ?>
            </div>
			<div id="card-view">
				<?php if($order->toy_id) { ?>
					<hr>
					<div class="control-group card">
						<div class="control-label">Букет из игрушек: </div>
						<div class="controls">
							ID: <a target="_blank" href="admin/item/manage/id/<?=$order->toy_id?>"><?=$order->toy_id?></a>
						</div>
					</div>
				<?php } ?>
			</div>
        <!-- Блок описания упаковки -->
        </div>
        <?php } ?>
        <div class="tab-pane" id="delivery">
            <!-- Блок информация о доставке -->
            <div class="control-group">
                <div class="control-label">Вид доставки</div>
                <div class="controls">
                     <select name="OrderInfo[delivery]" disabled>
                        <option value="0">Самовывоз (0 руб.)</option>
                        <option value="1" <?=($order->is_delivery_general) ? 'selected="selected"' : "" ;?>>
                            Обычная доставка (<?=(Order::DELIVERY_GENERAL_PRICE)?>)
                        </option>
                        <option value="2" <?=($order->is_express_delivery) ? 'selected="selected"' : "" ;?>>
                            Экспресс доставка (<?=(Order::DELIVERY_EXPRESS_PRICE);?> руб.)
                        </option>
                        <option value="3" <?=($order->is_thematic_delivery) ? 'selected="selected"' : "" ;?>>
                            Тематическая доставка
                        </option>
                     </select>
                    <br /><br />
                    <? if(!empty($order->is_thematic_delivery)):?>
                        <input type="checkbox" name="OrderInfo[is_express_delivery_recipient]" <?php echo ($order->is_express_delivery_recipient) ? 'checked="checked"' : "" ;?> value="1"/>   Вручить лично получателю.
                        <br /><br />
                        <? $thematicDeliveryOptions = PaymentType::getThematicDeliveryOptions($order->city); ?>
                        Тематическая доставка:
                        <br />
                        <select name="OrderInfo[thematic_delivery_id]"disabled>
                            <option value="" <?php echo (!$order->thematic_delivery_name) ? 'selected="selected"' : "" ;?>>Нет</option>
                            <? foreach ($thematicDeliveryOptions as $option) :?>
                                <option value="<?=$option->id?>" <?=($order->thematic_delivery_name == $option->id) ? 'selected="selected"' : "" ;?>
                                        thematic_price="<?=$option->getPrice($order->city);?>"><?=$option->title;?> (<?=$option->getPrice($order->city);?> руб.)</option>
                            <? endforeach; ?>
                        </select>
                    <? endif;?>

                 </div>
            </div>

            <div class="control-group">
                <div class="control-label">Адрес оплаты заказа</div>
                <div class="controls"><textarea name="OrderInfo[payer_adr]"><?php echo $order->payer_adr; ?></textarea></div>
            </div>

            <div class="control-group">

                <div class="control-label">Желаемое время оплаты
                    <br /><br />
                    с
                    <br />
                    по
                </div>
                <div class="controls">

                    <div id="payer_date_id" class="input-append date">
                    <input data-format="yyyy-MM-dd" type="text" name="OrderInfo[payer_date]" id="payer_date" value="<? echo $order->payer_date; ?>"/>
                    <span class="add-on">
                    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
                </div>
                    <br />
                    <br />
                <div id="payer_time_from_id" class="input-append date">
                    <input data-format="hh:mm" type="text" name="OrderInfo[payer_time_from]" id="payer_time_from" value="<? echo substr($order->payer_time_from, 0, -3); ?>"/>
                    <span class="add-on">
                    <i data-time-icon="icon-time" data-date-icon="icon-time"></i>
                    </span>
                    </div>
                <br />
                <div id="payer_time_to_id" class="input-append date">
                    <input data-format="hh:mm" type="text" name="OrderInfo[payer_time_to]" id="payer_time_to" value="<? echo substr($order->payer_time_to, 0, -3); ?>"/>
                    <span class="add-on">
                    <i data-time-icon="icon-time" data-date-icon="icon-time"></i>
                    </span>
                    </div>

                <script type="text/javascript">
                $(function() {
                    $('#payer_date_id').datetimepicker();
                    $('#payer_date_validity').datetimepicker();
                    $('#payer_time_from_id').datetimepicker({ pickDate: false});
                    $('#payer_time_to_id').datetimepicker({ pickDate: false});
                });
                </script>

                </div>

            </div>

            <div class="control-group">
                <div class="control-label">Имя получателя</div>
                <div class="controls"><input type="text" name="OrderInfo[recipient_firstname]" value="<?php echo $order->recipient_firstname; ?>" /></div>
            </div>
            <div class="control-group">
                <div class="control-label">Фамилия получателя</div>
                <div class="controls"><input type="text" name="OrderInfo[recipient_lastname]" value="<?php echo $order->recipient_lastname; ?>" /></div>
            </div>
            <div class="control-group">
                <div class="control-label">Телефон получателя</div>
                <div class="controls"><input type="text" name="OrderInfo[recipient_phone]" value="<?php echo $order->recipient_phone; ?>" /></div>
            </div>

            <div class="control-group">
                <div class="control-label">Адрес доставки</div>
                <div class="controls"><input type="text" name="OrderInfo[adr]" value="<?php echo $order->adr; ?>" /></div>
            </div>

            <div class="control-group">
                <div class="control-label">Адрес вручения подарка</div>
                <div class="controls"><input type="text" name="OrderInfo[recipient_address]" value="<?php echo $order->recipient_address; ?>" /></div>
            </div>


            <div class="control-group">
                <div class="control-label">Желаемое время доставки
                    <br /><br />
                    с
                    <br />
                    по
                </div>
                <div class="controls">

                    <div id="gift_date_id" class="input-append date">
                    <input data-format="yyyy-MM-dd" type="text" name="OrderInfo[gift_date]" id="gift_date" value="<? echo $order->gift_date; ?>"/>
                    <span class="add-on">
                    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
                </div>
                    <br />
                    <br />
                <div id="gift_time_from_id" class="input-append date">
                    <input data-format="hh:mm" type="text" name="OrderInfo[gift_time_from]" id="gift_time_from" value="<? echo substr($order->gift_time_from, 0, -3); ?>"/>
                    <span class="add-on">
                    <i data-time-icon="icon-time" data-date-icon="icon-time"></i>
                    </span>
                    </div>
                <br />
                <div id="gift_time_to_id" class="input-append date">
                    <input data-format="hh:mm" type="text" name="OrderInfo[gift_time_to]" id="gift_time_to" value="<? echo substr($order->gift_time_to, 0, -3); ?>"/>
                    <span class="add-on">
                    <i data-time-icon="icon-time" data-date-icon="icon-time"></i>
                    </span>
                    </div>

                <script type="text/javascript">
                $(function() {
                    $('#gift_date_id').datetimepicker();
                    $('#gift_time_from_id').datetimepicker({ pickDate: false});
                    $('#gift_time_to_id').datetimepicker({ pickDate: false});
                });
                </script>
                </div>
            </div>

            <div class="control-group">
                <div class="control-label">Желаемое время вручения подарка
                     <br /><br />
                    с
                    <br />
                    по
                </div>
                <div class="controls">
                <div id="recipient_desired_date_id" class="input-append date">
                    <input data-format="yyyy-MM-dd" type="text" name="OrderInfo[recipient_desired_date]" id="recipient_desired_date" value="<? echo $order->recipient_desired_date; ?>"/>
                    <span class="add-on">
                    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
                </div>
                    <br />
                    <br />
                <div id="recipient_desired_time_from_id" class="input-append date">
                    <input data-format="hh:mm" type="text" name="OrderInfo[recipient_desired_time_from]" id="recipient_desired_time_from" value="<? echo substr($order->recipient_desired_time_from, 0, -3); ?>"/>
                    <span class="add-on">
                    <i data-time-icon="icon-time" data-date-icon="icon-time"></i>
                    </span>
                    </div>
                <br />
                <div id="recipient_desired_time_to_id" class="input-append date">
                    <input data-format="hh:mm" type="text" name="OrderInfo[recipient_desired_time_to]" id="recipient_desired_time_to" value="<? echo substr($order->recipient_desired_time_to, 0, -3); ?>"/>
                    <span class="add-on">
                    <i data-time-icon="icon-time" data-date-icon="icon-time"></i>
                    </span>
                    </div>

                <script type="text/javascript">
                $(function() {
                    $('#recipient_desired_date_id').datetimepicker();
                    $('#recipient_desired_time_from_id').datetimepicker({ pickDate: false});
                    $('#recipient_desired_time_to_id').datetimepicker({ pickDate: false});
                });
                </script>
            </div>
            </div>
        </div>

        <div class="tab-pane " id="activation">
            <!-- Блок информация о активации -->
            <div class="control-group">
                <!--
                <label class="checkbox inline">
                   <input type="checkbox" id="inlineCheckbox1" value="1"> Оформлено
                </label> -->
                <label class="checkbox inline">
                    <input type="checkbox" id="inlineCheckbox2" name="OrderInfo[is_paid]" <?= $order->is_paid ? 'checked="checked"' : '' ?> value="1"> Оплачено
                </label>
                <!--
                <label class="checkbox inline">
                    <input type="checkbox" id="inlineCheckbox3" name="OrderInfo[is_safe]" value="1"> Есть чек
                </label><br /> -->
                <label class="checkbox inline">
                    <input type="checkbox" id="inlineCheckbox5" name="OrderInfo[delivered]" <?= $order->isDelivered() ? 'checked="checked"' : '' ?> value="1"> Доставлено
                </label>

                <label class="checkbox inline">
                    <input type="checkbox" id="inlineCheckbox6" name="OrderInfo[canceled]" <?= $order->is_canceled ? 'checked="checked"' : '' ?> value="1"> Отменён
                </label>
                <label class="checkbox inline">
                    <input type="checkbox" id="inlineCheckbox7" name="OrderInfo[processed]" <?= $order->is_processed ? 'checked="checked"' : '' ?> value="1"> Обработан
                </label>
            </div>


            <div class="tab-content">
                <ul class="nav nav-tabs">
                    <? foreach ($order->orderItems as $k=>$orderItem){?>
                    <li <?=$k==0?'class="active"':''?>><a href="#order-item_<?=$k?>">
                            <? $titleItem = $orderItem->itemObj->title; ?>
                            <?=mb_strlen($titleItem)>10 ? mb_substr($titleItem, 0, 10).'...':$titleItem?>
                        </a></li>
                    <? }?>
                </ul>
                <? foreach ($order->orderItems as $k=>$orderItem){
                    /** @var OrderItems $orderItem */
                    ?>
                <div class="tab-pane <?=$k==0?'active':''?>" id="order-item_<?=$k?>">
                    <?=$orderItem->getIsActivated() ? '<span style="color: green; float: right;">Активирован</span>' : '<span style="color: yellow; float: right;">Не активирован</span>'?>
                    <? if (!empty($orderItem->code)) {?>
                        <a href="/order/printPdf?id=<?=$orderItem->id?>" target="_blank">PDF</i></a>
                    <?}?>
                    <? if (!empty($orderItem->code)) {?>
                        <a style="right: 24px;top: -1px;" href="/order/word?site=<?=$orderItem->id?>" target="_blank"><img src="/img/new_design/word.png"> WORD</a>
                    <?}?>

                    <div style="clear:both;display:block;height:0;width:0;font-size:0;visibility:hidden;"></div>



                    <div class="control-group">
                        <div class="control-label">Впечатление: </div>
                        <div class="controls">
                            <a href="<?php echo '/admin/' . ($orderItem->itemObj->type == 'group' ? 'superitem/edit/id/':'item/manage/id/').$orderItem->itemObj->id; ?>">
                                <?=$orderItem->itemObj->title; ?>
                            </a>
                        </div>
                    </div>



                    <? if(!empty($orderItem->optionObj)) { ?>
                        <div class="control-group">
                            <div class="control-label">Выбраная опция: </div>
                            <div class="controls">
                                <textarea disabled type="text"><?=$orderItem->optionObj->name?></textarea>
                            </div>
                        </div>
                    <? } ?>
                    <div class="control-group">
                        <div class="control-label">Цена: </div>
                        <div class="controls">
                            <input disabled type="text" value="<?=$orderItem->item_price?>"/>
                        </div>
                    </div>
                    <? if ($orderItem->discount_percent) {?>
                        <div class="control-group">
                            <div class="control-label">Скидка: </div>
                            <div class="controls">
                                <input disabled type="text" value="<?=$orderItem->discount_percent?>%"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <div class="control-label">Цена co скидкой: </div>
                            <div class="controls">
                                <input disabled type="text" value="<?=$orderItem->item_price_with_discount?>"/>
                            </div>
                        </div>
                    <? } ?>
                    <div class="control-group">
                        <?
                        if($orderItem->code == '000000000000') {
                            echo '<span style="color: #8e8989; font-size: 11px;">Это старый заказа поэтому код активации записан только у первой позиции!</span>';
                        }
                        ?>
                        <div class="control-label">Номер карты: </div>
                        <div class="controls">
                            <input style="width: 100px" type="text" disabled name="code" id="code" class="cart-number" placeholder="000000000000" value="<?=$orderItem->code?>" maxlength="12" />
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="control-label">Активировал <br /> Имя <br /> Фамилия</div>
                        <div class="controls"><br />
                            <input type="text" name="OrderInfo[recivier_name][item_<?=$orderItem->id?>]" id="recivier_name" value="<? echo $orderItem->recivier_name; ?>"/>
                            <input type="text" name="OrderInfo[recivier_lname][item_<?=$orderItem->id?>]" id="recivier_lname" value="<? echo $orderItem->recivier_lname; ?>"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="control-label">Email активировавшего</div>
                        <div class="controls"><input type="text" name="OrderInfo[recivier_email][item_<?=$orderItem->id?>]" id="recivier_email" value="<?php echo $orderItem->recivier_email; ?>"/></div>
                    </div>
                    <div class="control-group">
                        <div class="control-label">Телефон активировавшего</div>
                        <div class="controls"><input type="text" name="OrderInfo[recivier_phone][item_<?=$orderItem->id?>]" id="recivier_phone" value="<?php echo $orderItem->recivier_phone; ?>"/></div>
                    </div>
                    <div class="control-group">
                        <div class="control-label">Дата активации</div>
                        <div class="controls">
                            <div id="active_date_id_<?=$k?>" class="input-append date">
                                <input data-format="yyyy-MM-dd" type="text" name="OrderInfo[recivier_activation_date][item_<?=$orderItem->id?>]" id="active_date" value="<?php echo $orderItem->recivier_activation_date; ?>"/>
                    <span class="add-on">
                    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                    </span>
                            </div>

                            <script type="text/javascript">
                                $(function() {
                                    $('#active_date_id_<?=$k?>').datetimepicker();
                                });
                            </script>
                        </div>
                    </div>
                    <p><strong>Цена партнера</strong></p>
                    <ul>
                        <?
                        $partnerPrices = $orderItem->itemObj->getPartnersForCity($order->city);
                        if(!empty($partnerPrices)) { ?>
                            <? foreach ($partnerPrices as $link): ?>
                                <li>
                                    <?= empty($link->option_id) ? '' : $order->getNameOption($link->option_id) . ': '; ?>
                                    <?= $link->partner->name; ?> - <strong><?= ($link->partner_price); ?> руб.</strong>
                                </li>
                            <? endforeach; ?>
                            <?
                        } else {
                            echo "<li>Нет Цены партнера</li>";
                        }
                            ?>
                    </ul>
                </div>
                <? }?>
            </div>
            <div class="control-group">
                <label class="control-label">Город:</label>
                <div class="controls"><?= $order->city->title; ?></div>
            </div>
        </div>

        <div class="tab-pane" id="lkp">
            <div class="tab-content">
                <ul class="nav nav-tabs">
                    <? foreach ($order->orderItems as $k=>$orderItem){?>
                        <li <?=$k==0?'class="active"':''?>>
                            <a href="#lkp_<?=$k?>">
                                <? $titleItem = $orderItem->itemObj->title; ?>
                                <?=mb_strlen($titleItem)>10 ? mb_substr($titleItem, 0, 10).'...':$titleItem?>
                            </a>
                        </li>
                    <? }?>
                </ul>
                <? foreach ($order->orderItems as $k=>$orderItem){
                    /** @var OrderItems $orderItem */
                    $orderItemType = $orderItem->itemObj->type;
                    ?>
                    <div class="tab-pane <?=$k==0?'active':''?>" id="lkp_<?=$k?>">
                        <div class="control-group">
                            <div class="control-label">Погашен партнёрам: </div>
                            <div class="controls">
                                <input type="checkbox" name="OrderInfo[partner_activated][item_<?=$orderItem->id?>]" <?=$orderItem->partner_activated ? 'checked="checked"' : ''?>"/>
                            </div>
                        </div>
                        <script>
                            $('#partnerActivatedDate<?=$k?>').datetimepicker({pickTime: false});
                        </script>
                        <div class="control-group">
                            <div class="control-label">Когда погашен: </div>
                            <div class="controls">
                                <div id="partnerActivatedDate<?=$k?>" class="input-append date">
                                    <input class="input-small" data-format="yyyy-MM-dd" type="text" name="OrderInfo[partner_activated_date][item_<?=$orderItem->id?>]" value="<?=$orderItem->partner_activated_date?>"/>
                                    <span class="add-on">
                                        <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <div class="control-label"><?=$orderItemType == 'group' ? 'Комплект впечатлений' : 'Впечатление' ?>: </div>
                            <div class="controls">
                                <a href="<?php echo '/admin/' . ($orderItemType == 'group' ? 'superitem/edit/id/':'item/manage/id/').$orderItem->itemObj->id; ?>">
                                    <?=$orderItem->itemObj->title; ?>
                                </a>
                            </div>
                        </div>
                        <div class="control-group">
                            <div class="control-label">Партнёр: </div>
                            <div class="controls">

<!--                                --><?//var_dump(array_map(function($v){return $v['partner_id'];},$orderItem->group_item_price_data));?>
                                <select name="OrderInfo[partner][item_<?=$orderItem->id?>]">
                                    <? if (empty($orderItem->partner_id)) {?>
                                        <option value="null" selected>Партнёр не определён</option>
                                    <? } ?>
                                    <? if (!empty($orderItem->partner_id) && !isset($partners[$orderItem->partner_id])) {?>
                                        <option selected value="<?=$orderItem->partner_id?>">(<?=$orderItem->partner_id?>) Партнёр удалён</option>
                                    <? } ?>

                                    <?
                                    $availablePartnerIds = [];
                                    if ($orderItemType == 'group' && !empty($orderItem->group_item_price_data)) {
                                        $availablePartnerIds = array_values(array_map(function($v){return $v['partner_id'];},$orderItem->group_item_price_data));
                                    } ?>

                                    <? foreach ($partners as $partnerId=>$partner) {?>
                                        <option <?=$orderItem->partner_id==$partnerId ? 'selected' : ''?> value="<?=$partnerId?>">
                                            <?= array_search($partnerId, $availablePartnerIds)!== false ? '+' : '' ?>
                                            (<?=$partnerId?>) <?=$partner?>
                                        </option>
                                    <? } ?>
                                </select>

                            </div>
                        </div>
                        <? if($orderItemType == 'group') { ?>
                            <div class="control-group">
                                <div class="control-label">Активированное впечатления: </div>
                                <div class="controls">
                                    <select name="OrderInfo[activated_item][item_<?=$orderItem->id?>]" class="active_impression">
                                        <?if(empty($orderItem->activatedItemObj)):?>
                                            <option value="null" selected>Впечетление не выбрано!</option>
                                        <?endif;?>
                                        <? foreach ($orderItem->group_item_price_data as $k=>$itemData):?>
                                            <option data-option-id="<?=$itemData['option_id']?>" <?=$orderItem->activatedItemObj->id == $itemData['id'] ? 'selected' : ''?> value="<?=$itemData['id']?>">
                                                <?=$itemData['title']?>
                                            </option>
                                        <?endforeach;?>
                                    </select>
                                </div>
                            </div>
                            <? if(!empty($orderItem->activatedItemObj) || !empty($orderItem->optionObj)) { ?>
                                <div class="control-group">
                                    <div class="control-label">Опция впечатления: </div>
                                    <div class="controls">
                                        <select name="OrderInfo[option_id][item_<?=$orderItem->id?>]" class="option_impression">
                                            <option value="0" <?=$orderItem->optionObj->id == 0 ? 'selected' : ''?>>Опция не выбрана!</option>
                                            <? foreach ($orderItem->group_item_price_data as $k=>$itemData):?>
                                                <?$option_ids[] = $itemData['option_id'];?>
                                                <?if($itemData['option_id'] != 0):?>
                                                    <option <?=$orderItem->optionObj->id == $itemData['option_id'] ? 'selected' : ''?> value="<?=$itemData['option_id']?>">
                                                        <?=$itemData['option_small_name']?>
                                                    </option>
                                                <?endif;?>
                                            <?endforeach;?>
                                            <?if(!in_array($orderItem->optionObj->id, $option_ids)):?>
                                                <option selected value="<?=$orderItem->optionObj->id?>">
                                                    <?=$orderItem->optionObj->name?>
                                                </option>
                                            <?endif;?>
                                        </select>
                                    </div>
                                </div>
                            <? } ?>
                        <? } else { ?>
                            <? if(!empty($orderItem->optionObj)) { ?>
                                <div class="control-group">
                                    <div class="control-label">Опция впечатления: </div>
                                    <div class="controls">
                                        <select name="OrderInfo[option_id][item_<?=$orderItem->id?>]" class="option_impression">
                                            <option value="0" <?=$orderItem->optionObj->id == 0 ? 'selected' : ''?>>Опция не выбрана!</option>
                                            <? foreach ($orderItem->group_item_price_data as $k=>$itemData):?>
                                                <?$option_ids[] = $itemData['option_id'];?>
                                                <?if($itemData['option_id'] != 0):?>
                                                    <option <?=$orderItem->optionObj->id == $itemData['option_id'] ? 'selected' : ''?> value="<?=$itemData['option_id']?>">
                                                        <?=$itemData['option_small_name']?>
                                                    </option>
                                                <?endif;?>
                                            <?endforeach;?>
                                            <?if(!in_array($orderItem->optionObj->id, $option_ids)):?>
                                                <option selected value="<?=$orderItem->optionObj->id?>">
                                                    <?=$orderItem->optionObj->name?>
                                                </option>
                                            <?endif;?>
                                        </select>
                                    </div>
                                </div>
                            <? } ?>
                        <?}?>


                        <div class="control-group">
                            <div class="control-label">Цена: </div>
                            <div class="controls">
                                <input name="OrderInfo[item_price][item_<?=$orderItem->id?>]" type="text" value="<?=$orderItem->item_price?>"/>
                            </div>
                        </div>
                        <div class="control-group">
                            <div class="control-label">Цена партнёра: </div>
                            <div class="controls">
                                <input name="OrderInfo[partner_price][item_<?=$orderItem->id?>]" type="text" value="<?=$orderItem->partner_price?>"/>
                            </div>
                        </div>

                        <? if($orderItemType == 'group') { ?>
                            <ul class="nav nav-tabs">
                                <? $counter = 0;?>
                                <? foreach ($orderItem->group_item_price_data as $k=>$itemData){?>
                                    <li <?=$counter++==0?'class="active"':''?>>
                                        <a href="#group_item_cache_data_<?=$k?>">
                                            <?=mb_strlen($itemData['title'])>10 ? mb_substr($itemData['title'], 0, 10).'...':$itemData['title']?>
                                        </a>
                                    </li>
                                <? }?>
                            </ul>
                            <? $counter = 0;?>
                            <div class="tab-content " style="max-height: 10000px;">
                                <? foreach ($orderItem->group_item_price_data as $k=>$itemData) { ?>
                                <div class="tab-pane <?=$counter++==0?'active':''?>" id="group_item_cache_data_<?=$k?>">
                                    <input name="GroupItem[<?=$k?>][id]" type="hidden" value="<?=$itemData['id']?>"/>
                                    <input name="GroupItem[<?=$k?>][option_id]" type="hidden" value="<?=$itemData['option_id']?>"/>
                                    <input name="GroupItem[<?=$k?>][option_name]" type="hidden" value="<?=$itemData['option_name']?>"/>
                                    <input name="GroupItem[<?=$k?>][option_city_id]" type="hidden" value="<?=$itemData['option_city_id']?>"/>
                                    <input name="GroupItem[<?=$k?>][partner_id]" type="hidden" value="<?=$itemData['partner_id']?>"/>

                                    <div class="control-group">
                                        <div class="control-label">Название: </div>
                                        <div class="controls">
                                            <input name="GroupItem[<?=$k?>][title]" type="text" value="<?=$itemData['title']?>"/>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <div class="control-label">Цена: </div>
                                        <div class="controls">
                                            <input name="GroupItem[<?=$k?>][price]" type="text" value="<?=$itemData['price']?>"/>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <div class="control-label">Цена партнёра: </div>
                                        <div class="controls">
                                            <input name="GroupItem[<?=$k?>][partner_price]" type="text" value="<?=$itemData['partner_price']?>"/>
                                        </div>
                                    </div>

                                    <? if ($itemData['option_id'] != 0) {?>
                                        <div class="control-group">
                                            <div class="control-label">Опция: </div>
                                            <div class="controls">
                                                <input name="GroupItem[<?=$k?>][option_small_name]" type="text" value="<?=$itemData['option_small_name']?>" />
                                            </div>
                                        </div>
                                    <? } else {?>
                                        <input name="GroupItem[<?=$k?>][option_small_name]" type="hidden" value="<?=$itemData['option_small_name']?>" />
                                    <? } ?>
                                </div>
                                <? } ?>
                            </div>
                        <? } ?>



                    </div>
                <? }?>
            </div>
        </div>
    <!--    <div class="tab-pane" id="stats">-->
    <!--        Статистика если есть-->
    <!--    </div>-->
    </div>
    <div class="form-actions">
        <a data-id="<?=$order->id?>" class="save btn btn-primary">Сохранить</a> | <a href="<?= $this->createUrl('order/delete',array('id' => $order->id));?>" class="btn btn-danger delete">Удалить</a> | <a href="#" class="btn cancel">Отмена</a>
    </div>
</form>

<script>
    $(document).ready(function(){
        $('.cart-number').keypress(function(){
            var txt = $(this).val();

            if (txt.length == 4) {
                //alert();
                if ($(this).attr('id') == 'code_1') {
                    $('#code_2').focus();
                } else if ($(this).attr('id') == 'code_2') {
                    $('#code_3').focus();
                }


            }

        });

        $('.cart-number').keydown(function(e){
            if (e.keyCode==8) {
                var txt = $(this).val();

                if (txt.length == 0) {
                    if ($(this).attr('id') == 'code_3') {
                        $('#code_2').focus();
                    } else if ($(this).attr('id') == 'code_2') {
                        $('#code_1').focus();
                    }
                }
            }
        });
    });
</script>
