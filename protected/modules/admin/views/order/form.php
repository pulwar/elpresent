<form action='' method='post' enctype='multipart/form-data'>
            <div class="title">
                <span><?= _('Order list'); ?></span>
            </div>
            <div class="inner">
            	<? $this->widget('NotifierWidget'); ?>
                <div class="grayLineText">
                    <span><?= _('Manage order'); ?></span>
                </div>
                <div>
  				</div>
                <div class="grayLine"><!-- --></div>
                <div class="adminItem">
                
                    <div class="rightAdminItem">
                        <div class="mainTitle">
                            <? $this->widget('AdminTableView',array('list' => array($model))); ?>
                        </div>
                        <div class="clear"><!-- --></div>
                        
                        <div class="itemTitle">
                            <?= _('Code'); ?>
                        </div>
                        <div class="itemInfo">
                            <input class="input" name="code" type="text" value='<?= $model->code ?>'>
                        </div>
                        
                        <div class="itemTitle">
                            <?= _('Delivered'); ?>
                        </div>
                        <div class="itemInfo">
                            <input type="checkbox" name='delivered' value='1' <?= $model->isDelivered() ? 'checked="checked"' : '' ?> />
                        </div>
                        
                        <div class="itemTitle">
                            <?= _('Sended'); ?>
                        </div>
                        <div class="itemInfo">
                            <input type="checkbox" name='sended' value='1' <?= $model->isSended() ? 'checked="checked"' : '' ?> />
                        </div>
                        
                        <div class="clear"><!-- --></div>
                        <div class="itemButtons">
                            <input value="<?= _('Save'); ?>" type="submit">
                            <input value="<?= _('Delete'); ?>" onclick="confirmDelete('');" type="button">
                        </div>
                    </div>
                </div>
                <div class="clear"><!-- --></div>
            </div>         
</form> 
<script language='javascript'>
function confirmDelete(url)
{
	if(!confirm('Delete?'))
		return false;
	window.location.href = url;
}
</script>
