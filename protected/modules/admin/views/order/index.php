<?php
Yii::app()->clientScript->registerScriptFile('/js/admin/order.js');
Yii::app()->clientScript->registerCssFile('/css/admin/order.css');
Yii::app()->clientScript->registerScriptFile('/js/datepicker/bootstrap-datepicker.js');
Yii::app()->clientScript->registerCssFile('/js/datepicker/datepicker.css');
?>
<div class="row">
    <h1>Список заказов</h1>
    <div class="span6">
        <div class="well">
            <p><strong>Фильтры</strong></p>
            <form method="GET" action="" class="form form-horizontal">
                <div class="control-group controls-row">
                    По дате
                    <select name="filter[date][type]" class="input-medium" >
                        <option value="order_date"<?php if(isset($filter['date']['type']) && $filter['date']['type'] == 'order_date'){?> selected="selected"<?php } ?>>заказа</option>
                        <option value="delivery_date"<?php if(isset($filter['date']['type']) && $filter['date']['type'] == 'delivery_date'){?> selected="selected"<?php } ?>>желаемой доставки</option>
                    </select>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input <?php if(!empty($filter['date']['from'])){ ?> value="<?=$filter['date']['from']; ?>"<?php } ?> placeholder="C" class="input-small" type="text" name="filter[date][from]" />
                    - <input class="input-small" placeholder="По" <?php if(!empty($filter['date']['to'])){ ?> value="<?=$filter['date']['to']; ?>"<?php } ?> type="text" name="filter[date][to]" />
                </div>
                <div class="control-group controls-row">
                    Номер заказа <input <?php if(!empty($filter['order_number'])){ ?> value="<?=$filter['order_number']; ?>"<?php } ?> type="text" class="input-small" name="filter[order_number]" />
                    Номер карты <input <?php if(!empty($filter['code'])){ ?> value="<?=$filter['code']; ?>"<?php } ?> type="text" maxlength="12" class="input-medium" name="filter[code]" />
                </div>
                <div class="control-group controls-row">
                    Партнер:
                    <input <?php if(!empty($filter['partner'])){ ?> value="<?=$filter['partner']; ?>"<?php } ?> type="text" name="filter[partner]" class="input-xlarge" />
                </div>
                <div class="control-group controls-row">
                    Впечатление: <input <?php if(!empty($filter['item_title'])){ ?> value="<?=$filter['item_title']; ?>"<?php } ?> type="text" name="filter[item_title]" class="input-xlarge" />
                    <input <?php if(!empty($filter['item_id'])){ ?> value="<?=$filter['item_id']; ?>"<?php } ?> type="hidden" name="filter[item_id]"/>
                </div>Позиции заказа:
                <div class="control-group controls-row">
                    <div class="row">
                        <div class="filterCheckboxOption">
                            <label>
                                Доставлено
                                <select class="input-small" name="filter[delivered]">
                                    <option value <?=$filter['delivered'] === null ? 'selected' : ''?>>-</option>
                                    <option value="1" <?=$filter['delivered'] === '1' ? 'selected' : ''?>>Да</option>
                                    <option value="0" <?=$filter['delivered'] === '0' ? 'selected' : ''?>>Нет</option>
                                </select>
                            </label>
                        </div>
                        <div class="filterCheckboxOption">
                            <label>
                                Отменен
                                <select class="input-small" name="filter[is_canceled]">
                                    <option value <?=$filter['is_canceled'] === null ? 'selected' : ''?>>-</option>
                                    <option value="1" <?=$filter['is_canceled'] === '1' ? 'selected' : ''?>>Да</option>
                                    <option value="0" <?=$filter['is_canceled'] === '0' ? 'selected' : ''?>>Нет</option>
                                </select>
                            </label>
                        </div>
                        <div class="filterCheckboxOption">
                            <label>
                                 Обработан
                                <select class="input-small" name="filter[is_processed]">
                                    <option value <?=$filter['is_processed'] === null ? 'selected' : ''?>>-</option>
                                    <option value="1" <?=$filter['is_processed'] === '1' ? 'selected' : ''?>>Да</option>
                                    <option value="0" <?=$filter['is_processed'] === '0' ? 'selected' : ''?>>Нет</option>
                                </select>
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="filterCheckboxOption" style="width: 150px;">
                            <label>
                                Продажа диллером
                                <select class="input-small" name="filter[is_remote_order]">
                                    <option value <?=$filter['is_remote_order'] === null ? 'selected' : ''?>>-</option>
                                    <option value="1" <?=$filter['is_remote_order'] === '1' ? 'selected' : ''?>>Да</option>
                                    <option value="0" <?=$filter['is_remote_order'] === '0' ? 'selected' : ''?>>Нет</option>
                                </select>
                            </label>
                        </div>
                    </div>
                </div>
                <input type="submit" value="Применить фильтр" class="btn btn-primary" />
            </form>
        </div>

        <div class="order-list affix-top">
            <?php $this->widget('ext.bootstrap.widgets.TbListView', array(
                 'dataProvider' => $orders,
                 'itemView'=>'_order'
            )); ?>
        </div>
    </div>
    <div class="span6 order-info-wrap">
        <!--data-spy="affix"-->
        <div class="order-info"  data-offset-top="150">
            <p class="empty span5">Выберите заказ</p>
        </div>
        <?=$this->renderPartial('/public/loader', [], true, false); ?>

    </div>
</div>