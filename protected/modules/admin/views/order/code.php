<div class="well">
    <? if(!empty($code)) :?>
    <div>
        <h3>Индивидуальный номер карты: <?= $code['id'];?></h3><br/>
        <? foreach ($code['message'] as $v): ?>
        <p><?= $v; ?></p>
        <? endforeach; ?>
    </div><br/>
    <? endif; ?>

    <? if(!empty($oOrderItems)) :?>
    <div>
        <p style="color: red;font-weight: bold;">Данный номер обнаружен в неактивированных заказах. Удалить его можно вручную, перейдя по ссылке:</p><br/>
        <? foreach ($oOrderItems as $v):?>
        <p>
            <a href="/admin/order/manage/id/<?=$v->orderObj->id?>" target="_blank" >Номер заказа:<?= $v->orderObj->order_number;?> (позиция: <?= htmlspecialchars($v->itemObj->title);?>)</a>
        </p>
        <? endforeach; ?>

    </div><br/>
    <? endif; ?>

    <? if(!empty($dOrderItems)) :?>
    <div>
        <p>Данный номер был удален в следующих заказах:</p><br/>
        <? foreach ($dOrderItems as $v):?>
        <p>
            Номер заказа:<?= $v->orderObj->order_number;?> (позиция: <?= htmlspecialchars($v->itemObj->title);?>)
        </p>
        <? endforeach; ?>

    </div><br/>
    <? endif; ?>

    <div style="padding-top: 40px;">
    <form action="" method="POST" >
        <label>Индивидуальный номер карты: <input type="test" name="code" maxlength="12" /></label>
        <input class="btn btn-primary" type="submit" value="Удалить номер из базы" />

    </form>
    </div>
</div>
