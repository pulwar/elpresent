<div class="item" data-id="<?php echo $data->id; ?>">
    <p class="name">
        <?php
            $items = $data->orderItems;
            if(count($items) == 1) {
                echo "<span><b>Одно впечатление:</b> <br>";
            } else {
                echo " <span><b>Комплект:</b> <br>";
            }
        ?>
        <? foreach($items as $item){?>
            <a href="<?='/admin/' . ($item->itemObj->type == 'group' ? 'superitem/edit/id/':'item/manage/id/').$item->itemObj->id; ?>">
                <?=$item->itemObj->title;?>  <?=$item->itemObj->verified ? '<i class="icon-plus"></i>' : '<i class="icon-minus"></i>';?><br>
            </a>
        <?}?>
                </span>
        <?php if ($data->is_fast_order) echo '<span class="label label-important">быстрый заказ</span>'; ?>
	    <?php if ($data->is_remote_order) echo '<span class="label label-purple">продажа диллером</span>'; ?>
        <?php if($data->is_paid) {?><?php if($data->sended){?><span class="label label-inverse">cгенерировано</span><?php } ?> <span class="label label-info">оплачено</span><?php } ?>
        <?php if($data->delivered){?><span class="label label-success">доставлено</span><?php } ?>
        <?php if($data->is_canceled){?><span class="label label-danger">Отменён</span><?php } ?>
        <?php if($data->is_processed){?><span class="label label-warning">Обработан</span><?php } ?>
	    <a href="/order/print?id=null&site=<?=$data->id?>" class="my-print" target="_blank"><i class="icon-print"></i></a>
    </p>
    <p class="price"><?=($data->price);?> руб.</p>
    <p class="customer"><strong><?php echo $data->name , ' ', $data->lname; ?></strong>, <?php echo $data->formatDate('order_date'); ?></p>
</div>