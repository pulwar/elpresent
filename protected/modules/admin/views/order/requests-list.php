<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th>Имя</th>
        <th>Телефон</th>
        <th>Впечатление</th>
        <th>Дата</th>
        <th>Обработано</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($list as $item) {?>
    <tr>
        <td><?php echo htmlentities($item->name, null, 'UTF-8'); ?></td>
        <td><?php echo htmlentities($item->phone, null, 'UTF-8'); ?></td>
        <td><?php echo $item->item->title; ?></td>
        <td><?php echo $item->date; ?></td>
        <td><?php echo $item->processed ? 'Да':'Нет'; ?></td>
        <td><a href="/order/pack/item_id/<?= $item->item->id; ?>.html" class="btn btn-primary" target="_blank" title="Откроется в новой вкладке">Оформить заказ</a> <a href="/admin/order/markAsProcessed?id=<?= $item->id; ?>" class="btn btn-success" title="Откроется в новой вкладке">Обработанно</a></td>
    </tr>
        <?php }?>
    </tbody>
</table>