<style>
    #floatingCirclesG{
        position:relative;
        width:80px;
        height:80px;
        -moz-transform:scale(0.6);
        -webkit-transform:scale(0.6);
        -ms-transform:scale(0.6);
        -o-transform:scale(0.6);
        transform:scale(0.6);
    }

    .f_circleG{
        position:absolute;
        background-color:#F7E3C6;
        height:15px;
        width:15px;
        -moz-border-radius:7px;
        -moz-animation-name:f_fadeG;
        -moz-animation-duration:0.8s;
        -moz-animation-iteration-count:infinite;
        -moz-animation-direction:normal;
        -webkit-border-radius:7px;
        -webkit-animation-name:f_fadeG;
        -webkit-animation-duration:0.8s;
        -webkit-animation-iteration-count:infinite;
        -webkit-animation-direction:normal;
        -ms-border-radius:7px;
        -ms-animation-name:f_fadeG;
        -ms-animation-duration:0.8s;
        -ms-animation-iteration-count:infinite;
        -ms-animation-direction:normal;
        -o-border-radius:7px;
        -o-animation-name:f_fadeG;
        -o-animation-duration:0.8s;
        -o-animation-iteration-count:infinite;
        -o-animation-direction:normal;
        border-radius:7px;
        animation-name:f_fadeG;
        animation-duration:0.8s;
        animation-iteration-count:infinite;
        animation-direction:normal;
    }

    #frotateG_01{
        left:0;
        top:33px;
        -moz-animation-delay:0.3s;
        -webkit-animation-delay:0.3s;
        -ms-animation-delay:0.3s;
        -o-animation-delay:0.3s;
        animation-delay:0.3s;
    }

    #frotateG_02{
        left:9px;
        top:9px;
        -moz-animation-delay:0.4s;
        -webkit-animation-delay:0.4s;
        -ms-animation-delay:0.4s;
        -o-animation-delay:0.4s;
        animation-delay:0.4s;
    }

    #frotateG_03{
        left:33px;
        top:0;
        -moz-animation-delay:0.5s;
        -webkit-animation-delay:0.5s;
        -ms-animation-delay:0.5s;
        -o-animation-delay:0.5s;
        animation-delay:0.5s;
    }

    #frotateG_04{
        right:9px;
        top:9px;
        -moz-animation-delay:0.6s;
        -webkit-animation-delay:0.6s;
        -ms-animation-delay:0.6s;
        -o-animation-delay:0.6s;
        animation-delay:0.6s;
    }

    #frotateG_05{
        right:0;
        top:33px;
        -moz-animation-delay:0.7s;
        -webkit-animation-delay:0.7s;
        -ms-animation-delay:0.7s;
        -o-animation-delay:0.7s;
        animation-delay:0.7s;
    }

    #frotateG_06{
        right:9px;
        bottom:9px;
        -moz-animation-delay:0.8s;
        -webkit-animation-delay:0.8s;
        -ms-animation-delay:0.8s;
        -o-animation-delay:0.8s;
        animation-delay:0.8s;
    }

    #frotateG_07{
        left:33px;
        bottom:0;
        -moz-animation-delay:0.9s;
        -webkit-animation-delay:0.9s;
        -ms-animation-delay:0.9s;
        -o-animation-delay:0.9s;
        animation-delay:0.9s;
    }

    #frotateG_08{
        left:9px;
        bottom:9px;
        -moz-animation-delay:1s;
        -webkit-animation-delay:1s;
        -ms-animation-delay:1s;
        -o-animation-delay:1s;
        animation-delay:1s;
    }

    @-moz-keyframes f_fadeG{
        0%{
            background-color:#2E2D2E}

        100%{
            background-color:#F7E3C6}

    }

    @-webkit-keyframes f_fadeG{
        0%{
            background-color:#2E2D2E}

        100%{
            background-color:#F7E3C6}

    }

    @-ms-keyframes f_fadeG{
        0%{
            background-color:#2E2D2E}

        100%{
            background-color:#F7E3C6}

    }

    @-o-keyframes f_fadeG{
        0%{
            background-color:#2E2D2E}

        100%{
            background-color:#F7E3C6}

    }

    @keyframes f_fadeG{
        0%{
            background-color:#2E2D2E}

        100%{
            background-color:#F7E3C6}

    }

</style>
<div id="floatingCirclesG" class="ajaxLoader">
    <div class="f_circleG" id="frotateG_01">
    </div>
    <div class="f_circleG" id="frotateG_02">
    </div>
    <div class="f_circleG" id="frotateG_03">
    </div>
    <div class="f_circleG" id="frotateG_04">
    </div>
    <div class="f_circleG" id="frotateG_05">
    </div>
    <div class="f_circleG" id="frotateG_06">
    </div>
    <div class="f_circleG" id="frotateG_07">
    </div>
    <div class="f_circleG" id="frotateG_08">
    </div>
</div>
