<h4>Изменение контента Landing page</h4>

<div class="landing update">
    <? $form = $this->beginWidget('CActiveForm',array(
        // будет передаваться в post как параметр
        'id'=>'admin-landing',
        'enableAjaxValidation' => false,
    ));
    ?>

    <?=$form->errorSummary($model); ?>

    <div>
        <?=$form->labelEx($model, 'meta_d'); ?>
        <?=$form->textArea($model, 'meta_d'); ?>
    </div>

    <div>
        <?=$form->labelEx($model, 'meta_k'); ?>
        <?=$form->textField($model,'meta_k'); ?>
    </div>

    <div>
        <?=$form->labelEx($model, 'phones'); ?>
        <?=$form->textField($model,'phones'); ?>
    </div>

    <div>
        <?=$form->labelEx($model, 'address'); ?>
        <? $this->widget('ext.tinymce.TinyMce', array(
                'model' => $model,
                'attribute' => 'address',
                'fileManager' => array(
                'class' => 'ext.elFinder.TinyMceElFinder',
                'connectorRoute'=>'admin/elfinder/connector',
            ),
        ));?>
    </div>

    <div>
        <?=$form->labelEx($model, 'time_work'); ?>
        <? $this->widget('ext.tinymce.TinyMce', array(
            'model' => $model,
            'attribute' => 'time_work',
            'fileManager' => array(
                'class' => 'ext.elFinder.TinyMceElFinder',
                'connectorRoute'=>'admin/elfinder/connector',
            ),
        ));?>
    </div>

    <div>
        <?=$form->labelEx($model, 'map_code'); ?>
        <?=$form->textArea($model, 'map_code'); ?>
    </div>

    <div>
        <?=$form->labelEx($model, 'delivery'); ?>
        <? $this->widget('ext.tinymce.TinyMce', array(
            'model' => $model,
            'attribute' => 'delivery',
            'fileManager' => array(
                'class' => 'ext.elFinder.TinyMceElFinder',
                'connectorRoute'=>'admin/elfinder/connector',
            ),
        ));?>
    </div>

    <div>
        <?=$form->labelEx($model, 'customers'); ?>
        <? $this->widget('ext.tinymce.TinyMce', array(
            'model' => $model,
            'attribute' => 'customers',
            'fileManager' => array(
                'class' => 'ext.elFinder.TinyMceElFinder',
                'connectorRoute'=>'admin/elfinder/connector',
            ),
        ));?>
    </div>

    <div>
        <?=$form->labelEx($model, 'email'); ?>
        <?=$form->emailField($model, 'email'); ?>
    </div>

    <div>
        <?=CHtml::submitButton('Изменить', ['class' => 'btn btn-primary']); ?>
    </div>
    <? $this->endWidget(); ?>
</div>


