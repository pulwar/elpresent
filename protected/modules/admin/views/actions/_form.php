<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'page-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    )); ?>

    <p class="note">Поля c <span class="required">*</span> обязательны для заполнения.</p>

    <?php echo $form->errorSummary($model); ?>

    <div>
        <?php echo $form->labelEx($model, 'title'); ?>
        <?php echo $form->textField($model, 'title'); ?>
        <?php echo $form->error($model, 'title'); ?>
    </div>

    <div>
        <?php echo $form->labelEx($model, 'name'); ?>
        <?php echo $form->textField($model, 'name'); ?>
        <?php echo $form->error($model, 'name'); ?>
    </div>

    <div>
        <?php echo $form->labelEx($model, 'meta_k'); ?>
        <?php echo $form->textField($model, 'meta_k'); ?>
        <?php echo $form->error($model, 'meta_k'); ?>
    </div>

    <div>
        <?php echo $form->labelEx($model, 'meta_d'); ?>
        <?php echo $form->textArea($model, 'meta_d'); ?>
        <?php echo $form->error($model, 'meta_d'); ?>
    </div>

    <div>
        <?php echo $form->labelEx($model, 'desc'); ?>
        <?php
        $this->widget('ext.tinymce.TinyMce', array(
            'model' => $model,
            'attribute' => 'desc',
            'fileManager' => array(
                'class' => 'ext.elFinder.TinyMceElFinder',
                'connectorRoute'=>'admin/elfinder/connector',
            ),
        ));?>
        <?php echo $form->error($model, 'desc'); ?>
    </div>

    <div>
        <?php echo $form->labelEx($model, 'txt'); ?>
        <?php
        $this->widget('ext.tinymce.TinyMce', array(
            'model' => $model,
            'attribute' => 'txt',
            'fileManager' => array(
                'class' => 'ext.elFinder.TinyMceElFinder',
                'connectorRoute'=>'admin/elfinder/connector',
            ),
        ));?>
        <?php echo $form->error($model, 'txt'); ?>
    </div>

    <div>
        <?php echo $form->labelEx($model, 'show'); ?>
        <?php echo $form->dropDownList($model, 'show', array(0 => 'Нет', 1 => 'Да')); ?>
        <?php echo $form->error($model, 'show'); ?>
    </div>

    <div>
        <?php echo $form->labelEx($model, 'order'); ?>
        <?php echo $form->numberField($model, 'order', array('min' => 0)); ?>
        <?php echo $form->error($model, 'order'); ?>
    </div>

    <div>
        <?php echo $form->labelEx($model, 'img'); ?>
        <?php echo CHtml::activeFileField($model, 'img'); ?>
        <?php echo $form->error($model, 'img'); ?>
    </div>

    <? if (is_file('images/'.$model->img)): ?>
        <div>
            <img src="/images/<?= $model->img ?>" width="50%">
        </div>
    <? endif; ?>

    <div class="buttons">
        <br>
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', ['class' => 'btn btn-primary']); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->