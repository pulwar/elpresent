<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#page-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<h1>Менеджер акций</h1>
<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'page-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id' => array(
            'name' => 'id',
            'filter' => false,
        ),
        'title',
        'name',
        'date' => array(
            'name' => 'date',
            'value' => 'date("d.m.Y",$data->date)',
            'filter' => false,
        ),
        'show' => array(
            'name' => 'show',
            'value' => '($data->show == 1 ? "Да" : "Нет")',
            'filter' => false
        ),

        'order' => array(
            'name' => 'order',
            'filter' => false,
        ),

        array(
            'class' => 'CButtonColumn',
        ),
    ),
)); ?>

<?= CHtml::link('Создать',array('/admin/actions/create'), ['class' => 'btn btn-primary']); ?>
