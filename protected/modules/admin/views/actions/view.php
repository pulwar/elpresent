<h1>Акция <i>'<?= $model->title; ?>'</i></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'id',
        'title',
        'name',
        'meta_k',
        'meta_d',
        'desc' => [
            'name' => 'desc',
            'type' => 'html'
        ],
        'txt' => [
            'name' => 'txt',
            'type' => 'html'
        ],
        'date' => array(
            'name' => 'date',
            'value' => date("d.m.Y",$model->date),
        ),
        'show' => array(
            'name' => 'show',
            'value' => $model->show == 1 ? 'Да' : 'Нет',
        ),

        'order',
    ),
)); ?>
<br>
<?php echo CHtml::link('К списку', array('/admin/actions'), ['class' => 'btn btn-primary']); ?>
