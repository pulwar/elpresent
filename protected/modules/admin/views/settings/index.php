<h1>Настройки</h1>
<?php if ( Yii::app()->user->hasFlash('setting_message_ok') ) { ?>
<div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert">×</button>
    <?php echo Yii::app()->user->getFlash('setting_message_ok'); ?>
</div>
<?php } ?>
<form action="" method="post" class="form-horizontal">
    <div class="control-group">
        <label class="control-label">Новостной блок:</label>
        <div class="controls">
            <label style="display: inline-block; margin-left: 20px;">
                <input type="radio" name="enable_news_block" value="1" <?= ( Settings::getValueByAlias('enable_news_block') == 1 ) ? ' checked="checked"' : ''; ?> />
                <span>Да</span>
            </label>

            <label style="display: inline-block; margin-left: 10px;">
                <input type="radio" name="enable_news_block" value="0" <?= ( Settings::getValueByAlias('enable_news_block') == 0 ) ? ' checked="checked"' : ''; ?> />
                <span>Нет</span>
            </label>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Название сайта:</label>
        <div class="controls">
            <input type="text" name="site_name" value="<?= Settings::getValueByAlias('site_name'); ?>" />
        </div>
    </div>

    <div class="control-group">
        <label class="control-label">Email:</label>
        <div class="controls">
            <input type="email" name="site_name" value="<?= Settings::getValueByAlias('site_name'); ?>" />
            <span class="help-block">Email на который будет приходить сообщение о купленных подарках</span>
        </div>
    </div>
    <div class="form-actions">
        <input type="submit" class="btn btn-primary" value="Сохранить" />
    </div>
</form>