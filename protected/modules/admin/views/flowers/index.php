<a href="/admin/flowers/manage">Добавить</a>


<? if ( Yii::app()->user->hasFlash('flower_message_successfully') ) : ?>
<div class="b-nessage-successfully">
	<?= Yii::app()->user->getFlash('flower_message_successfully'); ?>
</div>
<? endif; ?>


<?php
$this->widget('ext.bootstrap.widgets.TbGridView', array (
    'type' => array('condensed', 'striped'),
    'dataProvider' => $oFlowers,
    'columns'      => array
    (
        'flower_id',
        'flower_title',
        'flower_price',
        array
        (
            'name'  => 'flower_image',
            'type'  => 'html',
            'value' => 'CHtml::image("/upload/flowers/".$data->flower_image."_48.png")'
        ),
        array
        (
            'type'  => 'html',
            'value' => 'CHtml::link("[редактировать]", "/admin/flowers/manage/id/".$data->flower_id)."<br />".CHtml::link("[удалить]", "/admin/flowers/delete/id/".$data->flower_id, array("class" => "delete"))'
        )
    )
));

?>

<script>
$(document).ready(function()
{
	$('a.delete').click(function()
	{
		if ( confirm('Вы действительно удалить цветы?') )
		{
			return true;
		}

		return false;
	});	
});
</script>