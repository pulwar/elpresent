<? if ( count($oFlower->getErrors()) ) : ?>
	<?= CHtml::errorSummary($oFlower); ?>
<? endif; ?>

<form action="" class="b-admin-form" method="post" enctype="multipart/form-data" class="b-admin-form">
	<p>
		<label>
			<span>Название</span>
			<input type="text" name="Flower[flower_title]" value="<?= $oFlower->flower_title; ?>" />
		</label>
	</p>
	<p>
		<label>
			<span>Цена</span>
			<input type="text" name="Flower[flower_price]" value="<?= $oFlower->flower_price; ?>" />
		</label>
	</p>
	<p>
		<label>
			<? if ( $oFlower->flower_image ) : ?>
				<img src="/upload/flowers/<?= $oFlower->flower_image.'_48.png'; ?>" />
			<? endif; ?>
			<span>Изображение</span>
			<input type="file" name="Flower[flower_image]" />
		</label>
	</p>
	<p>
		<button>Сохранить</button>
	</p>
</form>