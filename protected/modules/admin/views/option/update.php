<? header('Content-type: text/html; charset=UTF-8'); ?>
<div class="option update">
        <?= CHtml::errorSummary($optionUpdateForm); ?>
        <?php if (Yii::app()->user->hasFlash('update_option_success')): ?>
            <div class="alert alert-info">
                <?php echo Yii::app()->user->getFlash('update_option_success'); ?>
            </div>
        <?php endif; ?>
        <div class="row">
            <div class="span10"><h4>Изменение опций подарка <i><?= $model->title ?></i></h4></div>
            <div class="span2">
                <ul class="nav nav-pills">
                	<li>
                        <a href="<?= Yii::app()->createUrl('/admin/option/create', array('id' => $model->id)) ?>" title="Добавить"><i class="icon-plus"></i></a>
                    </li>
                    <li   class="active">
                        <a href="<?= Yii::app()->createUrl('/admin/option/update', array('id' => $model->id)) ?>" title="Изменить"><i class="icon-edit"></i></a>
                    </li>
                    <li>
                        <a href="<?= Yii::app()->createUrl('/admin/option/delete', array('id' => $model->id)) ?>"  title="Удалить все"><i class="icon-trash"></i></a>
                    </li>
                    
                    <li>
                    	<a href="<?= Yii::app()->createUrl('/admin/item/manage', array('id' => $model->id)) ?>" title="В подарок"><i class="icon-home"></i></a>
                    </li>
                    <li>
                    	<a href="<?= Yii::app()->createUrl($model->perm_link) ?>" title="Просмотреть"><i class="icon-zoom-in"></i></a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs">
            <? foreach ($cities as $k => $citiy): ?>
                <li <?= empty($k) ? 'class="active"' : '' ?>><a href="#<?= $citiy->id ?>" data-toggle="tab"><?= $citiy->title ?></a></li>
            <? endforeach; ?>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <? foreach ($cities as $k => $citiy): ?>
                <div class="tab-pane  <?= empty($k) ? 'active' : '' ?>" id="<?= $citiy->id ?>">
                    <form action="" method="post">
                        <ul class="thumbnails">
                            <? //html::pr($model->prices,1)?>
                            <? foreach ($model->prices as $k_price => $v_price): ?>
                                <? if ($v_price->city->id == $citiy->id): ?>
                                    <? $option_id = empty($v_price->option->id) ? 0 : $v_price->option->id ?>
                                    <li class="span4">
                                        <div class="thumbnail">
                                            <? if (!empty($option_id)): ?>
                                                <p>
                                                    <label>Название опции </label>
                                                    <input value="<?= $v_price->option->name; ?>" type="text"
                                                           name="option[option_name][]" required>
                                                </p>
                                                <p>
                                                    <label>Короткое название опции </label>
                                                    <input value="<?= $v_price->option->small_name; ?>" type="text"
                                                           name="option[option_small_name][]" required>
                                                </p>

                                                <p>
                                                    <label>Описание опции </label>
                                                    <textarea name="option[option_desc][]"
                                                              required><?= $v_price->option->desc; ?></textarea>
                                                </p>

                                                <p>
                                                    <label>Id опции </label>
                                                    <input value="<?= $v_price->option->id; ?>" type="text"
                                                           name="option[option_id][]"
                                                           readonly required>
                                                </p>

                                                <? foreach ($model->partners as $partner): ?>
                                                    <p>
                                                        <? if ($v_price->option->id == $partner->option_id && $partner->partner->city_id == $v_price->city->id): ?>
                                                            <label>Цена <?= $partner->partner->name ?></label>
                                                            <input type="number" step="any"
                                                                   name="partner_price[<?= $v_price->option->id ?>][<?= $partner->partner->id ?>]"
                                                                   value="<?= $partner->partner_price ?>" required>
                                                        <? endif; ?>
                                                    </p>
                                                <? endforeach; ?>
                                            <? else: ?>
                                                <div class="alert alert-error">Нет опций</div>
                                            <? endif; ?>
                                            <p>
                                                <label>Цена </label>
                                                <input value="<?= $v_price['item_price'] ?>" type="number" name="option[price][]" required step="any">
                                            </p>
                                            <p>
                                                <a href="<?= Yii::app()->createUrl('/admin/option/unlink', ['option_id' => $v_price->option->id, 'item_id' => $model->id, 'citiy_id' => $v_price->city->id]) ?>" title="Удалить">
                                                    <i class="icon-trash"></i>
                                                </a>
                                            </p>
                                        </div>
                                    </li>
                                <? endif; ?>

                            <? endforeach; ?>
                        </ul>
                        <p style="display:none;">
                            город <input value="<?= $citiy->id ?>" type="text" name="citiy_id" readonly>
                        </p>
                        <label>Какая опция будет выводиться в комплекте</label>
                        <select name="show_in_group_id">
                            <option value="0">Опция для вывода в комплекте</option>
                            <? foreach ($model->prices as $k_price => $v_price): ?>
                                <? if ($v_price->city->id == $citiy->id): ?>
                                    <option value="<?= $v_price->option->id;?>" <?=Item::getOptionInGroup($model->prices_cache[$citiy->id])==$v_price->option->name ? 'selected' : ''?> >
                                        <?= $v_price->option->name; ?>
                                    </option>
                                <? endif;?>
                            <? endforeach?>
                        </select>

                        <hr style="clear:both"/>
                        <input type="submit" name="submit"
                               value="Изменить <?= $citiy->title ?>" <?= empty($option_id) ? 'disabled' : '' ?>
                               class="btn btn-primary">


                    </form>
                </div>
            <? endforeach; ?>
        </div>
    </div>
<? //html::pr($model->prices,1);?>