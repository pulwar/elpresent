<? header('Content-type: text/html; charset=UTF-8'); ?>

<div class="option create">
    <?= CHtml::errorSummary($optionCreateForm); ?>

    <?php if (Yii::app()->user->hasFlash('create_option_success')): ?>
        <div class="alert alert-info">
            <?php echo Yii::app()->user->getFlash('create_option_success'); ?>
        </div>
    <?php endif; ?>
    <div class="row">
            <div class="span10"><h4>Добавление опций подарка <i><?= $model->title ?></i></h4></div>
            <div class="span2">
                <ul class="nav nav-pills">
                	<li class="active">
                        <a href="<?= Yii::app()->createUrl('/admin/option/create', array('id' => $model->id)) ?>" title="Добавить"><i class="icon-plus"></i></a>
                    </li>
                    <li>
                        <a href="<?= Yii::app()->createUrl('/admin/option/update', array('id' => $model->id)) ?>" title="Изменить"><i class="icon-edit"></i></a>
                    </li>
                    <li>
                        <a href="<?= Yii::app()->createUrl('/admin/option/delete', array('id' => $model->id)) ?>"  title="Удалить"><i class="icon-trash"></i></a>
                    </li>
                    
                    <li>
                    	<a href="<?= Yii::app()->createUrl('/admin/item/manage', array('id' => $model->id)) ?>" title="В подарок"><i class="icon-home"></i></a>
                    </li>
                    <li>
                    	<a href="<?= Yii::app()->createUrl($model->perm_link) ?>" title="Просмотреть"><i class="icon-zoom-in"></i></a>
                    </li>
                </ul>
            </div>
        </div>

    <!-- Nav tabs -->
    <ul class="nav nav-tabs">
        <? foreach ($cities as $k => $citiy): ?>
            <li <?= empty($k) ? 'class="active"' : '' ?>><a href="#<?= $citiy->id ?>"
                                                            data-toggle="tab"><?= $citiy->title ?></a></li>
        <? endforeach; ?>
    </ul>
    <!-- Tab panes -->

    <div class="tab-content">
        <? foreach ($cities as $k => $citiy): ?>
            <div class="tab-pane  <?= empty($k) ? 'active' : '' ?>" id="<?= $citiy->id ?>">
                <form action="" method="post">
                    <ul class="thumbnails">
                        <? foreach ($model->prices as $k_price => $v_price): ?>
                            <? if ($v_price->city->id == $citiy->id): ?>
                                <li class="span4">
                                    <div class="thumbnail">
                                        <? $option_id = empty($v_price->option->id) ? 0 : $v_price->option->id ?>

                                        <p style="display:<?= empty($v_price->option->name) ? 'none' : 'block' ?>">
                                        <h4><?= $v_price->option->name; ?></h4>

                                        </p>

                                        <p style="display:<?= empty($v_price->option->desc) ? 'none' : 'block' ?>">

                                        <div><?= $v_price->option->desc; ?></div>
                                        </p>
                                        <p>
                                            <i>Цена:</i>
                                            <?= $v_price['item_price'] ?>
                                        </p>

                                        <? if (empty($v_price->option->id)): ?>
                                            <? $partners_options = []; ?>
                                            <? foreach ($model->partners as $partner): ?>
                                                <p><? if ($partner->partner->city_id == $v_price->city->id): ?>
                                                        <i>Цена <?= $partner->partner->name ?>: </i>
                                                        <?= $partner->partner_price ?>
                                                        <? $partners_options[$partner->partner->id] = $partner->partner->name ?>
                                                    <? endif; ?></p>
                                            <? endforeach; ?>
                                        <? else: ?>
                                            <? $partners_options = []; ?>
                                            <? foreach ($model->partners as $partner): ?>
                                                <p><? if ($v_price->option->id == $partner->option_id && $partner->partner->city_id == $v_price->city->id): ?>
                                                    <i>Цена <?= $partner->partner->name ?> :</i>
                                                    <?= $partner->partner_price ?>

                                                    <? $partners_options[$partner->partner->id] = $partner->partner->name ?>
                                                    </p>
                                                <? endif; ?>
                                            <? endforeach; ?>
                                        <? endif; ?>
                                    </div>
                                </li>
                            <? endif; ?>
                        <? endforeach; ?>
                    </ul>
                    <p>
                        <label>Название опции</label>
                        <input value="<?= $optionCreateForm->option_name ?>" name="option_name" type="text" required="required"/>
                    </p>
                    <p>
                        <label>Короткое название опции </label>
                        <input value="<?= $optionCreateForm->option_small_name; ?>" name="option_small_name" type="text" required="required"/>
                    </p>

                    <p>
                        <label>Описание опции</label>
                        <textarea name="option_desc" required><?= $optionCreateForm->option_desc ?></textarea>
                    </p>

                    <p>
                        <label>Цена </label>
                        <input value="<?= $optionCreateForm->price ?>" type="number" step="any" name="price" required>
                    </p>

                    <p>
                        <? foreach ($partners_options as $partner_option_k => $partner_option_v): ?>
                            <label>Цена партнера <?= $partner_option_v ?></label>
                            <input name="partner_price[<?= $partner_option_k ?>]"
                                   value="<?= $optionCreateForm->partner_price[$partner_option_k] ?>" type="number" step="any" required/>
                        <? endforeach; ?>
                    </p>

                    <p style="display:none;">
                        город <input value="<?= $citiy->id ?>" type="text" name="citiy_id" readonly>
                    </p>
                    <input type="submit" name="submit" value="Сохранить (<?= $citiy->title ?>)" class="btn btn-primary">
                </form>
            </div>
        <? endforeach; ?>
    </div>
</div>