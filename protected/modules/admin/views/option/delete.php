<? //html::pr($model->prices,1);?>
<? header('Content-type: text/html; charset=UTF-8'); ?>
<div class="option del">
        <?= CHtml::errorSummary($optionDeleteForm); ?>
        <?php if (Yii::app()->user->hasFlash('delete_option_success')): ?>
            <div class="alert alert-info">
                <?php echo Yii::app()->user->getFlash('delete_option_success'); ?>
            </div>
        <?php endif; ?>
        <div class="row">
            <div class="span10"><h4>Удаление опций подарка <i><?= $model->title ?></i></h4></div>
            <div class="span2">
                <ul class="nav nav-pills">
                	<li>
                        <a href="<?= Yii::app()->createUrl('/admin/option/create', array('id' => $model->id)) ?>" title="Добавить"><i class="icon-plus"></i></a>
                    </li>
                    <li>
                        <a href="<?= Yii::app()->createUrl('/admin/option/update', array('id' => $model->id)) ?>" title="Изменить"><i class="icon-edit"></i></a>
                    </li>
                    <li class="active">
                        <a href="<?= Yii::app()->createUrl('/admin/option/delete', array('id' => $model->id)) ?>"  title="Удалить"><i class="icon-trash"></i></a>
                    </li>
                    
                    <li>
                    	<a href="<?= Yii::app()->createUrl('/admin/item/manage', array('id' => $model->id)) ?>" title="В подарок"><i class="icon-home"></i></a>
                    </li>
                    <li>
                    	<a href="<?= Yii::app()->createUrl($model->perm_link) ?>" title="Просмотреть"><i class="icon-zoom-in"></i></a>
                    </li>
                </ul>
            </div>
        </div>

        <!-- Nav tabs -->
        <ul class="nav nav-tabs">
            <? foreach ($cities as $k => $citiy): ?>
                <li <?= empty($k) ? 'class="active"' : '' ?>><a href="#<?= $citiy->id ?>"
                                                                data-toggle="tab"><?= $citiy->title ?></a></li>
            <? endforeach; ?>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <? foreach ($cities as $k => $citiy): ?>
                <div class="tab-pane  <?= empty($k) ? 'active' : '' ?>" id="<?= $citiy->id ?>">
                    <form action="" method="post">
                        <ul>
                            <? foreach ($model->prices as $k_price => $v_price): ?>
                                <? if ($v_price->city->id == $citiy->id): ?>
                                    <? $option_id = empty($v_price->option->id) ? 0 : $v_price->option->id ?>
                                    <? if (!empty($option_id)): ?>
                                        <li>
                                            <?= $v_price->option->name; ?>
                                            <input value="<?= $v_price->option->id; ?>" type="hidden" name="option_id[]"
                                                   readonly required>
                                        </li>
                                    <? else: ?>
                                        <div class="alert alert-error">Нет опций</div>
                                    <? endif; ?>
                                <? endif; ?>
                            <? endforeach; ?>
                        </ul>
                        <? if (!empty($option_id)): ?>
                            <div class="alert">Значения после удаления опций</div>

                            <p>
                                <label>Партнер</label>
                                <select name="partner_id">
                                    <? foreach ($getPartnerListByCityId($citiy->id) as $partner): ?>
                                        <option value="<?= $partner->id ?>"><?= $partner->name ?></option>
                                    <? endforeach; ?>
                                </select>
                                <label>Цена партнера</label>
                                <input type="number" name="partner_price" required step="any" value="<?= $optionDeleteForm->partner_price ?>">
                                <label>Цена</label>
                                <input type="number" name="price" required step="any" value="<?= $optionDeleteForm->price ?>">
                            </p>

                            <p style="display:none">
                                город <input value="<?= $citiy->id ?>" type="text" name="citiy_id" readonly required>
                            </p>
                            <input type="submit" name="submit"
                                   value="Удалить (<?= $citiy->title ?>)"
                                   class="btn btn-primary">
                        <? endif; ?>
                    </form>
                </div>
            <? endforeach; ?>
        </div>
    </div>
<? //html::pr($model->prices,1);?>