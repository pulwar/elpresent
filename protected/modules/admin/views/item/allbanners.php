<?php
$this->widget('ext.bootstrap.widgets.TbGridView', array (
            'type' => array('condensed', 'striped'),
            'dataProvider' => $banners,
            'columns' => array
                (
                'title',
                'text',
                array
                    (
                    'type' => 'html',
                    'name' => 'item',
                    'value' => '$data->item->title'

                ),
                array
                    (
                    'type' => 'html',
                    'value' => 'CHtml::link("[редактировать]", "/admin/item/addbanner/item_id/".$data->item->id."/page/".(Yii::app()->request->getParam("Item_page")))."<br />".CHtml::link("[удалить]", "/admin/item/deletebanner/id/".$data->banner_id, array("class" => "delete"))."<br />"'
                )
            )
        ));
?>