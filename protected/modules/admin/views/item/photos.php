<form class="form form-horizontal" enctype="multipart/form-data" name="newFile" method="post" action="/admin/item/photo_upload/item_id/<?=$oItem->id
?>" >
    <h1>Новое фото</h1>
    <div class="control-group">
        <label class="control-label">Путь к файлу</label>
        <div class="controls">
            <input type="file" name="newPhoto" />
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Комментарий</label>
        <div class="controls">
            <input type="text" name="newPhotoComment" />
        </div>
    </div>
    <div class="form-actions">
        <input type="submit" value="Закачать" class="btn btn-primary" />
    </div>
</form>
<form name="saveSelected" method="post" action="/admin/item/photos/item_id/<?=$oItem->id
?>" onsubmit="return onSaveSelected()"><table class="b-admin-photos-form" cellpadding="0" cellspacing="0">



    <tr>
            <td>

            </td>
            <td>

            </td>
        </tr>
    </table>
    <table class="table table-striped" cellpadding="0" cellspacing="0">
        <?php
        if (!count($oPhoto))
        {
        ?>
            <tr><td>Не найдено ни одной фотографии! :-(</td></tr>
        <?php
        }
        else
        {
            echo "
            <tr>
            <th>Выбран</th>
            <th>Порядок</th>
            <th>Автор снимка</th>
            <th>Комментарий</th>
            <th>Снимок</th>
            <th>Управление</th>
        </tr>
";
        
        foreach ($oPhoto as $photo)
        {
            $checked = ($photo->moderated) ? 'checked' : '';
            ?>
<tr>
    <td><input type="checkbox" class="moderated" <?php echo $checked; ?> name="aPhoto[<?=$photo->id;?>][mod]"></td>
    <td><input type="text" class="input-mini" value="<?=$photo->order?>" name="aPhoto[<?=$photo->id; ?>][order]" size="3" /></td>
    <td><?=$photo->user->user_firstname?> <?=$photo->user->user_lastname?></td>
    <td><textarea name="aPhoto[{$photo->id}][comment]" rows="2" cols="30"><?=$photo->comment?></textarea></td>
    <td align="center">
            <input type="hidden" value="<?=$photo->filename?>">
            <img id="photo" class="thumb img-polaroid" src="/images/item/photo/thumb/<?=$photo->filename?>" />
    </td>
    <td>
        <a href="/images/item/photo/original/<?=$photo->filename?>" target="_blank" >Оригинал</a>
        <a href="/admin/item/photo_delete/id/<?=$photo->id?>" class="delete" />Удалить</a>
    </td>
</tr>
    <?php
            
        }
        }

        ?>
    </table>
    <div class="form-actions">
        <p><b>Выбрано снимков: <span id="checked-count">0</span> (максимум 3)</b></p>
        <input type="submit" value="Сохранить изменения" class="btn btn-primary" />
    </div>
</form>

<script type="text/javascript" language="javascript">
    $('img').click(function()
    {
        if ($(this).hasClass('thumb'))
            $(this).removeClass().addClass('actual').attr('src', '/images/item/photo/' + $(this).prev().val());
        else if ($(this).hasClass('actual'))
            $(this).removeClass().addClass('thumb').attr('src', '/images/item/photo/thumb/' + $(this).prev().val());

    }).css('cursor','pointer');

    function selected()
    {
        var aCheckboxes = $('.moderated');
        var aChecked = $('.moderated:checked');
        if (aChecked.length >= 3)
        {
            aCheckboxes.each(function()
            {
                if (!this.checked)
                    this.disabled = true;
            });
        }
        else
        {
            aCheckboxes.each(function()
            {
                this.disabled = false;
            });
        }
        aCheckboxes.each(function()
        {
            if (this.checked)
                $($(this).parents()[1]).addClass('selected');
            else
                $($(this).parents()[1]).removeClass('selected');
        });
        $('#checked-count').html(aChecked.length);
    }
    $('.moderated').click(selected);
    selected();

    function onSaveSelected()
    {
        var aChecked = $('.moderated:checked');
        if (aChecked.length <= 3)
            return true;
        else
            return false;
    } 
</script>