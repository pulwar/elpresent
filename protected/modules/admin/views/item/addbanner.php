<div class="b-admin-wrapper">
	<? if ( count($banner->getErrors()) ) : ?>
		<div class="b-admin-errors">
	    <?= CHtml::errorSummary($banner); ?>
	    </div>
	<? endif; ?>
    <br/>
            <div class="b-cloud">
                <table><tr>
                        <td>
                <? if($banner->item->preview_img) :?>
                    <img src="/<?= $banner->item->preview_img; ?>" alt="" />
                <? endif;?>
                        </td><td>
                <a href="<?= path('catalog/item',['perm_link' => $banner->item->perm_link]);?>" ><?= $banner->title; ?></a>
                        </td></tr></table>
                <p>
                    <?= $banner->text; ?>
                </p>
            </div><br/><br/>
            <div style="clear:both;"></div>
	<form action="" method="post" class="b-admin-form b-item">
                <p>
			<label>
				<span>Title</span>
                                <input type="text" name="Banner[title]" value="<?= addslashes($banner->title); ?>" />
			</label>
		</p>
                <p>
                    <label>
                        <span>Text</span>
                        <textarea name="Banner[text]"><?= htmlspecialchars($banner->text); ?></textarea>

                    </label>

                </p>
		<p>
			<br />
                        <button onclick="updateCloud(this);return false;">Preview</button><br/><br/>
                        <button type="submit"><? if ( $banner->isNewRecord ) : ?>Add <? else : ?>Save<? endif; ?></button>
                        <button type="button" onclick="location.href='<?= $fromPage; ?>'">Cancel</button>
		</p>
	</form>
</div>
<script type="text/javascript">
    function updateCloud(b){
       $('.b-cloud a').html($(b.form).find('input').val());
       $('.b-cloud p').html($(b.form).find('textarea').val());
       return false;
    }
</script>