<!DOCTYPE>
<html>
<head>
    <meta charset="utf-8" />
    <style>
        body {
            padding: 0;
            margin: 0;
            background: #efefef;
            font: normal 1em/1.4 Arial;
            font-size: 13px;
        }

        .hide {
            display: none;
        }

        .page {
            height: 774px;
            width: 1096px;
            background: #fff;
            margin: 20px auto;
            border:solid 1px #ccc;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.3);
            padding:10px 50px;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;

        }

        ol, ul {
            list-style-type: none;
            counter-reset: list1;
            margin:0;
            padding:0;
        }
        ol > li {
            position:relative;
            padding-left: 25px;
            margin: 10px 0;
        }
        ol > li:before {
            content: counter(list1);
            counter-increment: list1;
            display: block;
            position: absolute;
            left:0;
            top: 0;
            background: #f00;
            border-radius: 100%;
            width: 20px;
            height: 20px;
            line-height: 20px;
            text-align: center;
            color: #fff;
        }

        ol ol {
            counter-reset: list2;
        }
        ul > li {
            padding-left: 15px;
            position:relative;
            margin: 5px 0;
        }
        ul > li:before {
            content: "";
            display: block;
            width: 7px;
            height: 7px;
            position: absolute;
            left:0;
            top: 5px;
            border-radius: 100%;
            background: #ccc;
        }

        h1, h2, h3, h4 {
            font-size: 18px;
            margin:10px 0 10px;
        }
        h1:first-child, h2:first-child, h3:first-child, h4:first-child {
            margin-top:0;
        }
        h2 {
            font-size: 16px;
        }

        p {
            margin: 0;
        }
        p + p {
            margin-top: 5px;
        }

        .row {
            height: 50%;
            border: dashed 1px #ccc;
            border-top: none;
            overflow: hidden;
        }

        .row:first-child {
            border-top: dashed 1px #ccc;
        }

        .row:after {
            clear: both;
            content: '';
            display: table;
            height: 0;
        }

        .col {
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            width: 25%;
            float:left;
            border-right: dashed 1px #ccc;
            height: 100%;
            padding: 5px 10px;
        }

        .col.spanned {
            width: 50%;
        }

        .col.spanned > .split {
            -webkit-column-width: 50%;
            -moz-column-width: 50%;
            column-width: 50%;
            -webkit-column-count: 2;
            -moz-column-count: 2;
            column-count: 2;
            -webkit-column-gap: 20px;
            -moz-column-gap: 20px;
            column-gap: 20px;
            -webkit-column-rule: 1px dashed #ccc;
            -moz-column-rule: 1px dashed #ccc;
            column-rule: 1px dashed #ccc;
            height: 100%;
        }

        .preview {
            width: 100px;
            margin-bottom: -120px;
            position: relative;
            margin-top: 30px;
        }

        .preview:after {
            display: block;
            content: '';
            background: url('/images/gift-main-big.png') 0 0;
            position: absolute;
            top: -37px;
            bottom: -35px;
            left: -10px;
            right: -10px;
            z-index: 100;
            background-size: contain;
            background-repeat: no-repeat;

        }

        .preview img {
            width: 100%;
        }

        .preview + div:before {
            content: "";
            display: block;
            width: 120px;
            height: 120px;
            float:left;
        }

        @media print {
            body {
                background: #fff;
            }

            .page {
                background: #fff;
                margin:0;
                box-shadow: none;
                border: none;
            }
        }
    </style>
    <script src="//yandex.st/jquery/1.8.2/jquery.min.js"></script>
    <script src="/js/admin/showdown.js"></script>
    <script src="/js/admin/instruction.js"></script>
</head>

<body>
<div class="page hide">
    <div class="row">
        <div class="col">
            <strong class="heading red">Важная информация:</strong>
            <div data-shared-bind="info1" data-shared-bind-processor="markdown"></div>
        </div>
        <div class="col shifted">
            <div data-shared-bind="info2" data-shared-bind-processor="markdown"></div>
        </div>
        <div class="col">
            <div data-shared-bind="consult" data-shared-bind-processor="markdown"></div>
            <p>Skype: <span class="link">elpresent</span></p>
        </div>
        <div class="col">
            LOGO AND PRODUCT IMAGES
        </div>
    </div>
    <div class="row">
        <div class="col">
            <strong class="heading red">Важная информация:</strong>
            <div data-shared-bind="info1"></div>
        </div>
        <div class="col shifted">
            <div data-shared-bind="info2"></div>
        </div>
        <div class="col">
            <div data-shared-bind="consult"></div>
            <p>Skype: <span class="link">elpresent</span></p>
        </div>
        <div class="col">
            LOGO AND PRODUCT IMAGES
        </div>
    </div>
</div>
<div class="page" data-shared="it" data-shared-type="view">
    <div class="row">
        <div class="col">
            <h1 data-shared-bind="item-title"></h1>
            <div class="preview">
                <img src="" data-shared-bind="preview-img" data-shared-bind-processor="attr" data-shared-attr="src" />
            </div>
            <div data-shared-bind="about" data-shared-bind-processor="markdown"></div>
        </div>
        <div class="col">
            <h2>Что нужно знать?</h2>
            <div data-shared-bind="need-to-know" data-shared-bind-processor="markdown"></div>
            <h2>Сценарий</h2>
            <div data-shared-bind="action-script" data-shared-bind-processor="markdown"></div>
        </div>
        <div class="col spanned">
            <div class="split">
                <h2>Активация подарка</h2>
                <p>Обладатель подарочной пластиковой карты может активировать подарок 2-мя способами:</p>
                <ol>
                    <li>
                        <p>Активация подарка на сайте</p>
                        <ul>
                            <li>Зайдите на <strong>www.elpresent.by</strong></li>
                            <li>введите пин-код и заполните форму активации</li>
                            <li>Свяжитесь с организатором вашего впечатления по выданным контактным данным</li>
                            <li>Согласуйте возможную дату, время и место получения активированного подарка</li>
                            <li>Свяжитесь с организатором вашего впечатления по выданным контактным данным</li>
                            <li>Согласуйте возможную дату, время и место получения активированного подарка</li>
                        </ul>
                    </li>
                    <li>самостоятельная связь с организатором подарка</li>
                </ol>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <h1 data-shared-bind="item-title"></h1>
            <img src="" data-shared-bind="preview-img" data-shared-bind-processor="attr" data-shared-attr="src" class="preview" />
            <div data-shared-bind="about" data-shared-bind-processor="markdown"></div>
        </div>
        <div class="col">
            <h2>Что нужно знать?</h2>
            <div data-shared-bind="need-to-know" data-shared-bind-processor="markdown"></div>
            <h2>Сценарий</h2>
            <div data-shared-bind="action-script" data-shared-bind-processor="markdown"></div>
        </div>
        <div class="col spanned">
            <div class="split">
                <h2>Активация подарка</h2>
                <p>Обладатель подарочной пластиковой карты может активировать подарок 2-мя способами:</p>
                <ol>
                    <li>
                        <p>Активация подарка на сайте</p>
                        <ul>
                            <li>Зайдите на <strong>www.elpresent.by</strong></li>
                            <li>введите пин-код и заполните форму активации</li>
                            <li>Свяжитесь с организатором вашего впечатления по выданным контактным данным</li>
                            <li>Согласуйте возможную дату, время и место получения активированного подарка</li>
                            <li>Свяжитесь с организатором вашего впечатления по выданным контактным данным</li>
                            <li>Согласуйте возможную дату, время и место получения активированного подарка</li>
                        </ul>
                    </li>
                    <li>самостоятельная связь с организатором подарка</li>
                </ol>
            </div>
        </div>
    </div>
</div>
</body>
</html>