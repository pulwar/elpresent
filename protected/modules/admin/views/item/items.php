<p>Введите заголовок впечатления которые будут включать в себя эта группа. (для поиска нужно не менее 3-х символов)</p>
<div class="items-list row">
    <?php foreach($model->childs as $item) { $iniqId = uniqid()?>
        <div class="span6 item">
            <div class="item-wrap" data-prices='<?=json_encode($childsData[$item->id]['prices'])?>' data-cities='<?=json_encode($childsData[$item->id]['cities'])?>'>
                <div class="span2">
                    <img src="/<?php echo $item->preview_img; ?>" alt="" />
                </div>
                <div class="span3">
                    <strong><?php echo $item->title; ?></strong>
                    <div class="city-wrap">
                        <?
                        $selfItemLink = $model->getSelfLinkByChildren($item);
                        $cities = $childsData[$item->id]['cities'];
                        $cityId = $selfItemLink->city_id;
                        ?>
                        <select class="city-select" name="Items[<?=$iniqId?>][city]" <?=count($cities) == 1 ? 'disabled' : ''?>>
                            <? foreach ($cities as $city) {?>
                                <option <?=$city['id'] == $cityId ? 'selected="selected"' : ''?> value="<?=$city['id']?>">
                                    <?=$city['title']?>
                                </option>
                            <?} ?>
                        </select>
                        <? if (count($cities) == 1) {?>
                            <input type="hidden" name="Items[<?=$iniqId?>][city]" value="<?=$cityId?>">
                        <?}?>
                    </div>
                    <div class="price-wrap">
                        <?
                        $prices = $childsData[$item->id]['prices'];
                        $prices = (array) $prices[$cityId];
                        $optionId = $selfItemLink->option_id;
                        ?>
                        <select name="Items[<?=$iniqId?>][option]" <?=count($prices) == 1 ? 'disabled' : ''?>>
                            <? foreach ($prices as $option => $price) { ?>
                                <option <?=$option == $optionId ? 'selected="selected"' : ''?> value="<?=$option?>">
                                    <?=$price?>
                                </option>
                            <?}?>
                        </select>
                        <? if (count($prices) == 1) {?>
                            <input type="hidden" name="Items[<?=$iniqId?>][option]" value="<?=$optionId?>">
                        <?}?>
                    </div>
                    <input data-uniq="<?=$iniqId?>" type="hidden" name="Items[<?=$iniqId?>][id]" value="<?php echo $item->id; ?>" />
                </div>
                <a href="#" class="replace">Убрать</a>
            </div>
        </div>
    <?php } ?>
    <?php for($i = count($model->childs);$i<8;$i++) { ?>
        <div class="span6 item">
            <input name="Items[hint][]" type="text" placeholder="Введите заголовок" />
        </div>
    <?php } ?>

</div>