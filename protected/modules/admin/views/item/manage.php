<?php if (count($oItem->getErrors())) : ?>
    <div class="wall ">
        <?php echo  CHtml::errorSummary($oItem); ?>
    </div>
<?php endif; ?>
<form action="" method="post" enctype="multipart/form-data" class="form" data-shared="it" data-shared-type="source">
    <input type="hidden" id="Item_image_full" name="Item[image][full_img]" />
    <input type="hidden" id="Item_image_big" name="Item[image][big_img]" />
    <input type="hidden" id="Item_image_preview" name="Item[image][preview_img]" />

<?php
$isNewrecord = $oItem->isNewRecord;

$selectedCategories = array_map(function ($category) {
    return $category->id;
}, $oItem->categories);
$selectedCategories = array_map(function ($selected) {
    return ['selected' => 'selected'];
}, array_combine($selectedCategories, $selectedCategories));

foreach ($selectedCategories as $cid => $options) {
    if (isset($subcategoriesOptions[$cid])) {
        $subcategoriesOptions[$cid]['selected'] = 'selected';
    }
}
?>
<div class="row">
    <div class="span12 position_admin_img _top">
        <div id="image_full_crop" style="display:none;">
            <button id="image_crop_full_result" type="button">Подтвердить</button>
        </div>
    </div>
    <div class="span12 position_admin_img _top">
        <div id="image_preview_crop" style="display:none;">
            <button id="image_crop_preview_result" type="button">Подтвердить</button>
        </div>
    </div>
    <div class="span12 position_admin_img">
        <div id="image_big_crop" style="display:none;">
            <button id="image_crop_big_result" type="button">Подтвердить</button>
        </div>
    </div>
    <div class="span12">
        <div class="control-group">
            <label><span>Большое изображение (макс 1 Мб)</span></label>
            <div class="controls">
                <?php if ($oItem->big_img) : ?>
                    <img class="img-polaroid" data-shared-source="preview-img" data-shared-attr="src" src="/<?php echo  $oItem->big_img; ?>"/>
                    <br/>
                <?php endif; ?>
                <input type="file" id="Item_big_img" name="Item[big_img]" data-max-size="1000000"/>
            </div>
        </div>
    </div>
    <div class="span4">
        <div class="control-group">
            <label>
                <span>Изображение (макс 1 Мб)</span>
            </label>
            <div class="controls"><?php if ($oItem->full_img) : ?>
                    <img class="img-polaroid" src="/<?php echo  $oItem->full_img; ?>"/>
                    <br/>
                <?php endif; ?>
                <input type="file" id="Item_full_img" name="Item[full_img]" data-max-size="1000000"/>
            </div>
        </div>
    </div>
    <div class="span4">
        <div class="control-group">
            <label><span>Превью (макс 1 Мб)</span></label>
            <div class="controls">
                <?php if ($oItem->preview_img) : ?>
                    <img class="img-polaroid" data-shared-source="preview-img" data-shared-attr="src" src="/<?php echo  $oItem->preview_img; ?>"/>
                    <br/>
                <?php endif; ?>
                <input type="file" id="Item_preview_img" name="Item[preview_img]" data-max-size="1000000"/>
            </div>
        </div>
    </div>
    <div class="span4">
        <div class="control-group">
            <label class="checkbox inline">
                <input type="checkbox" id="verified" name="verified" <?=$oItem->verified ? 'checked="checked"' : ''; ?>/>
                Проверено
            </label>
            <label class="inline">
                Дата обновления:
                <span><b><?=$isNewrecord ? '0000-00-00' : date( 'Y-m-d',strtotime($oItem->updated_at))?></b></span>
            </label>
        </div>
        <div class="control-group">
            <div style="float: left; margin-right: 10px;">
                <label>
                    <span>Вид срока действия </span>
                </label>
                <div class="controls">
                    <select name="kind_validity" id="kind_validity">
                        <option value="0" <?php if ($oItem->kind_validity == 0): ?>selected="selected" <?php endif; ?>>С даты продажи</option>
                        <option value="1" <?php if ($oItem->kind_validity == 1): ?>selected="selected" <?php endif; ?>>С даты начала сезона</option>

                    </select>
                </div>
            </div>
            <div>
                <label>Длительность (месяцев) срока действия</label>
                <input type="number" step="1" min="1" max="12" id="Item[months_of_validity]" name="Item[months_of_validity]" class="" value="<?php echo  htmlspecialchars($oItem->months_of_validity); ?>"/>
            </div>
        </div>
        <div class="control-group start_season_div" style="display:none;">
            <label>
                Дата начала сезона
                <div class="input-append date" id="div-datepicker">
                    <input type="text" name="start_season_date" class="" data-format="yyyy-MM-dd" value="<?= $oItem->start_season_date ? $oItem->start_season_date : ''?>"/>
                    <span class="add-on">
                        <i data-time-icon="icon-time" data-date-icon="icon-calendar" class="icon-calendar"></i>
                    </span>
                </div>
            </label>
        </div>
        <div class="control-group">
            <label>Название</label>
            <div class="controls"><input data-shared-source="item-title" type="text" id="Item[title]" name="Item[title]" class="input-xxlarge" value="<?php echo  htmlspecialchars($oItem->title); ?>"/></div>
        </div>
        <div class="control-group">
            <label class="checkbox inline">
                <input type="checkbox" id="is_landing" name="is_landing" <?=$oItem->is_landing ? 'checked="checked"' : ''; ?>/>
                В landing-page
            </label>
            <label class="checkbox inline">
                <input type="checkbox" id="is_kit" name="is_kit" <?=$oItem->is_kit ? 'checked="checked"' : ''; ?>/>
                Вывод в комплект
            </label>
            <label class="checkbox inline">
                <input type="checkbox" id="is_catalog" name="is_catalog" <?php echo  $oItem->is_catalog ? 'checked="checked"' : ''; ?>/>
                В каталоге
            </label>
            <label class="checkbox inline">
                <input type="checkbox" id="is_new" name="is_new" <?php echo  $oItem->is_new ? 'checked="checked"' : ''; ?>/>
                Новый
            </label>

			<label class="checkbox inline">
				<input type="checkbox" id="is_season" name="is_season" <?php echo  $oItem->is_season ? 'checked="checked"' : ''; ?>/>
				Сезонный
			</label>

            <label class="checkbox inline">
                <input type="checkbox" id="is_hot" name="is_hot" <?php echo  $oItem->is_hot ? 'checked="checked"' : ''; ?>/>
                Хит Продаж
            </label>
            <label class="checkbox inline">
                <input type="checkbox" id="is_special" name="is_special" <?php echo  $oItem->is_special ? 'checked="checked"' : ''; ?> />
                Специальный
            </label>
            <label class="checkbox inline">
                <input type="checkbox" id="is_main" name="is_main" <?php echo  $oItem->is_main ? 'checked="checked"' : ''; ?> />
                На главной
            </label>
            <label class="checkbox inline">
                <input type="checkbox"  id="is_weekend" name="is_weekend" <?php echo  $oItem->is_weekend ? 'checked="checked"' : ''; ?> />
                Weekend
            </label>
            <label class="checkbox inline">
                <input type="checkbox" id="is_recommend" name="is_recommend" <?php echo  $oItem->is_recommend ? 'checked="checked"' : ''; ?> />
                Рекомендуем (в личном кабинете)
            </label>
            <label class="checkbox inline">
                <input type="checkbox" id="is_vkpost" name="is_vkpost" <?php echo  $oItem->is_vkpost ? 'checked="checked"' : ''; ?> />
                Постить в VK ?
            </label>
            <label class="checkbox inline">
                <input type="checkbox" id="yml" name="yml" <?=$oItem->yml ? 'checked="checked"' : ''; ?> />
                Yml?
            </label>
        </div>
        <div class="control-group">
            <label>
                <span>Наличие</span>
            </label>
            <div class="controls"><select id="available" name="available">
                    <option value="1" <?php if ($oItem->available == 1): ?>selected="selected" <?php endif; ?>>Есть</option>
                    <option value="0" <?php if ($oItem->available == 0): ?>selected="selected" <?php endif; ?>>Нет</option>
                    <option value="3" <?php if ($oItem->available == 3): ?>selected="selected" <?php endif; ?>>Под заказ</option>
                </select>
            </div>
        </div>
        <div class="control-group">
            <label>
                <span>Порядок</span>
            </label>
            <div class="controls"><input type="text" id="Item_order" name="Item[order]" value="<?php echo  $oItem->order; ?>"/></div>
        </div>
    </div>
</div>

<div class="tabbable"> <!-- Only required for left/right tabs -->
    <ul class="nav nav-tabs">
        <li<?= $isNewrecord ? ' class="active"':''; ?>><a href="#categories_tab" data-toggle="tab">Категории</a></li>
        <li><a href="#features_tab" data-toggle="tab">Особенности</a></li>
        <? //if($oItem->isNewRecord): ?>
        <li><a href="#prices_tab" data-toggle="tab">Стоимость</a></li>
        <? //endif;?>
        <? if ($oItem->type == 'group') { ?><li><a href="#group_tab" data-toggle="tab">Комплект</a></li><?php } ?>
        <li<?= !$isNewrecord ? ' class="active"':''; ?>><a href="#description_tab" data-toggle="tab">Описание</a></li>
        <? if ($oItem->type != 'group') { ?><li><a href="#instruction_tab" data-toggle="tab">Инструкция</a></li><?php } ?>
        <li><a href="#seo_tab" data-toggle="tab">SEO</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane <?= $isNewrecord ? 'active':''; ?>" id="categories_tab">
            <div class="row">
                <div class="span6">
                    <div class="control-group">
                        <label>
                            <strong>Категория</strong>
                        </label>
                        <div class="controls">
                            <?= CHtml::dropDownList('Category[]', null, $oCategories, [
                                'multiple' => true,
                                'size' => 20,
                                'class' => 'input-xxlarge',
                                'options' => array_map(function () {
                                    return ['selected' => 'selected'];
                                }, $selectedCategories)
                            ]); ?>
                        </div>
                    </div>

                </div>
                <div class="span6">
                    <div class="control-group">
                        <label><strong>Подкатегории</strong></label>
                        <div class="controls">
                            <?= CHtml::dropDownList('Subcategory[]', null, $oSubcategories, [
                                'multiple' => true,
                                'size' => 20,
                                'class' => 'input-xxlarge',
                                'options' => $subcategoriesOptions
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="tab-pane" id="features_tab">
            <div class="row">
                <?php foreach ($features as $feature) : ?>

                    <div class="span3">
                        <strong><?php echo  $feature->feature_value; ?></strong><br/>
                        <?php foreach ($feature->feature_values as $feature_value) : ?>
                            <label>
                                <?php

                                $isChecked = '';
                                if (isset($aItemFeatures[$feature->feature_id]) && in_array($feature_value->feature_value_id, $aItemFeatures[$feature->feature_id])) {
                                    $isChecked = ' checked="checked"';
                                }

                                ?>
                                <input type="checkbox" id="features[<?php echo  $feature->feature_id; ?>][]" name="features[<?php echo  $feature->feature_id; ?>][]"
                                       value="<?php echo  $feature_value->feature_value_id; ?>" <?php echo  $isChecked; ?> /> <?php echo  $feature_value->feature_value_value; ?>
                                <br/>
                            </label>
                        <?php endforeach; ?>
                    </div>
                <?php endforeach; ?>
                <div class="span3">
                    <div class="control-group">
                        <label>
                            <strong>Тип События</strong>
                        </label>
                        <?= CHtml::activeDropDownList($oItem, 'event', CMap::mergeArray(['' => 'Выбирете наименование события'], CHtml::listData(Event::model()->findAll(), 'id', 'title'))); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="prices_tab">
        <? if(empty($is_option)):?>
            <div class="row">
                <div class="span3" id="city_select_container"> 
                    <? if(empty($oItem->isNewRecord)):?>
                    	 <p><a href="<?= Yii::app()->createUrl('/admin/option/create', array('id' => $oItem->id)) ?>" target="_blank" class="btn btn-success">Создать опции</a></p>
                    <? endif;?>
                    <label><strong>Цены и регионы</strong></label>
                    <select id="city_select" style="display:none;">
                        <option value="">Выбирите город</option>
                        <?php foreach ($cities as $c) { ?>
                            <option value="<?= $c->id; ?>"><?= $c->title; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <?php if ($oItem->type != 'group') { ?>
                <div class="span3" id="partner_select_container">
                    <label>&nbsp;</label>
                    <select id="partner_select" style="display:none;">
                        <option value="">Выберите партнера</option>
                        <?php foreach ($oPartners as $partner) { ?>
                            <option value="<?= $partner->id; ?>" data-city-id="<?= $partner->city_id; ?>"><?= $partner->name; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <?php } ?>
                <div class="span1" style="padding-top: 25px"><span class="btn" data-add-partner><i class="icon-plus"></i></span></div>
            </div>
            <div class="row form form-horizontal" style="padding-top: 20px;">
                <?php if ($oItem->type != 'group') { ?>
                <div class="span4" data-partner-prices>
                    <strong>Цены партнеров</strong>
                    <?php foreach ($oItem->partners as $partnerLink) { ?>
                        <div class="control-group" data-partner-price="<?= $partnerLink->partner_id; ?>">
                            <label class="control-label"><?= $partnerLink->partner->name; ?></label>
                            <div class="controls">
                    <span class="input-append">
                        <input type="text" class="input-small" name="Item[partner_prices][<?= $partnerLink->partner_id; ?>][price]" required value="<?php echo  $partnerLink->partner_price; ?>"/>
                        <span class="add-on">Руб</span>
                        <span class="add-on btn" title="убрать" data-remove><i class="icon-minus"></i></span>
                    </span>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <?php } ?>
                <div class="span4" data-city-prices>
                    <strong>Цена в регионе</strong>
                    <?php foreach ($oItem->prices as $cityLink) { ?>
                        <div class="control-group" data-city-price="<?= $cityLink->city_id; ?>">
                            <label class="control-label"><?= $cityLink->city->title; ?></label>
                            <div class="controls">
                    <span class="input-append">
                        <input type="text" class="input-small" name="Item[city_prices][<?= $cityLink->city_id; ?>][price]" required value="<?php echo  $cityLink->item_price; ?>"/>
                        <span class="add-on">Руб</span>
                        <span class="add-on" title="В наличии"><input value="" <?= $cityLink->available ? 'checked=""' : ''; ?> type="checkbox" name="Item[city_prices][<?= $cityLink->city_id; ?>][available]" /></span>
                    </span>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <hr />
            <div class="form-horizontal">
                <div class="control-group">
                    <label class="control-label">
                        <span>Партнер</span>
                    </label>
                    <div class="controls">
                        <select disabled="disabled" id="Item[partner_id]" name="Item[partner_id]">
                            <?php foreach ($oPartners as $oPartner) : ?>
                                <option value="<?php echo  $oPartner->id; ?>" <?php echo  ($oPartner->id == $oItem->partner_id) ? 'selected="selected"' : ''; ?>><?php echo  $oPartner->name; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">
                        <span>Цена</span>
                    </label>
                    <div class="controls">
                        <span class="input-append">
                            <input type="text" disabled="disabled" class="input-small" id="Item[price]" name="Item[price]" value="<?php echo  $oItem->price; ?>"/>
                            <span class="add-on">Руб</span>
                        </span>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">
                        <span>Цена партнера</span>
                    </label>
                    <div class="controls">
                        <span class="input-append">
                            <input type="text" disabled="disabled" class="input-small" id="Item[partner_price]" name="Item[partner_price]" value="<?php echo  $oItem->partner_price; ?>"/>
                            <span class="add-on">Руб</span>
                        </span>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">
                        <span>Скидка</span>
                    </label>
                    <div class="controls">
                    <span class="input-append">
                        <input type="text" class="input-small" id="Item[off_price]" name="Item[off_price]" value="<?php echo  $oItem->off_price; ?>"/>
                        <span class="add-on">%</span>
                    </span>
                    </div>
                </div>
            </div>
            <? else:?>
            <div class="control-group">
                <label class="control-label">
                    <span>Скидка</span>
                </label>
                <div class="controls">
                    <span class="input-append">
                        <input type="text" class="input-small" id="Item[off_price]" name="Item[off_price]" value="<?php echo  $oItem->off_price; ?>"/>
                        <span class="add-on">%</span>
                    </span>
                </div>
            </div>
            Подарок содержит опции. <a href="<?=Yii::app()->createUrl('/admin/option/update', array('id'=>$oItem->id))?>" target="_blank">Перейти для редактирования</a>
			<? endif;?>
        </div>
        <? if ($oItem->type == 'group') { ?>
            <?php
            $cs = Yii::app()->getClientScript();
            $cs->registerScriptFile('/js/admin/super-impressions.js');
            ?>
            <div class="tab-pane" id="group_tab">
                <?php $this->renderPartial('items', ['model' => $oItem, 'childsData'=>$childsData]); ?>
            </div>
        <? } ?>
        <div class="tab-pane <?= !$isNewrecord ? 'active':''; ?>" id="description_tab">
            <div class="control-group">
                <label>
                    <strong>Краткое описание</strong></label>
                <div class="controls"><textarea id="Item_desc" name="Item[desc]" class="input-xxlarge" rows="5"><?php echo  $oItem->desc; ?></textarea></div>

            </div>
            <div class="control-group">
                <label><strong>Полное описание</strong></label>
                <?php

                // ElFinder widget
                $this->widget('ext.tinymce.TinyMce', [
                    'model' => $oItem,
                    'attribute' => 'text',
                    // Optional config
                    'fileManager' => [
                        'class' => 'ext.elFinder.TinyMceElFinder',
                        'connectorRoute'=>'admin/elfinder/connector',
                    ],
                ]);?>
            </div>
        </div>
        <div class="tab-pane" id="instruction_tab">
<!--            <a href="/admin/item/instruction_preview" target="_blank" class="pull-right btn btn-big btn-primary" data-insturction-preview>Предпросмотр</a>-->
            <a href="<?=path('admin/item/cert_preview', ['id' => $oItem->id]); ?>" target="_blank" class="pull-right btn btn-big btn-primary">Предпросмотр</a>
            <div class="control-group">
                <label><strong>Описания подарка</strong> (только для инструкций)</label>
                <div class="controls"><textarea data-shared-source="about" id="Item_instuction_description" name="Item[instuction_description]" class="input-xxlarge" rows="5"><?php echo  $oItem->instuction_description; ?></textarea></div>
            </div>
            <div class="control-group">
                <label><strong>Сценарий</strong></label>
                <div class="controls"><textarea data-shared-source="action-script" id="Item_scenario" name="Item[scenario]" class="input-xxlarge" rows="5"><?php echo  $oItem->scenario; ?></textarea></div>
            </div>
            <div class="control-group">
                <label><strong>Что нужно знать</strong></label>
                <?php

                // ElFinder widget
                $this->widget('ext.tinymce.TinyMce', [
                    'model' => $oItem,
                    'attribute' => 'need_to_know',
                    // Optional config
                    'fileManager' => [
                        'class' => 'ext.elFinder.TinyMceElFinder',
                        'connectorRoute'=>'admin/elfinder/connector',
                    ],
                ]);?>
                
            </div>
        </div>
        <div class="tab-pane" id="seo_tab">
            <div class="control-group">
                <label><strong>Ключевые слова</strong> (через запятую).</label>
                <div class="controls">
                    <textarea id="Item_keywords"  class="input-xxlarge" rows="5" name="Item[keywords]"><?php echo  $oItem->keywords; ?></textarea>
                    <div class="help-block">При формировании данного тега необходимо использовать только те слова,
                        которые содержатся в самом документе. Использование тех слов, которых нет на странице,
                        не рекомендуется. Рекомендованное количество слов в данном теге — не более десяти.</div>
                </div>
            </div>

            <div class="control-group">
                <label>
                    <strong>meta-описание</strong>
                </label>
                <div class="controls">
                    <textarea id="Item_description" class="input-xxlarge" rows="5" name="Item[description]"><?php echo  $oItem->description; ?></textarea>
                    <div class="help-block">используется при создании краткого описания страницы,
                        используется поисковыми системами для индексации,
                        а также при создании аннотации в выдаче по запросу</div>
                </div>
            </div>

            <div class="control-group">
                <label><strong>Заголовок страницы</strong></label>
                <div class="controls">
                    <input class="input-xxlarge" type="text" id="Item_seo_title" name="Item[seo_title]" value="<?php echo  $oItem->seo_title; ?>"/>
                </div>
            </div>
        </div>
    </div>
</div>

<div class=form-actions>
    <button class="btn btn-primary"><?php if ($oItem->isNewRecord) : ?>Добавить <?php else : ?>Сохранить<?php endif; ?></button>
    <button class="btn" type="button" onclick="location.href='<?php echo  $fromPage; ?>'">Отмена</button>
</div>
</form>
<!-- javascripts -->
<script>
    (function ($) {

        $('form').submit(function(){
            var isOk = true;
            $('input[type=file][data-max-size]').each(function(){
                if(typeof this.files[0] !== 'undefined'){
                    var maxSize = parseInt($(this).attr('data-max-size'),10),
                    size = this.files[0].size;
                    isOk = maxSize > size;
                    console.log(size);
                    console.log(maxSize);
                    if(isOk == false ) {
                        alert('Певышен максимальный размер файла!')
                    }
                    return isOk;
                }
            });
            return isOk;
        });

        $(function () {
            $('[data-instruction-preview]').on('click', function () {
                // start data sharing
            })
        })

        $(function () {
            var select = $('select[name="Subcategory[]"]');
            var copy = select.clone().attr('name', 'copy').hide();
            select.after(copy);
            $('select[name="Category[]"]').on('change', function(){
                var selected = [];
                $('option:selected', this).each(function(){
                    selected.push(this.value);
                });
                select.empty();
                $('option', copy).each(function(){
                    if($.inArray($(this).data('owner')+'', selected) >=0 ) {
                        select.append($(this).clone());
                    }
                });
            }).trigger('change');

            var $cities = $('#city_select').on('change', function () {
                    showPartnersSelect(this.value);
                }),
                $citiesOptions = $('option', $cities).clone(false),
                $partners = $('#partner_select'),
                $partnersOptions = $('option', $partners).clone(false),
                $partnerPrices = $('[data-partner-prices]').on('click', '[data-remove]', onRemoveItem),
                $citiesPrices = $('[data-city-prices]'),
                addedCities = collectIds('city-price', $citiesPrices),
                addedPartners = collectIds('partner-price', $partnerPrices),
                $cityPriceTpl = $(
                    '<div class="control-group">' +
                        '<label class="control-label"></label>' +
                        '<div class="controls">' +
                        '<span class="input-append">' +
                        '<input type="text" required class="input-small"/>' +
                        '<span class="add-on">Руб</span>' +
                        '<span class="add-on" title="В наличии"><input type="checkbox" /></span>' +
                        '</span>' +
                        '</div>' +
                        '</div>'
                ),
                $partnerPriceTpl = $(
                    '<div class="control-group">' +
                        '<label class="control-label"></label>' +
                        '<div class="controls">' +
                        '<span class="input-append">' +
                        '<input type="text" required class="input-small"/>' +
                        '<span class="add-on">Руб</span>' +
                        '<span class="add-on btn" title="убрать" data-remove><i class="icon-minus"></i></span>' +
                        '</span>' +
                        '</div>' +
                        '</div>'
                ),
                $addBtn = $('[data-add-partner]').on('click', function () {
                    // add partners and cities
                    var tpl;
                    if (-1 == addedCities.indexOf($cities[0].value)) {
                        addedCities.push($cities[0].value);
                        addItem($citiesPrices, 'city-price', $cityPriceTpl, $cities);
                    }

                    if ($partners.length && -1 == addedPartners.indexOf($partners[0].value)) {
                        addedPartners.push($partners[0].value);
                        addItem($partnerPrices, 'partner-price', $partnerPriceTpl, $partners);
                        $cities.trigger('change');
                    }
                });

            showCitiesSelect();
            showPartnersSelect();

            function onRemoveItem() {
                var $container = $(this).closest('.control-group'),
                    partnerId = '' + $container.data('partner-price');

                addedPartners = addedPartners.filter(function (el) {
                    return el !== partnerId;
                });
                $container.remove();
                $cities.trigger('change');
            }

            function showCitiesSelect() {
                showSelect($citiesOptions, $cities, function () {
                    return  true;
                });
            }

            function showPartnersSelect(cityId) {
                $addBtn[cityId ? 'show' : 'hide']();
                var filter = function () {
                    return  -1 === addedPartners.indexOf($(this).attr('value')) && $(this).data('city-id') == cityId;
                };
                showSelect($partnersOptions, $partners, cityId ? filter : undefined);
            }

            function showSelect($options, $select, filter) {
                $select.empty().hide();
                if (typeof filter !== 'function') {
                    return $([]);
                }

                return $select.append($options.clone().filter(filter)).show();
            }

            function collectIds (attr, $parent) {
                var ids = [];
                $('[data-'+attr+']', $parent).each(function () {
                    var data = '' + $(this).data(attr);
                    if (-1 === ids.indexOf(data))
                        ids.push(data);
                });

                return ids;
            }

            function addItem($container, name, $tpl, $select) {
                var tpl = $tpl.clone(false);
                tpl.data(name, $select[0].value);
                tpl.find('label').text($select.find('option[value='+$select[0].value+']', $select).text());
                tpl.find('input[type=text]').attr('name', 'Item['+name.replace(/\-/g, '_')+'s]['+$select[0].value+'][price]');
                tpl.find('input[type=checkbox]').attr('name', 'Item['+name.replace(/\-/g, '_')+'s]['+$select[0].value+'][available]');
                $container.append(tpl);
            }
        })
    }(window.jQuery))
    function uploadImage() {
        var $uploadCropFull;
        var $uploadCropPreview;
        var $uploadCropBig;

        function readFile(input, crop) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    crop.croppie('bind', {
                        url: e.target.result
                    });

                };

                reader.readAsDataURL(input.files[0]);
            }
            else {
                swal("Sorry - you're browser doesn't support the FileReader API");
            }
        }

        $uploadCropFull = $('#image_full_crop').croppie({
            enableExif: true,
            viewport: {
                width: 468,
                height: 368,
                type: 'square'
            },
            boundary: {
                width: 600,
                height: 600
            }
        });

        $uploadCropPreview = $('#image_preview_crop').croppie({
            enableExif: true,
            viewport: {
                width: 96,
                height: 96,
                type: 'square'
            },
            boundary: {
                width: 600,
                height: 600
            }
        });

        $uploadCropBig = $('#image_big_crop').croppie({
            enableExif: true,
            viewport: {
                width: 1196,
                height: 496,
                type: 'square'
            },
            boundary: {
                width: 1300,
                height: 800
            }
        });

        $('#Item_full_img').on('change', function () {
            readFile(this, $uploadCropFull);
            $('#image_full_crop').show();
        });
        $('#image_crop_full_result').on('click', function (ev) {
            $uploadCropFull.croppie('result', {quality: 1, format: 'jpeg'}).then(function (result) {
                $('#image_full_crop').hide();
                $('#Item_full_img').prop("disabled", true).val(null);
                $('#Item_image_full').val(result);
            });
        });

        $('#Item_preview_img').on('change', function () {
            readFile(this, $uploadCropPreview);
            $('#image_preview_crop').show();
        });
        $('#image_crop_preview_result').on('click', function (ev) {
            $uploadCropPreview.croppie('result', {quality: 1, format: 'jpeg'}).then(function (result) {
                $('#image_preview_crop').hide();
                $('#Item_preview_img').prop("disabled", true).val(null);
                $('#Item_image_preview').val(result);
            });
        });

        $('#Item_big_img').on('change', function () {
            readFile(this, $uploadCropBig);
            $('#image_big_crop').show();
        });
        $('#image_crop_big_result').on('click', function (ev) {
            $uploadCropBig.croppie('result', {quality: 1, format: 'jpeg'}).then(function (result) {
                $('#image_big_crop').hide();
                $('#Item_big_img').prop("disabled", true).val(null);
                $('#Item_image_big').val(result);
            });
        });
    }
    uploadImage();
</script>