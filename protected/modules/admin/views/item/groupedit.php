
<div class="b-admin-wrapper">
    
    <table cellpadding="0" cellspacing="0" class="group-item">
            <tr>
                <th>Название товара</th><th>Цена</th><th>Категория</th><th>Подкатегория</th><th></th>
            </tr>
	<? foreach ($oItems as $oItem) :?>
            <?
            $subs = array();
            foreach ($oItem->subcategory as $sub){
                $subs[] = $sub->id;
            }
            ?>
        
            <tr>
                <form action="" method="post" class="b-admin-form b-item">
                <td style="width:30%;" id="<?= $oItem->id ?>">
                    <input type="hidden" name="id" value="<?=$oItem->id?>" />
                    <?= $oItem->title ?>
                    <p class="message"></p>
                </td>
                <td>
                    Цена:<br/>
                    <input type="text" name="price" value="<?= $oItem->price ?>" /><br/>
                    Цена партнера:<br/>
                    <input type="text" name="partner_price" value="<?= $oItem->partner_price ?>" /><br/>
                    Скидка:<br/>
                    <input type="text" name="off_price" value="<?= $oItem->off_price ?>" style="width: 100px;" /> <span style="float:right;line-height: 27px;">%</span>
                </td>
                <td>
                    <select name="category_id">
                        <? foreach ($oCategories as $v) : ?>
                        <option <?= $oItem->category_id == $v->id ? 'selected' : ''; ?> value="<?= $v->id ?>"><?= $v->title ?></option>
                        <? endforeach; ?>
                    </select>
                </td>
                <td>
                    <? foreach ( $oSubcategories as $oCategory ) : ?>
                        <br /><label>
                            <input type="checkbox" name="Subcategory[]" value="<?= $oCategory->id; ?>" <?= in_array($oCategory->id,$subs) ? 'checked' : ''; ?> />
                        <?= $oCategory->title ?> ( <?= $oCategory->category->title ?> )
                        </label>
                    <? endforeach; ?>
                </td>
                <td>
                    <input type="button" value="Применить" />
                </td>

                </form>
            </tr>

        
        <? endforeach; ?>
    
        




        </table>
    
</div>
<script type="text/javascript">
    $('input[type=button]').each(function(){
        $(this).bind('click',function(){
            var form = this.form;
            var id = form.id.value;
            $.ajax({
                url: '/admin/item/groupedit.html',
                type: "POST",
                dataType: 'json',
                data: $(form).serialize(),
                success: function(data){
                    if(data.message){
                        $('td[id='+id+']'+' p.message').html('<span style="color:green;">'+data.message+'</span>');
                    }else{
                        $('td[id='+id+']'+' p.message').html('<span style="color:red;">'+data.error+'</span>');
                    }
                    
                }
            });









        });
    })
</script>