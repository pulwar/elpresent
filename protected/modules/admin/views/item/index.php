<div class="well filter <?=($filter) ? 'active' : 'closed'?>">
    <h3 id="label">Фильтр <?=($filter) ? '<sup id="red">(активен)</sup>' : ''?></h3>
    <form method="GET">
        <div class="row">

            <div class="span3">
                <table class="table">
                <tr>
                    <?
                    foreach ($is_options as $key => $value)
                    {
                        ?>
                        <tr>
                            <td>
                                <?=$value ?>
                            </td>
                            <td colspan="2"><input type="checkbox" name="filter[<?=$key ?>]" <?=($filter && array_key_exists($key, $filter) && $filter[$key] == true) ? 'checked' : '' ?> /></td>
                        </tr>
                        <? } ?>
                </table>
            </div>
            <div class="span7">
            <table class="table">
                <tr>
                    <td>Город</td>
                    <td colspan="2">
                        <?= CHtml::dropDownList('filter[city]', $filter['city'], CHtml::listData($cities, 'id', 'title'), ['empty' => 'Выберите город']); ?>
                    </td>
                </tr>
                <tr>
                    <td>Категория</td>
                    <td colspan="2">
                        <?= CHtml::dropDownList('filter[category]', $filter['category'], $categories, ['empty' => 'Выберите категорию']); ?>
                    </td>
                </tr>
                <tr>
                    <td>Цена</td>
                    <td>
                        <?= CHtml::dropDownList('filter[price_operation]', $filter['price_operation'], [
                            '=' => 'равна',
                            '<=' => 'равна и более',
                            '>=' => 'равна и менее'
                        ]); ?>
                    </td>
                    <td><input type="text" name="filter[price]" value="<?=($filter && $filter['price']) ? $filter['price'] : ''
                            ?>" /></td>
                </tr>
                <tr>
                    <td>Фраза</td>
                    <td colspan="2">
                        <input type="text" name="filter[text]" value="<?=($filter && $filter['text']) ? htmlspecialchars($filter['text']) : '' ?>" />
                    </td>
                </tr>
                <tr>
                    <td>Партнер</td>
                    <td colspan="2">
                        <?= CHtml::dropDownList('filter[partner]', $filter['partner'], $partners, ['empty' => 'Выберите партнёра']); ?>
                    </td>
                </tr>					
            </table>
            </div>
        </div>
        <div>
            <input type="submit" value="Применить" class="btn btn-primary"> | <input class="btn" type="reset" value="Очистить">
        </div>
    </form>
</div>
<? ?>
<div style="clear:both"></div>
<span class="btn-group">
    <a class="btn btn-primary btn-large dropdown-toggle" data-toggle="dropdown" href="#">
        Добавить
        <span class="caret"></span>
    </a>
    <ul class="dropdown-menu">
        <li><a href="/admin/item/manage?type=item">Впечатление</a></li>
        <li><a href="/admin/item/manage?type=group">Комплект впечатлений</a></li>
    </ul>
</span>

<?php
$this->widget('ext.bootstrap.widgets.TbGridView', array (
            'type' => array('condensed', 'striped'),
            'dataProvider' => $oItems,
            'columns' => array
                (
                'id',
                'title',
                array(
                    'type' => 'html',
                    'name' => 'verified',
                    'value' => '$data->verified ? \'<i class="icon-plus"></i>\' : \'<i class="icon-minus"></i>\''
                ),
                array(
                    'type' => 'html',
                    'name' => 'updated_at',
                    'value' => 'date( \'Y-m-d\',strtotime($data->updated_at))'
                ),
//                'updated_at',
                array(
                    'type' => 'html',
                    'name' => 'price',
                    'value' => '$data->getPrice()'
                ),
                array(
                    'type' => 'html',
                    'name' => 'off_price',
                    'value' => '$data->off_price ? round($data->off_price)."%":"Нет"'
                ),
                array
                    (
                    'type' => 'raw',
                    'name' => 'is_new',
                    'value' => '$data->is_new ? \'<i class="icon-plus"></i>\' : \'<i class="icon-minus"></i>\'',
                    'headerHtmlOptions' => array(
                        'class' => 'valigned',
                    ),
                    'htmlOptions' => array(
                        'class' => 'smaller',
                    )
                ),
                array
                    (
                    'type' => 'raw',
                    'name' => 'is_special',
                    'header' => 'Спец-ый',
                    'value' => '$data->is_special ? \'<i class="icon-plus"></i>\' : \'<i class="icon-minus"></i>\'',
                    'headerHtmlOptions' => array(
                        'class' => 'valigned',
                    ),
                    'htmlOptions' => array(
                        'class' => 'smaller',
                    )
                ),
                array
                    (
                    'type' => 'raw',
                    'name' => 'is_main',
                    'value' => '$data->is_main? \'<i class="icon-plus"></i>\' : \'<i class="icon-minus"></i>\'',
                    'headerHtmlOptions' => array(
                        'class' => 'valigned',
                    ),
                    'htmlOptions' => array(
                        'class' => 'smaller',
                    )
                ),
                array
                    (
                    'type' => 'raw',
                    'name' => 'available',
                    'value' => '$data->available ? \'<i class="icon-plus"></i>\' : \'<i class="icon-minus"></i>\'',
                    'headerHtmlOptions' => array(
                        'class' => 'valigned',
                    ),
                    'htmlOptions' => array(
                        'class' => 'smaller',
                    )
                ),
                array
                (
                    'name' => 'partner_id',
                    'value' => function ($data) {
                        return implode(', ', array_map(function (ItemPartnerLink $link) {
                            return $link->partner->name;
                        }, $data->partners));
                    },
					'htmlOptions' => array(
						'style' => 'max-width:250px',
					)
                ),
                array
                    (
                    'type' => 'html',
                    'value' => 'CHtml::link("", "/admin/item/manage/id/".$data->id, array("class"=>"icon-pencil", "title"=>"редактировать"))."&nbsp;".CHtml::link("", "/admin/item/delete/id/".$data->id, array("class" => "icon-remove delete", "title"=>"удалить"))."<br />".CHtml::link("", "/admin/comments/index/item_id/".$data->id, array("class"=>"icon-comment", "title"=>"комментарии"))."&nbsp;".CHtml::link("", "/admin/item/photos/item_id/".$data->id, array("class"=>"icon-camera", "title"=>"фотографии"))."&nbsp;"'.
                    '.CHtml::link("", "/admin/item/addbanner/item_id/".$data->id, array("class"=>"icon-bookmark", "title"=>"Добавить в баннер"))'
                )
            )
        ));
?>