<h1>Управление тизерами</h1>
<form action="/admin/teasers/save" method="post" enctype="multipart/form-data">
<div class="row">
    <?php foreach($teasers as $teaser) { ?>
    <div class="teaser span4 form-horizontal">
        <img alt="" style="margin:0 auto 20px;display:block;border:solid 2px #ccc;" src="/<?= $teaser->getImageUri(); ?>" />
        <div class="control-group">
            <label class="control-label" for="teaser_<?= $teaser['id']; ?>_img">Изображение</label>
            <div class="controls">
                <input type="file" name="teaser_<?= $teaser['id']; ?>" id="teaser_<?= $teaser['id']; ?>_img" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="teaser_<?= $teaser['id']; ?>_description">Описание</label>
            <div class="controls">
                <input type="text" name="teaser[<?= $teaser['id']; ?>][description]" id="teaser_<?= $teaser['id']; ?>_description"  value="<?= $teaser['description']; ?>"/>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="teaser_<?= $teaser['id']; ?>_link">Ссылка</label>
            <div class="controls">
                <input type="text" name="teaser[<?= $teaser['id']; ?>][link]" id="teaser_<?= $teaser['id']; ?>_link" value="<?= $teaser['link']; ?>" />
            </div>
        </div>
    </div>
    <?php } ?>
</div>
    <div class="form-actions">
        <input type="submit" class="btn btn-primary btn-large" value="Сохранить" />
    </div>
</form>