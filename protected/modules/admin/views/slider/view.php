<h1>Баннер #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'id',
        'title',
        'img' => array(
            'name' => 'img',
            'type' => 'image',
            'value'=> '/images/slider/' . $model->img,
            'headerHtmlOptions'=>array('width'=>'200px'),
        ),
        'show' => array(
            'name' => 'show',
            'value' => $model->show == 1 ? 'Да' : 'Нет',
        ),
        'link',
        'order',
    ),
)); ?>

<?php echo CHtml::link('К списку',array('/admin/slider')); ?>
