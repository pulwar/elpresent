<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'page-form',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    )); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

    <div class="row">
        <?php echo $form->labelEx($model, 'title'); ?>
        <?php echo $form->textField($model, 'title'); ?>
        <?php echo $form->error($model, 'title'); ?>
    </div>

    <div class="row">
        <? if (empty($model->img)): ?>
            <?php echo $form->labelEx($model, 'img'); ?>
            <?php echo $form->fileField($model, 'img[]', array('accept' => 'image')); ?>
            <?php echo $form->error($model, 'img'); ?>
        <? else: ?>
            <img src="/images/slider/<?=$model->img?>" alt="удалить" style="display: block;max-width: 90%">
            <a href="<?=Yii::app()->createUrl('/admin/slider/delimg', array('id'=>$model->id))?>">Удалить</a>
        <? endif; ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'desc'); ?>
        <?php
        // ElFinder widget
        $this->widget('ext.tinymce.TinyMce', array(
            'model' => $model,
            'attribute' => 'desc',
            // Optional config
            'fileManager' => array(
                'class' => 'ext.elFinder.TinyMceElFinder',
                'connectorRoute'=>'admin/elfinder/connector',
            ),
        ));?>
        <?php echo $form->error($model, 'desc'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'link'); ?>
        <?php echo $form->textField($model, 'link'); ?>
        <?php echo $form->error($model, 'link'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'show'); ?>
        <?php echo $form->dropDownList($model, 'show', array(0 => 'Нет', 1 => 'Да')); ?>
        <?php echo $form->error($model, 'show'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'order'); ?>
        <?php echo $form->numberField($model, 'order', array('min' => 0)); ?>
        <?php echo $form->error($model, 'order'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model, 'type_id'); ?>
        <?php echo $form->dropDownList($model, 'type_id', array(1 => 'Баннеры', 2 => 'Внутренний')); ?>
        <?php echo $form->error($model, 'type_id'); ?>
    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->