<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#page-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Менеджер баннеров</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'page-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        'id' => array(
            'name' => 'id',
            'filter' => false,
        ),
        'title',
        'img' => array(
            'name' => 'img',
            'type' => 'image',
            'filter' => false,
            'value'=> '"/images/slider/".$data->img',
            'headerHtmlOptions'=>array('width'=>'200px'),
        ),
        'show' => array(
            'name' => 'show',
            'value' => '($data->show == 1 ? "Да" : "Нет")',
            'filter' => false,
        ),
        /*'link' => array(
            'name' => 'link',
            'filter' => false,
        ),*/
        'order' => array(
            'name' => 'order',
            'filter' => false,
        ),
        'type_id' => array(
            'name' => 'type_id',
            'value' => '($data->type_id == 1 ? "Баннеры" : "Внутренний")',
            'filter' => array(1 => "Баннеры", 2 => "Внутренний"),
        ),
        array(
            'class' => 'CButtonColumn',
        ),
    ),
)); ?>

<?php echo CHtml::link('Создать',array('/admin/slider/create'), ['class' => 'btn btn-primary']); ?>
