<?php
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#page-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<h1>Менеджер подписчиков из заказов</h1>

<?php $form = $this->beginWidget('CActiveForm', array(
    'id' => 'page-form',
    'enableAjaxValidation' => false,
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'page-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        [
            'id'=>'autoId',
            'class'=>'CCheckBoxColumn',
            'selectableRows' => '100',
        ],
        'id' => array(
            'name' => 'id',
            'filter' => false,
        ),
        'name' => [
            'name' => 'name',
            'filter' => false,
        ],
        'lname' => [
            'name' => 'lname',
            'filter' => false,
        ],
        'phone' => [
            'name' => 'phone',
            'filter' => false,
        ],
        'adr' => [
            'name' => 'adr',
            'filter' => false,
        ],
        'email' => [
            'name' => 'email',
            'filter' => false,
        ],
        /*'recivier_email' => [
            'name' => 'recivier_email',
            'filter' => false,
        ],*/
        [
            'class' => 'CButtonColumn',
            'template'=>'',
        ],
    ),
)); ?>

<div class="buttons">
    <?=CHtml::submitButton('Выгрузить', ['class' => 'btn btn-primary']); ?>
	<?=CHtml::submitButton('Выгрузить все', ['name'=> 'exportAll','class' => 'btn btn-primary']); ?>
</div>
<?php $this->endWidget(); ?>
