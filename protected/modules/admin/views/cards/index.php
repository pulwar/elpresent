<a href="/admin/cards/manage">Добавить</a>


<? if ( Yii::app()->user->hasFlash('card_message_successfully') ) : ?>
<div class="b-nessage-successfully">
	<?= Yii::app()->user->getFlash('card_message_successfully'); ?>
</div>
<? endif; ?>


<?php
$this->widget('ext.bootstrap.widgets.TbGridView', array (
        'type' => array('condensed', 'striped'),
        'dataProvider' => $oCards,
        'columns'      => array
        (
            'card_id',
            'card_title',
            'card_price',
            array
            (
                'name'  => 'card_image',
                'type'  => 'html',
                'value' => 'CHtml::image("/upload/cards/".$data->card_image."_48.png")'
            ),
            array
            (
                'type'  => 'html',
                'value' => 'CHtml::link("[редактировать]", "/admin/cards/manage/id/".$data->card_id)."<br />".CHtml::link("[удалить]", "/admin/cards/delete/id/".$data->card_id, array("class" => "delete"))'
            )
        )
    ));
?>

<script>
$(document).ready(function()
{
	$('a.delete').click(function()
	{
		if ( confirm('Вы действительно удалить открытку?') )
		{
			return true;
		}

		return false;
	});	
});
</script>