<? if ( count($oCard->getErrors()) ) : ?>
	<?= CHtml::errorSummary($oCard); ?>
<? endif; ?>

<form action="" class="b-admin-form" method="post" enctype="multipart/form-data">
	<p>
		<label>
			<span>Название</span>
			<input type="text" name="Card[card_title]" value="<?= $oCard->card_title; ?>" />
		</label>
	</p>
	<p>
		<label>
			<span>Цена</span>
			<input type="text" name="Card[card_price]" value="<?= $oCard->card_price; ?>" />
		</label>
	</p>
	<p>
		<label>
			<? if ( $oCard->card_image ) : ?>
				<img src="/upload/cards/<?= $oCard->card_image; ?>_48.png" />
			<? endif; ?>
			<span>Изображение</span>
			<input type="file" name="Card[card_image]" />
		</label>
	</p>
	<p>
		<button>Сохранить</button>
	</p>
</form>