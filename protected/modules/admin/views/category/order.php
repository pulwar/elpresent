<script>
    $(document).ready(function() {
        $('.cats.order ul.sortable-list').sortable({
            connectWith: 'ul.sortable-list',
            stop: function(event, ui) {
                var items = $(this).sortable('toArray');
                $.ajax({
                    type: "POST",
                    url: "/admin/category/order",
                    data: ({
                        sortitems: items
                    }),
                    success: function(msg) {
                        //alert(msg);
                    }
                });
            }
        });
    });
</script>
<div class="cats order">
    <h3>Сортировка категорий первого уровня</h3>
    <div class="alert">
        <button type="button" class="close" data-dismiss="alert">?</button>
        <strong>Предупреждение!</strong> Чтобы изменить порядок, перетяните категорию на нужное место!
    </div>
    <ul class="sortable-list">
        <? foreach($cats as $cat):?>
            <li class="sortable-item" id="<?=$cat->id?>" title="перетянуть" style="cursor: move">
                 <div class="title"><?=$cat->title?></div>
                 <div class="id"><span>id:</span><?=$cat->id?></div>
                 <div class="slug"><span>slug:</span> <?=$cat->slug?></div>
            </li>
        <? endforeach;?>
    </ul>
</div>