<p><br /><a class="btn" href="/admin/superitem/edit">Добавить супервпечатление</a></p>
<?php
$this->widget('ext.bootstrap.widgets.TbGridView', array (
    'type' => array('condensed', 'striped'),
	'dataProvider' => $items,
	'columns' => array
	(
		'id',
		'title',
		'price',
		array (
			'type' => 'html',
			'value' => 'CHtml::link("[Редактировать]", "/admin/superitem/edit/id/".$data->id) . " " . CHtml::link("[Удалить]", "/admin/superitem/delete/id/".$data->id, array("class"=>"delete"))',
		)
	)
));