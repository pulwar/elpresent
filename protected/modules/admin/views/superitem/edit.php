<?php
	$baseUrl = Yii::app()->baseUrl; 
	$cs = Yii::app()->getClientScript();
	$cs->registerScriptFile($baseUrl.'/js/admin/super-impressions.js?v=1.0.0');
?>
<div class="b-admin-wrapper">
	<form action="" method="post" enctype="multipart/form-data" class="b-admin-form b-item">
<?php
$selectedCategories = array_map(function ($category) {
    return $category->id;
}, $model->categories);
$selectedCategories = array_map(function ($selected) {
    return ['selected' => 'selected'];
}, array_combine($selectedCategories, $selectedCategories));

foreach ($selectedCategories as $cid => $options) {
    if (isset($subcategoriesOptions[$cid])) {
        $subcategoriesOptions[$cid]['selected'] = 'selected';
    }
}
?>
        <ul class="errors">
            <?php foreach($model->getErrors() as $field => $error) { ?>
            <li><?php

                ?></li>
            <?php } ?>
        </ul>

        <div class="row">
    <div class="span3">
        <div class="control-group">
            <label>
                <strong>Категория</strong>
                <?= CHtml::dropDownList('Category[]', null, $categories, [
                    'multiple' => true,
                    'size' => 10,
                    'options' => array_map(function () {
                        return ['selected' => 'selected'];
                    }, $selectedCategories)
                ]); ?>
            </label>
        </div>

    </div>
    <div class="span3">
        <div class="control-group">

            <strong>Подкатегория</strong>
            <?= CHtml::dropDownList('Subcategory[]', null, $subcategories, [
                'multiple' => true,
                'size' => 10,
                'options' => $subcategoriesOptions
            ]); ?>
            <script>
                $(function(){
                    var select = $('select[name="Subcategory[]"]');
                    var copy = select.clone().attr('name', 'copy').hide();
                    select.after(copy);
                    $('select[name="Category[]"]').on('change', function(){
                        var selected = [];
                        $('option:selected', this).each(function(){
                            selected.push(this.value);
                        });
                        select.empty();
                        $('option', copy).each(function(){
                            if($.inArray($(this).data('owner')+'', selected) >=0 ) {
                                select.append($(this).clone());
                            }
                        });
                    }).trigger('change');
                });
            </script>
        </div>
    </div>

    <?php foreach ($features as $feature) : ?>

    <div class="span3">
        <strong><?php echo  $feature->feature_value; ?></strong><br/>
        <?php foreach ($feature->feature_values as $feature_value) : ?>
        <label>
            <?php

            $isChecked = '';
            if (isset($aItemFeatures[$feature->feature_id]) && in_array($feature_value->feature_value_id, $aItemFeatures[$feature->feature_id])) {
                $isChecked = ' checked="checked"';
            }

            ?>
            <input type="checkbox" id="features[<?php echo  $feature->feature_id; ?>][]" name="features[<?php echo  $feature->feature_id; ?>][]"
                   value="<?php echo  $feature_value->feature_value_id; ?>" <?php echo  $isChecked; ?> /> <?php echo  $feature_value->feature_value_value; ?>
            <br/>
        </label>
        <?php endforeach; ?>
    </div>
    <?php endforeach; ?>


</div>    
            
        <p class="row">
                <input type="text" name="Superimpression[title]" value="<?php echo $model->title; ?>" placeholder="Заголовок" />
		</p>
		
		<?php include dirname(__FILE__) . '/items.php'; ?>
		
		<div class="control-group">
			<label for="Superimpression_desc">Описание</label>
            <?php
            // ElFinder widget
            $this->widget('ext.tinymce.TinyMce', array(
                'model' => $model,
                'attribute' => 'desc',
                // Optional config
                'fileManager' => array(
                    'class' => 'ext.elFinder.TinyMceElFinder',
                    'connectorRoute'=>'admin/elfinder/connector',
                ),
            ));?>
		</div>

                <div class="control-group">
    <span>Текст</span>
    <?php

    // ElFinder widget
    $this->widget('ext.tinymce.TinyMce', array(
        'model' => $model,
        'attribute' => 'text',
        // Optional config
        'fileManager' => array(
            'class' => 'ext.elFinder.TinyMceElFinder',
            'connectorRoute'=>'admin/elfinder/connector',
        ),
    ));?>
</div>
                
                
		<p class="control-group">
			<label>Спрятать <input type="checkbox" value="1" name="Superimpression[draft]" <?php echo $model->draft ? 'checked="checked"':''; ?>" /></label>
		</p>
		<p class="control-group">
			<label>Стоимость</label><div class="controls"> <input type="text" pattern="[0-9\.]+" name="Superimpression[price]" required="required" value="<?php echo $model->price; ?>" /></div>
		</p>

        <p class="row">
			<label for="" style="display:block;">Превью для каталога</label>
			<?php if(!$isNew){ ?><p class="img_preview"><img src="/images/superimpressions/<?php echo $model->preview_img; ?>" /></p><?php } ?>
			<input type="file" name="Superimpression[preview_img]" />
		</p>
		<p class="row">
			<label for="" style="display:block;">Превью</label>
			<?php if(!$isNew){ ?><p class="img_preview"><img src="/images/superimpressions/<?php echo $model->full_img; ?>" /></p><?php } ?>
			<input type="file" name="Superimpression[full_img]" />
		</p>

                
                <div class="control-group">
    <label>Ключевые слова (через запятую).</label>
    <div class="controls">
        <textarea id="Item_keywords"  class="input-xxlarge" rows="5" name="Superimpression[keywords]"><?php echo  $model->keywords; ?></textarea>
        <div class="help-block">При формировании данного тега необходимо использовать только те слова,
            которые содержатся в самом документе. Использование тех слов, которых нет на странице,
            не рекомендуется. Рекомендованное количество слов в данном теге — не более десяти.</div>
    </div>

</div>

<div class="control-group">
    <label>
            Description
    </label>
    <div class="controls">
    <textarea id="Item_description" class="input-xxlarge" rows="5" name="Superimpression[description]"><?php echo  $model->description; ?></textarea>
    <div class="help-block">используется при создании краткого описания страницы,
        используется поисковыми системами для индексации,
        а также при создании аннотации в выдаче по запросу</div>
    </div>
</div>

<div class="control-group">
    <label>Title</label>
    <div class="controls">
        <input type="text" id="Item_seo_title" name="Superimpression[seo_title]" value="<?php echo  $model->seo_title; ?>"/>
    </div>
</div>
                
                
		<p class="form-actions">
			<input type="submit" class="btn btn-primary" value="Сохранить" />
		</p>
	</form>
</div>