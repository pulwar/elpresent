<h3>Галлерея "<?=$gallery->name; ?>"</h3>
<div class="form">
    <?php
    $this->widget('GalleryManager', array(
        'gallery' => $gallery,
        'controllerRoute' => '/admin/gallery', //route to gallery controller
    ));
    ?>
</div>