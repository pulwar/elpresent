<h3>Галлереи</h3>

<div class="well">
    <span class="btn btn-big btn-primary" data-toggle="modal" data-target="#add-form">Добавить галлерею</span>
</div>

<div class="modal fade" id="add-form">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
                <form class="form form-horizontal" name="addForm">
                    <div class="control-group">
                        <label for="gallery_title">Название</label>
                        <input type="text" class="controls input-large" id="gallery_title" required="" name="name" />
                    </div>
                    <div class="control-group">
                        <label for="gallery_title">Описание</label>
                        <textarea class="controls input-large" id="gallery_title" required="" name="description"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                <button type="button" class="btn btn-primary save">Добавить</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
    $(function () {
        $('#add-form .save').on('click', function () {
            var $this = $(this);
            if (document.forms[0] && document.forms[0].checkValidity()) {
                $this.prop('disabled', true);
                $.ajax({
                    type: 'POST',
                    url: '/admin/gallery/add',
                    data: $(document.forms[0]).serialize(),
                    success: function () {
                        $this.prop('disabled', false);
                        $('#add-form').modal('hide');
                        document.forms[0].reset();
                        $.fn.yiiGridView.update("yw0");
                    }
                });
            }
        })
    })
</script>

<?php
$this->widget('ext.bootstrap.widgets.TbGridView', [
    'type' => ['condensed', 'striped'],
    'dataProvider' => $provider,
    'columns' => [
        ['name'=>'id', 'header' => 'ID'],
        ['name' => 'name', 'header' => 'Название'],
        [
            'class'=>'bootstrap.widgets.TbButtonColumn',
            'template' => '{update} {delete}',
            'buttons'=>array
            (
                'delete' => array
                (
                    'label'=>'Удалить альбом',
                    'icon'=>'remove',
                    'url'=>'Yii::app()->createUrl("/admin/gallery/deletealb", array("id"=>$data->id))',
                    'options'=>array(
                        'class'=>'btn btn-small',
                    ),
                ),
            ),
            'htmlOptions'=>['style'=>'width: 50px'],
        ]
    ]
]);

