<br />
<h4><? if ( $oFeature->isNewRecord ) : ?>Добавление<? else : ?>Редактирование<? endif; ?> характеристики</h4>
<br />

<? $this->widget('ShowErrors', array('oModel' => $oFeature)); ?>

<form action="" method="post">
	<p>
		<label>
    		Название характеристики:&nbsp;
    		<input type="text" name="Feature[feature_value]" value="<?= $oFeature->feature_value; ?>" size="70" />
		</label>
	</p>
	<br />
	<p>
		<button><? if ( $oFeature->isNewRecord ) : ?>Добавить<? else : ?>Сохранить<? endif; ?></button>
		<button type="button" onclick="location.href='/admin/features.html'">Отмена</button>
	</p>
</form>