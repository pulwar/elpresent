<? if ( Yii::app()->user->hasFlash('feature_message_success') ) : ?>
<div class="message-success"><?= Yii::app()->user->getFlash('feature_message_success'); ?></div>
<? endif; ?>

<a href="/admin/features/manage_feature.html">Добавить характеристику</a>&nbsp;&nbsp;
<a href="/admin/features/manage_feature_value.html">Добавить значение характеристики</a>

<br />
<br />
<div class="b-features">
<? if ( count($oFeatures) ) : ?>
	<ul>
    <? foreach ( $oFeatures as $oFeature ) : ?>
		<li>
			<span>
    			<a href="<?= '/admin/features/manage_feature/id/'.$oFeature->feature_id; ?>" class="edit"></a>
    			<!--  <a href="" class="delete"></a> -->
			</span>
			<?= $oFeature->feature_value; ?>
			
			<? if ( count($oFeature->feature_values) ) : ?>
				<ul>
			    <? foreach ( $oFeature->feature_values as $oFeatureValue ) : ?>
					<li>
						<span>
    						<a href="<?= '/admin/features/manage_feature_value/id/'.$oFeatureValue->feature_value_id; ?>" class="edit"></a>
    						<!-- <a href="" class="delete"></a> -->
						</span>
					    <?= $oFeatureValue->feature_value_value; ?>
					</li>
				<? endforeach; ?>
				</ul>
			<? endif; ?>
		</li>
	<? endforeach; ?>
	</ul>
<? else : ?>
Характеристики отсуствуют.
<? endif; ?>
</div>