<br />
<h4><? if ( $oFeatureValue->isNewRecord ) : ?>Добавление<? else : ?>Редактирование<? endif; ?> характеристики</h4>
<br />

<? $this->widget('ShowErrors', array('oModel' => $oFeatureValue)); ?>

<form action="" method="post">
	<p>
		<label>
    		Характеристика:&nbsp;
    		<select name="FeatureValue[feature_id]">
    			<? foreach ( $oFeatures as $oFeature ) : ?>
    				<? if ( $oFeature->feature_id == $oFeatureValue->feature_id ) : ?>
    				<option value="<?= $oFeature->feature_id; ?>" selected="selected"><?= $oFeature->feature_value; ?></option>
    				<? else : ?>
    				<option value="<?= $oFeature->feature_id; ?>"><?= $oFeature->feature_value; ?></option>
    				<? endif; ?>
    			<? endforeach; ?>
    		</select>
		</label>
	</p>
	<br />
	<p>
		<label>
    		Значение характеристики:&nbsp;
    		<input type="text" name="FeatureValue[feature_value_value]" value="<?= $oFeatureValue->feature_value_value; ?>" size="70" />
		</label>
	</p>
	<br />
	<p>
		<button><? if ( $oFeatureValue->isNewRecord ) : ?>Добавить<? else : ?>Сохранить<? endif; ?></button>
		<button type="button" onclick="location.href='/admin/features.html'">Отмена</button>
	</p>
</form>