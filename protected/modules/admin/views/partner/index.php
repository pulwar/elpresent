<script type="text/javascript">
    $(document)
        .on('keyup', '#searchStr', function () {
            $('#enumSearch').val('empty');
            var searchStr = $(this).val();
            searchRequest(searchStr);
        })
        .on('change', '#enumSearch', function () {
            $('#searchStr').val('');
            var searchStr = $(this).val();
            searchStr = searchStr == 'empty' ? '' : $(this).val();
            searchRequest(searchStr);
        });

    var searchRequest = function (searchStr) {
        $.ajax({
            type: 'POST',
            data: {'searchStr': searchStr},
            cache: false,
            url: '/admin/partner/filter',
            success: function (jsonData) {
                var data = jQuery.parseJSON(jsonData);
                $('#partnersGrid').html(data.text);
            }
        });
    }
</script>

<div id="user-table">
<br />
<a class="btn" href="/admin/partner/add">Добавить</a>
    <input type="text" id="searchStr" name="searchStr">
    <select id="enumSearch">
        <option value="empty">Выбрать</option>
        <? foreach ($partnersList as $partner) {
            echo "<option value='".$partner['name']."'>".$partner['name'].' ('.$partner['COUNT(id)'].")</option>";
        }
        ?>
    </select>
    <div id="partnersGrid">
        <?=$this->renderPartial('/partner/_grid', ['oPartner' => $oPartner], true, false);?>
    </div>
</div>