<div class="b-admin-errors">
    <?= CHtml::errorSummary($oPartner); ?>
</div>
<div class="b-admin-wrapper">
    <form action="" method="post" class="b-admin-form b-item">
        <table class="user-manage-table" cellspacing="0" cellpadding="0" border="0">
            <tr><td>Компания</td><td><input type="text" name="Partner[name]" value='<?=$oPartner->name ?>' /></td></tr>            
            <tr><td>Город</td><td>
                    <select id="Partner[city_id]" name="Partner[city_id]">
                        <option value="0">Город не указан</option>
                        <?php foreach ($oCitys as $oCity) : ?>
                            <option value="<?php echo  $oCity->id; ?>" <?php echo  ($oCity->id == $oPartner->city_id) ? 'selected="selected"' : ''; ?>><?php echo  $oCity->name; ?></option>
                        <?php endforeach; ?>
                    </select></td>
            </tr>             
            <tr><td>Адрес</td><td><input type="text" name="Partner[adress]" value="<?=$oPartner->adress ?>" /></td></tr>
            <tr><td>Время работы</td><td><input type="text" name="Partner[work_time]" value="<?=$oPartner->work_time ?>" /></td></tr>
            <tr><td>Контактное лицо</td><td><input type="text" name="Partner[contact_person]" value="<?=$oPartner->contact_person ?>" /></td></tr>
            <tr><td>Телефон</td><td><input type="text" name="Partner[phone]" value="<?=$oPartner->phone ?>" /></td></tr>
            <tr><td>Email</td><td><input type="text" name="Partner[email]" value="<?=$oPartner->email ?>" /></td></tr>
            <tr><td>Условия сотрудничества</td><td><input type="text" name="Partner[agreement_terms]" value="<?=$oPartner->agreement_terms ?>" /></td>
            <tr><td>Процент комииссии</td><td><input type="text" name="Partner[commission]" value="<?=$oPartner->commission ?>" /></td>
            </tr>
            <tr><td>Meсто проведения</td><td><input type="text" name="Partner[place]" value="<?=$oPartner->place ?>" /></td></tr>
            <tr><td>Latitude</td><td><input type="text" name="Partner[lat]" value="<?=$oPartner->lat ?>" /></td></tr>
            <tr><td>Longitude</td><td><input type="text" name="Partner[lon]" value="<?=$oPartner->lon ?>" /></td></tr>
            <tr><td>Пароль для проверки кодов</td><td><input type="text" name="Partner[password]" value="" /></td></tr>
        </table>
            <input type="submit" value="Сохранить" />
            <input type="reset" onclick="location.href='<?= $fromPage; ?>'" value="Отмена" />
    </form>
</div>