<?php
$this->widget('ext.bootstrap.widgets.TbGridView', array (
    'type' => array('condensed', 'striped'),
    'dataProvider' => $oPartner,
    'columns'      => array
    (
        'id',
        'name',
        array (
            'name' => 'city_id',
            'value' => '$data->city->title'
        ),
        'adress',
        'work_time',
        'contact_person',
        'phone',
        'agreement_terms',
        'commission',
        'reg_date',
        'email',

        array
        (
            'type'  => 'html',
            'value' => 'CHtml::link("[править]", "/admin/partner/edit/id/".$data->id."/page/".(Yii::app()->request->getParam("User_page")))."<br />".CHtml::link("[удалить]", "/admin/partner/delete/id/".$data->id,array("class" => "delete"))'
        )
    )
));