<h1>Карта <i>'<?= $model->id; ?>'</i></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'id',
        'title',
        'caption',
        'coordinates',
        'address',
        'time_work',
        'city',
        'show' => array(
            'name' => 'show',
            'value' => $model->show == 1 ? 'Да' : 'Нет',
        ),
        'order',
    ),
)); ?>
<br>
<?php echo CHtml::link('К списку', array('/admin/wherebuy'), ['class' => 'btn btn-primary']); ?>
