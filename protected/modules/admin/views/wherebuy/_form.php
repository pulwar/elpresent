<div class="form">

    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'page-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    )); ?>

    <p class="note">Поля c <span class="required">*</span> обязательны для заполнения.</p>

    <?php echo $form->errorSummary($model); ?>

    <div>
        <?php echo $form->labelEx($model, 'title'); ?>
        <?php echo $form->textField($model, 'title'); ?>
        <?php echo $form->error($model, 'title'); ?>
    </div>

    <div>
        <?php echo $form->labelEx($model, 'caption'); ?>
        <?php echo $form->textField($model, 'caption'); ?>
        <?php echo $form->error($model, 'caption'); ?>
    </div>

    <div>
        <?php echo $form->labelEx($model, 'coordinates'); ?>
        <?php echo $form->textField($model, 'coordinates'); ?>
        <?php echo $form->error($model, 'coordinates'); ?>
    </div>

    <div>
        <?php echo $form->labelEx($model, 'city'); ?>
        <?php echo $form->dropDownList($model,'city',[
            'minsk' => 'Минск', 'brest' => 'Брест', 'gomel' => 'Гомель', 'grodno'=>'Гродно', 'mogilev' => 'Могилев', 'vitebsk' => 'Витебск'
        ]); ?>
        <?php echo $form->error($model, 'city'); ?>

    </div>

    <div>
        <?php echo $form->labelEx($model, 'address'); ?>
        <?php echo $form->textField($model, 'address'); ?>
        <?php echo $form->error($model, 'address'); ?>
    </div>

    <div>
        <?php echo $form->labelEx($model, 'time_work'); ?>
        <?php echo $form->textField($model, 'time_work'); ?>
        <?php echo $form->error($model, 'time_work'); ?>
    </div>

    <div>
        <?php echo $form->labelEx($model, 'show'); ?>
        <?php echo $form->dropDownList($model, 'show', array(0 => 'Нет', 1 => 'Да')); ?>
        <?php echo $form->error($model, 'show'); ?>
    </div>

    <div>
        <?php echo $form->labelEx($model, 'order'); ?>
        <?php echo $form->numberField($model, 'order', array('min' => 0)); ?>
        <?php echo $form->error($model, 'order'); ?>
    </div>

    <div class="buttons">
        <br>
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', ['class' => 'btn btn-primary']); ?>
    </div>

    <?php $this->endWidget(); ?>

</div><!-- form -->