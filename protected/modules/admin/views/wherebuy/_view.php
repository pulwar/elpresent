<?php
/* @var $this PageController */
/* @var $data Page */
?>

<div class="view">

    <b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
    <?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id' => $data->id)); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
    <?php echo CHtml::encode($data->title); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('caption')); ?>:</b>
    <?php echo CHtml::encode($data->caption); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('coordinates')); ?>:</b>
    <?php echo CHtml::encode($data->coordinates); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('city')); ?>:</b>
    <?php echo CHtml::encode($data->city); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('address')); ?>:</b>
    <?php echo CHtml::encode($data->address); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('time_work')); ?>:</b>
    <?php echo CHtml::encode($data->time_work); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('order')); ?>:</b>
    <?php echo CHtml::encode($data->order); ?>
    <br/>

    <b><?php echo CHtml::encode($data->getAttributeLabel('show')); ?>:</b>
    <?php echo $data->show == 1 ? 'Да' : 'Нет'; ?>
    <br/>
</div>