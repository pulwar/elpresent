<div class="anoncer">
	<h2>Анонсер</h2>
	<div class="alert alert-info">
		Чтобы удалить объявление, нажмите кнопку `Сохранить` с пустым текстом
	</div>
	<? if (!empty($txt)): ?>
		<?= $txt; ?>
	<? else: ?>
		<i>Нет объявления.</i>
	<? endif; ?>

	<div class="landing-form-order">
		<? $form = $this->beginWidget('CActiveForm', array(
			'enableAjaxValidation' => false,
			'action' => CHtml::normalizeUrl(array("/admin/anoncer")),
			'enableClientValidation' => false,
			'clientOptions' => array(
				'validateOnChange' => true,
				'validateOnSubmit' => true,
			)
		));
		?>

		<div>
			<hr>
			<?=$form->labelEx($model, 'txt'); ?>
			<?php
			$this->widget('ext.tinymce.TinyMce', array(
				'model' => $model,
				'attribute' => 'txt',
				'fileManager' => array(
					'class' => 'ext.elFinder.TinyMceElFinder',
					'connectorRoute'=>'admin/elfinder/connector',
				),
			));?>
			<?php echo $form->error($model, 'txt'); ?>
		</div>
		<div>
			<br>
			<?=CHtml::submitButton('Сохранить'); ?>
		</div>
		<? $this->endWidget(); ?>
	</div>
</div>
