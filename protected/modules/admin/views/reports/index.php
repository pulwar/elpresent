<script type="text/javascript" src="/js/admin/reports.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
    $('select[name=reportType]').change(showHidePartners);

    function showHidePartners()
    {
        var sltVal = $.trim( $('option:selected', this).val() );
        if (sltVal == 2)
        {
            $('select[name=aFilter\\[partner\\]]').show();
            return false;
        }
        $('select[name=aFilter\\[partner\\]]').hide();
        return false;
    }
});
</script>

<div class="well"><h3>Отчет</h3>
    <form id="ReportsForm" method="POST" action="">
        <table class="filterTable">
            <tr>
                <td colspan="2" style="text-align: right;">
                    <select name="reportType">
                        <? foreach (ReportsController::$reportTypes as $number => $name): ?>
                        <option <?= ($number == $reportType) ? 'selected="selected"' : ''; ?> value="<?= $number; ?>"><?= $name; ?></option>
                        <? endforeach; ?>
                    </select>
                    <br />
                    <br />
                    
                    <select name="aFilter[partner]" style="display: <?= (2 == $reportType) ? 'block;' : 'none;'  ?>">
                        <option value="0">Все партнеры</option>
                        <? foreach (Partner::getPartnerList() as $Partner) : ?>
                            <option value=" <?= $Partner->id; ?> "> <?= $Partner->name; ?> </option>
                        <? endforeach; ?>
                    </select>

                </td>
            </tr>
            <tr>
                <td>С</td><td><input type="text" class="datepicker" name="aFilter[dateFrom]" value="<?=$dateFrom ?>" /></td>
            </tr>
            <tr>
                <td>По</td><td><input type="text" class="datepicker" name="aFilter[dateTo]" value="<?=$dateTo ?>" /></td>
            </tr>
            <tr>
                <td>
                    <input class="btn btn-primary" type="submit" value="Показать">
                </td>
                <td>
                    <input class="btn" type="reset" value="Очистить" onclick="window.location = window.location.href">
                </td>
            </tr>
        </table>
    </form>
</div>

<div style="clear:both"></div>

<? 
if (isset($aData) && count($aData) && 3==$reportType)
{
    $this->widget('application.components.SaleReport', array('aData' => $aData) );
}
if (isset($oData) && count($oData) && 2==$reportType)
{
    $this->widget('application.components.PartnerReport', array('oData' => $oData) );
}
?>

<?
if (isset($result) && is_array($result) && count($result))
{
?>
<div style="font-size: 18px; text-align: center">Отчет "<?=ReportsController::$reportTypes[$reportType]
?>" за период (<?=$dateFrom ?> - <?=$dateTo ?>)</div><br />
<?
    foreach ($result as $known => $res)
    {
?>
<table cellpadding="0" cellspacing="0" class="table table-condensed table-striped" style="margin: 20px 0px 0;border-top:2px #ddd solid;">
    <tr>
        <td><strong>Источник</strong></td>
        <td>
                    <b style="font-size:120%;"><?=$known
?></b>
        </td>
    </tr>
    <?
    foreach ($res as $order)
    {
        if ($order->item)
        {
    ?><tr><td colspan="2"><?=$order->item->title ?></td></tr>
    <?
         }
    }
    ?>

    <tr>
        <td><strong>Всего заказов</strong></td><td><b><?=Order::model()->count("order_knowwhere='" . $known . "'")
    ?></b></td>
    </tr>
</table>
<? } ?>
<? } ?>