<a href="/admin/news/manage">Добавить</a>
<br /><br />

    <?php
    $this->widget('ext.bootstrap.widgets.TbGridView', array (
        'type' => array('condensed', 'striped'),
    'dataProvider' => $oNews,
    'columns'      => array
    (
        'title',
        'date',
        array
        (
            'type'  => 'html',
            'value' => 'CHtml::link("[редактировать]", "/admin/news/manage/id/".$data->id."/page/".(Yii::app()->request->getParam("News_page")))."<br />".CHtml::link("[удалить]", "/admin/news/delete/id/".$data->id, array("class" => "delete"))'
        )
    )
));

?>

<script>
$(document).ready(function()
{
	$('a.delete').click(function()
	{
		if ( confirm('Вы действительно хотите удалить новость?') )
		{
			return true;
		}

		return false;
	});	
});
</script>