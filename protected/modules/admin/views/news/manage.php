<div class="b-admin-wrapper">
	<? if ( count($oNews->getErrors()) ) : ?>
		<div class="b-admin-errors">
	    <?= CHtml::errorSummary($oNews); ?>
	    </div>
	<? endif; ?>
	<form action="" method="post" enctype="multipart/form-data" class="b-admin-form b-item">
		<p>
			<label>
				<span>Заголовок</span>
				<input type="text" name="News[title]" value="<?= $oNews->title; ?>" />
			</label>
		</p>
		<p>
			<span>Текст</span>
            <?php
            // ElFinder widget
            $this->widget('ext.tinymce.TinyMce', array(
                'model' => $oNews,
                'attribute' => 'text',
                // Optional config
                'fileManager' => array(
                    'class' => 'ext.elFinder.TinyMceElFinder',
                    'connectorRoute'=>'admin/elfinder/connector',
                ),
            ));?>
            ?>
		</p>
		<p>
			<? if ( $oNews->preview_img ) : ?>
			<br />
			<img src="/upload/news/<?= $oNews->preview_img; ?>" />
			<br /><br />
			<? endif; ?>
			<label>
				<span>Превью</span>
				<input type="file" name="News[preview_img]" />
			</label>
		</p>
		<p>
			<? if ( $oNews->full_img ) : ?>
			<br />
			<img src="/upload/news/<?= $oNews->full_img; ?>" />
			<br /><br />
			<? endif; ?>
			<label>
				<span>Изображение</span>
				<input type="file" name="News[full_img]" />
			</label>
		</p>
		<p>
			<br />
			<button><? if ( $oNews->isNewRecord ) : ?>Добавить <? else : ?>Сохранить<? endif; ?></button>
			<button type="button" onclick="location.href='<?= $fromPage; ?>'">Отмена</button>
		</p>
	</form>
</div>