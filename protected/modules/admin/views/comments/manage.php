<? if (count($oComments->getErrors())) : ?>
<div class="well">
    <?= CHtml::errorSummary($oComments); ?>
</div>
<? endif; ?>

<form action="" method="post" enctype="multipart/form-data" class="form form-horizontal">
    <div class="control-group">
        <label class="control-label">Имя отправителя</label>
        <div class="controls">
            <input name="Comments[sender_name]" type="text" value="<?=htmlspecialchars($oComments->sender_name);?>" />
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Текст</label>
        <div class="controls">
            <textarea class="input-xxlarge" name="Comments[text]" style="height:100px;"><?=htmlspecialchars($oComments->text);?></textarea>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Рейтинг</label>
        <div class="controls">
            <input name="Comments[rating]" type="text" value="<?=$oComments->rating?>" />
        </div>
    </div>
    <div class="form-actions">
        <input class="btn btn-primary" type="submit" value="Сохранить" />
        <input class="btn" type="reset" value="Отменить" />
    </div>
</form>