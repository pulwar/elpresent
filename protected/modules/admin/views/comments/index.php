<?php
$this->widget('ext.bootstrap.widgets.TbGridView', array (
        'type' => array('condensed', 'striped'),
        'dataProvider' => $oComments,
        'columns' => array
            (
            'sender_name',
            array
                (
                'name' => 'item_id',
                'type' => 'raw',
                'value' => '$data->item ? htmlspecialchars($data->item->title) : ""'
            ),
            array
                (
                'name' => 'text',
                'type' => 'html',
                'value' => 'nl2br(htmlspecialchars($data->text))'
            ),
            'rating',
            'cdate',
            array
                (
                'type' => 'html',
                'value' => 'CHtml::link("[редактировать]", "/admin/comments/manage/id/".$data->id."/page/".(Yii::app()->request->getParam("Comment_page")))."<br />
                            ".CHtml::link("[удалить]", "/admin/comments/delete/id/".$data->id, array("class" => "delete"))."
                            ".(!$data->visibility ? "<br />".CHtml::link("[опубликовать]","/admin/comments/publish/id/".$data->id) : "")'
            ),
        )
    ));
?>