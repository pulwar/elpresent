<div class="admin-main">
    <div class="title">
    <span>Manage mood</span>
    </div>

    <div class="inner">
        <div class="adminItem">
        <div class="rightAdminItem">
            <div class="notify-messages"><? $this->widget('NotifierWidget'); ?></div>
            <form action="" method="POST" enctype="multipart/form-data">
                <div class="itemTitle">Order:</div>
                <div class="itemInfo">
                    <input type="text" name="Mood[mood_order]" value="<?= $mood->mood_order; ?>" size="3" style="width: 40px;"/>
                </div>
                 <div class="itemTitle">Visible:</div>
                <div class="itemInfo">
                    <select name="Mood[visible]">
                        <option value="1" <?= $mood->visible ? 'selected="selected"' : ''; ?> >Yes</option>
                        <option value="0" <?= !$mood->visible ? 'selected="selected"' : ''; ?>>No</option>
                    </select>
                </div>
                <div class="clear"></div>
                <div class="itemTitle">Text:</div>
                <div class="itemInfo">
                    <textarea class="input-xxlarge" name="Mood[mood]" style="width:100%;" rows="10" ><?= CHtml::encode($mood->mood); ?></textarea>
                </div>

                <div class="itemTitle">Items: <br/>
                    <a href="javascript:void(0)" onclick="$('#popupSelect').show();return false;" >Add item</a>
                </div>
                <div class="itemInfo" id="mood_items"><br/><br/>
                    <? foreach ($mood->items as $item): ?>
                    <div>
                        <a href="<?= path('catalog/item',['perm_link'=>$item->perm_link], true); ?>" target="_blank" >
                        <?= CHtml::encode($item->item->title); ?>
                    </a><br/>
                    Title: <input type="text" name="Mood_items[<?= $item->item_id; ?>]" value="<?= CHtml::encode($item->title); ?>" /> <button onclick="removeItem(this)" >Remove</button>
                    </div>
                    <? endforeach; ?>
                </div>


                <div class="itemTitle">Image:</div>
                <div class="itemInfo">
                    <? if($mood->image):?>
                    <img src="/upload/moods/<?= $mood->image ?>" width="50" />
                    <? endif;?>
                    <input type="file" name="Mood[image]"/>
                </div>
                <div class="clear"> </div>
                <div class="clear"> </div>
                <input type="submit" value="Save"/>
            </form>
        </div>
        </div>
        <div class="clear"></div>
    </div>

</div>

<div style="display:none;text-align: center;padding: 10px;background-color: gray;width: 400px;position: fixed;margin: 100px 30%; z-index: 100;" id="popupSelect">
    <div style="background: #E9E9E9;padding: 10px;">
            <form action="/admin/mood/searchItems" method="POST">
            Seacrh item:
            
            <input type="text" name="search" id="search" /> <input type="submit" value="Search" /> <a href="javascript:void(0)" style="position: absolute;right: -10px;top:-10px;" onclick="$('#popupSelect').hide()" ><img src="/images/close-button.png" /></a>
            <br/>
            <div id="items-form" style="text-align: left;">

            </div>
            </form>
         

    </div>
</div>

<script type="text/javascript">
    function removeItem(elem) {
        $(elem).parent().remove();
    }
    function addItem(id,title) {
        var html = '<div>';
        html += '<a href="'+'/catalog/item/item_id/'+id+'" target="_blank">'+title+'</a><br/>';
        html += 'Title: <input type="text" name="Mood_items['+id+']" value="'+title.substr(0,32)+'" />';
        html += '<button onclick="removeItem(this)" >Remove</button>';
        html += '</div>';
        $('#mood_items').append(html);
    }
    $(document).ready(function(){
        var search = '';
        $('#popupSelect form').bind('submit',function(){
            $.ajax({
                type: 'post',
                url : '/admin/mood/searchItems',
                data : $(this).serialize(),
                dataType: "json",
                success: function(data) {
                    var str = '';
                    for (var i=0; i<data.length;i++) {
                        str += '<a href="'+'/catalog/item/item_id/'+data[i].item_id+'" target="_blank">'+data[i].title+'</a> <button onclick="addItem('+data[i].item_id+',\''+data[i].title+'\')">Add</button><br/>';
                        
                    }
                    if(str == '') {
                        str = 'Ничего не найдено';
                    }
                    $('#items-form').html(str);
                }
            });
            return false;
        });

    });

</script>