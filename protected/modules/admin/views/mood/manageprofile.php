<div class="admin-main">
    <div class="title">
    <span>Manage Profile</span>
    </div>

    <div class="inner">
        <div class="adminItem">
        <div class="rightAdminItem">
            <div class="notify-messages"><? $this->widget('NotifierWidget'); ?></div>
            <form action="" method="POST">
                <div class="itemTitle">Order:</div>
                <div class="itemInfo">
                    <input type="text" name="profile_order" value="<?= $profile->profile_order; ?>" size="3" style="width: 40px;"/>
                </div>
                 <div class="itemTitle">Visible:</div>
                <div class="itemInfo">
                    <select name="visible">
                        <option value="1" <?= $profile->visible ? 'selected="selected"' : ''; ?> >Yes</option>
                        <option value="0" <?= !$profile->visible ? 'selected="selected"' : ''; ?>>No</option>
                    </select>
                </div>
                <div class="clear"></div>
                <div class="itemTitle">Name:</div>
                <div class="itemInfo">
                    <input type="text" name="name" value="<?= $profile->name; ?>" />
                </div>
                <div class="clear"></div>
                <div class="itemTitle">Post:</div>
                <div class="itemInfo">
                    <input type="text" name="post" value="<?= $profile->post; ?>" />
                </div>
                <div class="clear"></div>
                <div class="itemTitle">Text:</div>
                <div class="itemInfo">
                    <textarea class="input-xxlarge" name="profile" cols="" rows="10" ><?= CHtml::encode($profile->profile); ?></textarea>
                </div>
                <div class="clear"> </div>
                <div class="clear"> </div>
                <input type="submit" class="btn btn-primary" value="Save"/>
            </form>
        </div>
        </div>
        <div class="clear"></div>
    </div>
    
</div>