<a class="btn" href="/admin/mood/manageprofile/">Добавить</a>

<? if(!empty ($profile)){
$this->widget('ext.bootstrap.widgets.TbGridView', array (
                                                        'type' => array('condensed', 'striped'),
            'dataProvider' => $profile,
            'columns' => array
                (
                    array
                        (
                        'name' => 'profile_order',
                        'value' => '$data->profile_order'
                    ),
                    array
                        (
                        'name' => 'name',
                        'value' => '$data->name'
                    ),
                    array
                        (
                        'name' => 'profile',
                        'type' => 'html',
                        'value' => '$data->profile'
                    ),
                    array
                        (
                        'type' => 'html',
                        'value' => 'CHtml::link("[редактировать]", "/admin/mood/manageprofile/id/".$data->id)."<br />".CHtml::link("[удалить]", "/admin/mood/profiledelete/id/".$data->id, array("class" => "delete"))."<br />".CHtml::link("[редактировать фото]", "/admin/mood/managemoods/id/".$data->id)'
                    )
                )
        ));
}
?>