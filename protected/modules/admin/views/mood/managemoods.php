<div class="admin-main">
    <div class="inner">

    <table width="100%" id="moodform" >
        <tr>
            <th>Order</th>
            <th>Text</th>
            <th>Image</th>
            <th>Visible</th>
            <th>Actions</th>
        </tr>
    <? foreach($moods as $mood): ?>
        <tr>
            <td>
                <?= $mood->mood_order ?>
            </td>
            <td>
                <div class="mood-text" >
                        <div class="mood-text-content"><p><?=  str_replace(array("\n","\r"), array("<br/>",''),$mood->mood); ?></p></div>
                        <div class="mood-text-bottom"></div>
                </div>
            </td>
            <td>
                <img src="/upload/moods/<?= $mood->image ?>" />
              </td>
            <td>
                <?= $mood->visible ?>
            </td>
            <td>
                <a href="/admin/mood/deletemood/id/<?= $mood->id ?>" class="delete" >delete</a><br/>
                <a href="/admin/mood/managemood/id/<?= $profile ?>/mood_id/<?= $mood->id ?>" >manage</a><br/>
            </td>
        </tr>
    <? endforeach; ?>
        <tr>
            <td>
                <a href="/admin/mood/managemood/id/<?= $profile ?>" class="add" >Add</a>
            </td>
            <td></td><td></td><td></td><td></td>
        </tr>
    </table>    

    </div>
</div>

