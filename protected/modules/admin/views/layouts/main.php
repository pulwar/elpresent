<!DOCTYPE html>
<html>
    <head>
        <title>Elpresent :: Admin Panel</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="shortcut icon" href="/favicon.png" type="image/x-icon">
        <link rel="stylesheet" type="text/css" href="/css/admin/style.css?v=1" />
        <link rel="stylesheet" type="text/css" href="/css/admin/pack.css" />
        <link rel="stylesheet" type="text/css" href="/css/admin/croppie.css" />
<!--        <link rel="stylesheet" type="text/css" href="/css/admin/prompt.css" />-->
        <link rel="stylesheet" type="text/css" href="/css/smoothness/jquery-ui-1.9.1.custom.min.css" />
        
        <link rel="stylesheet" type="text/css" href="/js/datetimepicker/bootstrap-datetimepicker.min.css" />
        <!--[if IE 6]>-->
<!--            <link rel="stylesheet" type="text/css" href="ie6.css" />-->
        <![endif]-->        		
		<?php
            Yii::app()->clientScript->registerCoreScript('cookie');
            $baseUrl = '';//Yii::app()->baseUrl;
			$cs = Yii::app()->clientScript;
			//$cs->registerCoreScript('jquery');
            $cs->registerScriptFile($baseUrl.'/js/functions.js');
			$cs->registerScriptFile($baseUrl.'/js/sound_message.js');
			$cs->registerScriptFile($baseUrl.'/js/jquery.facebox.js');
			$cs->registerScriptFile($baseUrl.'/js/jquery.fbox.js');
			$cs->registerScriptFile($baseUrl.'/js/jquery.easing.js');
			$cs->registerScriptFile($baseUrl.'/js/jquery.lavalamp.js');
			$cs->registerScriptFile($baseUrl.'/js/jquery.pause.js');
			$cs->registerScriptFile($baseUrl.'/js/admin/common.js');
			$cs->registerScriptFile($baseUrl.'/js/admin/instruction.js');
            $cs->registerScriptFile($baseUrl.'/js/jquery.easing.js');
            $cs->registerScriptFile($baseUrl.'/js/datetimepicker/bootstrap-datetimepicker.min.js');
            $cs->registerScriptFile($baseUrl.'/js/jquery.sort.js');
            $cs->registerScriptFile($baseUrl.'/js/admin/script.js');
            $cs->registerScriptFile($baseUrl.'/js/admin/croppie.js');
		?>
        <style>
            .no-row-margin .row {
                margin-left: 0 !important;
                padding-left: 10px;
            }
        </style>
		<script>

		</script>
    </head>
    <body>
        <div class="container">
            <header class="header">
                <nav class="container">
                    <? $this->widget('admin.components.AdminMainMenu'); ?>
                </nav>
            </header>

            <div class="container">
                <?php echo $content; ?>
            </div>

            <div class="fixed-bar">
                <div class="row">
                    <div class="span10"><a href="/" target="_blank" title="На сайт"><i class="icon-home"></i></a></div>
                    <div class="span2"><a href="/profile/logout"><i class="icon-off" title="Выход"></i></a></div>
                </div>
            </div>
        </div>

        <div id="orderInfo">
	        <a href="javascript:void(0);" class="close">x</a>
	        <a href="/admin">
		        <p><span class="label label-danger">Внимание!</span> новый заказ</p>
		        <section></section>
		    </a>
        </div>

    </body>
</html>