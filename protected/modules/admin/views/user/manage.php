<form action="" method="post" class="form-horizontal">
    <div class="control-group">
        <label class="control-label">Изменить пароль</label>
        <div class="controls"><input type="text" name="User[user_password]" value="" /><span class="help-block">оставьте пустым, если не требуется</span></div>

    </div>
    <div class="control-group">
        <label class="control-label">Имя</label>
        <div class="controls"><input type="text" name="User[user_firstname]" value="<?=$oUsers->user_firstname?>" /></div>
    </div>
    <div class="control-group">
        <label class="control-label">Фамилия</label>
        <div class="controls"><input type="text" name="User[user_lastname]" value="<?=$oUsers->user_lastname?>" /></div>
    </div>
    <div class="control-group">
        <label class="control-label">Email</label>
        <div class="controls"><input type="text" name="User[user_email]" value="<?=$oUsers->user_email?>" /></div>
    </div>
    <div class="control-group">
        <label class="control-label">Телефон</label>
        <div class="controls"><input type="text" name="User[user_phone]" value="<?=$oUsers->user_phone?>" /></div>
    </div>
    <div class="control-group">
        <label class="control-label">Адрес</label>
        <div class="controls"><input type="text" name="User[user_address]" value="<?=$oUsers->user_address?>" /></div>
    </div>
    <div class="control-group">
        <label class="control-label">Пол</label>
        <div class="controls">
            <select name="User[user_sex]">
                <option value="" <?=$oUsers->user_sex==''?'selected':''?>>Не указан</option>
                <option value="m" <?=$oUsers->user_sex=='m'?'selected':''?>>М</option>
                <option value="f" <?=$oUsers->user_sex=='f'?'selected':''?>>Ж</option>
            </select>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Роль</label>
        <div class="controls">
            <select name="User[user_role]">
                <option value="admin" <?=$oUsers->user_role=='admin'?'selected':''?> style="color:#900">Администратор</option>
                <option value="user" <?=$oUsers->user_role=='user'?'selected':''?> style="color:#090">Пользователь</option>
                <option value="guest" <?=$oUsers->user_role=='guest'?'selected':''?> style="color:#999">Гость</option>
            </select>
        </div>
    </div>
    <div class="form-actions">
        <input type="submit" value="Сохранить" class="btn btn-primary" /> | <a href="<?= $fromPage; ?>" class="btn">Отменить</a>
    </div>
</form>