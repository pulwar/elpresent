<h1>Новый функционал</h1>
<p>Как только начинает реализовываться новый функционал, в этом разделе будет помещаться выключатель для него. Выключатели привязаны к профилю пользователя, так что при включении какой-то функции для тестирования, никто другой этой функции не увидет.</p>
<p>По окончанию тестирования нового функционала, он будет включен глобально и выключатель удалится.</p>
<form method="POST" class="form feature-switches">
<?php if (!empty($features)) { ?>
    <?php foreach ($features as $feature => $details) { ?>
        <div class="control-group">
            <label class="control-label"><strong><input name="Switches[<?= $feature; ?>]" <?php if (isFeatureEnabled($feature)) { ?>checked="checked"<?php } ?> value="1" type="checkbox" /> <?= $details['title']; ?></strong></label>
            <p class="help-block"><?= $details['hint']; ?></p>
        </div>
    <?php } ?>
    <p class="well well-large clearfix">
        <input type="submit" class="btn btn-primary btn-big pull-right" value="Сохранить" />
    </p>
<?php } else { ?>
    <p class="lead">Нового функционала пока нет :(</p>
<?php } ?>
</form>