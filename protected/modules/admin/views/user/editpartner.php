
<div class="yiiForm">
    <?php echo CHtml::beginForm('', 'post', ['class'=>'form-horizontal']); ?>
    <?php echo CHtml::errorSummary($form); ?>
    <div class="control-group">
        <label class="control-label">Логин*</label>
        <div class="controls">
            <?php echo CHtml::activeTextField($form, 'login') ?>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Пароль</label>
        <div class="controls">
            <?php echo CHtml::activePasswordField($form,'password') ?>
            <span class="help-block">Для сохранения текущего, оставьте поле пустым.</span>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Email*</label>
        <div class="controls">
            <?php echo CHtml::activeEmailField($form, 'email') ?>
        </div>
    </div>
    <div class="form-actions">
        <input type="submit" value="Сохранить" class="btn btn-primary" /> | <a href="<?=$fromUrl;?>" class="btn">Отменить</a>
    </div>
    <?php echo CHtml::endForm(); ?>
</div>



