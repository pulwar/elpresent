<div class="yiiForm">
    <?php echo CHtml::beginForm('', 'post', ['class'=>'form-horizontal']); ?>
    <?php echo CHtml::errorSummary($form); ?>
    <div class="control-group">
        <label class="control-label">Логин*</label>
        <div class="controls">
            <?php echo CHtml::activeTextField($form, 'login') ?>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Пароль*</label>
        <div class="controls">
            <?php echo CHtml::activePasswordField($form,'password') ?>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Email*</label>
        <div class="controls">
            <?php echo CHtml::activeEmailField($form, 'email') ?>
        </div>
    </div>
    <?php echo CHtml::hiddenField('id', $partnerId) ?>

    <div class="form-actions">
        <input type="submit" value="Создать" class="btn btn-primary" /> | <a href="/admin/partner/edit/id/<?=$partnerId?>" class="btn">Отменить</a>
    </div>
    <?php echo CHtml::endForm(); ?>
</div>