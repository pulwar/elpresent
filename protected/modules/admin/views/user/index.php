<div class="well">
    <form class="form-inline" action="" method="POST">
        <label>Email: <input type="text" name="user_email" value="<?= $filter['user_email']; ?>"</label>
        <input type="submit" value="поиск" class="btn btn-primary" />
    </form>
</div>

<div id="user-table">
    <?
    $this->widget('ext.bootstrap.widgets.TbGridView', array
    (
        'type' => array('condensed', 'striped'),
        'dataProvider' => $oUsers,
        'columns'      => array
        (
            'id',
            'user_email',

            array(
                'type' => 'raw',
                'name' => 'user_birthday',
                'value' => 'date("d.m.Y",$data->user_birthday)'
            ),
            'user_role',
            'user_phone',
            'user_address',
            array(
                'type' => 'raw',
                'name' => 'user_sex',
                'value' => '$data->user_sex ? ($data->user_sex == "f" ? "жен":"муж"):"--"'
            ),
            array(
                'type'=>'raw',
                'name' => 'user_age',
                //'value' => '$data->getAge()'
                'value' => 'floor((time() - $data->user_birthday) / 31536000)'
            ),
            'user_firstname',
            'user_lastname',

            array
            (
                'type'  => 'html',
                'value' => '
CHtml::link("войти", "/admin/user/login/id/".$data->id."/page/".(Yii::app()->request->getParam("User_page")),array("class" => "btn btn-small"))
."<br/>".
CHtml::link("править", ($data->user_role == partner ? "/admin/user/editpartner/id/".$data->id : "/admin/user/manage/id/".$data->id)."/page/".(Yii::app()->request->getParam("User_page")),array("class" => "btn btn-small"))
."<br />".
CHtml::link("удалить", "/admin/user/delete/id/".$data->id,array("class" => "btn btn-small delete"))'
            )
        )
    ));
    ?>
</div>