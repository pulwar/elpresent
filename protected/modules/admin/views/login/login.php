<? $this->pageTitle=Yii::app()->name . ' - Login'; ?>
<h1>Login</h1>

<div class="yiiForm">
<? echo CHtml::beginForm(); ?>

<? echo CHtml::errorSummary($form); ?>

<div class="simple">
<? echo CHtml::activeLabel($form,'username'); ?>
<? echo CHtml::activeTextField($form,'username') ?>
</div>

<div class="simple">
<? echo CHtml::activeLabel($form,'password'); ?>
<? echo CHtml::activePasswordField($form,'password') ?>
</div>

<div class="action">
<? echo CHtml::activeCheckBox($form,'rememberMe'); ?>
<? echo CHtml::activeLabel($form,'rememberMe'); ?>
<br/>
<input type="submit" class="btn btn-primary" value="Войти"/> | <a href="/" class="btn btn-link">Отмена</a>
</div>
<? echo CHtml::endForm(); ?>
</div>