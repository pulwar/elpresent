<div id="user-table">
<br />
<a class="btn" href="/admin/city/add">Добавить</a>
<?php
$this->widget('ext.bootstrap.widgets.TbGridView', array (
    'type' => array('condensed', 'striped'),
    'dataProvider' => $oCity,
    'columns'      => array
    (
        'id',
        'title',
        'name',

        array
        (
            'type'  => 'html',
            'value' => 'CHtml::link("[править]", "/admin/city/edit/id/".$data->id."/page/".(Yii::app()->request->getParam("User_page")))."<br />".CHtml::link("[удалить]", "/admin/city/delete/id/".$data->id,array("class" => "delete"))'
        )
    )
));
?>
</div>