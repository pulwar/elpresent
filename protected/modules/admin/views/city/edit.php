<?php if (count($oCity->getErrors())) : ?>
    <div class="well">
        <?php echo  CHtml::errorSummary($oCity); ?>
    </div>
<?php endif; ?>
<form action="" method="post" class="form form-horizontal">
    <div class="control-group">
        <label class="control-label">Название</label>
        <div class="controls">
            <input type="text" required="" name="City[title]" value="<?= $oCity->title; ?>" />
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Системное имя</label>
        <div class="controls">
            <input type="text" required="" name="City[name]" value="<?= $oCity->name; ?>" />
            <p class="help-block">этот параметр будет использоваться в ссылках. Допускаются только латинские символы.</p>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label">Включить город?</label>
        <div class="controls">
            <input value="1" type="checkbox" name="City[enabled]" <?= $oCity->enabled ? 'checked=""' : ''; ?> />
        </div>
    </div>
    <div class="form-actions">
        <input type="submit" value="Сохранить" />
    </div>
</form>