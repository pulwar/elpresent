<a href="/admin/post/manage.html">[Создать новую рассылку]</a><br />
<?php
$this->widget('ext.bootstrap.widgets.TbGridView', array (
    'type' => array('condensed', 'striped'),
    'dataProvider' => $oPosts,
    'columns' => array
        (
        array
            (
            'name' => 'schedule',
            'type' => 'raw',
            'value' => '$data->schedule ? $data->schedule : "Не указана"'
        ),
        'subject',
        array
            (
            'name' => 'body',
            'value' => 'substr(strip_tags($data->body),0,100)."..."'
        ),
        array
            (
            'name' => 'sent',
            'type' => 'raw',
            'value' => '$data->sent == 0 ? "<span class=\"no\">Не отправлено</span>" : "<span class=\"yes\">Отправлено</span>"'
        ),
        array(
            'name' => 'actions',
            'type' => 'raw',
            'value' => '($data->sent == 0 ? CHtml::link("[редактировать]", "/admin/post/manage/id/{$data->id}.html")."<br />" : "").CHtml::link("[удалить]", "/admin/post/delete/id/{$data->id}.html", array("class" => "delete"))'
        )
    )
));
?>