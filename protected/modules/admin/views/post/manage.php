<? if ($oPost->hasErrors()) :?>
<div style="color: red;">
    <?= CHtml::errorSummary($oPost); ?>
</div>
<div style="width:800px;margin: 20px auto 20px auto;" id="preview">
    <?= $oPost->body; ?>
</div>
<script language="javascript" type="text/javascript">
    $("#preview a:not(a[href^=http])").each(function(){
      $(this).after($('<span style="color:red;border:solid 1px red;margin:0px 5px 0px 5px;">href='+this.href+'</span>'));
    })
    $("#preview img:not(img[src^=http])").each(function(){
      $(this).after($('<span style="color:red;border:solid 1px red;margin:0px 5px 0px 5px;">href='+this.src+'</span>'));
    })
    
</script>
<?
endif;
?>

<div class="b-admin-wrapper">
    <form action="" method="post" enctype="multipart/form-data" class="b-admin-form b-item">
        <div style="padding: 5px;">
            <p>

                <label>
                    <span>Тема письма</span>
                    <input type="text" name="aPost[subject]" value="<?=($oPost && $oPost->subject) ? $oPost->subject : 'Новости Elpresent.by'
?>" maxlength="255" />
                </label>
            </p>
            <p>
                <span>Дата рассылки новости</span><br />
                <label><input type="radio" name="aPost[send]" <?=($oPost && !$oPost->schedule) ? 'checked' : ''
?> value="1" /> Немедленно.</label><br />
                <label><input type="radio" name="aPost[send]" <?=($oPost && $oPost->schedule || !$oPost) ? 'checked' : ''
?> value="0" /> По расписанию <input class="postDatepicker" style="width: 100px;" type="text" name="aPost[schedule]" value=" <?=($oPost && $oPost->schedule) ? $oPost->schedule : date('Y-m-d', strtotime('+1 day'))
?>" /></label>
            </p>
            <p><label>Email Для дебага <input type="text" name="aPost[debug_mail]" /></p>
            <p>
                <span>Текст</span>
                <textarea name="aPost[body]" class="ckeditor"><?=($oPost && $oPost->body) ? $oPost->body : ''
?></textarea>
            </p>
            <input class="btn btn-primary" type="submit" value="Сохранить" />
        </div>
    </form>
</div>
<script language="javascript" type="text/javascript">
    $('.postDatepicker').datepicker({dateFormat: 'yy-mm-dd', minDate: '+1D'});
</script>