<? class TableView extends CWidget
{
	public $list = array();
	public function init()
	{
		if(!$this->owner instanceof CAdminController)
			return;
		$this->normalizeList();
		$this->startTable();
		$this->thead();
		$this->tbody();
		$this->endTable();
	}
	
	protected function normalizeList()
	{
		if(is_array($this->list))
			return;
		if($this->list instanceof IteratorAggregate)
		{
			$this->list = $this->list->getIterator();
		}
		if($this->list instanceof ArrayIterator)
			$this->list = $this->list->getArrayCopy();
			
		if(!is_array($this->list))
			throw new exception('can not normalize AdminTableView::list. <pre>'.print_r($this->list,1).'</pre>');
	}

	public function getActionsLinks($model)
	{
		$actions = $this->owner->getItemActions();
		$links = array();
		foreach($actions as $each)
			$links[] = $this->owner->getActionLink($model,$each);
		return implode(' ',$links);
	}
	
	public function getModelTypes($model)
	{
		$types = $model->getViewData();
		$props = array_keys($types);
		$titles = array_intersect_key($model->attributeLabels(),$types);
	}
}
