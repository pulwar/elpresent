<? class DateTimeObjDateType extends AbstractDataType
{
	public function getFormat()
	{
		return 'Y-m-d';
	}
	
	public function toHTML()
	{
		return $this->getValue()->format($this->getFormat());
	}
	
	public function isEmptyValue($value)
	{
		return false;
		return trim($this->getValue()->format($this->getFormat()),'0- ') == '';
	}
}
