<? class InputListTextType extends AbstractDataType
{
	private function getId()
	{
		static $id;
		if(is_null($id))
			$id = 'c'.uniqid();
		return $id;
	}
	
	public function toHTML()
	{
		$value = $this->getValue();
		$html = implode($this->getSeparator(),array_map(array($this,'getEachHtml'),(array)$value) );
		$html .= "<br />";
		$html .= $this->getAddButton();
		$html .= $this->getAddFunc();
		return $html;
	}
	
	protected function getSeparator()
	{
		return '<br />';
	}
	
	protected function getEachHtml($each)
	{
		$propname = $this->getModelPropertyName().'[]';
		return "<input type='text' name='{$propname}' value='{$each}' />";
	}
	
	protected function getAddButton()
	{
		$id = $this->getId();
		$title = _('Add');
		return "<input type='button' value='{$title}' onclick='add{$id}()' id='{$id}' />";
	}
	
	protected function getAddFunc()
	{
		$id 	= $this->getId();
		$html 	= $this->getEachHtml('');
		$html 	= strtr($html,"'",'"')."<br />";
		return 
		"<script language='javascript'>
		function add{$id}() { $('#{$id}').before($('{$html}')); }
		</script>";
	}
}
