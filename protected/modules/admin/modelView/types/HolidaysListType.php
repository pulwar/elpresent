<? class HolidaysListType extends AbstractDataType
{
	public function toHTML()
	{
		$value = $this->getValue();
		if(is_string($value))
			return $value;
		return implode("<br />",array_map(function ($model)
		{
			return $model->title;
		},$value));
	}
	
	public function isEmptyValue($value)
	{
		return empty($value);
	}
}
