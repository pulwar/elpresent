<? class EditorViewType extends AbstractDataType
{
	public function toHTML()
	{
		$value 	= $this->getValue();
		$prop	= $this->getModelPropertyName();
		return "<textarea name='{$prop}' class='ckeditor'>{$value}</textarea>";
	}
	
	public function getDefaultValue()
	{
		return '';
	}
}
