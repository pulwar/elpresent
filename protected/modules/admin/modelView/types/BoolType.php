<? class BoolType extends AbstractDataType
{
	public function toHTML()
	{
		return $this->isEmptyValue($this->value) ? _('No') : _('Yes');
	}
}
