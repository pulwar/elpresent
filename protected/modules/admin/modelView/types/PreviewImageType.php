<? class PreviewImageType extends AbstractDataType
{
	public function getValue()
	{
		return ( isset($this->value[0]) ) ? $this->value[0] : null;
	}
	
	protected function normalizeImage($image)
	{
		if( ! ( is_object($image) && method_exists($image,'toWrapper')))
			return '';
		return $image->toWrapper()->crop(100,100);
	}
	
	public function toHTML()
	{
		$image = $this->normalizeImage($this->getValue());
		return "<img src='{$image}' width='100' height='100' />";
	}
	
	public function getDefaultValue()
	{
		return _('No image');
	}
}
