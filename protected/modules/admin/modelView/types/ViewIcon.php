<? class ViewIcon extends AbstractDataType
{
        protected $modelClass;

        public function  __construct($title, $modelPropertyName, $model)
        {
            parent::__construct($title, $modelPropertyName, $model);
            $this->modelClass = strtolower(get_class($this->getModel()));
        }
	protected function getImage()
	{
		$value = $this->getValue();
		if(!$value)
			return '';

                return "<img src='/images/{$this->modelClass}/{$value}'/>";
	}
	
	public function toHTML()
	{
		return $this->getImage();
	}
         
        public function getDefaultValue()
	{
		return false;
	}
}