<? class CheckboxType extends AbstractDataType
{
	public function toHTML()
	{
		$prop = $this->getModelPropertyName();
		$html = "<input type='checkbox' name='{$prop}' value='1'";
		$html .= $this->isChecked() ? " checked='checked' " : '';
		$html .= "/>";
		return $html;
	}
	
	protected function isChecked()
	{
		$value = $this->getValue();
		return !empty($value);
	}
	
	public function getDefaultValue()
	{
		return 0;
	}
}
