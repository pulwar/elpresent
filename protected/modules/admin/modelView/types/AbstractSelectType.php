<? abstract class AbstractSelectType extends AbstractDataType
{
	abstract protected function getList();
	
	protected function isSelected($el)
	{
		return $this->getValue() == $this->getElValue($el);
	}
	
	protected function getElValue($el)
	{
		return $el->id;
	}
	
	protected function getElTitle($el)
	{
		return $el->title;
	}
	
	protected function getOption($el)
	{
		$value = $this->getElValue($el);
		$title = $this->getElTitle($el);
		return "<option value='{$value}'".($this->isSelected($el) ? 'selected="selected"' : '') . ">{$title}</option>";
	}
	
	public function toHTML()
	{
		$prop = $this->getModelPropertyName();
		return "<select name='{$prop}'>".implode(array_map(array($this,'getOption'),$this->getList()))."</select>";
	}
}
