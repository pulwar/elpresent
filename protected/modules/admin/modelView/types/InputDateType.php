<? class InputDateType extends DateTimeType
{
	private function getId()
	{
		static $id;
		if(is_null($id))
			$id = "c".uniqid();
		return $id;
	}
	
	protected function getDatepickerFormat()
	{
		$search = array('Y','m','d');
		$replace = array('yy','mm','dd');
		return str_replace($search,$replace,$this->getFormat());
	}
	public function toHTML()
	{
		$id = $this->getId();
		$propname = $this->getModelPropertyName();
		$value = parent::toHTML();
		$format = $this->getDatepickerFormat();
		return 
		"<input id='{$id}' type='text' name='{$propname}' value='{$value}'/>
		<script language='javascript'>$(function() { $('#{$id}').datepicker({dateFormat: '{$format}'}); });</script>";
	}		
}
