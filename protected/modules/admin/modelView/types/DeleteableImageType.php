<? class DeleteableImageType extends ImageType
{
	public function getImage()
	{
		$parent = parent::getImage();
	}
	
	protected function getDeleteInput()
	{
		$v = $this->getValue();
		return '<input type="checkbox" name="delete_image" value="'.$v.'">';
	}
}
