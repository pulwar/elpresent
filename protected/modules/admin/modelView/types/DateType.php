<? class DateType extends AbstractDataType
{
	public function toHTML()
	{
		return $this->getValue();
	}
	
	public function isEmptyValue($value)
	{
		return empty($value) || trim($value,'0:- ') == '';
	}
}
