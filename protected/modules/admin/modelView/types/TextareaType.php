<? class TextareaType extends AbstractDataType
{
	public function toHTML()
	{
		$prop 	= $this->getModelPropertyName();
		$value 	= $this->getValue();
		$wysiwyg = !in_array($prop, array('seo_text', 'meta_keywords','meta_description'));
        return $wysiwyg ? Yii::app()->controller->widget('ext.tinymce.TinyMce', array(
            'model' => $this->getModel(),
            'attribute' => $prop,
            // Optional config
            'fileManager' => array(
                'class' => 'ext.elFinder.TinyMceElFinder',
                'connectorRoute'=>'admin/elfinder/connector',
            ),
        ), true):(in_array($prop, array('seo_text'))?
                Yii::app()->controller->widget('ext.tinymce.TinyMce', array(
            'model' => $this->getModel(),
            'attribute' => 'seo_text',
            'name' => 'seo_text',        
            // Optional config
            'fileManager' => array(
                'class' => 'ext.elFinder.TinyMceElFinder',
                'connectorRoute'=>'admin/elfinder/connector',
            ),
        ), true): "<textarea name='{$prop}' style='width: 400px; height: 150px;'>{$value}</textarea>");
	}
	
	public function getDefaultValue()
	{
		return '';
	}
}
