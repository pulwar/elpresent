<? abstract class AbstractDataType implements iDataType
{
	protected
		$value,
		$title,
		$modelPropertyName,
		$model;
	public function __construct($title,$modelPropertyName,$model)
	{
		$this->value = $model->$modelPropertyName;
		$this->model = $model;
		$this->title = $title;
		$this->modelPropertyName = $modelPropertyName;
	}
	
	public function getValue()
	{
		return !$this->isEmptyValue($this->value) ? $this->value : $this->getDefaultValue();
	}
	
	public function isEmptyValue($value)
	{
		return empty($value);
	}
	
	public function getDefaultValue()
	{
		return 'Not defined';
	}
	
	public function getModel()
	{
		return $this->model;
	}
	
	public function getTitle()
	{
		return $this->title;
	}
	
	public function getModelPropertyName()
	{
		return $this->modelPropertyName;
	}
}
