<?php

class IconType extends AbstractDataType
{
    protected $modelClass;

    public function  __construct($title, $modelPropertyName, $model)
    {
        parent::__construct($title, $modelPropertyName, $model);
        $this->modelClass = strtolower(get_class($this->getModel()));
    }

    protected function getImage()
	{
		$value = $this->getValue();
		if(!$value) {
            return '';
        }

        return "<img src='/images/{$this->modelClass}/{$value}'/>";
	}

	protected function getImageInput()
	{
		$propertyName = $this->getModelPropertyName();
		return "<input type='hidden' name='{$propertyName}' value='{$this->getValue()}' class='icon_input' />";
	}

	public function toHTML()
	{
		return $this->getRangeImages().'<p>Выбранная иконка:</p><p id="current_icon">'.
                        $this->getImage().'</p>'.$this->getImageInput().$this->getScript();
	}

    public function getScript()
    {
        $res = '<script type="text/javascript">';
        $res .= '$(".range_icon").each(function(){
                $(this).bind("click",function(){
                    $("p#current_icon").html("<img src=\"/images/'.$this->modelClass.'/"+this.id+"\"/>");
                    $("input.icon_input").val(this.id);
                })
            })';
        $res .= '</script>';
        return $res;
    }

    public function getRangeImages()
    {

        chdir("./images/{$this->modelClass}/");
        $images = glob('*.svg');
		//html::pr($images, 1);
        chdir('../../');
        $res = '<p> Доступные иконки: </p>';
        foreach ($images as $v){
            $res .= " <img src='/images/{$this->modelClass}/$v' class='range_icon' id='$v' style='cursor:pointer;'/>";
        }

        // add button
        $res .= '<br/><br/><span class="btn add-icon"><i class="icon-plus"></i> Добавить иконку</span><br/><br/>';

        return $res;
    }

    public function getDefaultValue()
	{
		return false;
	}
}