<? class ImagesListType extends AbstractDataType
{
    protected function getImageHtml($data)
    {
        list($id,$value) = $data;
        return 
        "<div class='delete-images'>
            <img src='{$value}' height='100' weight='100' />
            <input type='checkbox' class='check' name='image_id' value='{$id}'>
        </div>";
    }
    protected function getImage()
    {
        $values = $this->value;
        if(empty($values))
            return '';
    
        return implode(array_map(array($this,'getImageHtml'),array_map(array($this,'normalizeImage'),$values)));
    }
    
    protected function normalizeImage($value)
    {           
        return array($value->id,$value->toWrapper()->resize(85,70));
    }
    
    protected function getImageInput()
    {
        $propname = $this->getModelPropertyName();
        return "<div style='padding: 0px;'><input type='file' name='{$propname}[]' /></div>";
    }
    
    public function toHTML()
    {
        return 
        "<div class='images'>".
            $this->getImage().
        '</div>'.
            $this->getImageInput();
    }
}
