<? class DateTimeType extends DateType
{
	protected function getFormat()
	{
		return 'Y-m-d';
	}
	
	public function toHTML()
	{
		$value = $this->getValue();
		if(! $value instanceof DateTime)
			return $value;
		return $value->format($this->getFormat());
	}
	
	public function isEmptyValue($value)
	{
		return !is_a($value,'DateTime') || trim($value->format($this->getFormat()),'-0: ') == ''; 
	}
}
