
<? class InputTextType extends AbstractDataType
{
	public function toHTML()
	{
		$propName 	= $this->getModelPropertyName();
		$value		= $this->getValue();
		return "<input type='text' name='{$propName}' value='{$value}' />";
	}
	
	public function getDefaultValue()
	{
		return '';
	}
}
