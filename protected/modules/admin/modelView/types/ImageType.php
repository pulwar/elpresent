<? class ImageType extends AbstractDataType
{
	protected function getImage()
	{
		$value = $this->getValue();
		if(!$value)
			return '';
		
		if($value instanceof ImageWrapper)
			$value = $value->crop(100,100);
		
		return "<img src='{$value}' height='100' weight='100' />";
	}
	
	protected function getImageInput()
	{
		$propname = $this->getModelPropertyName();
		return "<input type='file' name='{$propname}' />";
	}
	
	public function toHTML()
	{
		return $this->getImage().$this->getImageInput();
	}
}
