<? class RelatedRecordTitleType extends AbstractDataType
{
	public function toHTML()
	{
		if ( isset($this->model->{$this->modelPropertyName}) )
		{
			return $this->model->{$this->modelPropertyName}->title;
		}
		
		return '';
	}
}
