<? interface iDataType
{
	public function toHTML();
	public function getTitle();
	public function getModel();
	public function getValue();
	public function getModelPropertyName();
}
