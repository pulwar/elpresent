<? class getCategoryWidget extends CAction
{
	protected
		$category,
		$custom_field;
	
	protected function requireRequestVar($var,$type = 'int')
	{
		$value = Yii::app()->request->getParam($var);
		settype($value,$type);
		if($value)
			return $value;
			
		throw new CHttpException(404);
		
	}
	
	public function init()
	{
		$cutsom_field_id 	= $this->requireRequestVar('custom_field_id');
		$category_id		= $this->requireRequestVar('category_id');
		$this->category 	= Category::model()->findByPk($category_id);
		$this->custom_field	= CustomField::model()->findByPk($custom_field_id);
	}
	
	public function run()
	{
		
	}
	
	public function update()
	{
		if($_SERVER['REQUEST_METHOD'] != 'POST' || ! $this->custom_field instanceof iHasCategoryDataCollector)
			return true;
		$this->custom_field->update($this->category);
	}
}
?>
