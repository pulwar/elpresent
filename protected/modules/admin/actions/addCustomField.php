<? class AddCustomField extends CAction
{
	protected
		$category;
	protected function init()
	{
		$this->category = Category::model()->findByPk((int)Yii::app()->request->getParam('category_id'));
	}
	
	protected function getFieldModel()
	{
		$field = new CustomField();
		$field->category_id = $this->category->id;
		$field->class = (string)Yii::app()->request->getParam('class');
		$field->title = (string)Yii::app()->request->getParam('title');
		return $field;
	}
	
	protected function validateModel($model)
	{
		return $model->validate() && $model->validateCategoryCustomData($this->category);
	}
	
	protected function saveModel($model)
	{
		return $model->save() && $model->collectCategoryCustomData($this->category);
	}
		
	public function run()
	{
		if($_SERVER['REQUEST_METHOD'] == 'POST')
			$this->runPost();
		$this->runGet();
	}

	protected function runPost()
	{
		$model = $this->getFieldModel();	
	}
	
}
?>
