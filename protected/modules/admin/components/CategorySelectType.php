<? class CategorySelectType extends AbstractSelectType
{
	protected function getList()
	{
		return Category::model()->resetScope()->onlyRoots()->findAll();
	}
}
