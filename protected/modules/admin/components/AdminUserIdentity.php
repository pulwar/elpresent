<? 
/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class AdminUserIdentity extends CUserIdentity {
	/**
	 * @var string
	 */
	
	public $oUser = null;
	protected $id = null;

	public function authenticate() {

	    $this->oUser = AdminUser::model()->find(
	    	'`username` = :sUserLogin AND `pass` = :sUserPassword',
	        array(
	            ':sUserLogin'	    => $this->username,
	            ':sUserPassword'	=> md5($this->password)
		    )
	    );
	    
	    if($this->oUser)
	    	$this->oUser->setLastLogin();

	    $this->errorCode = ($this->oUser && (int)$this->oUser->id) ? self::ERROR_NONE : self::ERROR_PASSWORD_INVALID;

		return (self::ERROR_NONE === $this->errorCode);
	}

	public function getId()
	{
		return $this->oUser->id;
	}
    	
	public function getName() {
	    return $this->oUser->name;
	}
}