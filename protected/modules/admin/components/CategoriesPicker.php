<? 
class CategoriesPicker extends CWidget
{
	public 
		$model;
	public function init()
	{
		$class = get_class($this);
		echo "<div class='filter' id='{$class}'>";
		$this->echoMenu();
	}

	public function echoMenu()
	{
		$this->manufactureSelect();
		if(!$this->getCurrentManufactureId())
			return;
		$this->selectDelim();
		$this->modelsSelect();
		if(!$this->getCurrentModelId())
			return;
		$this->selectDelim();
		$this->accessoriesSelect();
	}
	
	public function getCurrentModelId()
	{
		return $this->model->model_id;
	}
	
	public function getCurrentManufactureId()
	{
		return $this->model->manufacture_id;
	}
	
	public function getCurrentAccessoryId()
	{
		return $this->model->accessory_id;
	}
	
	protected function isCurrentManufacture($model)
	{
		return $this->getCurrentManufactureId() == $model->id;
	}
	
	protected function isCurrentModel($model)
	{
		return $this->getCurrentModelId() == $model->id;
	}
	
	protected function isCurrentAccessory($model)
	{
		return $this->getCurrentAccessoryId() == $model->id;
	}

	protected function manufactureSelect()
	{
		$list = ModelT::model()->getManufacturesList();
		echo "<select name='manufacture_id'>";
		echo "<option value=''>Select manufacture</option>";
		foreach($list as $each)
		{
			echo "<option value='{$each->id}'";
			if($this->isCurrentManufacture($each))
				echo " selected='selected' ";
			echo ">{$each->title}</option>";
		}
		echo "</select>";
	}
	
	
	protected function modelsSelect()
	{
		$list = ModelT::model()->getModelsList($this->getCurrentManufactureId());
		echo "<select name='model_id'>";
		echo "<option value=''>Select model</option>";
		foreach($list as $each)
		{
			echo "<option value='{$each->id}'";
			if($this->isCurrentModel($each))
				echo ' selected="selected" ';
			echo ">{$each->title}</option>";
		}
		echo "</select>";
		
	}
	
	
	protected function accessoriesSelect()
	{
		$list = Accessory::model()->findAll(); 
		echo '<select name="accessory_id" id="accessory_picker">';
		echo "<option value=''>Select accessory</option>";
		foreach($list as $each)
		{
			echo "<option value='{$each->id}'";
			if($this->isCurrentAccessory($each))
				echo ' selected="selected" ';
			echo ">{$each->title}</option>";
 		}
 		echo "</select>";
	}
	
	protected function selectDelim()
	{
		echo ' ';
	}
	
	public function run()
	{
		echo "</div>";
	}
}
/*
class CategoriesPicker extends categoriesTree
{
	public
		$model,
		$list;
	protected
		$modelClassName;
	public function init()
	{
		$this->modelClassName = get_class($this->model);
		$this->current_id = 1;
		parent::init();
	}
	
	public function each($cat,$i)
	{
		$checked = $this->isChecked($cat) ? 'checked' : '';
		echo "<li>";
		echo "<input type='checkbox' name='{$this->modelClassName}[categories][]' value='{$cat->category_id}' {$checked}>";
		echo str_repeat('&nbsp;',$cat->category_level);
		echo $cat->category_title;
		echo "</li>";
	}
	
	protected function isChecked($cat)
	{
		return 
		!empty($_REQUEST[$this->modelClassName]['categories'])
		&&
		in_array($cat->category_id,$_REQUEST[$this->modelClassName]['categories']);
	}
}
?>
*/