<? abstract class CAdminWidget extends CWidget
{
	abstract protected function getViewData();
	abstract protected function getViewName();
	abstract protected function getModelData($model);
	public function __construct($controller = null)
	{
		if(!is_a($controller,'CAdminController'))
			throw new Exception('CAdminWidget can belongs only for CAdminController');
			
		parent::__construct($controller);
	}

	public function run()
	{
		$this->render($this->getViewName(),$this->getViewData());
	}
	
	protected function getActions()
	{
		return $this->owner->getActions();
	}
	
	protected function getActionLink($model,$action)
	{
		return $this->owner->getActionLink($model,$action);
	}
	
	protected function getOwnerModel()
	{
		return $this->owner->getModel();
	}
	
	protected function getOwnerModelData()
	{
		return $this->getModelData($this->getOwnerModel());
	}
	
	protected function getTitles($modelData)
	{
		$res = array_intersect_key($this->getOwnerModel()->attributeLabels(),$modelData);
		$results = array();
                foreach (array_keys($modelData) as $k => $key) {
                    $results[$k] = isset($res[$key]) ? $res[$key] : '';
                }

                return $results;
	}
	
	protected function getProps($modelData)
	{
		static $res;
		if(is_null($res))
			$res = array_keys($modelData);
		return $res;
	}
	
	public function buildType($title,$prop,$type,$model)
	{
		return new $type($title,$prop,$model);
	}
	
	public function getTypes($model)
	{
		$modelData 	= $this->getModelData($model);
		$props 		= $this->getProps($modelData);
		$titles 	= $this->getTitles($modelData);
		$widget = $this;
                $results = array();
                foreach ($titles as $key => $title) {
                    $results[$key] = $widget->buildType($title,$props[$key],$modelData[$props[$key]],$model);
                }

		return $results;
	}
}
