<? abstract class CListAdminWidget extends CAdminWidget
{
	protected
		$list;
	public function setList($list)
	{
		$this->list = $this->normalizeList($list);
	}
	
	protected function normalizeList($list)
	{
		if(is_array($list))
			return $list;
			
		if($list instanceof IteratorAggregate)
			$list = $list->getIterator();
			
		if($list instanceof ArrayIterator)
			$list = $list->getArrayCopy();
			
		if(!is_array($list))
			throw new Exception(sprintf('Can not normalize list at %s<br />Dump: %s',__METHOD__,print_r($list,1)));
		
		return $list;
	}
}
