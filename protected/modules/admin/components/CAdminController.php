<?php


abstract class CAdminController extends Controller
{

    abstract protected function getModel();

    abstract protected function setModelRequestData($model);

    public function getControllerTitle()
    {
        $class = get_class($this);
        return substr($class, 0, - strlen('controller'));
    }

    public function attributeLabels()
    {
        return $this->getModel()->attributeLabels();
    }

    protected function error404IfNot($bool)
    {
        if (!$bool)
            throw new CHttpException(404);
        return $bool;
    }

    protected function isDataManagmentRequest()
    {
        return $_SERVER['REQUEST_METHOD'] == 'POST';
    }

    protected function getRequestedModelId()
    {
        return (int) $this->getRequest()->getParam('id');
    }

    public function getAddModel()
    {
        $class = get_class($this->getModel());
        $model = new $class;
        if ($this->isDataManagmentRequest())
            $this->manageModelData($model);
        return $model;
    }

    public function getEditModel()
    {
        $model = $this->getModel()->findByPk($this->getRequestedModelId());
        $this->error404IfNot(!is_null($model));
        if ($this->isDataManagmentRequest())
            $this->manageModelData($model, 'Данные успешно сохранены');
        return $model;
    }

    protected function manageModelData($model, $okMsg = 'Данные успешно добавлены', $url = null)
    {
        $this->setModelRequestData($model);
        if ($model->save())
        {
            $this->setOkMsg($okMsg);
            if ($url)
                $this->redirect($url);
            else
                $this->refresh();
        }
        else
        {
            $this->setErrorMsg($this->getModelErrorsHtml($model));
        }
    }

    protected function getModelErrorsHtml($model)
    {
        return implode("<br />", call_user_func_array('array_merge', $model->getErrors()));
    }

    protected function setErrorMsg($msg)
    {
        Yii::app()->user->setFlash('error', $msg);
    }

    protected function setOkMsg($msg)
    {
        Yii::app()->user->setFlash('ok', $msg);
    }

    protected function getListModel()
    {
        return $this->getModel();
    }

    public function actionIndex()
    {
        if (isset($_POST['category_orders']))
        {
            echo '<pre>';
            print_r($_POST['category_orders']); die;
            Category::model()->updateOrders($_POST['category_orders']);
        }

        $list = new ModelPager($this->getListModel());
        $text = $this->getRenderedWidget('TableView', array('list' => $list));
        $text .= $this->getRenderedWidget('AdminPagerWidget', array('pager' => $list));
        $this->renderText($text);
    }

    public function actionEdit()
    {
        $model = $this->getEditModel();
        $this->renderWidget('FormView', array('model' => $model));
    }

    public function actionAdd()
    {
        $this->renderWidget('FormView', array('model' => $this->getAddModel()));
    }

    public function actionDelete()
    {
        $this->getEditModel()->delete();
        $this->setOkMsg('Deleted');
        $this->redirect($_SERVER['HTTP_REFERER']);
    }

    public function getModelActions()
    {
        return array('edit', 'delete');
    }

    public function getControllerActions()
    {
        return array('Add');
    }

    protected function stringMethodDispatcher($prefix, $string, $data, $defaultMethodPrefix = 'default')
    {
        $method = $prefix . $string;
        if (method_exists($this, $method))
            return call_user_func_array(array($this, $method), $data);
        return call_user_func_array(array($this, $defaultMethodPrefix . $prefix), $data);
    }

    public function getActions()
    {
        return array('edit', 'delete');
    }

    protected function defaultGetActionLink($model, $action)
    {
        $url = $this->createUrl($action, array('id' => $model->id));
        $title = $action;
        return "<a href='{$url}'>[{$title}]</a>";
    }

    protected function getActionLinkDelete($model, $action)
    {
        $url = $this->createUrl($action, array('id' => $model->id));
        $title = $action;
        $ausure = "Are you sure";
        return "<a href='{$url}' onclick='return confirm(\"{$ausure}\");'>[{$title}]</a>";
    }

    public function getActionLink($model, $action)
    {
        return $this->stringMethodDispatcher(__FUNCTION__, $action, array($model, $action));
    }

    protected function getRenderedWidget($class, $data)
    {
        $w = new $class($this);
        foreach ($data as $k => $v)
        {
            $w->$k = $v;
        }

        ob_start();
        ob_clean();
        $w->init();
        $w->run();
        return ob_get_clean();
    }

    protected function renderWidget($class, $data)
    {
        $this->renderText($this->getRenderedWidget($class, $data));
    }

    public function getControllerActionsLinks()
    {
        $controller = $this;
        $results = '';
        foreach ($this->getControllerActions() as $action) {
            $id = $controller->id;
            $action = strtolower($action);
            $url = $controller->createUrl("{$id}/{$action}");
            $title = ucfirst($action);
            $results .= " <a class='btn' href='{$url}'>{$title}</a> ";
        }
        return '<div class="well">'.$results.'</div>';
    }

}
