<? class AdminUserType
{
	static public function getUsersTypeHash()
	{
		return array('1' => AdminUserType::getTitle(),'2' => ClerkUserType::getTitle());
	}
	
	static public function Factory(AdminUser $model)
	{
		if($model->type == 1)
			return new self($model);
		if($model->type == 2)
			return new ClerkUserType($model);
	}
	
	protected 
		$model;
	public function __construct(AdminUser $model)
	{
		$this->model = $model;
	}
	
	public function isAccessGranted()
	{
		return true;
	}
	
	public function isIHasPermsToGetRoute($route)
	{
		return true;
	}
	
	static public function getTitle()
	{
		return 'Super user';
	}
}

class ClerkUserType extends AdminUserType
{
	public function isAccessGranted()
	{
		return $this->isOrderSection() || $this->isMemberSection();
	}
	
	private function isOrderSection()
	{
		return strcasecmp(Yii::app()->controller->id,'order') == 0;
	}
	
	private function isMemberSection()
	{
		return strcasecmp(Yii::app()->controller->id,'member') == 0;
	}
	
	public function isIHasPermsToGetRoute($route)
	{
		$route = preg_replace('!(/|admin/)!i','',$route);
		return preg_match('/^(order|memeber)/',$route);
	}
	
	static public function getTitle()
	{
		return 'Clerk';
	}
}