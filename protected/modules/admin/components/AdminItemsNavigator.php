<? class AdminItemsNavigator
{
	protected
		$accessory_id,
		$model_id,
		$manufacture_id;
	public function __construct()
	{
		$this->checkRequestVars();
		$this->setVars();
	}
	
	protected function normalizeArgs($after)
	{
		//function normalize vars dependency hierarchy
		$args = $this->getUserFilterVars();
		while($arg = array_shift($args) and $arg != $after)
		{
			//dummy
		}
		foreach($args as $each)
			$this->setSessionUserVar($each,'');
	}
	
	
	protected function getUserFilterVars()
	{
		return array('manufacture_id','accessory_id','model_id'); //order is improtant!
	}
	
	protected function isCorrectValue($var,$val)
	{
		return is_numeric($val) || empty($val);
	}
	
	protected function checkRequestVars()
	{
		foreach($this->getUserFilterVars() as $var)
		{
			$val = Yii::app()->request->getParam($var);
			if($this->isCorrectValue($var,$val))
			{
				$this->setSessionUserVar($var,$val);
				$this->normalizeArgs($var);
			}
		}
	}
	
	private function setSessionUserVar($var,$val)
	{
		$_SESSION[get_class($this)][$var] = $val;
	}
	
	private function getSessionUserVar($var)
	{
		return $_SESSION[get_class($this)][$var];
	}
	
	protected function setVars()
	{
		foreach($this->getUserFilterVars() as $var)
			$this->$var = $this->getSessionUserVar($var);
	}
	
	
	public function getAccessoryId()
	{
		return $this->accessory_id;
	}
	
	public function getModelId()
	{
		return $this->model_id;
	}
	
	public function getManufactureId()
	{
		return $this->manufacture_id;	
	}
	
	public function getAttributesArray()
	{
		return array_filter(array_intersect_key(get_object_vars($this),array_flip($this->getUserFilterVars())));
	}
	
}
