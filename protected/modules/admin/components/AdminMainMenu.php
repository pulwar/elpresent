<? 
class AdminMainMenu extends CWidget
{

    protected $list;

    public function init()
    {
        $this->list = $this->getList();
        //$list = array_filter($list, array($this, 'isCurrentUserHasPerms'));
        //array_walk($list, array($this, 'each'));
        return $this;
    }

    protected function isCurrentUserHasPerms($route)
    {
        static $model;
        if (is_null($model))
            $model = User::model()->findByPk(Yii::app()->user->getId());
        return $model->user_role == 'admin';
    }

    public function run()
    {
        $this->render('navigation', array('list'=>$this->list, 'current'=>$this->getCurrentRoute()));
    }

    private function getList()
    {
        return array
        (
            'Заказы' => array(
                'Список заказов' => '/admin/order',
                'Список заявок на оформление заказа' => '/admin/order/requests_list',
            ),
            'Подарки' => array(
                'Впечатления' => '/admin/item',
                'Цветы' => '/admin/flowers',
                'Открытки' => '/admin/cards',
                'Очистка номера карты' => '/admin/order/code',
                'Скидки' => '/admin/discount',

            ),
            'Разное' => array(
                'Партнеры' => '/admin/partner',
                'Рассылки'=> '/admin/post',
                'Тизеры'=> '/admin/teasers',
                'Города'=> '/admin/city',
                'Емейлы заказов' => 'admin/buyers',
                'Карты' => '/admin/wherebuy',
            ),
            'Контент' => array(
                'Акции' => '/admin/actions',
                //'Новости' => '/admin/news',
                'Страницы' => '/admin/text',
                'Отзывы' => '/admin/comments',
                'Faq' => '/admin/faq',
                'Отзывы о сайте' => '/admin/contact',
                'Галлерея' => '/admin/gallery',
				'Анонсер' => '/admin/anoncer'
            ),
            'Статистика' => array(
                'Отчеты' => '/admin/reports',
            ),
            'Настройки' => array(
                'Настройки' => '/admin/settings',
                'SEO текст' => '/admin/settings/seo',
                'Категории' => '/admin/category',
                'Подкатегории' => '/admin/subcategory',
                'Исключение суффикса из категорий' => '/admin/categorytitle',
                'Сортировка' => '/admin/category/order',
                'Характеристики' => '/admin/features',
                'Пользователи' => '/admin/user',
                'Новый функционал' => '/admin/user/feature_switch',
            ),
            'Баннеры' => '/admin/slider',
            'Landing page' => '/admin/landing',
            'Скидки' => '/admin/skidki',
        );
    }

    public function each($route, $title)
    {
        if ($this->isCurrent($route))
            echo $this->currentView($title, $route);
        else
            echo $this->regularView($title, $route);
    }

    public function isCurrent($route)
    {
        $cRoute = Yii::app()->urlManager->parseUrl(Yii::app()->getRequest());
        return $cRoute == substr($route, 1);
    }

    public function getCurrentRoute() {
        return Yii::app()->urlManager->parseUrl(Yii::app()->getRequest());
    }

    public function getUrl($route)
    {
        return Yii::app()->urlManager->createUrl($route);
    }

    public function currentView($title, $route)
    {
        $url = $this->getUrl($route);
        return "<a class='current' href='{$url}' class='current'>{$title}</a>";
    }

    public function regularView($title, $route)
    {
        $url = $this->getUrl($route);
        return "<a href='{$url}'>{$title}</a>";
    }
}
?>
