<?php

class ItemCategoriesPicker extends AdminItemsFilter
{	
	public	$item;
		
	protected function isCurrent($category)
	{
		return $this->item->category_id == $category->id;
	}
}