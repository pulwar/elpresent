<? class FormView extends CAdminWidget
{
	public
		$model;
	
	public $features;
    public $type = '';
		
	public function init()
	{
		
	}
	
	protected function getViewName()
	{
		return 'FormView';
	}
	
	protected function getViewData()
	{
		return array('data' => $this->getTypes($this->model), 'features' => $this->features);
	}
	
	public function getModelData($model)
	{
		return $model->getFormData();
	}
}

