<? class TableView extends CListAdminWidget
{
	public function init()
	{
		
	}
	
	protected function getViewData()
	{
		$ownerModelData = $this->getModelData($this->owner->getModel());
		return array(
			'list' 		=> array_map(array($this,'getTypes'),$this->list),
			'titles'	=> $this->getTitles($ownerModelData),
			'actions'	=> array_map(array($this,'getActionsLinks'),$this->list),
			'orders'    => $this->getOrders($this->list)
		);
	}
	
	public function getActionsLinks($model)
	{
		$actions = $this->getActions();
		
		$links = array();
		foreach($actions as $each)
			$links[] = $this->getActionLink($model,$each);
		return implode(' ',$links);
	}
	
	public function getOrders($list)
	{
		$orders = array();
		
		foreach ( $list as $l )
		{
			if ( isset($l->order) )
			{
				$orders[] = array('id' => $l->id, 'order' => $l->order);
			}
		}
		
		return $orders;
	}
	
	protected function getViewName()
	{
		return 'TableView';
	}
	
	public function getModelData($model)
	{
		return $model->getViewData();
	}
}
