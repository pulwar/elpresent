<? class AdminTableView extends CWidget
{
	public $list = array();
	public function init()
	{
		if(!$this->owner instanceof CAdminController)
			return;
		$this->normalizeList();
		$this->startTable();
		$this->thead();
		$this->tbody();
		$this->endTable();
	}
	
	protected function normalizeList()
	{
		if(is_array($this->list))
			return;
		if($this->list instanceof IteratorAggregate)
		{
			$this->list = $this->list->getIterator();
		}
		if($this->list instanceof ArrayIterator)
			$this->list = $this->list->getArrayCopy();
			
		if(!is_array($this->list))
			throw new exception('can not normalize AdminTableView::list. <pre>'.print_r($this->list,1).'</pre>');
	}

	public function getActionsLinks($model)
	{
		$actions = $this->owner->getModelActions();
		$links = array();
		foreach($actions as $each)
			$links[] = $this->owner->getActionLink($model,$each);
		return implode(' ',$links);
	}
	
	public function getTHeadHTML()
	{
		return "<tr><th>".implode('</th><th>',array_map('_',$this->owner->attributeLabels())).'</th><th>'._('Actions').'</th></tr>';
	}
	
	public function getTRowHTML($model)
	{
		$attributes = array();
		foreach(array_keys($this->owner->attributeLabels()) as $prop)
			$attributes[] = $model->$prop;
		return '<tr><td>'.implode('</td><td>',$attributes).'</td><td>'.$this->getActionsLinks($model).'</td></tr>';
	}
	
	public function startTable()
	{
		echo '<table class="tableView" id='.get_class($this->owner).'>';
	}
	
	public function endTable()
	{
		echo '</table>';
	}
	
	public function thead()
	{
		echo '<thead>';
		echo $this->getTHeadHTML();
		echo '</thead>';
	}
	
	public function tbody()
	{
		echo '<tbody>';
		echo implode("\n",array_map(array($this,'getTRowHTML'),$this->list));
		echo '</tbody>';
	}
}
