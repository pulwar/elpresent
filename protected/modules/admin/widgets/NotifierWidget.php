<? class NotifierWidget extends CWidget
{
	public function init()
	{
		$this->showOk();
		$this->showError();
	}
	
	public function showOk()
	{
		$msg = Yii::app()->user->getFlash('ok');
		if($msg)
		{
            echo '<div class="alert alert-success">' .
    '<button type="button" class="close" data-dismiss="alert">×</button>' .
                $msg .
                '</div>';
		}
	}
	
	public function showError()
	{
		$msg = Yii::app()->user->getFlash('error');
		if($msg)
		{
            echo '<div class="alert alert-error">' .
                '<button type="button" class="close" data-dismiss="alert">×</button>' .
                $msg .
                '</div>';
		}
	}
	
}