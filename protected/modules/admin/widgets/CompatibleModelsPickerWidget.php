<? class CompatibleModelsPickerWidget extends CWidget
{
	public
		$item,
		$selectName = 'compatible[]';
	protected
		$list = array(),
		$comp = array();
	public function init()
	{
		$this->comp = $this->item->getCompatibleModelsIdList();
		$this->list = ModelT::model()->tree()->findAll();
		$this->start();
	}
	
	protected function start()
	{
		echo '<select name="'.$this->selectName.'" class="multiple" multiple="yes">';
		array_walk($this->list,array($this,'each'));
	}
	
	protected
		$prev = null;
	protected function setCurrentLevel($model)
	{
		if(is_null($this->prev))
		{
			$this->prev = $model;
			return;
		}
		if($this->prev != $model)
		{
			echo "</optgroup>";
		}
	}
	
	protected function each($model)
	{
		if($model->level == 1)
		{
			$this->setCurrentLevel($model);
			echo "<optgroup label='{$model->title}'>";
		}
		else
		{
			echo "<option value='{$model->id}'";
			if($this->isSelected($model))
				echo 'selected="selected"';
			echo '>';
			echo $model->title;
			echo "</option>";
		}
	}
	
	public function run()
	{
		$this->setCurrentLevel(null);
		echo '</select>';
	}
	
	
	protected function isSelected($model)
	{
		return in_array($model->id,$this->comp);
	}
}
