<? class adminCategoriesWidget extends CWidget
{
	public function init()
	{
		$categories = Category::model()->tree()->findAll();
		$this->root();
		echo "<ul class='line'>";
		array_walk($categories,array($this,'eachCategory'));
		echo "</ul>";
	}
	
	protected function createPopupUrl($title)
	{
		$args = func_get_args();
		$title = array_shift($args);
		$url = call_user_func_array(array(Yii::app()->urlManager,'createUrl'),$args);
		return "<a href='{$url}' rel='fbox'>{$title}</a>";
	}
	
	protected function createUrl($title)
	{
		$args 	= func_get_args();
		$title  = array_shift($args);
		$url 	= call_user_func_array(array(Yii::app()->urlManager,'createUrl'),$args);
		return "<a href='{$url}'>{$title}</a>";
	}
	
	protected function eachCategory($category)
	{
		if($category->isLastLevel())
			$this->showLastLevel($category);
		else
			$this->showCategory($category);
	}
	
	protected function showCategory($category)
	{
		$offset = $this->level2offset($category->category_level);
		$class = $category->category_level == 1 ? 'class="bold"' : '';
		echo "<li>";
		echo "<div style='padding-left:{$offset}' {$class}><span>";
		echo $category->category_title;
		echo $this->showCategoryLinks($category);
		echo "</span></div>";
		echo "</li>";
	}
	
	protected function showLastLevel($category)
	{
		$this->showCategory($category);
		$fields = $category->custom_fields;
		$level = $category->category_level + 1;
		$offset = $this->level2offset($level);
		foreach($fields as $each)
		{
			echo "<li>";
			echo "<div style='padding-left:{$offset}'>";
			echo "<span>";
			echo $each->custom_field_title;
			$this->showCustomFieldsLinks($each);
			echo "</span>";
			echo "</div>";
			echo "</li>";
		}
	}
	
	protected function showCustomFieldsLinks($each)
	{
		echo $this->createPopupUrl('[Edit custom field]','admin/customField/edit',array('custom_field_id' => $each->custom_field_id));
		echo $this->createUrl('[Delete custom field]','admin/customField/delete',array('custom_field_id' => $each->custom_field_id));
	}
	
	protected function level2offset($level)
	{
		return ($level*10)."px"; 
	}
	
	protected function showCategoryLinks($category)
	{
		$method = $this->categoryToShowLinksMethod($category);
		return call_user_func($method,$category);
	}
	
	protected function categoryToShowLinksMethod($category)
	{
		if($category->isLastLevel())
			return array($this,'showCategoryLinksLastLevel');
			
		return array($this,'showCategoryLinksRegular');
	}
	
	private function getCategoryUrls($category)
	{
		$nextLevelAlias = $category->getLevelAlias($category->category_level + 1);
		$nextLevelAlias = strtolower($nextLevelAlias);
		$curLevelAlias	= $category->getLevelAlias();
		$curLevelAlias  = strtolower($curLevelAlias);
		$add 		= $this->createPopupUrl("[Add {$nextLevelAlias}]",'admin/category/add',array('pid' => $category->category_id));
		$delete 	= $this->createPopupUrl("[Delete {$curLevelAlias}]",'admin/category/delete',array('category_id' => $category->category_id));
		$edit		= $this->createPopupUrl("[Edit {$curLevelAlias}]",'admin/category/edit',array('category_id' => $category->category_id));
		return compact('add','edit','delete');
	}
	
	private function root()
	{
		echo $this->createPopupUrl('[Add manufature]','admin/category/add',array('pid' => 1));
		echo "<br/><br/>";
	}
	
	private function showCategoryLinksRegular($category)
	{
		$urls = $this->getCategoryUrls($category);
		echo implode(" ",$urls);
	}
	
	private function showCategoryLinksLastLevel($category)
	{
		$urls = $this->getCategoryUrls($category);
		$urls['add'] = $this->createPopupUrl('[Add custom field]','admin/customField/add',array('category_id' => $category->category_id));
		echo implode(' ',$urls);
	}
}
