
<a href="#modal" role="button" class="btn" data-toggle="modal">Изменить оформление подарка</a>

<div id="modal" class="modal hide fade" style="height: 569px;">
	<div class="modal-header" style="height: 17px;">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h6>Новое оформление подарка</h6>
	</div>
	<div class="modal-body" style="max-height: 547px;">

            <div id="alert-ok" class="alert alert-info hide" style="position: absolute; z-index: 9999; left: 20%; top: 50%">
                <strong>Сообщение: </strong> Изменения сохранены.
            </div>

            <div id="alert-error" class="alert alert-error hide" style="position: absolute; z-index: 9999; left: 20%; top: 50%">
                <strong>Ошибка: </strong> Данные не сохранены.
            </div>

            <ul class="nav nav-tabs" id="myTab">
                <li class="active"><a href="#pack">Упаковка</a></li>
                <li><a href="#flower">Цветы</a></li>
                <li><a href="#card">Открытка</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="pack">
                     <div class="left-block">
                         <img id="texture-container" class="texture" src="<?php echo ($oOrder->texture) ? "/images/".str_replace('.png', '.jpg', $oOrder->texture):"/images/box-01.jpg"; ?>" alt=""/>
                         <img id="bow-container" class="bow" src="<?php echo ($oOrder->bow) ? "/images/".$oOrder->bow:""; ?>" alt=""/>
                    </div>

                    <div class="right-block">

                <div class="well">
                <h4>Текстура:</h4>
                <div class="select-box-bg">
                   <div id="myCarousel1" class="carousel slide">

                        <!-- Carousel items -->
                        <div class="carousel-inner" style="width: 150px;">
                            <?
                                $first = true;

                                if ($oOrder->texture)
                                    $first = false;
                            ?>

                            <? for ($i = 1; $i <= 12; $i++) { ?>
                            <div class="item<? if ($first || $oOrder->texture == 'box-bg-'.sprintf("%02d", $i).'.jpg') {$first = false; echo ' active';}?>"><div class="box-select-img ex-box-bg-<? echo sprintf("%02d", $i);?> box-item"></div></div>
                            <? } ?>
                        </div>
                        <!-- Carousel nav -->
                        <a class="carousel-control left" style="left: 0; top: 0;margin-top: -1px; border-radius: 0; width: 20px; height: 50px; padding-top: 10px;" href="#myCarousel1" data-slide="prev">&lsaquo;</a>
                        <a class="carousel-control right" style="left: 130px; top: 0;margin-top: -1px; border-radius: 0; width: 20px; height: 50px; padding-top: 10px;" href="#myCarousel1" data-slide="next">&rsaquo;</a>
                    </div>
                </div>
                </div>

                <button class="btn btn-small btn-primary" id="show-bow" type="button">Добавить ленту</button>
                <div class="well" id="bow">
                <h4>Лента:</h4>
                <div class="select-box-bg">
                   <div id="myCarousel2" class="carousel slide">

                        <!-- Carousel items -->
                        <div class="carousel-inner" style="width: 80px;">
                            <div class="active item"><div class="box-select-img ex-bow-bg-01 bow-item"></div></div>
                            <div class="item"><div class="box-select-img ex-bow-bg-02 bow-item"></div></div>
                            <div class="item"><div class="box-select-img ex-bow-bg-03 bow-item"></div></div>
                            <div class="item"><div class="box-select-img ex-bow-bg-04 bow-item"></div></div>
                            <div class="item"><div class="box-select-img ex-bow-bg-05 bow-item"></div></div>
                            <div class="item"><div class="box-select-img ex-bow-bg-06 bow-item"></div></div>
                        </div>
                        <!-- Carousel nav -->
                        <a class="carousel-control left" style="left: 0; top: 0;margin-top: -1px; border-radius: 0; width: 20px; height: 50px; padding-top: 10px;" href="#myCarousel2" data-slide="prev">&lsaquo;</a>
                        <a class="carousel-control right" style="left: 60px; top: 0;margin-top: -1px; border-radius: 0; width: 20px; height: 50px; padding-top: 10px;" href="#myCarousel2" data-slide="next">&rsaquo;</a>
                    </div>
                </div>
                </div>

            </div>

                </div>

                <div class="tab-pane" id="flower">
                    <div class="left-block-flower">
                        <? $oFlowersData = $oFlowers->getData(); $first = true;?>
                        <? $flowerImage = '';
                           $flowerPrice = 0;

                           foreach ( $oFlowersData as $fk => $oFlower ) {
                            if ($oFlower->flower_id == $oOrder->flower_id ) {
                                $flowerImage = '/upload/flowers/'.$oFlower->flower_image.'_297.png';
                                $flowerPrice = $oFlower->flower_price;
                            }
                        } ?>
                         <img src="<?= $flowerImage; ?>" alt=""/>
                    </div>

                    <div class="right-block">

                <div class="well">
                <h4>Цветы:</h4>
                <div class="select-box-bg">
                   <div id="myCarousel3" class="carousel slide">

                        <!-- Carousel items -->
                        <div class="carousel-inner" style="width: 85px;">
                            <? foreach ( $oFlowersData as $fk => $oFlower ) : ?>
                            <div class="item<? if ($first) {$first = false; echo ' active';}?>">
                                <div class="flower-item box-select-img ex-bow-bg-<?= $oFlower->flower_image; ?>" style="text-align: center; width: 85px;"
                                    flower_title="<?= $oFlower->flower_title; ?>" flower_id="<?= $oFlower->flower_id; ?>" flower_price="<?= $oFlower->flower_price; ?>">
                                    <img  src="/upload/flowers/<?= $oFlower->flower_image; ?>_48.png" width="48" height="45" />
                                </div>
                            </div>
                            <? endforeach; ?>
                        </div>
                        <!-- Carousel nav -->
                        <a class="carousel-control left" style="left: 0; top: 0;margin-top: -1px; border-radius: 0; width: 20px; height: 50px; padding-top: 10px;" href="#myCarousel3" data-slide="prev">&lsaquo;</a>
                        <a class="carousel-control right" style="left: 65px; top: 0;margin-top: -1px; border-radius: 0; width: 20px; height: 50px; padding-top: 10px;" href="#myCarousel3" data-slide="next">&rsaquo;</a>
                    </div>
                </div>

                <? if ($flowerPrice) {?>
                    <a id="remove-flower" onClick="removeBtnFlower();" class="btn btn-warning" href="#">Убрать цветы</a>
                <? } ?>
                </div>

            </div>
                </div>

                <div class="tab-pane" id="card">

                    <div class="left-block-card">
                        <? $oCardsData = $oCards->getData();?>
                        <? $cardImage = '';
                           $cardPrice = 0;

                           foreach ( $oCardsData as $ck => $oCard ) {
                            if ($oCard->card_id == $oOrder->card_id ) {
                                $cardImage = '/upload/cards/'.$oCard->card_image.'_297.png';
                                $cardPrice = $oCard->card_price;
                            }
                        } ?>
                         <img src="<?= $cardImage; ?>" alt=""/>
                    </div>

                    <div class="right-block">

                <div class="well">
                <h4>Открытка:</h4>
                <div class="select-box-bg">
                   <div id="myCarousel4" class="carousel slide">
                        <? $first = true;?>
                        <!-- Carousel items -->
                        <div class="carousel-inner" style="width: 85px;">
                            <? foreach ( $oCardsData as $ck => $oCard ) : ?>
                            <div class="item<? if ($first) {$first = false; echo ' active';}?>">
                                <div class="card-item box-select-img ex-bow-bg-<?= $oCard->card_image; ?>" style="text-align: center; width: 85px; height: 100px;"
                                     card_title="<?= $oCard->card_title; ?>" card_id="<?= $oCard->card_id; ?>" card_price="<?= $oCard->card_price; ?>">
                                    <img  src="/upload/cards/<?= $oCard->card_image; ?>_48.png" width="48" height="45" />
                                </div>
                            </div>
                            <? endforeach; ?>
                        </div>
                        <!-- Carousel nav -->
                        <a class="carousel-control left" style="left: 0; top: 0;margin-top: -1px; border-radius: 0; width: 20px; height: 92px; padding-top: 10px;" href="#myCarousel4" data-slide="prev">&lsaquo;</a>
                        <a class="carousel-control right" style="left: 65px; top: 0;margin-top: -1px; border-radius: 0; width: 20px; height: 92px; padding-top: 10px;" href="#myCarousel4" data-slide="next">&rsaquo;</a>
                    </div>
                </div>

                <? if ($cardPrice) {?>
                    <a id="remove-card" onClick="removeBtnCard();" class="btn btn-warning" href="#">Убрать открытку</a>
                <? } ?>
                </div>

            </div>

                </div>
            </div>
            <div class="clear"></div>

           <div>
               <div class="well" style="float: left;">
                    	<span class="label label-info">Подарок</span> <br />
                        <span id="price-box"><?= $oItem->getRoundedPrice(); ?></span> руб.
               </div>
               <div class="well" style="float: left;">
                    	<span class="label label-info">Цветы</span> <br />
                        <span id="price-flowers"><?= $flowerPrice;?></span> руб.
               </div>
               <div class="well" style="float: left;">
                    	<span class="label label-info">Открытка</span> <br />
                        <span id="price-card"><?= $cardPrice;?></span> руб.
               </div>
               <div class="well" style="float: left;">
                    	<span class="label label-info">Всего</span> <br />
                        <span id="price-all"><?= ($oItem->getRoundedPrice() + $flowerPrice + $cardPrice); ?></span> руб.
               </div>
           </div>

            <div class="clear"></div>

            <div class="well">
                    <input type="hidden" id="order_id" name="OrderPackId" value="<?= $order_id; ?>"/>
                    <input type="hidden" id="order_texture" name="OrderPackTexture" value="<?= $oOrder->texture; ?>"/>
                    <input type="hidden" id="order_bow" name="OrderPackBow" value="<?= $oOrder->bow; ?>"/>
                    <input type="hidden" id="order_flower_id" name="OrderPackFlowerId" value="<?= $oOrder->flower_id; ?>"/>
                    <input type="hidden" id="order_flower_title" name="OrderPackFlowerTitle" value=""/>
                    <input type="hidden" id="order_card_title" name="OrderPackCardTitle" value=""/>
                    <input type="hidden" id="order_card_id" name="OrderPackCardId" value="<?= $oOrder->card_id; ?>"/>
                    <input type="hidden" id="order_price" name="OrderPackPrice" value="<?= $oItem->getRoundedPrice(); ?>"/>

                    <a id="send-pack-order" class="btn btn-primary">Сохранить оформление товара</a>
            </div>

	</div>
</div>


<script>

function calcPriceOrder() {
        pb = $('#price-box').text();
        pf = $('#price-flowers').text();
        pc = $('#price-card').text();

        sum = parseInt(pb) + parseInt(pf) + parseInt(pc);
        $('#price-all').text(sum);
};

function removeBtnFlower(){
        $('#remove-flower').remove();
        $('#price-flowers').text('0');
        $('.left-block-flower img').attr({'src': ''});
        $('#order_flower_id').val('0');
        $('#order_flower_title').val('');
        calcPriceOrder();
};

function removeBtnCard(){
        $('#remove-card').remove();
        $('#price-card').text('0');
        $('.left-block-card img').attr({'src': ''});
        $('#order_card_id').val('0');
        $('#order_card_title').val('');
        calcPriceOrder();
};

function defaultBox(){
    var textureView = $('#order_texture').val().replace('bg-','');
    if (! textureView) {
            textureView = 'box-01';
            $('#order_texture').val('box-bg-01');
    }

    return textureView;
};

function changePack(){
        var textureView = defaultBox();

        $('#texture-view').attr({'src':'/images/' + textureView + '.jpg'});
        $('#bow-view').attr({'src':'/images/' + $('#order_bow').val() + '.png'});

        if ($('#order_flower_id').val() && $('#order_flower_id').val() != '0') {
            $('#flower-view').html('<div class="control-group card"><div class="control-label">Цветы: </div><div class="controls"><a href="#" style="font-weight: bold;">'+$('#order_flower_title').val()+'</a><br /><span class=" price">'+$('#price-flowers').text()+' Руб.</span><br /><img src="'+  $('.left-block-flower img').attr('src')+'" alt="" /></div></div>');
        } else {
            $('#flower-view').html('');
        }

        if ($('#order_card_id').val() && $('#order_card_id').val() != '0') {
            $('#card-view').html('<div class="control-group card"><div class="control-label">Открытка: </div><div class="controls"><a href="#" style="font-weight: bold;">'+$('#order_card_title').val()+'</a><br /><span class=" price">'+$('#price-card').text()+' Руб.</span><br /><img src="'+  $('.left-block-card img').attr('src')+'" alt="" /></div></div>');
        } else {
            $('#card-view').html('');
        }

        $('#orderInfoPrice').val($('#price-all').text());

};


$(document).ready(function()
{

    if ($('#order_bow').val()) {
        $('#show-bow').text('Убрать ленту');
        $('#bow-container').attr({'src': '/images/' + $('#order_bow').val() + '.png'});
    } else {
        $('#bow').hide();
    }


    $('.carousel').carousel({interval: 2000000});
    $('#myCarousel1').carousel('pause');
    $('#myCarousel2').carousel('pause');
    $('#myCarousel3').carousel('pause');
    $('#myCarousel4').carousel('pause');

    $('.box-item').click(function(){
        num = $(this).attr('class');
        num = num.replace('box-select-img ex-box-bg-','');
        num = num.replace(' box-item','');

        //$('#box-bg').attr({'class': 'conteiner box-bg-' + num});
        $('#texture-container').attr({'src': '/images/box-' + num + '.jpg'});
        $('#order_texture').val('box-bg-' + num);
    });

    $('.bow-item').click(function(){
        num = $(this).attr('class');
        num = num.replace('box-select-img ex-bow-bg-','');
        num = num.replace(' bow-item','');

        //$('#bow-bg').attr({'class': 'conteiner bow-bg-' + num});
        $('#bow-container').attr({'src': '/images/bow-bg-' + num + '.png'});
        $('#order_bow').val('bow-bg-' + num);
    });

    $('.flower-item').click(function(){
        num = $(this).attr('class');
        num = num.replace('flower-item box-select-img ex-bow-bg-','');
        if ($('#price-flowers').text() == '0') {
            $('#flower .select-box-bg').append('<a id="remove-flower" onClick="removeBtnFlower();" class="btn btn-warning" href="#">Убрать цветы</a>');
        }

        $('#price-flowers').text($(this).attr('flower_price'));

        calcPriceOrder();
        $('.left-block-flower img').attr({'src': '/upload/flowers/' + num + '_297.png'});

        $('#order_flower_id').val($(this).attr('flower_id'));
        $('#order_flower_title').val($(this).attr('flower_title'));
    });

    $('.card-item').click(function(){
        num = $(this).attr('class');
        num = num.replace('card-item box-select-img ex-bow-bg-','');
        if ($('#price-card').text() == '0') {
            $('#card .select-box-bg').append('<a id="remove-card" onClick="removeBtnCard();" class="btn btn-warning" href="#">Убрать открытку</a>');
        }

        $('#price-card').text($(this).attr('card_price'));

        calcPriceOrder();
        $('.left-block-card img').attr({'src': '/upload/cards/' + num + '_297.png'});

        $('#order_card_id').val($(this).attr('card_id'));
        $('#order_card_title').val($(this).attr('card_title'));
    });

    $('#show-bow').click(function(){

    if ($('#order_bow').val()) {
        $(this).text('Добавить ленту');
        $('#bow').hide();
        //$('.bow').remove();
        $('#bow-container').attr({'src': ''});
        $('#order_bow').val('');
    } else {
        $(this).text('Убрать ленту');
        $('#bow').show();
        //$('.left-block').append('<img id="bow-container" class="bow" src="/images/bow-bg-01.png" alt=""/>');
        $('#bow-container').attr({'src': '/images/bow-bg-01.png'});
        $('#order_bow').val('bow-bg-01');
    }
    });


    $('#send-pack-order').click(function(){
        defaultBox();
        url = $('#formOrderEdit').attr('action');
        $.ajax(
        {
            type    : 'GET',
            url     : url,
            data    : 'sendFormPack=ok&texture='+$('#order_texture').val()+'&bow='+$('#order_bow').val()+'&flower_id='+$('#order_flower_id').val()+'&card_id='+$('#order_card_id').val()+'&price='+$('#price-all').text(),
            dataType: 'json',
            cache   : false,
            success : function(data)
            {
                $("#alert-ok").show(0).delay(2000).hide(0);
                changePack();
		return;
            },
            error : function(){
                $("#alert-error").show(0).delay(2000).hide(0);

                return;
            }
	});
    });

});

</script>