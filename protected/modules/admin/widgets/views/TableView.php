<h1><?= $this->owner->getControllerTitle(); ?> list</h1>
<form action="" method="post">
        <div class="inner">
            <div class="inner-links">
                <?= $this->owner->getControllerActionsLinks(); ?>
            </div>
                <table class="table table table-condensed table-striped" id='<?= get_class($this->owner); ?>'>
                <thead>
                    <tr>
                        <? if ( count($orders) ) : ?>
                        <th class="category-order"><?= '#'; ?></th>
                        <? endif; ?>

                        <? foreach($titles as $title) : ?>
                            <th><?= $title ?></th>
                        <? endforeach; ?>
                        <th><?= 'Действия'; ?></th>
                    </tr>
                </thead>
                <tbody>
                    <? foreach($list as $i => $each) : ?>

                    <tr>
                        <? if ( count($orders) ) : ?>
                        <td class="category-order">
                            <input class="input-mini" type="text" name="category_orders[<?= $orders[$i]['id']; ?>][]" value="<?= $orders[$i]['order']; ?>" />
                        </td>
                        <? endif; ?>

                        <? foreach($each as $type) :?>
                            <td>
                                <?= $type->toHTML(); ?>
                            </td>
                        <? endforeach; ?>
                            <td><?= $actions[$i]; ?></td>
                    </tr>
                    <? endforeach; ?>
                </tbody>
                </table>
        </div>
        <br />
        <? if ( count($orders) ) : ?>
        <div class="form-actions">
            <button class="btn btn-primary" type="submit">Сохранить порядок</button>
        </div>
        <? endif; ?>
 </form>



