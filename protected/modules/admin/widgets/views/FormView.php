<h1><?= $this->owner->getControllerTitle(); ?></h1>
<? $this->widget('NotifierWidget'); ?>
<form action='' method='post' class="<?php $this->type; ?>" enctype='multipart/form-data'>
    <? if ( count($features) ) : ?>
    <? foreach ( $features as $feature ) : ?>
        <div class="control-group">
            <div class="control-label">
                <?= $feature->feature_value; ?>
            </div>
            <div class="controls">
                <? if ( count($feature->feature_values) ) : ?>
                <select name="feature[<?= $feature->feature_id; ?>]">
                    <option value="">Выберите <?= $feature->feature_value; ?></option>
                    <? foreach ( $feature->feature_values as $featureValue ) : ?>
                    <option value="<?= $featureValue->feature_value_id; ?>"><?= $featureValue->feature_value_value; ?></option>
                    <? endforeach; ?>
                </select>
                <? endif; ?>
            </div>
        </div>
        <? endforeach; ?>
    <? endif; ?>

    <? foreach($data as $el): ?>
    <div class="control-label">
        <label><?= $el->getTitle(); ?></label>
    </div>
    <div class="controls">
        <?= $el->toHTML(); ?>
    </div>
    <? endforeach; ?>
    <div class="form-actions">
        <input class="btn btn-primary" type='submit' value="Сохранить" />
    </div>
</form>