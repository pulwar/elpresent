<? $range = $this->getPager()->getDisplayingPagesRange(); ?>
<? if(!empty($range)): ?>
<div class="pagination">
  <ul>
	  <? foreach($range as $page) : ?>
	
	<? if($this->getPager()->getCurrentPage() == $page) : ?>
		<li class="active"><a href="javascript:void(0);"><?= $page ?></a></li>
	<? else : ?>
		<li><a href="<?= $this->getPager()->getPageUrl($page); ?>"><?= $page ?></a></li>
	<? endif; ?>
	
	<? endforeach; ?>
</ul>
</div>
<? endif;?>
