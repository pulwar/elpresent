<? class AdminItemsFilter extends CWidget
{
	public function init()
	{
		$list = Category::model()->findAll();
		echo "<select name='category_id'>";
		echo "<option>"._('Select cagtegory')."</option>";
		array_walk($list,array($this,'each'));
	}
	
	public function run()
	{
		echo '</select>';
	}
	
	protected function each($category)
	{
		if($this->isCurrent($category))
			$this->showCurrent($category);
		else 
			$this->showRegular($category);
	}
	
	protected function showCurrent($category)
	{
		echo "<option value='{$category->id}' selected='selected'>{$category->title}</option>";
	}
	
	protected function showRegular($category)
	{
		echo "<option value='{$category->id}'>{$category->title}</option>";	
	}
	
	protected function isCurrent($category)
	{
		return Yii::app()->request->getParam('category_id') == $category->id;
	}
}
