<? class PackWidget extends CWidget
{
    
    public 
           $itemId,
           $oFlowers,
           $oCards, 
           $oItem,
           $orderId,
           $oOrder; 
           

    public function init()
    {
        $this->itemId = intval($_GET['id']);
        $this->orderId = intval($_GET['id']);
        
        $order = Order::model()->findByPk($this->itemId);
        
        $this->oOrder = $order;
        
        $this->itemId = $order->item_id;
        
	$this->oItem = Item::model()->findByPk($this->itemId);

        // skip packing for super impressions
        if ($this->oItem->type != 'item') {            
            return;
        }                

        $this->oFlowers = new CActiveDataProvider('Flower', array(
            'pagination' => array(
                'pageSize' => 12
            )
        ));

        $this->oCards = new CActiveDataProvider('Card', array(
            'pagination' => array(
                'pageSize' => 12
            )
        ));
    }
    
    public function run()
    {                                
        $this->render('PackView', array
        (
            'order_id' => $this->orderId,
            'item_id' => $this->itemId,//$this->requireVar('item_id'),
            'oFlowers' => $this->oFlowers,
            'oCards' => $this->oCards,
            'oItem' => $this->oItem,
            'oOrder' => $this->oOrder
	));         
         
       
    }
}

