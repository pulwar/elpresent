<?php

class AnoncerController extends Controller
{
    private $_dir;

    public function init()
    {
        $this->_dir = Yii::getPathOfAlias('webroot.assets');
    }


    public function actionIndex()
    {
        $file_path = $this->_dir . '/anoncer.txt';
        $txt = null;

        if (is_file($file_path)) {
            $txt = file_get_contents($file_path);
        }

        $model = new AnoncerForm;

        if (!empty($_POST['AnoncerForm'])) {
            file_put_contents($file_path, $_POST['AnoncerForm']['txt']);
            $this->redirect('/admin/anoncer');
        }

        $this->render('view', array(
            'model' => $model,
            'txt' => $txt
        ));
    }

}
