<? class DiscountController extends CAdminController
{
    public function getModel()
	{
		return Discount::model();
	}
	
    public function getControllerActions()
	{
		return array('add');
	}
	
    public function setModelRequestData($model)
	{
		$model->code     = (string)Yii::app()->request->getParam('code');
	}
	
	public function getActions()
	{
		return array('edit', 'delete');
	}
}
