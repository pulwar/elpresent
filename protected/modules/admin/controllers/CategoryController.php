<? class CategoryController extends CAdminController
{
	protected function setModelRequestData($model)
	{
		$model->title = (string)Yii::app()->request->getParam('title');
        $model->icon = Yii::app()->request->getParam('icon');
        $model->meta_description = Yii::app()->request->getParam('meta_description');
        $model->slug = Yii::app()->request->getParam('slug');
        $model->meta_keywords = Yii::app()->request->getParam('meta_keywords');
        $model->meta_title = (string)Yii::app()->request->getParam('meta_title');
        $model->seo_text = (string)Yii::app()->request->getParam('seo_text');
        $model->visible = (int)Yii::app()->request->getParam('visible');
	}

	public function getModel()
	{
		return Category::model()->onlyRoots();
	}

	public function attributeLabels()
	{
		static $labels;
		if(is_null($labels))
			$labels = array('id' => _('Id'),'title' => _('Title'));
		return $labels;
	}


    public function actionOrder(){

        if(!empty($_POST['sortitems'])){
            foreach($_POST['sortitems'] as $k => $v){
                $model = Category::model()->findByPk($v);
                $model->position = $k;
                $model->save();
            }
            Yii::app()->end();
        }

        $criteria = new CDbCriteria;
        $criteria->condition='visible=:visible';
        $criteria->params=array(':visible'=> 1);
        $criteria->order = 'position ASC';
        $cats = Category::model()->onlyRoots()->findAll($criteria);
        $this->render('order', [
            'cats' => $cats,
        ]);
    }
}
