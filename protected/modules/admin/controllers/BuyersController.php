<?php

class BuyersController extends Controller
{
    public function actionIndex()
    {
		if (isset($_POST['exportAll'])) {
            $ids = (array)$_POST['autoId'];
            $criteria = new CDbCriteria;
            $criteria->select = 'name, lname, phone, adr, email';
            $buyers = Order::model()->findAll($criteria);
            $file = fopen('php://memory', 'w');
            $fileName    = 'test.csv';
            foreach ($buyers as $field) {
                $datas = self::_dataFilter($field);
                fputcsv($file, $datas, ';');
            }
            fseek($file, 0);
            header('Content-Type: text/html; charset=windows-1251');
            header('Content-Type: application/csv');
            header('Content-Disposition: attachement; filename="'.$fileName.'"');
            fpassthru($file);
            die;
        }
		
        if (isset($_POST['autoId'])) {
            $ids = (array)$_POST['autoId'];
            $criteria = new CDbCriteria;
            //$criteria->select = 'name, lname, phone, adr, email, recivier_email';
            $criteria->select = 'name, lname, phone, adr, email';
            $criteria->addInCondition('id', $ids);
            $buyers = Order::model()->findAll($criteria);
            $file = fopen('php://memory', 'w');
            $fileName    = 'test.csv';
            foreach ($buyers as $field) {
                $datas = self::_dataFilter($field);
                fputcsv($file, $datas, ';');
            }
            fseek($file, 0);
            header('Content-Type: text/html; charset=windows-1251');
            header('Content-Type: application/csv');
            header('Content-Disposition: attachement; filename="'.$fileName.'"');
            fpassthru($file);
            die;
            //html::pr($buyers,1);
        }
        $model = new Order('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Buyers']))
            $model->attributes = $_GET['Buyers'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    private static function _dataFilter($field){
        $datas = [
            iconv('UTF-8', 'windows-1251', str_replace(';', ',', trim($field->name))),
            iconv('UTF-8', 'windows-1251', str_replace(';', ',', trim($field->lname))),
            iconv('UTF-8', 'windows-1251', str_replace(';', ',', trim($field->phone))),
            iconv('UTF-8', 'windows-1251', str_replace(';', ',', trim($field->adr))),
            iconv('UTF-8', 'windows-1251', str_replace(';', ',', trim($field->email))),
            //$field->recivier_email
        ];

        return $datas;
    }
}
