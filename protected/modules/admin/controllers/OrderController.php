<?php

class OrderController extends Controller
{
    public function actionIndex()
    {
        $filter = array();

        $criteria = new CDbCriteria();

        $criteria->alias = 'el_order';


        $criteria->join =
            'LEFT JOIN `el_order_items` AS items ON `el_order`.`id` = items.order_id 
             LEFT JOIN `el_partner` AS partner ON items.partner_id = partner.id';

        //$criteria->join .= 'LEFT JOIN `el_partner` ON `el_order_items`.`partner_id` = `el_partner`.`id`';

        // filtering results
        if (!empty($_GET['filter'])) {
            $filter = $_GET['filter'];
            // filter by date
            $dateField = $filter['date']['type'];
            if(!empty($filter['date']['from'])) {
                $date = CDateTimeParser::parse($filter['date']['from'], 'yyyy-MM-dd');
                $criteria->addCondition($dateField . " > '" . date('Y-m-d', $date) . "'" );
            }
            if(!empty($filter['date']['to'])) {
                $date = CDateTimeParser::parse($filter['date']['to'], 'yyyy-MM-dd');
                $criteria->addCondition($dateField . " < '" . date('Y-m-d', $date) . "'" );
            }

            if($filter['delivered'] !== '' && $filter['delivered'] !== null) {
                $criteria->addCondition('delivered='.$filter['delivered']);
            }


            if($filter['is_processed'] !== '' && $filter['is_processed'] !== null) {
                $criteria->addCondition('is_processed='.$filter['is_processed']);
            }

            if($filter['is_canceled'] !== '' && $filter['is_canceled'] !== null) {
                $criteria->addCondition('is_canceled='.$filter['is_canceled']);
            }

            if($filter['is_remote_order'] !== '' && $filter['is_remote_order'] !== null) {
                $criteria->addCondition('is_remote_order='.$filter['is_remote_order']);
            }

            // filter by number
            if(!empty($filter['order_number'])) {
                $criteria->compare('order_number', $filter['order_number'], true);
            }

            // filter by code
            if(!empty($filter['code'])) {
                $orderItems = OrderItems::model()->findAll((new CDbCriteria())->compare('code', $filter['code'], true));
                foreach ($orderItems as $orderItem) {
                    $criteria->compare('order_number', $orderItem->orderObj->order_number, true, 'OR');
                }
            }

            // filter by item_id
            if(!empty($filter['item_id'])) {
                //find item
                $item = Item::model()->findByPk($filter['item_id']);
                if($item) {
                    $filter['item_title'] = $item->title;
                    $criteria->addCondition("item_id = " . intval($filter['item_id']));
                }else{
                    unset($filter['item_id']);
                }
            }




            if(!empty($filter['partner']) && $filter['partner'] != '') {


                $filter['partner'] = htmlspecialchars_decode($filter['partner'], ENT_QUOTES);
                $match = addslashes($filter['partner']);


                $criteria->condition =  'partner.name LIKE :match';
                $criteria->params = array(':match' => "%$match%");

                unset($filter['item_id']);
            }

            // filter by item_title
            if(!empty($filter['item_title'])) {
                //find item                                                                    
                $filter['item_title'] = htmlspecialchars_decode($filter['item_title'], ENT_QUOTES);
                $match = addslashes($filter['item_title']);                        
                
                $q = new CDbCriteria( array(
                    'condition' => "title LIKE :match",
                    'params'    => array(':match' => "%$match%")
                ) );
 
                $items = Item::model()->findAll( $q );

                /** Todo: Used join query later */

                foreach ($items as $item) {
                    foreach ($item->in_orders as $orderItem) {
                        $criteria->compare('order_number', $orderItem->orderObj->order_number, true, 'OR');
                    }
                }

                unset($filter['item_id']);
                $filter['item_title'] = htmlspecialchars($filter['item_title'], ENT_QUOTES);
            }
        }
        // order by default
        $criteria->order = 'id DESC';
        $criteria->distinct = true;

        // partners for select list
        $partners = Partner::getPartnersList();

        $orders = new CActiveDataProvider('Order', array(
             'criteria' => $criteria,
             'pagination' => array(
                 'pageSize'=> 10,
             )
        ));

        $this->render('index', compact('filter', 'orders', 'partners'));
    }

    public function actionManage()
    {
        $fromPage = Yii::app()->request->getParam('page');

        if (!$fromPage) {
            $fromPage = '/admin/order';
        } else {
            $fromPage = '/admin/order/index/Order_page/' . $fromPage . '.html';
        }

        $orderId = intval(Yii::app()->request->getParam('id'));
        $oOrder = Order::model()->findByPk($orderId);
        $this->redirect(path('admin/order', ['filter' => ['order_number' => $oOrder->order_number]]));
    }

    public function actionDelete()
    {
        $orderId = intval(Yii::app()->request->getParam('id'));
        if ($orderId && $oOrder = Order::model()->findByPk($orderId))
        {
            $orderItems = $oOrder->orderItems;
            if ($oOrder->delete())
            {
                foreach ($orderItems as $orderItem) {
                    $orderItem->delete();
                }

                $this->redirect($_SERVER['HTTP_REFERER']);
            }
        }
    }
    public function actionCheckinvoice()
    {
        $orderId = Yii::app()->request->getParam('id');
        $answer = EasyPay::IsInvoicePaid($orderId);
        if($orderId){
            $this->render('invoice',array
                (
                 'answer' => $answer
            ));
        }
    }
    
    public function actionCode()
    {
        $params = array();

        if($_POST['code']){
            $code = array();
            $code['id'] = $_POST['code'];
            $code['message'] = array();
            /** @var OrderItems[] $ordersItems */
            $ordersItems = OrderItems::model()->findAllByAttributes(array('code' => $code['id']));
            if(empty($ordersItems)){
                $code['message'] = array('Для данного индивидуального номера карты не найдено заказов. Данный номер можно использовать');
            }else{
                $oOrderItems =array();
                $dOrderItems = array();
                foreach ($ordersItems as $v){
                    if($v->getIsActivated()){
                        $v->code = NULL;
                        if($v->save()){
                            $dOrderItems[] = $v;
                        }else{
                            $code['message'][] = 'Не удалось очистить значение номера в заказе:'.$v->id;
                        }
                    }else{
                        $oOrderItems[] = $v;
                    }
                }
                if(empty ($oOrderItems) && empty ($code['message'])){
                    $code['message'][] = 'Данный номер можно использовать';
                }
                $params['dOrderItems'] = $dOrderItems;
                $params['oOrderItems'] = $oOrderItems;
            }
            $params['code'] = $code;
        }

        $this->render('code', $params);
    }
	
	public function actionView($id)
	{
		$order = Order::model()->findByPk($id);
		if( !$order )
			throw new CHttpException(404);

		$partners = Partner::getPartnersList();
        
        ksort($partners);
        $this->renderPartial('_view', compact('order', 'partners'));
	}

    protected function sendResponse($data)
    {

        header('Content-type: application/json');
        echo CJSON::encode($data);

        // stupid loggers...
        foreach (Yii::app()->log->routes as $route) {
            if($route instanceof CWebLogRoute) {
                $route->enabled = false; // disable any weblogroutes
            }
        }
        Yii::app()->end();
        die();
    }

    public function actionEdit()
    {
        $id = $this->getRequest()->getParam('id');
        $order = Order::model()->findByPk($id);
        
        if (isset($_GET['sendFormPack'])) {             
            $order->texture = $_GET['texture'];
            $order->bow = $_GET['bow'];
            $order->flower_id = intval($_GET['flower_id']);
            $order->card_id = intval($_GET['card_id']);
            $order->price = $_GET['price'];
        } else {            
            $orderInfo = $_POST['OrderInfo'];

            $order->discount = intval($orderInfo['discount']);
            $order->validity_date = $orderInfo['validity_date'] ? $orderInfo['validity_date'] : null;
            $order->name = $orderInfo['name']; 
            $order->phone = $orderInfo['phone']; 
            $order->email = $orderInfo['email'];
            $order->comment = $orderInfo['comment'];
            $order->message = $orderInfo['message'];
            $order->payment_type_id = intval($orderInfo['payment_type_id']);
            $order->price = $orderInfo['price'];
            $order->saled_price = ceil(($order->price - ($order->discount/100*$order->price))/1000)*1000;
            
            $order->payer_adr = $orderInfo['payer_adr'];
            $order->payer_date = $orderInfo['payer_date'];
            $order->payer_time_from = $orderInfo['payer_time_from'];
            $order->payer_time_to = $orderInfo['payer_time_to'];
            $order->recipient_firstname = $orderInfo['recipient_firstname'];
            $order->recipient_lastname = $orderInfo['recipient_lastname'];
            $order->recipient_phone = $orderInfo['recipient_phone'];
            
            $order->adr = $orderInfo['adr'];
            $order->recipient_address = $orderInfo['recipient_address'];
            $order->gift_date = $orderInfo['gift_date'];
            $order->gift_time_from = $orderInfo['gift_time_from'];
            $order->gift_time_to = $orderInfo['gift_time_to'];            
            $order->recipient_desired_date = $orderInfo['recipient_desired_date'];
            $order->recipient_desired_time_from = $orderInfo['recipient_desired_time_from'];
            $order->recipient_desired_time_to = $orderInfo['recipient_desired_time_to'];
            $order->is_express_delivery_recipient = (int)$orderInfo['is_express_delivery_recipient'];
            
            if ($orderInfo['thematic_delivery_id']) {
                $order->is_thematic_delivery = 1;
                $order->thematic_delivery_price = BuyForm::$aThematicDelivery[$orderInfo['thematic_delivery_id']]['price'];
                $order->thematic_delivery_name = BuyForm::$aThematicDelivery[$orderInfo['thematic_delivery_id']]['title'];
            } else {
                $order->is_thematic_delivery = 0;
                $order->thematic_delivery_price = 0;
                $order->thematic_delivery_name = '';
            }
            if($orderInfo['delivery']) {
                if ($orderInfo['delivery'] == 0) {
                    $order->delivery = 0;
                    $order->is_express_delivery = 0;
                } elseif ($orderInfo['delivery'] == 1) {
                    $order->delivery = 1;
                    $order->is_express_delivery = 0;
                } elseif ($orderInfo['delivery'] == 2) {
                    $order->delivery = 0;
                    $order->is_express_delivery = 1;
                }
            }
            $order->is_paid = (int)$orderInfo['is_paid'];
            //$order->is_safe = (int)$orderInfo['is_safe'];
            $order->sended = $orderInfo['sended']? true : false;
            $order->delivered = $orderInfo['delivered']? true : false;

            $order->is_canceled = $orderInfo['canceled']? true : false;
            $order->is_processed = $orderInfo['processed']? true : false;


            foreach ($order->orderItems as $orderItem) {

                /** @var OrderItems $orderItem */
                $orderItem->recivier_name = $orderInfo['recivier_name']['item_'.$orderItem->id];
                $orderItem->recivier_lname = $orderInfo['recivier_lname']['item_'.$orderItem->id];
                $orderItem->recivier_email = $orderInfo['recivier_email']['item_'.$orderItem->id];
                $orderItem->recivier_phone = $orderInfo['recivier_phone']['item_'.$orderItem->id];
                $orderItem->recivier_activation_date = $orderInfo['recivier_activation_date']['item_'.$orderItem->id];

                $orderItem->partner_price = $orderInfo['partner_price']['item_'.$orderItem->id];
                $orderItem->item_price = $orderInfo['item_price']['item_'.$orderItem->id];
                $orderItem->partner_id = $orderInfo['partner']['item_'.$orderItem->id] == 'null' ? null : $orderInfo['partner']['item_'.$orderItem->id];

                $orderItem->partner_activated = $orderInfo['partner_activated']['item_'.$orderItem->id] ? true : false;
                $partnerActivatedDate = $orderInfo['partner_activated_date']['item_'.$orderItem->id];
                if (!empty($partnerActivatedDate)) {
                    $partnerActivatedDate = CDateTimeParser::parse($partnerActivatedDate, 'yyyy-MM-dd');
                    $orderItem->partner_activated_date = date('Y-m-d', $partnerActivatedDate);
                }

//                if($order->sended == true && empty($orderItem->code)){
//                    $orderItem->code = OrderItems::model()->generateUniqCode(true);
//                }

                if(!empty($_POST['code'])){
                    $orderItem->code = $_POST['code'];
                }
                // опция впечатления
                if(isset($orderInfo['option_id']['item_'.$orderItem->id]) ){
                    $orderItem->option_id = $orderInfo['option_id']['item_'.$orderItem->id];
                }
                // Активированное впечатление
                if(isset($orderInfo['activated_item']['item_'.$orderItem->id])){
                    $orderItem->activated_item = $orderInfo['activated_item']['item_'.$orderItem->id];
                }

            }

            /** @var CHttpRequest $request */
            $request = Yii::app()->request;
            if ($orderItem->itemObj->type == Item::TYPE_GROUP) {
                $orderItem->group_item_price_data = $request->getParam('GroupItem');
            }
        }
        
        if(!$order->validate()) {
            $this->sendResponse(array(
                'success' => false,
                'errors' => $order->getErrors()
            ));
        }

        if($order->save() && $order->saveOrderItems()) {
            if($order->is_paid){
                $order->setPaid()->save();
            }
        }else{
            $this->sendResponse(array(
                'success' => false,
                'errors' => 'unknown'
            ));
        }

        $this->sendResponse(array(
            'success' => true,
        ));
    }

    public function actionRequests_List()
    {
        $list = OrderRequest::model()->with('item')->findAllByAttributes(array('processed' => 0), array('order'=>'date DESC'));
        $this->render('requests-list', compact('list'));
    }

    public function actionMarkAsProcessed()
    {
        $request = $this->getRequest();
        $item = OrderRequest::model()->findByPk($request->getParam('id'));
        if ($item) {
            $item->processed = 1;
            $item->save();
            $this->redirect('/admin/order/requests_list');
        }
    }

	public function actionCheck()
	{
		$ids = [];
		$order_date_start = time() - 60;
		$order_date_finish = time();
		$order_date_start = date('Y-m-d H:i:s', $order_date_start);
		$order_date_finish = date('Y-m-d H:i:s', $order_date_finish);

		$criteria = new CDbCriteria;
		$criteria->select = 'order_number';
		$criteria->addBetweenCondition('order_date', $order_date_start, $order_date_finish);
		$orders = Order::model()->findAll($criteria);

		foreach($orders as $order){
			$ids[] = $order->order_number;
		}

		if(!empty($ids)){
			$status = true;
			$str_ids_hash = md5(implode(',', $ids));
		}else{
			$status = false;
			$str_ids_hash = '';
		}

		$status = empty($ids) ? false : true;

		if(Yii::app()->request->isAjaxRequest){
			echo CJSON::encode(array(
				'status' => $status,
				'ids' => $ids,
				'str_ids_hash' => $str_ids_hash,
			));

			Yii::app()->end();
		}
	}
}