<?php

class LandingController extends Controller
{

    public function actionIndex()
    {

        $model = Landing::model()->find();

        if (isset($_POST['Landing'])) {

            $model->attributes = $_POST['Landing'];

            if ($model->save()) {
                $this->redirect(array('/admin/landing'));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }
}
