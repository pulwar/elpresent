<? class BirthdayController extends CAdminController
{
	public function getModel()
	{
		return HolidayDay::model();
	}
	
	public function setModelRequestData($model)
	{
		$model->setBirthsList((array)Yii::app()->request->getParam('birthsList'));
		$model->date = (string)Yii::app()->request->getParam('date');
	}
	
	public function getActions()
	{
		return array_merge(parent::getActions(),array('holidays'));
	}
	
	public function getActionLinkHolidays($model,$action)
	{
		$url = $this->createUrl('holiday/index',array('day_id' => $model->id));
		$title = _('Редактировать праздники');
		return "<a href='{$url}'>[{$title}]</a>";
	}
}