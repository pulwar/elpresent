<?php

class SkidkiController extends Controller
{

    protected $_dir;
    public function init()
    {
        $path =  Yii::getPathOfAlias('webroot.images') . DIRECTORY_SEPARATOR . 'discounts';
        if(!file_exists($path)){
            mkdir($path, 0755);
        }
        $this->_dir = $path;
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $model = $this->loadModel($id);

        $this->render('view', array(
            'model' => $model,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model = new Discounts('create');

        if (isset($_POST['Discounts'])) {

            //d::pr($_POST['Discounts'],1);

            $model->attributes = $_POST['Discounts'];
            $model = $this->_fileUpload($model);

            if ($model->save()) {
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('create', array(
            'model' => $model
        ));
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        $model->scenario = 'update';

        if (isset($_POST['Discounts'])) {
            $model = $this->_fileUpload($model);
            $model->attributes = $_POST['Discounts'];

            if ($model->save()) {
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    public function actionDelsmall($id)
    {
        $model = $this->loadModel($id);
        $model->filesRemove($model->img_small, $this->_dir);
        $model->img_small = '';

        if ($model->save()) {
            $this->redirect(array('update', 'id' => $model->id));
        }else{
            print_r($model->getErrors());
        }
    }

    public function actionDelbig($id)
    {
        $model = $this->loadModel($id);
        $model->filesRemove($model->img_big, $this->_dir);
        $model->img_big = '';

        if ($model->save()) {
            $this->redirect(array('update', 'id' => $model->id));
        }else{
            print_r($model->getErrors());
        }
    }

    public function actionDelete($id)
    {
        $model = $this->loadModel($id);
        $model->filesRemove($model->img_small, $this->_dir);
        $model->filesRemove($model->img_big, $this->_dir);
        $model->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    public function actionIndex()
    {
        $model = new Discounts('search');
        $model->unsetAttributes(); // clear any default values
        if (isset($_GET['Discounts']))
            $model->attributes = $_GET['Discounts'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function loadModel($id)
    {
        $model = Discounts::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param User $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'user-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    private function _fileUpload($model)
    {
        if ($file = CUploadedFile::getInstance($model, 'img_small')) {
            $ext = pathinfo($file->getName(), PATHINFO_EXTENSION);
            $name_file = 'img_small' . md5(time() . rand(1, 9)) . '.' . $ext;
            $file_path = $this->_dir . DIRECTORY_SEPARATOR . $name_file;
            $file->saveAs($file_path);
            $model->img_small = $name_file;
        }

        if ($file = CUploadedFile::getInstance($model, 'img_big')) {
            $ext = pathinfo($file->getName(), PATHINFO_EXTENSION);
            $name_file = 'img_big' . md5(time() . rand(1, 9)) . '.' . $ext;
            $file_path = $this->_dir . DIRECTORY_SEPARATOR . $name_file;
            $file->saveAs($file_path);
            $model->img_big = $name_file;
        }

        return $model;
    }
}
