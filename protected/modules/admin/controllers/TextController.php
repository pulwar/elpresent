<?php

class TextController extends CAdminController
{
	public function getModel()
	{
		return TextRecord::model();
	}
	
	public function setModelRequestData($model)
	{
		$model->text 	= $_POST['TextRecord']['text'];
		$model->title 	= Yii::app()->request->getParam('title');
		$model->alias 	= Yii::app()->request->getParam('alias');
		$model->enabled 	= Yii::app()->request->getParam('enabled', false);

		$model->meta_title 		= Yii::app()->request->getParam('meta_title');
		$model->meta_description 	= Yii::app()->request->getParam('meta_description');
		$model->meta_keywords 		= Yii::app()->request->getParam('meta_keywords');

	}

	public function getControllerActions()
	{
		return array('add');
	}
	
	public function getActions()
	{
		return array('edit');
	}
}
