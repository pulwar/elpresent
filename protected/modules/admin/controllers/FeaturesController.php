<? 
class FeaturesController extends CController
{
	public function actionManage_Feature()
	{
	    $featureId = Yii::app()->request->getParam('id');
	    
	    if ( !$featureId )
	    {
	        $oFeature = new Feature();
	    }
	    else
	    {
	        $oFeature = Feature::model()->findByPk($featureId);
	    }
	    
	    if ( isset($_POST['Feature']) )
	    {
	        $oFeature->attributes = $_POST['Feature'];

	        if ( $oFeature->validate() )
	        {
	            $oFeature->save();
	            Yii::app()->user->setFlash('feature_message_success', 'Характеристика успешно добавлена.');
	            
	            $this->redirect('/admin/features');
	        }
	    }
	    
		$this->render('manage_feature', array
		(
		    'oFeature' => $oFeature
		));
	}
	
    public function actionManage_Feature_Value()
	{
	    $featureValueId = Yii::app()->request->getParam('id');
	    
	    if ( !$featureValueId )
	    {
	        $oFeatureValue = new FeatureValue();
	    }
	    else
	    {
	        $oFeatureValue = FeatureValue::model()->findByPk($featureValueId);
	    }
	    
	    if ( isset($_POST['FeatureValue']) )
	    {
	        $oFeatureValue->attributes = $_POST['FeatureValue'];

	        if ( $oFeatureValue->validate() )
	        {
	            $oFeatureValue->save();
	            Yii::app()->user->setFlash('feature_message_success', 'Значение характеристики успешно добавлено.');
	            
	            $this->redirect('/admin/features');
	        }
	    }
	    
		$this->render('manage_feature_value', array
		(
		    'oFeatures'     => Feature::model()->findAll(),
		    'oFeatureValue' => $oFeatureValue
		));
	}

	public function actionIndex()
	{
		$this->render('index', array
		(
		    'oFeatures' => Feature::model()->findAll()
		));
	}
}
