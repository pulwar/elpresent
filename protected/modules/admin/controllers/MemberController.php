<? class MemberController extends CController
{
	public function actionIndex()
	{
		$model = User::model()->adminList();
		$model->with('session');
		$list = new ModelPager($model);
		$this->render('list',array('list' => $list));
	}
	
	public function actionSearch()
	{
		$keyword = (string)Yii::app()->request->getParam('keyword');
		$model = User::model()->adminList()->search($keyword);
		$model->with('session');
		$list = new ModelPager($model);
		$this->render('list',array('list' => $list));
	}
}