<?php


class NewsController extends CController
{

    public function actionIndex()
    {
        $oNews = new CActiveDataProvider('News', array
                    (
                    'pagination' => array
                        (
                        'pageSize' => 20
                    )
                ));

        $this->render('index', array
            (
            'oNews' => $oNews
        ));
    }

    public function actionManage()
    {
        $newsId = Yii::app()->request->getParam('id');
        $fromPage = Yii::app()->request->getParam('page');

        $fromPage = $fromPage ? ('/admin/news/index/News_page/' . $fromPage . '.html') : '/admin/news';
        $oNews = $newsId ? News::model()->findByPk($newsId) : new News();

        if (isset($_POST['News']))
        {
            $imagePreviewSaved = $oNews->preview_img;
            $imageFullSaved = $oNews->full_img;

            $oNews->attributes = $_POST['News'];
            $oNews->preview_img = CUploadedFile::getInstance($oNews, 'preview_img');
            $oNews->full_img = CUploadedFile::getInstance($oNews, 'full_img');
            $oNews->date = date('Y-m-d H:i:s');

            if ($oNews->validate())
            {
                Yii::import('application.extensions.image.Image');

                if ($oNews->preview_img)
                {
                    $uniqName = uniqid();
                    $imageName = $uniqName . '.' . $oNews->preview_img->getExtensionName();
                    $pathToImage = Yii::getPathOfAlias('webroot') . '/upload/news/' . $imageName;
                    $oNews->preview_img->saveAs($pathToImage);
                    try
                    {
                        $image = new Image($pathToImage);
                        $image->resize(62,62, Image::AUTO);
                        $image->crop(62,62);
                        $image->save();
                        $oNews->preview_img = $imageName;
                    }
                    catch (Exception $e){}
                }
                else
                {
                    $oNews->preview_img = $imagePreviewSaved;
                }

                if ($oNews->full_img)
                {
                    $imageName = uniqid() . '.' . $oNews->full_img->getExtensionName();
                    $pathToImage = Yii::getPathOfAlias('webroot') . '/upload/news/' . $imageName;
                    $oNews->full_img->saveAs($pathToImage);
                    try
                    {
                        $image = new Image($pathToImage);
                        $image->resize(200,133, $image->height);
                        $image->crop(200,133);
                        $image->save();
                    }
                    catch (Exception $e){}
                    $oNews->full_img = $imageName;
                }
                else
                {
                    $oNews->full_img = $imageFullSaved;
                }

                $oNews->save();

                $this->redirect($fromPage);
            }
        }

        $this->render('manage', array (
            'oNews' => $oNews,
            'fromPage' => $fromPage
        ));
    }

    public function actionDelete()
    {
        $newsId = Yii::app()->request->getParam('id');
        $oNews = News::model()->findByPk($newsId);

        @unlink(Yii::getPathOfAlias('webroot') . '/upload/news/' . $oNews->full_img);
        @unlink(Yii::getPathOfAlias('webroot') . '/upload/news/' . $oNews->preview_img);

        $oNews->delete();

        $this->redirect('/admin/news');
    }

}
