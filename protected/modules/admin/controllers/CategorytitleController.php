<?php
/**
 * @author: Alexander Zhaliazouski <goper@tut.by>
 * @date: 09.05.2016 16:59
 */
class CategorytitleController extends Controller{

    private $_dir;

    public function init()
    {
        $this->_dir = Yii::getPathOfAlias('webroot.assets');
    }

    public function actionIndex(){

        $file_path = $this->_dir . '/categorywithoutsuffix.txt';
        $txt = null;

        if (is_file($file_path)) {
            $txt = file_get_contents($file_path);
        }

        $model = new CategoryWithoutSuffixForm;

        if (!empty($_POST['CategoryWithoutSuffixForm'])) {
            file_put_contents($file_path, $_POST['CategoryWithoutSuffixForm']['txt']);
            $this->redirect('/admin/categorytitle');
        }

        $this->render('view', array(
            'model' => $model,
            'txt' => $txt
        ));
    }
}
