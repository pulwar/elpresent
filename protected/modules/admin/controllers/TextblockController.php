<? class TextblockController extends CController
{
    public function filters() {
        return array(
            'accessControl'
        );
    }
    
    public function accessRules() {
        return array(
            array(
                'deny',
                'actions'	    => array('index', 'save'),
                'expression'    => '!$user->getIsAdmin()'
            )
        );
    }
    
	public function actionIndex()
	{
		$this->render('index',array('list' => TextBlock::model()->findAll()));
	}

	public function actionSave()
	{
		$text_block_id 		= Yii::app()->request->getParam('text_block_id',0) or $this->error404();
		$text_block_text 	= Yii::app()->request->getParam('text_block_text','');
		$model = TextBlock::model()->findByPk($text_block_id);
		$model->text_block_text = $text_block_text;
		$model->save();
		$this->render('save',array('model' => $model));
	}
	
	protected function error404()
	{
		throw new CHttpException(404);
	}

	// -----------------------------------------------------------
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}
