<? 
class PostController extends CController
{

    public function actionIndex()
    {
        $oPosts = new CActiveDataProvider('Post', array(
                    'criteria' => array(
                        'order' => '`schedule` DESC'
                        )));
        $this->render('index', array
            (
            'oPosts' => $oPosts
        ));
    }

    public function actionManage()
    {
        $params = array();
        $postId = Yii::app()->request->getParam('id');
        if ($postId)
        {
            $oPost = Post::model()->findByPk($postId);
        }
        else
        {
            $oPost = new Post();
        }

        $aPost = Yii::app()->request->getParam('aPost');
        if ($aPost && is_array($aPost))
        {
            if ($aPost['send'] > 0)
            {
                $aPost['schedule'] = null;
            }
            $oPost->schedule = $aPost['schedule'];
            $oPost->body = $aPost['body'];
            $oPost->subject = $aPost['subject'];

            $debugMail = $aPost['debug_mail'];
            $debugMail = $debugMail == ""? false:$debugMail;

            if ($oPost->save()){
                Yii::import('application.commands.CalendarEventsCommand');
                CalendarEventsCommand::sendOutPost(false, $debugMail);
                $this->redirect('/admin/post.html');
            }
        }

        if ($oPost)
            $params['oPost'] = $oPost;
        $this->render('manage', $params); //render default
    }

    public function actionEdit()
    {
        $oPost = Post::model()->findByPk($postId);

        $aPost = Yii::app()->request->getParam('aPost');

        $this->savePost($aPost, $oPost);

        $this->render('manage', array());
    }

    public function actionDelete()
    {
        $postId = Yii::app()->request->getParam('id');
        Post::model()->deleteByPk($postId);
        $this->redirect('/admin/post.html');
    }

}