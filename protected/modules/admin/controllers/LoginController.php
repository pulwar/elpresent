<? 
class LoginController extends CController
{

    public
    $defaultAction = 'login',
    $layout = 'login';

    public function actionLogin()
    {
        $form = new AdminLoginForm;
        // collect user input data
        if (isset($_POST['AdminLoginForm']))
        {
            if (!strpos($_POST['AdminLoginForm']['username'], '@'))
            {
                $_POST['AdminLoginForm']['username'] = User::model()->findByAttributes(array('user_login' => $_POST['AdminLoginForm']['username']))->user_email;
            }
            $form->attributes = $_POST['AdminLoginForm'];
            // validate user input and redirect to previous page if valid
            if ($form->validate())
            {
                $this->redirect('/admin');
            }
            //$this->redirect(Yii::app()->user->returnUrl);
        }
        // display the login form
        $this->render('login', array('form' => $form));
    }

}