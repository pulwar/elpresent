<?php


class OptionController extends CController
{

    public function actionIndex()
    {

    }

    public function actionCreate($id)
    {

        $model = $this->loadModel($id);
        $cities = $this->getItemCityList($model);
        $prices_cache = $model->prices_cache;
        //html::pr($cities,1);
        $optionCreateForm = new OptionCreateForm;
        //html::pr($optionCreateForm);
        //html::pr($model,1);

        if(!empty($_POST)){
            //html::pr($_POST);
            $option_name = $_POST['option_name'];
            $option_small_name = $_POST['option_small_name'];
            $option_desc = $_POST['option_desc'];
            $price = $_POST['price'];
            $partner_prices = $_POST['partner_price'];
            $citiy_id = $_POST['citiy_id'];
            $item_id = $id;

            $optionCreateForm->attributes = $_POST;
            //html::pr($optionCreateForm->attributes,1);

            if ($optionCreateForm->validate()){

                $option = new Option;
                $option->name = $option_name;
                $option->small_name = $option_small_name;
                $option->desc = $option_desc;
                $option->save();
                $option_id = Yii::app()->db->getLastInsertId();

                foreach($partner_prices as $partner_price_k => $partner_price_v){
                    $criteria=new CDbCriteria;
                    $criteria->condition='item_id=:item_id  AND option_id=:option_id AND partner_id=:partner_id';
                    $criteria->params=array(':item_id' => $item_id, ':option_id' => 0, ':partner_id' => $partner_price_k);
                    ItemPartnerLink::model()->deleteAll($criteria);
                }

                $criteria=new CDbCriteria;
                $criteria->condition='item_id=:item_id AND city_id=:city_id AND option_id=:option_id';
                $criteria->params=array(':item_id' => $item_id, ':city_id' => $citiy_id, ':option_id' => 0);
                ItemCityLink::model()->deleteAll($criteria);

                foreach($partner_prices as $partner_price_k => $partner_price_v){
                    ItemPartnerLink::saveItemPartnerLinkPrices($item_id, $partner_price_k, $option_id, $partner_price_v);
                }

                ItemCityLink::saveItemCityLinkPrices($item_id, $citiy_id, $option_id, $price);

                if(is_array($prices_cache[$citiy_id])){
                    // добавить цену по опшену
                    $prices_cache[$citiy_id][$option_id] = $price;
                    //$model->prices_cache = $prices_cache;
                }
                else{
                    $prices_cache[$citiy_id]= array($option_id => $price);
                    //$model->prices_cache = $prices_cache;
                }


                $model->prices_cache = $prices_cache;
                $model->save();


                Yii::app()->user->setFlash('create_option_success',  "Опция создана!");
                $this->redirect(array('create', 'id' => $model->id));
            }
        }

        $this->render('create', [
            'model' => $model,
            'cities' => $cities,
            'optionCreateForm' => $optionCreateForm,
        ]);
    }


    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);
        $this->doRedirectIfEmptyOption($model);
        $cities = $this->getItemCityList($model);
        $prices_cache = $model->prices_cache;
        //html::pr($prices_cache,1);
        $optionUpdateForm = new OptionUpdateForm;

        if(!empty($_POST)){
            //html::pr($_POST,1);
            $citiy_id = (int)$_POST['citiy_id'];
            $item_id = $id;
            $options_id = $_POST['option']['option_id'];
            $prices = $_POST['option']['price'];
            $options_name = $_POST['option']['option_name'];
            $options_small_name = $_POST['option']['option_small_name'];
            $options_desc = $_POST['option']['option_desc'];
            $show_in_group_id = $_POST['show_in_group_id'];
            $partners_price = $_POST['partner_price'];

            //html::pr($_POST,1);
            $optionUpdateForm->attributes = $_POST;

            if ($optionUpdateForm->validate()){

                foreach ($options_id as $k => $option_id){
                    // НЕТ опций
                    if(empty($option_id)){

                        //html::pr($partners_price,1);
                        foreach ($partners_price as $partner_id =>  $partner_price) {
                            ItemPartnerLink::updateItemPartnerLinkPrices($item_id, $partner_id, 0, $partner_price);
                        }

                        //html::pr('просто изменение цены');

                        ItemCityLink::updateItemCityLinkPrices($item_id, $citiy_id, 0, $prices[0]);

                        $prices_cache[$citiy_id] = $prices[0];


                    }
                    // ЕСТЬ опции
                    else{
                        //html::pr($partners_price,1);

                        foreach ($partners_price[$option_id] as $partner_id =>  $partner_price) {
                            ItemPartnerLink::updateItemPartnerLinkPrices($item_id, $partner_id, $option_id, $partner_price);
                        }



                        $option = Option::model()->findByPk($option_id);
                        $option->name = $options_name[$k];
                        $option->small_name = $options_small_name[$k];
                        $option->desc = $options_desc[$k];
                        $option->show_in_group = $option_id == $show_in_group_id;
                        $option->save();

                        ItemCityLink::updateItemCityLinkPrices($item_id, $citiy_id, $option_id, $prices[$k]);

                        $prices_cache[$citiy_id][$option_id] = $prices[$k];

                    }
                }
                //html::pr(($prices_cache),1);
                $model->prices_cache = $prices_cache;
                $model->save();
                Yii::app()->user->setFlash('update_option_success',  "Опция изменена!");
                $this->redirect(array('update', 'id' => $model->id));
            }

        }

        //html::pr($model->prices ,1);
        $this->render('update', [
            'model' => $model,
            'cities' => $cities,
            'optionUpdateForm' => $optionUpdateForm,
        ]);
    }


    /**
     * Удаление одной опции у подарка
     *
     * @param $option_id
     * @param $item_id
     * @param $citiy_id
     * @throws CHttpException
     */
    public function actionUnlink($option_id, $item_id, $citiy_id)
    {
        $model = $this->loadModel($item_id);
        $prices_cache = $model->prices_cache;

        if(count($prices_cache[$citiy_id]) < 2){
            $this->redirect(array('delete', 'id' => $model->id));
        }

        Option::model()->findByPk($option_id)->delete();
        $criteria = new CDbCriteria;
        $criteria->condition = 'item_id=:item_id AND option_id=:option_id';
        $criteria->params = array(':item_id' => $item_id, ':option_id' => $option_id);
        ItemPartnerLink::model()->deleteAll($criteria);

        $criteria = new CDbCriteria;
        $criteria->condition = 'item_id=:item_id AND city_id=:city_id AND option_id=:option_id';
        $criteria->params = array(':item_id' => $item_id, ':city_id' => $citiy_id, ':option_id' => $option_id);
        ItemCityLink::model()->deleteAll($criteria);


        unset($prices_cache[$citiy_id][$option_id]);

        $model->prices_cache = $prices_cache;
        $model->save();


        $this->redirect(array('update', 'id' => $model->id));
    }

    /**
     * Удаление всех опций для подарка
     *
     * @param $id
     * @throws CHttpException
     */
    public function actionDelete($id)
    {
        $model = $this->loadModel($id);
        $this->doRedirectIfEmptyOption($model);
        $cities = $this->getItemCityList($model);
        $prices_cache = $model->prices_cache;
        $optionDeleteForm = new OptionDeleteForm;

        if(!empty($_POST)){
            //html::pr($_POST,1);
            $options_id = $_POST['option_id'];
            $citiy_id = $_POST['citiy_id'];
            $item_id = $id;
            $partner_id = $_POST['partner_id'];
            $partner_price = $_POST['partner_price'];
            $price = $_POST['price'];

            $optionDeleteForm->attributes = $_POST;

            if ($optionDeleteForm->validate()){
                foreach($options_id as $option_id){

                    Option::model()->findByPk($option_id)->delete();

                    $criteria = new CDbCriteria;
                    $criteria->condition = 'item_id=:item_id AND option_id=:option_id';
                    $criteria->params = array(':item_id' => $item_id, ':option_id' => $option_id);
                    ItemPartnerLink::model()->deleteAll($criteria);

                    $criteria = new CDbCriteria;
                    $criteria->condition = 'item_id=:item_id AND city_id=:city_id AND option_id=:option_id';
                    $criteria->params = array(':item_id' => $item_id, ':city_id' => $citiy_id, ':option_id' => $option_id);
                    ItemCityLink::model()->deleteAll($criteria);

                    $prices_cache[$citiy_id] = $price;
                }

                //html::pr($prices_cache,1);

                $itemPartnerLink = new ItemPartnerLink;
                $itemPartnerLink->item_id = $item_id;
                $itemPartnerLink->partner_id = $partner_id;
                $itemPartnerLink->option_id = 0;
                $itemPartnerLink->partner_price = $partner_price;
                $itemPartnerLink->enabled = 0;
                $itemPartnerLink->save();

                $itemCityLink = new ItemCityLink;
                $itemCityLink->item_id = $item_id;
                $itemCityLink->city_id = $citiy_id;
                $itemCityLink->option_id = 0;
                $itemCityLink->item_price = $price;
                $itemCityLink->available = 1;
                $itemCityLink->save();

                $model->prices_cache = $prices_cache;
                $model->save();

                Yii::app()->user->setFlash('delete_option_success',  "Опции удалены!");
                $this->redirect(array('delete', 'id' => $model->id));
            }


        }

        $this->render('delete', [
            'model' => $model,
            'cities' => $cities,
            'optionDeleteForm' => $optionDeleteForm,
            'getPartnerListByCityId' => function($city_id){
                return Partner::getPartnerListByCityId($city_id);
             }
        ]);
    }

    /**
     * Перенаправление на стандартную форму, если нет опшенов
     * @param $model
     * @return bool
     */
    private function doRedirectIfEmptyOption($model){
        $is_option = false;
        foreach ($model->prices as $v_price){
            if(!empty($v_price->option->id)){
                $is_option = true;
                breack;
            }
        }

        if(empty($is_option)){
            $this->redirect(array('/admin/item/manage/', 'id' => $model->id));
        }
        else{
            return true;
        }

    }

    /**
     * Города для определенного итема
     * @param $model
     * @return array
     */
    private function getItemCityList($model){
        $cities = City::getCityList();
        $cities_id = [];
        $cities_items = [];
        foreach($model->prices as $price){
            $cities_id[] = $price->city->id;
        }
        $cities_id = array_unique($cities_id);
        foreach($cities as $city){
            if(in_array($city->id, $cities_id)){
                $cities_items[] = $city;
            }
        }
        return $cities_items;
    }

    public function loadModel($id)
    {
        $model = Item::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

}
