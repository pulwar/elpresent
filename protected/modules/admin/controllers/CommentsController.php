<? 
/*class CommentsController extends CAdminController
{
	protected function setModelRequestData($model)
	{
	    $model->comment_id     = (int)Yii::app()->request->getParam('id');
	    $model->sender_name = (string)Yii::app()->request->getParam('sender_name');
	    $model->text        = (string)Yii::app()->request->getParam('text');
	    $model->rating      = (int)Yii::app()->request->getParam('rating');	
	}
	
    public function getControllerActions()
	{
		return array();
	}
	
    public function getListModel()
	{
		$model = $this->getModel();
		
		if ( Yii::app()->request->getParam('comment_id') )
		{
		    $model->getDbCriteria()->addColumnCondition(array('comment_id' => Yii::app()->request->getParam('comment_id')));
		}
		
		return $model; 
	}
	
	public function getModel()
	{
		return Comments::model();
	}
 
}*/
class CommentsController extends CController
{
    public function actionIndex()
    {
        $oComments = new CActiveDataProvider('Comments', array
        (
            'criteria' => array
            (
           //   'comments' => '`cdate` ASC',
              
            ),
            'pagination' => array
            (
              'pageSize' => 10
            ),
            'sort' => array(
                'defaultOrder' => 'cdate DESC'
            )
        ));

        $this->render('index', array
        (
                'oComments' => $oComments
        ));
    }

    public function actionManage()
    {
        $commentId = Yii::app()->request->getParam('id');
        $fromPage = Yii::app()->request->getParam('Comments_page');

        if ( !$fromPage )
        {
                $fromPage = '/admin/comments';
        }
        else
        {
                $fromPage = '/admin/comments/index/Comments_page/'.$fromPage.'.html';
        }

        $commentId = Yii::app()->request->getParam('id');
        $oComments = Comments::model()->findByPk($commentId);

        if ( count($_POST) && array_key_exists('Comments', $_POST) )
        {
            $oComments->attributes = $_POST['Comments'];
            $oComments->save();
            $this->redirect($fromPage);
        }

        $this->render('manage', array
        (
                'oComments'  => $oComments,
                'fromPage' => $fromPage
        ));
    }
    public function actionDelete()
    {
        $commentId = Yii::app()->request->getParam('id');
        $fromPage = Yii::app()->request->getParam('Comments_page');

        if ( !$fromPage )
        {
                $fromPage = '/admin/comments';
        }
        else
        {
                $fromPage = '/admin/comments/index/Comments_page/'.$fromPage.'.html';
        }
        

        Comments::model()->deleteByPk($commentId);

        $this->redirect($fromPage);
    }

    public function actionPublish()
    {
        $commentId = Yii::app()->request->getParam('id');
        $fromPage = Yii::app()->request->getParam('Comments_page');

        $fromPage = '/admin/comments';
        
        Comments::model()->updateByPk($commentId, array('visibility' => 1));

        $this->redirect($fromPage);
    }
}