﻿<? 
class ItemController extends CAdminController
{

    public function attributeLabels()
    {
        static $labels;
        if (is_null($labels))
        {
            $labels = array(
                'id' => _('Id'),
                'title' => _('Title'),
                'desc' => _('Description'),
                'text' => _('Text'),
                'price' => _('Price'),
                'new' => _('New'),
                'special' => _('Special'),
                'available' => _('Available'),
            );
        }
        return $labels;
    }

    public function getModel()
    {
        return Item::model();
    }

    protected function setModelRequestData($model)
    {
        $request = $this->getRequest();
        $model->title = (string) $request->getParam('title');
        $model->category_id = (int) $request->getParam('category_id');
        $model->price = (int) $request->getParam('price');
        $model->desc = (string) $request->getParam('desc');
        $model->text = (string) $request->getParam('text');
        $model->off_price = (string) $request->getParam('off_price');
        $model->off_price = $model->off_price ? $model->off_price:null;
        $model->scenario = (string) $request->getParam('scenario');
        $model->setSpecial((bool) $request->getParam('special'));
        $model->setNew((bool) $request->getParam('new'));
        $model->setMain((bool) $request->getParam('main'));
        $model->available = (bool) $request->getParam('available');

        $model->uploadImages = (array) $_FILES['images'];
        if ($_FILES['preview_img']['error'] == 0)
            $this->updatePreviewImage($model);
        if ($_FILES['full_img']['error'] == 0)
            $this->updateFullImage($model);
    }

    protected function updatePreviewImage($model)
    {
        $wrapper = ImageWrapper::uploadForOwner($_FILES['preview_img'], $model, 'images/item', 'full_img');
        if (!$wrapper)
            return;
        $model->preview_img = $wrapper->getFilepath();
    }

    protected function updateFullImage($model)
    {
        $wrapper = ImageWrapper::uploadForOwner($_FILES['full_img'], $model, 'images/item', 'preview_img');
        if (!$wrapper)
            return;
        $model->full_img = $wrapper->getFilepath();
    }

    protected function getImageModel()
    {
        return ItemImage::model();
    }

    protected function loadImageModel($id)
    {
        $id = (int) $id;
        return $this->getImageModel()->findByPk($id);
    }

    /**
     * @param CActiveRecord $model
     * @param string $okMsg
     * @param null $url
     */
    protected function manageModelData($model, $okMsg = 'Data was added', $url = null)
    {
        $this->setModelRequestData($model);

        if ($model->save())
        {
            $ftrs = $_POST['feature'];
            if (is_array($ftrs) && count($ftrs))
            {
                foreach ($ftrs as $feature)
                {
                    $featureValueItem = new FeatureValueItem();
                    $featureValueItem->item_id = $model->id;
                    $featureValueItem->feature_value_id = $feature;
                    $featureValueItem->save();
                }
            }

            $this->setOkMsg(_($okMsg));
            if ($url)
            {
                $this->redirect($url);
            }
            else
            {
                $this->refresh();
            }
        }
        else
        {
            $this->setErrorMsg($this->getModelErrorsHtml($model));
        }
    }

    public function subActionDeleteImages()
    {
        $ids = (array) $this->getRequest()->getParam('image_id');
        $models = array_filter(array_map(array($this, 'loadImageModel'), $ids));
        foreach ($models as $each)
            $each->delete();
        $this->setOkMsg(_('Images were deleted'));
    }

    public function actionAdd()
    {
        $features = Feature::model()->findAll();

        $this->renderWidget('FormView', array('model' => $this->getAddModel(), 'features' => $features));
    }

    public function actionEdit()
    {
        if (!empty($_REQUEST['image_id']))
            $this->subActionDeleteImages();
        $model = $this->getEditModel();
        $this->renderWidget('FormView', array('model' => $model));
    }

    public function getActions()
    {
        return array_merge(parent::getActions(), array('comments'));
    }

    public function getActionLinkComments($model, $action)
    {
        $url = $this->createUrl('comments/index', array('item_id' => $model->id));
        $title = _('Comments');
        return "<a href='{$url}'>[{$title}]</a>";
    }

    public function actionIndex()
    {
        $itemId = intval($this->getRequest()->getParam('id'));
        $aItemsParams = array
            (
            'pagination' => array
                (
                'pageSize' => 20
            )
        );
        $aFilter = array();
        $aConditions = array();
        $aParams = array();
        $aCriterias = array();
        $is_options = array(
            'is_new' => 'Новый',
            'is_special' => 'Специальный',
            'is_main' => 'На главной',
            'is_weekend' => 'Is Weekend',
            'is_recommend' => 'Рекомендуем',
            'is_catalog' => 'В каталоге',
        );


        if (!empty($_POST['aFilter']))
        {

            $aFilter = $_POST['aFilter'];
            if ($aFilter['category_id'])
            {
                $aConditions[] = 't.category_id=' . $aFilter['category_id'];
            }
            if ($aFilter['price'] && $aFilter['price_operation'])
            {
                $aConditions[] = 'price' . $aFilter['price_operation'] . $aFilter['price'];
            }
            if ($aFilter['text'])
            {
                $aConditions[] = 't.title LIKE "%'.addslashes($aFilter['text']).'%"';
            }
            foreach ($is_options as $key => $option)
            {
                if (isset($aFilter[$key]))
                {
                    $aConditions[] = $key . '=1';
                    $aFilter[$key] = true;
                }
                else
                {
                  //  $aConditions[] = $key . '=0';
                }
            }
            $aItemsParams['pagination']['pageSize'] = 50;
            foreach ($aFilter as $key => $value)
            {
                if (substr($key, 0, 3) == 'is_' && $value = 'on')
                {
                    $aConditions[] = $key . '=1';
                    $aFilter[$key] = true;
                }
            }
        }
        elseif ($_POST['aFilter'])
        {
            foreach ($is_options as $key => $option)
            {
                //$aConditions[] = $key.'=0';
            }
        }
        if ($itemId)
        {
            $aConditions[] = 't.id=' . $itemId;
        }
        if (count($aConditions))
        {
            $aCriterias['condition'] = implode(' AND ', $aConditions);
        }

        if (count($aCriterias))
            $aItemsParams['criteria'] = $aCriterias;
            $aItemsParams['criteria']['with'] = 'subcategory';
        
        $oItems = new CActiveDataProvider('Item', $aItemsParams);
        $oCategories = Category::model()->findAll();
        $oPartners = Partner::model()->findAll();

        $this->render('index', array
            (
            'oItems' => $oItems,
            'oCategories' => $oCategories,
            'oPartners' => $oPartners,
            'aFilter' => $aFilter,
            'is_options' => $is_options,
        ));
    }

    public function actionManage()
    {
        $fromPage = $this->getRequest()->getParam('page');

        if (!$fromPage)
        {
            $fromPage = '/admin/item';
        }
        else
        {
            $fromPage = '/admin/item/index/Item_page/' . $fromPage . '.html';
        }

        $itemId = intval($this->getRequest()->getParam('id'));
        $aItemFeatures = array();
        $features = Feature::model()->findAll();
        $categories = Category::model()->findAll();
        $subcategories = Subcategory::model()->with('category')->findAll(array(
            'order'=>'category_id',
        ));

        if (!$itemId)
        {
            $oItem = new Item();
        }
        else
        {
            $oItem = Item::model()->with('subcategory')->findByPk($itemId);
            $oItemFeaturesValues = FeatureValueItem::model()->findAllByAttributes(array('item_id' => $itemId));

            if ($oItemFeaturesValues)
            {
                foreach ($oItemFeaturesValues as $oItemFeaturesValue)
                {
                    if ($oItemFeaturesValue->feature_value_id)
                    {
                        $oFeatureValue = FeatureValue::model()->findByPk($oItemFeaturesValue->feature_value_id);
                        $aItemFeatures[$oFeatureValue->feature_id][] = $oItemFeaturesValue->feature_value_id;
                    }
                }
            }
        }


        $oPartners = Partner::model()->findAll();

        if (isset($_POST['Item']))
        {
            $imagePreviewSaved = $oItem->preview_img;
            $imageFullSaved = $oItem->full_img;
            
            $oItem->attributes = $_POST['Item'];
            $oItem->price = $_POST['Item']['price'];
            $oItem->partner_price = $_POST['Item']['partner_price'];
            $oItem->partner_id = $_POST['Item']['partner_id'];
            $oItem->preview_img = CUploadedFile::getInstance($oItem, 'preview_img');
            $oItem->full_img = CUploadedFile::getInstance($oItem, 'full_img');
            $oItem->is_new = (bool) $this->getRequest()->getParam('is_new');
            $oItem->off_price = (int) $this->getRequest()->getParam('off_price');
            $oItem->off_price = $_POST['Item']['off_price'] ? (int)$_POST['Item']['off_price']:null;
            $oItem->is_special = (bool) $this->getRequest()->getParam('is_special');
            $oItem->is_in_group = (int) $this->getRequest()->getParam('is_in_group');
            $oItem->is_main = (bool) $this->getRequest()->getParam('is_main');
            $oItem->is_weekend = (bool) $this->getRequest()->getParam('is_weekend');
            $oItem->is_recommend = (bool) $this->getRequest()->getParam('is_recommend');
            $oItem->is_catalog = (bool) $this->getRequest()->getParam('is_catalog');
            $oItem->available = intval($this->getRequest()->getParam('available'));
            
            if ($oItem->validate())
            {
                Yii::import('application.extensions.image.Image');

                if ($oItem->preview_img)
                {
                    $uniqName = uniqid();
                    $imageName = $uniqName . '.' . $oItem->preview_img->getExtensionName();
                    $pathToImage = Yii::getPathOfAlias('webroot') . '/images/item/' . $imageName;
                        $oItem->preview_img->saveAs($pathToImage);
                    $oItem->preview_img = 'images/item/' . $imageName;

                    $image = new Image($pathToImage);
                    $size = 130;
                    if ($image->width > $image->height)
                    {
                        $coeff = $image->width / $image->height;
                        $image->resize($size, round($size / $coeff));
                    }
                    else
                    {
                        $coeff = $image->height / $image->width;
                        $image->resize(round($size / $coeff), $size);
                    }
                    $image->save();
                }
                else
                {
                    $oItem->preview_img = $imagePreviewSaved;
                }

                if ($oItem->full_img)
                {
                    $uniqName = uniqid();
                    $imageName = $uniqName . '.' . $oItem->full_img->getExtensionName();
                    $pathToImage = Yii::getPathOfAlias('webroot') . '/images/item/' . $imageName;
                    $oItem->full_img->saveAs($pathToImage);
                    $oItem->full_img = 'images/item/' . $imageName;

                    $image = new Image($pathToImage);
                    $size = 240;
                    if ($image->width > $image->height)
                    {
                        $coeff = $image->width / $image->height;
                        $image->resize($size, round($size / $coeff));
                    }
                    else
                    {
                        $coeff = $image->height / $image->width;
                        $image->resize(round($size / $coeff), $size);
                    }
                    $image->save();
                }
                else
                {
                    $oItem->full_img = $imageFullSaved;
                }

                $oItem->save();
                
                CategoryItems::addItems($_POST['Category'], $oItem->id);
                SubcategoryItems::addItems($_POST['Subcategory'], $oItem->id);
                
                FeatureValueItem::model()->deleteAllByAttributes(array('item_id' => $oItem->id));

                foreach ($this->getRequest()->getParam('features', array()) as $featureValues)
                {
                    foreach ($featureValues as $featureValue)
                    {
                        $oFeatureValueItem = new FeatureValueItem();
                        $oFeatureValueItem->item_id = $oItem->id;
                        $oFeatureValueItem->feature_value_id = $featureValue;
                        $oFeatureValueItem->save();
                    }
                }

                $this->redirect($fromPage);
            }
        }

        $this->render('manage', array
            (
            'oItem' => $oItem,
            'features' => $features,
            'oSubcategories' => $subcategories,
            'oCategories' => $categories,
            'oPartners' => $oPartners,
            'aItemFeatures' => $aItemFeatures,
            'fromPage' => $fromPage
        ));
    }

    public function actionDelete()
    {
        $itemId = intval($this->getRequest()->getParam('id'));

        $oItem = Item::model()->findByPk($itemId);

        $previewImgPath = Yii::getPathOfAlias('webroot') . $oItem->preview_img;
        $fullImgPath = Yii::getPathOfAlias('webroot') . $oItem->full_img;

        if (file_exists($previewImgPath))
        {
            unlink($previewImgPath);
        }

        if (file_exists($fullImgPath))
        {
            unlink($fullImgPath);
        }

        Item::model()->deleteByPk($itemId);
        SubcategoryItems::model()->deleteAllByAttributes(array('item_id' => $itemId));
        Banners::model()->deleteAllByAttributes(array('item_id' => $itemId));
        MoodItems::model()->deleteAllByAttributes(array('item_id' => $itemId));
        $this->redirect($_SERVER['HTTP_REFERER']);
    }

    public function actionPhotos()
    {
        $itemId = intval($this->getRequest()->getParam('item_id'));
        $oItem = Item::model()->findByPk($itemId);
        $uploaded = false;

        if (count($_POST))
        {
            $aPhoto = $this->getRequest()->getParam('aPhoto');
            foreach ($aPhoto as $id => $params)
            {
                $oItemPhoto = ItemPhoto::model()->findByPk($id);
                $oItemPhoto->moderated = (array_key_exists('mod', $params) && $params['mod'] == 'on') ? 1 : 0;
                $oItemPhoto->comment = $params['comment'];
                $oItemPhoto->order = $params['order'];
                $oItemPhoto->save();
            }
            //die(nl2br(print_r($aPhoto,true)));
        }

        $oCriteria = new CDbCriteria();
        $oCriteria->order = '`order` asc';
        $oCriteria->condition = '`item_id`=' . $itemId;
        $oPhoto = ItemPhoto::model()->findAll($oCriteria);

        $this->render('photos', array(
            'oItem' => $oItem,
            'oPhoto' => $oPhoto,
            'uploaded' => $uploaded
        ));
    }

    public function actionPhoto_delete()
    {
        $photoId = intval($this->getRequest()->getParam('id'));
        ItemPhoto::model()->findByPk($photoId)->delete();

        $this->redirect($_SERVER['HTTP_REFERER']);
    }

    public function actionPhoto_replace()
    {
//        $photoId = intval($this->getRequest()->getParam('id'));
//        $oPhoto = ItemPhoto::model()->findByPk($photoId);
    }

    public function actionPhoto_upload()
    {
        $photoId = intval($this->getRequest()->getParam('id'));
        $comment = $this->getRequest()->getParam('newPhotoComment');
        $item_id = intval($this->getRequest()->getParam('item_id'));
        $oPhoto = ItemPhoto::model()->findByPk($photoId);
        if (count($_FILES))
        {
            foreach ($_FILES as $image)
            {
                $oPhoto = new PhotoWrapper($item_id);
                $oPhoto->prepare($image);
                $filename = $oPhoto->getPath();
                $oPhotoModel = new ItemPhoto('saving');
                $oPhotoModel->filename = $filename;
                $oPhotoModel->item_id = $item_id;
                $oPhotoModel->user_id = Yii::app()->user->getId();
                $oPhotoModel->comment = $comment;

                if ($oPhotoModel->save())
                {
                    $oPhoto->process();
                    $oPhoto->save();
                    $uploaded = true;
                }
            }
        }
        $this->redirect($_SERVER['HTTP_REFERER']);
    }

    public function actionGroupedit()
    {
        if($_POST['id']){
            $item_id = intval($_POST['id']);
            $item = Item::model()->findByPk($item_id);
            if(empty($item)){
                echo json_encode(array('error' => 'Товара с таким ID не существует'));
                exit();
            }
            $item->category_id = $_POST['category_id'];
            $item->price = $_POST['price'];
            $item->partner_price = $_POST['partner_price'];
            $item->off_price = $_POST['off_price'];
            SubcategoryItems::addItems($_POST['Subcategory'], $item_id);
            if($item->save()){
                echo json_encode(array('message' => 'Изменено'));
                exit();
            }else{
                echo json_encode(array('error' => 'При сохранении в базу произошла ошибка'));
                exit();
            }
        }
        $categories = Category::model()->findAll();
        $oSubcategories = Subcategory::model()->with('category')->findAll();
        $items = Item::model()->findAll();
        $this->render('groupedit', array(
            'oItems' => $items,
            'oCategories' => $categories,
            'oSubcategories' => $oSubcategories,
        ));
    }

    public function actionAddbanner()
    {
        $fromPage = $this->getRequest()->getParam('page');

        if (!$fromPage)
        {
            $fromPage = '/admin/item';
        }
        else
        {
            $fromPage = '/admin/item/index/Item_page/' . $fromPage . '.html';
        }
        $itemId = $this->getRequest()->getParam('item_id');
        $banner = Banners::model()->findByAttributes(array('item_id' => $itemId));
        if(!$banner){
            $banner = new Banners();
            $banner->item_id = intval($itemId);
            $item = Item::model()->findByPk(intval($itemId));
            if(!$item){
                $this->redirect($fromPage);
            }
            $banner->item = $item;
            $banner->title = mb_substr($item->title, 0, 30);
            $banner->text = mb_substr($item->desc, 0, 150);
        }
        if (isset($_POST['Banner']))
        {
            $banner->title = $_POST['Banner']['title'];
            $banner->text = $_POST['Banner']['text'];
            if($banner->save()){
                $this->redirect($fromPage);
            }
        }
        
        $this->render('addbanner',array(
            'banner' => $banner,
            'fromPage' => $fromPage,
        ));
    }
    public function actionAllbanners()
    {
        $aItemsParams = array
            (
            'pagination' => array
                (
                'pageSize' => 20
            ),
            'criteria' => array(
                'with' => 'item'
            )
        );
        $banners = new CActiveDataProvider('Banners', $aItemsParams);
        $this->render('allbanners',array(
            'banners' => $banners,
        ));
    }
    public function actionDeletebanner()
    {
        $itemId = $this->getRequest()->getParam('id');
        $banner = Banners::model()->findByPk(intval($itemId));
        $banner->delete();
        $this->redirect('/admin/item/allbanners');
    }

	
}
