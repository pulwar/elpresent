<? class CustomFieldController extends CFboxController
{
	private function requireRequestVar($var,$type = 'int')
	{
		$value = Yii::app()->request->getParam($var);
		settype($value,$type);
		if($value)
			return $value;
			
		throw new CHttpException(404);
	}
	
	private function getCategoryModel()
	{
		static $category;
		if(is_null($category))
		{
			$category_id = $this->requireRequestVar('category_id');
			$category = Category::model()->findByPk($category_id);
		}
		return $category;
	}
	
	public function actionDelete()
	{
		$id = (int)Yii::app()->request->getParam('id',0) or die();
		$model = CustomField::model()->findByPk($id) or die();
		$model->delete();
		$this->refresh();
	}	
	
	public function actionGetCategoryWidget()
	{
		$cid 	= $this->requireRequestVar('category_id');
		$class 	= $this->requireRequestVar('class','string');
		CustomFieldManager::isCorrectClass($class) or die();
		$category = Category::model()->findByPk($cid);
		$field = CustomFieldBuilder::BuildNewInstance($class);
		if(!$field instanceof iHasWidgetAtCategoryAdminPage)
			return;
		$widget = $field->getCategoryAdminPageWidget();
		$widget->init();
		$widget->run();
	}
	
	public function actionAdd()
	{
		if($_SERVER['REQUEST_METHOD'] == 'POST')
			return $this->postActionAdd();
			
		return $this->getActionAdd();
	}
	
	private function postActionAdd()
	{
		$model = new CustomField();
		$model->class 				= (string)Yii::app()->request->getParam('class');
		$model->custom_field_title 	= (string)Yii::app()->request->getParam('custom_field_title');
		$model->category 			= $this->getCategoryModel();
		$model->category_id 		= $model->category->id;
		
		$model->validate() && $model->save(false) && $this->refresh();
		
		$this->errors($model->getErrors());
	}
	
	private function getActionAdd()
	{
		$model = new CustomField();
		$fieldsList 				= CustomFieldManager::getList();
		$model->class 				= array_shift(array_values($fieldsList));
		$model->custom_field_title 	= 'Entry title';
		$model->category 			= $this->getCategoryModel();
		$model->category_id 		= $model->category->id;
		$this->renderPartial('form',array(
			'action' => $this->createUrl('/admin/customField/add',array('category_id' => $model->category_id)),
			'model' => $model
		));
	}
	
	public function actionEdit()
	{
		$model = CustomField::model()->findByPk((int)Yii::app()->request->getParam('custom_field_id'));
		$model->category = Category::model()->findByPk($model->category_id);
		if(!$model)
			throw new CHttpException(404);
		
		if($_SERVER['REQUEST_METHOD'] == 'POST')
			return $this->postActionEdit($model);
		
		return $this->getActionEdit($model);
	}
	
	private function getActionEdit($model)
	{
		$this->renderPartial('form',array(
			'model' => $model,
			'action' => $this->createUrl('/admin/customField/edit',array('custom_field_id' => $model->custom_field_id)
		)));
	}
	
	private function postActionEdit($model) 
	{
		$model->custom_field_title = (string)Yii::app()->request->getParam('custom_field_title');
		if($model->save())
			$this->refresh();
		
		$this->errors($model->getErrors());
	}
}
