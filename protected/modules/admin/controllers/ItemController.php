﻿<?php

class ItemController extends CAdminController
{

    public function attributeLabels()
    {
        static $labels;
        if (is_null($labels)) {
            $labels = array(
                'id' => _('Id'),
                'title' => _('Title'),
                'desc' => _('Description'),
                'text' => _('Text'),
                'price' => _('Price'),
                'new' => _('New'),
                'special' => _('Special'),
                'available' => _('Available'),
            );
        }
        return $labels;
    }

    public function getModel()
    {
        return Item::model();
    }

    // todo: use setAttributes method
    // todo: is this method is used anyhow?
    protected function setModelRequestData($model)
    {
        $request = $this->getRequest();
        $data = $request->getParam('Item', []);
        $model->title = (string)$request->getParam('title');
        $model->category_id = (int)$request->getParam('category_id');
//        $model->price = (int) $request->getParam('price');
        $model->desc = (string)$request->getParam('desc');
        $model->text = (string)$request->getParam('text');
        $model->off_price = (string)$request->getParam('off_price');
        $model->off_price = $model->off_price ? $model->off_price : null;
        $model->instuction_description = (string)$request->getParam('instruction_description');
        $model->scenario = (string)$request->getParam('scenario');
        $model->need_to_know = (string)$request->getParam('need_to_know');
        $model->event_id = empty($data['event']) ? null : $data['event'];
        $model->setSpecial((bool)$request->getParam('special'));
        $model->setNew((bool)$request->getParam('new'));
        $model->setMain((bool)$request->getParam('main'));
        $model->is_hot = (bool)$request->getParam('is_hot');
        $model->available = (bool)$request->getParam('available');

        $model->uploadImages = (array)$_FILES['images'];
        if ($_FILES['preview_img']['error'] == 0) {
            $this->updatePreviewImage($model);
        }
        if ($_FILES['full_img']['error'] == 0) {
            $this->updateFullImage($model);
        }
    }

    protected function updatePreviewImage($model)
    {
        $wrapper = ImageWrapper::uploadForOwner($_FILES['preview_img'], $model, 'images/item', 'full_img');
        if (!$wrapper) {
            return;
        }
        $model->preview_img = $wrapper->getFilepath();
    }

    protected function updateFullImage($model)
    {
        $wrapper = ImageWrapper::uploadForOwner($_FILES['full_img'], $model, 'images/item', 'preview_img');
        if (!$wrapper) {
            return;
        }
        $model->full_img = $wrapper->getFilepath();
    }

    protected function getImageModel()
    {
        return ItemImage::model();
    }

    protected function loadImageModel($id)
    {
        $id = (int)$id;
        return $this->getImageModel()->findByPk($id);
    }

    /**
     * @param CActiveRecord $model
     * @param string $okMsg
     * @param null $url
     */
    protected function manageModelData($model, $okMsg = 'Data was added', $url = null)
    {
        $this->setModelRequestData($model);

        if ($model->save()) {
            $ftrs = $_POST['feature'];
            if (is_array($ftrs) && count($ftrs)) {
                foreach ($ftrs as $feature) {
                    $featureValueItem = new FeatureValueItem();
                    $featureValueItem->item_id = $model->id;
                    $featureValueItem->feature_value_id = $feature;
                    $featureValueItem->save();
                }
            }

            $this->setOkMsg(_($okMsg));
            if ($url) {
                $this->redirect($url);
            } else {
                $this->refresh();
            }
        } else {
            $this->setErrorMsg($this->getModelErrorsHtml($model));
        }
    }

    public function subActionDeleteImages()
    {
        $ids = (array)$this->getRequest()->getParam('image_id');
        $models = array_filter(array_map(array($this, 'loadImageModel'), $ids));
        foreach ($models as $each) {
            $each->delete();
        }
        $this->setOkMsg(_('Images were deleted'));
    }

    public function actionAdd()
    {
        $features = Feature::model()->findAll();

        $this->renderWidget('FormView', array('model' => $this->getAddModel(), 'features' => $features));
    }

    public function actionEdit()
    {
        if (!empty($_REQUEST['image_id'])) {
            $this->subActionDeleteImages();
        }
        $model = $this->getEditModel();
        $this->renderWidget('FormView', array('model' => $model));
    }

    public function getActions()
    {
        return array_merge(parent::getActions(), array('comments'));
    }

    public function getActionLinkComments($model, $action)
    {
        $url = $this->createUrl('comments/index', array('item_id' => $model->id));
        $title = _('Comments');
        return "<a href='{$url}'>[{$title}]</a>";
    }

    public function actionIndex()
    {
        // create filter criteria
        $request = $this->getRequest();
        $criteria = Item::model()->resetScope()->getDbCriteria();
        $is_options = [
            'is_new' => 'Новый',
            'is_special' => 'Специальный',
            'is_main' => 'На главной',
            'available' => 'В наличии',
        ];

        $filter = $request->getQuery('filter', ['is_catalog' => '1']);
        if (!empty($filter['category'])) {
            $criteria->join = 'INNER JOIN {{category_has_items}} l ON l.item_id=t.id AND l.category_id = ' . $filter['category'];
        }
        if (!empty($filter['city'])) {
            if (!$criteria->join) {
                $criteria->join = '';
            }

            $criteria->join .= ' INNER JOIN {{item_with_partner}} ip ON t.id=ip.item_id';
            $criteria->join .= ' INNER JOIN {{partner}} p ON ip.partner_id=p.id';
            $criteria->addCondition('p.city_id = ' . $filter['city']);
        }
		if(!empty($filter['partner'])) {
            $criteria->join .= ' INNER JOIN {{item_with_partner}} ip1 ON t.id=ip1.item_id AND ip1.partner_id='.$filter['partner'];
        }
        if (!empty($filter['price']) && !empty($filter['price_operation']) && in_array($filter['price_operation'], ['=', '>=', '<='])) {
            if (!$criteria->join) {
                $criteria->join = '';
            }

            $criteria->join .= ' INNER JOIN {{item_with_city}} ic ON t.id=ic.item_id AND ' . sprintf('ic.item_price %s :price', $filter['price_operation']);
            $criteria->params['price'] = $filter['price'];
        }
        if (!empty($filter['text'])) {
            $criteria->addSearchCondition('t.title', $filter['text']);
        }
        foreach ($is_options as $key => $option) {
            if (isset($filter[$key])) {
                $criteria->addCondition(sprintf('t.%s = %d', $key, isset($filter[$key]) ? 1 : 0));
            }
        }

        $criteria->with = ['prices', 'partners'];

        $criteria->group = 't.id';



        $oItems = new CActiveDataProvider('Item', [
            'criteria' => $criteria,
            'sort' => [
                'defaultOrder' => 't.id DESC'
            ],
            'pagination' => [
                'pageSize' => 20
            ]
        ]);

        // todo: move to widget
        $categoriesList = [];
        foreach (Category::getTree(false) as $node) {
            $group = [$node['id'] => sprintf('Все (%s)', $node['title'])];
            foreach ($node['children'] as $child) {
                $group[$child['id']] = $child['title'];
            }
            $categoriesList[$node['title']] = $group;
        }

        $cities = City::getCityList();

        $this->render('index', [
            'oItems' => $oItems,
            'categories' => $categoriesList,
            'filter' => $filter,
            'is_options' => $is_options,
            'cities' => $cities,
			'partners' => Partner::getPartnersList()
        ]);
    }

    public function actionManage()
    {
        $request = $this->getRequest();
        $fromPage = $request->getParam('page');
        $fromPage = $fromPage ?
            '/admin/item/index/Item_page/' . $fromPage . '.html' : '/admin/item';

        $itemId = intval($request->getParam('id'));
        $aItemFeatures = [];
        $features = Feature::model()->findAll();
        $cities = City::model()->findAll();
        $categoriesTree = Category::getTree(true, false);
        $categories = [];
        foreach ($categoriesTree as $node) {
            $categories[$node['id']] = $node['title'];
        }
        $subcategories = [];
        $subcategoriesOptions = [];
        foreach ($categoriesTree as $node) {
            foreach ($node['children'] as $child) {
                $subcategories[$child['id']] = $child['title'];
                $subcategoriesOptions[$child['id']] = ['data-owner' => $node['id']];
            }
        }

        if (!$itemId) {
            $oItem = new Item('create');
            $type = $request->getQuery('type', 'item');
            if (!in_array($type, ['item', 'group'])) {
                $type = 'item';
            }
            $oItem->type = $type;
        } else {
            $oItem = Item::model()->with('categories')->findByPk($itemId);
            $oItem->setScenario('update');
            $oItemFeaturesValues = FeatureValueItem::model()->findAllByAttributes(['item_id' => $itemId]);

            if ($oItemFeaturesValues) {
                foreach ($oItemFeaturesValues as $oItemFeaturesValue) {
                    if ($oItemFeaturesValue->feature_value_id) {
                        $oFeatureValue = FeatureValue::model()->findByPk($oItemFeaturesValue->feature_value_id);
                        $aItemFeatures[$oFeatureValue->feature_id][] = $oItemFeaturesValue->feature_value_id;
                    }
                }
            }
        }

        $oPartners = Partner::model()->findAll([
            'order' => 'name ASC'
        ]);

        // существуют ли опшены для этого подарка
        $is_option = ItemCityLink::model()->find('item_id=:item_id AND option_id>:option_id', array(':item_id' => $itemId, 'option_id' => 0));

        //html::pr($is_option,1);

        if ($request->isPostRequest && isset($_POST['Item'])) {
            
            $is_vkpost = (bool)$request->getParam('is_vkpost');
            // todo: reduce LoC
            $oItem->attributes = $_POST['Item'];
            $oItem->price = $_POST['Item']['price'];
            $oItem->partner_price = $_POST['Item']['partner_price'];
            $oItem->partner_id = (int)$_POST['Item']['partner_id'];
            $oItem->is_landing = (bool)$request->getParam('is_landing');
            $oItem->is_kit = (bool)$request->getParam('is_kit');
            $oItem->is_new = (bool)$request->getParam('is_new');
            $oItem->yml = (bool)$request->getParam('yml');
            $oItem->is_season = (bool)$request->getParam('is_season');
            $oItem->is_hot = (bool)$request->getParam('is_hot');
            $oItem->off_price = (int)$request->getParam('off_price');
            $oItem->off_price = $_POST['Item']['off_price'] ? (int)$_POST['Item']['off_price'] : null;
            $oItem->is_special = (bool)$request->getParam('is_special');
            $oItem->is_in_group = (int)$request->getParam('is_in_group');
            $oItem->is_main = (bool)$request->getParam('is_main');
            $oItem->is_weekend = (bool)$request->getParam('is_weekend');
            $oItem->is_recommend = (bool)$request->getParam('is_recommend');
            $oItem->is_catalog = (bool)$request->getParam('is_catalog');
            $oItem->available = intval($request->getParam('available'));
            $oItem->event_id = empty($_POST['Item']['event']) ? null : $_POST['Item']['event'];
            $oItem->verified = (int)(bool)$request->getParam('verified');
            $oItem->kind_validity = $request->getParam('kind_validity');
            $oItem->months_of_validity = $_POST['Item']['months_of_validity'];

            if($request->getParam('kind_validity') == 1) {
                $oItem->start_season_date = $request->getParam('start_season_date');
            } else {
                $oItem->start_season_date = null;
            }

            // если есть опшен, ничего не делаем
            if (empty($is_option)) {
                $prices_cache = [];
                if (!empty($_POST['Item']['city_prices'])) {
                    foreach ($_POST['Item']['city_prices'] as $cityId => $priceData) {
                        $prices_cache[$cityId] = $priceData['price'];
                    }
                }
                $oItem->prices_cache = $prices_cache;
            }

            $oItem->setRelatedData('categories', array_merge($request->getParam('Category', []), $request->getParam('Subcategory', [])));
            $oItem->setRelatedData('features', $this->getRequest()->getParam('features', []));

            // если есть опшен, ничего не делаем
            if (empty($is_option)) {
                $oItem->setRelatedData('partners', empty($_POST['Item']['partner_prices']) ? [] : $_POST['Item']['partner_prices']);
                $oItem->setRelatedData('prices', empty($_POST['Item']['city_prices']) ? [] : $_POST['Item']['city_prices']);
            }



            if ($oItem->save()) {
                if ($oItem->type == 'group') {
                    foreach ($oItem->selfLinks as $selfLink) {
                        $selfLink->delete();
                    }
                    foreach ($_POST['Items'] as $rItemOptions) {
                        $selfLinkNew = new ItemSelfLink();
                        $selfLinkNew->item_id = $oItem->id;
                        $selfLinkNew->child_id = $rItemOptions['id'];
                        $selfLinkNew->option_id = $rItemOptions['option'];
                        $selfLinkNew->city_id = $rItemOptions['city'];
                        $selfLinkNew->save();
                    }

                    /** @var Item $oItem */
//                $oItem->setRelatedData('items', empty($_POST['Items']['id']) ? [] : $_POST['Items']['id']);
                }

                if (!$oItem->perm_link) {
                    $oItem->perm_link = $oItem->generatePermLink();
                    $oItem->save(false);
                }

                if($is_vkpost){
                    $text = strip_tags($oItem->desc);
                    $text .= ' Подробнее можно прочесть здесь: ' . 'http://' . $_SERVER['SERVER_NAME'] . '/' . $oItem->perm_link;
                    $image = $oItem->full_img;
                    $image = str_replace('/images/', '', $image);
                    $image = str_replace('images/', '', $image);
                    $image = Yii::getPathOfAlias('webroot.images') . DIRECTORY_SEPARATOR . $image;
                    try {
                        $vk = Vk::create(Yii::app()->params['vk_token']);
                        $post = new VkPost($vk, null, Yii::app()->params['vk_group_id']);
                        $post->post($text, $image);

                    } catch (Exception $e) {
                        echo 'Error: <b>' . $e->getMessage() . '</b><br />';
                        echo 'in file "' . $e->getFile() . '" on line ' . $e->getLine();
                        die;
                    }
                }

                $this->redirect($fromPage);
            }
        }

        $cities = City::getCities();
        $cityList = City::cityListToArray($cities);
        /** @var Item $oItem */
        $childsData = [];
        foreach ($oItem->childs as $cItem) {
            /** @var Item $cItem*/
            $childsData[$cItem->id] = [
                'prices' => $cItem->getItemPriceSelection($cities),
                'cities' => $cityList,
            ];
        }

        $this->render('manage', [
            'oItem' => $oItem,
            'features' => $features,
            'cities' => $cities,
            'oSubcategories' => $subcategories,
            'subcategoriesOptions' => $subcategoriesOptions,
            'oCategories' => $categories,
            'oPartners' => $oPartners,
            'aItemFeatures' => $aItemFeatures,
            'fromPage' => $fromPage,
            'is_option' => $is_option,
            'childsData' => $childsData,
        ]);
    }

    public function actionDelete()
    {
        $itemId = intval($this->getRequest()->getParam('id'));
        $item = Item::model()->findByPk($itemId);

        if ($item === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        } else {

            $itemCityLinks = ItemCityLink::model()->findAll(array(
                'condition' => 'item_id=:item_id',
                'params' => array(':item_id' => $item->id),
            ));

            foreach ($itemCityLinks as $itemCityLink) {
                if (!empty($itemCityLink->option_id)) {
                    Option::model()->findByPk($itemCityLink->option_id)->delete();
                }
                $itemCityLink->delete();
            }

            ItemPartnerLink::model()->deleteAll(array(
                'condition' => 'item_id=:item_id',
                'params' => array(':item_id' => $item->id),
            ));

            @unlink(Yii::getPathOfAlias('webroot') . $item->preview_img);
            @unlink(Yii::getPathOfAlias('webroot') . $item->full_img);
            //$item->removed = true;
            //$item->save();

            $item->delete();
            //$item->save();
            $this->redirect('/admin/item');
        }


        //$this->redirect($_SERVER['HTTP_REFERER']);
    }

    public function actionPhotos()
    {
        $itemId = intval($this->getRequest()->getParam('item_id'));
        $oItem = Item::model()->findByPk($itemId);
        $uploaded = false;

        if (count($_POST)) {
            $aPhoto = $this->getRequest()->getParam('aPhoto');
            foreach ($aPhoto as $id => $params) {
                $oItemPhoto = ItemPhoto::model()->findByPk($id);
                $oItemPhoto->moderated = (array_key_exists('mod', $params) && $params['mod'] == 'on') ? 1 : 0;
                $oItemPhoto->comment = $params['comment'];
                $oItemPhoto->order = $params['order'];
                $oItemPhoto->save();
            }
            //die(nl2br(print_r($aPhoto,true)));
        }

        $oCriteria = new CDbCriteria();
        $oCriteria->order = '`order` asc';
        $oCriteria->condition = '`item_id`=' . $itemId;
        $oPhoto = ItemPhoto::model()->findAll($oCriteria);

        $this->render('photos', array(
            'oItem' => $oItem,
            'oPhoto' => $oPhoto,
            'uploaded' => $uploaded
        ));
    }

    public function actionPhoto_delete()
    {
        $photoId = intval($this->getRequest()->getParam('id'));
        ItemPhoto::model()->findByPk($photoId)->delete();

        $this->redirect($_SERVER['HTTP_REFERER']);
    }

    public function actionPhoto_replace()
    {
//        $photoId = intval($this->getRequest()->getParam('id'));
//        $oPhoto = ItemPhoto::model()->findByPk($photoId);
    }

    public function actionPhoto_upload()
    {
        $photoId = intval($this->getRequest()->getParam('id'));
        $comment = $this->getRequest()->getParam('newPhotoComment');
        $item_id = intval($this->getRequest()->getParam('item_id'));
        $oPhoto = ItemPhoto::model()->findByPk($photoId);
        if (count($_FILES)) {
            foreach ($_FILES as $image) {
                $oPhoto = new PhotoWrapper($item_id);
                $oPhoto->prepare($image);
                $filename = $oPhoto->getPath();
                $oPhotoModel = new ItemPhoto();
                $oPhotoModel->setScenario('saving');
                $oPhotoModel->filename = $filename;
                $oPhotoModel->item_id = $item_id;
                $oPhotoModel->user_id = Yii::app()->user->getId();
                $oPhotoModel->comment = $comment;

                if ($oPhotoModel->save()) {
                    $oPhoto->process();
                    $oPhoto->save();
                    $uploaded = true;
                }
            }
        }
        $this->redirect($_SERVER['HTTP_REFERER']);
    }

    public function actionGroupedit()
    {
        if ($_POST['id']) {
            $item_id = intval($_POST['id']);
            $item = Item::model()->findByPk($item_id);
            if (empty($item)) {
                echo json_encode(array('error' => 'Товара с таким ID не существует'));
                exit();
            }
            $item->category_id = $_POST['category_id'];
//            $item->price = $_POST['price'];
            $item->partner_price = $_POST['partner_price'];
            $item->off_price = $_POST['off_price'];
            SubcategoryItems::addItems($_POST['Subcategory'], $item_id);
            if ($item->save()) {
                echo json_encode(array('message' => 'Изменено'));
                exit();
            } else {
                echo json_encode(array('error' => 'При сохранении в базу произошла ошибка'));
                exit();
            }
        }
        $categories = Category::model()->findAll();
        $oSubcategories = Subcategory::model()->with('category')->findAll();
        $items = Item::model()->findAll();
        $this->render('groupedit', array(
            'oItems' => $items,
            'oCategories' => $categories,
            'oSubcategories' => $oSubcategories,
        ));
    }

    public function actionAddbanner()
    {
        $fromPage = $this->getRequest()->getParam('page');

        if (!$fromPage) {
            $fromPage = '/admin/item';
        } else {
            $fromPage = '/admin/item/index/Item_page/' . $fromPage . '.html';
        }
        $itemId = $this->getRequest()->getParam('item_id');
        $banner = Banners::model()->findByAttributes(array('item_id' => $itemId));
        if (!$banner) {
            $banner = new Banners();
            $banner->item_id = intval($itemId);
            $item = Item::model()->findByPk(intval($itemId));
            if (!$item) {
                $this->redirect($fromPage);
            }
            $banner->item = $item;
            $banner->title = mb_substr($item->title, 0, 30);
            $banner->text = mb_substr($item->desc, 0, 150);
        }
        if (isset($_POST['Banner'])) {
            $banner->title = $_POST['Banner']['title'];
            $banner->text = $_POST['Banner']['text'];
            if ($banner->save()) {
                $this->redirect($fromPage);
            }
        }

        $this->render('addbanner', array(
            'banner' => $banner,
            'fromPage' => $fromPage,
        ));
    }

    public function actionAllbanners()
    {
        $aItemsParams = array
        (
            'pagination' => array
            (
                'pageSize' => 20
            ),
            'criteria' => array(
                'with' => 'item'
            )
        );
        $banners = new CActiveDataProvider('Banners', $aItemsParams);
        $this->render('allbanners', array(
            'banners' => $banners,
        ));
    }

    public function actionDeletebanner()
    {
        $itemId = $this->getRequest()->getParam('id');
        $banner = Banners::model()->findByPk(intval($itemId));
        $banner->delete();
        $this->redirect('/admin/item/allbanners');
    }

    public function actionInstruction_preview()
    {
        // placeholders


        $this->renderPartial('instruction');
    }

    public function actionCert_preview($id)
    {
        /** @var Item $item */
        $item = Item::model()->findByPk($id);
        if (!$item) {
            throw new CHttpException(404);
        }

        if (!$item->isDigitalCertificateAvailable()) {
            throw new CHttpException(404, 'К сожалению электронные сертификаты не доступны для данного впечатления');
        }

        $fakeOrder = new Order();
        $fakeOrder->item_id = $item->id;
        $code = range(0, 12);
        shuffle($code);
        $fakeOrder->code = implode("", $code);

        $this->renderFile(Yii::getPathOfAlias('application.views.pdf') . '/digital_certificate.php', [
            'order' => $fakeOrder
        ]);
    }
}
