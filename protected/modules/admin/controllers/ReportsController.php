<? 
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ReportsController
 *
 * @author WaveCut
 */
class ReportsController extends CController
{
    public static $reportTypes = array
    (
        0 => 'Выберите фильтр',
        1 => 'Информационные источники',
        2 => 'Расчеты с поставщиками',
        3 => 'Реализация',
        4 => 'Заказы'
    );

    //put your code here
    public function actionIndex()
    {

        $aFilter = Yii::app()->request->getParam('aFilter');
        $reportType = Yii::app()->request->getParam('reportType');

        switch ($reportType)
        {
            case (1):
                $this->knowwhereReport($aFilter, $reportType);
                break;
            case (2):
                $this->partnerReport($aFilter, $reportType);
                break;
            case (3):
                $this->saleReport($aFilter, $reportType);
                break;
            case (4):
                $this->exportOrders($aFilter);
                break;
            case (0):
                $this->render('index', array(
                    'dateFrom' => '0000-00-00',
                    'dateTo' => date('Y-m-d')));
        }
    }

    public function saleReport($aFilter, $reportType)
    {
        $partnerId = (boolean) $aFilter['partner'] ? (int) $aFilter['partner'] : 0;
        $dateFrom  = empty($aFilter['dateFrom']) ? '0000-00-00' : $aFilter['dateFrom'];
        $dateTo = empty ($aFilter['dateTo']) ? date('Y-m-d') : $aFilter['dateTo'];
        $result = array();
        $where  = array();
        $aData = array();

        if (is_array($aFilter) && count($aFilter))
        {
            $oCriteria=new CDbCriteria;
            $oCriteria->select='*';

            if ($partnerId)
                $where[] = 'partner_id = '.$partnerId;
            $where[] = 'is_paid = 1';
            $where[] = 'DATE(order_date)>=\'' . $dateFrom . '\'';
            $where[] = 'DATE(order_date)<=\'' . $dateTo . '\'';

            $oCriteria->condition = count($where) ? implode(' AND ', $where) : '';
            $oCriteria->order = 'partner_id, order_date ASC';

            $aOrders = Order::model()->with('item', 'user', 'flower', 'card', 'item.partner')->findAll($oCriteria);

            $aData[0]['totalSum']     = 0;
            $aData[0]['totalSumDisc'] = 0;

            $i = 0;

            foreach ($aOrders as $oOrder)
            {
                $aData[$i]['order'] = $oOrder;

                $aData[$i]['price']  = $oOrder->price - ($oOrder->flower_quantity * $oOrder->flower->flower_price + $oOrder->card_quantity * $oOrder->card->card_price);
                $aData[$i]['flower'] = $oOrder->flower_quantity * $oOrder->flower->flower_price;
                $aData[$i]['card']   = $oOrder->card_quantity * $oOrder->card->card_price;

                if ($oOrder->discount)
                {
                    $aData[$i]['priceDisc']   = $aData[$i]['price']  - ceil($aData[$i]['price'] / 100 * $oOrder->discount);
                    $aData[$i]['flowerDisc']  = $aData[$i]['flower'] - ceil($aData[$i]['flower'] / 100 * $oOrder->discount);
                    $aData[$i]['cardDisc']    = $aData[$i]['card']   - ceil($aData[$i]['card'] / 100 * $oOrder->discount);
                }
                else
                {
                    $aData[$i]['priceDisc']   = $aData[$i]['price'];
                    $aData[$i]['flowerDisc']  = $aData[$i]['flower'];
                    $aData[$i]['cardDisc']    = $aData[$i]['card'];
                }

                $aData[$i]['deliv']         = $oOrder->is_express_delivery ? ShopOrder::getDelivPrice() : 0;
                $aData[$i]['giftDeliv']     = $oOrder->is_express_delivery_recipient ? ShopOrder::getGiftDelivPrice() : 0;
                $aData[$i]['thematicDeliv'] = $oOrder->is_thematic_delivery ? $oOrder->thematic_delivery_price : 0;

                $aData[$i]['total']     = $aData[$i]['price'] + $aData[$i]['flower'] + $aData[$i]['card'] + $aData[$i]['deliv'] + $aData[$i]['giftDeliv'] + $aData[$i]['thematicDeliv'];
                $aData[$i]['totalDisc'] = $aData[$i]['priceDisc'] + $aData[$i]['flowerDisc'] + $aData[$i]['cardDisc'] + $aData[$i]['deliv'] + $aData[$i]['giftDeliv'] + $aData[$i]['thematicDeliv'];

                $aData[0]['totalSum']     += $aData[$i]['total'];
                $aData[0]['totalSumDisc'] += $aData[$i]['totalDisc'];

                $i++;
            }
        }
        $this->render('index', array(
            'result' => $result,
            'dateFrom' => $dateFrom,
            'dateTo' => $dateTo,
            'reportType' => $reportType,
            'aData' => $aData,
        ));
    }


    public function partnerReport($aFilter, $reportType)
    {
        $partnerId = (boolean) $aFilter['partner'] ? (int) $aFilter['partner'] : 0;
        $dateFrom = empty($aFilter['dateFrom']) ? '0000-00-00' : $aFilter['dateFrom'];
        $dateTo = empty ($aFilter['dateTo']) ? date('Y-m-d') : $aFilter['dateTo'];
        $result = array();
        $where = array();

        if (is_array($aFilter) && count($aFilter))
        {
            $oCriteria=new CDbCriteria;
            $oCriteria->select='*';

            if ($partnerId)
                $where[] = 'partner_id = '.$partnerId;
            $where[] = 'is_paid = 1';
            $where[] = 'DATE(order_date)>=\'' . $dateFrom . '\'';
            $where[] = 'DATE(order_date)<=\'' . $dateTo . '\'';

            $oCriteria->order = 'partner_id, order_date ASC';

            $oCriteria->condition = count($where) ? implode(' AND ', $where) : '';

            $aOrders = Order::model()->with('item', 'user', 'item.partner')->findAll($oCriteria);

        }
        $this->render('index', array(
            'result' => $result,
            'dateFrom' => $dateFrom,
            'dateTo' => $dateTo,
            'reportType' => $reportType,
            'oData' => $aOrders,
        ));
    }




    public function knowwhereReport($aFilter, $reportType)
    {
        $dateFrom = '0000-00-00';
        $dateTo = date('Y-m-d');
        $result = array();
        $where = array();

        if (is_array($aFilter) && count($aFilter))
        {
            $oLowCriteria = new CDbCriteria();
            $oLowCriteria->group = 'item_id';

            $oCriteria = new CDbCriteria();
            $oCriteria->select = 'order_knowwhere';
            $oCriteria->group = 'order_knowwhere';
            $oCriteria->order = 'count(item_id) desc';

            $dateFrom = ($aFilter['dateFrom']) ? $aFilter['dateFrom'] : $dateFrom;
            $where[] = 'DATE(order_date)>=\'' . $dateFrom . '\'';
            $dateTo = ($aFilter['dateTo']) ? $aFilter['dateTo'] : $dateTo;
            $where[] = 'DATE(order_date)<=\'' . $dateTo . '\'';

            $oCriteria->condition = count($where) ? implode(' AND ', $where) : '';
            $oKnown = Order::model()->findAll($oCriteria);

            foreach ($oKnown as $row)
            {
                if (isset($row->order_knowwhere))
                {
                    $oLowCriteria->condition = (count($where) ? implode(' AND ', $where) : '') . ' AND order_knowwhere=\'' . $row->order_knowwhere . '\'';
                    $result[$row->order_knowwhere] = Order::model()->findAll($oLowCriteria);
                }
            }
        }
        $this->render('index', array(
            'result' => $result,
            'dateFrom' => $dateFrom,
            'dateTo' => $dateTo,
            'reportType' => $reportType,
        ));
    }

    public function exportOrders($aFilter)
    {
        $dateFrom = empty($aFilter['dateFrom']) ? '0000-00-00' : $aFilter['dateFrom'];
        $dateTo = empty ($aFilter['dateTo']) ? date('Y-m-d') : $aFilter['dateTo'];

        $orders = Yii::app()->db->createCommand()
            ->select('o.order_number, o.order_date, o.phone, item.title, item.id, p.name, p.id as partner_id, o.price, o.delivery_price, IF(i.partner_price is null, o.price *8/10, i.partner_price) partner_price, o.code, i.partner_activated, i.partner_activated_date, o.is_paid, o.is_canceled')
            ->from('el_order o')
            ->join('el_order_items i', 'o.id = i.order_id')
            ->join('el_item item', 'item.id = i.item_id')
            ->leftjoin('el_partner p', 'p.id = i.partner_id')
            ->where('o.order_date >=:from and o.order_date <=:to', array(':from'=>$dateFrom, ':to'=>$dateTo))
            ->order('order_date')
            ->queryAll();


        if(count($orders) > 0 ) {


            Yii::import('ext.phpexcel.XPHPExcel');
            $objPHPExcel= XPHPExcel::createPHPExcel();
            $objPHPExcel->getProperties()->setCreator("Elpresent.by")
                ->setLastModifiedBy("Elpresent.b")
                ->setTitle("Выгрузка заказов")
                ->setSubject("Выгрузка заказов")
                ->setDescription("Выгрузка заказов за определенный период времени")
                ->setKeywords("office 2007 openxml php")
                ->setCategory("import export file");


            // Add some data
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Order number')
                ->setCellValue('B1', 'order date!')
                ->setCellValue('C1', 'phone')
                ->setCellValue('D1', 'title')
                ->setCellValue('E1', 'id')
                ->setCellValue('F1', 'name')
                ->setCellValue('G1', 'partner_id')
                ->setCellValue('H1', 'price')
                ->setCellValue('I1', 'delivery_price')
                ->setCellValue('J1', 'partner_price')
                ->setCellValue('K1', 'code')
                ->setCellValue('L1', 'partner_activated')
                ->setCellValue('M1', 'partner_activated_date')
                ->setCellValue('N1', 'is_paid')
                ->setCellValue('O1', 'is_canceled');

            $from = "A1"; // or any value
            $to = "O1"; // or any value
            $objPHPExcel->getActiveSheet()->getStyle("$from:$to")->getFont()->setBold( true );


            foreach($objPHPExcel->getActiveSheet()->getColumnDimension() as $col) {
                $col->setAutoSize(true);
            }

            // Miscellaneous glyphs, UTF-8
            $i=2;
            foreach ($orders as $key => $v){
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A'.$i, $v['order_number'])
                    ->setCellValue('B'.$i, $v['order_date'])
                    ->setCellValue('C'.$i, $v['phone'])
                    ->setCellValue('D'.$i, $v['title'])
                    ->setCellValue('E'.$i, $v['id'])
                    ->setCellValue('F'.$i, $v['name'])
                    ->setCellValue('G'.$i, $v['partner_id'])
                    ->setCellValue('H'.$i, $v['price'])
                    ->setCellValue('I'.$i, $v['delivery_price'])
                    ->setCellValue('J'.$i, $v['partner_price'])
                    ->setCellValue('K'.$i, $v['code'])
                    ->setCellValue('L'.$i, $v['partner_activated'])
                    ->setCellValue('M'.$i, $v['partner_activated_date'])
                    ->setCellValue('N'.$i, $v['is_paid'])
                    ->setCellValue('O'.$i, $v['is_canceled']);
                $i++;
            }

            $as =  $objPHPExcel->getActiveSheetIndex();


            foreach(range('A','O') as $columnID) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }

            // Rename worksheet
           // $objPHPExcel->getActiveSheet()->setTitle('Заказы');


            // Set active sheet index to the first sheet, so Excel opens this as the first sheet
            $objPHPExcel->setActiveSheetIndex(0);


            // Redirect output to a clientâ€™s web browser (Excel5)
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="Заказы.xls"');
            header('Cache-Control: max-age=0');
            // If you're serving to IE 9, then the following may be needed
            header('Cache-Control: max-age=1');

            // If you're serving to IE over SSL, then the following may be needed
            header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
            header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header ('Pragma: public'); // HTTP/1.0

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
            Yii::app()->end();
        } else {
            echo 'no result';
        }
    }

}
?>
