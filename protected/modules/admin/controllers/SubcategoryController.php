<?php

class SubcategoryController extends CAdminController
{
	protected function setModelRequestData($model)
	{
		$model->title = (string)Yii::app()->request->getParam('title');
        $model->icon = Yii::app()->request->getParam('icon');
        $model->parent_id = Yii::app()->request->getParam('parent_id');
        $model->slug = Yii::app()->request->getParam('slug');
        $model->meta_description = Yii::app()->request->getParam('meta_description');
        $model->meta_keywords = Yii::app()->request->getParam('meta_keywords');
        $model->meta_title = (string)Yii::app()->request->getParam('meta_title');
        $model->seo_text = (string)Yii::app()->request->getParam('seo_text');
        $model->visible = (int)Yii::app()->request->getParam('visible');
	}

	public function getModel()
	{
		return Category::model()->onlyChild();
	}

	public function attributeLabels()
	{
		static $labels;
		if(is_null($labels))
			$labels = array(
                            'id' => _('Id'),
                            'title' => _('Title'),
                            'parent_id' => _('Category'),
                            'icon' => 'icon'
                            
                        );
		return $labels;
	}
        public function actionIndex()
        {
            if (isset($_POST['category_orders']))
            {
                Subcategory::model()->updateOrders($_POST['category_orders']);
            }
            $model = $this->getListModel();
            $model->getDbCriteria()->order = 'parent_id ASC, `order` ASC';
            $list = new ModelPager($model);
            $text = $this->getRenderedWidget('TableView', array('list' => $list));
            $text .= $this->getRenderedWidget('AdminPagerWidget', array('pager' => $list));
            $this->renderText($text);
        }
        public function actionEdit()
        {
            parent::actionEdit();
            $this->renderPartial('link_js');

        }
        public function actionAdd()
        {
            parent::actionAdd();
            $this->renderPartial('link_js');
        }
}