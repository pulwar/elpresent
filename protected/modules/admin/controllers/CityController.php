<?php


class CityController extends Controller
{

    public function actionIndex()
    {
        $cities = new CActiveDataProvider('City', [
                'criteria'=>[
                    'order'=>'name ASC',       
                ],
                'pagination' => [
                    'pageSize' => 20
                ]
        ]);

        $this->render('index', [
            'oCity' => $cities
        ]);
    }

    public function actionEdit()
    {
        $cityId   = Yii::app()->request->getParam('id');
        $fromPage = Yii::app()->request->getParam('page');

        $fromPage = !$fromPage ? '/admin/city' : '/admin/city/index/page/' . $fromPage;

        $oCity = City::model()->findByPk($cityId);

        if ($this->getRequest()->isPostRequest) {
            $oCity->attributes = $_POST['City'];
            $oCity->enabled = isset($_POST['City']['enabled']);

            if ($oCity->save()) {
                $this->redirect($fromPage);
            }
        }

        $this->render('edit', [
            'oCity' => $oCity,
            'fromPage' => $fromPage
        ]);
    }

    public function actionAdd()
    {
        $fromPage = Yii::app()->request->getParam('page');

        if (!$fromPage)
        {
            $fromPage = '/admin/city';
        }
        else
        {
            $fromPage = !$fromPage ? '/admin/city' : '/admin/city/index/page/' . $fromPage;
        }

        $oCity = new City;

        if ($this->getRequest()->isPostRequest) {
            $oCity->attributes = $_POST['City'];

            if ($oCity->save()) {
                $this->redirect($fromPage);
            }
        }

        $this->render('add', [
            'oCity' => $oCity,
            'fromPage' => $fromPage
        ]);
    }

    public function actionDelete()
    {
        $cityId = Yii::app()->request->getParam('id');
        $fromPage = Yii::app()->request->getParam('page');
        if (!$fromPage)
        {
            $fromPage = '/admin/city';
        }
        else
        {
            $fromPage = '/admin/city/index/page/' . $fromPage . '.html';
        }

        City::model()->deleteByPk($cityId);
        $this->redirect($fromPage);
    }

}