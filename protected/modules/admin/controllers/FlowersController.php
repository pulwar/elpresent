<? 
class FlowersController extends CController
{
    protected $_sizes = array
    (
        array('width' => 48,  'height' => 46),
        array('width' => 297, 'height' => 283)
    );
    
    public function actionIndex()
    {
        $oFlowers = new CActiveDataProvider('Flower', array
        (
        	'pagination' => array
            (
            	'pageSize' => 20
            )
        ));
        
        $this->render('index', array
        (
            'oFlowers' => $oFlowers
        ));
    } 

    public function actionManage()
    {
        $flowerId = Yii::app()->request->getParam('id');
        
        if ( !$flowerId )
        {
            $oFlower = new Flower();    
        }
        else
        {
            $oFlower = Flower::model()->findByPk($flowerId);    
        }
        
        if ( isset($_POST['Flower']) )
        {
            $flowerImageSaved      = $oFlower->flower_image;
            $oFlower->attributes   = $_POST['Flower'];
            $oFlower->flower_price = $_POST['Flower']['flower_price'];
            $oFlower->flower_image = CUploadedFile::getInstance($oFlower, 'flower_image');
            
            if ( $oFlower->validate() )
            {
                if ( $oFlower->flower_image )
                {
                    $extension             = $oFlower->flower_image->getExtensionName();
                    $originalFlowerImage   = $oFlower->generateRandomName();
                    $flowerImagePath       = YiiBase::getPathOfAlias('webroot').'/upload/flowers/'.$originalFlowerImage.'.'.$extension;
                    $flowerThumbsImagePath = YiiBase::getPathOfAlias('webroot').'/upload/flowers/'.$originalFlowerImage;
                    $oFlower->flower_image->saveAs($flowerImagePath);
                    $oFlower->flower_image = $originalFlowerImage;
                    
                    Yii::import('application.extensions.image.Image');
                    $image = new Image($flowerImagePath);
                    
                    foreach ( $this->_sizes as $size )
                    {
                        $image->resize($size['width'], $size['height']);
                        $image->save($flowerThumbsImagePath.'_'.$size['width'].'.png');    
                    }
                }
                else
                {
                    $oFlower->flower_image = $flowerImageSaved;
                }
                
                $oFlower->save();
                
                Yii::app()->user->setFlash('flower_message_successfully', 'Цветы успешно добавлены.');
                $this->redirect('/admin/flowers');
            }
        }
        
        $this->render('manage', array
        (
            'oFlower' => $oFlower
        ));    
    }   

    public function actionDelete()
    {
        $flowerId = Yii::app()->request->getParam('id');
        
        if ( $flowerId )
        {
            $oFlower = Flower::model()->findByPk($flowerId);
            $flowerBigImagePath   = Yii::getPathOfAlias('webroot').'/upload/flowers/'.$oFlower->flower_image.'_297.png';
            $flowerSmallImagePath = Yii::getPathOfAlias('webroot').'/upload/flowers/'.$oFlower->flower_image.'_48.png';
            
            if ( file_exists($flowerBigImagePath) )
            {
                unlink($flowerBigImagePath);
            }
            
            if ( file_exists($flowerSmallImagePath) )
            {
                unlink($flowerSmallImagePath);
            }
            
            $oFlower->delete();
            
            $this->redirect('/admin/flowers');
        }
    }
}
