<?php
class PartnerController extends CController
{

    public function actionFilter()
    {
        /** @var CHttpRequest $request */
        $request = Yii::app()->request;
        $searchStr = $request->getParam('searchStr');
        if ($request->isAjaxRequest) {
            $criteria = new CDbCriteria();
            if(!empty($searchStr)) {
                $criteria->addCondition('name LIKE "%' . $searchStr . '%"');
            }
            $criteria->order = 'name ASC';
            $oCitys = City::model()->findAll(array('order' => 'name ASC'));
            $oPartner = new CActiveDataProvider('Partner', array
            (
                'criteria' => $criteria,
                'pagination' => array
                (
                    'pageSize' => 20
                )
            ));

            echo json_encode(['text' => $this->renderPartial('/partner/_grid', ['oPartner' => $oPartner], true, false)]);
            Yii::app()->end();
        }
    }

    public function actionIndex()
    {
        $oCitys = City::model()->findAll(array('order'=>'name ASC'));
        $oPartner = new CActiveDataProvider('Partner', array
        (
                'criteria'=>array(
                    'order'=>'name ASC',
                ),
                'pagination' => array
                (
                    'pageSize' => 20
                )
        ));

        $dbCommand = Yii::app()->db->createCommand("SELECT name, COUNT(id) FROM el_partner GROUP BY name ORDER BY COUNT(id) DESC");
        $data = $dbCommand->queryAll();

        $this->render('index', array
        (
            'oPartner' => $oPartner,
            'oCitys' => $oCitys,
            'partnersList' => empty($data)?[]:$data,
        ));
    }

    public function actionEdit()
    {
        $oCitys = City::model()->findAll(array('order'=>'name ASC'));
        
        $partnerId   = Yii::app()->request->getParam('id');
        $fromPage = Yii::app()->request->getParam('page');

        if (!$fromPage)
        {
            $fromPage = '/admin/partner';
        }
        else
        {
            $fromPage = '/admin/partner/index/page/' . $fromPage . '.html';
        }

        $oPartner = Partner::model()->findByPk($partnerId);

        if (count($_POST))
        {
            $oPartner->name = $_POST['Partner']['name'];
            $oPartner->city_id = intval($_POST['Partner']['city_id']);
            $oPartner->phone = $_POST['Partner']['phone'];
            $oPartner->adress = $_POST['Partner']['adress'];
            $oPartner->work_time = $_POST['Partner']['work_time'];
            $oPartner->contact_person = $_POST['Partner']['contact_person'];
            $oPartner->agreement_terms = $_POST['Partner']['agreement_terms'];
            $oPartner->commission = $_POST['Partner']['commission'];
            $oPartner->place = $_POST['Partner']['place'];
            $oPartner->lat = $_POST['Partner']['lat'];
            $oPartner->lon = $_POST['Partner']['lon'];
            $oPartner->email = $_POST['Partner']['email'];
            
            if ($oPartner->validate())
            {
                
                if($_POST['Partner']['password'] !== ''){
                    $oPartner->password = md5($_POST['Partner']['password']);
                }
                
                $oPartner->save();
                $this->redirect($fromPage);
            }
        }

        $this->render('edit', array
        (
            'oPartner' => $oPartner,
            'oCitys' => $oCitys,
            'fromPage' => $fromPage
        ));
    }

    public function actionAdd()
    {
        $oCitys = City::model()->findAll(array('order'=>'name ASC'));
        
        $fromPage = Yii::app()->request->getParam('page');

        if (!$fromPage)
        {
            $fromPage = '/admin/partner';
        }
        else
        {
            $fromPage = '/admin/partner/index/page/' . $fromPage . '.html';
        }

        $oPartner = new Partner;

        if (count($_POST))
        {
            $oPartner->name = $_POST['Partner']['name'];
            $oPartner->city_id = intval($_POST['Partner']['city_id']);
            $oPartner->phone = $_POST['Partner']['phone'];
            $oPartner->adress = $_POST['Partner']['adress'];
            $oPartner->work_time = $_POST['Partner']['work_time'];
            $oPartner->contact_person = $_POST['Partner']['contact_person'];
            $oPartner->agreement_terms = $_POST['Partner']['agreement_terms'];
            $oPartner->commission = $_POST['Partner']['commission'];
            $oPartner->reg_date = date("Y-m-d");

            if ($oPartner->validate())
            {
                if($_POST['Partner']['password'] !== ''){
                    $oPartner->password = md5($_POST['Partner']['password']);
                }
                $oPartner->save();
                $this->redirect($fromPage);
            }
        }

        $this->render('add', array
        (
            'oPartner' => $oPartner,
            'oCitys' => $oCitys,
            'fromPage' => $fromPage
        ));
    }

    public function actionDelete()
    {
        $partnerId = Yii::app()->request->getParam('id');
        $fromPage = Yii::app()->request->getParam('page');
        if (!$fromPage)
        {
            $fromPage = '/admin/partner';
        }
        else
        {
            $fromPage = '/admin/partner/index/page/' . $fromPage . '.html';
        }

        Partner::model()->deleteByPk($partnerId);
        $this->redirect($fromPage);
    }

}