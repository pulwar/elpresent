<?php

class UserController extends Controller
{

    public function actionIndex()
    {
        $filter['user_email'] = $_POST['user_email'];
        $criteria = array();
        if($filter['user_email']){
            $mail = $filter['user_email'];
            $criteria['condition'] = "user_email LIKE '%$mail%'";
        }
        $oUsers = new CActiveDataProvider('User', array
                    (
                    'pagination' => array
                        (
                        'pageSize' => 20,
                    ),
                    'criteria' => $criteria,

                ));

        $this->render('index', array
            (
            'oUsers' => $oUsers,
            'filter' => $filter,
        ));
    }

    public function actionManage()
    {
        $userId = Yii::app()->request->getParam('id');
        $fromPage = Yii::app()->request->getParam('page');

        if (!$fromPage)
        {
            $fromPage = '/admin/user';
        }
        else
        {
            $fromPage = '/admin/user/index/page/' . $fromPage . '.html';
        }

        $userId = Yii::app()->request->getParam('id');
        $oUsers = User::model()->findByPk($userId);

        if (count($_POST))
        {
            $oUsers->user_firstname = $_POST['User']['user_firstname'];
            $oUsers->user_lastname = $_POST['User']['user_lastname'];
            $oUsers->user_email = $_POST['User']['user_email'];
            $oUsers->user_phone = $_POST['User']['user_phone'];
            $oUsers->user_address = $_POST['User']['user_address'];
            $oUsers->user_sex = $_POST['User']['user_sex'];
            $oUsers->user_role = $_POST['User']['user_role'];

            if ($oUsers->validate())
            {
                if ($_POST['User']['user_password'] != '')
                {
                    $oUsers->user_password = md5($_POST['User']['user_password']);
                }
                $oUsers->save();
            }

            $this->redirect($fromPage);
        }

        $this->render('manage', array
            (
            'oUsers' => $oUsers,
            'fromPage' => $fromPage
        ));
    }

    public function actionCreatepartner()
    {
        /** @var CHttpRequest $request */
        $request = Yii::app()->request;
        $partnerId = $request->getParam('id');
        if (!empty($partnerId)) {
            /** @var Partner $partner */
            $partner = Partner::model()->findByPk($partnerId);
            if (empty($partner)) {
                throw new CHttpException('Partner Not Found!');
            }
            if (!empty($partner->user_id)) {
                $this->redirect('/admin/user/editpartner/id/'.$partner->user_id);
            }
            $form = new UserPartnerForm();
            $form->login = 'partner-'.$partner->id;
            $form->email = $partner->email;
            if ($request->getIsPostRequest()) {
                $partnerRequest = $request->getPost('UserPartnerForm');
                $form->attributes = $partnerRequest;
                if ($form->validate()) {
                    $user = new User();
                    $user->user_role = User::ROLE_PARTNER;
                    $user->user_firstname = $partner->name;
                    $user->user_lastname = $partner->name;
                    $user->user_email = $partnerRequest['email'];
                    $user->user_login = $partnerRequest['login'];
                    $user->user_password = md5($partnerRequest['password']);
                    $user->save();
                    //-------
                    $partner->user_id = $user->id;
                    $partner->save();
                    $this->redirect('/admin/partner/edit/id/'.$partner->id);
                }
            }
            $this->render('createpartner', [
                'form' => $form,
                'partnerId' => $partner->id,
            ]);
        } else {
            throw new CHttpException('Not Found!');
        }
    }

    public function actionEditpartner()
    {
        /** @var CHttpRequest $request */
        $request = Yii::app()->request;
        $fromPage = $request->getParam('page');
        $fromPage = '/admin/user'. (!$fromPage ? '' : '/index/page/' . $fromPage . '.html');
        $userId = $request->getParam('id');
        /** @var User $user */

        $user = User::model()->findByPk($userId);
        if (!empty($user)) {
            if ($request->getParam('fromPartnerPage')) {
                $fromPage = '/admin/partner/edit/id/'.Partner::model()->findByAttributes(['user_id' => $user->id])->id;
            }
            $form = new UserPartnerForm(UserPartnerForm::TYPE_EDIT);
            $form->login = $user->user_login;
            $form->email = $user->user_email;
            if ($request->getIsPostRequest()) {
                $partnerRequest = $request->getPost('UserPartnerForm');
                $form->attributes = $partnerRequest;
                if ($form->validate()) {
                    $user->user_email = $partnerRequest['email'];
                    $user->user_login = $partnerRequest['login'];
                    if (!empty($partnerRequest['password'])) {
                        $user->user_password = md5($partnerRequest['password']);
                    }
                    $user->save();
                    $this->redirect($fromPage);
                }
            }
            $this->render('editpartner', [
                'form' => $form,
                'fromUrl' => $fromPage,
            ]);
        } else {
            throw new CHttpException('Not Found!');
        }
    }

    public function actionDelete()
    {
        $userId = Yii::app()->request->getParam('id');
        $fromPage = Yii::app()->request->getParam('page');
        if (!$fromPage)
        {
            $fromPage = '/admin/user';
        }
        else
        {
            $fromPage = '/admin/user/index/page/' . $fromPage . '.html';
        }
        /** @var User $user */
        $user = User::model()->findByPk($userId);
        if ($user->user_role == User::ROLE_PARTNER) {
            /** @var Partner $partner */
            $partner = Partner::model()->findByAttributes(['user_id' => $user->id]);
            if (!empty($partner)) {
                $partner->user_id = null;
                $partner->save();
            }
        }
        $user->delete();
        $this->redirect($fromPage);
    }

    public function actionLogin()
    {
        $session = Yii::app()->getSession();
        $admin_id = Yii::app()->user->getId();

        $user_id = Yii::app()->request->getParam('id');

        $user = User::model()->findByPk($user_id);

        if('admin' == $user->user_role){
            $this->redirect('/admin/user', true);
        }

        if(strlen($user->user_password) < 32){
            $user->user_password = md5($user->user_password);
            $user->save();
        }

        $identity = new UserIdentity($user->user_email, $user->user_password,true);
        
        if ($identity->authenticate())
        {
             Yii::app()->user->login($identity);
        }else{
            $this->redirect('/admin/user', true);
        }

        $session['admin_id'] = $admin_id;
        $this->redirect('/',true);
        
    }

    public function actionFeatureSwitch()
    {
        $request = $this->getRequest();
        // get current user
        $user = Yii::app()->user->getUser();
        $features = Yii::app()->featureSwitch->getAvailableSwitchers();

        if ($request->isPostRequest) {
            $user->feature_switches = empty($_POST['Switches']) ? [] : array_keys($_POST['Switches']);
            if ($user->save()) {
                Yii::app()->user->setFlash('success', 'Выключатели успешно сохранены');
                $this->redirect(Yii::app()->urlManager->createUrl('admin/user/featureSwitch'));
            }
        }

        $this->render('feature_switch', compact('features'));
    }

}