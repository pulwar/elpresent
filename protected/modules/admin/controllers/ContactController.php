<?
class ContactController extends CController
{

    public function actionIndex()
    {
        $oContact = new CActiveDataProvider('Contact', array
        (
                'pagination' => array
                (
                    'pageSize' => 20
                )
        ));

        $this->render('index', array
        (
            'oContact' => $oContact
        ));
    }

    public function actionView()
    {
        $contactId = Yii::app()->request->getParam('id');
        $fromPage  = Yii::app()->request->getParam('page');

        if (!$fromPage)
        {
            $fromPage = '/admin/contact';
        }
        else
        {
            $fromPage = '/admin/contact/index/page/' . $fromPage . '.html';
        }

        $oContact = Contact::model()->findByPk($contactId);

        $this->render('view', array
        (
            'oContact' => $oContact,
            'fromPage' => $fromPage
        ));
    }
    

    public function actionDelete()
    {
        $contactId = Yii::app()->request->getParam('id');
        $fromPage  = Yii::app()->request->getParam('page');
        if (!$fromPage)
        {
            $fromPage = '/admin/contact';
        }
        else
        {
            $fromPage = '/admin/contact/index/page/' . $fromPage . '.html';
        }

        Contact::model()->deleteByPk($contactId);
        $this->redirect($fromPage);
    }

    public function actionApprove()
    {
        $contactId = Yii::app()->request->getParam('id');
        $fromPage  = Yii::app()->request->getParam('page');
        if (!$fromPage)
        {
            $fromPage = '/admin/contact';
        }
        else
        {
            $fromPage = '/admin/contact/index/page/' . $fromPage . '.html';
        }
        $contact = Contact::model()->updateByPk($contactId,array('visibility' => 1));

        $this->redirect($fromPage);
    }

}