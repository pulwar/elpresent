<?php

class TeasersController extends Controller
{

    public $defaultAction = 'edit';

    public function actionEdit()
    {
        /** @var $teasers Teaser[] */
        $teasers = Teaser::model()->findAll();

        $this->render('teasers', compact('teasers'));
    }

    public function actionSave()
    {

        $request = $this->getRequest();
        if (!$request->getIsPostRequest()) {
            throw new CHttpException(400, 'Wrong HTTP method');
        }

        /** @var $teasers Teaser[] */
        $teasers = Teaser::model()->findAll();
        foreach($teasers as $teaser) {
            if (empty($_POST['teaser'][$teaser->id])) {
                continue;
            }

            $data = $_POST['teaser'][$teaser->id];
            foreach($data as $name=>$value) {
                $teaser->$name = $value;
            }
            // check file
            $teaser->imageFile = CUploadedFile::getInstanceByName('teaser_' . $teaser->id);
            $teaser->save();
        }

        $this->redirect('/admin/teasers');
    }

}