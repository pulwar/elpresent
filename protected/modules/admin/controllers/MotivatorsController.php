<?php
class MotivatorsController extends CController
{

    public function actionIndex()
    {
        $images = Motivators::model()->findAll();
        $this->render('index',array(
            'images' => $images,
        ));
    }

    public function actionUpload()
    {
        if ( isset($_POST['motivator']) ){
            $motivators = new Motivators();
            $motivators->image = CUploadedFile::getInstance($motivators,'image');
            $extension = $motivators->image->getExtensionName();
            $name = $motivators->generateRandomName().'.'.$extension;
            $ImagePath = YiiBase::getPathOfAlias('webroot').'/upload/motivators/'.$name;
            $motivators->image->saveAs($ImagePath);
            Yii::import('application.extensions.image.Image');
                $image = new Image($ImagePath);
                $image->resize(600,380);
                $image->save($ImagePath);

                $image->resize(183,114);
                $image->save(YiiBase::getPathOfAlias('webroot').'/upload/motivators/thumbs/'.$name);
           $motivators->image = $name;
           if($motivators->save()){
               $this->redirect($this->createUrl('motivators/index'));
           }

            
        }
        $this->render('upload');
    }

    public function actionDelete()
    {
        $id = Yii::app()->request->getParam('id');
        if(!$id){
            return;
        }
        $motivator = Motivators::model()->findByPk(intval($id));
            $bigImagePath   = Yii::getPathOfAlias('webroot').'/upload/motivators/'.$motivator->image;
            $smallImagePath = Yii::getPathOfAlias('webroot').'/upload/motivators/thumbs/'.$motivator->image;

            if ( file_exists($bigImagePath) )
            {
                unlink($bigImagePath);
            }

            if ( file_exists($smallImagePath) )
            {
                unlink($smallImagePath);
            }
        $motivator->delete();
        $this->redirect('/admin/motivators');
    }
}