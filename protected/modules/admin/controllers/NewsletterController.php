<? class NewsletterController extends CController
{
    public function filters() {
        return array(
            'accessControl'
        );
    }
    
    public function accessRules() {
        return array(
            array(
                'deny',
                'actions'	    => array('index', 'add', 'send'),
                'expression'    => '!$user->getIsAdmin()'
            )
        );
    }
    
	public function actionAdd()
	{
		$this->render('add');
	}

	public function actionSend()
	{
		$this->render('send');
	}

	public function actionIndex()
	{
		$this->render('index');
	}

	// -----------------------------------------------------------
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}
