<? class CardsController extends CController
{
    protected $_sizes = array
    (
        array('width' => 48, 'height' => 46),
        array('width' => 297, 'height' => 283)
    );
    
    public function actionIndex()
    {
        $oCards = new CActiveDataProvider('Card', array
        (
        	'pagination' => array
            (
            	'pageSize' => 20
            )
        ));
        
        $this->render('index', array
        (
            'oCards' => $oCards
        ));
    } 

    public function actionManage()
    {
        $cardId = Yii::app()->request->getParam('id');
        
        if ( !$cardId )
        {
            $oCard = new Card();
        }
        else
        {
            $oCard = Card::model()->findByPk($cardId);
        }
        
        if ( isset($_POST['Card']) )
        {
            $cardImageSaved    = $oCard->card_image;
            $oCard->card_title = $_POST['Card']['card_title'];
            $oCard->card_price = $_POST['Card']['card_price'];
            $oCard->card_image = CUploadedFile::getInstance($oCard, 'card_image');
            
            if ( $oCard->validate() )
            {
                if ( $oCard->card_image )
                {
                    $extension             = $oCard->card_image->getExtensionName();
                    $originalCardImage     = $oCard->generateRandomName();
                    $cardImagePath         = YiiBase::getPathOfAlias('webroot').'/upload/cards/'.$originalCardImage.'.'.$extension;
                    $cardThumbsImagePath   = YiiBase::getPathOfAlias('webroot').'/upload/cards/'.$originalCardImage;
                    $oCard->card_image->saveAs($cardImagePath);
                    $oCard->card_image = $originalCardImage;
                    
                    Yii::import('application.extensions.image.Image');
                    $image = new Image($cardImagePath);
                    
                    foreach ( $this->_sizes as $size )
                    {
                        $image->resize($size['width'], $size['height']);
                        $image->save($cardThumbsImagePath.'_'.$size['width'].'.png');    
                    }
                }
                else
                {
                    $oCard->card_image = $cardImageSaved;        
                }
                
                $oCard->save();
                
                Yii::app()->user->setFlash('card_message_successfully', 'Открытка успешно добавлены.');
                $this->redirect('/admin/cards');
            }
        }
        
        $this->render('manage', array
        (
            'oCard' => $oCard
        ));    
    } 

    public function actionDelete()
    {
        $cardId = Yii::app()->request->getParam('id');
        
        if ( $cardId )
        {
            $oCard = Card::model()->findByPk($cardId);
            $cardBigImagePath   = Yii::getPathOfAlias('webroot').'/upload/cards/'.$oCard->card_image.'_297.png';
            $cardSmallImagePath = Yii::getPathOfAlias('webroot').'/upload/cards/'.$oCard->card_image.'_48.png';
            
            if ( file_exists($cardBigImagePath) )
            {
                unlink($cardBigImagePath);
            }
            
            if ( file_exists($cardSmallImagePath) )
            {
                unlink($cardSmallImagePath);
            }
            
            $oCard->delete();
            
            $this->redirect('/admin/cards');
        }
    }
}
