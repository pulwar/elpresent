<?php

class ActionsController extends Controller
{
	private $_dir;

	public function init()
	{
		$this->_dir = Yii::getPathOfAlias('webroot.images');
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view', array(
			'model' => $this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new Actions('create');

		if (isset($_POST['Actions'])) {
			$model->attributes = $_POST['Actions'];

			$model = $this->_fileUpload($model);

			if ($model->save()) {
				$text = strip_tags($model->desc);
				$text .= ' Подробнее можно прочесть здесь: ' . 'http://' . $_SERVER['SERVER_NAME'] . '/actions/view?id=' . $model->id;
				$image = $this->_dir . DIRECTORY_SEPARATOR . $model->img;
				try {
					$vk = Vk::create(Yii::app()->params['vk_token']);
					$post = new VkPost($vk, null, Yii::app()->params['vk_group_id']);
					$post->post($text, $image);
					$this->redirect(array('view', 'id' => $model->id));
				} catch (Exception $e) {
					echo 'Error: <b>' . $e->getMessage() . '</b><br />';
					echo 'in file "' . $e->getFile() . '" on line ' . $e->getLine();
					die;
				}
			}
		}

		$this->render('create', array(
			'model' => $model,
		));
	}

	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);
		$model->scenario = 'update';

		if (isset($_POST['Actions'])) {

			$model->attributes = $_POST['Actions'];

			$model = $this->_fileUpload($model);

			if ($model->save()) {
				$this->redirect(array('view', 'id' => $model->id));
			}
		}

		$this->render('update', array(
			'model' => $model,
		));
	}

	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if (!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	private function _fileUpload($model)
	{
		if ($file = CUploadedFile::getInstance($model, 'img')) {
			$ext = pathinfo($file->getName(), PATHINFO_EXTENSION);
			$name_file = md5(time() . rand(1, 9)) . '.' . $ext;
			$file_path = $this->_dir . DIRECTORY_SEPARATOR . $name_file;
			$file->saveAs($file_path);
			$model->img = $name_file;
		}
		return $model;
	}

	public function actionIndex()
	{
		$model = new Actions('search');
		$model->unsetAttributes(); // clear any default values
		if (isset($_GET['Actions']))
			$model->attributes = $_GET['Actions'];

		$this->render('admin', array(
			'model' => $model,
		));
	}

	public function loadModel($id)
	{
		$model = Actions::model()->findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param User $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'user-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

}
