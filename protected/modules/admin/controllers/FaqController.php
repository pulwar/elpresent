<? 
class FaqController extends CAdminController
{
	public function getModel()
	{
		return Faq::model();
	}
	
    public function getControllerActions()
	{
		return array('add');
	}
	
    public function setModelRequestData($model)
	{
		$model->name     = (string)Yii::app()->request->getParam('name');
		$model->email    = (string)Yii::app()->request->getParam('email');
		$model->question = (string)Yii::app()->request->getParam('question');
	}
	
	public function getActions()
	{
		return array('edit', 'delete');
	}
}