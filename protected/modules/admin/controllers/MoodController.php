<?php
class MoodController extends CController
{

    public function actionIndex()
    {

        $moodProfile = MoodProfile::model()->findAll();
        $moodProfile = new CActiveDataProvider('MoodProfile');
        $this->render('index',array(
            'profile' => $moodProfile,
        ));
    }

    public function actionManageprofile()
    {
        $id = Yii::app()->request->getParam('id');
        if ( !$id )
        {
            $profile = new MoodProfile();
        }
        else
        {
            $profile = MoodProfile::model()->findByPk(intval($id));
        }
        if(!empty($_POST['profile'])){
            $profile->name = $_POST['name'];
            $profile->profile = $_POST['profile'];
            $profile->post = $_POST['post'];
            $profile->profile_order = !empty($_POST['profile_order']) ? intval($_POST['profile_order']) : 0;
            $profile->visible = intval($_POST['visible']);
            if($profile->save()){
                $this->redirect('/admin/mood');
            }
        }

        $this->render('manageprofile',array(
            'profile' => $profile,
        ));
    }

    public function actionManagemoods()
    {
       Yii::app()->clientScript->registerCssFile('/css/moods.css');
       $id = Yii::app()->request->getParam('id');
       if ( !$id )
        {
            $this->redirect('admin/mood');
        }
        else
        {
            $moods = Mood::model()->findAllByAttributes(array('profile_id' => $id));
        }
        $this->render('managemoods',array(
                'moods' => $moods,
                'profile' => $id,
        ));
    }

    public function actionManagemood()
    {
        $id = Yii::app()->request->getParam('id');
        if ( !$id )
        {
            $this->redirect('admin/mood');
        }
        $mood_id = Yii::app()->request->getParam('mood_id');
        if ( !$mood_id)
        {
            $mood= new Mood();
            $mood->visible = 1;
        }
        else
        {
            $mood = Mood::model()->findByPk(intval($mood_id));
        }
        if ( isset($_POST['Mood']) )
        {
            $imageSaved = $mood->image;
            $mood->attributes = $_POST['Mood'];
            $mood->profile_id = $id;
            $mood->image = CUploadedFile::getInstance($mood,'image');
            if($mood->image){
                $imagepath = YiiBase::getPathOfAlias('webroot').'/upload/moods/';
                Yii::import('application.extensions.image.Image');
                //$image = CUploadedFile::getInstancesByName('file');
                              
                $extension = $mood->image->getExtensionName();
                $name = $mood->generateRandomName().'.'.$extension;
                $mood->image->saveAs($imagepath.$name);
                $mood->image = $name;
                $image = new Image($imagepath.$name);
                $image->resize(200, 200);
                $image->save($imagepath.$name);

            }else{
               $mood->image = $imageSaved;
            }
            
            if(!$mood->mood_order){
                $mood->mood_order = 0;
            }

            if($mood->save()){
                MoodItems::addItems(!empty($_POST['Mood_items']) ? $_POST['Mood_items'] : array(),$mood->id);
                $this->redirect($this->createUrl('mood/managemoods', array('id' => $id)));
            }

        }

        $this->render('managemood', array(
            'mood' => $mood,
        ));
    }

    public function actionAddmood()
    {
        
        $this->render('addmood');
    }

    public function actionDeletemood()
    {
        $id = Yii::app()->request->getParam('id');
        if(!$id){
            return;
        }
        $mood = Mood::model()->findByPk(intval($id));
            $imagePath   = Yii::getPathOfAlias('webroot').'/upload/moods/'.$mood->image;
            if ( file_exists($imagePath) )
            {
                unlink($imagePath);
            }

        $mood->delete();
        $this->redirect('/admin/mood');
    }

    public function actionSearchItems()
    {
        if(empty($_POST['search'])) {
            throw new CHttpException(404,'Page not found');
        }
        $search = $_POST['search'];

        $criteria = new CDbCriteria();
        $criteria->addSearchCondition('title', $search);
        $criteria->limit = 10;

        $items = Item::model()->findAll($criteria);

        $results = array();

        foreach ($items as $item) {
            $results[] = array(
                'item_id' => $item->id,
                'title'   => CHtml::encode($item->title),

            );
        }

        echo json_encode($results);
        die();
    }
}