<?php

class SuperitemController extends CAdminController
{

    public function getModel()
    {
        return Superimpression::model();
    }

    public function setModelRequestData($model)
    {

    }

    public function actionIndex()
    {
        $groupItems = $this->getModel()->with('childs')->findAll();

        $dataProvider=new CActiveDataProvider('Superimpression', array(
            'criteria'=>array(
            ),
            'pagination'=>array(
                'pageSize'=>20,
            )
        ));

        $this->render('index', array(
            'items'=>$dataProvider,
        ));
    }

    public function actionEdit()
    {
        $aItemFeatures = array();
        $itemId = intval(Yii::app()->request->getParam('id'));
        if($itemId)
        {
            $item = Superimpression::model()->with('childs')->findByPk($itemId);
            if(!$item) {
                throw new CHttpException(404);
            }

            $oItemFeaturesValues = FeatureValueItem::model()->findAllByAttributes(array('item_id' => $itemId));

            if ($oItemFeaturesValues)
            {
                foreach ($oItemFeaturesValues as $oItemFeaturesValue)
                {
                    if ($oItemFeaturesValue->feature_value_id)
                    {
                        $oFeatureValue = FeatureValue::model()->findByPk($oItemFeaturesValue->feature_value_id);
                        $aItemFeatures[$oFeatureValue->feature_id][] = $oItemFeaturesValue->feature_value_id;
                    }
                }
            }
        }
        else
            $item = new Superimpression();

        $isNew = $item->isNewRecord;

        if($this->getRequest()->isPostRequest && isset($_POST['Superimpression']) )
        {
            $attributes = $_POST['Superimpression'];

            $item->title = $attributes['title'];
            $item->desc = $attributes['desc'];
            $item->text = $attributes['text'];
//			$item->price = $attributes['price'];
            $item->draft = $attributes['draft'];

            $item->seo_title = $attributes['seo_title'];
            $item->description = $attributes['description'];
            $item->keywords = $attributes['keywords'];


            if (!empty($_FILES['Superimpression']['name']['preview_img'])) {
                $item->preview_img = CUploadedFile::getInstance($item, 'preview_img');
            }
            if (!empty($_FILES['Superimpression']['name']['full_img'])) {
                $item->full_img = CUploadedFile::getInstance($item, 'full_img');
            }

            if( $item->validate() ) {
                Yii::import('application.extensions.image.Image');

                foreach(array('preview_img', 'full_img') as $field) {
                    if ($item->$field instanceof CUploadedFile) {

                        $newFileName = uniqid() . '.' . $item->$field->getExtensionName();
                        $path = Yii::getPathOfAlias('webroot.images.superimpressions');
                        $item->$field->saveAs($path . DIRECTORY_SEPARATOR . $newFileName);
                        $item->$field = $newFileName;
                        // resize image
                        $image = new Image($path . DIRECTORY_SEPARATOR . $newFileName);
                        $size = 300;
                        if ($image->width > $image->height)
                        {
                            $coeff = $image->width / $image->height;
                            $image->resize($size, round($size / $coeff));
                        }
                        else
                        {
                            $coeff = $image->height / $image->width;
                            $image->resize(round($size / $coeff), $size);
                        }
                        $image->save();
                    }
                }


                if ( $item->save() ) {
                    $this->saveRelations(($itemId ? $itemId:$item->id), $item->isNew());
                    Yii::app()->user->setFlash('success', 'Супервпечатление успешно ' . ( $isNew ? "добавлена":"измеено") );
                    CategoryItems::addItems($_POST['Category'], ($itemId ? $itemId:$item->id));
                    SubcategoryItems::addItems($_POST['Subcategory'], ($itemId ? $itemId:$item->id));

                    FeatureValueItem::model()->deleteAllByAttributes(array('item_id' => ($itemId ? $itemId:$item->id)));

                    foreach ($this->getRequest()->getParam('features', array()) as $featureValues)
                    {
                        foreach ($featureValues as $featureValue)
                        {
                            $oFeatureValueItem = new FeatureValueItem();
                            $oFeatureValueItem->item_id = ($itemId ? $itemId:$item->id);
                            $oFeatureValueItem->feature_value_id = $featureValue;
                            $oFeatureValueItem->save();
                        }
                    }
                    $this->redirect('/admin/superitem.html');
                }
            }

            $ids = array();
            foreach($_POST['Items']['id'] as $id) {
                $ids[] = intval($id);
            }
            $ids = array_unique($ids);
            $criteria = new CDbCriteria();
            $criteria->addInCondition('id', $ids);
            $childs = Item::model()->findAll($criteria);
            $item->childs = $childs;

        }

        $categoriesTree = Category::getTree();
        $categories = [];
        foreach ($categoriesTree as $node) {
            $categories[$node['id']] = $node['title'];
        }
        $subcategories = [];
        $subcategoriesOptions = [];
        foreach ($categoriesTree as $node) {
            foreach ($node['children'] as $child) {
                $subcategories[$child['id']] = $child['title'];
                $subcategoriesOptions[$child['id']] = ['data-owner' => $node['id']];
            }
        }

        $features = Feature::model()->findAll();
        $this->render('edit', [
            'model' => $item,
            'isNew' => $isNew,
            'subcategories' => $subcategories,
            'subcategoriesOptions' => $subcategoriesOptions,
            'categories' => $categories,
            'features' => $features,
            'aItemFeatures' => $aItemFeatures,
        ]);
    }

    protected function saveRelations($itemId, $isNew = false)
    {
        $sqlQueue = array();

        if(isset($_POST['Items']['id']) && $itemId)
        {
            // forgive me satan

            // generate delete statements
            if(!$isNew && $itemId) {
                $sql = "DELETE FROM {{item_item}} WHERE item_id=".$itemId;
                $sqlQueue[] = $sql;
            }

            // generate insert statements
            foreach($_POST['Items']['id'] as $id) {
                $sql = "INSERT INTO {{item_item}}  (`item_id`, `child_id`) VALUES (".$itemId.", ".$id.")";
                $sqlQueue[] = $sql;
            }
            foreach($sqlQueue as $sql) {
                $r = Yii::app()->db->createCommand($sql)->execute();
            }
        }
    }

    public function actionDelete()
    {
        $itemId = intval(Yii::app()->request->getParam('id'));
        $sql = 'DELETE FROM {{item_item}} WHERE `item_id`='.$itemId;
        GroupItem::model()->deleteByPk($itemId);
        Yii::app()->db->createCommand($sql)->execute();
        Yii::app()->user->setFlash('success', 'Супервпечатление было успешно удалено.');
        $this->redirect($this->createUrl("/admin/superitem"));
    }

    public function actionSearch()
    {
        $query = $_POST['query'];

        $criteria = new CDbCriteria();
        if( $query )
            $criteria->compare('title', $query, true);
        $criteria->select = "title, id";

        echo json_encode(array_map(function (Item $item) {
            return [
                'title' => $item->title,
                'id' => $item->id
            ];
        }, Item::model()->findAll($criteria)));

        Yii::app()->end();
    }

    public function actionGetItemThumb()
    {
        $cities = City::getCities();
        $id = intval($_POST['id']);
        $item = Item::model()->with('images')->findByPk($id);
        /** @var Item $item */
        echo json_encode([
            'id' => $item->id,
            'title' => $item->title,
            'price' => $item->getPrice(),
            'allPrices' => $item->getItemPriceSelection($cities),
            'cities' => City::cityListToArray($cities),
            'preview_img' => $item->preview_img
        ]);

        Yii::app()->end();
    }

}
