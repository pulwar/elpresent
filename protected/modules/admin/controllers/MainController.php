<? class MainController extends CController
{
    public function filters() {
        return array(
            'accessControl'
        );
    }
    
    public function accessRules() {
        return array(
            array(
                'deny',
                'actions'	    => array('index'),
                'expression'    => '!$user->getIsAdmin()'
            )
        );
    }
	
	public function actionIndex()
	{
		$this->render('index');
	}
}
?>
