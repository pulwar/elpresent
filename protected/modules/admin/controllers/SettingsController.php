<?php

class SettingsController extends Controller
{
    public function actionIndex()
    {
        if ( count($_POST) )
        {
            foreach ( $_POST as $settingAlias => $settingValue )
            {
                $oSetting = Settings::model()->findByAttributes(array('setting_alias' => $settingAlias));
                $oSetting->setting_value = $settingValue;
                $oSetting->save();
            }
            
            Yii::app()->user->setFlash('setting_message_ok', 'Настройки сохранены');
        }
        
        $this->render('index');
    }

    public function actionSeo()
    {
        $settings = Settings::model()->findByAttributes([
            'setting_alias' => 'main_page_seo_text'
        ]);

        if (!$settings) {
            $settings = new Settings();
            $settings->setting_alias = 'main_page_seo_text';
            $settings->setting_value = '';
        }

        $form = new CForm([
            'title'=>'',

            'elements'=>[
                'setting_value' => [
                    'type' => 'ext.tinymce.TinyMce',
                    'label' => false,
                    'fileManager' => [
                        'class' => 'ext.elFinder.TinyMceElFinder',
                        'connectorRoute'=>'admin/elfinder/connector',
                    ],
                ]
            ],

            'buttons'=>[
                'save'=>[
                    'type'=>'submit',
                    'label'=>'Сохранить',
                    'class' => 'btn btn-primary'
                ],
            ],
        ], $settings);

        $request = $this->getRequest();
        if ($request->isPostRequest && $form->submitted('save')) {
            $settings = $form->getModel();
            if ($settings->save()) {
                Yii::app()->user->setFlash('setting_message_ok', 'SEO текст сохранен');
                $this->redirect('/admin/settings/seo');
            }
        }

        $this->render('seo', compact('form'));
    }
}
