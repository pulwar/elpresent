<? 
class HolidayController extends CAdminController
{

    public function getModel()
    {
        return Holiday::model();
    }

    public function getData()
    {
        return Holiday::findAllActual();
    }

    public function setModelRequestData($model)
    {
        $model->desc = (string) Yii::app()->request->getParam('desc');
        $model->title = (string) Yii::app()->request->getParam('title');
        $model->day_id = (int) Yii::app()->request->getParam('day_id');
        $model->is_calendar = (bool) Yii::app()->request->getParam('is_calendar');
        $model->is_important = (bool) Yii::app()->request->getParam('is_important');
    }

    public function getListModel()
    {
        $model = $this->getModel();
        $model->getDbCriteria()->addColumnCondition(array('day_id' => Yii::app()->request->getParam('day_id')));
        return $model;
    }

    public function defaultGetActionLink($model, $action)
    {
        $url = $this->createUrl($action, array('id' => $model->id, 'day_id' => Yii::app()->request->getParam('day_id')));
        return "<a href='{$url}'>[{$action}]</a>";
    }

    public function getControllerActionLinks()
    {
        $url = $this->createUrl('add', array('day_id' => Yii::app()->request->getParam('day_id')));
        return "<a href='{$url}'>Add</a>";
    }

    public function createUrl($route, $data = array(), $amp = '&')
    {
        $data['day_id'] = Yii::app()->request->getParam('day_id');
        return parent::createUrl($route, $data, $amp);
    }

}
