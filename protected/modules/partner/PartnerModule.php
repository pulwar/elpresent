<?php

class PartnerModule extends CWebModule
{
    /** @var */
    public $user;

    public function preinit()
    {

        $this->import();
        //checks user permissions
        Yii::app()->user->loginUrl = Yii::app()->urlManager->createUrl('partner/login/');
        Yii::app()->urlManager->appendParams = false;

        //change db timezone for admins convinience
        self::changeTimezone();

        // set jQuery to 1.8.1
        Yii::app()->setComponent('clientScript', new CClientScript());
        Yii::app()->clientScript->scriptMap = array(
            'jquery.min.js' => '/js/jquery-1.8.1.min.js',
            'jquery-ui.min.js' => '/js/jquery-ui-1.9.1.custom.min.js'
        );
        Yii::app()->clientScript->registerCoreScript('jquery');

        yii::import('ext.bootstrap.components.Bootstrap');
        yii::import('ext.ext.tinymce.*');
        Yii::app()->setComponent('bootstrap', new Bootstrap());
    }

    private static function changeTimezone()
    {
        $sql = 'SET time_zone = "' . Yii::app()->getParams()->elTimezone['timezone'] . '"';
        Yii::app()->db->createCommand($sql)->query();
    }

    /**
     * @param CController $controller
     * @return bool
     */
    private function checkPermissions($controller)
    {
        return $this->isUserNotGuest() && $this->isUserHasPerms($controller);
    }

    private function isCurrentRequestIsLoginPageRequest($controller)
    {
        return $controller->id == 'login';
    }

    private function isUserNotGuest()
    {
        return (bool)Yii::app()->user->getId();
    }

    /**
     * @param CController $controller
     * @return bool
     */
    private function isUserHasPerms($controller)
    {
        $model = User::model()->findByPk(Yii::app()->user->getId());
        if ($model->user_role == 'partner') {
            return true;
        }
        $controller->redirect('/');

        //throw new CHttpException(401,'You has not enought permssions to see this page');
    }

    private function import()
    {
        $this->setImport(array(
            'partner.controllers.*',
            'partner.models.*',
            'partner.components.*',
            'application.models.*',
//            'admin.widgets.*',
            //'application.widgets.*',
//            'zii.widgets.*',
//            'admin.modelView.*',
//            'admin.modelView.types.*'
        ));
    }


    public function init()
    {
        $this->layoutPath = dirname(__FILE__) . '/views/layouts';
        $this->viewPath = dirname(__FILE__) . '/views';
        $this->layout = 'main';
    }

    /**
     * @param CController $controller
     * @param CAction $action
     * @return bool
     */
    public function beforeControllerAction($controller, $action)
    {
        $this->checkAuth($controller);
        if (parent::beforeControllerAction($controller, $action)) {
            if (!$this->isCurrentRequestIsLoginPageRequest($controller) && !$this->checkPermissions($controller)) {
                $controller->redirect('/partner/login');
            }
            return true;
        }
        return false;
    }

    /**
     * @param CController $controller
     */
    protected function checkAuth($controller)
    {
        /** @var CHttpSession $session */
        $session = Yii::app()->getSession();
        $partnerId = $session->get('partner_id');

        if (!empty($partnerId)) {
            $user = User::model()->findByPk($partnerId);
            $identity = new UserIdentity($user->user_email, $user->user_password, true);
            if ($identity->authenticate()) {
                Yii::app()->user->login($identity);
            }

            $session->add('partner_id', '');
            $controller->redirect('/', true);
        }
    }
}
