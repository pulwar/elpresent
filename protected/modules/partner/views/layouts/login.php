<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title>Elpresent :: Partner Panel</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" type="text/css" href="/css/admin/style.css" />
	<link rel="stylesheet" type="text/css" href="/css/admin/prompt.css" />
	<link rel="stylesheet" type="text/css" href="/css/admin/ui.css" />
    <!--[if IE 6]>
        <link rel="stylesheet" type="text/css" href="ie6.css" />
    <![endif]-->
</head>
<body class="login-screen">
	<div class="container">
		<div class="admin-main">
		   <div class="form-horizontal">
           <?= $content ?>
           </div>
		</div>
	</div>
</body>
</html>
