<!DOCTYPE html>
    <head>
        <title>Elpresent :: Partner Panel</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
            <!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PBTX93Z');</script>
<!-- End Google Tag Manager -->
        <link rel="stylesheet" type="text/css" href="/css/chosen.css" />
        <link rel="stylesheet" type="text/css" href="/css/partner/style.css?v=1" />
        <link rel="stylesheet" type="text/css" href="/css/admin/pack.css" />
        <link rel="stylesheet" type="text/css" href="/css/smoothness/jquery-ui-1.9.1.custom.min.css" />
        <link rel="stylesheet" type="text/css" href="/js/datetimepicker/bootstrap-datetimepicker.min.css" />
        <!--[if IE 6]>-->
<!--            <link rel="stylesheet" type="text/css" href="ie6.css" />-->
        <![endif]-->        		
		<?php
            Yii::app()->clientScript->registerCoreScript('cookie');
            $baseUrl = '';//Yii::app()->baseUrl;
			$cs = Yii::app()->clientScript;
			//$cs->registerCoreScript('jquery');
            $cs->registerScriptFile($baseUrl.'/js/functions.js');
            $cs->registerScriptFile($baseUrl.'/js/partner/script.js');
			$cs->registerScriptFile($baseUrl.'/js/jquery.facebox.js');
			$cs->registerScriptFile($baseUrl.'/js/jquery.fbox.js');
			$cs->registerScriptFile($baseUrl.'/js/jquery.easing.js');
			$cs->registerScriptFile($baseUrl.'/js/jquery.lavalamp.js');
			$cs->registerScriptFile($baseUrl.'/js/chosen.jquery.min.js');
			$cs->registerScriptFile($baseUrl.'/js/jquery.pause.js');
			$cs->registerScriptFile($baseUrl.'/js/admin/instruction.js');
            $cs->registerScriptFile($baseUrl.'/js/jquery.easing.js');
            $cs->registerScriptFile($baseUrl.'/js/datetimepicker/bootstrap-datetimepicker.min.js');
            $cs->registerScriptFile($baseUrl.'/js/datetimepicker/bootstrap-datepicker.ru.min.js');
            $cs->registerScriptFile($baseUrl.'/js/jquery.sort.js');
            $cs->registerScriptFile($baseUrl.'/js/admin/script.js?v=1.0.1');
		?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>

    <style>
            .no-row-margin .row {
                margin-left: 0 !important;
                padding-left: 10px;
            }
        </style>
    </head>
    <body>
    <!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PBTX93Z"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
        <header>
            <div class="container">
                <div class="header-inner">
                    <a href="/" target="_blank"><img src="img/logo.svg" alt="logo"/></a>
                    <div class="nav-medium">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                    <div class="header-nav">
                        <a class="nav-item" href="/contact">Контакты</a>
                        <a class="nav-item" href="/site/landing">Корпоративным клиентам</a>
                        <a class="nav-item" href="/files/politicy-elpresent.pdf">Политика конфиденциальности</a>
                    </div>
                    <div class="user-info">
                        <div class="login_cell_ico"></div>
                        <span>User</span>
                        <i></i>
                    </div>
                    <div class="user-menu">
                        <div class="user-menu-item"><a href="/profile/profile">Профиль</a></div>
                        <div class="user-menu-item"><a href="/profile/logout">Выйти</a></div>
                    </div>
                    <div class="header-nav-mob">
                        <i class="nav-close"></i>
                        <div><a href="/profile/profile">
                                <div class="login_cell_ico"></div>
                                <span>User</span>
                            </a></div>
                        <div> <a href="/contact">Контакты</a></div>
                        <div><a href="/site/landing">Корпоративным клиентам</a></div>
                        <div><a href="/files/politicy-elpresent.pdf">Политика конфиденциальности</a></div>
                        <div><a href="/profile/logout">Выйти</a></div>
                    </div>
                </div>
            </div>
        </header>
        <div class="container">
<!--            <header class="header">-->
<!--                <nav class="container">-->
<!--                    --><?php //$this->widget('partner.components.PartnerMainMenu'); ?>
<!--                </nav>-->
<!--            </header>-->
            <?php echo $content; ?>
        </div>
        <div id="partnerInfo">
            <div class="partnerInfoInner">
                <a href="javascript:void(0);" class="close"></a>
                <p>
                    <span class="label label-danger"></span>
                </p>
                <section></section>
            </div>
        </div>
    </body>
</html>