<?php
/** @var CActiveDataProvider $orderItems */
Yii::app()->clientScript->registerScriptFile('/js/datepicker/bootstrap-datepicker.js');
Yii::app()->clientScript->registerCssFile('/js/datepicker/datepicker.css');
$countOrderItems = count($orderItems->getData());
?>
<style>
    .well.searchBlock {
        background-color : #ff7f26;
        border-color: #fa7a21;
        margin-top: 55px;
    }

    .certificate-info>.tab-content {
        border:none;
    }
    div.orderItemStatus{
        text-align: center;
        /*font-weight: normal;*/
        /*font-size: 30px;*/
        /*line-height: 40px;*/
        margin-top: 8px;
    }
    input[id=codeCert] {
        width: 120px;
    }
    .activationDateLabel {
        float: right;
        font-size: 13px;
        margin-right: 5px;
    }
</style>

<div class="help-box">
    При возникновении вопросов, воспользуйтесь <a class="note-link" href="https://docs.google.com/document/d/13vN3Jva3wgIH5WItq6r-UfU3R3_39VRW/edit?usp=sharing&ouid=112148712616922956078&rtpof=true&sd=true" target="_blank">инструкцией</a> по работе с личным кабинетом либо свяжитесь
    с нами по телефону <a class="tel-link" href="tel:+375296878728">+375 (29) 687-87-28</a>
</div>

<div class="tab-wrap">
    <div class="tab-header">
        <div class="tab-item selected">Отчет</div>
        <div class="tab-item">Проверить сертификат</div>
    </div>
    <div class="tab-content">

        <span id="activateInfo">Погашенный сертификат №<span id="certNum"></span> <span id="certActivDate"></span>, для отображения в таблице выберите период <span id="proposedPeriod"></span></span>
        <?/*php if(count($orderItems->getData()) != 0 || !empty($filter)) { */?>
        <form method="GET" class="form form-horizontal">
            <div class="control-group controls-row period-line">
                <div class="data-wrap">
                    <span>Период с</span>
                    <div id="fromDate" class="input-append date">
                        <input class="input-small"  data-format="yyyy-MM-dd" type="text" name="filter[date][from]" value="<?=!empty($filter['date']['from'])?$filter['date']['from']:'';?>"/>
                        <span class="add-on">
                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                        </span>
                    </div>
                </div>
                <div class="data-wrap">
                    <span>по</span>
                    <div id="toDate" class="input-append date">
                        <input class="input-small" data-format="yyyy-MM-dd" type="text" name="filter[date][to]" value="<?=!empty($filter['date']['to'])?$filter['date']['to']:'';?>"/>
                        <span class="add-on">
                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                        </span>
                    </div>
                </div>
                <input type="submit" value="Показать" class="btn btn-primary"/>
            </div>
        </form>
        <?/* } */?>

        <div class="order-list affix-top">
            <? $this->renderPartial('_itemsTable', ['orderItems' => $orderItems, 'sum' => $sum]); ?>
        </div>

    </div>

    <div class="tab-content">

        <form id="formPartnerCheckCert" method="GET" class="form form-horizontal">
            <div class="control-group controls-row">
                <div>
                    <div class="line-info check-line-info">
                        <span>Введите номер сертификата</span>
                        <input id="codeCert" maxlength="12" placeholder="000000000000" class="input-small" type="text" name="cert_code"/>
                        <input id="checkCert" type="submit" value="Проверить" class="btn btn-primary" />

                    </div>
                    <div class="selectActiveImpression line-info check-line-info" style="display: none;">
                        <span>Оказываемая услуга</span>
                        <div class="select-wrap">
                            <select name="activated_item" id="activation_popup_activated_item" class="form-control select-font">
                            </select>
                        </div>
                    </div>
                    <div style="display:none;" id="activateDate" class="line-info check-line-info">
                        <span style="display:none;" class="activationDateLabel">Дата оказания услуги</span>
                        <div class="input-append date">
                            <input class="input-small disable-date" disabled style="margin-right: 0" data-format="Y-m-d" type="text" name="activateDate" value="<?=date('Y-m-d')?>"/>
                        </div>
                        <input id="activateCert" disabled type="submit" value="Погасить" class="btn btn-primary" />
                    </div>
                    <div class="certificate-info"  data-offset-top="150">
                        <?=$infoMessage?>
                    </div>
                </div>

            </div>
        </form>

    </div>
</div>

<!--old-->

<div class="row" style="display: none">
    <div class="span6">
        <div class="span12">
            <h1 style="font-weight: normal; font-size: 30px;">Здравствуйте, <?=$userName?></h1>
        </div>
        <div class="well searchBlock">

            <div>
                <form id="formPartnerCheckCert" method="GET" class="form form-horizontal">
                    <div class="control-group controls-row">
                        <div>
                            <div>

                                <strong>Введите номер сертификата (12 цифр)</strong>
                                <br>
                                <input id="codeCert" maxlength="12" placeholder="000000000000" class="input-small" type="text" name="cert_code"/>
                                <input id="checkCert" type="submit" value="Проверить" class="btn btn-primary" />
                                <input id="activateCert" disabled type="submit" value="Погасить" class="btn btn-primary" />
                            </div>
                            <div style="display:none;" id="activateDate" class="input-append date">
                                <strong><span style="display:none;" class="activationDateLabel">Дата оказания услуги</span></strong>
                                <br>
                                <input class="input-small" data-format="yyyy-MM-dd" type="text" name="activateDate" value="<?=date('Y-m-d')?>"/>
                                <span class="add-on">
                                    <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                                </span>
                            </div>
                        </div>
                        <div class="selectActiveImpression" style="display: none;">
                            <strong>Выберите оказываемую услугу</strong>
                            <br>
                            <div>
                                <select name="activated_item" id="activation_popup_activated_item" class="form-control select-font">
                                </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <span id="activateInfo">Погашенный сертификат №<span id="certNum"></span> <span id="certActivDate"></span>, для отображения в таблице выберите период <span id="proposedPeriod"></span></span>
        <?/*php if(count($orderItems->getData()) != 0 || !empty($filter)) { */?>
            <form method="GET" class="form form-horizontal">
                <div class="control-group controls-row">
                    <span>Период с</span>
                    <div id="fromDate" class="input-append date">
                        <input class="input-small" data-format="yyyy-MM-dd" type="text" name="filter[date][from]" value="<?=!empty($filter['date']['from'])?$filter['date']['from']:'';?>"/>
                        <span class="add-on">
                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                        </span>
                    </div>
                    <span>по</span>
                    <div id="toDate" class="input-append date">
                        <input class="input-small" data-format="yyyy-MM-dd" type="text" name="filter[date][to]" value="<?=!empty($filter['date']['to'])?$filter['date']['to']:'';?>"/>
                        <span class="add-on">
                            <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
                        </span>
                    </div>
                    <input type="submit" value="Показать" class="btn btn-primary"/>
                </div>
            </form>
        <?/* } */?>

        <div class="order-list affix-top">
            <? $this->renderPartial('_itemsTable', ['orderItems' => $orderItems, 'sum' => $sum]); ?>
        </div>

</div>
<div class="span6" style="padding-top: 30px;">
    <!--data-spy="affix"-->
    <div class="certificate-info"  data-offset-top="150">
        <?=$infoMessage?>
    </div>
</div>