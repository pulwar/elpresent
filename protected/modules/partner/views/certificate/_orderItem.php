<? /** @var OrderItems $data */ ?>
<tr>
    <td>
        <?=date('Y-m-d', strtotime($data->partner_activated_date))?>
    </td>
    <td>
        <?=$data->code?>
    </td>
    <td>
        <?php if ($data->itemObj->type == 'item') { ?>
            <a href="<?= path('catalog/item', ['perm_link' => $data->itemObj->perm_link]); ?>"><?= $data->itemObj->title; ?></a>
        <?} else { ?>
            <a href="<?= path('catalog/item', ['perm_link' => $data->activatedItemObj->perm_link]); ?>"><?= $data->activatedItemObj->title; ?></a>
        <?}?>
    </td>
    <td>
        <? if(!empty($data->optionObj)):?>
            <?=$data->optionObj->name?>
        <? endif;?>
        <? if(!empty($data->orderObj->option->name)):?>
            <?=$orderItem->orderObj->option->name?>
        <? endif;?>
    </td>

    <td>
        <?=number_format($data->partner_price, 2, '.', '')?>
    </td>

</tr>