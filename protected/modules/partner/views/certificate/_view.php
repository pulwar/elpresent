<?php
/** @var OrderItems $orderItem */

function getStatusName($orderItem) {
    $statusStr = '';
    $class = '';
    if (empty($orderItem)) {
        $statusStr = '<div class="no-found">Сертификат не найден <i class="no-found-icon"></i></div> 
<div class="no-found-note"><div class="no-found-close"></div>Проверьте правильность введеного вами номера: 12 цифр, без пробелов. Если у вас остались вопросы, 
свяжитесь с нами по телефону <a class="tel-link" href="tel:+375296878728">+375 (29) 687-87-28</a></div>';
    } else {
        switch (true) {
            case $orderItem->partner_activated:
                $statusStr = '<div class="found-activated">Использован '.date('Y-m-d', strtotime($orderItem->partner_activated_date)).'</div>';
                break;
            case $orderItem->isOverdue();
                $statusStr = '<div class="no-found">Сертификат просрочен <i class="no-found-icon"></i></div> 
<div class="no-found-note"><div class="no-found-close"></div>При истечении срока действия сертификата, всех клиентов связывайте с нами по телефону <a class="tel-link" href="tel:+375296878728">+375 (29) 687-87-28</a></div>';
                break;
            default:
                $statusStr = '<div class="found">Сертификат найден</div>';
                break;
        }
    }

    return "<div class=\"orderItemStatus $class\">$statusStr</div>";
}

?>
<div class="tab-content">
    <?=getStatusName($orderItem)?>
    <? if (!empty($orderItem)) { ?>
        <div class="orderItemCard well">
            <div class="table-info">
                Информация о подарочном сертификате
            </div>
            <table class="table">
                <tbody>
                    <tr>
                        <td>Наименование впечатления</td>
                        <td>
                            <?php if ($orderItem->itemObj->type == 'item') { ?>
                                <a href="<?= path('catalog/item', ['perm_link' => $orderItem->itemObj->perm_link]); ?>"><?= $orderItem->itemObj->title; ?></a>
                            <?} else { ?>
                                <a href="<?= path('catalog/item', ['perm_link' => $orderItem->activatedItemObj->perm_link]); ?>"><?= $orderItem->activatedItemObj->title; ?></a>
                            <?}?>
                        </td>
                    </tr>
                    <? if(!empty($orderItem->optionObj)):?>
                        <tr>
                            <td>Опция: </td>
                            <td> <?=$orderItem->optionObj->name?></td>
                        </tr>
                    <? endif;?>
                    <? if(!empty($orderItem->orderObj->option->name)):?>
                        <tr>
                            <td>Опция: </td>
                            <td> <?=$orderItem->orderObj->option->name?></td>
                        </tr>
                    <? endif;?>
                    <tr>
                        <td>Срок действия</td>
                        <td>до <?=$orderItem->getOverdueTime()?></td>
                    </tr>
                    <tr>
                        <td>Сумма к оплате партнёру</td>
                        <td><?=$orderItem->partner_price?> р.</td>
                    </tr>
                    <tr>
                        <td>Номер сертификата</td>
                        <td><?=$orderItem->code?> </td>
                    </tr>
                    <tr>
                        <td>Имя кто активировал</td>
                        <td><?=$orderItem->recivier_name. ' '. $orderItem->recivier_lname?> </td>
                    </tr>
                    <tr>
                        <td>Телефон кто активировал</td>
                        <td><?=$orderItem->recivier_phone?> </td>
                    </tr>

                </tbody>
            </table>
        </div>
    <? } ?>
</div>