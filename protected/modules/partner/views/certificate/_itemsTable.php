<?php if(count($orderItems->getData()) != 0) { ?>
<!--    <div style="text-align: center;"><strong>Список оказанных услуг по подарочным сертификатам</strong></div>-->
    <?php $this->widget('ext.bootstrap.widgets.TbListView', array(
        'dataProvider' => $orderItems,
        'itemView'=>'_orderItem',
        'itemsTagName' => 'test',
        'template' => '
                        <table class="table orderItemsTable">
                            <thead>
                                <tr>
                                    <th scope="col">Дата</th>
                                    <th scope="col">Номер</th>
                                    <th scope="col">Наименование</th>
                                    <th scope="col">Опция</th>
                                    <th scope="col">Сумма к оплате партнёру</th>
                                </tr>
                            </thead>
                            <tbody>
                            {items}
                            <tr class="totalPrice"><td>Итог:</td><td></td><td></td><td></td><td>'.$sum.'</td></tr>
                            </tbody>
                        </table>
                        {pager}
                    ',
    )); ?>
<? } else {?>
    Нет результатов
<? } ?>