<?php

/**
 * Class CertificateController
 */
class CertificateController extends Controller
{
    public function actionIndex()
    {
        /** @var CHttpRequest $request */
        $request = Yii::app()->request;
        $filter = $request->getParam('filter', []);

        if (empty($filter)) {
            $filter['date']['from'] = date('Y-m-01');
            $a_date =  date('Y-m-d');
            $filter['date']['to'] =  date("Y-m-t", strtotime($a_date));
        }

        $criteria = new CDbCriteria();

        $this->assignFilter($criteria, $filter);
        $provider = new CActiveDataProvider('OrderItems', [
            'criteria' => $criteria,
            'pagination' => false
        ]);

        $this->render(
            'index',
            [
                'userName' => Yii::app()->user->getUser()->user_firstname,
                'infoMessage' => '<p class="empty span5"></p>',
                'orderItems' => $provider,
                'filter' => $filter,
                'sum' => number_format($this->sumPrice($provider), 2, '.', ''),
            ]
        );
    }

    protected function assignFilter(CDbCriteria $criteria, $filter)
    {
        $user = User::model()->findByPk(Yii::app()->user->getId());
        $partnerId = $user->getPartner()->id;
        $criteria->addCondition('partner_activated=TRUE');
        $criteria->addCondition('partner_id=' . $partnerId);

        if (empty($filter)) {
            $filter['date']['from'] = date('Y-m-01');
            $filter['date']['to'] = date('Y-m-d');
        }

        if (!empty($filter)) {
            if(!empty($filter['date']['from'])) {
                $date = date('Y-m-d', CDateTimeParser::parse($filter['date']['from'], 'yyyy-MM-dd'));
                $criteria->addCondition("partner_activated_date >= '$date'");
            }
            if(!empty($filter['date']['to'])) {
                $date = date('Y-m-d', CDateTimeParser::parse($filter['date']['to'], 'yyyy-MM-dd'));
                $criteria->addCondition("partner_activated_date <= '$date'");
            }
        }
        $criteria->order = 'partner_activated_date DESC';

    }

    protected function sumPrice(CActiveDataProvider $provider)
    {
        $sum = 0;
        array_map(function ($v) use (&$sum) {$sum += $v->partner_price;} ,$provider->getData());

        return $sum;
    }

    public function actionActivate()
    {
        /** @var CHttpRequest $request */
        $request = Yii::app()->request;
        if ($request->getIsPostRequest()) {
            $code = $request->getParam('code');
            if (!empty($code)) {
                /** @var OrderItems $orderItem */
                $user = User::model()->findByPk(Yii::app()->user->getId());
                $partnerId = $user->getPartner()->id;
                $orderItem = OrderItems::model()->findByAttributes(['code' => $code/*, 'partner_id' => $partnerId*/]);


                if($orderItem->itemObj->type != 'group' && $orderItem->item_id == 2292) {
                    $orderItem->partner_price = $orderItem->partner_price * (100 - $user->getPartner()->commission)/100;
                    $orderItem->partner_id = $partnerId;

                } else if ($orderItem->itemObj->type != 'group' && $orderItem->partner_id != $partnerId) {
                    $orderItem= false;
                }
//               
                if (!empty($orderItem)) {
                    switch (true) {
                        case $orderItem->partner_activated:
                            $this->response('Сертификат уже был погашен!', false, 403);
                            break;
                        case $orderItem->isOverdue():
                            $this->response('Время действия сертификата истело.', false, 403);
                            break;
                        case (($request->getParam('activated_item') == 0) && ($request->getParam('activated_item')!='')):
                            $this->response('Необходимо выбрать впечатление!', false, 403);
                            break;
                        default:
                            $activateDate = $request->getParam('activateDate');
                            if (!empty($activateDate)) {
                                $activateDate = date('Y-m-d', CDateTimeParser::parse($activateDate, 'yyyy-MM-dd'));
                                if (!$this->isValidActivateDate($activateDate)) {
                                    $this->response('Не верная дата активации!', false, 403);
                                    return;
                                }
                            }

                            $orderItem->partner_activated = true;
                            $orderItem->partner_activated_date = !empty($activateDate) ? $activateDate : date('Y-m-d H:i:s');

                            /* добавляем партнера и впечатление*/
                            // если это групповое
                            $isGroupItem = $orderItem->itemObj->type == 'group';
                            if ($isGroupItem) {
                                $activationItem = (string) $request->getParam('activated_item');
                                if (!empty($orderItem->group_item_price_data[$activationItem])) {
                                    $orderItem->activated_item = $activationItem;
                                    $itemData = $orderItem->group_item_price_data[$activationItem];
                                    $orderItem->partner_id = $itemData['partner_id'];
                                    $orderItem->partner_price = $itemData['partner_price'];
                                    $orderItem->option_id = $itemData['option_id'];
                                    $orderItem->activated_item_price = $itemData['price'];
                                }
                                if (empty($orderItem->activated_item)) {
                                    throw new CHttpException('Error 404. Not Fount this Activation Item!');
                                }
                            }
                            /* добавляем партнера и впечатление*/
                            $orderItem->save();
                            $this->response('Сертификат погашен!', true);
                    }
                } else {
                    $this->response('Ошибка! Код не валиден для нанного партнёра!', false, 403);
                }
            } else {
                $this->response('Ошибка! Не корректный код!', false, 403);
            }
        }
    }

    /**
     * @param string $dateStr
     * @return bool
     */
    private function isValidActivateDate($dateStr)
    {
        $activateDate = new DateTime($dateStr);
        $validDate = new DateTime('midnight');
        $validDate->modify('-4 day');

        return $validDate <= $activateDate;
    }

    public function actionCheck()
    {
        /** @var CHttpRequest $request */
        $request = Yii::app()->request;
        $code = $request->getParam('code');
        $orderItem = null;
        $responseCode = 200;
        $status = true;
        if (!empty($code)) {
            $orderItem = $this->getOrderItemByCode($code);
            if ($orderItem['orderItem']) {
                $responseCode = 200;
                $status = true;
            } else {
                $responseCode = 404;
                $status = false;
            }
        }

        echo json_encode([
            'message' => $this->renderPartial('_view', ['orderItem' => $orderItem['orderItem']], true),
            'status' => $status,
            'code' => $responseCode,
            'items' => $orderItem['items']
        ]);
        //$this->response($this->renderPartial('_view', ['orderItem' => $orderItem['orderItem']], true), $status, $responseCode, $items);
    }

    public function actionResetGrid()
    {
        /** @var CHttpRequest $request */
        $request = Yii::app()->request;
        $filter = $request->getParam('filter', []);

        $criteria = new CDbCriteria();
        $this->assignFilter($criteria, $filter);
        $provider = new CActiveDataProvider('OrderItems', [
            'criteria' => $criteria,
            'pagination' => false
        ]);

        $this->renderPartial(
            '_itemsTable',
            [
                'orderItems' => $provider,
                'sum' => number_format($this->sumPrice($provider), 2, '.', ''),
            ]
        );
    }

    /**
     * @param string $message
     * @param bool   $status
     * @param int    $code
     */
    protected function response($message, $status, $code = 200)
    {
         echo json_encode([
             'code' => $code,
             'status' => $status,
             'message' => $message
         ]);
    }

    /**
     * @param string $code
     * @return array|CActiveRecord[]|mixed|null
     */
    private function getOrderItemByCode($code)
    {
        /** @var User $user */
        $user = User::model()->findByPk(Yii::app()->user->getId());
        $partnerId = $user->getPartner()->id;

        $criteria = new CDbCriteria();
        // проверку партнера 
        //$criteria->addCondition('partner_id=' . $partnerId);
        $criteria->addCondition('code=\'' . $code . '\'');
        //$criteria->addCondition('partner_activated<>TRUE');
//            $criteria->addCondition('recivier_phone<>\'\' AND recivier_phone IS NOT NULL');
        $orderItem =  OrderItems::model()->find($criteria);

        // массив впечатлений для группы
        $data['items'] = array();

        if($orderItem->orderObj->is_canceled == 1) {
            $orderItem = false;
        }
        //  для не групповых проверяем приязку к партнеру
        else if($orderItem && $orderItem->itemObj->type == Item::TYPE_ITEM) {
            if($orderItem->item_id == 2292) // id универсального сертификата, может погасить любой партнер
            {
                // необходимо поменять сумму к оплате в случае если еще не погашен
                if($orderItem->partner_activated == 0) {
                    $orderItem->partner_price = $orderItem->partner_price * (100 - $user->getPartner()->commission)/100;
                }
            }
            else if($orderItem->partner_id != $partnerId)
            {
                $orderItem = false;
            }
        // для групп
        } else if($orderItem && $orderItem->itemObj->type == Item::TYPE_GROUP) {
            // есть ли среди предлагаемых впечатлений - впечатление данного партнера

            $itemsData = $orderItem->group_item_price_data;
            foreach ($itemsData as $itemId => $itemData) {
                //echo $itemData['partner_id'] . "<br>";
                if($itemData['partner_id'] == $partnerId) {
                    $optionTitle = '';
                    if (!empty($itemData['option_name'])) {
                        $optionTitle = ' ('.$itemData['option_small_name'].')';
                    }
                    $data['items'][$itemId] = $itemData['title'].$optionTitle;
                }

            }
            // среди предлагаемых впечатлений нет впечатления данного партнера
            if(count($data['items']) == 0) {
                $orderItem = false;
            } else {
                $data['items'][0] = 'Не выбрана';
            }
            //die;
        }

        $result = [
            'orderItem' => $orderItem,
            'items' => $data['items']
        ];
        return $result;
    }
}