<?php

/**
 * Class LoginController
 */
class LoginController extends CController
{
    public $defaultAction = 'login';
    public $layout = 'login';

    public function actionLogin()
    {
        $form = new PartnerLoginForm();
        /** @var CHttpRequest $request */
        $request = Yii::app()->request;
        $formRequest = $request->getPost('PartnerLoginForm');
        if (!empty($formRequest)) {
            if (!empty($formRequest['username']) && !strpos($formRequest['username'], '@')) {
                $formRequest['username'] = User::model()->findByAttributes(['user_login' => $formRequest['username']])->user_email;
            }
            $form->attributes = $formRequest;
            if ($form->validate()) {
                $this->redirect('/partner');
            }
        }
        $this->render('login', ['form' => $form]);
    }
}