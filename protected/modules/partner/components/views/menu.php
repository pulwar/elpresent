<div class="navbar">
    <div class="navbar-inner">
        <ul class="nav">
            <?php foreach ($list as $name => $route) { ?>
                <?php if (is_array($route)) { ?>
                    <li class="dropdown<?php if (in_array($current, $route)) { ?> active<?php } ?>">
                        <a class="dropdown-toggle"
                           data-toggle="dropdown"
                           href="#">
                            <?php echo $name; ?>
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu">
                            <?php foreach ($route as $subName => $subRoute) { ?>
                                <li<?php if ($route == $current) { ?> class="active"<?php } ?>><a
                                            href="<?php echo $this->getUrl($subRoute); ?>"><?php echo $subName; ?></a>
                                </li>
                            <?php } ?>
                        </ul>
                    </li>
                <?php } else { ?>
                    <li<?php if ($route == $current) { ?> class="active"<?php } ?>><a
                                href="<?php echo $this->getUrl($route); ?>"><?php echo $name; ?></a></li>
                <?php } ?>
            <?php } ?>
        </ul>
    </div>
</div>