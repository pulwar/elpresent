<?php

/**
 * Class PartnerMainMenu
 */
class PartnerMainMenu extends CWidget
{
    /** @var  array */
    protected $list;

    public function init()
    {
        $this->list = $this->getList();
        //$list = array_filter($list, array($this, 'isCurrentUserHasPerms'));
        //array_walk($list, array($this, 'each'));
        return $this;
    }

    protected function isCurrentUserHasPerms($route)
    {
        static $model;
        if (is_null($model)) {
            $model = User::model()->findByPk(Yii::app()->user->getId());
        }
        return $model->user_role == 'partner';
    }

    public function run()
    {
        $this->render('menu', array('list' => $this->list, 'current' => $this->getCurrentRoute()));
    }

    private function getList()
    {
        return array
        (
            'Сертификаты' => '/partner/certificate',
            'Отчёты' => '/partner/reports',
            'Выйти' => '/profile/logout',
        );
    }

    public function each($route, $title)
    {
        if ($this->isCurrent($route)) {
            echo $this->currentView($title, $route);
        } else {
            echo $this->regularView($title, $route);
        }
    }

    public function isCurrent($route)
    {
        $cRoute = Yii::app()->urlManager->parseUrl(Yii::app()->getRequest());
        return $cRoute == substr($route, 1);
    }

    public function getCurrentRoute()
    {
        return Yii::app()->urlManager->parseUrl(Yii::app()->getRequest());
    }

    public function getUrl($route)
    {
        return Yii::app()->urlManager->createUrl($route);
    }

    public function currentView($title, $route)
    {
        $url = $this->getUrl($route);
        return "<a class='current' href='{$url}' class='current'>{$title}</a>";
    }

    public function regularView($title, $route)
    {
        $url = $this->getUrl($route);
        return "<a href='{$url}'>{$title}</a>";
    }
}