<?php

/**
 * Class PartnerLoginForm
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class PartnerLoginForm extends CFormModel
{
    /** @var  string */
    public $username;
    /** @var  string */
    public $password;
    /** @var  string */
    public $rememberMe;

    public function rules()
    {
        return array(
            // username and password are required
            array('username, password', 'required'),
            // password needs to be authenticated
            array('password', 'authenticate'),
            array('username,password', 'safe')
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array(
            'username' => 'Логин партнёра',
            'password' => 'Пароль',
            'rememberMe' => 'Запомнить меня',
        );
    }

    /**
     * Authenticates the password.
     * This is the 'authenticate' validator as declared in rules().
     */
    public function authenticate($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $identity = new UserIdentity($this->username, $this->password);
            if ($identity->authenticate()) {
                $duration = 3600 * 24 * 30;
                Yii::app()->user->login($identity, $duration);
            } else {
                $this->addError($attribute, 'Имя партнёра или пароль не корректны.');
            }
        }
    }
}
