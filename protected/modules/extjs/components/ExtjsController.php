<?php

class ExtjsController extends Controller
{
    protected $_users = [
        // верховный админ
        'elpresent' => [
            'password' => 't_2934851',
            'fio' => 'Таня',
            'score_hide' => false,
            "paid_hide" => false,
            'company' => [
                'name' => 'elpresent',
                'address' => 'Тимерязево 65б',
            ],
        ]
    ];

    public function __construct()
    {
        $json_file = RemoteSalesOrders::JSON_USERS_FILE_PATH;
        if (is_file($json_file)) {
            $json = file_get_contents($json_file);
            $this->_users = json_decode($json, true);
        } else {
            self::_userJsonSave($this->_users);
        }

        //html::pr($this->_users,1);
    }

    /**
     * Создать заказ на сайте
     *
     * @param $user
     * @param $item_id
     * @param $sum
     * @param $code
     * @param $city_id
     * @param $order_id
     * @return mixed
     */
    protected function _makeOrderSite($user, $item_id, $sum, $code, $city_id, $order_id)
    {
        /*
        if(!empty($order_id)){
            $orderRequest = Order::model()->findByPk($order_id);
        }else{
        */
            $orderRequest = new Order();
        /*}*/

        $orderRequest->scenario = 'fast_order';

        $orderRequest->type = Order::TYPE_BASIC;
        $orderRequest->name = $user;
        $orderRequest->lname = '';
        $orderRequest->phone = 'not-phone';

        $orderRequest->item_id = (int) $item_id;

        /** @var Item $item */
        $item = Item::model()->findByPk($orderRequest->item_id);
        if ($item) {
            $prices = $item->prices_cache[$city_id];
            foreach ($prices as $k => $v) {
                if ($v == $sum) {
                    $option_id = $k;
                }
            }
            // выставляем срок действия
            if($item->start_season_date != null) {
                $date = new DateTime($item->start_season_date);
            } else {
                $date = new DateTime($orderRequest->order_date);
            }
            date_modify($date, '+' .$item->months_of_validity . ' month');
            $orderRequest->validity_date = $date->format('Y-m-d');

        }
        $orderRequest->price = $sum;
        $orderRequest->city_id = $city_id;
//        $orderRequest->code = empty($orderRequest->code) ?  $code : $orderRequest->code;
        $orderRequest->order_number = Order::generateUniqueOrderNumber();
        $orderRequest->email = 'not-email@elpresent.by';
        $orderRequest->is_remote_order = 1;
        $orderRequest->option_id = empty($option_id) ? 0 : $option_id;
        $orderRequest->save();

        if (!empty($orderRequest->orderItems)) {
            $orderItem = $orderRequest->orderItems;
            $orderItem = $orderItem[0];
        } else {
            $orderItem = new OrderItems();
        }
        $orderItem->order_id = $orderRequest->id;
        $orderItem->item_id = $item_id;
        $orderItem->option_id = $orderRequest->option_id;
        $orderItem->item_title = $item->title;
        $orderItem->item_price = $sum;
        $orderItem->item_price_with_discount = $item->getRoundedPrice($orderRequest->option_id, City::model()->findByPk($city_id));
        $orderItem->discount_percent = empty($item->off_price) ? 0 : $item->off_price;
        $orderItem->discount_price = $orderItem->item_price * ($orderItem->discount_percent/ 100);
        if (empty($orderItem->code)) {
            $orderItem->code = $code;
        }


        $orderItem->save();

        return $orderRequest;
    }

    /**
     * Вернуть подготовленные итемы подарков
     *
     * @param $items
     *
     * @return array
     */
    protected function _getGiftsItemsPrepare($items)
    {
        $city_id = $this->_getCityId();
        $gifts = [];
        foreach ($items as $key => $item) {
            $pr = [];
            foreach ($item->prices as $price) {
                if ($price['city_id'] == $city_id) {
                    $pr[] = $price['item_price'];
                }
            }

            foreach ($item->categories as $cat) {
                $cats[] = $cat['title'];
            }

            $pr = array_unique($pr);

            sort($pr);
            $gifts[$key]['price'] = implode(',', $pr);
            $gifts[$key]['cats'] = implode(',', $cats);
            $gifts[$key]['id'] = $item->id;
            $gifts[$key]['title'] = $item->title;
            $gifts[$key]['perm_link'] = $item->perm_link;
            $gifts[$key]['is_hot'] = $item->is_hot;
        }
        return $gifts;
    }

    /**
     * Проверка авторизации
     *
     * @param $email
     * @param $password
     *
     * @return bool
     */
    protected function _identity($email, $password)
    {
        if (!empty($this->_users[$email]) && $this->_users[$_POST['email']]['password'] == $password) {
            return true;
        }
        return false;
    }

    /**
     * Установить фильтры
     *
     * @param $request
     * @param $criteria
     *
     * @return mixed
     */
    protected function setFilters($request, $criteria)
    {
        $filters = $request->getParam('filter', null);

        if (!empty($filters)) {
            $filters = json_decode($filters, true);
            foreach ($filters as $filter) {
                if ($filter['operator'] == 'like') {
                    if ($filter['property'] == 'item_title') {
                        $filter['property'] = 'ic.title';
                    }
                    $criteria->addSearchCondition($filter['property'], $filter['value'], true, 'AND');
                } else {
                    if ($filter['property'] == 'price') {
                        $property = 'ic.item_price';
                        $property_filter = 'item_price';
                        $value = $filter['value'];
                    } elseif ($filter['property'] == 'date') {
                        $property = 'date';
                        $property_filter = $property;
                        $value = strtotime($filter['value']);
                    } else {
                        $property = $filter['property'];
                        $property_filter = $property;
                        $value = $filter['value'];
                    }
                    $operator = RemoteSalesOrders::$operators[$filter['operator']];
                    $criteria->addCondition("$property $operator :$property_filter", 'AND');
                    $criteria->params = array(":$property_filter" => $value);
                }
            }
        }
        return $criteria;
    }

    /**
     * Установить сортировки
     *
     * @param $request
     *
     * @return array
     */
    public function setOrder($request)
    {
        $sort = $request->getParam('sort', null);
        $arr = [];
        if (!empty($sort)) {
            $sort = json_decode($sort, true);
            $sort = array_shift($sort);
            if ($sort['property'] == 'price') {
                $sort['property'] = 'ic.item_price';
            }
            if ($sort['property'] == 'price_pickup') {
                $sort['property'] = 'price';
            }
            if ($sort['property'] == 'item_title') {
                $sort['property'] = 'ic.title';
            }
            $property = $sort['property'];
            $direction = $sort['direction'];
        } else {
            $property = 'id';
            $direction = 'DESC';
        }
        $arr['property'] = $property;
        $arr['direction'] = $direction;
        return $arr;
    }

    /**
     * Получить id города из авторизации
     *
     * @return int
     */
    protected function _getCityId()
    {
        $logged_arr = json_decode(Yii::app()->request->cookies['LoggedIn']->value, true);
        $city = empty($logged_arr['city']) ? 1 : (int)$logged_arr['city'];
        return empty($city) ? 1 : $city;
    }

    /**
     * Сохранение json файла с юзерами
     *
     * @param array $users
     */
    protected static function _userJsonSave(array $users)
    {
        $json_file = RemoteSalesOrders::JSON_USERS_FILE_PATH;
        $json = json_encode($users);
        file_put_contents($json_file, $json);
    }
}