<?php
class UserController extends ExtjsController
{
    public function actionIndex()
    {
        $data = [];

        foreach ($this->_users as $user => $d) {
            $data[]
                = [
                'user' => $user,
                'password' => $d['password'],
                'fio' => $d['fio'],
                'company_name' => $d['company']['name'],
                'company_address' => $d['company']['address'],
                'score_hide' => $d['score_hide'],
                'paid_hide' => $d['paid_hide'],
            ];
        }

        $result['success'] = true;
        $result['users'] = $data;

        echo json_encode($result);
        die;
    }

    public function actionEdit()
    {
        $user = trim($_POST['user']);
        $field = trim($_POST['field']);
        $value = trim($_POST['value']);

        if ($field == 'company_name' || $field == 'company_address') {
            $field = explode('_', $field);
            $field = $field[1];
            $this->_users[$user]['company'][$field] = $value;
        } else {
            $this->_users[$user][$field] = $value;
        }

        self::_userJsonSave($this->_users);
        $result['success'] = true;

        echo json_encode($result);
        die;
    }

    public function actionSave()
    {
        $result['success'] = false;
        $result['error'] = 'Неизвестная ошибка!';

        $user = trim($_POST['user']);
        $password = trim($_POST['password']);
        $fio = trim($_POST['fio']);
        $company_name = trim($_POST['company_name']);
        $company_address = trim($_POST['company_address']);
        $score_hide = $_POST['score_hide'];
        $paid_hide = $_POST['paid_hide'];

        if (!empty($this->_users[$user])) {
            $result['error'] = 'Такой юзер уже есть!';
            echo json_encode($result);
            die;
        }

        if (!preg_match("/[A-z\_\- ]/i", $user) || !preg_match("/[A-z\_\- ]/i", $password)) {
            $result['error'] = 'Такой логин/пароль невозможен';
            echo json_encode($result);
            die;
        }

        $this->_users[$user] = [
            'password' => $password,
            'fio' => $fio,
            'score_hide' => (bool)$score_hide,
            'paid_hide' => (bool)$paid_hide,
            'company' => [
                'name' => $company_name,
                'address' => $company_address,
            ]
        ];

        self::_userJsonSave($this->_users);
        $result['success'] = true;

        echo json_encode($result);
        die;
    }

    public function actionDelete()
    {
        $request = Yii::app()->request;
        $item_id = trim($request->getParam('item_id'));
        unset($this->_users[$item_id]);
        self::_userJsonSave($this->_users);
        $result['success'] = true;
        echo json_encode($result);
        die;
    }
}