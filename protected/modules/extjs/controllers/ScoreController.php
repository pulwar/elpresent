<?php
class ScoreController extends ExtjsController
{
    public function actionIndex()
    {
        $result = [];
        $data = [];
        $json = file_get_contents(RemoteSalesOrders::JSON_SCORE_FILE_PATH);
        $arr = json_decode($json, true);
        foreach ($arr as $key => $value) {
            $data[]
                = [
                'score' => $key,
                'from' => $value[0],
                'to' => $value[1],
            ];
        }
        $result['success'] = true;
        $result['scores'] = $data;
        echo json_encode($result);
        die;
    }

    public function actionEdit()
    {
        $request = Yii::app()->request;
        $originalValue = (int)$request->getParam('originalValue');
        $newValue = (int)$request->getParam('value');
        $part = (int)$request->getParam('part');

        $json = file_get_contents(RemoteSalesOrders::JSON_SCORE_FILE_PATH);
        $arr = json_decode($json, true);

        foreach ($arr as $key => $val) {
            if ($val[$part] == $originalValue) {
                $arr[$key][$part] = $newValue;
            }
        }

        $json = json_encode($arr);

        $written = file_put_contents(RemoteSalesOrders::JSON_SCORE_FILE_PATH, $json);

        if (!empty($written) && $written > 0) {
            $result['success'] = true;
        } else {
            $result['success'] = false;
            $result['error'] = 'Ошибка!';
        }

        echo json_encode($result);
        die;
    }

    /**
     * Подсчет количества очков у юзера
     */
    public function actionCount()
    {
        $request = Yii::app()->request;
        $user = $request->getParam('user');
        $user = json_decode($user, true);
        $email = $user['user_info']['company']['name'];
        $criteria = new CDbCriteria;
        $criteria->select = 'score';
        $criteria->condition = 'user = :email';
        $criteria->params = array(':email' => $email);

        $res = RemoteSalesOrders::model()->findAll($criteria);
        $count = 0;
        foreach ($res as $r) {
            $count += $r->score;
        }

        $result['success'] = true;
        $result['count'] = $count;
        echo json_encode($result);
        die;
    }

    /**
     * Очистка очков для залогиненного юзера
     */
    public function actionClear()
    {
        $logged_arr = json_decode(Yii::app()->request->cookies['LoggedIn']->value, true);
        RemoteSalesOrders::model()->updateAll(['score' => 0], 'user=:user', [':user' => $logged_arr['email']]);
        $result['success'] = true;
        echo json_encode($result);
        die;
    }
}