<?php

class GiftController extends ExtjsController
{
    /**
     * Вывод подарков (только минск)
     */
    public function actionIndex()
    {
        $city_id = $this->_getCityId();

        $model = Item::model();
        $request = Yii::app()->request;

        $criteria = new CDbCriteria;
        $criteria->join = " INNER JOIN {{item_with_city}} ic ON t.id = ic.item_id AND ic.city_id = $city_id AND ic.available = 1";

        $offset = $request->getParam('start');
        $limit = $request->getParam('limit');

        $sort = $this->setOrder($request);
        $criteria = $this->setFilters($request, $criteria);
        // исключаем лишние подарки
        $criteria->addNotInCondition('t.id', RemoteSalesOrders::$exclusionGifts);
        $criteria->addCondition('t.available = 1');

        $total = $model->count($criteria);
        $criteria->group = 't.id';
        $criteria->order = "$sort[property] $sort[direction]";
        $criteria->limit = $limit;
        $criteria->offset = $offset;
        //var_export($criteria); die;

        $items = $model->findAll($criteria);
        $result = [];
        $gifts = $this->_getGiftsItemsPrepare($items);

        $result['gifts'] = $gifts;
        $result['success'] = true;
        $result['total'] = $total;

        echo json_encode($result);
        die;
    }

    public function actionView()
    {
        $result = [];
        $options = [];
        $return = [];

        $request = Yii::app()->request;
        $id = $request->getParam('id');

        $model = Item::model();
        $item = $model->findByPk($id);

        $return['preview_img'] = $item->preview_img;
        $return['title'] = $item->title;
        $return['desc'] = $item->desc;

        if (is_array($item->getAllPrice())) {
            foreach ($item->getAllPrice() as $k => $v) {
                $options[] = $item->getOption($k)->name;
            }
        }

        $return['options'] = implode(';', $options);

        $result['item'] = $return;
        $result['success'] = true;
        echo json_encode($result);
        die;
    }
}