<?php

class DefaultController extends ExtjsController
{

    public function init(){

    }

    public function actionLogin()
    {
        if (!empty($_POST['email']) && !empty($_POST['password'])) {
            $city = (int)$_POST['city'];
            $email = trim($_POST['email']);
            $password = trim($_POST['password']);
            $result['success'] = true;
            if ($this->_identity($email, $password)) {
                $user = [
                    'email' => $email,
                    'user_info' => $this->_users[$email],
                    'company_info' => $this->_users[$email]['company'],
                    'score_hide' => empty($this->_users[$email]['score_hide']) ? false : $this->_users[$email]['score_hide'],
                    'paid_hide' => empty($this->_users[$email]['paid_hide']) ? false : $this->_users[$email]['paid_hide'],
                    'city' => empty($city) ? 1 : $city
                ];

                unset($user[$email]['password']);

                $result['user'] = json_encode($user);
                $result['success'] = true;
            } else {
                $result['success'] = false;
                $result['errors']['reason'] = 'Логин али пароль порочен!';
            }
            echo json_encode($result);
            die;
        }
    }

    public function actionDiscont()
    {
        $request = Yii::app()->request;
        $id = (int)$request->getParam('id');
        $value = (int)$request->getParam('value');

        $value = empty($value) ? 0 : (int)$value;
        $model = RemoteSalesOrders::model();
        $model = $model->findByPk($id);
        $model->discont = $value;
        if ($model->save()) {
            $result['success'] = true;
        } else {
            $result['success'] = false;
            $result['error'] = 'Ошибка!';
        }

        echo json_encode($result);
        die;
    }

    public function actionExcel()
    {
        $logged_arr = json_decode(Yii::app()->request->cookies['LoggedIn']->value, true);
        $login = $logged_arr['email'];

        $data_json = $_POST['data_json'];
        $data_array = json_decode($data_json, true);

        foreach ($data_array as $k => $v) {
            $data_array[$k]['date'] = date('d.m.Y H:i', (int)$data_array[$k]['date']);
            $data_array[$k]['activated'] = empty($data_array[$k]['activated']) ? 'Нет' : 'Да';
        }

        $headers = array(
            'ID товара',
            'Навзание',
            'Дата',
            'Пользователь',
            'ID заказа',
            'Компания',
            'ФИО',
            'Сумма',
            'Адрес',
            'Очки',
            'Код',
            'Активирован?',
            'Скидка'
        );

        Yii::import('ext.phpexcel.XPHPExcel');
        $phpExcel = XPHPExcel::createPHPExcel();

        $phpExcel->getActiveSheet()->fromArray($headers, null, 'A1');
        $phpExcel->getActiveSheet()->fromArray($data_array, null, 'A2');
        $phpExcel->getActiveSheet()->getStyle('K2:K500')->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);
        $phpExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        $phpExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
        $phpExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
        $phpExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
        $phpExcel->getActiveSheet()->getStyle("A1:P1")->getFont()->getColor()->setRGB('FF0000');
        $phpExcel->getActiveSheet()->setTitle('Отчёт');
        $objWriter = PHPExcel_IOFactory::createWriter($phpExcel, 'Excel2007');

        $xlsx_name = 'assets/report_' . $login . '_ID' . time() . '.xlsx';
        $objWriter->save($xlsx_name);

        $result['link'] = $xlsx_name;
        $result['success'] = true;

        echo json_encode($result);
        die;
    }

    public function actionBuy()
    {
        if (empty(Yii::app()->request->cookies['LoggedIn']->value)) {
            $result['success'] = false;
            $result['error'] = 'Отсутствует компания';
        } else {
            $request = Yii::app()->request;
            $logged_arr = json_decode(Yii::app()->request->cookies['LoggedIn']->value, true);

            $order_id = $request->getParam('order_id');

            $item_id = $request->getParam('item_id');
            $sum = $request->getParam('sum');
            $city_id = $logged_arr['city'];
            $company = $logged_arr['company_info']['name'];
            $fio = $logged_arr['user_info']['fio'];
            $user = $logged_arr['email'];
            $address = $logged_arr['company_info']['address'];

            $score = RemoteSalesOrders::model()->getScores($sum);

            if (empty($item_id) || empty($sum)) {
                $result['error'] = 'Не все данные преданы';
            } else {
                $code = OrderItems::getUniqCode();
                $model = new RemoteSalesOrders();
                $model->item_id = $item_id;
                $model->date = time();
                $model->user = $user;
                $model->fio = $fio;
                $model->sum = $sum;
                $model->company = $company;
                $model->address = $address;
                $model->score = $score;
                $model->code = $code;
                if ($model->save()) {
                    if ($this->_makeOrderSite($user, $item_id, $sum, $code, $city_id, $order_id)) {
                        $result['id'] = $model->id;
                        $result['success'] = true;
                    } else {
                        $result['success'] = false;
                        $result['error'] = 'Ошибка! Покупка зарегестрирована, но не совершен заказ';
                    }
                } else {
                    $result['success'] = false;
                    $result['error'] = 'Ошибка! Покупка не зарегестрирована.';
                }
            }
        }
        echo json_encode($result);
        die;
    }

    /**
     * Скидки для дробокса скидок
     */
    public function actionSales()
    {
        $result = RemoteSalesOrders::$sales;
        echo json_encode($result);
        die;
    }


    /**
     * Города
     */
    public function actionCities()
    {
        $result = [];
        $cities = $cities = City::getCityList();
        foreach ($cities as $key => $city) {
            $result[$key]['id'] = $city->id;
            $result[$key]['title'] = $city->title;
        }
        echo json_encode($result);
        die;
    }
}