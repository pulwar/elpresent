<?php
class OrderController extends ExtjsController
{

    public function actionIndex()
    {
        $result = [];

        if (empty(Yii::app()->request->cookies['LoggedIn']->value)) {
            $result['success'] = true;
            $result['total'] = 0;
        } else {
            $model = RemoteSalesOrders::model();
            $criteria = new CDbCriteria;
            $criteria->join = ' INNER JOIN {{item}} ic ON t.item_id = ic.id';
            $request = Yii::app()->request;
            $offset = $request->getParam('start');
            $limit = $request->getParam('limit');

            $logged_arr = json_decode(Yii::app()->request->cookies['LoggedIn']->value, true);
            //$company = $logged_arr['company_info']['name'];
            $user = $logged_arr['email'];

            // не админ
            if ($user != RemoteSalesOrders::ADMIN) {
                // простой пользователь
                if (strpos($user, '_') !== false) {
                    $criteria->condition = 'user = :user';
                    $criteria->params = [':user' => $user];
                } else {
                    // админ своей компании
                    $criteria->addSearchCondition('user', $user, true, 'AND');
                }
            }

            $sort = $this->setOrder($request);
            $criteria = $this->setFilters($request, $criteria);

            $count = $model->count($criteria);

            $criteria->order = "$sort[property] $sort[direction]";
            $criteria->limit = $limit;
            $criteria->offset = $offset;

            $items = $model->findAll($criteria);
            $result['success'] = true;
            $result['total'] = $count;

            foreach ($items as $k => $item) {
                $result['orders'][$k]['item_id'] = $item->item->id;
                $result['orders'][$k]['item_title'] = $item->item->title;
                $result['orders'][$k]['date'] = $item->date;
                $result['orders'][$k]['user'] = $item->user;
                $result['orders'][$k]['id'] = $item->id;
                $result['orders'][$k]['company'] = $item->company;
                $result['orders'][$k]['fio'] = $item->fio;
                $result['orders'][$k]['sum'] = $item->sum;
                $result['orders'][$k]['address'] = $item->address;
                $result['orders'][$k]['score'] = $item->score;
                $result['orders'][$k]['code'] = $item->code;
                $result['orders'][$k]['activated'] = $item->activated;
                $result['orders'][$k]['discont'] = $item->discont;
            }
        }

        echo json_encode($result);
        die;
    }

    /**
     * Удаление заказа
     */
    public function actionDelete()
    {
        $request = Yii::app()->request;
        $item_id = (int)$request->getParam('item_id');

        $model = RemoteSalesOrders::model();
        $item = $model->findByPk($item_id);

        $orderItem = OrderItems::model()->find('item_id=:item_id', [':item_id' => $item->item_id]);
        $order = $orderItem->orderObj;

        if (!empty($order)) {
            if (!$order->delete()) {
                $result['success'] = false;
            }
        }

        if (empty($item_id) || empty($item)) {
            $result['success'] = false;
        } else {
            $item->delete();
            $result['success'] = true;
        }
        error_log('Удалено ' . date('Y-m-d H:i') . Yii::app()->request->cookies['LoggedIn']->value . "\n", 3, "assets/log_extjs_del.txt");

        echo json_encode($result);
        die;
    }


    public function actionActivated()
    {
        $request = Yii::app()->request;
        $id = (int)$request->getParam('id');
        $value = (int)$request->getParam('value');

        $value = empty($value) ? 0 : 1;
        $model = RemoteSalesOrders::model();
        $model = $model->findByPk($id);
        $model->activated = $value;
        if ($model->save()) {
            $result['success'] = true;
        } else {
            $result['success'] = false;
            $result['error'] = 'Ошибка!';
        }

        echo json_encode($result);
        die;
    }


}