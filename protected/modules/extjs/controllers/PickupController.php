<?php

class PickupController extends ExtjsController
{

    public function actionIndex()
    {
        $logged_arr = json_decode(Yii::app()->request->cookies['LoggedIn']->value, true);
        $city_id = empty($logged_arr['city']) ? 1 : (int)$logged_arr['city'];

        $request = Yii::app()->request;
        $result = [];
        $offset = $request->getParam('start');
        $limit = $request->getParam('limit');
        $sort = $this->setOrder($request);

        $model = Order::model();
        $criteria = new CDbCriteria;
        //$criteria->join = ' INNER JOIN {{item}} ic ON t.item_id = ic.id';

        $criteria->condition = 'is_express_delivery = :is_express_delivery
            AND is_thematic_delivery = :is_thematic_delivery
            AND is_delivery_general =:is_delivery_general
            AND is_fast_order = :is_fast_order
            AND city_id = :city_id
            AND order_date > :order_date
            AND is_remote_order = :is_remote_order';

        $criteria->params = [
            ':is_express_delivery' => 0,
            ':is_thematic_delivery' => 0,
            ':is_delivery_general' => 0,
            ':is_fast_order' => 0,
            ':is_remote_order' => 0,
            ':city_id' => $city_id,
            ':order_date' => '2016-01-01'
        ];

        $criteria = $this->setFilters($request, $criteria);

        $count = $model->count($criteria);
        $criteria->limit = $limit;
        $criteria->offset = $offset;
        $criteria->order = "$sort[property] $sort[direction]";

        $items = $model->findAll($criteria);
        $result['success'] = true;
        $result['total'] = $count;

        $counter = 0;

        foreach ($items as $k => $item) {
            foreach ($item->orderItems as $num => $orderItem) {
                $result['pickup'][$counter]['id'] = $orderItem->id;
                $result['pickup'][$counter]['item_id'] = $orderItem->itemObj->id;
                $result['pickup'][$counter]['item_title'] = $orderItem->itemObj->title;
                $result['pickup'][$counter]['price_pickup'] = $item->price;
                $result['pickup'][$counter]['phone'] = $item->phone;
                $result['pickup'][$counter]['order_date'] = $item->order_date;
                $result['pickup'][$counter]['order_number'] = $item->order_number;
                $result['pickup'][$counter]['name'] = $item->name;
                $result['pickup'][$counter]['lname'] = $item->lname;
                $result['pickup'][$counter]['email'] = $item->email;
                $result['pickup'][$counter]['is_paid'] = $item->is_paid;
                $counter++;
            }
        }

        echo json_encode($result);
        die;
    }

    public function actionIs_paid(){
        $request = Yii::app()->request;
        $id = (int)$request->getParam('id');
        $value = (int)$request->getParam('value');

        $value = empty($value) ? 0 : 1;
        $model = Order::model();
        $model = $model->findByPk($id);
        $model->is_paid = $value;
        if ($model->save()) {
            $result['success'] = true;
        } else {
            $result['success'] = false;
            $result['error'] = 'Ошибка!';
        }

        echo json_encode($result);
        die;
    }


}