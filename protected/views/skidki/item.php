<h1>Скидки <?=$model->category->title;?></h1>
<br>
<div class="row catalog-item ">
    <div class="col-md-3">
        <div class="product-item-image">
            <div class="hidden-product-img">
                <? if(is_file('images/discounts/' . $model->img_big)):?>
                    <img src="/images/discounts/<?=$model->img_big?>" alt="<?=$model->title?>"/>
                <? endif;?>
            </div>
            <div class="product-item-image__shadow"></div>
            <div class="product-item-price"><?=($model->price)?> руб.</div>
            <div class="product-item-add">
                <span> Осталось кодов: <?= $model->countCods($model->id)?></span>
            </div>
        </div>
    </div>
    <div class="col-md-9">
        <div class="select-social">
            <h1><?= $model->title ?></h1>
            <div class="item-description">
                <?=$model->text;?>
            </div>
        </div>

        <hr>
        <h3>Получи номер скидки на почту!</h3>
        <form method="POST">
            <input name="email" type="email" value="" required="required" style="width: 200px; height: 40px; font-size: 20px; vertical-align: bottom">
            <input name="submit" class="btn btn-primary buy-button shose-price" type="submit" value="Отправить">
        </form>
        <hr>
    </div>
</div>