<div class="row gift list">
<? foreach ($models as $k => $model) : ?>
    <? if (($k + 1) % 2 == 0): ?>
        <div class="row">
    <? endif; ?>
    <div class="col-md-6">
        <div class="product-item item">

            <h3><a href="/skidki/<?= $model->category->slug ?>/<?= $model->alias ?>"><?= $model->title ?></a></h3>

            <div class="product-item-content clearfix">

                <div class="product-item-image">
                    <a href="/skidki/<?= $model->category->slug ?>/<?= $model->alias ?>">
                        <div class="hidden-product-img">
                            <? if (is_file('images/discounts/' . $model->img_small)): ?>
                                <img src="/images/discounts/<?= $model->img_small ?>" alt="<?= $model->title ?>"/>
                            <? endif; ?>
                        </div>
                    </a>

                    <div class="product-item-image__shadow"></div>
                    <div class="product-item-add">
                        <span> Осталось кодов: <?= $model->countCods($model->id) ?></span>
                    </div>
                </div>

                <div class="product-item-description">
                    <span><?= text::setCutStr($model->tiser, 300) ?></span>

                    <div class="product-item-price">От <?= ($model->price) ?> руб.</div>
                    <a class="btn btn-primary buy-button shose-price" href="/skidki/<?= $model->category->slug ?>/<?= $model->alias ?>">Подробнее...</a>
                </div>
            </div>
        </div>
    </div>
    <? if (($k + 1) % 2 == 0 || $count == ($k + 1)): ?>
        </div>
        <hr class="product-separator">
    <? endif; ?>
<? endforeach; ?>
</div>
<?$this->widget('CLinkPager', array(
    'pages' => $pages,
    'header' => '',
    'cssFile' => false,
    'id' => 'pagination',
    'nextPageLabel'=>'<span aria-hidden="true">Следущая</span><span class="sr-only">Следущая</span>',
    'previousPageCssClass' => 'hidden',
    'lastPageCssClass' => 'hidden',
    'firstPageLabel'=>'<span aria-hidden="true">Первая</span><span class="sr-only">Первая</span>',
    'selectedPageCssClass' => 'active',
    'hiddenPageCssClass' => 'bgg',
    'htmlOptions' => array(
        'class' => 'pagination',
    ),
))?>