<script type="text/javascript">
    ymaps.ready(init);

    function init()
    {
        var myMap = new ymaps.Map('map', {

            <? if(url::is_city('minsk')):?>
            center: [53.914659, 27.570544],
            <? elseif(url::is_city('brest')):?>
            center: [52.09755, 23.68775],
            <? elseif(url::is_city('gomel')):?>
            center: [52.4345, 30.9754],
            <? elseif(url::is_city('grodno')):?>
            center: [53.6884, 23.8258],
            <? elseif(url::is_city('mogilev')):?>
            center: [53.9168, 30.3449],
            <? elseif(url::is_city('vitebsk')):?>
            center: [55.1904, 30.2049],
            <? endif;?>

            zoom: 11
        }, {
            searchControlProvider: 'yandex#search'
        });

        var objectManager = new ymaps.ObjectManager({
            clusterize: true,
            gridSize: 32
        });

        myMap.geoObjects.add(objectManager);
        objectManager.add(<?=$json?>);

        $('.map-right-part a.addresses').click(function(){
            var id = $(this).data('id');
            if(id == 'all'){
                $('.address-description > div').text('Нажмите на название компании/фирмы, чтобы узнать ее адрес и отобразить на карте');
                objectManager.setFilter(true);

            }else{
                objectManager.setFilter('id ==' + id);
                var address = $(this).data('address');
                var time = $(this).data('time');
                var txt = 'Адрес: ' + address + '; время работы: ' + time;
                $('.address-description > div').text(txt);
            }
        });

    }
</script>

<h1>Как купить?</h1>
<ol>
    <li>Забрать подарок самостоятельно на нашей точке продаж по адресу: <?= Yii::app()->params['address']?> <?= Yii::app()->params['time_of_mork_big']?>.</li>
    <li>Заказать платную доставку курьером.</li>
    <li>Заказать <a href="https://elpresent.by/originalnaya-dostavka" target="_blank">оригинальную доставку подарка</a>.</li>
    <li>Приобрести сертификат у наших дилеров:</li>
</ol>


<ul class="nav nav-tabs map">
    <? foreach($cities as $city):?>
        <li class="<?=url::is_page($city->name) == true ? 'active' : ''?>">
            <a href="<?=$city->name?>"><?=$city->title?></a>
        </li>
    <? endforeach;?>
</ul>

<div class="address-description">
    <div class="alert alert-warning">Нажмите на название компании/фирмы, чтобы узнать ее адрес и отобразить на карте</div>
</div>

<div class="row map-area">
    <div class="col-md-8">
        <div id="map"></div>
    </div>
    <div class="col-md-4 map-right-part">
        <? if(empty($items)):?>
            <p>нет адресов</p>
        <? else:?>
            <ul class="list-group">
                <li class="list-group-item">
                    <a href="javascript:void(0);" data-address="all" data-id="all" class="addresses">Все</a>
                </li>
                <? foreach($items as $item):?>
                    <li class="list-group-item">
                        <a href="javascript:void(0);" data-time='<?=$item->time_work?>' data-address='<?=$item->address?>' data-id='<?=$item->id?>' class="addresses"><?=$item->title?></a>
                    </li>
                <?endforeach?>
            </ul>
        <? endif;?>
    </div>
</div>
