<h1 class="main-title"><?=$page->title; ?></h1>
<?=$page->text; ?>
<? if(url::is_page('cooperation')):?>
	<div class="disqus comments clearfix">
		<h3>Отзывы партнеров</h3>
		<div id="disqus_thread"></div>
		<script type="text/javascript">
			var disqus_shortname = 'elpresentby'; // required: replace example with your forum shortname
			var disqus_identifier = 'cooperation';
			(function() {
				var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
				dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
				(document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
			})();
		</script>
		<noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
	</div>
<? endif;?>