<?php

function formatDeliveryPriceLabel($price)
{
    if ($price === 0) {
        return 'бесплатно';
    }

    return ($price) . ' руб.';
}

?>
<script type="text/javascript">
var ordersCount = <?=count($oShopOrder) ?>;
var discountPercent;
var resultPrice;

function changePayType(select) {
    if (4 == $('option:selected', select).val()) {
        $('tr.easypay').show();
    } else {
        $('tr.easypay').hide();
    }
}
function edtHandler(_select) {
    var _option = $('option:selected', _select);
    var themPrice = parseInt(_option.attr('thematic_price'));
    var elem = parseInt(_option.attr('elem'));
    var themPriceBlock = $('.thematic-price-block-' + elem);
    var prevThemPrice = themPriceBlock.attr('thematic_price');

    themPriceBlock.attr('thematic_price', themPrice);
    themPriceBlock.text(
        $().number_format(themPrice,
            {
                numberOfDecimals: 0,
                decimalSeparator: ',',
                thousandSeparator: ','
            }) + ' руб.'
    );


    var inputElem = $('#edr\\[' + elem + '\\]');
    if (themPrice > 0) {
        /* Do click on checkbox to recalculate price */
        if (inputElem[0].checked) {
            inputElem[0].checked = false;
            /*window.edrHandler();*/
            $(inputElem).click();
        }

        /* Show recipient delivery info block*/
        inputElem.parent().parent().next().next().find('div.recipient-info').show();


        /* Show/hide pay fields */
        var iTotalItems = $('input[id^=edr]').length;
        var iCheckedDelivs = $('input[id^=edr]:checked').length + $('option[thematic_price]:selected').filter(function () {
                return parseInt($(this).attr('thematic_price')) > 0;
            }).length;

        $('#pay-date-tr').show();
        $('#pay-time-tr').show();

        /* hide get fields, if necessary */
        if (iTotalItems == iCheckedDelivs) {
            $('#pay-adr-tr').show();
            $('#gift-date-tr').hide();
            $('#gift-time-tr').hide();
            $('#adr-tr').hide();
        }
    }
    else {
        /* Hide recipient delivery info block*/
        inputElem.parent().parent().next().next().find('div.recipient-info').hide();

        /* Show/hide pay fields */

        var iTotalItems = $('input[id^=edr]').length;
        var iUnCheckedDelivs = $('input[id^=edr]').not(':checked').length + $('option[thematic_price]:selected').filter(function () {
                return parseInt($(this).attr('thematic_price')) == 0;
            }).length;

        if (iUnCheckedDelivs == iTotalItems * 2) {
            $('#pay-date-tr').hide();
            $('#pay-time-tr').hide();
        }

        $('#pay-adr-tr').hide();

        /* show get fields */
        $('#gift-date-tr').show();
        $('#gift-time-tr').show();
        $('#adr-tr').show();
    }


    /* Total prices recount */

    var totalPrice = parseInt($('#resultPrice').attr('price'));
    var totalDiscountPrice = parseInt($('#resultPriceWithDiscount').attr('price'));

    totalPrice -= prevThemPrice;
    totalPrice += themPrice;
    totalDiscountPrice -= prevThemPrice;
    totalDiscountPrice += themPrice;


    $('#resultPrice').attr('price', totalPrice);
    $('#resultPrice').html($().number_format(totalPrice,
        {
            numberOfDecimals: 0,
            decimalSeparator: ',',
            thousandSeparator: ','
        }));

    $('#resultPriceWithDiscount').attr('price', totalDiscountPrice);
    $('#resultPriceWithDiscount').html($().number_format(totalDiscountPrice,
        {
            numberOfDecimals: 0,
            decimalSeparator: ',',
            thousandSeparator: ','
        }));

    if (totalPrice > totalDiscountPrice) {
        $('#resultPrice').css(
            {
                textDecoration: 'line-through'
            });

        $('#discount-container').show();
    }
    else {
        $('#resultPrice').css(
            {
                textDecoration: 'none'
            });

        $('#discount-container').hide();
    }
}


$(document).ready(function () {

    var divabyPercent = $('#divaby_discount_percent').val();
    var svadbabyPercent = $('#svadbaby_discount_percent').val();

    var giftDate = $('#gift-date-tr');
    var recipientInfo = $('.recipient-info');
    var buyInfo = $('.buy');


    var hiddableFields = $('#adr-tr, #gift-date-tr, #gift-time-tr').show();

    if ($('input[name="delivery_id_1[0]"]')[0].checked) {
        hiddableFields.hide();
    }

    /* Work with seleboxes of Thematic options */


    /* Show/hide Заказать button */
    var orderBtn = $('button.order-button[type=submit]');
    if ($('#agreement').attr('checked') != true) {
        orderBtn.show();
        $('#agreement').attr('checked', '');
    }
    $('#agreement').click(agrmtHandler);
    function agrmtHandler() {
        if ($(this).attr('checked')) {
            orderBtn.show();
        }
        else {
            orderBtn.show();
        }
    }

    /* Include/exclude */
    var divaCard = $('#divaby_id');

    if ($('#diva_discount').attr('checked') != true) {
        divaCard.hide();
        $('#divaby_card_id').val('');
    }

    /* Include/exclude */
    var svadbaCard = $('#svadbaby_id');

    if ($('#svadba_discount').attr('checked') != true) {
        svadbaCard.hide();
        $('#svadbaby_card_id').val('');
    }


    /* elpresent code processing */
    $('#discount-code').keyup(function () {
        $('.d-no').hide();
        $('.d-yes').hide();
        $('#total-price').html(startPrice);

        if (this.value.length == 6) {
            $.ajax(
                {
                    type: 'POST',
                    url: '/order/checkCode',
                    data: 'code=' + encodeURIComponent(this.value) + '&item_id=' + $('#item-id').val(),
                    success: function (data) {
                        data = parseInt($.trim(data));

                        if (data > 0) {
                            cardPercent = data;

                            $('.d-yes').show();
                            $('.d-no').hide();
                        }
                        else {
                            cardPercent = 0;
                            removeDiscount();
                            $('.d-yes').hide();
                            $('.d-no').show();
                        }

                        refreshPrice();
                    }
                });
        }
        else if (this.value.length > 6) {
            $('.d-yes').hide();
            $('.d-no').show();
            cardPercent = 0;
            removeDiscount();
            refreshPrice();
        }
        else if (this.value.length < 6) {
            $('.d-yes').hide();
            $('.d-no').hide();
            cardPercent = 0;
            removeDiscount();
            refreshPrice();
        }
    });

    function addDiscount() {
        $('#resultPrice').css(
            {
                textDecoration: 'line-through'
            });

        $('#discount-container').show();
    }

    function removeDiscount() {
        $('#resultPrice').css(
            {
                textDecoration: 'none'
            });

        $('#discount-container').hide();
    }


    /* elpresent code processing end */


    $('#diva_discount').click(divaDiscountHandler);
    function divaDiscountHandler() {
        if ($(this).attr('checked')) {
            divaCard.show();
            $('#divaby_card_id').focus();

            /*discount price processing*/
            refreshPrice();
        }
        else {
            divaCard.hide();
            refreshPrice();

        }
    }


    $('#svadba_discount').click(svadbaDiscountHandler);
    function svadbaDiscountHandler() {
        if ($(this).attr('checked')) {
            /* svadbaCard.show(); */
            $('#svadbaby_card_id').focus();

            /* discount price processing */
            refreshPrice();
        }
        else {
            svadbaCard.hide();
            refreshPrice();
        }
    }


    function refreshPrice() {
        var totalPrice = 0;
        var totalDiscountPrice = 0;

        $('.quantity').each(function () {
            var quantity = parseInt($(this).val());
            var price = parseInt($(this).attr('price'));
            var itemPrice = price * quantity;

            totalPrice += itemPrice;

            var itemId = $(this).attr('item_id').replace('[', '\\[').replace(']', '\\]');
            $('.' + itemId).html($().number_format(itemPrice,
                {
                    numberOfDecimals: 0,
                    decimalSeparator: ',',
                    thousandSeparator: ','
                }));
        });

        totalDiscountPrice = totalPrice;

        /*apply other discounts, if they've setted*/
        totalDiscountPrice = applyDiscounts(totalPrice);

        for (var i = 0; i < ordersCount; i++) {
            if ($('#ed\\[' + i + '\\]').attr('checked')) {
                totalPrice += expressDeliveryPrice;
                totalDiscountPrice += expressDeliveryPrice;
            }
            if ($('#edr\\[' + i + '\\]').attr('checked')) {
                totalPrice += deliveryRecipientPrice;
                totalDiscountPrice += deliveryRecipientPrice;
            }

            totalPrice += deliveryThematicPrice(i);
            totalDiscountPrice += deliveryThematicPrice(i);
        }

        $('#resultPrice').attr('price', totalPrice);
        $('#resultPrice').html($().number_format(totalPrice,
            {
                numberOfDecimals: 0,
                decimalSeparator: ',',
                thousandSeparator: ','
            }));

        $('#resultPriceWithDiscount').attr('price', totalDiscountPrice);
        $('#resultPriceWithDiscount').html($().number_format(totalDiscountPrice,
            {
                numberOfDecimals: 0,
                decimalSeparator: ',',
                thousandSeparator: ','
            }));


        if (totalPrice > totalDiscountPrice) {
            addDiscount();
        }
        else {
            removeDiscount();
        }

    }

    /* function means to get Thematic Delivery Price of current element */
    function deliveryThematicPrice(i) {
        return parseInt($('.thematic-price-block-' + i).attr('thematic_price'));
    }

    /* function means for discounts appling */
    /* now only 4 diva.by */
    function applyDiscounts(iVal) {
        var divabyPercentTmp = ($('#diva_discount').attr('checked') && divabyPercent) ? divabyPercent : 0;
        var svadbabyPercentTmp = ($('#svadba_discount').attr('checked') && svadbabyPercent) ? svadbabyPercent : 0;

        discountPercentTmp = ('undefined' == typeof discountPercent) ? 0 : discountPercent;
        cardPercentTmp = ('undefined' == typeof cardPercent) ? 0 : cardPercent;
        discountPercentTmp = Math.max(discountPercent, divabyPercentTmp, cardPercentTmp, svadbabyPercentTmp);

        iVal = iVal - Math.ceil(iVal / 100 * discountPercentTmp);

        $('#result_discount').attr('result_discount', discountPercentTmp);
        $('#result_discount').text(discountPercentTmp);

        return iVal;
    }

    function decoratePrices() {
        $('#resultPrice').css('text-decoration', 'line-through');
        $('#resultPriceWithDiscount').parent().show();
    }

    totalPrice = parseFloat($('#totalPrice').html());
    discountPercent = parseInt($('#discountPercent').html(), 10);

    deliveryRecipientPrice = parseFloat($('#edr\\[0\\]').attr('price'));
    expressDeliveryPrice = parseFloat($('#ed\\[0\\]').attr('price'));

    resultPrice = parseFloat($('#resultPrice').attr('price'), 10);

    $('.quantity').keyup(function () {
        if ($(this).val() == '') {
            return;
        }

        refreshPrice();
    });

    $('#buyForm').submit(function () {
        if ($('.payment_type_id').val() == 2) {
            $('#show-payment-notification').show();
        }
        reachGoal('zakaz');
        return true;
    });

    function edHandler() {
        var currPrice = parseFloat($('#resultPrice').attr('price'));
        var discPrice = parseFloat($('#resultPriceWithDiscount').attr('price'));
        hiddableFields.show();
        if (this.checked) {
            /*currPrice += expressDeliveryPrice;
             discPrice += expressDeliveryPrice;*/
            currPrice += parseFloat($(this).attr('price'));
            discPrice += parseFloat($(this).attr('price'));
            var id = $(this).attr('id').substr(-4, 1);
            var check = $(this).attr('id').substr(-3, 2);
            if (check == '_0') {
                $('input[id=ed[' + id + ']]:checked').each(function () {
                    currPrice -= parseFloat($(this).attr('price'));
                    discPrice -= parseFloat($(this).attr('price'));
                    $(this).removeAttr('checked');
                });
                $('input[id=ed[' + id + '_1]]:checked').each(function () {
                    currPrice -= parseFloat($(this).attr('price'));
                    discPrice -= parseFloat($(this).attr('price'));
                    $(this).removeAttr('checked');
                });
            } else if (check == '_1') {
                hiddableFields.hide();
                $('input[id=ed[' + id + ']]:checked').each(function () {
                    currPrice -= parseFloat($(this).attr('price'));
                    discPrice -= parseFloat($(this).attr('price'));
                    $(this).removeAttr('checked');
                });
                $('input[id=ed[' + id + '_0]]:checked').each(function () {
                    currPrice -= parseFloat($(this).attr('price'));
                    discPrice -= parseFloat($(this).attr('price'));
                    $(this).removeAttr('checked');
                });
                $('input[id=edr[' + id + ']]:checked').each(function () {
                    $(this).removeAttr('checked');
                    currPrice -= deliveryRecipientPrice;
                    discPrice -= deliveryRecipientPrice;
                    $(this).parent().parent().next().next().find('div.recipient-info').hide();
                });

            } else {
                id = $(this).attr('id').substr(-2, 1);
                $('input[id=ed[' + id + '_1]]:checked').each(function () {
                    currPrice -= parseFloat($(this).attr('price'));
                    discPrice -= parseFloat($(this).attr('price'));
                    $(this).removeAttr('checked');
                });
                $('input[id=ed[' + id + '_0]]:checked').each(function () {
                    currPrice -= parseFloat($(this).attr('price'));
                    discPrice -= parseFloat($(this).attr('price'));
                    $(this).removeAttr('checked');
                });
            }

        }
        else {
            currPrice -= parseFloat($(this).attr('price'));
            discPrice -= parseFloat($(this).attr('price'));

        }


        $('#resultPrice').attr('price', currPrice);
        $('#resultPrice').html($().number_format(currPrice,
            {
                numberOfDecimals: 0,
                decimalSeparator: ',',
                thousandSeparator: ','
            }));


        $('#resultPriceWithDiscount').attr('price', discPrice);
        $('#resultPriceWithDiscount').html($().number_format(discPrice,
            {
                numberOfDecimals: 0,
                decimalSeparator: ',',
                thousandSeparator: ','
            }));

    }


    function countSelectedThematicDelivs() {
        return $('option[thematic_price]:selected').filter(function () {
            return parseInt($(this).attr('thematic_price')) > 0;
        }).length;
    }


    function countUnSelectedThematicDelivs() {
        return $('option[thematic_price]:selected').filter(function () {
            return parseInt($(this).attr('thematic_price')) == 0;
        }).length;
    }


    function clearCurrentThematicDelivery(elCheck) {
        var themPriceBlock = $(elCheck).parent().parent().prev().find('i[thematic_price]');
        var themPrice = parseInt(themPriceBlock.attr('thematic_price'));

        themPriceBlock.attr('thematic_price', 0);
        themPriceBlock.text('0 руб.');

        /* modifing current sum prices */
        var totalPrice = parseInt($('#resultPrice').attr('price'));
        var totalDiscountPrice = parseInt($('#resultPriceWithDiscount').attr('price'));

        totalPrice -= themPrice;
        totalDiscountPrice -= themPrice;

        $('#resultPrice').attr('price', totalPrice);
        $('#resultPrice').html($().number_format(totalPrice,
            {
                numberOfDecimals: 0,
                decimalSeparator: ',',
                thousandSeparator: ','
            }));

        $('#resultPriceWithDiscount').attr('price', totalDiscountPrice);
        $('#resultPriceWithDiscount').html($().number_format(totalDiscountPrice,
            {
                numberOfDecimals: 0,
                decimalSeparator: ',',
                thousandSeparator: ','
            }));


        /* set selectbox to correct values */
        selDelivBlock = themPriceBlock.parent().prev();

        selDelivBlock.find('option[selected=selected]').removeAttr('selected');
        selDelivBlock.find('option[elem]:first').attr('selected', 'selected');
        selDelivBlock.find('span.jquery-selectbox-currentItem').text(selDelivBlock.find('option[elem]:first').text());
    }


    function edrHandler() {

        var checkedEdr = $('.edr:checked');

        var edrArr = $('input[id^=edr]');
        var edrCheckedArr = $('input[id^=edr]:checked');
        var edrUncheckedArr = $('input[id^=edr]').not(':checked');

        var edrLen = edrArr.length;
        var edrCheckedLen = edrCheckedArr.length + countSelectedThematicDelivs();
        var edrUncheckedLen = edrUncheckedArr.length + countUnSelectedThematicDelivs();

        if (this.checked) {

            clearCurrentThematicDelivery(this);
            var id = $(this).attr('id').substr(-2, 1);

            var currPrice = parseFloat($('#resultPrice').attr('price'));
            var discPrice = parseFloat($('#resultPriceWithDiscount').attr('price'));

            currPrice += deliveryRecipientPrice;
            discPrice += deliveryRecipientPrice;

            $('input[id=ed[' + id + '_1]]:checked').each(function () {
                currPrice -= parseFloat($(this).attr('price'));
                discPrice -= parseFloat($(this).attr('price'));
                $(this).removeAttr('checked');
            });
            /* show recipientInfo block */
            $(this).parent().parent().next().next().find('div.recipient-info').show();


            /* show pay fields */
            $('#pay-date-tr').show();
            $('#pay-time-tr').show();

            /* hide get fields, if necessary */
            if (edrLen == edrCheckedLen) {
                $('#pay-adr-tr').show();
                $('#gift-date-tr').hide();
                $('#gift-time-tr').hide();
                $('#adr-tr').hide();
            }
        }
        else {
            var currPrice = parseFloat($('#resultPrice').attr('price'));
            var discPrice = parseFloat($('#resultPriceWithDiscount').attr('price'));


            currPrice -= deliveryRecipientPrice;
            discPrice -= deliveryRecipientPrice;

            /* hide recipientInfo block */
            $(this).parent().parent().next().next().find('div.recipient-info').hide();

            /* hide pay fields, if necessary */
            if (edrUncheckedLen == edrLen * 2) {
                $('#pay-date-tr').hide();
                $('#pay-time-tr').hide();
            }

            $('#pay-adr-tr').hide();

            /* show get fields */
            $('#gift-date-tr').show();
            $('#gift-time-tr').show();
            $('#adr-tr').show();
        }

        $('#resultPrice').attr('price', currPrice);
        $('#resultPrice').html($().number_format(currPrice,
            {
                numberOfDecimals: 0,
                decimalSeparator: ',',
                thousandSeparator: ','
            }));

        $('#resultPriceWithDiscount').attr('price', discPrice);
        $('#resultPriceWithDiscount').html($().number_format(discPrice,
            {
                numberOfDecimals: 0,
                decimalSeparator: ',',
                thousandSeparator: ','
            }));
    }

    window.edrHandler = edrHandler;

    function getCopyId(oElem) {
        var sId = $(oElem).attr('id');
        return sId.substr(9, sId.length - 10);
    }

    /* function means for coping previos form fields to current form */
    function copyHandler() {
        var id = getCopyId(this);

        var prevId = id--;
        var edrEl = $('#edr\\[' + id + '\\]');

        var hasThematicSelected = (parseInt($('.thematic-price-block-' + id).attr('thematic_price')) > 0) ? true : false;

        if (edrEl[0].checked || hasThematicSelected) {
            prevFirstname = $('#recipient-firstname\\[' + id + '\\]').val();
            $('#recipient-firstname\\[' + prevId + '\\]').val(prevFirstname);

            prevLastname = $('#recipient-lastname\\[' + id + '\\]').val();
            $('#recipient-lastname\\[' + prevId + '\\]').val(prevLastname);

            prevAddress = $('#recipient-address\\[' + id + '\\]').val();
            $('#recipient-address\\[' + prevId + '\\]').val(prevAddress);

            prevPhone = $('#recipient-phone\\[' + id + '\\]').val();
            $('#recipient-phone\\[' + prevId + '\\]').val(prevPhone);

            prevDate = $('#recipient-date\\[' + id + '\\]').val();
            $('#recipient-date\\[' + prevId + '\\]').val(prevDate);

            prevHour = $('#recipient-time-hour\\[' + id + '\\]').val();
            $('#recipient-time-hour\\[' + prevId + '\\]').val(prevHour);

            prevMin = $('#recipient-time-minute\\[' + id + '\\]').val();
            $('#recipient-time-minute\\[' + prevId + '\\]').val(prevMin);
        }
    }

    for (var i = 0; i < ordersCount; i++) {
        $('#ed\\[' + i + '\\]').click(edHandler);
        $('#ed\\[' + i + '_0\\]').click(edHandler);
        $('#ed\\[' + i + '_1\\]').click(edHandler);
        $('#edr\\[' + i + '\\]').click(edrHandler);
        $('#edr_copy\\[' + i + '\\]').click(copyHandler);
    }
    ;
    $('#gift-date').datepicker(
        {
            minDate: new Date(),
            dateFormat: 'dd.mm.yy'
        });
    ;
    $('#pay-date').datepicker(
        {
            minDate: new Date(),
            dateFormat: 'dd.mm.yy'
        });
    ;
    $('.recipient-date').datepicker(
        {
            minDate: new Date(),
            dateFormat: 'dd.mm.yy'
        });


});

</script>


<div class="article">
<div class="section">
<form action='' method='post' id='buyForm'>
<table class="text-head-green" cellpadding="0" cellspacing="0">
    <tbody>
    <tr>
        <td class="left">&nbsp;</td>
        <td class="center"><h1>Оформление заказа</h1></td>
        <td class="right">&nbsp;</td>
    </tr>
    </tbody>
</table>
<table width="100%" cellpadding="0" cellspacing="0" style="min-width: 830px;">
<tbody>
<tr>
<td class="text-main-left">&nbsp;</td>
<td class="text-main">
<? if ($oShopOrder->oBuyForm->getErrors()) : ?>
    <div class="d-no">
        <?= CHtml::errorSummary($oShopOrder->oBuyForm); ?>
    </div>
<? endif; ?>

<? if (!$oShopOrder->getDiscountPercent()) : ?>
    <div class="d-no hide">
        Неверный код
    </div>
    <div class="d-yes hide">
        Верный код
    </div>
    <?php if (0) { ?>
        <div class="gray-bg">
            <table class="form-inputs">
                <tr>
                    <td class="one" style="width: 400px; padding-top: 0px;">
                        <label for="discount-code">Если Вы получили купон на скидку в подарочной коробке, впишите сюда
                            код, расположенный на оборотной стороне купона</label>
                    </td>
                    <td class="two">
                        <div class="input-order" style="width: 300px;">
                            <input type="hidden" id="item-id" value=""/>
                            <input type="text" name='discount_code' value='' id="discount-code">

                            <div class="input-order-right"></div>
                        </div>
                        <div class="clear"><!-- --></div>
                    </td>
                </tr>
            </table>
        </div>
    <?php } ?>
    <br/>
<? elseif (Yii::app()->request->getParam('discount_code')): ?>
    <input type="hidden" type="text" name='discount_code' value='<?= Yii::app()->request->getParam('discount_code') ?>'
           id="discount-code">
<? endif; ?>
<div class="order-form">
<h2>Ваш заказ:</h2>

<div id="discountPercent" style="display: none;"><?= $oShopOrder->getDiscountPercent(); ?></div>
<table class="form">
<tbody>
<tr class="hr">
    <td colspan="3"></td>
</tr>
<?
foreach ($oShopOrder as $id => $oOrder) : ?>
    <? if (!$oOrder->item->available && $oOrder->item->type == "item") : ?>
        <tr>
            <td colspan="3">
                <p class="d-no"> В настоящий момент этого впечатления нет в наличии, выберите другое впечатление.</p>
            </td>
        </tr>
    <? endif; ?>
    <tr>
        <td>
            Подарок: <?= $oOrder->item->title; ?>
        </td>
        <td>
            <input type="text" size="2" class="quantity" item_id="item_<?= $oOrder->item->id; ?>[<?= $id ?>]"
                   price="<?= $oOrder->item->price; ?>" name="item_quantity[<?= $oOrder->item->id; ?>][<?= $id ?>]"
                   value="<?= $oOrder->item_quantity; ?>"/>
        </td>
        <td class="order-form-price">
            <strong><span
                    class="buy_items item_<?= $oOrder->item->id; ?>[<?= $id ?>]"><?= ($oShopOrder->getItemPrice($oOrder)); ?></span>
                руб.</strong>
        </td>
    </tr>
    <? if ($oOrder->flower) : ?>
        <tr>
            <td>
                Цветы: <?= $oOrder->flower->flower_title; ?>
            </td>
            <td>
                <? $oShopOrder->getFlowerQuantity($oOrder, $id); ?>
                <input type="text" size="2" class="quantity" price="<?= $oOrder->flower->flower_price; ?>"
                       item_id="flower_<?= $oOrder->flower->flower_id; ?>[<?= $id ?>]"
                       name="flower_quantity[<?= $oOrder->flower->flower_id; ?>][<?= $id ?>]"
                       value="<?= $oShopOrder->getFlowerQuantity($oOrder, $id); ?>"/>
            </td>
            <td class="order-form-price">
                <strong><span
                        class="buy_flowers flower_<?= $oOrder->flower->flower_id; ?>[<?= $id ?>]"><?= ($oShopOrder->getFlowerPrice($oOrder, $id)); ?></span>
                    руб.</strong>
            </td>
        </tr>
    <? endif; ?>
    <? if ($oOrder->card) : ?>
        <tr>
            <td>
                Открытка: <?= $oOrder->card->card_title; ?>
            </td>
            <td>
                <input type="text" size="2" class="quantity" price="<?= $oOrder->card->card_price; ?>"
                       item_id="card_<?= $oOrder->card->card_id; ?>[<?= $id ?>]"
                       name="card_quantity[<?= $oOrder->card->card_id; ?>][<?= $id ?>]"
                       value="<?= $oShopOrder->getCardQuantity($oOrder, $id); ?>"/>
            </td>
            <td class="order-form-price">
                <strong><span
                        class="buy_cards card_<?= $oOrder->card->card_id; ?>[<?= $id ?>]"><?= ($oShopOrder->getCardPrice($oOrder, $id)); ?></span>
                    руб.</strong>
            </td>
        </tr>

    <? endif; ?>
    <tr class="extra_options">
        <td style="width:150px;">
            <div>
                <input class="ed" type="checkbox" id="ed[<?= $id ?>_1]"
                       price="<?= $oShopOrder->getDeliveryPriceByType(1); ?>" name="delivery_id_1[<?= $id ?>]"
                       style="display: block; float: left; margin: 2px 5px 0px 0px;" <?= $oOrder->delivery == 1 ? ' checked="checked"' : ''; ?> />

                                        <span class="delivery">
                                            <a href="javascript: void(0);" class="tooltip">
                                                <span><?= BuyForm::$delivery[1]['desc']; ?></span>
                                                <label
                                                    for="ed[<?= $id ?>_1]"><?= BuyForm::$delivery[1]['title']; ?></label>
                                            </a>
                                        </span>
                <br/>
                <i class="delivery-sum"><?= formatDeliveryPriceLabel($oShopOrder->getDeliveryPriceByType(1)); ?></i>
            </div>
        </td>

        <td style="width:150px;">
            <div>
                <input class="ed" type="checkbox" id="ed[<?= $id ?>_0]"
                       price="<?= $oShopOrder->getDeliveryPriceByType(0); ?>" name="delivery_id_0[<?= $id ?>]"
                       style="display: block; float: left; margin: 2px 5px 0px 0px;" <?= $oOrder->delivery == 0 ? ' checked="checked"' : ''; ?> />

                                        <span class="delivery">
                                            <a href="javascript: void(0);" class="tooltip">
                                                <span><?= BuyForm::$delivery[0]['desc']; ?></span>
                                                <label
                                                    for="ed[<?= $id ?>_0]"><?= BuyForm::$delivery[0]['title']; ?></label>
                                            </a>
                                        </span>
                <br/>
                <i class="delivery-sum"><?= formatDeliveryPriceLabel($oShopOrder->getDeliveryPriceByType(0)); ?></i>
            </div>
        </td>
        <td>

            <select name="thematic_delivery[<?= $id ?>]" class="edt" id="edt[<?= $id ?>]"
                    style="display: block; float: left; margin: 2px 5px 0px 0px;">
                <? foreach (BuyForm::$aThematicDelivery as $iDeliveryKey => $aDelivery) : ?>
                    <option
                        value="<?= $iDeliveryKey; ?>" <?= ($iDeliveryKey == $oShopOrder->hasThematicDelivery($id)) ? 'selected="selected"' : ''; ?>
                        thematic_price="<?= $aDelivery['price']; ?>"
                        elem="<?= $id ?>"><?= $aDelivery['title']; ?></option>
                <? endforeach; ?>
            </select>


            <div style="margin-left: 80px; padding: 0 0 0 60px;">
                                    	<span class="delivery">
                                    		<a href="javascript: void(0);" class="tooltip">
                                                <span>При выборе опции «тематическое вручение», наш артист доставит подарок человеку, которого вы хотите поздравить.</span>
                                                <label for="edt[<?= $id ?>]">Тематическая доставка </label>
                                            </a>
                                    	</span>

                <i class="delivery-sum thematic-price-block-<?= $id ?>"
                   thematic_price="<?= $oOrder->thematic_delivery_price ?>"><?= formatDeliveryPriceLabel($oOrder->thematic_delivery_price); ?></i>
            </div>
        </td>
    </tr>
    <tr class="extra_options">
        <td>
            <input class="edr" type="checkbox" id="edr[<?= $id ?>]"
                   price="<?= $oShopOrder->getDeliveryForRecipientPrice($id); ?>"
                   name="is_express_delivery_recipient[<?= $id ?>]"
                   style="display: block; float: left; margin: 2px 5px 0px 0px;" <?= $oShopOrder->isDeliveryForRecipient($id) ? ' checked="checked"' : ''; ?> />

            <div style="margin-left: 15px;">
                                    	<span class="delivery">
                                    		<a href="javascript: void(0);" class="tooltip">
                                                <span>При выборе опции «вручить лично получателю подарка», курьер доставит подарок человеку, которого вы хотите поздравить.</span>
                                                <label for="edr[<?= $id ?>]">Вручить лично получателю подарка</label>
                                            </a>
                                    	</span>
                <br/>
                <i class="delivery-sum"><?= formatDeliveryPriceLabel($oShopOrder->getDeliveryForRecipientPrice()); ?></i>
            </div>
        </td>
        <td style="vertical-align:top;">

            <div>
                <input class="ed" type="checkbox" id="ed[<?= $id ?>]"
                       price="<?= $oShopOrder->getExpressDeliveryPrice($id); ?>" name="is_express_delivery[<?= $id ?>]"
                       style="display: block; float: left; margin: 2px 5px 0px 0px;" <?= $oShopOrder->isExpressDelivery($id) ? ' checked="checked"' : ''; ?> />

                                        <span class="delivery">
                                            <a href="javascript: void(0);" class="tooltip">
                                                <span>При выборе опции «экспресс доставка» курьер доставит подарок в указанное место в течение 2-х часов c момента подтверждения заказа.</span>
                                                <label for="ed[<?= $id ?>]">Экспресс доставка</label>
                                            </a>
                                        </span>
                <br/>
                <i class="delivery-sum"><?= formatDeliveryPriceLabel($oShopOrder->getExpressDeliveryPrice()); ?></i>
            </div>

        </td>
        <td>

        </td>


    </tr>

    <!--                    multiple user accounts                              -->
    <tr>
        <td></td>
        <td colspan="2" style="display: <?= ($id == 0) ? 'none' : 'block'; ?>;">
            <div style="margin-left: 15px;">
                                    	<span class="delivery" id="edr_copy[<?= $id ?>]">
                                            <a href="javascript: void(0);" class="tooltip">
                                                <span>Скопировать заполненые поля.</span>
                                                <label for="edr_copy[<?= $id ?>]">Использовать предыдущие поля</label>
                                            </a>
                                    	</span>
            </div>
        </td>
    </tr>
    <tr>

        <td colspan="3">

            <!--                                -->
            <div class="recipient-info gray-bg buy"
                 style="display: <?= ($oShopOrder->isDeliveryForRecipient($id)) ? 'block' : 'none'; ?>">
                <table class="form-inputs">
                    <tr>
                        <td class="one">
                            <label for="recipient-firstname[<?= $id ?>]">Имя получателя подарка:</label><br><i>(обязательно)</i>
                        </td>
                        <td class="two big">
                            <div class="input-order">
                                <input type="text" id="recipient-firstname[<?= $id ?>]"
                                       name='BuyForm[recipient_firstname][<?= $id ?>]'
                                       value='<?= $oShopOrder->oBuyForm->recipient_firstname[$id]; ?>'>

                                <div class="input-order-right"></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="one">
                            <label for="recipient-lastname[<?= $id ?>]">Фамилия получателя подарка:</label><br><i>(обязательно)</i>
                        </td>
                        <td class="two big">
                            <div class="input-order">
                                <input type="text" name='BuyForm[recipient_lastname][<?= $id ?>]'
                                       id="recipient-lastname[<?= $id ?>]"
                                       value='<?= $oShopOrder->oBuyForm->recipient_lastname[$id]; ?>'>

                                <div class="input-order-right"></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="one">
                            <label for="recipient-address[<?= $id ?>]">Адрес вручения подарка:</label><br><i>(обязательно)</i>
                        </td>
                        <td class="two big">
                            <div class="input-order">
                                <input type="text" name='BuyForm[recipient_address][<?= $id ?>]'
                                       id="recipient-address[<?= $id ?>]"
                                       value='<?= $oShopOrder->oBuyForm->recipient_address[$id]; ?>'>

                                <div class="input-order-right"></div>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td class="one">
                            <label for="recipient-phone[<?= $id ?>]">Контактный телефон получателя
                                подарка:</label><br><i>(обязательно)</i>
                        </td>
                        <td class="two big">
                            <div class="input-order">
                                <input type="text" name='BuyForm[recipient_phone][<?= $id ?>]'
                                       id="recipient-phone[<?= $id ?>]"
                                       value='<?= $oShopOrder->oBuyForm->recipient_phone[$id]; ?>'>

                                <div class="input-order-right"></div>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td class="one">
                            <label for="recipient-date[<?= $id ?>]">Желаемая дата вручения подарка:</label><br><i>(обязательно)</i>
                        </td>
                        <td class="two big">
                            <div class="input-order">
                                <input class="recipient-date" type="text"
                                       name='BuyForm[recipient_desired_date][<?= $id ?>]'
                                       id="recipient-date[<?= $id ?>]"
                                       value='<?= $oShopOrder->oBuyForm->recipient_desired_date[$id]; ?>'>

                                <div class="input-order-right"></div>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td class="one">
                            <label for="recipient-time-hour[<?= $id ?>]">Желаемое время вручения подарка:<br><i>(обязательно)</i></label>
                        </td>
                        <td class="two big">
                            <table style="width:100%">
                                <tr>
                                    <td style="padding: 0 5px 0 0;">с</td>
                                    <td style="padding: 0 40px 0 0;">
                                        <div>
                                            <select name='BuyForm[recipient_desired_hour_from][<?= $id ?>]'
                                                    id="recipient-time-hour[<?= $id ?>]">
                                                <?
                                                $iHourFrom = ($oShopOrder->oBuyForm->recipient_desired_hour_from[$id]) ? $oShopOrder->oBuyForm->recipient_desired_hour_from[$id] : 10;
                                                foreach (BuyForm::hourRange() as $iHour): ?>
                                                    <option
                                                        value="<?= $iHour; ?>" <?= ($iHour == $iHourFrom) ? 'selected="selected"' : ''; ?>><?= $iHour; ?></option>
                                                <? endforeach; ?>
                                            </select>
                                        </div>
                                    </td>
                                    <td style="padding: 0 5px 0 5px;">:</td>
                                    <td style="padding: 0 40px 0 0;">
                                        <div>
                                            <select name='BuyForm[recipient_desired_minute_from][<?= $id ?>]'
                                                    id="recipient-time-minute[<?= $id ?>]">
                                                <? foreach (BuyForm::minuteRange() as $iMinute): ?>
                                                    <option
                                                        value="<?= $iMinute; ?>" <?= ($iMinute == $oShopOrder->oBuyForm->recipient_desired_minute_from[$id]) ? 'selected="selected"' : ''; ?>><?= $iMinute; ?></option>
                                                <? endforeach; ?>
                                            </select>
                                        </div>
                                    </td>
                                    <td style="padding: 0 5px 0 25px;">по</td>
                                    <td style="padding: 0 40px 0 0;">
                                        <div>
                                            <select name='BuyForm[recipient_desired_hour_to][<?= $id ?>]'
                                                    id="recipient-time-hour-to[<?= $id ?>]">
                                                <?
                                                $iHourTo = ($oShopOrder->oBuyForm->recipient_desired_hour_to[$id]) ? $oShopOrder->oBuyForm->recipient_desired_hour_to[$id] : 20;
                                                foreach (BuyForm::hourRange() as $iHour): ?>
                                                    <option
                                                        value="<?= $iHour; ?>" <?= ($iHour == $iHourTo) ? 'selected="selected"' : ''; ?>><?= $iHour; ?></option>
                                                <? endforeach; ?>
                                            </select>
                                        </div>
                                    </td>
                                    <td style="padding: 0 5px 0 5px;">:</td>
                                    <td style="padding: 0 40px 0 0;">
                                        <div>
                                            <select name='BuyForm[recipient_desired_minute_to][<?= $id ?>]'
                                                    id="recipient-time-minute-to[<?= $id ?>]">
                                                <? foreach (BuyForm::minuteRange() as $iMinute): ?>
                                                    <option
                                                        value="<?= $iMinute; ?>" <?= ($iMinute == $oShopOrder->oBuyForm->recipient_desired_minute_to[$id]) ? 'selected="selected"' : ''; ?>><?= $iMinute; ?></option>
                                                <? endforeach; ?>
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                            </table>


                        </td>
                    </tr>

                </table>
            </div>
            <!--                                -->
        </td>
    </tr>


    <tr class="hr">
        <td colspan="3"></td>
    </tr>

<? endforeach; ?>
</tbody>
</table>


<table class="form-extra">

    <? if (!$oShopOrder->hasDivaDiscount()): ?>
        <tr id="discount_tr">
            <td colspan="2">
                <label for="diva_discount">Скидка diva.by 5%:<br></label>
            </td>
            <td>
                <input type="checkbox" name='BuyForm[divaby_discount]'
                       id="diva_discount" <? if ($oShopOrder->oBuyForm->divaby_discount && $oShopOrder->hasDivaDiscount()) {
                    echo 'checked="checked"';
                } ?>>
                <input type="hidden" id="divaby_discount_percent" value="<?= $oShopOrder->getDivaDiscountPercent() ?>">
            </td>
        </tr>


        <tr id="divaby_id">
            <td colspan="2">
                <label for="divaby_card_id">Номер дисконтной карты:<br><i>(обязательно)</i></label>
            </td>
            <td>
                <div class="input-order">
                    <input id="divaby_card_id" type="text" name='BuyForm[divaby_card_id]'
                           value='<?= $oShopOrder->oBuyForm->divaby_card_id; ?>'>

                    <div class="input-order-right"></div>
                </div>
            </td>
        </tr>
    <? endif; ?>



    <?


    if (!$oShopOrder->hasSvadbaDiscount()): ?>
        <tr id="discount_tr">
            <td colspan="2">
                <label for="svadba_discount">Скидка rebenok.by 5%:<br></label>
            </td>
            <td>
                <input type="checkbox" name='BuyForm[svadbaby_discount]'
                       id="svadba_discount" <? if ($oShopOrder->oBuyForm->svadbaby_discount && $oShopOrder->hasSvadbaDiscount()) {
                    echo 'checked="checked"';
                } ?>>
                <input type="hidden" id="svadbaby_discount_percent"
                       value="<?= $oShopOrder->getSvadbaDiscountPercent() ?>">
            </td>
        </tr>

        <tr id="svadbaby_id">
            <td colspan="2">
                <label for="svadbaby_card_id">Номер дисконтной карты:<br><i>(обязательно)</i></label>
            </td>
            <td>
                <div class="input-order">
                    <input id="svadbaby_card_id" type="text" name='BuyForm[svadbaby_card_id]'
                           value='<?= $oShopOrder->oBuyForm->svadbaby_card_id; ?>'>

                    <div class="input-order-right"></div>
                </div>
            </td>
        </tr>
    <? endif; ?>

    <tr>
        <td class="one">
            Оплата:
        </td>
        <td class="two">
            <div class="payment_type_id_container">
                <select name="BuyForm[payment_type_id]" class="payment_type_id no-fancy-select"
                        onChange="changePayType(this)">
                    <option value="1">Курьеру</option>
                    <!--<option value="4">Easypay</option>-->
                    <!--<option value="5">Пластиковая карта (Webpay)</option>-->
                    <option value="6">Картой Visa, MasterCard, Maestro онлайн</option>
                    <option value="7">Картой Халва (2 месяца рассрочки)</option>
                    <?php //<option value="2">Система "Скарбник"</option> ?>
                </select>

            </div>
        </td>
        <td>
            <div class="gray-bg total-price" style="text-align: right;">Итого:
                <strong <? if ($oShopOrder->hasDiscount()) : ?>style="text-decoration: line-through;" <? endif; ?>>
                    <span id="resultPrice"
                          price="<?= $oShopOrder->getTotalPrice(); ?>"><?= ($oShopOrder->getTotalPrice()); ?></span>
                    руб.
                </strong>
                <br/>

                                                                        <span id="discount-container"
                                                                              <? if (!$oShopOrder->hasDiscount()) : ?>style="display: none;" <? endif; ?>>
                                                                            <span>Скидка <span id="result_discount"
                                                                                               result_discount="<?= $oShopOrder->getDiscountPercent(); ?>"><?= $oShopOrder->getDiscountPercent(); ?></span>%</span>
                                                                            <br/>
                                                                            <strong><span id="resultPriceWithDiscount"
                                                                                          price="<?= $oShopOrder->getTotalPriceWithDiscount(); ?>"><?= ($oShopOrder->getTotalPriceWithDiscount()); ?></span>
                                                                                руб.</strong>
									</span>

            </div>
        </td>
    </tr>
    <tr class="easypay" style="display:none;">
        <td class="one">

            Идентификатор электронного кошелька EasyPay:
        </td>
        <td class="two">
            <div class="input-order form-inputs">
                <input type="text" maxlength="8" name="BuyForm[easypay_card]"/>

                <div class="input-order-right"></div>
            </div>
        </td>
    </tr>

</table>
<div class="clear"><!-- --></div>
<div id="show-payment-notification">Сейчас вы будете перенаправлены на страницу платёжной системы.</div>

<div class="gray-bg buy">
<table class="form-inputs">
<tbody>
<tr>
    <td class="one">
        <label for="firstname">Ваше Имя:</label><br><i>(обязательно)</i>
    </td>
    <td class="two">
        <div class="input-order">
            <input type="text" name='BuyForm[firstname]' id="firstname"
                   value='<?= $oShopOrder->oBuyForm->firstname; ?>'>

            <div class="input-order-right"></div>
        </div>
    </td>
</tr>
<tr>
    <td class="one">
        <label for="lastname">Ваша Фамилия:</label><br><i>(обязательно)</i>
    </td>
    <td class="two">
        <div class="input-order">
            <input type="text" name='BuyForm[lastname]' id="lastname" value='<?= $oShopOrder->oBuyForm->lastname; ?>'>

            <div class="input-order-right"></div>
        </div>
    </td>
</tr>
<tr>
    <td class="one">
        <label for="phone">Ваш контактный телефон:</label><br><i>(обязательно)</i>
    </td>
    <td class="two">
        <div class="input-order">
            <input type="text" name='BuyForm[phone]' id="phone" class="validate[required]"
                   value='<?= $oShopOrder->oBuyForm->phone; ?>'>

            <div class="input-order-right"></div>
        </div>
    </td>
</tr>
<tr>
    <td class="one">
        <label for="email">E-mail:</label><br><i>(обязательно)</i>
    </td>
    <td class="two">
        <div class="input-order">
            <input type="text" name='BuyForm[email]' value='<?= $oShopOrder->oBuyForm->email; ?>' id="email"
                   class="validate[email]">

            <div class="input-order-right"></div>
        </div>
    </td>
</tr>
<tr id="adr-tr" style="display: <?= ($oShopOrder->detectScenario() != 'recipient') ? 'table-row' : 'none'; ?>">
    <td class="one">
        <label for="adr">Адрес доставки:</label><br><i>(обязательно)</i>
    </td>
    <td class="two">
        <div class="input-order">
            <input type="text" name='BuyForm[adr]' id="adr" value='<?= $oShopOrder->oBuyForm->adr; ?>'>

            <div class="input-order-right"></div>
        </div>
    </td>
</tr>

<tr id="pay-adr-tr" style="display: <?= ($oShopOrder->detectScenario() != 'user') ? 'table-row' : 'none'; ?>">
    <td class="one">
        <label for="pay_adr">Адрес оплаты заказа:</label><br><i>(обязательно)</i>
    </td>
    <td class="two">
        <div class="input-order">
            <input type="text" name='BuyForm[payer_adr]' id="pay_adr"
                   value='<?= ((boolean)$oShopOrder->oBuyForm->payer_adr) ? $oShopOrder->oBuyForm->payer_adr : $oShopOrder->oBuyForm->adr; ?>'>

            <div class="input-order-right"></div>
        </div>
    </td>
</tr>

<tr id="gift-date-tr" style="display: <?= ($oShopOrder->detectScenario() != 'recipient') ? 'table-row' : 'none'; ?>">
    <td class="one">
        <label for="gift-date">Желаемая дата доставки:<br><i>(обязательно)</i></label>
    </td>
    <td class="two">
        <div class="input-order">
            <input class="gift-date" type="text" name='BuyForm[gift_date]' id="gift-date"
                   value='<?= $oShopOrder->oBuyForm->gift_date; ?>'>

            <div class="input-order-right"></div>
        </div>
    </td>
</tr>

<tr id="pay-date-tr" style="display: <?= ($oShopOrder->detectScenario() != 'user') ? 'table-row' : 'none'; ?>">
    <td class="one">
        <label for="pay-date">Желаемая дата оплаты:<br><i>(обязательно)</i></label>
    </td>
    <td class="two">
        <div class="input-order">
            <input type="text" name='BuyForm[payer_date]' id="pay-date"
                   value='<?= $oShopOrder->oBuyForm->payer_date; ?>'>

            <div class="input-order-right"></div>
        </div>
    </td>
</tr>

<tr id="gift-time-tr" style="display: <?= ($oShopOrder->detectScenario() != 'recipient') ? 'table-row' : 'none'; ?>">
    <td class="one">
        <label for="gift-time-hour">Желаемое время доставки:<br><i>(обязательно)</i></label>
    </td>
    <td class="two">
        <table style="width:100%">
            <tr>
                <td style="padding: 0 5px 0 0;">с</td>
                <td style="padding: 0 40px 0 0;">
                    <div>
                        <select class="reciver-time-hour" name='BuyForm[gift_hour_from]' id="gift-time-hour">
                            <?
                            $iHourFrom = ($oShopOrder->oBuyForm->gift_hour_from) ? $oShopOrder->oBuyForm->gift_hour_from : 10;
                            foreach (BuyForm::hourRange() as $iHour): ?>
                                <option
                                    value="<?= $iHour; ?>" <?= ($iHour == $iHourFrom) ? 'selected="selected"' : ''; ?>><?= $iHour; ?></option>
                            <? endforeach; ?>
                        </select>
                    </div>
                </td>
                <td style="padding: 0 5px 0 5px;">:</td>
                <td style="padding: 0 40px 0 0;">
                    <div>
                        <select class="reciver-time-minute" name='BuyForm[gift_minute_from]' id="gift-time-minute">
                            <? foreach (BuyForm::minuteRange() as $iMinute): ?>
                                <option
                                    value="<?= $iMinute; ?>" <?= ($iMinute == $oShopOrder->oBuyForm->gift_minute_from) ? 'selected="selected"' : ''; ?>><?= $iMinute; ?></option>
                            <? endforeach; ?>
                        </select>
                    </div>
                </td>
                <td style="padding: 0 5px 0 25px;">по</td>
                <td style="padding: 0 40px 0 0;">
                    <div>
                        <select class="reciver-time-hour" name='BuyForm[gift_hour_to]' id="gift-time-hour-to">
                            <?
                            $iHourTo = ($oShopOrder->oBuyForm->gift_hour_to) ? $oShopOrder->oBuyForm->gift_hour_to : 20;
                            foreach (BuyForm::hourRange() as $iHour): ?>
                                <option
                                    value="<?= $iHour; ?>" <?= ($iHour == $iHourTo) ? 'selected="selected"' : ''; ?>><?= $iHour; ?></option>
                            <? endforeach; ?>
                        </select>
                    </div>
                </td>
                <td style="padding: 0 5px 0 5px;">:</td>
                <td style="padding: 0 40px 0 0;">
                    <div>
                        <select class="reciver-time-minute" name='BuyForm[gift_minute_to]' id="gift-time-minute-to">
                            <? foreach (BuyForm::minuteRange() as $iMinute): ?>
                                <option
                                    value="<?= $iMinute; ?>" <?= ($iMinute == $oShopOrder->oBuyForm->gift_minute_to) ? 'selected="selected"' : ''; ?>><?= $iMinute; ?></option>
                            <? endforeach; ?>
                        </select>
                    </div>
                </td>
            </tr>
        </table>
    </td>
</tr>

<tr id="pay-time-tr" style="display: <?= ($oShopOrder->detectScenario() != 'user') ? 'table-row' : 'none'; ?>">
    <td class="one">
        <label for="pay-time-hour">Желаемое время оплаты:<br><i>(обязательно)</i></label>
    </td>
    <td class="two">
        <table style="width:100%">
            <tr>
                <td style="padding: 0 5px 0 0;">с</td>
                <td style="padding: 0 40px 0 0;">
                    <div>
                        <select class="pay-time-hour" name="BuyForm[payer_hour_from]" id="pay-time-hour">
                            <?
                            $iHourFrom = ($oShopOrder->oBuyForm->payer_hour_from) ? $oShopOrder->oBuyForm->payer_hour_from : 10;
                            foreach (BuyForm::hourRange() as $iHour): ?>
                                <option
                                    value="<?= $iHour; ?>" <?= ($iHour == $iHourFrom) ? 'selected="selected"' : ''; ?>><?= $iHour; ?></option>
                            <? endforeach; ?>
                        </select>
                    </div>
                </td>
                <td style="padding: 0 5px 0 5px;">:</td>
                <td style="padding: 0 40px 0 0;">
                    <div>
                        <select class="gift-time-minute" name="BuyForm[payer_minute_from]" id="gift-time-minute">
                            <? foreach (BuyForm::minuteRange() as $iMinute): ?>
                                <option
                                    value="<?= $iMinute; ?>" <?= ($iMinute == $oShopOrder->oBuyForm->payer_minute_from) ? 'selected="selected"' : ''; ?>><?= $iMinute; ?></option>
                            <? endforeach; ?>
                        </select>
                    </div>
                </td>
                <td style="padding: 0 5px 0 25px;">по</td>
                <td style="padding: 0 40px 0 0;">
                    <div>
                        <select class="pay-time-hour" name="BuyForm[payer_hour_to]" id="pay-time-hour-to">
                            <?
                            $iHourTo = ($oShopOrder->oBuyForm->payer_hour_to) ? $oShopOrder->oBuyForm->payer_hour_to : 20;
                            foreach (BuyForm::hourRange() as $iHour): ?>
                                <option
                                    value="<?= $iHour; ?>" <?= ($iHour == $iHourTo) ? 'selected="selected"' : ''; ?>><?= $iHour; ?></option>
                            <? endforeach; ?>
                        </select>
                    </div>
                </td>
                <td style="padding: 0 5px 0 5px;">:</td>
                <td style="padding: 0 40px 0 0;">
                    <div>
                        <select class="gift-time-minute" name="BuyForm[payer_minute_to]" id="gift-time-minute-to">
                            <? foreach (BuyForm::minuteRange() as $iMinute): ?>
                                <option
                                    value="<?= $iMinute; ?>" <?= ($iMinute == $oShopOrder->oBuyForm->payer_minute_to) ? 'selected="selected"' : ''; ?>><?= $iMinute; ?></option>
                            <? endforeach; ?>
                        </select>
                    </div>
                </td>
            </tr>
        </table>
    </td>
</tr>

<tr>
    <td class="one">
        <label for="comment">Комментарий к заказу:<br>
            <i>(если у Вас есть коментарии к заказу, напишите нам об этом)</i></label>
    </td>
    <td class="two">
        <div class="textarea-order">
            <textarea name='BuyForm[comment]' id="comment"><?= $oShopOrder->oBuyForm->comment; ?></textarea>

            <div class="textarea-order-right"></div>
        </div>
    </td>
</tr>


<tr id="agreement-tr">
    <td class="one">
    </td>
    <td class="two" style="padding: 30px 0 20px 0;">
        <input type="checkbox" name='agreement' id="agreement" checked="checked"/>&nbsp;
        <label for="agreement">Я согласен с <a href="<?= $oShopOrder->agreementUrl; ?>" target="_blank"><strong>Правилами
                    оказания услуг</strong></a></label>
    </td>
</tr>

<tr>
    <td class="one">
        &nbsp;
    </td>
    <td class="two" style="text-align: right; padding-top: 15px;">
        <button type="submit" class="order-button"></button>
        <button type="button" class="order-cansel" onclick="location.href='/'"></button>
    </td>
</tr>

</tbody>
</table>
</div>
</div>
</td>
<td class="text-main-right">&nbsp;</td>
</tr>
</tbody>
</table>
<table class="text-bottom" cellpadding="0" cellspacing="0">
    <tbody>
    <tr>
        <td class="left">&nbsp;</td>
        <td class="center">&nbsp;</td>
        <td class="right">&nbsp;</td>
    </tr>
    </tbody>
</table>
</form>
</div>
</div>