<? $this->layout = 'certificate'; ?>
<div size="A4" class="page2">
    <div class="wrapper" style="height: 24cm;">
        <div class="left">
            <div class="item"><?=$model->item->title?></div>
            <div class="item">
                <? if(!empty($model->code)):?>
                    Номер сертификата <br>
                    <?=$model->code?>
                <?endif;?>
            </div>
            <div class="item">
                <?if(!empty($order->validity_date)):?>
                    Использовать до: <br>
                    <?=date('d.m.Y', strtotime($order->validity_date))?>
                    <br><br>
                <?endif;?>
                <? if(!empty($model->option) || !empty($model->option_name)):?>
                    Вариант: <br>
                    <?=empty($model->option_name) ? $model->option->name : $model->option_name?>
                    <br>
                <? endif;?>

            </div>
        </div>
        <div class="right">
            <? if ($model->item->type == 'group'): ?>
                <b>Выберите одну из услуг:</b>
                <ol>
                    <? foreach ($model->item->childs as $key => $child):?>
                        <li><?=$child->title; ?> <?=Item::getOptionInGroup($child->getAllPrice())?></li>
                    <? endforeach; ?>
                </ol>
            <? else: ?>
                <?=$model->item->desc?>
                <div class="what-know">
                    <? $what_know = strstr($model->item->getText(), 'Что нужно знать?');?>
                    <?=str_replace('<p>&nbsp;</p>', '', $what_know) ?>
                </div>
            <? endif;?>
        </div>
    </div>
    <div class="congratulation">
        <?=empty($model->message) ? '' : $model->message?>
    </div>
</div>