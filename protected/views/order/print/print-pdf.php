<?php
/** @var OrderItems $orderItem */
/** @var Order $order */

$order = $orderItem->orderObj;

?>
<!DOCTYPE html>
<head>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="/pdf/css/pdf.css" />
    <link rel="stylesheet" href="/pdf/css/pdf-preview.css" />
    <title>Предпросмотр сертификата</title>
</head>
<body>
<div class="page page-title">
</div>
<div class="page">
    <div class="page-wrap">
        <div class="logo">
            <div class="note-info">+375 29 687 87 28 | info@elpresent.by | ИП Калиновский В.С.</div>
        </div>
        <h1><?= htmlentities($orderItem->itemObj->title); ?></h1>
        <div class="column-set">
            <div class="aside">
                <div class="col card-example">
                    <h2>Номер сертификата</h2>
                    <?php foreach (str_split($orderItem->code, 4) as $codePart) { ?>
                    <span><?=$codePart; ?></span>
                    <?php } ?>
                </div>
                <div class="aside-item">
                        <h2>Срок действия подарка:</h2>
                       <p><b>по <?=date::human_ru_date($order->validity_date)?></b></p>
                </div>
                <div class="col scenario">
                    <h2>Сценарий:</h2>
                    <ol>
                        <?php foreach ($orderItem->itemObj->getItemScenario() as $line) { ?>
                            <li><?= $line; ?></li>
                        <?php } ?>
                    </ol>
                    <? if(!empty($orderItem->option_id)):?>
                    <div class="aside-item">
                        <h2>Опция:</h2>
                        <p>
                        <?=$order->getNameOption($orderItem->option_id)?>
                        </p>
                        </div>
                    <? endif;?>

                    <? if (!empty($order->message)) {?>
                    <div class="aside-item separate-block">
                        <h2>Поздравление:</h2>
                        <p>
                            <?=$order->message?>
                        </p>
                    </div>

                    <? } ?>
                </div>
            </div>
            <div class="content">
                <div class="col about">
                    <h2>О подарке:</h2>
                    <?=$orderItem->itemObj->instuction_description?>
                    <!--<p>
                        <?
                       /* if(! empty(Yii::app()->request->cookies['option_by_city_value']->value)){
                            $option_by_city_value = json_decode(Yii::app()->request->cookies['option_by_city_value']->value, true);
                            if ((int)$option_by_city_value['id'] == $order->item->id){
                                echo Item::getOption((int)$option_by_city_value['val'])->name;
                            }
                        }*/
                        ?>
                    </p>-->
                </div>
                <div class="col should_know">
                    <?/*<h2>Что нужно знать:</h2>?>
                    <?php/* foreach($orderItem->itemObj->whatShouldYouKnow() as $row) { ?>
                        <p><strong><?= $row['key']; ?></strong>: <?= $row['value']; ?></p>
                    <?php }*/ ?>
                    <?= $orderItem->itemObj->need_to_know ;?>
                </div>
            </div>
        </div>

    </div>
</div>
</body>