<? $this->layout = 'certificate'; ?>
<?php
    /** @var Order $order */
    foreach ($order->orderItems as $num => $orderItem) {
        /** @var OrderItems $order */
        ?>
        <div size="A4" class="page<?=$num+2?>">
            <div class="wrapper" style="height: 24cm;">
                <div class="left">
                    <div class="item"><?=$orderItem->itemObj->type == 'group' ? 'Комплект впечатлений': 'Впечатлениe'?>:</div>
                    <div class="item"><?=$orderItem->itemObj->title?></div>
                    <div class="item">
                        Номер сертификата <br>
                        <? if(!empty($orderItem->code)){
                            echo $orderItem->code;
                        } else {
                            echo "Код не сгененирован";
                        }?>
                    </div>
                    <div class="item">
                        <?if(!empty($order->validity_date)):?>
                            Использовать до: <br>
                            <?=date('d.m.Y', strtotime($order->validity_date))?>
                            <br><br>
                        <?endif;?>
                        <? if(!empty($orderItem->optionObj)):?>
                            Вариант: <br>
                            <?= $orderItem->optionObj->name?>
                            <br>
                        <? endif;?>

                    </div>
                </div>
                <div class="right">
                    <? if ($orderItem->itemObj->type == 'group'): ?>
                        <b>Выберите одну из услуг:</b>
                        <ol>
                            <? foreach ($orderItem->itemObj->childs as $key => $child){?>
                                <?
                                /** @var Item $child */
                                $selfLink = $orderItem->itemObj->getSelfLinkByChildren($child);
                                $optionTitle = '';
                                if (!empty($selfLink->option)) {
                                    $optionTitle = '('.$selfLink->option->small_name.')';
                                }
                                ?>
                                <li><?=$child->title; ?> <?=$optionTitle?></li>
                            <? } ?>
                        </ol>
                    <? else: ?>
                        <?=$orderItem->itemObj->instuction_description?>
                        <div class="what-know">
                            <br/>
                            <?= $orderItem->itemObj->need_to_know ;?>
                        </div>
                    <? endif;?>
                </div>
            </div>
            <div class="congratulation">
                <?=$order->message?>
            </div>
        </div>

        <?
    }
?>
