<?php
/** @var OrderItems $orderItem */
/** @var Order $order */
$order = $orderItem->orderObj;
?>
<!DOCTYPE html>
<head>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="/pdf/css/pdf.css" />
    <link rel="stylesheet" href="/pdf/css/pdf-preview.css" />
    <title>Предпросмотр сертификата</title>
</head>
<body>
<div class="page page-title">
</div>
<div class="page">
    <div class="page-wrap">
        <div class="logo"></div>
        <h1><?= htmlentities($orderItem->itemObj->title); ?></h1>
        <div class="column-set">
            <div class="aside">
                <div class="col card-example">
                    <h2>Номер сертификата</h2>
                    <?php foreach (str_split($orderItem->code, 4) as $codePart) { ?>
                    <span><?=$codePart; ?></span>
                    <?php } ?>
                </div>
            </div>

            <div class="content">
                <div class="col">
                    <div class="col should_know">
                        <?= $orderItem->itemObj->need_to_know ;?>
                        <p><strong>Срок действия подарка</strong>:
                    <br>
                        по <?=date::human_ru_date($order->validity_date)?>
                        </p>

                    </div>
                </div>
                <div class="col about">
                    <h2>Выберите одну из услуг:</h2>
                    <ol>
                        <? foreach ($orderItem->itemObj->childs as $key => $child){?>
                            <?
                            /** @var Item $child */
                            $selfLink = $orderItem->itemObj->getSelfLinkByChildren($child);
                            $optionTitle = '';
                            if (!empty($selfLink->option)) {
                                $optionTitle = '('.$selfLink->option->small_name.')';
                            }
                            ?>
                            <li><?=$child->title; ?> <?=$optionTitle?></li>
                        <? } ?>
                    </ol>
                </div>
            </div>
        </div>
        <? if (!empty($order->message)) {?>
        <h2>Поздравление:</h2>
        <p>
            <?=$order->message?>
        </p>
        <? } ?>
    </div>
</div>