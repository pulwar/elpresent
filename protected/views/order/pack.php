<div class="clearfix">
    <div class="summ-order float-left">
        <br>
        <h1>Прикрепить поздравление к сертификату</h1>

        <? if ($item->off_price != 0): ?>
            <div class="off-price-lable">* с учётом скидки</div>
        <? endif;?>

        <div class="summ-order-buttons">
            <form method="POST" action="">
                <textarea name="message" class="form-control"></textarea>
                <input class="texture-selector" value="" type="hidden" name='texture'>
                <input class="bow-selector" value="" type="hidden" name='bow'>
                <input class="card-selector" id="card-value" value="" type="hidden" name='card'>
                <input class="flower-selector" id="flower-value" value="" type="hidden" name='flower'>
                <input class="toy-selector" id="toy-value" value="" type="hidden" name='toy'>
                <input type="hidden" name="item_id" value="<?= $item->id; ?>"/>
                <button type="submit" class="btn btn-primary btn-sm btn-mid">Заказать</button>
                <!--<button type="reset" class="btn btn-cancel btn-sm btn-mid">Отменить</button>-->
            </form>
        </div>
        <h3>Итого к оплате:</h3>
        <div id="total-price"><?= ($item->getRoundedPrice()); ?></div>
        <span class="summ-total"> руб.</span>
    </div>
</div>