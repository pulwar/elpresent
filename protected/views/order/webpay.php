<?php
/** @var Order $order */
/** @var Webpay $invoice */
?>
<article>
	<section>
	
		<table class="text-head-green" cellpadding="0" cellspacing="0">
			<tbody><tr>
				<td class="left">&nbsp;</td>
				<td class="center"><h1>Оформление заказа</h1></td>
				<td class="right">&nbsp;</td>
			</tr>
		</tbody></table>
		<table width="100%" cellpadding="0" cellspacing="0">
			<tbody><tr>
				<td class="text-main-left">&nbsp;</td>
				<td class="text-main">
					<div class="order-form">
						<h2>Ваш заказ</h2>
						<div class="yes">
							Спасибо! Ваш заказ успешно оформлен!
						</div>
                                               
						<table class="form">
						<tbody>
    							<tr>
    								<td>
    									<?= $order->item->title; ?>
    								</td>
    								<td>
    									<?= $order->item_quantity; ?>
    								</td>
    								<td>
    									<strong><?= ($order->item->getRoundedPrice($order->city)); ?> руб.</strong>
    								</td>
    							</tr>
    							<? if ( $order->flower ) : ?>
    							<tr>
    								<td>
    									Цветы "<?= $order->flower->flower_title; ?>"
    								</td>
    								<td>
    									1
    								</td>
    								<td>
										<strong><?= ($order->flower->flower_price); ?> руб.</strong>
    								</td>
    							</tr>
    							<? endif; ?>
                           <? if ( $order->is_express_delivery_recipient || $order->is_express_delivery ) : ?>
								<tr>
									<td>Экспресс доставка</td>
									<td></td>
									<td><strong><?= (55000); ?> руб.</strong></td>
								</tr>
							<? endif; ?>
                           <? if ( $order->recipient_firstname ) : ?>
								<tr>
									<td>Вручить лично получателю подарка</td>
									<td></td>
									<td><strong><?= (40000); ?> руб.</strong></td>
								</tr>
							<? endif; ?>

    						</tbody>
						</table>
                        <div style="float: left; margin: 18px 0px 0px 10px;">
                        	<? if ( $order->payment_type_id == PaymentType::WEBPAY ) : ?>
                            После оплаты заказа в системе WebPay с вами свяжется наш сотрудник для уточнения деталей доставки выбранным вами способом.<br />
							<? endif; ?>
                       		-->
                        </div>
						<div class="gray-bg total-price">

						<div class="gray-bg total-price" style="text-align: right;">Итого:
							<strong><?= ($order->price); ?> руб.</strong>

						</div>
                                                                                           </div>
						</div>
						<div class="clear"><!-- --></div>
                                                <div style="text-align:right;padding-right: 210px;">
</div>
						<div style="height: 15px;"><!-- --></div>
                                                <div style="text-align:right;padding-right: 30px;">
<form action="<?= Webpay::$url; ?>" method="post" id="webpay">
<input type="hidden" name="*scart">
<input type="hidden" name="wsb_language_id" value="russian">
<input type="hidden" name="wsb_storeid" value="<?= Webpay::$wsb_storeid; ?>" >
<input type="hidden" name="wsb_store" value="<?= Webpay::$wsb_store; ?>" >
<input type="hidden" name="wsb_order_num" value="<?= $invoice->wsb_order_num; ?>" >
<input type="hidden" name="wsb_test" value="<?= Webpay::$wsb_test; ?>" >
<input type="hidden" name="wsb_currency_id" value="<?= Webpay::$wsb_currency_id; ?>" >
<input type="hidden" name="wsb_seed" value="<?= $invoice->wsb_seed; ?>">
<input type="hidden" name="wsb_return_url" value="<?= path('order/wsbnotify', [], true); ?>">
<input type="hidden" name="wsb_cancel_return_url" value="<?= path('order/wsbnotify', [], true); ?>">
<input type="hidden" name="wsb_notify_url" value="<?= path('order/wsbnotify', [], true); ?>">
<input type="hidden" name="wsb_email" value="<?= htmlspecialchars($order->email); ?>" >
<input type="hidden" name="wsb_phone" value="<?= htmlspecialchars($order->phone); ?>" >
<input type="hidden" name="wsb_invoice_item_name[]" value="<?= htmlspecialchars($order->item->title); ?>">
<input type="hidden" name="wsb_invoice_item_quantity[]" value="1">
<input type="hidden" name="wsb_invoice_item_price[]" value="<?= $order->price; ?>">
<input type="hidden" name="wsb_total" value="<?= $invoice->wsb_total; ?>" >
<input type="hidden" name="wsb_signature" value="<?= $invoice->wsb_signature; ?>" >

</form>
                                                    <span class="webpay">
<img src="/images/webpay-pay.png" alt="Оплатить" style="cursor: pointer;"/>
                                                    </span>
<script>
    $('span.webpay').bind('click', function(){
        $('form#webpay').submit();
    })


</script>
                                                </div>
				</td>
				<td class="text-main-right">&nbsp;</td>
			</tr>
		</tbody></table>
		<table class="text-bottom" cellpadding="0" cellspacing="0">
			<tbody><tr>
				<td class="left">&nbsp;</td>
				<td class="center">&nbsp;</td>
				<td class="right">&nbsp;</td>
			</tr>
		</tbody></table>
	         

	</section>
</article>
