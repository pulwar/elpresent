<?php
/**
 * Подарки для которых нужно применить 6 месяцев
 * @type {string[]}
 */
$items_for_6_month = [
    'Двухэтапный мастер-класс «Город»',
    'Курс «Штурман: боевое крещение»',
    'Курс «Штурман: раллийный драйв»',
    'Курс начинающих водителей',
    'Урок вождения: маневрирование',
    'Мастер-класс вождения на выбор',
    'Парковочный курс',
    'Правильная парковка',
    'Программа контраварийного вождения',
    'Раллийный день',
    'Раллийный день  для двоих',
    'Раллийный день для компании',
    'Урок контраварийного вождения',
    'Урок контраварийного вождения для двоих',
    'Универсальная подарочная карта'
];
?>
<? $this->layout = 'certificate'; ?>
<div size="A4" class="page2">
    <div class="wrapper">
        <div class="left">
            <div class="item">Комплект впечатлений</div>
            <div class="item">
                <? if(!empty($model->code)):?>
                    Номер сертификата <br>
                    <?=$model->code?>
                <?endif;?>
            </div>
            <div class="item">
                Использовать до: <br>
                <? if(!empty($model->date) || !empty($model->order_date)):?>
                    <? $date = empty($model->date) ? strtotime($model->order_date) : $model->date ?>
                    <? if(in_array($model->item->title, $items_for_6_month)):?>
                        <? $t = strtotime('+6 month', $date)?>
                    <? else:?>
                        <? $t = strtotime('+3 month', $date)?>
                    <? endif;?>
                    <?=date('d.m.Y', $t)?>
                    <br><br>
                <? endif;?>
                <? if(!empty($model->option) || !empty($model->option_name)):?>
                    Вариант: <br>
                    <?=empty($model->option_name) ? $model->option->name : $model->option_name?>
                    <br>
                <? endif;?>

            </div>
        </div>
        <div class="right">
            <b>Выберите одну из услуг:</b>
            <ol>
                <? foreach($items as $key => $item) {?>
                    <li><?=$item->title; ?> <?/*=Item::getOptionInGroup($items->getAllPrice())*/?></li>
                <? } ?>
            </ol>
        </div>
    </div>
    <div class="congratulation">
        <?=empty($model->message) ? '' : $model->message?>
    </div>
</div>