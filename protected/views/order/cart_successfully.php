<article>
    <section>
        <form action='' method='post' id='buyForm'>
            <table class="text-head-green" cellpadding="0" cellspacing="0">
                <tbody><tr>
                        <td class="left">&nbsp;</td>
                        <td class="center"><h1>Оформление заказа</h1></td>
                        <td class="right">&nbsp;</td>
                    </tr>
                </tbody></table>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tbody><tr>
                        <td class="text-main-left">&nbsp;</td>
                        <td class="text-main">
                            <div class="order-form">
                                <h2>Ваш заказ</h2>
                                <div class="yes">
							Ваш заказ успешно добавлен в корзину.
                                </div>
                                <table class="form">
                                    <tbody>
                                        <tr>
                                            <td class="one"><div>Подарок: <?= $oItem->title
?></div></td>
                                            <td class="two" style="text-align: right;">
								1
                                            </td>
                                            <td class="three"><?= ($oItem->getRoundedPrice());?> руб</td>
                                            <td class="four"></td>
                                        </tr>
<? if ($oFlower) : ?>
                                        <tr>
                                            <td class="one"><div>Цветы: <?= $oFlower->flower_title ?></div></td>
                                            <td class="two" style="text-align: right;">
								1
                                            </td>
                                            <td class="three"><?= ($oFlower->flower_price); ?> руб</td>
                                            <td class="four"></td>
                                        </tr>
                                        <? endif; ?>

<? if ($oCard) : ?>

                                                    <tr>
                                                        <td class="one"><div>Открытка: <?= $oCard->card_title ?></div></td>
                                                <td class="two" style="text-align: right;">
    								1
                                                </td>
                                                <td class="three"><?= ($oCard->card_price); ?> руб</td>
                                            <td class="four"></td>
                                        </tr>
<? endif; ?>
                                            </tbody></table>
                                        <div class="gray-bg total-price">Итого: <b><?= ($cartPrice); ?> руб</b></div>
                                <div class="clear"><!-- --></div><br />
                                <input type="button" class="order-submit" name="order-submit" value="" style="float: right" onclick="window.location = '/order/buy'">
                                <div style="height: 15px;"><!-- --></div>
                            </div>
                        </td>
                        <td class="text-main-right">&nbsp;</td>
                    </tr>
                </tbody></table>
            <table class="text-bottom" cellpadding="0" cellspacing="0">
                <tbody><tr>
                        <td class="left">&nbsp;</td>
                        <td class="center">&nbsp;</td>
                        <td class="right">&nbsp;</td>
                    </tr>
                </tbody></table>
        </form>
    </section>
</article>

