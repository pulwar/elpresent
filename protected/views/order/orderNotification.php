<?php /** @var Order $order */ ?>
<h1>Номер заказа: <?=$order->order_number; ?></h1>


    <table border="1">
        <tbody>
            <tr>
                <td>Имя:</td>
                <td><?=$order->name?></td>
        </tr>
        <tr>
            <td>Фамилия:</td>
            <td><?=$order->lname?></td>
        </tr>
        <tr>
            <td>E-mail: </td>
            <td><?=$order->email?></td>
        </tr>
        <tr>
            <td>Контактный телефон:</td>
            <td><?=$order->phone?></td>
        </tr>
        <tr>
            <td>Комментарий:</td>
            <td><?=$order->comment?></td>
        </tr>

        <? if(!strlen(trim($order->recipient_address))): ?>
        <tr>
            <td>Адрес доставки:</td>
            <td><?=$order->adr?></td>
        </tr>
        <? else:?>
        <tr>
            <td>Адрес оплаты заказа: </td>
            <td><?=$order->payer_adr?></td>
        </tr>
        <? endif; ?>

        <?if ((int) strtotime($order->gift_date) > 0):?>
        <tr>
            <td>Желаемая дата доставки:</td>
            <td><?=$order->gift_date?></td>
        </tr>
        <tr>
            <td>Желаемое время доставки:</td>
            <td><?=$order->gift_time_from.' - '.$order->gift_time_to?></td>
        </tr>
        <?endif;?>

        <?if ((int) strtotime($order->payer_date) > 0):?>
        <tr>
            <td>Желаемая дата оплаты:</td>
            <td><?=$order->payer_date?></td>
        </tr>
        <tr>
            <td>Желаемое время оплаты:</td>
            <td><?=$order->payer_time_from.' - '.$order->payer_time_to?></td>
        </tr>
        <?endif;?>


        <? if(strlen(trim($order->recipient_firstname)) && strlen(trim($order->recipient_address))): ?>
        <tr>
            <td>Имя получателя подарка:</td>
            <td><?=$order->recipient_firstname?></td>
        </tr>
        <tr>
            <td>Фамилия получателя подарка: </td>
            <td><?=$order->recipient_lastname?></td>
        </tr>
        <tr>
            <td>Адрес вручения подарка: </td>
            <td><?=$order->recipient_address?></td>
        </tr>
        <tr>
            <td>Дата получения подарка: </td>
            <td><?=$order->recipient_desired_date?></td>
        </tr>
        <tr>
            <td>Время получения подарка: </td>
            <td><?=$order->recipient_desired_time_from.' - '.$order->recipient_desired_time_to?></td>
        </tr>
        <? endif; ?>
        <tr>
            <td>Подарок: <?=$order->item->title?>
                <br />
                <img src="http://<?=$serverName?>/images/item/<?=$order->item->preview_img?>.jpg" />
            </td>
            <td>Количество:<?=$order->item_quantity?></td>
        </tr>
        <tr>
            <td>
    	    	Текстура: <?=$order->texture?>
            </td>
            <td>
                <?=($order->texture ? '<br /><img src="http://' . $serverName . '/images/' . $order->texture . '" width="130" />' : 'Нет')?>
            </td>
        </tr>
        <tr>
            <td>Лента: <?=($order->bow ? $order->bow : 'нет')?>
            </td>
            <td>
    	    	<?=(($order->bow) ? '<br /><img src="http://' . $serverName . '/images/' . $order->bow . '" width="130" />' : 'Нет')?>
            </td>
        </tr>
        <? if ($order->flower) :?>
        <tr>
            <td>Цветы: <?=($order->flower ? $order->flower->flower_title : 'нет')?>
    	    	<?=($order->flower ? '<img src="http://' . $serverName . '/upload/flowers/' . $order->flower->flower_image . '_48.png" />' : '')?>
            </td>
            <td>Количество:<?=($order->flower) ? $order->flower_quantity : '0'?></td>
        </tr>
        <? endif; ?>
        <? if ($order->card) :?>
        <tr>
            <td>Открытка: <?=($order->card ? $order->card->card_title : 'нет')?>
    	    	<?=($order->card ? '<br /><img src="http://' . $serverName . '/upload/cards/' . $order->card->card_image . '_48.png" />' : '')?>
            </td>
            <td>Количество:<?=($order->card) ? $order->card_quantity : '0'?></td>
        </tr>
        <? endif; ?>
        <tr>
            <td>Способ доставки: </td>
            <td><?=BuyForm::$delivery[$order->delivery]['title']; ?></td>
        </tr>
        <tr>
            <td>Цена: </td>
            <td><?=$order->price?></td>
        </tr>
        <tr>
            <td>Скидка: </td>
            <td><?=$order->discount?>%</td>
        </tr>
    </tbody>
</table>

<table>
    <tr>
        <td>Способ оплаты</td>
        <td><?=$order->payment_type->payment_type_title?></td>
    </tr>
<? if ($order->is_express_delivery || $order->is_express_delivery_recipient) { ?>
    <tr>
        <td>Экспресс доставка</td>
        <td><strong><?=(55000)?> руб.</strong></td>
    </tr>
<? } if ($order->is_express_delivery_recipient) { ?>
    <tr>
        <td>Вручить лично получателю подарка</td>
        <td><strong><?=(55000)?> руб.</strong></td>
    </tr>
<? } if ($order->is_thematic_delivery) { ?>
    <tr>
        <td>Тематическая доставка</td>
        <td><strong><?=(BuyForm::getThematicDeliveryPrice($order->thematic_delivery_name))?> руб.</strong></td>
    </tr>
<? } ?>


    <tr>
        <td>Итоговая цена</td>
        <td><strong><?=($order->price)?> руб.</strong></td>
    </tr>
</table>