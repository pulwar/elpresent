<?php
/** @var CActiveForm $form */
$form = $this->beginWidget(
    'CActiveForm',
    [
        'id' => 'buy-form',
        'enableAjaxValidation' => false,
        'clientOptions' => [
        'validateOnSubmit' => false
        ]
    ]
); ?>

<? if ($form->errorSummary($order)) : ?>
    <div class="col-md-12">
        <div class="validate-area clearfix">
            <?= $form->errorSummary($order); ?>
        </div>
    </div>
<? endif; ?>

<div class="col-md-12">
    <h1 class="main-title">Оформление заказа</h1>
</div>

<?
/** @var Item[] $cart_items */
$item = array_shift($cart_items);
?>
<div class="checkout-content clearfix">
    <div class="col-md-3">
        <div class="checkout-img">
            <img src="/<?= $item->full_img; ?>" alt="<?= htmlentities($item->title); ?>"/>
        </div>
    </div>
    <div class="col-md-9">
        <div class="checkout-info">
            <h3><?= $item->title; ?></h3>

            <p><?= $item->desc; ?></p>

            <?= $form->hiddenField($order, 'item_id', ['data-price' => $item->getRoundedPrice(Cart::getCart()->getOptionByItemId($item->id))]); ?>

            <h4><?= ($item->getRoundedPrice(Cart::getCart()->getOptionByItemId($item->id))); ?> руб.</h4>
        </div>
    </div>

</div>

<div class="col-md-12">
    <div class="checkout-line"></div>
</div>

<? if (!$item->isAvailable()) : ?>
    <p class="d-no"> В настоящий момент этого впечатления нет в наличии, выберите другое впечатление.</p>
<? else: ?>

    <div class="col-md-4">
        <div class="delivery delivery-left-border">
            <div class="checkout-title">
                <h3>Способ оплаты:</h3>
            </div>

            <ul>
                <?= $form->radioButtonList(
                    $order,
                    'paymentType',
                    [
//                        4 => 'EasyPay',
//                        5 => 'Пластиковая карта (WebPay)',
                        6 => 'Картой Visa, MasterCard, Maestro онлайн',
                        7 => 'Картой Халва (2 месяца рассрочки)',
                    ],
                    [
                        'separator' => '',
                        'template' => '<li>{input} {label}</li>'
                    ]
                ); ?>
            </ul>
        </div>
    </div>

    <div class="col-md-8">
        <div class="delivery-info clearfix">
            <div class="checkout-title">
                <h3>Информация о заказчике:</h3>
            </div>

            <?
            $add = ["placeholder" => "Ваше Имя"];
            if(empty($_POST) && !empty($user->user_firstname)){
                $add['value'] = $user->user_firstname;
            }
            echo $form->textField($order, 'name', $add);
            ?>

            <?
            $add = ["placeholder" => "Ваша Фамилия"];
            if(empty($_POST) && !empty($user->user_lastname)){
                $add['value'] = $user->user_lastname;
            }
            echo $form->textField($order, 'lname', $add);
            ?>

            <?
            if(empty($_POST) && !empty($user->user_phone)){
                echo $form->textField($order, 'phone', ['value'  => $user->user_phone]);
            }else{
                $this->widget('CMaskedTextField', array(
                    'model' => $order,
                    'attribute' => 'phone',
                    'mask' => '+375-99-99-99-999',
                    'placeholder' => '*',
                    'htmlOptions' => array(
                        'size' => '40',
                        'class' => 'form-control',
                        'placeholder' => 'Ваш контактый телефон*'
                    ),
                    //'completed' => 'function(){console.log("ok");}',
                ));
            }
            ?>

            <?
            $add = ['placeholder' => 'Email*'];
            if(empty($_POST) && !empty($user->user_email)){
                $add['value'] = $user->user_email;
            }
            echo $form->emailField($order, 'email', $add);
            ?>

            <?= $form->textField($order, 'comment', ["placeholder" => "Комментарий к заказу"]); ?>
            <?= $form->textArea($order, 'message', ["placeholder" => "Ваше поздравление", 'rows'=> 2, 'cols'=> 53]); ?>
            <p>Поля, отмеченные звездочкой (*) обязательны к заполнению.</p>
             <p>Нажимая кнопку "Заказать" вы соглашаетесь на обработку персональных данных в соответствии с <a href="/files/politicy-elpresent.pdf" target="_blank">политкой конфиденциальности</a>.</p>
        </div>
    </div>

    <div class="col-md-12">
        <div class="checkout-line"></div>
    </div>

    <div class="col-md-12 clearfix">
        <div class="summ-order float-left">
            <h3>Итого к оплате!:</h3>

            <span class="price-container" data-price-box="total"><?= ($item->getRoundedPrice(Cart::getCart()->getOptionByItemId($item->id))); ?> руб.</span><br>

            <div class="summ-order-buttons">
                <button type="submit" class="btn btn-primary btn-sm btn-mid"
                        onClick="yaCounter17198173.reachGoal('oformlenie_zakaza'); ga('send', 'pageview', '/virtual/oformlenie_zakaza');">Заказать</button>
            </div>
        </div>
    </div>



<? endif; ?>

<?php $this->endWidget(); ?>
