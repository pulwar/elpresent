<script>
	var timer = setInterval(function() {
		$.ajax({
			type: 'POST',
			cache: false,
			url: '/order/ispdfexist',
			success: function (data) {
				if(data == '1'){
					clearInterval(timer);
					$('.jumbotron div.wait').hide();
					$('.jumbotron div.success').show();
				}
			}
		});
	}, 5000);
</script>
<div class="alert alert-success"><?= $webpay_complete_message ?></div>

<div class="row">
	<div class="col-md-12">
		<p></p>
		<? if(!empty($wsb_order_num)):?>
			<p>№ заказа в системе webpay: <b><?=$wsb_order_num?></b></p>
		<? endif;?>
		<? if(!empty($wsb_tid)):?>
			<p>№ транзакции: <b><?=$wsb_tid?></b></p>
		<? endif;?>
		<? if(!empty($order_id)):?>
			<p>№ заказа на сайте: <b><?=$order_id?></b></p>
		<? endif;?>

		<? if(!empty($type) && $type == 1):?>
			<div class="jumbotron">
					<div class="success" style="display: none">
						<h1>Оплата произведена! Для вас сформирован PDF-сертификат.</h1>
						<p> Вы можете скачать сертификат здесь, также копия PDF - сертификата отправлена на ваш емейл.</p>
						<p> Если у вас возникли какие-то вопросы, вы можете связаться с нами: <br><?= arr::arrInStr(Yii::app()->params['phones']) ?></p>
						<p><a href="/order/downloadcertificate" target="_blank" class="btn btn-primary btn-midd">Скачать сертификат</a></p>
					</div>
					<div class="wait">
						<h1>Идет формирование сертификата!</h1>
						<p><span class="label label-warning">Внимание!</span> Не уходите, пока не будет сформирован сертификат.</p>
						<img scr="/images/loading.gif" />
					</div>

				</div>
		<? endif;?>
	</div>

</div>


