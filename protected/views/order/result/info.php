<?php
/** @var array $cart_items*/
/** @var Order $order*/
?>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <? if (count($order->orderItems) == 1){ ?>
                <?php
                /** @var OrderItems[] $item */
                $item = $order->orderItems;
                /** @var OrderItems $item */
                $item = array_shift($item);
                /** @var Item $item */
                $item = $item->itemObj;
                ?>
                <div class="col-md-2"><img src="/<?= $item->preview_img; ?>" alt="<?= htmlentities($item->title); ?>"/></div>
                <div class="col-md-10">
                    <h2><?=$item->title?></h2>
                    <p><?=$item->desc?></p>
                </div>
            <? } else {  ?>
                <h2>Куплен комплект из</h2>
                <ul>
                    <? foreach ($order->orderItems as $k => $orderItem): ?>
                        <li>
                            <div class="name-item"><?= $orderItem->itemObj->title ?></div>
                            <img src="/<?= $orderItem->itemObj->preview_img ?>"/>
                        </li>
                    <? endforeach; ?>
                </ul>
            <? } ?>
        </div>
    </div>
    <div class="col-md-12">

        <h3>Цена:</h3>
        <ul class="gift-delivery">
            <li>
                Цена заказа: <span><?=$order->calcSumOrderWithDiscount();?>  руб.</span>
            </li>
            <?php
            if ($order->type != Order::TYPE_DIGITAL) {
                switch (true) {
                    case (bool)$order->is_thematic_delivery:
                        echo "<li>Тематическая доставка: <span>{$order->thematic_delivery_price}руб.</span></li>";
                        break;
                    case (bool)$order->is_express_delivery:
                        echo "<li>Экспресс доставка: ";
                        if ($order->price - $order->delivery_price < Order::PRICE_FOR_FREE_EXPRESS_DELIVERY) {
                            echo "<span>" . $order->delivery_price . " руб.</span>";
                        } else {
                            echo "<span>бесплатно</span> (Цена выше " . $order->delivery_free_price . " руб.)";
                        }
                        break;
                    case (bool)$order->is_delivery_general:
                        echo "<li> Обычная доставка: ";
                        if ($order->price - $order->delivery_price < Order::PRICE_FOR_FREE_DELIVERY) {
                            echo "<span>" . $order->delivery_price . " руб.</span>";
                        } else {
                            echo "<span>бесплатно</span> (Цена выше " . $order->delivery_free_price . " руб.)";
                        }
                        echo "</li>";
                        break;
                    default:
                        echo "<li><span>Самовывоз: " . Order::DELIVERY_FREE_PRICE . " руб.</span></li>";
                }
            }
            ?>
            <? if (!empty($order->user_cash)): ?>
                <li>Баллов вычтенных из суммы: <span> - <?= ($order->user_cash) ?> руб.</span></li>
            <? endif; ?>
        </ul>

        <? if ($order->flower_id || $order->card_id || $order->toy_id): ?>
            <h3>К подарку так же прилагается:</h3>
            <ul class="gift-additional">
                <? if ($order->flower_id): ?>
                    <li>
                        Цветы "<?= $order->flower->flower_title; ?>"
                        <span class="help">
                            <span><?= ($order->flower->flower_price) ?> руб.</span>
                        </span>
                    </li>
                <? endif; ?>

                <? if ($order->card_id): ?>
                    <li>
                        Открытка "<?= $order->card->card_title; ?>"
                        <span class="help">
                            <span><?= ($order->card->card_price) ?> руб.</span>
                        </span>
                    </li>
                <? endif; ?>

                <? if ($order->toy_id): ?>
                    <li>
                        Букет из игрушек "<?= $order->toy_title; ?>"
                        <span class="help">
                            <span><?= ($order->toy_price) ?> руб.</span>
                        </span>
                    </li>
                <? endif; ?>
            </ul>
        <? endif; ?>


    </div>
    <div class="col-md-12">
        <h3>Итого к оплате:</h3>
        <span class="price-container" data-price-box="total"><?=$order->price?> руб.</span>
    </div>
</div>