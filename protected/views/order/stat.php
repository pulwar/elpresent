<?php if($partner_name):?>
<p>Здравствуйте <?=$partner_name?></p>
<?php endif;?>

<? if($err):?>
    <?=$err?>
<? endif;?>



<form action="<?=path('order/stat')?>" method="post">
    <div class="form-group">
        <div class="col-sm-6">
        <div class="row">
            <label class="col-xs-1 control-label">С:</label>
            <div class="col-xs-11">
                <input class="form-control datepicker " name="date_start" type="input" placeholder="2017-10-01" value="<?=$date_start?>">
            </div>
        </div>
            </div>
        <div class="col-sm-6">
        <div class="row">
            <label class="col-xs-1 control-label">По:</label>
            <div class="col-xs-11">
                <input class="form-control datepicker " name="date_end" type="input" placeholder="2017-10-01" value="<?=$date_end?>">
            </div>
        </div>
        </div>
    </div>
    
    
    <label>Пароль</label><br>
    <input name="pass" type="password"><br><br>
    <input class="btn btn-info" name="submit" value="Получить" type="submit">
    
</form>

<?php if($orderItems):?>
<h3>Список кодов активированных Вами за выбранный период</h3>
<ul>
    <?php foreach ($orderItems as $value):?>
    <li>
        <p><?=$value->code?> : <?=$value->recivier_activation_date?> </p>
    </li>
    <?php endforeach;?>
</ul>
<?php endif; ?>
