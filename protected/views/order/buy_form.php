<?php
/** @var Order $order */
/** @var Item[] $thematicDelivery */
/** todo: move this somewere... or just replace with better solution */
function deliveryTypeList($model, $attribute, $data, $myhtmlOptions = [])
{
    $html = '';
    foreach ($data as $val => $label) {
        
		
		$htmlOptions = [];
		
        // prepare htmlOptions for this field...
        foreach ($myhtmlOptions as $name => $option) {
            if (is_callable($option)) {
                $option = $option($val);
            }
            $htmlOptions[$name] = $option;
        }
        $htmlOptions['value'] = $val;
        CHtml::resolveValue($model, $attribute);
        CHtml::resolveNameID($model, $attribute, $htmlOptions);
        $htmlOptions['id'] = $htmlOptions['id'] . '_' . $val;
        $label = CHtml::activeLabel($model, $attribute, ['label' => $label, 'for' => $htmlOptions['id']]);
        if (!isset($htmlOptions['checked']) && CHtml::resolveValue($model, $attribute) == $htmlOptions['value']) {
            $htmlOptions['checked'] = 'checked';
        }
        $htmlOptions['type'] = 'radio';
        $input = CHtml::tag('input', $htmlOptions);

        $html .= strtr(
            '<li>{input} {label}</li>',
            [
                '{label}' => $label,
                '{input}' => $input
            ]
        );
    }

    return $html;
}

?>
    <script>
        function formatPrice(price) {
            if (price == 0) {
                return 'Бесплатно';
            }


            return price  + " руб.  ";

//            return ('' + price.toFixed(0)).replace(/./g, function (c, i, a) {
//                return i && c !== "." && !((a.length - i) % 3) ? ',' + c : c;
//            }) + ' руб.';
        }

        function recountPrice(totalPriceBox)
        {

            var total = 0;
            // накопленная скидка
            //var cash_sum = $('input.cash-sum:enabled').val();
            var cash_sum = $('input.cash-check:checked').attr('data-user-cash');

            if (typeof cash_sum == "undefined") {
                cash_sum = 0;
            }

            $('input[type=hidden][data-price], input[type=radio][data-price]:checked').each(function (i, el) {
                total += Number($(el).data('price'));
            });

            if(cash_sum >= total) {
                total = 0;
            } else {
                total = total - cash_sum;
            }

            total = total.toFixed(2);

            totalPriceBox.html(formatPrice(Number(total)));
        }

        /**
         * Отображение сколько юзер использвал денег из баланса
         */
        function showCashUsed(){
            var cash_sum = $('input.cash-sum:enabled').val();
            if(cash_sum > <?=empty($user->cash)?0:$user->cash?>) {
                $('input.cash-sum:enabled').val(<?=$user->cash?>);
            }
//            if(cash_sum % 0.1 != 0){
//                cash_sum = <?//=empty($user->cash) ? 0 : $user->cash?>//;
//                $('input.cash-sum:enabled').val(cash_sum);
//            }
//            $('.cash-used span').html(formatPrice(Number(cash_sum)));
        }

        $(function () {

            $('.gift-date.datepicker').datepicker({
                format: 'yyyy-mm-dd',
                startDate: 'new'
            });

            var deliveryPriceBox = $('[data-price-box=delivery]'),
                totalPriceBox = $('[data-price-box=total]'),
                recipientForm = $('[data-recipient-form]'),
                onlyWhenNoRecipientForm = $('[data-no-recipient-delivery]'),
                showRecipientForm = $('[data-show-recipient-form]').on('change', function () {
                    $('[data-delivery-type]:checked').trigger('change');
                })[0];

            var cash = $('.cash');
            cash.on('click', 'input.cash-check', function(){
                var cash_sum = $('input.cash-sum');
                if (cash_sum.prop('disabled') == true){
                    //cash_sum.show();
                    cash_sum.prop('disabled', false);
                    recountPrice(totalPriceBox);
                    showCashUsed();
                }else{
                    //cash_sum.hide();
                    cash_sum.prop('disabled', true);
                    recountPrice(totalPriceBox);
                }
            });

            cash.on('change', 'input.cash-sum', function(){
                showCashUsed();
                recountPrice(totalPriceBox);
            }).on('keyup', 'input.cash-sum', function(){
                showCashUsed();
                recountPrice(totalPriceBox);
            });

            var deliveryDepend = {};
            var deliveryDependAll = $('[data-depends-on-delivery]').each(function (i, el) {
                var indexes = ('' + $(el).data('depends-on-delivery')).split(',').forEach(function (idx) {
                    idx = idx.replace(/\s/, '');
                    if (!deliveryDepend.hasOwnProperty(idx)) {
                        deliveryDepend[idx] = $([]);
                    }

                    deliveryDepend[idx].push(el);
                });
            });

            $('[data-delivery-type]').on('change', function () {
                var index = Number(this.value);
                deliveryDependAll.addClass('hide');

                $(deliveryDepend[index]).removeClass('hide');
                deliveryPriceBox.html(formatPrice($(this).data('price')));
                // handle recipient form...
                recipientForm[((this.value > 0 && showRecipientForm.checked) || this.value > 3) ? 'removeClass' : 'addClass']('hide');
                onlyWhenNoRecipientForm[(this.value == 0 || (this.value > 0 && showRecipientForm.checked) || this.value == 3) ? 'addClass' : 'removeClass']('hide');
                recountPrice(totalPriceBox);
            }).filter(':checked').trigger('change');

            // var $thematicOptions = $('[data-thematic-value]');
            // $('#Order_thematic_delivery_name').on('change', function () {
                // $thematicOptions.addClass('hide').filter('[data-thematic-value=' + $(this).val() + ']').removeClass('hide');
                // var id = $(this).val();
                // var price = $(this).find('option:selected').attr('data-price');

                // $('input[type=radio][data-price]:last').val(id);
                // $('input[type=radio][data-price]:last').attr('data-price', price);

                // При перезагрузке стр не должна пропадать Тематическая доставка
                // <? if(!empty($_POST['Order']['deliveryType']) && $_POST['Order']['deliveryType'] > 2): ?>
                    // $('#Order_deliveryType_3').click();
                // <? endif;?>

            // }).trigger('change');

            // $('#Order_thematic_delivery_name').on('change', function () {
                // var price = $(this).find('option:selected').attr('data-price');
                // deliveryPriceBox.html(formatPrice(Number(price)));
                // var price1 = $('input[type=hidden][data-price]').data('price');
                // var total = Number(price) + Number(price1);

                // $('[data-price-box=total]').html(formatPrice(total));
            // });
        });
    </script>

<?php
/** @var CActiveForm $form */
$form = $this->beginWidget(
    'CActiveForm',
    [
        'id' => 'buy-form',
        'enableAjaxValidation' => false,
        'clientOptions' => [
            'validateOnSubmit' => false
        ]
    ]
); ?>
<? if ($form->errorSummary($order)) : ?>
    <div class="col-md-12">
        <div class="validate-area clearfix">
            <?= $form->errorSummary($order); ?>
        </div>
    </div>
<? endif; ?>

    <div class="col-md-12">
        <h1 class="main-title">Оформление заказа</h1>
    </div>
    <div class="checkout-content clearfix">
        <? if (!empty($cart_items)) { ?>
            <? foreach ($cart_items as $k => $cart_item): ?>
                <div class="clearfix">
                    <div class="col-md-3">
                        <div class="checkout-img">
                            <img src="/<?= $cart_item->full_img; ?>" alt="<?= htmlentities($cart_item->title); ?>"/>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="checkout-info">
                            <h3><?= $cart_item->title; ?></h3>

                            <p><?= $cart_item->desc; ?></p>


                            <h4><?= $cart_item->getRoundedPrice(Cart::getCart()->getOptionByItemId($cart_item->id)) ?> руб.</h4>
                            <?= ($cart_item->off_price) ? '<div class="off-price-lable">* цена указана c учетам скидки</div>' : '' ?>
                        </div>
                    </div>

                </div>
            <? endforeach; ?>
        <? } ?>
        <div class="<?= empty($cart_items) ? 'col-md-9' : '' ?>">
            <div class="checkout-info">
<!--                    --><?//$sumPrice = Cart::getCart()->calcDiscountPriceSum();?>
<!--                    <h4>--><?//= $sumPrice ?><!-- руб.</h4>-->
<!--                    <span class="without-denomination">--><?//= number_format($sumPrice * 10000) ?><!-- руб.</span>-->
<!--                --><?// if ($order->flower_id || $order->card_id || $order->toy_id): ?>
<!--                    <p><strong>К подарку так же прилагается:</strong></p>-->
<!--                    <ul>-->
<!--                        --><?// if ($order->flower_id): ?>
<!--                            <li>Цветы "--><?//= $order->flower->flower_title; ?><!--" <span class="help">(Стоимость: --><?//= ($order->flower->flower_price) ?><!-- руб.)</li>-->
<!--                            --><?//= $form->hiddenField($order, 'flower_id', ['data-price' => $order->flower->flower_price]); ?>
<!--                        --><?// endif; ?>
<!---->
<!--                        --><?// if ($order->card_id): ?>
<!--                            <li>Открытка "--><?//= $order->card->card_title; ?><!--" <span class="help">(Стоимость: --><?//= ($order->card->card_price) ?><!-- руб.)</li>-->
<!--                            --><?//= $form->hiddenField($order, 'card_id', ['data-price' => $order->card->card_price]); ?>
<!--                        --><?// endif; ?>
<!---->
<!--                        --><?// if ($order->toy_id): ?>
<!--                            <li>Букет из игрушек "--><?//= $order->toy_title; ?><!--" <span class="help">(Стоимость: --><?//= ($order->toy_price) ?><!-- руб.)</li>-->
<!--                            --><?//= $form->hiddenField($order, 'toy_id', ['data-price' => $order->toy_price]); ?>
<!--                        --><?// endif; ?>
<!---->
<!--                    </ul>-->
<!--                --><?// endif; ?>

                <?= $form->hiddenField($order, 'bow'); ?>
                <?= $form->hiddenField($order, 'texture'); ?>
                <input type="hidden" data-price="<?=Cart::getCart()->calcDiscountPriceSum()?>">
<!--                --><?//= $form->hiddenField($order, 'item_id', ['data-price' => $order->item->getRoundedPrice()]); ?>

            </div>
        </div>

    </div>

    <div class="col-md-12">
        <div class="checkout-line"></div>
    </div>

    <div class="col-md-4">
        <div class="delivery">
            <div class="checkout-title">
                <h3>Способ доставки:</h3>
            </div>
            <ul>
                <?= deliveryTypeList(
                    $order,
                    'deliveryType',
                    // todo править здесь
                  //  ['Самовывоз', 'Обычная доставка', 'Экспресс доставка', 'Тематическая доставка'],
                    ['Самовывоз', 'Обычная доставка', 'Экспресс доставка'],
                    [
                        'data-delivery-type' => 'true',
                        'data-price' => function ($deliveryType) use ($order) {
                            return $order->getPriceForDelivery($deliveryType);
                        },
                    ]
                ); ?>
            </ul>
            <div class="small-cost">
                <p>Стоимость доставки:</p>
                <!-- TODO: What? This value is anther om front-ent!! -->
                <h4><span class="price-container" data-price-box="delivery">140000 руб.</span></h4>
            </div>

        </div>
    </div>

    <div class="col-md-4">
        <div class="checkout-title">
            <h3></h3>
        </div>
        <div class="delivery">
            <div class="col block-offset">
                <div class="hide" data-depends-on-delivery="0">
                    <p class="Order_paymentType_2"><?= Yii::app()->params['pickup_message']?></p>
                    <p class="Order_paymentType_0"><?= Yii::app()->params['time_of_mork_small'] ?></p>
                    <p class="Order_paymentType_1"><?= Yii::app()->params['link_more_addresses'] ?></p>
                </div>
                <div class="hide" data-depends-on-delivery="1">
                                
              <? //  <!--<p>Стоимость доставки - 6 рублей.</p> --> ?>
                </div>
                <div class="hide" data-depends-on-delivery="2">
                    <p>Курьер доставит подарок в указанное место в течение 2-3 часов c момента подтверждения заказа.</p>
                </div>
                <div class="thematic-details hide" data-depends-on-delivery="<?= implode(',', array_keys($prices_thematic)) ?>">
                    <? foreach ($thematicDelivery as $option) : ?>
                        <div class="hide" data-thematic-value="<?= $option->id; ?>">
                            <img src="/<?= $option->preview_img; ?>" class="thumb"/>

                            <p class="thematic-description"><?= $option->desc; ?></p>
                        </div>
                    <? endforeach; ?>
                    <select class="no-fancy-select" name="Order[thematic_delivery_name]" id="Order_thematic_delivery_name">
                        <? foreach ($thematicDelivery as $option): ?>
                            <option data-price="<?= $option->prices_cache[$city_id]; ?>" value="<?= $option->id; ?>" <?= $order->thematic_delivery_name == $option->id
                                ? 'selected="selected"' : ''; ?>><?= $option->title; ?></option>
                        <? endforeach ?>
                    </select>
                </div>
                <div class="hide" data-depends-on-delivery="1,2">
                    <div class="checkbox-cust">
                        <?= $form->checkBox($order, 'sendToRecipient', ['data-show-recipient-form' => '']); ?>
                        <label for="Order_sendToRecipient"></label>
                        <span><strong>Вручить лично получателю подарка</strong></span>
                    </div>
                    <p class="help-box">При выборе опции «вручить лично получателю подарка», курьер доставит подарок
                        человеку, которого вы хотите поздравить.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="delivery delivery-left-border">
            <div class="checkout-title">
                <h3>Способ оплаты:</h3>
            </div>

            <ul>
                <?= $form->radioButtonList(
                    $order,
                    'paymentType',
                    [
                        1 => 'Наличными',
//                        4 => 'EasyPay',
//                        5 => 'Пластиковая карта (WebPay)',
                        6 => 'Картой Visa, MasterCard, Maestro онлайн',
                        7 => 'Картой Халва (2 месяца рассрочки)',
                    ],
                    [
                        'separator' => '',
                        'template' => '<li>{input} {label}</li>'
                    ]
                ); ?>
            </ul>

            <? if(!empty($user) && $user->cash > 0):?>
                <div class="cash">
                    <hr>
                    <h3>Ваш баланс:</h3>
                    <p class="cash-balans">На счету <?=($user->cash)?> руб.</p>
<!--                    <label>*0,1(1000),0,2(2000), 0.3(3000) и т.д.</label> <br>-->
<!--                    <input form="buy-form" disabled name="Order[cash]" class="cash-sum" step="1000" type="range" value="--><?//=$user->cash?><!--" min="1000" max="--><?//=$user->cash?><!--">-->
                    <!--input form="buy-form" disabled name="Order[cash]" class="cash-sum" step="0.1" type="number" value="<?=$user->cash?>" min="0.1" max="<?=$user->cash?>"/-->

                    <div class="cash-used">
                        <input name="Order[cash_check]" class="cash-check" type="checkbox" value="off"/ data-user-cash="<?=$user->cash?>"> <label>Использовать</label>
                        <br>
                        <a href="/bonusy" target="_blank">Подробнее об услуге</a>
                        <!--<span></span>-->
                    </div>

                    <hr>
                </div>
            <? endif?>

            <div class="summ-order item-footer">
                <h3>Итого к оплате:</h3>
                <?$sumPrice = Cart::getCart()->calcDiscountPriceSum();?>
                <p>
                    <span class="price-container" data-price-box="total">
                        <?=$sumPrice?> руб.
                    </span>
                </p>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="checkout-line"></div>
    </div>

    <div class="col-md-6 hide" data-recipient-form>
        <div class="delivery-info clearfix">
            <div class="checkout-title">
                <h3>Получатель подарка:</h3>
            </div>
            <?= $form->textField($order, 'recipient_firstname', ["placeholder" => "Имя получателя"]); ?>
            <?= $form->textField($order, 'recipient_lastname', ["placeholder" => "Фамилия получателя"]); ?>
            <?= $form->textField($order, 'recipient_address', ["placeholder" => "Адрес получения*"]); ?>
            <? $this->widget('CMaskedTextField', array(
                'model' => $order,
                'attribute' => 'recipient_phone',
                'mask' => '+375-99-99-99-999',
                'placeholder' => '*',
                'htmlOptions' => array(
                    'size' => '40',
                    'class' => 'form-control',
                    'placeholder' => 'Контактный телефон получателя подарка*'
                ),
                //'completed' => 'function(){console.log("ok");}',
            )); ?>
            <?= $form->textField($order, 'recipient_gift_date', ['class' => 'gift-date datepicker', "placeholder" => "Желаемая дата вручения подарка*"]); ?>
        </div>
    </div>

    <div class="col-md-6 col-xs-12">
        <div class="delivery-info clearfix">
            <div class="checkout-title">
                <h3>Информация о заказчике:</h3>
            </div>

            <?
            $add = ["placeholder" => "Ваше Имя"];
            if(empty($_POST) && !empty($user->user_firstname)){
                $add['value'] = $user->user_firstname;
            }
            echo $form->textField($order, 'name', $add);
            ?>

            <?
            $add = ["placeholder" => "Ваша Фамилия"];
            if(empty($_POST) && !empty($user->user_lastname)){
                $add['value'] = $user->user_lastname;
            }
            echo $form->textField($order, 'lname', $add);
            ?>

            <?/*
            if(empty($_POST) && !empty($user->user_phone)){
                echo $form->textField($order, 'phone', ['value'  => $user->user_phone]);
            }else{
                $this->widget('CMaskedTextField', array(
                    'model' => $order,
                    'attribute' => 'phone',
                    'mask' => '+375-99-99-99-999',
                    'placeholder' => '*',
                    'htmlOptions' => array(
                        'size' => '40',
                        'class' => 'form-control',
                        'placeholder' => 'Ваш контактый телефон*'
                    ),
                    //'completed' => 'function(){console.log("ok");}',
                ));
            }
            */ ?>

            <select id="countries_phone1" class="form-control bfh-countries" data-country="BY"></select>

            <input type="text" class="form-control bfh-phone" placeholde="Ваш контактый телефон*" name="Order[phone]" data-country="countries_phone1" <?if(empty($_POST) && !empty($user->user_phone)):?>value="<?=$user->user_phone?>"<?endif;?>>


            <?
            $add = ['placeholder' => 'Email*'];
            if(empty($_POST) && !empty($user->user_email)){
                $add['value'] = $user->user_email;
            }
            echo $form->emailField($order, 'email', $add);
            ?>

            <div id="adr-tr" class="hide" data-no-recipient-delivery>  <!-- Hide on recipient form or no delivery -->
                <? $add = ["placeholder" => "Адрес доставки*"];
                if(empty($_POST) && !empty($user->user_address)){
                $add['value'] = $user->user_address;
                }
                echo $form->textField($order, 'adr', $add);
                ?>
            </div>
            <div id="gift-date-tr" class="hide" data-no-recipient-delivery> <!-- Hide on recipient form or no delivery -->
                <?= $form->textField($order, 'gift_date', ["placeholder" => "Дата доставки*", 'class' => 'gift-date datepicker']); ?>
            </div>
            <?= $form->textField($order, 'comment', ["placeholder" => "Комментарий к заказу"]); ?>
            <?= $form->textArea($order, 'message', ["placeholder" => "Ваше поздравление", 'rows'=> 2, 'cols'=> 53]); ?>

            <p>Поля, отмеченные звездочкой (*) обязательны к заполнению.</p>
             <p>Нажимая кнопку "Заказать" вы соглашаетесь на обработку персональных данных в соответствии с <a href="/files/politicy-elpresent.pdf" target="_blank">политкой конфиденциальности</a>.</p>
        </div>
    </div>

    <div class="col-md-12 col-xs-12">
        <div class="checkout-line"></div>
    </div>

    <div class="col-md-12 col-xs-12">
        <div class="summ-order float-left">
            <h3>Итого к оплате:</h3>
            <?$sumPrice = Cart::getCart()->calcDiscountPriceSum();?>
            <p>
                    <span class="price-container" data-price-box="total">
                        <?=$sumPrice?> руб.
                    </span>
            </p>
            <div class="summ-order-buttons">
                <button type="submit" class="btn btn-primary btn-sm btn-mid"  onClick="yaCounter17198173.reachGoal('oformlenie_zakaza'); ga('send', 'pageview', '/virtual/oformlenie_zakaza');this.form.submit();this.innerHTML = 'Отправка';this.disabled = true;">Заказать</button>
            </div>
        </div>
    </div>
<? $this->endWidget(); ?>