<h1>Показыват информацию о заказе по его коду</h1>
<?php if($partner_name):?>
<p>Здравствуйте <?=$partner_name?></p>
<?php endif;?>


<? if(!empty($err)):?>
    <?=$err?>
<? endif;?>

<? if(!empty($title)):?>
<h3>Информация по коду <strong><?=$code?></strong>:</h3>
<h2><?=$title?></h2>
    <p>Действителен до: <?=$end_date?></p>
    <p>Цена: <?=$price_order?> р.</p>
<? endif;?>

<? if($options_info):?>
    <h3>Опции</h3>
    <ul>
        <? foreach($options_info as $option):?>
            <li><?=$option->name;?> <?=$option_name == $option->name ? '<b>[выбран]</b>' : ''?></li>
        <? endforeach;?>
    </ul>
<? endif;?>

<form action="<?=path('order/info')?>" method="post">
    <label>Код заказа</label><br>
    <input name="code" type="number" value="<?=$code?>"><br>
    <label>Пароль</label><br>
    <input name="pass" type="password"><br><br>
    <input class="btn btn-info" name="submit" value="Проверить" type="submit">
    <?php if($activation):?>
    <input class="btn btn-success" name="repay" value="Активировать" type="submit">
    <?php endif;?>
</form>