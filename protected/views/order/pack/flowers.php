<? foreach ($flowers as $flower) :?>
    <div class="flower-selector-outer" style="cursor: pointer;" flower_id="<?=$flower->flower_id;?>" flower_image="<?=$flower->flower_image; ?>" flower_price="<?=$flower->flower_price;?>">
        <div class="flower-selected" style="display: block;"><!-- --></div>
        <div class="flower-selector">
            <img src="/upload/flowers/<?=$flower->flower_image;?>_48.png" width="48" height="45"/>
        </div>
    </div>
<? endforeach; ?>!