<?function clear($subject)
{
    if (mb_strlen($subject, 'UTF-8') >= 50)
    {
        $subject = mb_substr($subject, 0, 48, 'UTF-8');
    }

    if (mb_strlen($subject, 'UTF-8') >= 40)
    {
        $subject = mb_substr($subject, 0, 39, 'UTF-8');
    }

    $charsDenied  = array('"', '&', '>', '<', '\'');
    $charsReplace = array('&quot;', '&amp;', '&gt;', '&lt;', '&apos;');
    $subject = str_replace($charsDenied, $charsReplace, trim($subject));

    return $subject;
}?><?= '<?xml version="1.0" encoding="UTF-8"?>'."\n"?>
<!DOCTYPE yml_catalog SYSTEM "shops.dtd">
<yml_catalog date="<?= date("Y-m-d H:i")?>">
<shop>
  <name>Elpresent.by</name>
  <company>ООО &quot;Интеллектсофт Медиа&quot;</company>
  <url><?= $data['shop']['url']?></url>

  <currencies>
      <currency id="BYR" />
  </currencies>
  <categories>
      <? foreach ($data['categories'] as $category) : ?>
      <category id="<?= $category['id']?>"><?= clear(trim($category['title']))?></category>
      <? endforeach;?>
  </categories>
  <offers>
      <? foreach ($data['offers'] as $offer) : ?>
      <offer id="<?= $offer['id']?>" available="true">
          <url><?= $data['shop']['url'].'/catalog/item/item_id/'.$offer['id'].'/category_id/'.$offer['categoryId'].'.html'?></url>

          <price><?= $offer['price']?></price>
          <currencyId>BYR</currencyId>
          <categoryId><?= $offer['categoryId']?></categoryId>
          <picture><?= $data['shop']['url'].'/'.$offer['picture']?></picture>
          <delivery>true</delivery>
          <local_delivery_cost>0</local_delivery_cost>
          <name><?= clear($offer['name'])?></name>
          <vendor>ООО &quot;Интеллектсофт Медиа&quot;</vendor>
          <vendorCode></vendorCode>
          <description><?= clear($offer['description'])?></description>
          <sales_notes>Сценарий: <?= "\n".clear($offer['sales_notes'])?></sales_notes>
          <country_of_origin>Беларусь</country_of_origin>

          <barcode><?=rand(100000000, 999999999); ?></barcode>
      </offer>
      <? endforeach;?>
  </offers>

</shop>
</yml_catalog>