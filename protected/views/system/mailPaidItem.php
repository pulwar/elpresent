<img src="https://elpresent.by/images/logo.png" width="251" height="95" /><br />
Здравствуйте,
<?=($oOrder->name)
?>!
<br />
<br />
Надеемся, Вы остались довольны уровнем сервиса, оказанного Вам при заказе подарка &quot;<a href="<?= path('catalog/item', ['perm_link' => $oItem->perm_link], true); ?>"><?=$oItem->title?></a>&quot;
<br />
<br />
Нам важно Ваше мнение о качестве работы <a href="https://elpresent.by">Elpresent.by</a>.
<br />
<br />
Мы стремимся предоставлять нашим клиентам только удобный, полезный и своевременный сервис.
<br />
Если Вы остались довольны, Вы можете поблагодарить нас или оставить свои пожелания и замечания по <a href="https://elpresent.by/site/Contact.html">ссылке</a>.
<br />
<br />
Заранее спасибо!
<br />
<br />
--
<br />
С уважением,<br />
команда Эльпрезент<br />
<a href="https://elpresent.by">www.elpresent.by</a><br />
тел.   + 375 29 767 87 28<br />
тел.   + 375 29 687 87 28<br />
факс  + 375 17 237 10 46<br />
Skype  elpresent<br />
icq 77773585<br />