<img src="https://elpresent.by/images/logo.png" width="251" height="95" /><br />
<h1>Информация по активированному подарку</h1>
<p>
    Активирован подарок:
    <a href="<?= path('catalog/item', ['perm_link' => $activeItem->perm_link], true); ?>"><?= $activeItem->title ?></a>
</p>
<p>
    Номер карточки: <?= $orderItem->code ?>
</p>
<p>
    Имя Активировавшего: <?= $orderItem->recivier_name ?>
</p>
<p>
    Контакты активировавшего: <?= $orderItem->recivier_phone ?> ,  <?= $orderItem->recivier_email ?>
</p>

<? if(!empty($partner->name)):?>
<p>
    Название партнера: <?= $partner->name?>
</p>
<? endif;?>

<? if(!empty($partner->contact_person)):?>
<p>
    Контактное лицо: <?= $partner->contact_person?>
</p>
<? endif;?>

<? if(!empty($partner->adress)):?>
<p>
    Адрес партнера: <?= $partner->adress?>
</p>
<? endif;?>

<? if(!empty($partner->place)):?>
<p>
    Место проведения: <?= $partner->place?>
</p>
<? endif;?>

<? if(!empty($partner->phone)):?>
<p>
    Номер телефона партнера: <?= $partner->phone?>
</p>
<? endif;?>

<? if(!empty($partner->work_time)):?>
    <p>
        Время работы: <?= $partner->work_time?>
    </p>
<? endif;?>