<img src="https://elpresent.by/images/logo.png" width="251" height="95" /><br />
Здравствуйте, <?= $recipient_name; ?>!<br />
<br />
<?= $your_name; ?> хочет получить в подарок впечатление <a href="https://elpresent.by/<?= path('catalog/item', ['perm_link'=>$item->perm_link]); ?>"><?= $item->title; ?></a>.
<hr />
С уважением,<br />
Сервис эмоций и впечатлений <a href="https://elpresent.by">elpresent.by</a>!