<img src="https://elpresent.by/images/logo.png" width="251" height="95" /><br />
Здравствуйте<?=($oShopOrder->oBuyForm->firstname && $oShopOrder->oBuyForm->lastname) ? ', '.$oShopOrder->oBuyForm->firstname . ' ' . $oShopOrder->oBuyForm->lastname : '' ?>!<br />
<br />
Спасибо за Ваш заказ в сервисе эмоций и впечатлений <a href="https://elpresent.by">elpresent.by</a>! Ваш подарок будет доставлен в указанный срок.
<hr />
Дата: <?=date('d.m.Y', CDateTimeParser::parse($oShopOrder->oBuyForm->recipient_desired_date, 'yyyy-mm-dd'))?><br />
Номер заказа: <strong><?=$oShopOrder->getOrderNumber()?></strong>
<ul>
  <? foreach ($oShopOrder as $order)
{ ?>
  <li><?=$order->item->title?> (<?=$order->item_quantity?> шт.)
    <? if ($order->flower) { ?><br />Цветы &quot;<?=$order->flower->flower_title?>&quot;<? } ?>
    <? if ($order->card) { ?><br />Открытка &quot;<?=$order->card->card_title?>&quot;<? } ?>
    <br />Цена: <?=($order->price)?>
  </li>
  <? } ?>
</ul>
Итого к оплате (с учетом скидок): <strong><?=($oShopOrder->getTotalPriceWithDiscount())?></strong>
<hr />
Адрес доставки:	<?=$oShopOrder->recipient_address?><br />
Тип оплаты: <?=PaymentType::model()->findByPk($oShopOrder->getPaymentType())->payment_type_title?><br />
<br />
<strong>Спасибо за покупку!</strong><br />
<? if ($oOrderPrevious) { ?>
<br />Если вы остались удовлетворены вашим <a href="<?= path('catalog/item', ['perm_link' => $oOrderPrevious->perm_link], true); ?>">предыдущим заказом</a> - оставьте о нем отзыв, для нас это важно.<br /><br />
<? } ?>С уважением,<br />
ваш персональный менеджер подарков <a href="https://elpresent.by">elpresent.by</a>