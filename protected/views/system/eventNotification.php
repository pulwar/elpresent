<img src="https://elpresent.by/images/logo.png" width="251" height="95" /><br /><span style="text-align: center">
    <strong>Здравствуйте<?=($user->user_firstname && $user->user_lastname) ? ', ' . $user->user_firstname . ' ' . $user->user_lastname : ''
?>!</strong></span><br />
Магазин эмоций и впечатлений <a href="https://elpresent.by/">elpresent.by</a> рад напомнить Вам о предстоящем празднике. Мы поможем Вам выбрать оригинальный и незабываемый подарок.<br />
Дарите своим близким хорошее настроение!<br /><br />

Дата: <strong><?=(isset($event->date) && get_class($event->date) == 'DateTime') ? $event->date->format('d.m.Y') : date('d.m.Y', CDateTimeParser::parse($event->event_date, 'yyyy-mm-dd'))
?></strong><br />
Событие: <strong><?=isset($event->title) ? $event->title : $event->event_title
?></strong><br />
Войти в <a href="https://elpresent.by/profile">личный кабинет</a><br /><br />

С уважением,<br />
Ваш персональный менеджер подарков <a href="https://elpresent.by/">elpresent.by</a>