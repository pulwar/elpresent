<?php /** @var Order $order */ ?>
<?php /** @var Order|null $previousOrder */ ?>
<img src="https://elpresent.by/images/logo.png" width="251" height="95" /><br />
Здравствуйте<?=($order->name && $order->lname) ? ', '.$order->name . ' ' . $order->lname : '' ?>!<br />
<br />
Спасибо за Ваш заказ в сервисе эмоций и впечатлений <a href="https://elpresent.by">elpresent.by</a>! Ваш подарок будет доставлен в указанный срок.
<hr />

    Дата: &nbsp;
    <?= date('d.m.Y', strtotime($order->order_date)); ?>
    <br />

Номер заказа: <strong><?=$order->order_number?></strong>
<ul>
  <li><?=$order->item->title?> (<?=$order->item_quantity?> шт.)
    <? if ($order->flower) { ?><br />Цветы &quot;<?=$order->flower->flower_title?>&quot;<? } ?>
    <? if ($order->card) { ?><br />Открытка &quot;<?=$order->card->card_title?>&quot;<? } ?>
    <br />Цена: <?=($order->price); ?>
  </li>
</ul>
Итого к оплате (с учетом скидок): <strong><?=($order->price)?></strong>
<hr />
Адрес доставки:	<?=$order->recipient_address?><br />
Тип оплаты: <?=$order->payment_type->payment_type_title?><br />
<br />
<strong>Спасибо за покупку!</strong><br />
<? if ($previousOrder) { ?>
<br />Если вы остались удовлетворены вашим <a href="<?= path('catalog/item', ['perm_link' => $previousOrder->item->perm_link], true); ?>">предыдущим заказом</a> - оставьте о нем отзыв, для нас это важно.<br /><br />
<? } ?>С уважением,<br />
ваш персональный менеджер подарков <a href="https://elpresent.by">elpresent.by</a>