<div id="cart">
	<? if(!empty($items)){?>
		<div class="row order-list">
			<div class="col-md-12">
				<div class="order-list-title clearfix">
					<h3 class="complect">Корзина</h3>
					<h3 class="qty">Количество: <span><?=Cart::getCart()->getCountItems()?></span></h3>
				</div>

				<? $cart = Cart::getCart(); ?>
				<? foreach ($items as $k => $item) {
					/** @var Item $item */
					?>
					<div class="order-list-item clearfix">
						<a href="javascript:void(0);" class="cart-delete-item" data-item_from_del_id="<?=$item->id?>">×</a>
						<img src="/<?=$item->preview_img?>"/>
						<h4><?=$item->title?></h4>
						<p><?=$item->getPrice($cart->getOptionByItemId($item->id))?> руб.</p>
						<?=$cart->geTypeByItemId($item->id) == 'digital' ? '<span style="color: #fa6535;">Электронный сертификат</span>': ''?>
					</div>
				<? } ?>
				<div class="order-list-total">
					<p>Стоимость: <span><?=Cart::getCart()->calcCartPriceSum()?> руб.</span></p>
					<a href="<?=path('/order/buy')?>" class="btn btn-primary btn-midd cart-by">Купить</a>
<!--					--><?//= CHtml::link('Купить комплект', [path('/order/buy')], ['class' => 'btn btn-primary btn-midd cart-by']); ?>
				</div>
			</div>
		</div>
	<? } else {?>
		<h3>Корзина пуста</h3>
	<? }?>
</div>
