<div class="modal fade option" id="myModalSeven<?= $item->id ?>" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span
                            aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title custom-header custom-font" id="myModalLabel">В комплект</h4>
            </div>
            <div class="modal-body">
                <div class="modal-inputs">
                    <div class="option-hover">
                        <? $this->renderPartial('_options', ['item' => $item]); ?>
                    </div>
                </div>
            </div>
            <div class="popup_btn_wrap">
                <button type="button" class="btn btn-default add-to-kit-cart" data-id="<?= $item->id ?>" data-item-modal='#myModalItem<?= $item->id ?>'>
                    Добавить
                </button>
            </div>
            <div class="modal-bottom"></div>
        </div>
    </div>
</div>

<div class="modal fade" id="fastOrderInList<?= $item->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title custom-header custom-font" id="myModalLabel">Быстрый заказ</h4>
            </div>
            <div class="modal-body">
                <div class="modal-inputs">
                    <div class="fast-order">
                        <form method="post" action="/order/makeRequest">
                            <p>Оставьте свой номер телефона и мы перезвоним вам.</p>
                            <? $this->widget('CMaskedTextField', [
                                'name' => "OrderRequest[phone]",
                                'attribute' => 'phone',
                                'mask' => '+375-99-99-99-999',
                                'placeholder' => '*',
                                'completed' => 'function(){console.log("ok");}',
                                'htmlOptions' => ['placeholder' => '*Ваш телефон', 'required' => 'required', 'id' => 'phoneRandomId' . $item->id . 'sda-' . rand(200,300)]
                            ]);
                            ?>
                            <input type="hidden" value="<?= $item->id; ?>" name="OrderRequest[item_id]"/>
                            <input type="text" name="OrderRequest[name]" value="" placeholder="*Ваше имя" required>
                            <input type="email" name="OrderRequest[email]" value="" placeholder="*Ваш email" required>

                            <button type="button" class="btn btn-primary btn-sm"
                                    onClick="yaCounter17198173.reachGoal('fast_order'); ga('send', 'pageview', '/virtual/fast_order');">Заказать</button>
                        </form>
                        <p>Нажимая кнопку "Заказать" вы соглашаетесь на обработку персональных данных в соответствии с <a href="/files/politicy-elpresent.pdf" target="_blank">политкой конфиденциальности</a></p>
                    </div>
                    <div data-show-on-success="" style="display: none">
                        <p>Спасибо за Ваш заказ! Если вы сделали заказ <?= Yii::app()->params['time_of_mork_big'] ?>, вам перезвонят в течение 10 минут.</p>
                        <p>Если Вы сделали заказ в другое время, Вам перезвонят утром.</p>
                    </div>
                </div>
            </div>
            <div class="modal-bottom"></div>
        </div>
    </div>
</div>

<div class="modal fade option" id="myModalItem<?= $item->id ?>" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Close</span></button>
                <h4 class="modal-title custom-header custom-font" id="myModalLabel">Добавлено: <?= $item->title; ?></h4>
            </div>
            <div class="modal-body">
                <div class="modal-inputs" style="border-top:0;">
                    <div class="product-item item modal0-Item">
<!--                        <h3><a href="--><?//= path('catalog/item', ['perm_link' => $item->perm_link]); ?><!--">--><?//= $item->title; ?><!--</a></h3>-->
                        <div class="product-item-content clearfix">
                            <div class="product-item-image">
                                <div class="product-item-image-inside">


                                    <div class="action_wrap">
                                        <? if ($item->is_new): ?>
                                            <div class="action_item _new">Новинка</div>
                                        <? endif; ?>
                                        <? if ($item->is_hot): ?>
                                            <div class="action_item _hit">Хит</div>
                                        <? endif; ?>
                                        <? if ($item->off_price != 0): ?>
                                            <div class="action_item _sale">Скидка <?= round($item->off_price); ?>%</div>
                                        <?endif;?>

                                        <a href="<?= path('catalog/item', ['perm_link' => $item->perm_link]); ?>" class="list_item_img lazy" data-src="<?= '/' . $item->full_img ?>"></a>
                                    </div>

                                </div>
                            </div>
                            <div class="product-item-description">
                                <span><?= $item->desc; ?></span>
                                <div class="product-item-price"><?=$count_prices > 1 ? '' : $item->getVariablePriceForView()?></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item_btn_wrap clearfix">
                    <div class="list_item_btn_cell">
                        <a class="btn_list btn_green" onclick="$('#myModalItem<?= $item->id ?>').modal('hide')">Продолжить покупки</a>
                    </div>
                    <div class="list_item_btn_cell">
                        <a class="btn_list shose-price " href="<?=path('cart')?>">В корзину</a>
                    </div>
                </div>
            </div>


            <div class="modal-bottom"></div>
        </div>
    </div>
</div>

