<img src="https://elpresent.by/images/logo.png" width="251" height="95" />
<p>Здравствуйте, <?=$user->getFullName(); ?></p>
<p>Поздравляем, Вы зарегистрировались на сайте ELpresent.by! Приятных Вам покупок!</p>
<p>
    Ваши данные учетной записи:<br />
    <strong>Email: </strong> <?= $user->user_email; ?>
    <strong>Пароль: </strong> <?= $password; ?>
</p>
<p>С уважением, команда <a href="https://elpresent.by/">ELpresent.by</a></p>
