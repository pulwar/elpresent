<?php
/** @var User $user */
$isToday = $user->user_birthday_date === date('m-d');
?>
<p><?php if ($isToday) { ?>Сегодня<?php } else { ?> Через неделю <?php } ?> пользователь <a href="<?= path('admin/user/manage', ['id' => $user->id], true); ?>"><?= $user->email; ?></a> отмечает день рождения</p>