<? $this->layout = 'yml'; ?>
<name><?= $name ?></name>
<company><?= $company ?></company>
<url><?= $url ?></url>
<currencies>
    <currency id="BYN" rate="1"/>
    <currency id="BYR" rate="10000"/>
</currencies>
<categories>
    <? foreach ($categories as $category): ?>
        <category id="<?= $category['id'] ?>" <? if (!empty($category['parentId'])): ?>parentId="<?= $category[parentId] ?>"<? endif ?>>
            <?= $category['title'] ?>
        </category>
    <? endforeach; ?>
</categories>
<delivery-options>
    <option cost="<?= $delivery_general_price ?>" days="0" order-before="17"/>
</delivery-options>
<cpa>1</cpa>
<offers>
    <? foreach ($offers as $offer): ?>
        <offer id="<?= $offer->id ?>" available="true">
            <url><?= $url . '/' . $offer->perm_link ?></url>
            <? if (is_array($offer->prices_cache[1])): ?>
                <price from="true"><?= array_shift($offer->prices_cache[1]) ?></price>
                <? else: ?>
                <price><?= $offer->prices_cache[1] ?></price>
            <? endif; ?>
            <currencyId>BYN</currencyId>
            <categoryId><?= $offer->categories[0]->id ?></categoryId>
            <picture><?= $url . '/' . $offer->preview_img ?></picture>
            <picture><?= $url . '/' . $offer->full_img ?></picture>
            <store>false</store>
            <delivery>true</delivery>
            <pickup>true</pickup>
            <name><?= htmlspecialchars($offer->title) ?></name>
            <description><?= $offer->desc ?></description>
        </offer>
    <? endforeach ?>
</offers>
