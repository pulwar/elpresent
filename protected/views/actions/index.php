<h1>Новости и акции</h1>
<div class="row">
    <div class="col-md-12">
        <? foreach($items as $key => $item):?>
            <div class="news-list-item">
                <h3><a href="<?=Yii::app()->createUrl('/actions/view', ['id'=>$item->id]); ?>"><?=$item->name?></a></h3>
                <div class="row">
                    <div class="news-list-item-img col-md-5">
                        <? if(!empty($item->img)):?>
                            <img src="/images/<?=$item->img?>" class="preview" alt="<?=$item->name?>"/>
                        <? endif;?>
                    </div>
                    <div class="news-list-item-info col-md-7">
                        <?=$item->desc?>
                        <div class="news-list-item-info-bottom">
                            <a class="btn btn-primary btn-sm btn-mid" href="<?=Yii::app()->createUrl('/actions/view', ['id'=>$item->id]); ?>">Подробнее</a>
                            <div class="news-list-item-date">
                                <time>
                                    <?=date::human_ru_time($item->date)?>
                                </time>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <? endforeach;?>

        <?$this->widget('CLinkPager', array(
            'pages' => $pages,
            'header' => '',
            'cssFile' => false,
            'id' => 'pagination',
            'nextPageLabel'=>'<span aria-hidden="true">Следущая</span><span class="sr-only">Следущая</span>',
            'previousPageCssClass' => 'hidden',
            'lastPageCssClass' => 'hidden',
            'firstPageLabel'=>'<span aria-hidden="true">Первая</span><span class="sr-only">Первая</span>',
            'selectedPageCssClass' => 'active',
            'hiddenPageCssClass' => 'bgg',
            'htmlOptions' => array(
                'class' => 'pagination',
            ),
        ))?>

    </div>
</div>