<h1>Новости и акции</h1>
<div class="row">
	<div class="col-md-12">
		<div class="news-main-item">
			<h3><?=$item->name?></h3>
			<?=$item->txt?>
		</div>
		<div class="news-main-item-bottom clearfix">
			<div class="news-main-item-date">
				<time>
					<?=date::human_ru_time($item->date)?>
				</time>
			</div>
			<a class="btn btn-primary btn-sm btn-mid float-r" href="<?=Yii::app()->createUrl('/actions'); ?>">Все новости и акции</a>
		</div>
		<hr>
	</div>
</div>
