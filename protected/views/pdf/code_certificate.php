<?php
/** @var Order $order */
$rootDir = empty($rootDir) ? '':$rootDir;
?>
<!DOCTYPE html>
<head>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="<?=$rootDir?>/pdf_code/css/pdf.css" />
    <title>Предпросмотр сертификата</title>
</head>

<body>
<div class="page">
    <div class="info">
        <div class="left">
            <p><b><?=$model->title;?></b></p>
            <p><b>Код скидки:<br><?=$code;?></b> </p>
            <p><b>Воспользоваться купоном до:<br><?=date('d.m.Y', $model->time_finish);?></b> </p>
        </div>
        <div class="right">
            <p><b>Услугу оказывает:</b> <?=$model->partner->name?></p>
            <p><b>Адрес проведения:</b> <?=$model->address?></p>
            <p><b>Телефоны:</b> <?=$model->tel?></p>
        </div>
    </div>
</div>
</body>