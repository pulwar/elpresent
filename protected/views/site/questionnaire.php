<?if ( $questionnaire->getErrors() && isset($_POST['Questionnaire'])) : ?>

<div class="d-no">
        <?= CHtml::errorSummary($questionnaire); ?>
</div>

<?elseif (Yii::app()->user->hasFlash('questionnaire')) : ?>

<div class="yes">
    <?= Yii::app()->user->getFlash('questionnaire'); ?>
</div>

<? endif; ?>

<form id="questionnaire-form" action="" method="post">
<div class="article">
    <div class="section">
        <table cellspacing="0" cellpadding="0" class="text-head-green">
            <tr>
                <td class="left">&nbsp;</td>
                <td class="center"><h1>Анкета корпоративного клиента</h1></td>
                <td class="right">&nbsp;</td>
            </tr>
        </table>
        <table width="100%" cellspacing="0" cellpadding="0">
            <tr>
                <td class="text-main-left">&nbsp;</td>
                <td class="text-main">
                        <div class="gray-bg questionnaire">
                                <table class="form-inputs">
                                    <tr>
                                        <td class="one">
                                            <label for="company-name">Название компании:<br /><i>(обязательно)</i></label>
                                        </td>
                                        <td class="two">
                                            <div class="input-order">
                                                <input type="text" name="Questionnaire[company_name]" id="company-name" value="<?
                                                            if (isset($questionnaire->company_name) && $questionnaire->company_name)
                                                            {
                                                                echo $questionnaire->company_name;
                                                            }?>" />
                                                <div class="input-order-right"></div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="one">
                                           <label for="company-activities">Направление деятельности компании:<br /><i>(обязательно)</i></label>
                                        </td>
                                        <td class="two">
                                            <div class="input-order">
                                                <input type="text" name="Questionnaire[company_activities]" id="company-activities" value="<?
                                                            if (isset($questionnaire->company_activities) && $questionnaire->company_activities)
                                                            {
                                                                echo $questionnaire->company_activities;
                                                            }?>" />
                                                <div class="input-order-right"></div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="one">
                                           <label for="employees-num">Количество сотрудников:<br /><i>(обязательно)</i></label>
                                        </td>
                                        <td class="two">
                                            <div class="input-order">
                                                <input type="text" name="Questionnaire[employees_num]" id="employees-num" value="<?
                                                            if (isset($questionnaire->employees_num) && $questionnaire->employees_num)
                                                            {
                                                                echo $questionnaire->employees_num;
                                                            }?>" />
                                                <div class="input-order-right"></div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="one">
                                           <label for="contact">Контактное лицо (с должностью):<br /><i>(обязательно)</i></label>
                                        </td>
                                        <td class="two">
                                            <div class="input-order">
                                                <input type="text" name="Questionnaire[contact]" id="contact" value="<?
                                                            if (isset($questionnaire->contact) && $questionnaire->contact)
                                                            {
                                                                echo $questionnaire->contact;
                                                            }
                                                            elseif(Yii::app()->user->getId() && Yii::app()->user->getUser()->user_firstname)
                                                            {
                                                                echo Yii::app()->user->getUser()->user_firstname;
                                                            }
                                                        ?>" />
                                                <div class="input-order-right"></div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="one">
                                           <label for="phone">Контактный телефон:<br /><i>(обязательно)</i></label>
                                        </td>
                                        <td class="two">
                                            <div class="input-order">
                                                <input type="text" name="Questionnaire[phone]" id="phone" value="<?
                                                            if (isset($questionnaire->phone) && $questionnaire->phone)
                                                            {
                                                                echo $questionnaire->phone;
                                                            }
                                                            elseif(Yii::app()->user->getId() && Yii::app()->user->getUser()->user_phone)
                                                            {
                                                                echo Yii::app()->user->getUser()->user_phone;
                                                            }
                                                        ?>" />
                                                <div class="input-order-right"></div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="one">
                                           <label for="source">Откуда вы о нас узнали?<br /><i>(обязательно)</i></label>
                                        </td>
                                        <td class="two">
                                            <div class="input-order">
                                                <input type="text" name="Questionnaire[source]" id="source" value="<?
                                                            if (isset($questionnaire->source) && $questionnaire->source)
                                                            {
                                                                echo $questionnaire->source;
                                                            }
                                                            ?>" />
                                                <div class="input-order-right"></div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="one">
                                           Принято ли в вашей компании поздравлять сотрудников с днями рождения и с другими памятными датами?<br /><i>(обязательно)</i>
                                        </td>
                                        <td class="two">
                                            <label for="employees-bd-yes">Да</label> <input type="radio" name="Questionnaire[employees_bd]" value="yes" id="employees-bd-yes" checked="checked" />
                                            <label for="employees-bd-no">Нет</label> <input type="radio" name="Questionnaire[employees_bd]" value="no" id="employees-bd-no" <?= ($questionnaire->employees_bd == 'no') ? 'checked="checked"' : ''; ?>/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="one">
                                           <label for="presents">Что вы обычно дарите вашим сотрудникам?<br /><i>(обязательно)</i></label>
                                        </td>
                                        <td class="two">
                                            <div class="input-order">
                                                <input type="text" name="Questionnaire[presents]" id="presents" value="<?
                                                            if (isset($questionnaire->presents) && $questionnaire->presents)
                                                            {
                                                                echo $questionnaire->presents;
                                                            }
                                                            ?>" />
                                                <div class="input-order-right"></div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="one">
                                           Работали ли раньше со службами по организации корпоративных мероприятий или магазинами подарков?<br /><i>(обязательно)</i>
                                        </td>
                                        <td class="two">
                                            <label for="other-shops-yes">Да</label> <input type="radio" value="yes" name="Questionnaire[other_shops]" id="other-shops-yes" checked="checked" />
                                            <label for="other-shops-no">Нет</label> <input type="radio" value="no" name="Questionnaire[other_shops]" id="other-shops-no" <?= ($questionnaire->other_shops == 'no') ? 'checked="checked"' : ''; ?> />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="one">
                                           <label for="other-companies">С какими?</label>
                                        </td>
                                        <td class="two">
                                            <div class="input-order">
                                                <input type="text" name="Questionnaire[other_companies]" id="other-companies" value="<?
                                                            if (isset($questionnaire->other_companies) && $questionnaire->other_companies)
                                                            {
                                                                echo $questionnaire->other_companies;
                                                            }
                                                            ?>" />
                                                <div class="input-order-right"></div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="one">
                                            <label for="wishes">Пожелания по работе с нами:</label>
                                        </td>
                                        <td class="two">
                                            <div class="textarea-order">
                                                <textarea name="Questionnaire[wishes]" id="wishes" cols="10" rows="10"><?= $oFaqModel->question; ?><?
                                                            if (isset($questionnaire->wishes) && $questionnaire->wishes)
                                                            {
                                                                echo $questionnaire->wishes;
                                                            }
                                                            ?></textarea>
                                                <div class="textarea-order-right"></div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="one">
                                            &nbsp;
                                        </td>
                                        <td class="two">
                                            <a class="send-button" href="javascript:void(0);" onclick="document.getElementById('questionnaire-form').submit();">
                                        </a></td>
                                    </tr>
                                </table>
                            </div>
                    </td>
                <td class="text-main-right">&nbsp;</td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" class="text-bottom">
            <tr>
                <td class="left">&nbsp;</td>
                <td class="center">&nbsp;</td>
                <td class="right">&nbsp;</td>
            </tr>
        </table>
    </div>
</div>
</form>