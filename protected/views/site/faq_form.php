<table class="form-inputs">
    <tr>
        <td class="one">
            Имя:<br/><i>(обязательно)</i>
        </td>
        <td class="two">
            <div class="input-order">
                <input type="text" name="name" value="<?= $oFaqModel->name; ?>"/>

                <div class="input-order-right"></div>
            </div>
        </td>
    </tr>
    <tr>
        <td class="one">
            E-mail:<br/><i>(обязательно)</i>
        </td>
        <td class="two">
            <div class="input-order">
                <input type="text" name="email" value="<?= $oFaqModel->email; ?>"/>

                <div class="input-order-right"></div>
            </div>
        </td>
    </tr>
    <tr>
        <td class="one">
            Ваш вопрос:<br/><i>(обязательно)</i>
        </td>
        <td class="two">
            <div class="textarea-order">
                <textarea name="question" cols="10" rows="10"><?= $oFaqModel->question; ?></textarea>

                <div class="textarea-order-right"></div>
            </div>
        </td>
    </tr>
    <tr>
        <td class="one">
            &nbsp;
        </td>
        <td class="two">
            <a class="send-request" href="javascript:void(0);" onclick="reachGoal('vopros');document.getElementById('<?= $target; ?>').submit();">
        </td>
    </tr>
</table>