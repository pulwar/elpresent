<script type="text/javascript" src="/js/jquery.jcarousel.min.js"></script>
<div class="article static-page">
    <h1 class="ribbon big green">Скидки</h1>
    <div class="content">
        <div class="article-container"><?= TextRecord::getAliasedText('discount'); ?></div>
        <form id="discount-form" action="" method="post">
            <br />
            <? if ( $oFaqModel->hasErrors() ) : ?>
                <div style="margin-left: 100px; width: 500px;" id="error_activation" class="no">
                    <?
                    foreach ( $oFaqModel->getErrors() as $error ) {
                        echo str_replace('Атрибут', 'Поле', $error[0]).'<br />';
                    }
                    ?>
                </div>
            <? else : ?>
                <? if ( Yii::app()->user->hasFlash('faq_success') ) : ?>
                    <div style="margin-left: 100px; width: 500px;" id="ok_activation" class="yes">
                        <?= Yii::app()->user->getFlash('faq_success'); ?>
                    </div>
                <? endif; ?>
            <? endif; ?>

            <div style="padding: 0px 55px;">
                <div class="gray-bg">
                    <?php $this->renderPartial('faq_form', ['oFaqModel' => $oFaqModel, 'target' => 'discount-form']); ?>
                </div>
            </div>
        </form>
    </div>
</div>