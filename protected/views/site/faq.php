<form id="faq-form" action="<?= path('site/faq'); ?>" method="POST">
<div class="article">
    <div class="section">
        <table cellspacing="0" cellpadding="0" class="text-head-green">
            <tbody><tr>
                <td class="left">&nbsp;</td>
                <td class="center"><h1>Часто задаваемые вопросы</h1></td>
                <td class="right">&nbsp;</td>
            </tr>
        </tbody></table>
        <table width="100%" cellspacing="0" cellpadding="0">
            <tr>
                <td class="text-main-left">&nbsp;</td>
                <td class="text-main">
                    <br /><br />
                    <?= TextRecord::model()->getAliasedText('Faq'); ?>
                    
                    <? if ( $oFaqModel->hasErrors() ) : ?>
                    <div style="margin-left: 100px; width: 500px;" id="error_activation" class="no">
                    <? 
                        foreach ( $oFaqModel->getErrors() as $error )
                        {
                            echo str_replace('Атрибут', 'Поле', $error[0]).'<br />';
                        }
                    ?>
                    </div>
                    <? else : ?>
                        <? if ( Yii::app()->user->hasFlash('faq_success') ) : ?>
                        <div style="margin-left: 100px; width: 500px;" id="ok_activation" class="yes">
                        <?= Yii::app()->user->getFlash('faq_success'); ?>
                        </div>                 
                        <? endif; ?>
                    <? endif; ?>

                    <div style="padding: 0px 90px;">
                        <div class="gray-bg" style="">
                           <?php $this->renderPartial('faq_form', ['oFaqModel' => $oFaqModel, 'target' => 'faq-form']); ?>
                        </div>
                    </div>
                </td>
                <td class="text-main-right">&nbsp;</td>
            </tr>
        </table>
        <table cellspacing="0" cellpadding="0" class="text-bottom">
            <tr>
                <td class="left">&nbsp;</td>
                <td class="center">&nbsp;</td>
                <td class="right">&nbsp;</td>
            </tr>
        </table>
    </div>
</div>
</form>