<h1 class="main-title">Об ELPRESENT</h1>

<? $this->pageTitle = Yii::app()->name . ' - Contact Us'; ?>

<? if ($oContact->getErrors() && isset($_POST['Contact'])) : ?>

    <div class="col-md-12">
        <div class="validate-area clearfix">
            <?= CHtml::errorSummary($oContact); ?>
        </div>
    </div>


<? elseif (Yii::app()->user->hasFlash('contact')) : ?>
    <div class="alert alert-success">
        <?= Yii::app()->user->getFlash('contact'); ?>
    </div>
<? endif; ?>

<div class="row">
    <div class="col-md-6">

        <div class="text-widget">
            <h3 class="sub-title">Наш сервис</h3>
            <img src="/img/new_design/our-service.png" alt="our-service" class="text-widget-image"/>

            <p>Мы предлагаем вам окунуться в мир необычных ощущений и эмоций. В
                нашем каталоге подарков-впечатлений вы найдете самые
                оригинальные и полезные решения для любого праздника ваших
                любимых и близких, бизнес-партнеров, друзей и просто хороших
                людей, которым вы хотите подарить частичку своего внимания.</p>
            <blockquote class="quote">
                <p>У многих людей хороший сервис ассоциируется с быстротой и
                    удобством, вежливостью и готовностью помочь, умением понять,
                    что хочет клиент, и дать ему именно то, что он хочет,
                    вовремя и с улыбкой. Мы стремимся сделать наш сервис именно
                    таким и еще немного лучше.</p>
            </blockquote>
        </div>

        <div class="text-widget">
            <h3 class="sub-title">Доставка</h3>
            <img src="/img/new_design/shipping.png" alt="shipping" class="text-widget-image"/>

            <p>Мы гордимся тем, что клиентам приятно и удобно с нами работать. Стандартная
                доставка осуществляется в тот же день при заказе до 15.00 и в первой половине
                следующего дня при заказе после 15.00 бесплатно.В случае если вам срочно нужно
                получить подарок, мы предлагаем воспользоваться опцией экспресс-доставка. В
                течение 2-х часов подарок будет у вас, и вы успеете поздравить ваших близких.
                Для тех, кто любит делать сюрпризы, мы предлагаем воспользоваться дополнительной
                опцией – вручить лично. После заказа и оплаты наш курьер в указанное время в
                указанном месте вручит подарок прямо в руки получателю.</p>
        </div>

        <div class="text-widget">
            <h3 class="sub-title">Служба корпоративного счастья</h3>
            <img class="text-widget-image" src="/img/new_design/SERVICE-OF-happyness.png" alt="">

            <p>Мы верим в высокую корпоративную культуру и в то, что праздники компании и дни
                рождения сотрудников нужно отмечать. Это несомненно приносит радость, хорошее
                настроение, и сплочает ваш коллектив. А где довольные сотрудники – там хорошо
                выполненная работа! Именно поэтому мы запустили Службу корпоративного счастья.
                Наша миссия – сделать так, чтобы ваши сотрудники гордились своей работой и
                знали, что их ценят.</p>
        </div>

        <div class="text-widget">
            <h3 class="sub-title">Накопительная система скидок</h3>
            <img class="text-widget-image" src="/img/new_design/discounts.png" alt="">

            <p>Мы любим наших клиентов, поэтому хотим, чтобы они росли вместе с нами. Став
                нашим постоянным клиентом, вы участвуете в накопительной системе скидок. Чем
                больше заказов вы сделаете, тем выше будет ваша скидка на последующие заказы.</p>
        </div>

    </div>
    <div class="col-md-6">

        <div class="text-widget">
            <h3 class="sub-title">Почему сервис?</h3>
            <img class="text-widget-image" src="/img/new_design/why-just-service.png" alt="">

            <p>Все дело в том, что elpresent.by – это не только подарки, это целая команда с
                искренним желанием принести радость и счастье вам и вашим близким. Мы помогаем
                вам выбрать подходящий подарок, оформить его, доставляем его вовремя и
                поздравляем ваших именинников. Мы стремимся сделать вашу жизнь чуточку проще,
                подумав за вас, что подарить и как это сделать.</p>
        </div>

        <div class="text-widget">
            <h3 class="sub-title">Онлайн поддержка</h3>
            <img class="text-widget-image" src="/img/new_design/online-support.png" alt="">

            <p>Внимательный консультант поможет с выбором подарка, больше раскажет о
                впечатлениях, ответит на все вопросы. Вы можете связаться с консультантом по
                телефону, icq, skype, электронной почте или через онлайн помощник на сайте. Ваш
                заказ будет своевременно обработан и доставлен по указанному адресу.</p>
        </div>

        <div class="text-widget">
            <h3 class="sub-title">Конструктор упаковки, цветы и открытки</h3>
            <img class="text-widget-image" src="/img/new_design/package-constructor.png" alt="">

            <p>При выборе подарка мы предлагаем вам немного покреативить и самим выбрать
                оформление. В ассортименте упаковки дизайнерские коробочки и яркие ленты. Кроме
                этого мы можете сопроводить подарок оригинальной открыткой и букетом свежих
                цветов. Все для того, чтобы сделать ваш подарок незабываемым, а праздник ярким.</p>
        </div>

        <div class="text-widget">
            <h3 class="sub-title">Время работы</h3>
            <img class="text-widget-image" src="/img/new_design/time.png" alt="">

            <p>Мы работаем для вас с <?= Yii::app()->params['time_of_mork_big'] ?>. Онлайн-заказ можно сделать в любое время.</p>
        </div>

        <div class="text-widget">
            <h3 class="sub-title">Личный кабинет и календарь праздников</h3>
            <img class="text-widget-image" src="/img/new_design/calendar.png" alt="">

            <p>Мы заботимся о наших клиентах. Специально для вашего удобства мы создали личный
                кабинет, доступ к которому вы получаете после совершения заказа. В нем можно
                отследить историю покупок на elpresent.by, а также настроить календарь
                праздников, отметив значимые даты. Мы напомним о важных событиях вашей жизни по
                e-mail, чтобы вы были к ним готовы.</p>
        </div>

    </div>
</div>

<div class="row">
    <div class="col-md-12">

        <hr class="border border-large">

        <div class="feedback">
            <h2 class="custom-header custom-font header-with__link"><span>Отзывы покупателей</span>
                <p><a href="#" class="btn btn-default btn-lg" data-toggle="modal" data-target="#myModal">Оставить отзыв</a></p>
            </h2>

            <? if (count($oMessages)) : ?>
                <? foreach ($oMessages as $oMessage) : ?>
                    <div class="feedback-article">
                        <h3 class="feedback-name">
                            <?= htmlspecialchars($oMessage->contact_name); ?> - <span class="feedback-rating"></span>
                            <span class="rating">
                                <? for ($i = 1; $i <= 5; $i++) : ?>
                                    <? if ($i <= $oMessage->contact_rating): ?>
                                        <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                    <? endif; ?>
                                <? endfor; ?>
                            </span>
                        </h3>

                        <div class="feedback-item">
                            <h4 class="feedback-item-title"><?= htmlspecialchars($oMessage->contact_subject); ?></h4>
                            <p><?= htmlspecialchars($oMessage->contact_body); ?></p>
                        </div>
                    </div>
                <? endforeach; ?>
            <? endif; ?>

            <p><a href="#" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">Оставить отзыв</a></p>

        </div>

    </div>
</div>


