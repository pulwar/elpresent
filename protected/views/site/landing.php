<? $this->layout = 'landing'; ?>

<div class="landing-wrap">
    <div class="header-wrap">
        <header class="header">
            <div class="logo"><a href="/"><img src="/img/landing/logo.svg" alt="logo"></a></div>
            <nav class="nav-landing">
                <a class="page-scroll" href="#main">Главная</a>
                <a class="page-scroll" href="#advantage">Преимущества</a>
                <a class="page-scroll" href="#reviews">Отзывы</a>
                <a class="page-scroll" href="#catalog">Ассортимент</a>
                <a class="page-scroll" href="#delivery">Доставка</a>
                <a class="page-scroll" href="#clients">Клиенты</a>
                <a class="page-scroll" href="#contacts">Контакты</a>
            </nav>
            <a class="header-phone" href="tel:+375296878728">+37529 687 87 28</a>
            <div class="nav-mob">
                <span></span>
                <span></span>
                <span></span>
            </div>
        </header>
    </div>
    <div class="front-block">
        <div id="main" class="landing-item">
            <div class="landing-item-inner">
                <div class="main-container">
                    <h1>Подарки</h1>
                    <h3>для корпоративных клиентов</h3>
                    <div class="main-info">Брендированные комплекты сертификатов для вас и ваших сотрудников. Наши специалисты в оптимальные сроки помогут вам с выбором корпоративных подарков.</div>
                    <a href="#contacts" class="landing-btn page-scroll">Связаться</a>
                </div>
            </div>
        </div>
        <div id="advantage" class="landing-item">
            <div class="landing-item-inner">
                <div class="advantage-slider">
                    <div class="advantage-row">
                        <div class="advantage-cell">
                            <div class="advantage-item">
                                <img src="/img/landing/smile.svg" alt="smile">
                                <h4>Индивидуальный подход</h4>
                                <p>Учитываем все особенности вашего запроса</p>
                            </div>
                            <div class="advantage-item">
                                <img src="/img/landing/like.svg" alt="like">
                                <h4>Огромный ассортимент</h4>
                                <p>Сотни вариантов для подарка: активный отдых, спа, мастер-классы и квесты</p>
                            </div>
                        </div>
                        <div class="advantage-cell">
                            <div class="advantage-item">
                                <img src="/img/landing/exclude.svg" alt="exclude">
                                <h4>Гибкая система скидок</h4>
                                <p>Скидки от 5 до 20 %</p>
                            </div>
                            <div class="advantage-item">
                                <img src="/img/landing/calendar.svg" alt="calendar">
                                <h4>Оптимальные сроки</h4>
                                <p>Мы подберем и соберем для вас наборы эмоций за оптимальные сроки</p>
                            </div>
                        </div>
                    </div>
                    <div class="advantage-row">
                        <div class="advantage-cell">
                            <div class="advantage-item">
                                <img src="/img/landing/reward.svg" alt="reward">
                                <h4>Качество услуг</h4>
                                <p>Мы лично тестируем услуги перед размещением на нашем сервисе</p>
                            </div>
                            <div class="advantage-item">
                                <img src="/img/landing/present.svg" alt="present">
                                <h4>Отличный подарок</h4>
                                <p>Наши сертификаты подарят настоящие эмоции и оставят приятные воспоминания</p>
                            </div>
                        </div>
                        <div class="advantage-cell">
                            <div class="advantage-item">
                                <img src="/img/landing/ring.svg" alt="ring">
                                <h4>Красивый дизайн</h4>
                                <p>Залог хорошего настроения наших клиентов - яркий дизайн сертификатов</p>
                            </div>
                            <div class="advantage-item">
                                <img src="/img/landing/costume.svg" alt="costume">
                                <h4>Подчеркнет стиль компании</h4>
                                <p>Сертификаты могут быть брендированы и выполнены в корпоративных цветах вашей компании</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="reviews" class="landing-item colored">
            <div class="landing-item-inner">
                <div class="photo-wrap">
                    <div class="photo-item"></div>
                    <div class="info-wrap">
                        <h2>Что говорят наши клиенты</h2>
                        <div class="reviews-slider">
                            <div class="reviews-item">
                                <div class="reviews-text">Заказывали сертификаты сотрудникам. Штат у нас маленький, поэтому нам важно было индивидуально подойти к подбору каждого подарка. Менеджер учел все пожелания и интересы коллег, поэтому все остались довольны. Приятно работать с таким сервисом.</div>
                                <div class="reviews-note">Ирина Шостак, заместитель директора по коммерческим вопросам ООО “Траффаэль”</div>
                            </div>
                            <div class="reviews-item">
                                <div class="reviews-text">Искали подарок для руководительницы. Учитывая, что на все праздники она получала подарочные сертификаты, очень сложно было не повториться. Обратились в elpresent.by, вдруг предложат что-то оригинальное. Менеджер очень оперативно прислала варианты под наши пожелания и бюджет. В итоге нашли то, что руководительнице еще не дарили!) Быстро оформили и доставили прямо в магазин. </div>
                                <div class="reviews-note">Юлия Бусова, продавец магазина ювелирных украшений ZIKO</div>
                            </div>
                            <div class="reviews-item">
                                <div class="reviews-text">Поздравляли коллег с 23 февраля. Написали менеджеру вечером 22-го февраля в конце рабочего дня. Нужно было оформить и доставить 15 сертификатов на следующий день утром. Боялись, что не успеем, но заказ очень быстро обработали в тот же вечер и курьер доставил максимально оперативно прямо в офис. Спасибо, что так выручили)</div>
                                <div class="reviews-note">Александра Барановская, менеджер по продажам ООО “МетаМакс”</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="catalog" class="landing-item">
            <div class="landing-item-inner">
                <h2>Варианты оформления подарков</h2>
                <p>Варианты оформления подарочных сертификатов. Для вас мы можем изготовить брендированные сертификаты и упаковки в ваших корпоративных цветах.</p>
                <div class="catalog-slider">
                    <div class="catalog-item">
                        <img src="/img/landing/sert.svg" alt="costume">
                        <h4>Подарочные сертификаты</h4>
                        <p>Универсальный подарочный сертификат с возможностью выбора любого подарка в пределах указанной стоимости.</p>
                        <a href="https://elpresent.by/podarki-dlya-devushek/spa-prozedyri/universalnyiy-podarochnyiy-sertifikat-elpresentby-na-summu" class="primary-btn">Подробнее</a>
                    </div>
                    <div class="catalog-item">
                        <img src="/img/landing/box.png" alt="costume">
                        <h4>Комплекты впечатлений</h4>
                        <p>Лучшие предложения собраны в подарочные комплекты, которые порадуют ваших сотрудников яркими эмоциями</p>
                        <a href="https://elpresent.by/komplekty-vpechatlenij" class="primary-btn">Подробнее</a>
                    </div>
                    <div class="catalog-item">
                        <img src="/img/landing/sert2.svg" alt="costume">
                        <h4>Брендирование конверта</h4>
                        <p>Подготовим для вас брендированный подарочный сертификат с логотипом вашей компании.</p>
                        <a href="#contacts" class="primary-btn page-scroll">Подробнее</a>
                    </div>
                </div>
            </div>
        </div>
        <div id="delivery" class="landing-item colored">
            <div class="landing-item-inner">
                <div class="photo-wrap">
                    <div class="photo-item"></div>
                    <div class="info-wrap">
                        <h2>Доставка подарков</h2>
                        <p>Наши курьеры доставят вам подарки прямо в офис либо указанное вами место. Мы заботимся о наших клиентах и доставляем наши подарки прямо в руки. </p>
                    </div>
                </div>
            </div>
        </div>
        <div id="clients" class="landing-item">
            <div class="landing-item-inner">
                <h2>Нам доверяют</h2>
                <div class="clients-wrap">
                    <div class="clients-item"><img src="/img/landing/relax.svg" alt="relax"></div>
                    <div class="clients-item"><img src="/img/landing/epam.svg" alt="epam"></div>
                    <div class="clients-item"><img src="/img/landing/milavitsa.svg" alt="milavitsa"></div>
                    <div class="clients-item"><img src="/img/landing/ittransition.svg" alt="ittransition"></div>
                    <div class="clients-item"><img src="/img/landing/24shop.svg" alt="24shop"></div>
                    <div class="clients-item"><img src="/img/landing/intellectsoft.svg" alt="intellectsoft"></div>
                </div>
            </div>
        </div>
        <div class="landing-item colored custom-padding">
            <div class="landing-item-inner">
                <h2>Более 10 лет работы на рынке<br>
                    подтверждают нашу компетентность</h2>
                <div class="clients-numbers">
                    <div class="clients-numbers-item">
                        <div class="numbers-top">1000 +</div>
                        <p>в нашем каталоге представлено более 1000 видов услуг</p>
                    </div>
                    <div class="clients-numbers-item">
                        <div class="numbers-top">600 +</div>
                        <p>мы  предлагаем более 600 вариантов сертификатов</p>
                    </div>
                    <div class="clients-numbers-item">
                        <div class="numbers-top">45 000 +</div>
                        <p>на нашем счету свыше 45 000 счастливых обладателей сертификатов</p>
                    </div>
                </div>
            </div>
        </div>
        <div id="contacts" class="landing-item">
            <div class="landing-item-inner">
                <h2>Связаться с нами</h2>
                <div class="form-wrap">
                    <div class="form-inner">
                        <h3>Оставьте заявку</h3>
                        <p>мы свяжемся с вами </p>
                        <? $this->widget('LandingFormBackCall'); ?>
                        <div class="policy">Нажимая на кнопку вы соглашаетесь с <a target="_blank" href="https://elpresent.by/files/politicy-elpresent.pdf">Политикой конфиденциальности</a></div>
                    </div>
                    <div class="contacts-inner">
                        <div class="contacts-row">
                            <div class="contacts-item">
                                <h4>Адрес</h4>
                                <p>г. Минск, ул. Куйбышева 22<br> Корпус 6, главный холл</p>
                            </div>
                            <div class="contacts-item">
                                <h4>Время работы</h4>
                                <p>пн-вс.: 10:00-20:00</p>
                            </div>
                        </div>
                        <div class="contacts-row">
                            <div class="contacts-item">
                                <h4>Телефон</h4>
                                <p><a href="tel:+375296878728">+375 (29) 687-87-28</a>,<br>
                                    <a href="tel:+375297678728">+375 (29) 767-87-28</a></p>
                            </div>
                            <div class="contacts-item color-white">
                                <h4>Email</h4>
                                <p><a href="mailto:info@elpresent.by">info@elpresent.by</a></p>
                            </div>
                            <div class="social-inner">
                                <a target="_blank" href="https://www.instagram.com/elpresent.by/"><img src="/img/landing/Instagram_white.svg" alt="relax"></a>
                                <a target="_blank" href="https://vk.com/elpresent_by"><img src="/img/landing/VK_white.svg" alt="relax"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer class="footer">
        <div class="copy">© 2022 «El Present» — оригинальные подарки — сервис подарочных сертификатов</div>
    </footer>
</div>
<div class="menu-wrap"></div>
<nav class="nav-landing-mob">
    <a class="page-scroll" href="#main">Главная</a>
    <a class="page-scroll" href="#advantage">Преимущества</a>
    <a class="page-scroll" href="#reviews">Отзывы</a>
    <a class="page-scroll" href="#catalog">Ассортимент</a>
    <a class="page-scroll" href="#delivery">Доставка</a>
    <a class="page-scroll" href="#clients">Клиенты</a>
    <a class="page-scroll" href="#contacts">Контакты</a>
</nav>
<div class="popup-wrap">

    <div class="popup-inner">
        <div class="close"></div>
        <div>Ваша заявка успешно отправлена</div>
    </div>
</div>
