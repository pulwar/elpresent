<article>
	<section>
		<table cellspacing="0" cellpadding="0" class="text-head">
			<tbody><tr>
				<td class="left"> </td>
				<td class="center">Новости</td>
				<td class="right"> </td>
			</tr>
		</tbody></table>
		<table cellspacing="0" cellpadding="0" width="100%">
			<tbody><tr>
				<td class="text-main-left"> </td>
				<td class="text-main">
				<? foreach($list as $each) : ?>
					<div class="news-block">
						<div class="gray-bg">
							<h2><i><?= $each->getDateFormated(); ?> </i><?= $each->title ?></h2>
								<?= nl2br($each->text) ; ?>
						</div>
					</div>
				<? endforeach; ?>
					<div class="pager">
						
					</div>
				</td>
				<td class="text-main-right"> </td>
			</tr>
		</tbody></table>
		<table cellspacing="0" cellpadding="0" class="text-bottom">
			<tbody><tr>
                <td class="left">&nbsp;</td>
                <td class="center">&nbsp;</td>
                <td class="right">&nbsp;</td>
			</tr>
		</tbody></table>
	</section>
</article>
