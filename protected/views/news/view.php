<script type="text/javascript" src="/js/jquery.selectbox-0.6.1.js"></script>
<script type="text/javascript" src="/js/jquery.jcarousel.min.js"></script>

		<table cellspacing="0" cellpadding="0" class="text-head-green">
			<tr>
				<td class="left">&nbsp;</td>
				<td class="center"><h1><?= $oNews->title ?></h1></td>
				<td class="right">&nbsp;</td>
			</tr>
		</table>
		<table cellspacing="0" cellpadding="0" width="100%">
			<tr>
				<td class="text-main-left">&nbsp;</td>
				<td class="text-main">
					<table style="width: 100%;">
						<tr>
							<td style="vertical-align: top; text-align: center; padding-top: 10px;">
							    <? if($oNews->full_img) : ?>
							        <img src="/upload/news/<?= $oNews->full_img ?>" width="200" />
							    <? endif; ?>
							</td>
							<td>
                    			<div class="gift-right news-right gray-bg">
                    				<?= $oNews->text; ?>
                    			</div>
                            </td>
                        </tr>
                    </table>
				</td>
				<td class="text-main-right">&nbsp;</td>
			</tr>
		</table>
		<table cellspacing="0" cellpadding="0" class="text-bottom">
			<tr>
				<td class="left">&nbsp;</td>
				<td class="center">&nbsp;</td>
				<td class="right">&nbsp;</td>
			</tr>
		</table>
