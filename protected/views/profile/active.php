<div class="description-text">
	<div class="col-md-12">
		<h1 class="main-title">Личный кабинет</h1>
		<? if(Yii::app()->user->hasFlash('success')):?>
			<div class="alert alert-success">
				<?=Yii::app()->user->getFlash('success'); ?>
			</div>
		<? endif; ?>
	</div>

	<div class="profile-content">
		<div class="col-md-9">

			<table class="profile-table table table-hover">
				<tr>
					<th>Дата</th>
					<th>Номер заказа</th>
					<th>Статус</th>
					<th>Название впечатления</th>
				</tr>
				<? foreach ($provider->getData() as $order): ?>
					<? /** @var Order $order */ ?>
					<tr>
						<td><?= substr($order->formatDate('order_date'), 0, strpos($order->formatDate('order_date'), ',')); ?></td>
						<td><?= $order->order_number; ?></td>
						<td><?= $order->getVerboseStatus(); ?></td>
						<td>
							<ul class="orderItmsListProfile">
							<?
							foreach ($order->orderItems as $orderItem) {
								?>
								<li>
									<?=$orderItem->getIsActivated() ? '<span style="color: green; cursor: pointer" title="Активирован">✔</span>' : '<span style="color: yellow; cursor: pointer" title="Не Активирован">⌛</span>'?>
									<a href="<?=path('catalog/item', ['perm_link'=>$orderItem->itemObj->perm_link])?>">
										<?=$orderItem->itemObj->title; ?>
									</a>
								</li>
								<?
							}
							?>
							</ul>

						</td>
					</tr>
				<?endforeach; ?>
			</table>

			<? if ($provider->itemCount == 0): ?>
				<div class="profile-alert">У Вас нет актуальных заказов</div>
			<? endif; ?>

		</div>

		<div class="col-md-3">
			<div class="profile-menu">
				<ul>
                    <li>Ваш баланс: <?=$user_all_info->cash > 0 ? $user_all_info->cash : '0.00'?> рублей</li>
					<li><a href="<?= path('profile/profile'); ?>">Профиль</a></li>
					<li class="active"><a href="<?= path('profile/order'); ?>">Текущие заказы</a></li>
					<li><a href="<?= path('profile/pending'); ?>">Ожидающие оплаты</a></li>
					<li><a href="<?= path('profile/history'); ?>">Архив заказов</a></li>
					<li><a href="<?= path('profile/logout'); ?>">Выход</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>