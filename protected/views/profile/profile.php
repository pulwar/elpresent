<div class="description-text">
	<div class="col-md-12">
		<h1 class="main-title">Личный кабинет</h1>
		<? if(Yii::app()->user->hasFlash('success')):?>
			<div class="alert alert-success">
				<?=Yii::app()->user->getFlash('success'); ?>
			</div>
		<? endif; ?>
	</div>

	<div class="profile-content">
		<div class="col-md-9">
            <h3>Данные пользователя</h3>            
            <hr>
			<?= $form; ?>
		</div>
		<div class="col-md-3">
			<div class="profile-menu">
				<ul>
					<li>Ваш баланс: <?=$user_all_info->cash > 0 ? $user_all_info->cash : '0.00'?> рублей</li>
					<li class="active"><a href="<?= path('profile/profile'); ?>">Профиль</a></li>
					<li><a href="<?= path('profile/order'); ?>">Текущие заказы</a></li>
					<li><a href="<?= path('profile/pending'); ?>">Ожидающие оплаты</a></li>
					<li><a href="<?= path('profile/history'); ?>">Архив заказов</a></li>
					<li><a href="<?= path('profile/logout'); ?>">Выход</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
