<? $this->pageTitle = Yii::app()->name . ' - Login'; ?>
<h1>Восстановление пароля</h1>
<p>
    <span class="label label-info">Внимание!</span> Если вы забыли свой пароль, мы можем создать новый и выслать вам на email под которым вы зарегестрированы на сайте. Далее вы можете изменить свой пароль в личном кабинете.
</p>
<div class="restore-password">
    <?=CHtml::beginForm(); ?>

    <?=CHtml::errorSummary($form); ?>

    <div class="row">
        <div class="col-md-12">
            <? if (Yii::app()->user->hasFlash('restore_password')): ?>
                <div class="alert alert-info">
                    <?=Yii::app()->user->getFlash('restore_password'); ?>
                </div>
            <? endif; ?>
        </div>
        <div class="col-md-3">
            <?=CHtml::activeEmailField($form, 'user_email', ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'Email для восстановления']) ?>
        </div>
        <div class="col-md-9">
            <input type="submit" class="btn btn-primary btn-sm" value="Восстановить"/>
        </div>
    </div>

    <?=CHtml::endForm(); ?>
</div>
