<!DOCTYPE html>
<html>
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PBTX93Z');</script>
    <!-- End Google Tag Manager -->
    <meta charset="utf-8">
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE"/>
    <meta name='yandex-verification' content='12f0fd413224d3e1'/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no"/>
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <script src="<?= Yii::app()->baseUrl . '/js/front_bootstrap/jquery-2.1.1.min.js' ?>"></script>
    <? Yii::app()->clientScript->registerCoreScript('cookie'); ?>
    <? Yii::app()->clientScript->scriptMap['jquery.min.js'] = false; ?>
    <? Yii::app()->clientScript->scriptMap['jquery.js'] = false; ?>
    <?
    if (isset($this->description)) {
        $page_description = htmlspecialchars(trim($this->description));
    } elseif (isset($GLOBALS['meta_description'])) {
        $page_description = htmlspecialchars(trim($GLOBALS['meta_description']));
    }
    ?>
    <? if (isset($page_description)): ?>
        <meta name="description" content="<?= $page_description ? $page_description : 'Подарки elpresent - это радость, впечатление и восторг! Заказать подарок прямо сейчас!'; ?>"/>
    <? endif; ?>
    <?
    if (isset($this->keywords)) {
        $page_keywords = htmlspecialchars(trim($this->keywords));
    } elseif (isset($GLOBALS['meta_keywords'])) {
        $page_keywords = htmlspecialchars(trim($GLOBALS['meta_keywords']));
    }
    ?>
    <? if (isset($page_keywords)): ?>
        <meta name="keywords" content="<?= $page_keywords; ?>"/>
    <? endif; ?>

    <? if (!empty($this->og_image)): ?>
        <meta property="og:image" content="<?= path('/', [], true) . '/' . $this->og_image; ?> "/>
    <? endif; ?>

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/>
    <link rel="canonical" href="<?=trim(url::get_url_without_pagination(url::curr('total', 'https', true)),'/')?>">

    <?
    if ($_SERVER['REQUEST_URI'] == '/podarki-14-fevralja') {
        $page_title = 'Подарки на 14 февраля l Подарки для мужчин и женщин в Минске';
    } elseif (!empty($this->itemTitle)) {
        $page_title = htmlspecialchars(trim($this->itemTitle));
    } elseif (!empty($GLOBALS['meta_title'])) {
        $page_title = htmlspecialchars(trim($GLOBALS['meta_title']));
    } else {
        $page_title = '«El Present» — оригинальные подарки. Впечатления в подарок.';
    }
    ?>
    <title><?= $page_title; ?></title>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300i,700|Open+Sans:300,400,600,700,700i,800&amp;subset=cyrillic" rel="stylesheet">
    <link href="/css/front_bootstrap/bootstrap.min.css" rel="stylesheet">
    <link href="/css/front_bootstrap/bootstrap-theme.min.css" rel="stylesheet">
    <link href="/css/slick.css" rel="stylesheet"/>
    <link href="/css/slick-theme.css" rel="stylesheet"/>
    <link href="/css/chosen.css" rel="stylesheet"/>
    <link href="/css/front_bootstrap/custom.css?v=1.0.4" rel="stylesheet">
    <link href="/css/front_bootstrap/star-rating.css" rel="stylesheet">
    <link href="/css/front_bootstrap/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="/js/ui/ui.min.css" rel="stylesheet">
    <link href="/css/front_bootstrap/lightbox.min.css" rel="stylesheet"/>
    <link href="/css/jquery-ui.css" rel="stylesheet"/>
    <link href="/css/front_bootstrap/bootstrap-formhelpers.min.css" rel="stylesheet"/>




</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PBTX93Z"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<? if(Yii::app()->user->hasFlash('main_error')):?>
    <div class="alert alert-danger main-error">
        <?=Yii::app()->user->getFlash('main_error'); ?>
    </div>
<? endif; ?>

<? if(url::is_page('skidki')):?>
    <? $this->widget('CategoriesDiscountsWidget', ['view' => 'Mobile']);?>
<? else: ?>
    <? $this->widget('CategoriesWidget', ['view' => 'Mobile']); ?>
<? endif;?>

<div class="header">
    <? $this->renderPartial('/layouts/header'); ?>
</div>

<div class="h_menu">
    <div class="h_menu_width">
        <div class="menu_top clearfix">
            <div class="menu_item_top menu_item_btn two">
                <div class="btnWrap">
                    <div class="toggle-menu-btn">
                        <span class="first"></span>
                        <span class="second"></span>
                        <span class="third"></span>
                    </div>
                    <span class="menu_btn_inner">Каталог <span class="mob_hide">подарков</span></span>
                </div>
            </div>
            <div class="menu_mob_pop two"><span>Популярные запросы</span>
                <div class="toggle-menu-btn">
                    <span class="first"></span>
                    <span class="second"></span>
                    <span class="third"></span>
                </div>
            </div>
            <? $this->widget('MenuPop', ['view' => 'Wrap']); ?>
        </div>
        <div class="h_menu_inner">

            <? if(url::is_page('skidki')):?>
            <? $this->widget('CategoriesDiscountsWidget', ['view' => 'Desctop']);?>
            <? else: ?>
            <? $this->widget('CategoriesWidget', ['view' => 'Desctop']); ?>
            <? endif;?>

        </div>
    </div>
</div>
<div class="container-fluid container-pale-orange zzz" style="display: none">
    <div class="container">
        <nav class="navbar navbar-inverse" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-mod">
                    <li><a class="item0 <?= url::active('contacts') ?>"
                           href="<?= path('pages/show', ['alias' => 'contacts']); ?>">О нас</a></li>
                    <li><a class="item1 <?= url::active('HowToActivate') ?>"
                           href="<?= path('pages/show', ['alias' => 'HowToActivate']); ?>">О подарке</a></li>
                    <li><a class="item2 <?= url::active('actions') ?>"
                           href="<?= path('pages/show', ['alias' => 'where-buy']); ?>">Где купить</a></li>
                    <li><a class="item3 <?= url::active('delivery') ?>"
                           href="<?= path("pages/show", ['alias' => 'delivery']); ?>">Доставка / Оплата</a></li>
                    <li><a class="item4" href="<?= path("site/landing"); ?>">Корпоративным клиентам</a></li>
                    <li><a class="item5 <?= url::active('gallery') ?>" href="<?= path("gallery/index"); ?>">Галерея</a>
                    </li>
                    <li><a class="item6 <?= url::active('contact') ?>" href="<?= path("contact"); ?>">Контакты</a>
                    </li>
<!--                    <li>-->
<!--                        <a class="item6 --><?//= url::active('skidki') ?><!--" href="--><?//= path("skidki/index"); ?><!--">Скидки</a>-->
<!--                    </li>-->
                </ul>
            </div>
        </nav>
    </div>
</div>

<div class="main_listing_inner">
    <div class="page_width">
        <div class="main_list">
            <div class="category_wrap">

                    <h4 class="with-border" style="margin: 0;">
                        <? $this->widget('AnoncerWidget'); ?>
                    </h4>

            </div>




            <!-- Везде кроме страницы скидок -->
            <? if(!url::is_page('skidki') && !url::is_page('profile')):?>
            <?/*
             <div class="sub_cat_wrap">
                 <div class="container_custom">
                     <a href="#" class="sub_cat_item">Название 1</a>
                     <a href="#" class="sub_cat_item">Название 2</a>
                     <a href="#" class="sub_cat_item active">Название 3</a>
                 </div>
             </div>
             */?>
             <? endif;?>

             <?= $content ?>
             <div class="widgets-bottom">

                 <? $this->widget('SocialWidget'); ?>
             </div>
             <? $this->widget('SeoContentWidget'); ?>
         </div>
     </div>
 </div>
 </div>
 </div>

 <div class="back_call">
     <div class="page_width">
         <div class="back_call_inner">
             <div class="back_call_title">Сложно определиться с подарком?</div>
             <div class="back_call_note">Заполните заявку и мы поможем Вам с выбором</div>
                 <form id="formHelp">
                     <div class="back_call_item">
                         <div class="back_label">ВАШЕ ИМЯ</div>
                         <input type="text" value="" name="LandingHelpForm[name]">
                     </div>
                     <div class="back_call_item">
                         <div class="back_label">НОМЕР ТЕЛЕФОНА</div>
                         <input id="phone_mask" name="LandingHelpForm[phone]" type="text" value="" placeholder="Телефон">
                         <!--<div class="back_phone">-->
                            <!--<span>+375</span><input type="text" value="">-->
                        <!--</div>-->
                    </div>
                    <div class="back_call_item">
                        <div class="back_label">ВРЕМЯ ЗВОНКА</div>
                        <div class="h_filter_numb">
                            <span>c:</span>
                            <input type="text" name="LandingHelpForm[time_from]">
                            <span>до:</span>
                            <input type="text" name="LandingHelpForm[time_to]">
                        </div>
                    </div>
                    <div class="back_call_cell">
                        <div class="back_call_item _big">
                            <div class="back_label">Комментарий</div>
                            <div class="h_filter_numb">
                                <textarea name="LandingHelpForm[comment]" placeholder="Нужен подарок другу на день рождение, до 100 рублей, желательно из активностей"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="back_call_btn_inner">
                        <div class="back_call_captcha">
                            <div class="g-recaptcha" data-sitekey="6Ld6L50UAAAAAPb3oTENdjFsdvdvd7anhZWyuDyJ"></div>
                        </div>
                        <button class="btn_list btn_green" type="submit">отправить запрос</button>
                    </div>
                    <div class="back_call_cell response_text">
                    </div>
                </form>
        </div>
    </div>
</div>
<? $this->widget('NewCommentWidget'); ?>
<div class="clearfix">
    <? $this->widget('SeoMainWidget'); ?>
</div>
<div class="adr_wrap">
    <div class="page_width">
        <div class="back_call_item">
            <div class="back_label">ОФИС</div>
            <select class="chosen-select" >
                <option>Минск</option>
            </select>
        </div>
        <div class="adr_item adr_ico">
            <div class="adr_item_title">Адрес</div>
            <div class="adr_item_info">г. Минск, ул. Куйбышева 22
                Корпус 6, главный холл</div>
        </div>
        <div class="adr_item time_ico">
            <div class="adr_item_title">Время работы</div>
            <div class="adr_item_info">пн-вс.: 10:00-20:00</div>
        </div>
        <div class="adr_item tel_ico">
            <div class="adr_item_title">телефоны</div>
            <div class="adr_item_info">
                <div><a href="tel:+375296878728">+375 (29) 687-87-28</a>,</div>
                <div><a href="tel:+375297678728">+375 (29) 767-87-28</a></div>
            </div>
        </div>
        <div class="adr_item email_ico">
            <div class="adr_item_title">email</div>
            <div class="adr_item_info">
                <div><a href="mailto:info@elpresent.by">info@elpresent.by</a></div>
            </div>
        </div>
    </div>
</div>

<? $this->renderPartial('/layouts/footer'); ?>

<!-- login -->
<div class="modal fade" id="myModalThree" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Close</span></button>
                <h4 class="modal-title custom-header custom-font" id="myModalLabel">Авторизация</h4>
            </div>
            <div class="modal-body">
                <div class="modal-inputs">
                    <form role="form" class="button-center form-top" method="POST"
                          action="<?= path('profile/login'); ?>">
                        <div class="form-group form-custom">
                            <a href="/profile/VkAuth" title="Авторизация через Вкнотакте">
                                <img src="/img/front_bootstrap/b.png" alt="Вкнотакте">
                            </a>
                            <a href="/profile/FsAuth" title="Авторизация через Facebook">
                                <img src="/img/front_bootstrap/facebook.png" alt="facebook">
                            </a>
                        </div>
                        <div class="form-group form-custom">
                            <input type="email" class="form-control" placeholder="Email"
                                   name="email" required>
                        </div>

                        <div class="form-group form-custom">
                            <input type="password" class="form-control" placeholder="Пароль"
                                   name="password" required>
                        </div>

                        <p><a href="/profile/restore">Забыл пароль</a></p>

                        <button type="submit" class="btn btn-primary btn-midd">Войти</button>
                        <!--<p><a href="#">Восстановить пароль?</a></p>-->
                    </form>
                </div>
            </div>
            <div class="modal-bottom"></div>
        </div>
    </div>
</div>

<!-- registration -->
<div class="modal fade" id="myModalTwo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                        class="sr-only">Close</span></button>
                <h4 class="modal-title custom-header custom-font" id="myModalLabel">Регистрация</h4>
            </div>
            <div class="modal-body">
                <div class="modal-inputs">
                    <div class="top-form"></div>
                    <form role="form" class="button-center" method="POST" action="<?= path('profile/register'); ?>">
                        <div class="form-group form-custom">
                            <input type="text" class="form-control" name="first_name"
                                   placeholder="*Ваше имя">
                        </div>
                        <div class="form-group form-custom">
                            <input type="text" class="form-control" name="last_name"
                                   placeholder="*Ваша фамилия">
                        </div>

                        <div class="form-group form-custom">
                            <div class="row" id="sandbox-container">
                                <label class="col-xs-6 control-label" for="">*Дата рождения:</label>

                                <div class="col-xs-6">
                                    <input class="form-control datepicker birthday" name="birthday" type="input"
                                           placeholder="1974-12-30"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group form-custom">
                            <input type="email" class="form-control" name="email"
                                   placeholder="*Email">
                        </div>

                        <div class="form-group form-custom">
                            <input type="password" class="form-control" name="password"
                                   placeholder="*Пароль">
                        </div>

                        <div class="form-group form-custom">
                            <input type="password" class="form-control" type="password" name="password_confirm"
                                   placeholder="*Подтвердите пароль" required>
                        </div>

                        <button type="submit" class="btn btn-primary btn-midd">Зарегистрироваться</button>
                    </form>
                </div>

            </div>
            <div class="modal-bottom"></div>
        </div>

    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Оставить отзыв</h4>
            </div>
            <div class="modal-body">
                <div class="modal-inputs">
                    <div class="stars">
                        <p>
                            <span>Ваша оценка - </span>
                            <input id="stars" value="0" type="number" class="rating" min=0 max=5 step=1 data-size="xss" data-stars="5">
                        </p>
                    </div>
                    <form role="form" id="_contact_comment_form" name="contact-comment-form" action="/contacts" method="post">
                    <input type="hidden" id="comment-rating" name="Contact[contact_rating]"
                           value="<?= ($oContact->contact_rating) ? $oContact->contact_rating : 5; ?>"/>

                    <div class="gobot" style="display: none">
                        <input name="Contact[gobot]" type="text" class="gobot">
                    </div>
                    <div class="row">
                        <div class="form-group col-xs-8 form-custom">
                            <input name="Contact[contact_name]" required type="text" class="form-control"  placeholder="Имя"
                                   value="<?
                                   if (isset($oContact->contact_name) && $oContact->contact_name) {
                                       echo $oContact->contact_name;
                                   } elseif (Yii::app()->user->getId() && Yii::app()->user->getUser()->user_firstname) {
                                       echo Yii::app()->user->getUser()->user_firstname;
                                   }
                                   ?>">
                        </div>
                        <div class="form-group col-xs-8 form-custom">
                            <input name="Contact[contact_email]" required type="email" class="form-control"
                                   placeholder="Email" value="<?
                            if (isset($oContact->contact_email) && $oContact->contact_email) {
                                echo $oContact->contact_email;
                            } elseif (Yii::app()->user->getId() && Yii::app()->user->getUser()->user_email) {
                                echo Yii::app()->user->getUser()->user_email;
                            }
                            ?>">
                        </div>
                        <div class="form-group col-xs-8 form-custom">
                            <input name="Contact[contact_subject]" required type="text" class="form-control"
                                   placeholder="Тема" value="<?= $oContact->contact_subject; ?>">
                        </div>
                    </div>
                    <textarea required name="Contact[contact_body]" class="form-control" rows="3"
                              placeholder="Текст"><?= $oContact->contact_body; ?></textarea>
                    <button type="submit" class="btn btn-primary btn-midd">Отправить</button>
                    </form>
                </div>
            </div>
            <div class="modal-bottom"></div>
        </div>
    </div>
</div>

<? //if(url::is_localhost()):?>
<? Yii::app()->getClientScript()->registerScriptFile(Yii::app()->baseUrl . '/js/functions.js', CClientScript::POS_END); ?>
<? Yii::app()->getClientScript()->registerScriptFile(Yii::app()->baseUrl . '/js/front_bootstrap/bootstrap.min.js', CClientScript::POS_END); ?>
<? Yii::app()->getClientScript()->registerScriptFile(Yii::app()->baseUrl . '/js/front_bootstrap/bootstrap-datepicker.min.js', CClientScript::POS_END); ?>

<? Yii::app()->getClientScript()->registerScriptFile(Yii::app()->baseUrl . '/js/front_bootstrap/star-rating.min.js', CClientScript::POS_END); ?>
<? Yii::app()->getClientScript()->registerScriptFile(Yii::app()->baseUrl . '/js/front_bootstrap/jquery.groupinputs.min.js',
    CClientScript::POS_END); ?>
<? Yii::app()->getClientScript()->registerScriptFile(Yii::app()->baseUrl . '/js/front_bootstrap/app.js?v1.0.4', CClientScript::POS_END); ?>
<? Yii::app()->getClientScript()->registerScriptFile(Yii::app()->baseUrl . '/js/front_bootstrap/lightbox.min.js', CClientScript::POS_END); ?>
<? if (Yii::app()->user->role == 'admin'): ?>
    <? Yii::app()->getClientScript()->registerScriptFile(Yii::app()->baseUrl . '/js/sound_message.js', CClientScript::POS_END); ?>
<? endif; ?>
<? //else:?>
<? //Yii::app()->getClientScript()->registerScriptFile(Yii::app()->baseUrl.'/js/front_bootstrap/prodaction.min.js',CClientScript::POS_END);?>
<? //endif;?>
<? Yii::app()->getClientScript()->registerScriptFile(Yii::app()->baseUrl . '/js/front_bootstrap/pack.js', CClientScript::POS_END); ?>
<? Yii::app()->getClientScript()->registerScriptFile(Yii::app()->baseUrl . '/js/front_bootstrap/bootstrap-formhelpers.min.js', CClientScript::POS_END); ?>



<? if (Yii::app()->user->role == 'admin'): ?>
    <div id="orderInfo">
        <a href="javascript:void(0);" class="close">x</a>
        <a href="/admin">
            <p><span class="label label-danger">Внимание!</span> новый заказ</p>
            <section></section>
        </a>
    </div>
<? endif; ?>

<script src='https://www.google.com/recaptcha/api.js'></script>
<script src="/js/jquery-ui/jquery-ui.js" type="text/javascript"></script>
<script src="/js/jquery.touchSwipe.min.js" type="text/javascript"></script>
<script src="/js/slick.min.js" type="text/javascript"></script>
<script src="/js/chosen.jquery.min.js" type="text/javascript"></script>
<script src="/js/jquery.inputmask.bundle.min.js" type="text/javascript"></script>
<script src="/js/jquery.inputmask-multi.min.js" type="text/javascript"></script>
<script src="/js/libs.js" type="text/javascript"></script>
<? Yii::app()->getClientScript()->registerScriptFile(Yii::app()->baseUrl . '/js/jquery.lazy.min.js', CClientScript::POS_END); ?>
<script src="/js/main.js?v=456" type="text/javascript"></script>
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
</body>
</html>