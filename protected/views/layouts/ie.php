<? header('Content-type: text/html; charset=UTF-8');?>
<!DOCTYPE HTML>
<html>
<head>
    <title>Старая версия браузера!</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" type="text/css" href="/css/ie/css/style.css">
    <link rel="icon" href="/css/ie/img/favicon.ico" type="image/x-icon">
    <link rel="SHORTCUT ICON" href="/css/ie/img/favicon.ico">
</head>
<body>
<div id="header">
    <div class="content">

    </div>
</div>
<div id="main">
<?=$content;?>
</div>
<div id="footer">
    <div class="content"></div>
</div>
</body>
</html>
