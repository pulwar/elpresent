<div class="h_nav">
	<div class="page_width">
		<div class="container_custom clearfix">
			<div class="h_login_wrap">
				<a href="/search?off_price=1" class="stock_cell"><span>Акции</span></a>
				<a href="/delivery" class="delivery_cell">Доставка и оплата</a>
				<div class="login_cell <?if(Yii::app()->user->getId()):?>active_prof<?endif;?>">
                    <div class="login_cell_ico"></div>
                    <span class="login_enter">
                        <span class="login_enter_none">
                            <?if(Yii::app()->user->getId()):?>
                            <?else:?>
                                Вход
                            <?endif;?>
                        </span> <i></i>
                    </span>
					<div class="login_all">
						<div class="close_ico"></div>
						<?if(Yii::app()->user->getId()):?>
                            <div>
                                <a href="/profile/profile" class=""><?=Yii::app()->user->getFullName()?></a>
                            </div>
                            <div><a href="/profile/logout" class="">Выйти</a></div>
                        <?else:?>
                            <div><button type="button" class="" data-toggle="modal" data-target="#myModalThree">Войти</button></div>
                            <div><a href="/partner/login" class="">Кабинет партнера</a></div>
                            <div><button type="button" class="" data-toggle="modal" data-target="#myModalTwo">Регистрация</button></div>
					    <?endif;?>
                    </div>
				</div>
                <?= $this->renderPartial('/cart/_header_cart'); ?>

			</div>
			<div class="h_contacts">
				<div class="h_menu_sm">
					<div class="h_menu_sm_btn">
						<div class="toggle-menu-btn">
							<span class="first"></span>
							<span class="second"></span>
							<span class="third"></span>
						</div>
					</div>
					<div class="h_menu_sm_wrap">
						<div class="menu_fixed_scroll">
							<!--<div class="h_menu_sm_item item_menu_mob">-->
							<div class="h_menu_sm_item">
								<span class="h_menu_sub_item">Каталог подарков <i></i></span>
								<div class="sub_menu">
									<div class="sub_menu_mob">
										<? if(url::is_page('skidki')):?>
										<? $this->widget('CategoriesDiscountsWidget', ['view' => 'Desctop']);?>
										<? else: ?>
										<? $this->widget('CategoriesWidget', ['view' => 'Desctop']); ?>
										<? endif;?>
									</div>
								</div>
							</div>
							<div class="h_menu_sm_item">
							<!--<div class="h_menu_sm_item item_menu_mob">-->
								<span class="h_menu_sub_item">Популярные запросы <i></i></span>
								<div class="sub_menu">
									<div class="sub_menu_mob">
										<!--<a href="/search?options%5B0%5D=0&features%5B0%5D=4&prices%5B0%5D=&prices%5B1%5D=&fast_search_type=3" class="menu_item_top two"><span class="menu_img_wrap"><img src="/images/category/flower.svg" alt="Летние подарки"></span><span>Для неё</span> <span class="badge">14</span></a>-->
										<a href="/search?options%5B0%5D=0&features%5B0%5D=0&prices%5B0%5D=&prices%5B1%5D=&is_new=1&fast_search_type=2" class="menu_item_top two"><span class="menu_img_wrap"><img src="/images/category/flower.svg" alt="Летние подарки"></span><span>Новинки</span></a>
										<a href="/search?options%5B0%5D=0&features%5B0%5D=0&prices%5B0%5D=&prices%5B1%5D=&fast_search_type=1" class="menu_item_top two"><span class="menu_img_wrap"><img src="/images/category/rings.svg" alt="Свадебные подарки"></span><span>Повод</span></a>
										<a href="/search?options%5B0%5D=0&features%5B0%5D=4&prices%5B0%5D=&prices%5B1%5D=&fast_search_type=3" class="menu_item_top two"><span class="menu_img_wrap"><img src="/images/category/flower.svg" alt="Летние подарки"></span><span>Для неё</span></a>
										<a href="/search?options%5B0%5D=0&features%5B0%5D=5&prices%5B0%5D=&prices%5B1%5D=&fast_search_type=4" class="menu_item_top two"><span class="menu_img_wrap"><img src="/images/category/car-presents.svg" alt="Для автомобилистов"></span><span>Для него</span></a>
										<a href="/search?options%5B0%5D=0&features%5B0%5D=6&prices%5B0%5D=&prices%5B1%5D=&fast_search_type=5" class="menu_item_top two"><span class="menu_img_wrap"><img src="/images/category/kolyaska.svg" alt="Для детей и родителей"></span><span>Для двоих</span></a>
										<a href="/search?options%5B0%5D=0&features%5B0%5D=20&prices%5B0%5D=&prices%5B1%5D=&fast_search_type=6" class="menu_item_top two"><span class="menu_img_wrap"><img src="/images/category/kolyaska.svg" alt="Для детей и родителей"></span><span>Для детей</span></a>
										<a href="/search?options%5B0%5D=0&features%5B0%5D=7&prices%5B0%5D=&prices%5B1%5D=&fast_search_type=7" class="menu_item_top two"><span class="menu_img_wrap"><img src="/images/category/Flag_alt.svg" alt="Активный отдых"></span><span>Для компаний</span></a>
									</div>
								</div>
							</div>
							<div class="h_menu_sm_item">
								<a href="/contacts">О нас</a>
							</div>
							<div class="h_menu_sm_item">
								<a href="/HowToActivate">О подарке</a>
							</div>
							<div class="h_menu_sm_item">
								<a href="/where-buy">Где купить</a>
							</div>
							<div class="h_menu_sm_item">
								<a href="/delivery">Доставка / Оплата</a>
							</div>
							<div class="h_menu_sm_item">
								<a href="#" data-toggle="modal" data-target="#myModalFour">Узнать о подарке</a>
							</div>
							<div class="h_menu_sm_item">
								<a href="/site/landing">Корпоративным клиентам</a>
							</div>
							<div class="h_menu_sm_item">
								<a href="/gallery">Галерея</a>
							</div>
							<div class="h_menu_sm_item">
								<a href="/contact">Контакты</a>
							</div>
						</div>
					</div>
				</div>
				<!--<div class="h_city">-->
				<!--<? $this->widget('CityMenuWidget'); ?>-->
				<!--</div>-->
				<div class="h_phone">
					<!--<?= arr::arrInStr(Yii::app()->params['phones']) ?>-->
					<span class="login_enter"><span class="h_phone_ico"></span><span class="tel_mob_none">+375 (29) 687-87-28</span><i></i></span>
					<div class="h_phone_all">
						<div class="close_ico"></div>
						<div class="h_contacts_title">
							<a href="/contacts">Контактная информация</a>
						</div>
						<div class="h_contacts_block">
							<div><a href="tel:+375296878728"><img src="/img/front_bootstrap/a1.svg" alt="">+375 (29) 687-87-28</a></div>
							<div><a href="tel:+375297678728"><img src="/img/front_bootstrap/MTS.svg" alt="">+375 (29) 767-87-28</a></div>
							<div><a href="mailto:info@elpresent.by"><img src="/img/front_bootstrap/email_ico.svg" alt="">info@elpresent.by</a></div>
						</div>
						<div class="h_contacts_title">
							<span>Не дозвониться? <a href="#" data-toggle="modal" data-target="#myModalSix">Заказать звонок</a></span>
						</div>
						<div class="h_contacts_block _separator">
							<div class="">Работаем ежедневно c 10 до 20</div>
							<div class="">г. Минск, ул. Куйбышева 22</div>
						</div>
						<div class="h_contacts_title">
							Онлайн-консультации:
						</div>
						<div class="h_contacts_block">
							<div class="h_online_item h_tel_ico"><a href="https://t.me/elpresent_by">elpresent_by</a></div>
							<div class="h_online_item h_vib_ico"><a href="viber://chat?number=375296878728">viber</a></div>
							<div class="h_online_item h_mail_ico"><a href="mailto:info@elpresent.by">info@elpresent.by</a></div>
						</div>
						<div class="h_social">
							<a class="social_item" href="https://www.facebook.com/elpresent.by/">
								<img src="/img/front_bootstrap/facebook.svg" alt="">
							</a>
							<a class="social_item" href="https://vk.com/club17973632">
								<img src="/img/front_bootstrap/vkontakte.svg" alt="">
							</a>
							<a class="social_item" href="https://www.instagram.com/elpresent.by/">
								<img src="/img/front_bootstrap/instagram.svg" alt="">
							</a>
							<a class="social_item" href="https://twitter.com/elpresentby">
								<img src="/img/front_bootstrap/twitter.svg" alt="">
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<? $this->widget('NewSearchWidget'); ?>


<div class="header" style="display: none;">

	<div class="col-md-6">

			<!--<div class="header-buttons">-->

				<!--<a class="btn btn-default" href="#" data-toggle="modal"-->
				   <!--data-target="#myModalSix">Заказать звонок</a>-->
			<!--</div>-->
		</div>
</div>
	<? if ((url::is_front())): ?>
	<div class="col-md-3" style="display: none;">
		<!--<? $this->widget('ProfileWidget') ?>-->
        <!--<? //$this->widget('ActionWidget'); ?>-->
	</div>
	<? endif; ?>

	<? if (!(url::is_front())): ?>
	<div class="col-md-3 mob-hidden" style="display: none;">
		<!--<? $this->widget('ProfileWidget') ?>-->
        <!--<? //$this->widget('ActionWidget'); ?>-->
	</div>
	<? endif; ?>



<!-- activation step 1 -->
<div class="modal fade" id="myModalFour" tabindex="-1" role="dialog"
	 aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span
						aria-hidden="true">&times;</span><span class="sr-only">Close</span>
				</button>
				<h4 class="modal-title custom-header custom-font"
					id="myModalLabel">Информация о подарке</h4>
			</div>
			<div class="modal-body">
				<div class="modal-inputs">
					<div class="activation-content">
						<p>Если у вас в руках подарок Elpresent половина радости
							уже в кармане. Осталось получить вторую половину,
							ваше Впечатление:</p>

						<div class="activation-content-bg">
							<div class="activation-icon">
								<img
									src="/img/front_bootstrap/catalog-icons/Mobile.svg"
									alt="">
							</div>
							<div class="activation-text">
								Для получения информации Вы можете просто позвонить нам.
								Наши телефоны:<br>
								<span>
									<a href="tel:+375296878728">+375 (29) 687-87-28</a><br>
									<a href="tel:+375297678728">+375 (29) 767-87-28</a>
								</span>
							</div>
						</div>

						<div class="activation-content-bg">
							<div class="activation-icon">
								<img class="card"
									 src="/img/front_bootstrap/catalog-icons/card.svg"
									 alt="">
							</div>
							<div class="activation-text">
								А можете взять сертификат, и вписать его индивидуальный номер в форму ниже
							</div>
						</div>
						<div class="arrow-down"></div>
					</div>
					<div class="alert alert-info" id="answer_false" role="alert"
						 style="display: none"></div>
					<form role="form" class="button-center form-activ"
						  id='itemform'>
						<div class="activation-inputs">
							<div class="inpt">
								<input type="text" class="form-control number"
									   placeholder="0000" name='code[0]'
									   maxlength="4" min="0" pattern="[0-9]*" onKeyPress="return numbersonly(this, event)">
								<input type="text" class="form-control number"
									   placeholder="0000" name='code[1]'
									   maxlength="4" min="0" pattern="[0-9]*" onKeyPress="return numbersonly(this, event)">
								<input type="text" class="form-control number"
									   placeholder="0000" name='code[2]'
									   maxlength="4" min="0" pattern="[0-9]*" onKeyPress="return numbersonly(this, event)">
							</div>
						</div>
						
						<button type="button" class="btn btn-primary btn-midd"
								onclick='activateItem();'>Подтвердить
						</button>
						<!--<p><a href="#">Проблемы с активацией?</a></p>-->
					</form>

				</div>
			</div>
			<div class="modal-bottom"></div>
		</div>
	</div>
</div>
<!-- activation step 2 -->
<div class="modal fade bs-example-modal-lg" id="myModalEight" tabindex="-1"
	 role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span
						aria-hidden="true">&times;</span><span class="sr-only">Close</span>
				</button>
				<h4 class="modal-title custom-header custom-font"
					id="myModalLabel">Активация</h4>
			</div>
			<div class="modal-body">
				<div class="modal-inputs">
					<div class="activation-gift">
						<h4>Вам подарили:
							<a href="#" target="_blank"
							   id='item_title_activation_popup'></a>
						</h4>

						<p class="code-activation">Код активации: <i
								id='item_code_activation_popup'></i></p>

						<p>Спасибо, что выбрали El Present! Мы дарим нашим
							клиентам незабываемые ощущения и качественный
							сервис.</p>

						<div class="yes bg-success active-gift" style="display: none"
							 id='ok_activation'>Ваш подарок успешно активирован.
						</div>
						<div class="no bg-warning active-gift" style="display: none"
							 id='error_activation'></div>
					</div>
					<form role="form" class="button-center form-top"
						  id='activation_popup_form'>
						<input type='hidden' name='code' value=''
							   id='activation_popup_code'/>

						<div class="form-group form-custom superimpressions">
							<select name='activated_item'
									id='activation_popup_activated_item'
									class="form-control select-font"></select>
						</div>

						<div class="form-group form-custom">
							<input type="text" name='recivier_name'
								   class="form-control" required
								   placeholder="Введите имя:">
						</div>

						<div class="form-group form-custom">
							<input type="text" name='recivier_phone'
								   class="form-control" required
								   placeholder="Введите номер телефона:">
						</div>

						<div class="form-group form-custom">
							<input type="email" name='recivier_email'
								   class="form-control" required
								   placeholder="Введите e-mail:">
						</div>

						<div id="activation-info" class="impression-coord clearfix" style="display:none; text-align: left">
							<p>Свяжитесь с нашим партнером и забронируйте время
								впечатления.</p>

							<div class="impression-coord clearfix"
							<div class="text-coord">
								<p>
									<strong>Ваше впечатление проходит:</strong>
									<span id="activation-partner-place"></span>
								</p>

								<p>
									<strong>Где находится:</strong>
									<span id="activation-partner-address"></span>
								</p>

								<p>
									<strong>Контактное лицо: </strong>
									<span id="activation-partner-contact"></span>
								</p>

							</div>
							<div class="map-coord">
								<span id="activation-map"></span>
							</div>
						</div>


						<button type="button" onclick="submitPopup(this);"
								class="btn btn-primary btn-midd">Активировать
						</button>
					</form>
				</div>
			</div>
			<div class="modal-bottom"></div>
		</div>
	</div>
</div>


<!-- order call -->
<div class="modal fade" id="myModalSix" tabindex="-1" role="dialog"
	 aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span
						aria-hidden="true">&times;</span><span class="sr-only">Close</span>
				</button>
				<h4 class="modal-title custom-header custom-font"
					id="myModalLabel">Заказать звонок</h4>
			</div>
			<div class="modal-body">
				<div class="modal-inputs">
					<? $this->widget('LandingFormBackCall'); ?>
				</div>
			</div>
			<div class="modal-bottom"></div>
		</div>
	</div>
</div>
