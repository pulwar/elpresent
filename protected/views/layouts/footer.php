<div class="footer">
    <div class="page_width">
        <div class="f_inner">
            <div class="logo_footer">
                <a href="/">
                    <img src="/img/front_bootstrap/h_logo1.svg" alt="El Present">
                </a>
            </div>
            <div class="f_cell_1">
                <p><a href="<?= path('site/contact'); ?>" rel="nofollow">Наш сервис</a></p>
                <p><a href="<?= path('pages/show', ['alias' => 'agreement']); ?>" rel="nofollow">Публичная оферта</a></p>
                <p><a href="<?= path('contact'); ?>" rel="nofollow">Контактная информация</a></p>
                <p><a href="<?= path('pages/show', ['alias' => 'cooperation']); ?>" rel="nofollow">Сотрудничество</a></p>
                <p><a href="<?= path('pages/show', ['alias' => 'partners']); ?>" rel="nofollow">Наши партнеры</a></p>
                <p><a href="<?= path('site/landing'); ?>" rel="nofollow">Корпоративные клиенты</a></p>
                <p><a href="/where-buy">Где купить</a></p>
                <p><a href="#" data-toggle="modal" data-target="#myModalFour">Узнать о подарке</a></p>
                <p><a href="/delivery">Доставка и оплата</a></p>
                <p><a href="/files/politicy-elpresent.pdf">Политика конфиденциальности</a></p>
                
            </div>
            <!--<div class="f_cell_2">-->
                <!--<p><a href="#">Подарки для неё</a></p>-->
                <!--<p><a href="#">Подарки для неё</a></p>-->
                <!--<p><a href="#">Подарки для неё</a></p>-->
                <!--<p><a href="#">Подарки для неё</a></p>-->
                <!--<p><a href="#">Подарки для неё</a></p>-->
                <!--<p><a href="#">Подарки для неё</a></p>-->
            <!--</div>-->
            <!--<div class="f_cell_2">-->
                <!--<p><a href="#">Подарки для неё</a></p>-->
                <!--<p><a href="#">Подарки для неё</a></p>-->
                <!--<p><a href="#">Подарки для неё</a></p>-->
                <!--<p><a href="#">Подарки для неё</a></p>-->
                <!--<p><a href="#">Подарки для неё</a></p>-->
                <!--<p><a href="#">Подарки для неё</a></p>-->
            <!--</div>-->
            <div class="f_cell_last">
                <!--<div class="about">-->
                    <p>ИП Калиновский Виталий Степанович</p>
                    <p>УНП 193426225</p>
                    <p>Зарегистрирован Минским Горисполкомом <br> Республики Беларусь 1 июня 2020 г.</p>
                    <p>220141, г.Минск, ул. Ф. Скорины, 45-33</p>
                    <p>ЗАО "МТБанк", БИК MTBKBY22</p>
                    <p>р/с BY95MTBK30130001093300058924</p>
                    <p>Почтовый адрес:</p>
                    <p>220029, г.Минск, ул. Куйбышева 22-113</p>
                    <p><a href="mailto:info@elpresent.by">info@elpresent.by</a></p>
                <!--</div>-->
            </div>
        </div>
        <div class="f_social clearfix">
            <div class="f_social_inner">
                <a class="social_item" href="https://www.facebook.com/elpresent.by/">
                    <img src="/img/front_bootstrap/facebook.svg" alt="">
                </a>
                <a class="social_item" href="https://vk.com/club17973632">
                    <img src="/img/front_bootstrap/vkontakte.svg" alt="">
                </a>
                <a class="social_item" href="https://www.instagram.com/elpresent.by/">
                    <img src="/img/front_bootstrap/instagram.svg" alt="">
                </a>
                <a class="social_item" href="https://twitter.com/elpresentby">
                    <img src="/img/front_bootstrap/twitter.svg" alt="">
                </a>
                <a class="social_item" href="https://t.me/elpresent_by">
                    <img src="/img/front_bootstrap/Telegam_gray.svg" alt="">
                </a>
            </div>
            <div class="footer-copyright">
                &copy; <?= date('Y') ?> «El Present» — оригинальные подарки — сервис подарочных сертификатов
            </div>
        </div>
    </div>
    <div class="pay_logos">
        <img src="/img/front_bootstrap/card/visa-logo_new.png" alt="">
        <!--img src="/img/front_bootstrap/card/visa_electron.png" alt="">-->
        <img src="/img/front_bootstrap/card/verified_by_visa.png" alt="">
        <!--img src="/img/front_bootstrap/card/maestro-logo_new.png" alt="">-->
        <img src="/img/front_bootstrap/card/mcard-logo_new.png" alt="">
        <img src="/img/front_bootstrap/card/master_card_secure_code.png" alt="">
        <img src="/img/front_bootstrap/card/belkart-logo.png" alt="">
        <img src="/img/front_bootstrap/card/belcart-logo-ip.png" alt="">
        <a href="https://bepaid.by"><img src="/img/front_bootstrap/card/bepaid.png" alt=""></a>
    </div>
</div>
<div class="btn_top"></div>