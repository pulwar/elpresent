<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="goper@tut.by">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon"/>
    <title>Сервис подарков, подарочных сертификатов в Минске</title>
    <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700&subset=cyrillic' rel='stylesheet' type='text/css'>
    <link href="/css/landing/bootstrap.css" rel="stylesheet">
    <link href="/css/landing/font-awesome.css" rel="stylesheet">
    <link href="/css/landing/scrolling-nav.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css" integrity="sha512-17EgCFERpgZKcm0j0fEq1YCJuyAWdz9KUtv1EjVuaOz8pDnh/0nZxmU6BBXwaaxqoi9PQXnRWqlcDB027hgv9A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css" integrity="sha512-yHknP1/AwR+yx26cB1y0cjvQUMvEa2PFzt1c9LlS4pRQ5NOTZFWbhBig+X9G9eYW/8m0/4OXNx8pxJ6z57x0dw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="/css/landing/main.css" rel="stylesheet">
</head>
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
<?=$content;?>
<!--<script src="/js/plugins/jquery.carouFredSel.js"></script>-->
<!--<script src="/js/landing/jquery.js"></script>-->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.0/jquery.js" integrity="sha512-c0S/pptXdIG65/JuW1Jbi5MR5E+5V0qSOowEDf7FVz0Tz7vi3O0z04Ov+SiRqM4Uap6wXlNbfxW77MqcX76PmA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>-->
<!--<script src="/js/landing/yiiActiveForm.js"></script>-->
<script src="/js/landing/bootstrap.js"></script>

<!--<script src=-->
<!--        "https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.js"-->
<!--        integrity=-->
<!--        "sha256-yE5LLp5HSQ/z+hJeCqkz9hdjNkk1jaiGG0tDCraumnA="-->
<!--        crossorigin="anonymous">-->
<!--</script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js" integrity="sha512-XtmMtDEcNz2j7ekrtHvOVR4iwwaD6o/FUJe6+Zq+HgcCsk3kj4uSQQR8weQ2QVj1o0Pk6PwYLohm206ZzNfubg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<!-- scrolling Nav -->
<script src="/js/landing/jquery.easing.min.js"></script>
<script src="/js/landing/scrolling-nav.js"></script>
<script src="/js/landing/main.js"></script>
</body>
</html>

