<?php
/** @var Item $item */
$count_prices = count($item->prices_cache[((int)$_SESSION['_city'])]);
?>
<div class="row catalog-item single_prod">
    <br/>
    <!--big img-->
    <!--<div class="single_prod_img">-->
        <!--<img src="/images/item/logo-1200x500.jpg" alt="ТЕСТОВЫЙ товар: Будь оптимистом!">-->
    <!--</div>-->
    <!--big img end-->
    <? $this->renderPartial('item/sidebar', ['item' => $item, 'numItem' => '1']); ?>
    <div class="col-md-9">
        <div class="select-social">
            <h1><?= $item->title ?></h1>
            <!--			--><? // if ($item->off_price != 0): ?>
            <!--				<h5 class="product-item-price">--><? //= number_format($item->getRoundedPrice()); ?><!-- руб.</h5>-->
            <!--				<div class="product-item-oldprice">--><? //= $item->getVariablePriceForView(); ?><!--</div>-->
            <!--			--><? // else: ?>
            <div class="product-item-price"><?= $item->getVariablePriceForView(); ?></div>
            <!--			--><? // endif; ?>
            <?
            $this->og_image = $item->full_img;
            $this->widget('ShareBlockWidget', [
                'title' => $item->title,
                'link' => path('catalog/want', ['hash' => $item->getPageHash()], true)
            ]);
            ?>

            <? if ($count_prices == 1): ?>
                <br><br>
                <a class="btn btn-primary buy-button <?= empty($item->available) ? 'hide' : '' ?>" href="<?= path('order/buyone', ['itemId' => $item->id]); ?>">Купить в подарок</a>
                <a class="btn btn-primary buy-button <?= empty($item->available) ? 'hide' : '' ?>" href="<?= path('order/buyone', ['itemId' => $item->id]); ?>">Купить для себя</a>

                <? if ($item->isDigitalCertificateAvailable()): ?>
                    <a class="btn btn-default buy-button <?= empty($item->available) ? 'hide' : '' ?>"
                       href="<?= path('order/buyone', ['itemId' => $item->id, 'type' => 'digital']); ?>">Электронный сертификат</a>
                <? endif; ?>
            <? endif ?>

            <div class="item-description">
                <? if ($item->type == Item::TYPE_GROUP): ?>
                <div class="gift-set" style="padding-bottom: 20px;">
                    <h3>В подарок входят:</h3><p>(необходимо выбрать одно впечатление из ниже предложенных)</p>
                    <div class="row">
                        <? foreach ($item->childs as $key => $child): ?>
                            <div class="col-md-4 gift-set-item">
                                <a target="_blank" href="<?= path('catalog/item', ['perm_link' => $child->perm_link]); ?>">
                                    <div class="gift-set-img">
                                        <span><?= ($key + 1) ?></span>
                                        <img src="/<?=$child->full_img; ?>" alt="<?=$item->title; ?>"/>
                                    </div>
                                    <?
                                    $selfLink = $item->getSelfLinkByChildren($child);
                                    $optionTitle = '';
                                    if (!empty($selfLink->option)) {
                                        $optionTitle = $selfLink->option->small_name;
                                    }
                                    ?>
                                    <p><?=$child->title; ?> <br><?=$optionTitle?></p>
                                </a>
                            </div>
                        <? endforeach; ?>
                    </div>
                </div>
                <? endif; ?>
                <h3>О подарке</h3>
                <?= $item->getText(); ?>
                <? if($item->type != Item::TYPE_GROUP) { ?>
                    <? $scenario = array_filter(explode("\n", $item->scenario), 'strlen'); ?>
                    <? if (!empty($scenario)): ?>
                        <h2>Сценарий</h2>
                        <ul class="custom-enumirable">
                            <? foreach ($scenario as $key => $text) : ?>
                                <li>
                                    <div class="numss"><?= $key + 1 ?></div><?= $text ?>
                                </li>
                            <? endforeach; ?>
                        </ul>
                    <? endif; ?>
                <? } ?>
                <? if ($count_prices > 1): ?>
                    <div class="item-variant">
                        <h3>Варианты</h3>

                        <? $this->renderPartial('_options', ['item' => $item]); ?>
                    </div>
                <? endif; ?>
                <script>
                    $(document).ready(function(){
                        $(document)
                            .on('click', '.buy-button.general, .buy-button.digital', function(e) {
                                e.preventDefault();
                                if($('.item-variant .price_choose')) {
                                    var checked = $('.item-variant .price_choose input[type=radio]:checked');
                                    var val = $(checked).val();
                                    window.document.location = $(this).attr('href') + '&optionId=' + val;
                                } else {
                                    window.document.location = $(this).attr('href');
                                }

                            });

                    });

                </script>

                <div class="item-bottom-buttons">
                    <a class="btn btn-primary buy-button general <?= empty($item->available) || $item->getVariablePriceForView() === false ? 'hide' : '' ?>"
                       href="<?= path('order/buyone', ['itemId' => $item->id]); ?>">Купить в подарок</a>
                    <a class="btn btn-primary buy-button general <?= empty($item->available) || $item->getVariablePriceForView() === false ? 'hide' : '' ?>"
                       href="<?= path('order/buyone', ['itemId' => $item->id]); ?>">Купить для себя</a>                       
                    <? if ($item->isDigitalCertificateAvailable()): ?>
                        <a class="btn btn-default buy-button digital <?= empty($item->available) || $item->getVariablePriceForView() === false ? 'hide' : '' ?>"
                           href="<?= path('order/buyone', ['itemId' => $item->id, 'type' => 'digital']); ?>">Электронный сертификат</a>
                    <? endif; ?>
                </div>
                <!-- h4><?= Yii::app()->params['add_text_in_gift1'] ?></h4 -->
            </div>
        </div>

        <?$this->renderPartial('/cart/_modal_item', ['item' => $item, 'numItem' => '1', 'count_prices' => $count_prices]); ?>


    </div>

</div>

<!-- хочу в подарок -->
<div class="modal fade" id="myModalFive" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title custom-header custom-font" id="myModalLabel">Хочу в подарок</h4>
            </div>
            <div class="modal-body">
                <div class="modal-inputs modal-window">
                    <p class="success-msg hide">Сообщение было отправлено</p>

                    <p class="error-msg  hide">При отправке письма произошла какая-то ошибка :(</p>

                    <form method="POST" role="form" class="button-center form-top" action="<?= path('site/wish'); ?>" data-submit="want-for-present">
                        <br>

                        <div class="form-group form-custom">
                            <input type="text" name="recipient_name" required class="form-control" id="exampleInputName" placeholder="Кому отправить?">
                        </div>

                        <div class="form-group form-custom">
                            <input type="email" name="recipient_email" required class="form-control" id="exampleInputEmail" placeholder="Куда отправить?">
                        </div>

                        <div class="form-group form-custom">
                            <input type="text" name="your_name" required class="form-control" id="exampleInputYourName" placeholder="Ваше имя">
                        </div>

                        <div class="form-group form-custom">
                            <input type="email" name="your_email" required class="form-control" id="exampleInputYourEmail" placeholder="Ваш email">
                        </div>
                        <input type="hidden" name="item_id" value="<?= $item->id; ?>"/>
                        <button type="submit" class="btn btn-primary btn-midd">Отправить</button>
                    </form>
                </div>
            </div>
            <div class="modal-bottom"></div>
        </div>
    </div>
</div>