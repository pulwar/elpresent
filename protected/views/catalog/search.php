<?$fast_search_type = Yii::app()->request->getParam('fast_search_type')?>
<?
    if($fast_search_type) {
        if($fast_search_type==1) {
            $text = 'Повод';
            $reasons = FeatureValue::model()->findAll(array(
                'select'=>'*',
                'condition'=>'`available`=:available AND `feature_id`=:feature_id',
                'params'=>array(':available' => 1, ':feature_id' => 7),
                'order'=> '`feature_value_value`',
            ));
        } else if($fast_search_type==2) {
            $text = 'Новинки';
        } else if($fast_search_type==3) {
            $text = 'Для нее';
        } else if($fast_search_type==4) {
            $text = 'Для него';
        } else if($fast_search_type==5) {
            $text = 'Для двоих';
        } else if($fast_search_type==6) {
            $text = 'Для детей';
        } else if($fast_search_type==7) {
            $text = 'Для компаний';
        } else if($fast_search_type== 'r-8') {
            $text = 'День рождения';
        } else if($fast_search_type== 'r-9') {
            $text = 'Мальчишник';
        } else if($fast_search_type== 'r-10') {
            $text = 'Девичник';
        } else if($fast_search_type== 'r-11') {
            $text = 'Свадьба';
        } else if($fast_search_type== 'r-12') {
            $text = 'Любимым';
        } else if($fast_search_type== 'r-13') {
            $text = 'Корпоративное мероприятие';
        } else if($fast_search_type== 'r-14') {
            $text = 'Юбилей';
        } else if($fast_search_type== 'r-15') {
            $text = '23 февраля';
        } else if($fast_search_type== 'r-16') {
            $text = '8 марта';
        } else if($fast_search_type== 'r-17') {
            $text = 'Новый год';
        } else if($fast_search_type== 'r-18') {
            $text = 'Бизнес-подарок';
        } else if($fast_search_type== 'r-19') {
            $text = 'День влюбленных';
        }
    }else{
        $text = 'Результаты поиска';
    }
?>
<div class="main-header main-header_title">
    <h1>
        <? if ($this->action->id != 'index') : ?>
            <?= $category->title ?  $category->title : $text?>
        <? endif; ?>
    </h1>
</div>
<?if($reasons):?>
    <div class="sub_cat_wrap">
        <div class="container_custom">
            <?foreach ($reasons as $reason):?>
                <a class="sub_cat_item" href="/search?options%5B0%5D=<?=$reason->feature_value_id?>&fast_search_type=r-<?=$reason->feature_value_id?>"><?=$reason->feature_value_value?></a>
            <?endforeach;?>
        </div>
    </div>
<?endif;?>


<?$count =  $list->getTotalItemCount(); ?>
<?if($count==0):?>
    <div class="search_no">По вашему запросу ничего не найдено. Попробуйте посмотреть в других разделах или изменить параметры поиска, либо обратитесь к нашим менеджерам.</div>
<?else:?>
    <div class="main-header_title">
        <div class="search_note">Найдено: <span><?=$list->getTotalItemCount()?></span> <?=Yii::t('yii', 'сертификат|сертификата|сертификатов', $count)?></div>
        <div class="list_sort">
            <div class="sort_select_wrap">
                <div class="sort_select_single" data-sort="t.`order` ASC">Сначала <span>Популярные</span> <i></i></div>
                <div class="sort_select_inner">
                    <div class="sort_select_item selectActive" data-sort="t.`order` ASC">Популярные</div>
                    <div class="sort_select_item" data-sort="ic.item_price ASC">Дешевые</div>
                    <div class="sort_select_item" data-sort="ic.item_price DESC">Дорогие</div>
                    <div class="sort_select_item" data-sort="t.is_new DESC, t.`order` ASC">Новые</div>
                </div>
            </div>
        </div>
    </div>
    <? $this->renderPartial("_search", compact("list", "count")); ?>
<?endif;?>
