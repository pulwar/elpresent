<? if(!empty($items)):?>
	<div class="row order-list">
		<div class="col-md-12">
			<div class="order-list-title clearfix">
				<h3 class="complect">Комплект</h3>
				<h3 class="qty">Количество эмоций в наборе: <span><?=count($items)?></span></h3>
			</div>

			<? $prices = []; ?>
			<? foreach ($items as $k => $item) { ?>
				<?
				$itemPrice = $item->getPrice();
				$prices[$item->id] = $itemPrice;
				?>
				<div class="order-list-item clearfix">
					<a href="javascript:void(0);" class="cart-delete-item" data-item_from_del_id="<?=$item->id?>">×</a>
					<img src="/<?=$item->preview_img?>"/>
					<h4><?=$item->title?></h4>
					<p><?=$itemPrice?> руб.</p>
				</div>
			<? } ?>
			<?
			$max_price = max($prices);
			$id = array_search($max_price,$prices);
			?>

			<div class="order-list-total">
				<p>Стоимость: <span><?=($max_price)?> руб.</span></p>
				<?= CHtml::link('Купить комплект', ['/order/pack/item_id/'.$id], ['class' => 'btn btn-primary btn-midd cart-by']); ?>
			</div>
		</div>
	</div>
<? endif;?>