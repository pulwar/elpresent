<? if (empty($list)): ?>
        <div class="search_no">По вашему запросу ничего не найдено. Попробуйте посмотреть в других разделах или изменить параметры поиска, либо обратитесь к нашим менеджерам.</div>
<?else:?>

<div class="main_list container_custom">
    <div class="row_custom">
        <? foreach ($list->getData() as $k => $item) : ?>
            <? $count_prices = count($item->prices_cache[((int)$_SESSION['_city'])]); ?>
            <?$this->renderPartial('/catalog/_one_item', ['item' => $item, 'k' => $k, 'count_prices' => $count_prices]); ?>
        <?endforeach;?>
    </div>
</div>
<?if($count>12):?>
    <div class="btn_add">Показать еще</div>
<?endif;?>


<div class="row gift list"  style="display: none;">
    <? $count = count($list->getData()); ?>
    <? foreach ($list->getData() as $k => $item) : ?>
        <? $count_prices = count($item->prices_cache[((int)$_SESSION['_city'])]); ?>
        <? if (($k + 1) % 2 == 0): ?>
            <div class="row">
        <? endif; ?>
        <div class="col-md-6">
            <div class="product-item item">
                <h3><a href="<?= path('catalog/item', ['perm_link' => $item->perm_link]); ?>"><?= $item->title; ?></a></h3>

                <div class="product-item-content clearfix">
                    <div class="product-item-image">
                        <div class="product-item-image-inside">
                        <a href="<?= path('catalog/item', ['perm_link' => $item->perm_link]); ?>">
                            <div class="hidden-product-img">
                                <img src="/<?= $item->full_img ?>" alt="<?= $item->title; ?>"/>
                            </div>
                        </a>

                        <div class="product-item-image__shadow"></div>

                        <? if (!empty($item->is_season)): ?>
                            <div class="season-img"></div>
                        <? endif; ?>

                        <? if ($item->off_price == 0): ?>
                            <? if ($item->is_new): ?>
                                <div class="new-img"></div><? endif; ?>
                            <? if ($item->is_hot): ?>
                                <div class="hit-img"></div><? endif; ?>
                            <? if (!$item->available): ?>
                                <div class="not-available"></div><? endif; ?>
                        <? else: ?>
                            <div class="discount-img"><span><?= round($item->off_price); ?>%</span></div>
                        <? endif; ?>
                        <? if ($item->off_price != 0): ?>
                            <div class="off-price-lable">* цена указана без учета скидки</div>
                        <? endif; ?>
                        </div>
                        <div class="product-item-add">
                            <? if (!empty($item->is_kit)): ?>
                                <? if ($count_prices > 1): ?>

                                    <div class="modal fade option" id="myModalSeven<?= $k ?>" tabindex="-1" role="dialog"
                                         aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal"><span
                                                            aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                    <h4 class="modal-title custom-header custom-font" id="myModalLabel">В комплект</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="modal-inputs">
                                                        <div class="option-hover">
                                                            <? $this->renderPartial('_options', ['item' => $item]); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-12 col-md-offset-4">
                                                        <button type="button" class="btn btn-default add-to-kit-cart" data-id="<?= $item->id ?>" data-item-modal='#myModalItem<?= $item->id ?>'>
                                                            Добавить
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="modal-bottom"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--<span class="glyphicon glyphicon-plus"></span>-->
                                    <a class="shose-price <?= empty($item->t_available) ? 'hide' : '' ?>" data-toggle="modal" data-target="#myModalSeven<?= $k ?>" >В корзину</a>
                                <? else: ?>
                                    <!--<span class="glyphicon glyphicon-plus"></span>-->
                                    <a class="add-to-kit-cart <?= empty($item->t_available) ? 'hide' : '' ?>" data-id="<?= $item->id ?>" data-item-modal='#myModalItem<?= $item->id ?>'>В корзину</a>
                                <? endif; ?>
                                    <br>
                                <a class="btn-fast-order" data-toggle="modal" data-target="#fastOrderInList<?= $item->id; ?>"
                                   onClick="yaCounter17198173.reachGoal('odin_klik'); ga('send', 'pageview', '/virtual/fodin_klik');">Купить в 1 клик</a>


                            <? endif; ?>
                        </div>
                        <!--<div class="rating-block">
                            <label for="input-8-xs" class="control-label"></label>
                            <input class="rating rating-loading input-8-xs" value="<?//$item->rating / $item->vote_count?>" data-min="0" data-max="5" data-step="1" data-size="xs" readonly>
                        </div>-->
                    </div>

                    <div class="product-item-description">
                        <span><?= $item->desc; ?></span>
                        <div class="product-item-price"><?= $item->getVariablePriceForView(); ?></div>
                        <div class="product-item-buy">
                            <? if ($count_prices > 1): ?>

                                <a class="btn btn-primary buy-button shose-price <?= empty($item->t_available) ? 'hide' : '' ?>" data-toggle="modal"
                                   data-target="#myModalSevenBy<?= $k ?>">Купить</a>

                                <div class="modal fade option" id="myModalSevenBy<?= $k ?>" tabindex="-1" role="dialog"
                                     aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                                        class="sr-only">Close</span></button>
                                                <h4 class="modal-title custom-header custom-font" id="myModalLabel">В корзину</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="modal-inputs">
                                                    <div class="option-hover">
                                                        <? $this->renderPartial('_options', ['item' => $item]); ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12 col-md-offset-4">
                                                    <button type="button" class="btn btn-default buy-quickly"
                                                            data-href="<?=path('order/buyone')?>" data-id="<?= $item->id ?>">Купить
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="modal-bottom"></div>
                                        </div>
                                    </div>
                                </div>
                            <? else: ?>
                                <a class="btn btn-primary buy-button <?= empty($item->t_available) ? 'hide' : '' ?>"
                                   href="<?=path('order/buyone', ['itemId' => $item->id])?>">Купить</a>
                            <? endif; ?>
                        </div>
                        <a class="btn btn-default more" href="<?= path('catalog/item', ['perm_link' => $item->perm_link]); ?>">Подробнее...</a>
                    </div>
                </div>
            </div>
            <?$this->renderPartial('/cart/_modal_item', ['item' => $item, 'numItem' => $k, 'count_prices' => $count_prices]); ?>
        </div>
        <? if (($k + 1) % 2 == 0 || $count == ($k + 1)): ?>
            </div>
            <hr class="product-separator">
        <? endif; ?>
    <? endforeach; ?>

    <? if (!Yii::app()->request->isAjaxRequest): ?>
    <? if ($_SERVER['REQUEST_URI'] == '/'): ?>
        <p class="product-memo">Для ознакомления с полным списком подарком воспользуйтесь поиском или выберите слева интересующую категорию.</p>
    <? endif; ?>

    <div class="row" style="clear: both">
        <div class="col-md-8">
            <? $this->widget('PagerWidget', array('pager' => $list->getPagination())); ?>
        </div>
        <div class="col-md-4">
            <? $this->widget('SelectionItemsWidget', ['type' => 'order']); ?>
        </div>
    </div>

    <? if (empty($count)): ?>
        <p>По вашему запросу ничего не найдено. Попробуйте посмотреть в других разделах или изменить параметры поиска, либо обратитесь к нашим менеджерам.</p>
    <? endif; ?>
<? endif; ?>
<? endif; ?>
