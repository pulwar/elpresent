<script>
$(document).ready(function() {

    $('body').on('click', 'div.pagination.ajax ul li a',function(e){
        e.preventDefault();
        var form = $('#kit-filters').serialize();
        var page = $.trim($(this).text());
        var res = form + '&page=' + page;
        $.ajax({
            type: 'POST',
            data: res,
            cache: false,
            url: "<?=Yii::app()->createAbsoluteUrl('/kit/index')?>",
            success: function (data) {
                console.log(data);
                var response = jQuery.parseJSON (data);
                $(".gift.wrap-list").html(response.html_gifts);
                $(".pagination").addClass("ajax").html(response.html_pager);
            }
        });
    });

    $('body').on('change', '#kit-filters', function(e){
        var form = $(this).serialize();
        var page = $('div.pagination.ajax ul li:first').text();
        var res = form + '&page=' + page
        console.log(form );
        $('.filter1-res').text($('#filter1').val());

        $.ajax({
            type: 'POST',
            data: res,
            cache: false,
            url: "<?=Yii::app()->createAbsoluteUrl('/kit/index')?>",
            success: function (data) {
                //console.log(data);
                //$(form)[0].reset();
                var response = jQuery.parseJSON (data);
                $(".gift.wrap-list").html(response.html_gifts);
                $(".pagination").addClass("ajax").html(response.html_pager);
                $("#fader").val(response.price);
            }
        });
    });

    $( "#slider-range" ).slider({
        range: true,
        step: 100000,
        min: <?=$min_price?>,
        max: <?=$max_price?>,
        values: [ <?=$min_price?>, <?=$max_price?> ],
        slide: function( event, ui ) {
            $( "#min_price" ).val($( "#slider-range" ).slider( "values", 0 ));
            $( "#max_price" ).val($( "#slider-range" ).slider( "values", 1 ));
        },
        stop: function( event, ui ) {$('#kit-filters').change()}
    });
    $( "#min_price" ).val($( "#slider-range" ).slider( "values", 0 ));
    $( "#max_price" ).val($( "#slider-range" ).slider( "values", 1 ));


    var coockie_items = [];
    $('body').on('click', '.buy-button.add-to', function(e){

        var id = $(this).data('id');

        if(JSON.parse($.cookie('kit_ids')) != null){
            coockie_items = JSON.parse($.cookie('kit_ids'));
        }
        coockie_items.push(id);
        // todo посмотреть поддержку в др браузерах
        coockie_items =  coockie_items.filter(function (value, index, self) {
            return self.indexOf(value) === index;
        });

        $.ajax({
            type: 'POST',
            data: {
                kits: JSON.stringify(coockie_items)
            },
            cache: false,
            url: "<?=Yii::app()->createAbsoluteUrl('/kit/cartadd')?>",
            success: function (data) {
                var response = jQuery.parseJSON (data);
                console.log(data);
                $('#cart').html(response.cart);
            }
        });

        $.cookie('kit_ids', JSON.stringify(coockie_items), { path: '/' });

        //console.log(JSON.parse($.cookie('kit_ids')));
    });

    $('body').on('click', '#cart .cart-delete-item', function(e){
       var item_from_del_id = $(this).data('item_from_del_id');
        $.ajax({
            type: 'POST',
            data: {
                item_from_del_id: item_from_del_id
            },
            cache: false,
            url: "<?=Yii::app()->createAbsoluteUrl('/kit/cartdel')?>",
            success: function (data) {
                var response = jQuery.parseJSON (data);
                console.log(data);
                $('#cart').html(response.cart);
            }
        });
    });

    $('body').on('click', 'a.cart-by', function(e){
        window.onbeforeunload = null;
    });

    window.onbeforeunload = function() {
        //return "Данные не сохранены. Точно перейти?";
    };


    $('body').on('click', '#cart .amount-cat', function(e){
        $('#cart section').toggle();
    });



});
</script>

<form id="kit-filters">
    <div class="inputs">
        От <input type="number" step="any" id="min_price" name="min_price" readonly>
        до <input type="number" step="any" id="max_price" name="max_price" readonly> руб.
    </div>
    <div id="slider-range"></div>
</form>

<span style="clear: both" />

<div id="cart"><?=$cart?></div>
<? $this->renderPartial('/catalog/_list', array('list' => $list)); ?>



