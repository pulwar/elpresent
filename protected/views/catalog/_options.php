<? if(is_array($item->getAllPrice())):?>
	<? $rand = mt_rand(1,99);?>
	<? $uniqid = uniqid(); ?>
	<div class="item-variant">
		<table class="price_choose">
			<? $counter = 0; ?>
			<? foreach($item->getAllPrice() as $k => $v):?>
				<tr class="item<?= $i = ( isset($i) ? ++$i : 0 ) ?>">
					<td class="td-first option-<?=$item->getOption($k)->id?>">
						<input type="radio" name="pr<?=$uniqid?>" id="custom-radio-<?=$k.$rand?>" data-id="<?=$item->id?>" value="<?=$k?>" <?=$counter++ == 0?'checked="checked"':''?> data-price="<?=($v)?> руб">
						<label for="custom-radio-<?=$k.$rand?>">
						<span>
							<?=$item->getOption($k)->name?>
						</span>
						</label>
					</td>
					<td class="td-color">
						<?=($v)?> руб.
					</td>
				</tr>
			<? endforeach;?>
		</table>
	</div>
<? endif;?>
