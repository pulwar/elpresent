<div class="select-gift-column">
    <h4 id="search-results-header"></h4>
    <? $i = 0; ?>
    <? foreach ($items as $item): ?>
        <div class="select-gift-item">
            <div class="row">
                <div class="col-md-2">
                    <img class="image-with-border" src="/<?= $item->preview_img ?>" width="40" height="40" alt="<?= $item->title; ?>">
                </div>
                <div class="col-md-10">
                    <a href="<?= path('/catalog/item', ['perm_link' => $item->perm_link]);?>"><?= $item->title; ?></a>
                </div>
            </div>
        </div>
    <? $i++;if ($i > 2) {break;} ?>
    <? endforeach; ?>
    <a class="btn btn-primary btn-midd " href="<?= $this->createUrl('/catalog/search', $params)?>">Смотреть весь список</a>
</div>