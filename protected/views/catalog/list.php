<div class="main-header main-header_title">
    <h1>
        <? if ($this->action->id != 'index') : ?>
            <?= $category->title ?>

        <? endif; ?>
    </h1>
</div>
<?if(count($category->children) > 0):?>
    <div class="sub_cat_wrap">
        <div class="container_custom">
            <?foreach ($category->children as $child):?>
                <a class="sub_cat_item" href="/<?=$category->slug?>/<?=$child->slug?>"><?=$child->title?></a>
            <?endforeach;?>
        </div>
    </div>
<?endif;?>

<?$count = $list->getTotalItemCount(); ?>
<?if($count==0):?>
    <div class="search_no">По вашему запросу ничего не найдено. Попробуйте посмотреть в других разделах или изменить параметры поиска, либо обратитесь к нашим менеджерам.</div>
<?else:?>
<div class="main-header_title">
    <div class="search_note">Найдено: <span><?=$list->getTotalItemCount()?></span> <?=Yii::t('yii', 'сертификат|сертификата|сертификатов', $count)?></div>
    <div class="list_sort">
        <div class="sort_select_wrap">
            <div class="sort_select_single" data-sort="t.`order` ASC">Сначала <span>Популярные</span> <i></i></div>
            <div class="sort_select_inner">
                <div class="sort_select_item selectActive" data-sort="t.`order` ASC">Популярные</div>
                <div class="sort_select_item" data-sort="ic.item_price ASC">Дешевые</div>
                <div class="sort_select_item" data-sort="ic.item_price DESC">Дорогие</div>
                <div class="sort_select_item" data-sort="t.is_new DESC, t.`order` ASC">Новые</div>
                <input type="hidden" name="category_id" <? if ($this->action->id != 'index') : ?>value="<?=$category->id?>"<? endif; ?>>
            </div>
        </div>
    </div>
</div>
<? $this->renderPartial("_list", compact("list","count")); ?>
<?endif;?>
