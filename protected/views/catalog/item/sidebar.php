<div class="col-md-3">
    <div class="product-item-image">
        <div class="hidden-product-img">
            <img src="/<?= $item->full_img ?>" alt="<?= htmlspecialchars($item->title); ?>"/>
        </div>
        <div class="product-item-image__shadow"></div>

        <? if (!empty($item->is_season)): ?>
            <div class="season-img"></div>
        <? endif; ?>

        <? if ($item->off_price == 0): ?>
            <? if ($item->is_new): ?>
                <div class="new-img"></div><? endif; ?>
            <? if ($item->is_hot): ?>
                <div class="hit-img"></div><? endif; ?>
            <? if (!$item->available || $item->getVariablePriceForView() === false): ?>
                <div class="not-available"></div><? endif; ?>
        <? else: ?>
            <div class="discount-img"><span><?= round($item->off_price); ?>%</span></div>
        <? endif; ?>
        <? if ($item->off_price != 0): ?>
            <div class="off-price-lable">* цена указана без учета скидки</div>
        <? endif; ?>
    </div>

    <!--<div class="rating-block">
        <label for="input-7-xs" class="control-label"></label>
        <input class="rating rating-loading input-7-xs" value="<?//$item->rating / $item->vote_count?>" data-min="0" data-max="5" data-step="1" data-size="xs">
        <input class="rating-loading-id-item" value="<?//$item->id?>" type="hidden">
    </div>-->


    <div class="select-image">
        <? if (!empty($item->is_kit)): ?>
            <a class="btn btn-default add-to-kit-cart item-page <?= empty($item->available) || $item->getVariablePriceForView() === false ? 'hide' : '' ?>" data-id="<?= $item->id ?>" data-item-modal='#myModalItem<?= $item->id ?>'>
                + В Корзину
            </a>
        <? endif; ?>

        <a class="btn <?= empty($item->available) || $item->getVariablePriceForView() === false ? 'hide' : '' ?>" data-toggle="modal" data-target="#myModalFive" href="#">
            <img src="/img/front_bootstrap/catalog-icons/heart.svg" alt=""> Хочу в подарок
        </a>
    </div>

    <div class="fast-order <?= empty($item->available) || $item->getVariablePriceForView() === false ? 'hide' : '' ?>">
        <h3>Быстрый заказ</h3>

        <form method="post" action="/order/makeRequest">
            <p>Оставьте свой номер телефона и мы перезвоним вам.</p>
            <? $this->widget('CMaskedTextField', [
                'name' => "OrderRequest[phone]",
                'attribute' => 'phone',
                'mask' => '+375-99-99-99-999',
                'placeholder' => '*',
                'completed' => 'function(){console.log("ok");}',
                'htmlOptions' => ['placeholder' => '*Ваш телефон', 'required' => 'required', 'id' => 'phoneRandomTwoId' . $item->id . '-ss' . rand(10,100) ]
            ]);
            ?>
            <input type="hidden" value="<?= $item->id; ?>" name="OrderRequest[item_id]"/>
            <input type="text" name="OrderRequest[name]" value="" placeholder="*Ваше имя" required>
            <input type="email" name="OrderRequest[email]" value="" placeholder="*Ваш email" required>
            <button type="submit" class="btn btn-primary btn-sm"
                    onClick="yaCounter17198173.reachGoal('bistri_zakaz'); ga('send', 'pageview', '/virtual/bistri_zakaz');">Заказать</button>
        </form>
        <div data-show-on-success="" style="display: none">
            <p>Спасибо за Ваш заказ! Если вы сделали заказ <?= Yii::app()->params['time_of_mork_big'] ?>, вам перезвонят в течение 10 минут.</p>
            <p>Если Вы сделали заказ в другое время, Вам перезвонят утром. <br><br></p>
        </div>
    </div>

    <? //$this->widget('SitebarGiftTextWidget'); ?>
</div>
