<div class="col_4">
    <div class="list_item_wrap">
        <div class="list_item">
            <div class="action_wrap">
                <? if ($item->is_new): ?>
                    <div class="action_item _new">Новинка</div>
                <? endif; ?>
                <? if ($item->is_hot): ?>
                    <div class="action_item _hit">Хит</div>
                <? endif; ?>
                <? if ($item->is_season): ?>
                    <div class="action_item _season">Сезонно</div>
                <? endif; ?>
                <? if ($item->off_price != 0): ?>
                    <div class="action_item _sale">Скидка <?= round($item->off_price); ?>%</div>
                <?endif;?>
                <a href="<?= path('catalog/item', ['perm_link' => $item->perm_link]); ?>" class="list_item_img" style="background-image: url(<?= '/' . $item->full_img ?>);"></a>
            </div>
            <div class="list_item_inner">
                <? if ($item->off_price != 0): ?>
                    <div>*цена указана без учета скидки</div>
                <?endif;?>
                <div class="list_item_title"><a href="<?= path('catalog/item', ['perm_link' => $item->perm_link]); ?>"><?= $item->title; ?></a></div>
                <div class="list_item_description"><?= $item->desc; ?></div>
                <div class="list_item_info">
                    <div class="list_item_price"><?= $item->getVariablePriceForView(); ?></div>
                    <div class="list_item_comment"><a href="<?= path('catalog/item', ['perm_link' => $item->perm_link]); ?>">Подробнее</a></div>
                </div>
                <div class="list_item_btn_wrap clearfix">
                    <div class="list_item_btn_cell">
                        <a class="btn_list btn_green" data-toggle="modal" data-target="#fastOrderInList<?= $item->id; ?>"
                           onClick="yaCounter17198173.reachGoal('odin_klik'); ga('send', 'pageview', '/virtual/fodin_klik');">БЫСТРЫЙ ЗАКАЗ</a>

                    </div>
                    <div class="list_item_btn_cell">
                        <? if ($count_prices > 1): ?>


                            <!--<span class="glyphicon glyphicon-plus"></span>-->
                            <a class="btn_list shose-price <?= empty($item->t_available) ? 'hide' : '' ?>" data-toggle="modal" data-target="#myModalSeven<?= $item->id ?>" >В корзину</a>
                        <? else: ?>
                            <!--<span class="glyphicon glyphicon-plus"></span>-->
                            <a class="btn_list add-to-kit-cart <?= empty($item->t_available) ? 'hide' : '' ?>" data-id="<?= $item->id ?>" data-item-modal='#myModalItem<?= $item->id ?>'>В корзину</a>
                        <? endif; ?>

                        <?/*?>
                                    <a href="#" class="btn_list">в корзину</a>
                                    <a href="/cart" class="btn_list _active">в корзине</a>
                                    <?*/?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?$this->renderPartial('/cart/_modal_item', ['item' => $item, 'numItem' => $k, 'count_prices' => $count_prices]); ?>