<h1>Галерея</h1>
<div class="gallery-view-list clearfix">
    <ul>
        <? foreach ($gallery->galleryPhotos as $photo) : ?>
            <li>
                <a class="gallery-preview" data-lightbox="roadtrip" href="<?=$photo->getUrl(); ?>">
                    <img src="<?=$photo->getPreview(); ?>">
                </a>
            </li>
        <? endforeach; ?>
    </ul>
</div>
<hr>
