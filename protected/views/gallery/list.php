<h1>Галерея</h1>
<div class="row padding-hr">
	<?php foreach ($galleries as $gallery):?>
	<div class="col-md-4">
		<div class="gallery-item">
			<a href="<?= path('gallery/view', ['id'=>$gallery->id]); ?>">
				<h3><?= $gallery->name; ?></h3>
                <div class="image-hidden">
				    <img src="<?= $gallery->galleryPhotos[0]->getPreview(); ?>" alt="<?= $gallery->name; ?>">
                </div>
			</a>
		</div>
	</div>
<? endforeach;?>