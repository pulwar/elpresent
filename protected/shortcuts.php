<?php

/**
 * @param $route
 * @param array $params
 * @param bool $absolute
 * @return string
 */
function path($route, array $params = array(), $absolute = false)
{
    $url =  Yii::app()->urlManager->createUrl($route, $params);
    if(!$absolute || strpos($url, 'http') === 0) {

        return $url;
    }

    return 'https://elpresent.by'.$url;
}

/**
 * Checks whatever feature is enabled or not
 * @param $featureName
 * @return bool
 */
function isFeatureEnabled($featureName)
{
    return Yii::app()->featureSwitch->isEnabled($featureName);
}

function asset($uri) {
    $filePath = Yii::getPathOfAlias('webroot') . $uri;

    if (is_file($filePath)) {
        $mtime = filemtime($filePath);

        return $uri.'?v=' . substr($mtime, 4);
    }

    return $uri;
}