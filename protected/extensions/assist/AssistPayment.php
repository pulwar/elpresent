<? 
class AssistPayment
{
    private $_params = array
    (
        'Shop_IDP'   => '',
        'Order_IDP'  => '',
        'Subtotal_P' => '',
        'Currency'   => '',
        'RETURN_URL' => '',
        'RETURN_URL_OK' => '',
        'RETURN_URL_NO' => ''
    );
    
    private $_config;
    
    public function __construct()
    {
        $config = include Yii::getPathOfAlias('webroot').'/protected/config/assist_payment.php';
        
        $this->_config                  = $config;
        $this->_params['Shop_IDP']      = $config['shop_idp'];
        $this->_params['Currency']      = $config['currency'];
        $this->_params['URL_RETURN']    = $config['url_return'];
        $this->_params['URL_RETURN_OK'] = $config['url_return_ok'];
        $this->_params['URL_RETURN_NO'] = $config['url_return_no'];

    }
    
    public function setOrderNumber($orderNumber)
    {
        $this->_params['Order_IDP'] = $orderNumber;       
    }
    
    public function setSubtotal($subTotal)
    {
        $this->_params['Subtotal_P'] = $subTotal;
    }
    
    public function setFirstName($firstName)
    {
        $this->_params['FirstName'] = $firstName;        
    }
    
    public function setLastName($lastName)
    {
        $this->_params['LastName'] = $lastName;    
    }
    
    public function setEmail($email)
    {
        $this->_params['Email'] = $email;        
    }
    
    public function setDemoResult($demoResultCode)
    {
        if ( $this->_config['status'] == 'test' )
        {
            $this->_params['DemoResult'] = $demoResultCode;
        }
    }
    
    public function getXmlResultUrl()
    {
        $url  = $this->_config[$this->_config['status']]['auth_result_url'].'?';
        $url .= 'ShopOrderNumber=%';
        $url .= '&Shop_ID='.$this->_config['shop_idp'];
        $url .= '&Login='.$this->_config['login'];
        $url .= '&Password='.$this->_config['password'];
        $url .= '&Success=1';
        $url .= '&MeanType=0';
        $url .= '&PaymentType=0';
        $url .= '&Format=4';
        
        return $url;
    }
    
    public function getUrl()
    {
        $url    = $this->_config[$this->_config['status']]['type'][$this->_config['type']]['url'].'?';
        $params = '';
        
        foreach ( $this->_params as $paramsName => $paramValue )
        {
            $params .= '&'.$paramsName.'='.$paramValue;    
        }
        
        $params = trim($params, '&');
        
        $url .= $params;
        
        return $url; 
    }
    
    public function setType($type = 1)
    {
        $this->_config['type'] = $type;    
    }
}

?>