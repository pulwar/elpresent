<?php
abstract class AbstractApplicationStrategy {
    abstract public function doIt($params);
}