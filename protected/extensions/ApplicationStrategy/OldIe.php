<?php
class OldIe extends AbstractApplicationStrategy{
    public function doIt($params) {
        if(Yii::app()->controller->id != 'ie'){
            Yii::app()->controller->redirect(['ie/index', 'id' => $params]);
        }
    }
}