<?php

class ApplicationStrategy extends CApplicationComponent{

    const MIN_IE_VERS = 8;

    public function init() {
        parent::init();

        Yii::import('application.extensions.ApplicationStrategy.*');

        $br =  $this->browser;

        if ($br->getBrowser() == $br::BROWSER_IE && $this->browser->getVersion() <= self::MIN_IE_VERS) {
            $obj = new OldIe();
            $obj->doIt($this->browser->getVersion());
        }
    }
}