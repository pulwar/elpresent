<? 

class Curl
{
    private $_instance = null;
    
    
    public function __construct()
    {
        $this->_instance = curl_multi_init();        
    }
    
    public function getSSLUrl($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        
        return curl_exec($ch);     
    }
    
    
    public function get(array $urls)
    {
        $curl         = array();
        $curlResponse = array();
        
        foreach ( $urls as $k => $url )
        {
            $curl[$k] = curl_init();
            $options  = array
            (
                CURLOPT_URL    => $url,
                CURLOPT_HEADER => false,
                CURLOPT_RETURNTRANSFER => true 
            );
            
            curl_setopt_array($curl[$k], $options);
            curl_multi_add_handle($this->_instance, $curl[$k]);
        }
        
        $running = null;
        
        do 
        {
            usleep(10000);
            curl_multi_exec($this->_instance, $running);
            curl_multi_getcontent($this->_instance);
        } 
        while ( $running > 0 );
        
        foreach ( $urls as $k => $url )
        {
            $curlResponse[] = curl_multi_getcontent($curl[$k]);
            curl_multi_remove_handle($this->_instance, $curl[$k]);
            curl_close($curl[$k]);   
        }
        
        curl_multi_close($this->_instance);
        
        return $curlResponse;
    }
}