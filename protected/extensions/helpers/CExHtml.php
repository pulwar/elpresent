<? 
	class CExHtml extends CHtml {
		/**
		 * (non-PHPdoc)
		 * @see framework/web/helpers/CHtml#beginForm($action, $method, $htmlOptions)
		 */
		static public function beginForm($action='',$method='post',$htmlOptions=array()){
			$class = null;

			// get class attribute
			if(array_key_exists('class', $htmlOptions))
				$class = &$htmlOptions['class'];

			// route replacement
			if(null!==$class && preg_match('/redirect\s*\[(.+?)\]/si', $class, $regex)){
				/* @var $controller CController */
				$controller = Yii::app()->controller;
				$route = str_replace(array('{controller}', '{action}'), array($controller->id, $controller->action->id), $regex[1]);
				$class = str_replace($regex[0], 'redirect['. $controller->createAbsoluteUrl($route) .']', $class);
			}

			// add ajaxable flag
			if(null!==$class)
				$class .= (isset($class{0}) ? ' ': ''). 'ajaxable';

			return parent::beginForm($action, $method, $htmlOptions);
		}
	}