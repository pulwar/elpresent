<? 
	class CDirectoryHelper {
		protected static $ds = DIRECTORY_SEPARATOR;

		static public function create($path, $chmod=0755)
		{
			$path = self::normalize($path);
			if(file_exists($path))
			{
				return is_dir($path);
			}

			$chunks = preg_split('/[\\\\\/]+/', $path);
			$path = '';

			foreach($chunks as $chunk)
			{
				$path .= (isset($path{0})? self::$ds : ''). $chunk;
				if(! file_exists($path))
				{
					$tries = 3;
					while($tries-- && !@mkdir($path, $chmod))
					{
						if(! $tries)
						{
							return false;
						}
					}
				}
				else if(! @is_dir($path))
				{
					return false;
				}
			}

			return (file_exists($path) && @is_dir($path));
		}

		static public function normalize($path)
		{
			return preg_replace('/[\\\\\/]+/', self::$ds, $path);
		}
	}