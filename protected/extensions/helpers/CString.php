<? 

class CString {
	public static function replace($value, $replacement = array()){
		return str_replace(array_keys($replacement), array_values($replacement), $value);
	}
}