<?php

class SiteMapAction extends CAction
{

    public function run()
    {
        // get all categories
        $categories = Category::getTree(true, true);
        // get all cities
        $cities = City::getCityList();

        // some optimizations
        $itemLinksProvider = new CActiveDataProvider('Item', []);
        $itemIterator = new CDataProviderIterator($itemLinksProvider, 0);

        $locations = $this->generateLinks($categories, $cities, $itemIterator);

        $sitemap = $this->controller->renderPartial('sitemap', compact('locations'), true);
        file_put_contents(Yii::getPathOfAlias('application') . '/../sitemap.xml', $sitemap);
        header('Content-Type: text/xml');
        echo $sitemap;
    }

    private function generateLinks($categories, $cities, $items)
    {
        $locations = [];

        // generate links for catalog
        foreach ($cities as $city) {
            $locations[] = path('catalog/index', ['perPage' => 'all', 'city' => $city], true);
        }

        // generate links for categories
        foreach ($categories as $root) {
            foreach ($cities as $city) {
                $locations[] = path('catalog/category', ['slug' => $root['slug'], 'city' => $city], true);
            }
        }

        // generate links for items
        foreach ($items as $item) {
            foreach ($item->prices as $cityLink) {
                $city = $cityLink->city;
                $locations[] = path('catalog/item', ['perm_link' => $item->perm_link, 'city' => $city], true);
            }
        }

        return $locations;
    }

}
