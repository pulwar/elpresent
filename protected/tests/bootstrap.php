<?php

// change the following paths if necessary
$yiit=dirname(__FILE__).'/../../framework/framework/yiit.php';
$config=dirname(__FILE__).'/../config/test.php';

/** @var \Composer\Autoload\ClassLoader $loader */
$loader = require(__DIR__ . '/../../vendor/autoload.php');
$loader->setUseIncludePath(false);
$loader->unregister();

require $yiit;
Yii::registerAutoloader([$loader, 'loadClass'], true);

Yii::createWebApplication($config);
