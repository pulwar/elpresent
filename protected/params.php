<?php
// this contains the application parameters that can be maintained via GUI
return array(
    'add_text_in_gift1' => 'До 1 декабря доставка бесплатна',
    'add_text_in_gift2' => 'Сертификат без активации недействителен.',
    'vk_group_id' => 17973632,
    'vk_token' => 'b3fd05f3d233372a53ef7be7d8effe1c5010e747be37738846a684e504e0aa371a97127227b0be432cd10',
    'order_alert_text' => 'В данное время основной магазин Elpresent.by не работает, вы можете приобрести подарочный сертификат в любой дилерской точке',
    'count_gifts_per_page' => 12,
    'count_gifts_in_main_per_page' => 12,
    // custom view config
    'phones' => [
        '+375 29 687-87-28',
        '+375 29 767-87-28'
    ],
    'time_of_mork_small' => '09:30-18:00, сб, вс - выходной ',
    'time_of_mork_small1' => 'Пн-пт: 9:30-13:00. Сб,вс - выходной',
    'time_of_mork_big' => 'с 09.30 до 13.00, сб, вс - выходной',
    'address' => 'ул. Куйбышева 22, первый этаж, холл, справа',
    'link_more_addresses' => '<a href="/wherebuy">Еще адреса</a>',
    'pickup_message' => 'Забрать сертификат вы можете по адресу: ул.Куйбышева 22, первый этаж, холл, справа',
    // end custom view config

    // this is used in error pages
    'adminEmail' => 'admin@elpresent.by',
    'catalog_default_meta_k' => 'elpresent,подарочные сертификаты,подарки,интернет-магазин подарков,новогодние подарки,подарки в Минске,подарки для девушек,подарки для мужчин,подарки на новый год,подарок на 8 марта,подарки на 14 февраля,подарки для парней',
    'catalog_default_meta_d' => 'Огромный выбор подарочных сертификатов, яркая упаковка, приятные эмоции и красочные впечатления для родителей, друзей, коллег, любимых. Дарите близким незабываемый праздник, улыбки и настроение! ELPresent.by - Интернет-магазин подарков, исполняющий самые заветные мечты.',
    // adminStatus by default on signup
    'adminEmailList' => array('admin@elpresent.by'),
    'webpay_complete_message' => 'Спасибо за ваш заказ!',
    'bepaid_complete_message' => 'Спасибо за ваш заказ!',
    'bepaid_decline_message' => 'Оплата не был совершена, транзакция отклонена банком',
    'bepaid_fail_message' => 'Оплата не был совершена, произошла неизвестная ошибка',
    'bepaid_fail_message' => 'Оплата не был отменена',
    'webpay_cancel_message' => 'Ваш платёж был отклонён! Деньги не были списаны с вашего счета!',
    // packages
    'packages' => array(
        array('time' => 60 * 60 * 24 * 7, 'cost' => 10, 'name' => 'Week'),
        array('time' => 60 * 60 * 24 * 30, 'cost' => 30, 'name' => 'Month'),
        array('time' => 60 * 60 * 24 * 365, 'cost' => 300, 'name' => 'Year'),
        array('time' => 60 * 60 * 24 * 365 * 2, 'cost' => 500, 'name' => 'Two Years'),
    ),
    'itemCustomFields' => array( //blockId => array(field => fieldTitle,.....,field=>fieldTitle)
    ),
    'itemCustomBlocksTitle' => array( //blockId => title
    ),
    'payments' => array(
        'cash' => array(),
        'paypal' => array(
            'url' => 'https://www.sandbox.paypal.com/cgi-bin/webscr',
            'urlParts' => array(
                'baseUrl' => 'www.sandbox.paypal.com',
                'script' => 'cgi-bin/webscr'
            ),
            'params' => array(
                'cmd' => '_cart',
                'charset' => 'utf-8',
                'upload' => 1,
                'business' => 'info@elpresent.by',
                'no_shipping' => 1,
                'rm' => 2
            )
        )
    ),
    'cronServer' => array('pathInfo' => $hostname),
    // set this param to change timezone in admin panel
    'elTimezone' => array('timezone' => '+3:00'),
    //'elTimezone' => array('timezone' => 'Europe/Minsk'),
    'questionnaire' => array(
        'emailTo' => 'info@elpresent.by',
    ),
    'review' => array(
        'emailTo' => 'info@elpresent.by',
    ),
    'feature_switches' => [
        'regions' => true,
    ],
    'emailer' => [
        'username' => 'elpwork@yandex.ru',
        'password' => 'elpwork@!',
    ],
);