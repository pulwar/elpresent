<?php

class PdfGenerator extends CApplicationComponent
{

    /**
     * @var string
     */
    protected $tmpDir;

    /**
     * @var  IViewRenderer
     */
    protected $viewRenderer;


    public function init()
    {
        $this->viewRenderer = Yii::app()->viewRenderer;
        //$this->tmpDir = Yii::getPathOfAlias('application.runtime.pdf_tmp');
	    $this->tmpDir = Yii::getPathOfAlias('webroot.assets.pdf_tmp');
        $this->checkTmpDir();
    }

    public function generateDigitalCertificate(Order $order)
    {
        $orderItem = $order->orderItems[0];
        $html = Yii::app()->controller->renderPartial(
            $orderItem->itemObj->type == Item::TYPE_ITEM ? '//pdf/digital_certificate' : '//pdf/digital_certificate_group',
            ['order' => $order, 'rootDir' => Yii::getPathOfAlias('webroot')],
            true
        );

        // save temp file
        $htmlFile = $this->tmpDir . '/' . $order->id . '.html';
        file_put_contents($htmlFile, $html);

        // generate pdf
        $pdfFilePath = $this->tmpDir . '/' . $order->id . '.pdf';
        $this->generatePdf($htmlFile, $pdfFilePath);

        return $pdfFilePath;
    }

    public function generatePdf($htmlFile, $pdfFile)
    {
        $rootDir = Yii::getPathOfAlias('application') . '/..';
        exec(sprintf('/home/qzby/nodevenv/node/6/bin/node %s/pdf/bin/html2pdf --html %s --pdf %s --portrait 2>&1', $rootDir, $htmlFile, $pdfFile));
    }

    private function checkTmpDir()
    {
        if (!is_dir($this->tmpDir)) {
            mkdir($this->tmpDir);
        }
    }

}