<?php

class ImageWrapperException extends Exception
{
	
}
class ImageWrapper
{
	static public function wrap($smth)
	{
		if(is_object($smth))
			return new self($smth->filepath);
		if(is_array($smth))
			return new self($smth['filepath']);
		if(is_string($smth))
			return new self($smth);
	}
	
	static protected function getAllowedExts()
	{
		return array('jpg','png','gif','jpeg');
	}
	
	static public function getFileExt($file)
	{
		return pathinfo($file,PATHINFO_EXTENSION);
	}
	
	static public function isCorrectExt($image,$allowed = null)
	{
		if(is_null($allowed))
			$allowed = self::getAllowedExts();
			
		return in_array(strtolower(self::getFileExt($image['name'])),$allowed);
	}
	
	static public function uploadListForOwner($list,$owner,$uploadPath,$propName = 'image')
	{
		$res = array();
		$listKeys = array_keys($list);
		for($i = 0;$i < count($list['name']);$i++)
		{
			if($list['error'][$i] != 0)
				continue;
			try
			{
				$image = array();
				foreach($listKeys as $key)
					$image[$key] = $list[$key][$i];
				$res[] = self::upload($image,$uploadPath);
			}
			catch(ImageWrapperException $e)
			{
				$owner->addError($propName,$e->getMessage());
			}
		}
		return $res;
	}
	
	static public function uploadForOwner($image,$owner,$uploadPath,$propName = 'image')
	{
		try
		{
			return self::upload($image,$uploadPath);
		}
		catch(ImageWrapperException $e)
		{
			$owner->addError($propName,$e->getMessage());
		}
	}
	
	static public function upload($image,$uploadPath)
	{
		if(!self::isCorrectExt($image))
		{
			throw new ImageWrapperException(strtr(
				_('Dissallowed image type {image}! Allowed file types is {types}'),
				array('{types}' => implode(', ',self::getAllowedExts()),'{image}' => $image['name'])
				)
			);
		}
		$filename = uniqid().'.'.self::getFileExt($image['name']);
		$savepath = $uploadPath.'/'.$filename;
		if(move_uploaded_file($image['tmp_name'],$savepath))
		{
			return new self($savepath);
		}
		throw new ImageWrapperException(strtr(_('error while uploading image {image}'),array('{image}' => $image['name'])));
	}
	
	protected
		$filepath;
	
	public function __construct($filepath)
	{
		if(!$filepath)
		{
			throw new ImageWrapperException(_('no imagepath given'));
		}
		if(!is_file($filepath))
		{
			throw new ImageWrapperException(strtr(_("file {filepath} does not exists"),array('{filepath}' => $filepath)));
		}
		$this->filepath = $filepath;
	}
	
	public function getFilepath()
	{
		return $this->filepath;
	}

	public function resize()
	{
		$args = func_get_args();
		return call_user_func_array(array($this,'resolution'),$args);
	}

	public function resolution($w,$h)
	{
		$filename = pathinfo($this->filepath,PATHINFO_FILENAME);
		$filepath = dirname($this->filepath).'/'.$filename;
		if(!file_exists($filepath))
			mkdir($filepath);
			
		$base 	= basename($this->filepath);
		$ext 	= pathinfo($base,PATHINFO_EXTENSION);
		$path  	= "{$filepath}/resized_{$w}x{$h}.{$ext}";
		if(!file_exists($path))
			$this->resizeMe($w,$h,$path);
		
		return $this->getWebFullpath($path);
	}
	
	public function resizeMe($w,$h,$savePath)
	{
		Yii::app()->setImport(array('application.extensions.image.*'));
		$image = new Image($this->filepath);
		$image->resize($w,$h,Image::NONE);
		return $image->save($savePath);
	}
	
	public function getModifiedImagesDir()
	{
		return dirname($this->filepath).'/'.pathinfo($this->filepath,PATHINFO_FILENAME);
	}
	
	public function getImagePathname()
	{
		return $this->filepath;
	}

	public function crop($w,$h)
	{
		$dir 		= $this->getModifiedImagesDir();
		if(!file_exists($dir))
			mkdir($dir);
		$filename	= "croped_{$w}x{$h}.".pathinfo($this->getImagePathname(),PATHINFO_EXTENSION);
		$pathname 	= "{$dir}/{$filename}";
		if(!file_exists($pathname))
			$this->cropMe($w,$h,$pathname);
		return $this->getWebFullpath($pathname);
	}
	
	protected function getWebFullpath($path)
	{
		return '/'.$path;
	}
	
	
	final protected function cropMe($w,$h,$savepath)
	{
		$image = new Image($this->getImagePathname());
		$image->crop($w,$h);
		return $image->save($savepath);
	}
	
	
	public function delete()
	{
		@unlink($this->filepath);
		$dir = $this->getModifiedImagesDir();
		@exec("rm -fr {$dir}");
		return true;
	}
	
	public function __toString()
	{
		return $this->getWebFullpath($this->filepath);
	}
}
