<?php

class Controller extends CController
{
    /**
     * @return CHttpRequest
     */
    protected function getRequest()
    {
        return Yii::app()->request;
    }


    public function get($service)
    {
        return Yii::app()->getComponent($service);
    }

}