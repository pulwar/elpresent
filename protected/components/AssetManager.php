<?php

class AssetManager extends CApplicationComponent
{

    public $javascripts;

    public $stylesheets;

    public $cwd;

    public $map;
    
    public $uglifyJsBin = 'uglifyjs';
    
    public $uglifyCssBin = 'uglifycss';
    
    public $lesscBin = 'lessc';


    public function dump($mask = null)
    {
        $mask = $mask ? sprintf('/%s/Ui', $mask) : null;
        foreach ($this->map as $target=>$files) {
            if ($mask && !$this->is($target, $mask)) {
                continue;
            }

            $isJs = preg_match('/\.js$/i', $target);
            $target = $this->cwd . '/' . $target;
            if ($isJs) {
                $this->processJs($this->expand($files), $target);
            } else {
                $this->processCss($this->expand($files), $target);
            }
        }
    }

    private function is($target, $mask)
    {
        return (bool) preg_match($mask, $target);
    }

    private function expand(array $patterns = [])
    {
        $result = [];
        $excluded = [];
        $cwd = $this->cwd;
        foreach ($patterns as $pattern) {

            // is it exclusion?
            $exclude = 0 === strpos($pattern, '!');
            if ($exclude) {
                $pattern = substr($pattern, 1);
            }

            $matches = glob($cwd . '/' . ltrim($pattern, '/'));
            if ($exclude) {
                $excluded = array_unique(array_merge($excluded, $matches));
            } else {
                $result = array_unique(array_merge($result, $matches));
            }
        }

        // remove excluded items from result array
        return array_diff($result, $excluded);
    }

    private function processJs($files, $target)
    {
        // uglify js
        echo sprintf('Dumping file %s...' . "\n", $target);
        $this->runCommand($this->uglifyJsBin, $files, [
            'o' => $target
        ]);

        echo sprintf('Compressing file %s...' . "\n", $target);
        $this->runCommand('gzip', [$target . ' > ' .$target.'.gz'], ['c' => true, '5' => true]);
    }

    private function processCss(array $files, $target)
    {
        $tmp = [];

        // process less stylesheets
        $less = array_filter($files, function ($file) {
            return preg_match('/\.less$/i', $file);
        });

        // convert each less file into css file
        // store it in temp directory add add to main list
        $tempDir = Yii::getPathOfAlias('application.runtime') . '/c_less';
        if (!is_dir($tempDir)) {
            mkdir($tempDir);
        }

        foreach ($less as $file) {
            $tempFile = $tempDir . md5($file) . '.css';
            echo sprintf('Processing file %s...' . "\n", $file);
            $this->runCommand($this->lesscBin, [
                $file, $tempFile
            ]);
            $tmp[] = $tempFile;
            $files[] = $tempFile;
        }

        // remove all processed files from original array
        $files = array_diff($files, $less);

        // uglify css
        echo sprintf('Dumping file %s...' . "\n", $target);
        $this->createFile($target, $this->runCommand($this->uglifyCssBin, $files));

        echo sprintf('Compressing file %s...' . "\n", $target);
        $this->runCommand('gzip', ['-c -5', $target . ' > ' .$target.'.gz']);
    }

    private function runCommand($cmd, array $args, array $options = [])
    {
        if (!$this->isCommandExists($cmd)) {
            throw new CException(sprintf('Command %s not found', $cmd));
        }

        $optionsString = [];
        foreach ($options as $name => $value) {
            $optionsString[] = trim((1 == strlen($name) ? '-' : '--'). $name . ' ' . $value);
        }
        $optionsString = implode(' ', $optionsString);

        $ecmd = sprintf('%s %s %s', $cmd, $optionsString, implode(' ', $args));
        return shell_exec($ecmd);
    }

    private function isCommandExists($cmd)
    {
        $returnVal = shell_exec("which $cmd");

        return !empty($returnVal);
    }

    private function createFile($path, $contents = '')
    {
        $dir = dirname($path);
        if (!is_dir($dir)) {
            mkdir($dir, 0644, true);
        }

        file_put_contents($path, $contents);
    }

}