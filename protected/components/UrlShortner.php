<?php

class UrlShortner extends CApplicationComponent
{

    public $hashMinLength = 6;

    public function encode($id, $prefix = 'i')
    {
        return $this->pad(base_convert($id, 10, 36), $prefix, $this->hashMinLength);
    }

    public function decode($hash, $prefix = 'i')
    {
        $id = base_convert(substr($hash, strpos($hash, $prefix)+1), 36, 10);

        return $id;
    }

    protected function pad($str, $prefix, $length)
    {
        $str = $prefix.$str;
        $length = $length - strlen($str);
        $characters = str_replace($prefix, '', '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }

        return $randomString.$str;
    }

}