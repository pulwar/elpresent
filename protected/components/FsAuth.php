<?php

class FsAuth extends CApplicationComponent
{
	/**
	 * ID приложения
	 *
	 * @var string
	 */
	const client_id = '850305875060056';

	/**
	 * Защищённый ключ
	 *
	 * @var string
	 */
	const client_secret = '2a1e7b0e108ebb2054d928171efb275a';

	/**
	 * Адрес метода авторизации на сайте
	 *
	 * @var string
	 */
	const redirect_uri = 'https://elpresent.by/profile/FsAuth';

	/**
	 *
	 */
	const url = 'https://www.facebook.com/dialog/oauth';

	/**
	 * Информация о профайле юзера из контакта
	 *
	 * @var array
	 */
	private $_userInfo = [];

	/**
	 * @var array
	 */
	private static $_params = array(
		'client_id' => self::client_id,
		'redirect_uri' => self::redirect_uri,
		'response_type' => 'code',
		'scope' => 'email,user_birthday'
	);

	public function init()
	{
		$link = self::url . '?' . urldecode(http_build_query(self::$_params));

		if (isset($_GET['code'])) {

			$params = array(
				'client_id' => self::client_id,
				'redirect_uri' => self::redirect_uri,
				'client_secret' => self::client_secret,
				'code' => $_GET['code']
			);

			$url = 'https://graph.facebook.com/oauth/access_token';

			$tokenInfo = null;
			parse_str(file_get_contents($url . '?' . http_build_query($params)), $tokenInfo);

			if (count($tokenInfo) > 0 && isset($tokenInfo['access_token'])) {
				$params = array('access_token' => $tokenInfo['access_token']);

				$userInfo = json_decode(file_get_contents('https://graph.facebook.com/me' . '?' . urldecode(http_build_query($params))), true);

				if (isset($userInfo['id'])) {
					$this->_userInfo = $userInfo;
				}
			}
		}else{
			header("Location: $link");
		}

	}

	/**
	 * @return array
	 */
	public function getUserInfo()
	{
		return $this->_userInfo;
	}
}