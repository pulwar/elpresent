<?php

class UserSocialIdentity extends CUserIdentity
{

	protected $_id;
	public $uid;
	public $user_firstname;
	public $user_email;

	public function __construct($uid, $first_name, $email = null)
	{
		$this->uid = $uid;
		$this->user_firstname = $first_name;
		$this->user_email = $email;
	}

	public function authenticate()
	{
		// если из социалка дала email
		if(!empty($this->user_email)){
			$user = User::model()->findByAttributes([
				'user_email'=> $this->user_email,
			]);
		}else{
			$user = User::model()->findByAttributes([
				'user_password' => md5($this->uid),
				'user_firstname'=> $this->user_firstname,
			]);
		}

		if (!$user) {
			return false;
		}

		$this->_id = $user->id;

		return true;
	}

	public function getId()
	{
		return $this->_id;
	}

}