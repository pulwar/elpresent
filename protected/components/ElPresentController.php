<?php

class ElPresentController extends Controller
{
    public $loadOlark = false;   
    
    public function getLeftMenuWidget()
	{
		return array('ProfileWidget', 'CategoriesWidget', 'SocialWidget');
	}
	
	protected function requireVar($name,$type = 'int')
	{
		$val = Yii::app()->request->getParam($name);
		settype($val,$type);
		if($val)
			return $val;
			
		throw new CHttpException(404);
	}

    public function init()
    {
        Yii::app()->appStrategy;

        Yii::app()->clientScript->registerCoreScript('cookie');

        if (isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER'])){

            if (false === strpos($_SERVER['HTTP_REFERER'],'http://elpresent')
                && false === strpos($_SERVER['HTTP_REFERER'],'http://www.elpresent')
                ){
                    $cookie = new CHttpCookie('REFERER', $_SERVER['HTTP_REFERER']);
                    $cookie->httpOnly = true;
                    $cookie->expire = time() + 259200; //3 days
                    Yii::app()->request->cookies['REFERER'] = $cookie;
                    $_SESSION['REFERER'] = $_SERVER['HTTP_REFERER'] ;
            }

        }
		
        return parent::init();
    }
	
	 
}
