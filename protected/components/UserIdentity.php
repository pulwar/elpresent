<?php

class UserIdentity extends CUserIdentity
{

    protected $_id;
    public $user_email;
    public $user_password;

    public function __construct($user_email, $user_password, $hesh = false)
    {
        $this->user_email = $user_email;

        if(!$hesh){
            $this->user_password = md5($user_password);
        }else{
            $this->user_password = $user_password;
        }
    }

    public function authenticate()
    {
        if(url::is_localhost()){
            $user = User::model()->findByAttributes([
                'user_email' => $this->user_email
            ]);
        }else{
            $user = User::model()->findByAttributes([
                'user_email' => $this->user_email,
                'user_password' => $this->user_password
            ]);
        }

        if (!$user) {
            return false;
        }

        $this->_id = $user->id;

        return true;
    }

    public function getId()
    {
        return $this->_id;
    }

}