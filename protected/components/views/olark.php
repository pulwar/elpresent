<div id="olark-data"><a class="olark-key" id="olark-5294-505-10-2994" title="Powered by Olark" href="http://olark.com/about" rel="nofollow">Powered by Olark</a></div> 
<? if ($olark_is_enabled) : ?>
<? Yii::app()->clientScript->registerScriptFile('/js/ext/wc.js', CClientScript::POS_END); ?>
<? Yii::app()->clientScript->registerScript('olark_init', '
       wc_init();
       setTimeout(function(e){
            $(\'#habla_expanded_div div:last\').hide();
       }, 3000);
    ', CClientScript::POS_READY); ?>
<? else: ?>
<? Yii::app()->clientScript->registerScript('olark_init', "
        $('#olark-data').hide();
        $('#olark_enabler').click(function() {
            $(this).html('Loading... Please wait.');
            loadOlarkJs();
            $.get('/ajax_enable_olark.html', {enable: 1});
            return false;
        });
        $('.startChat').click(function() {
            $('#olark_enabler').html('Loading... Please wait.');
            loadOlarkJs();
            $.get('/ajax_enable_olark.html', {enable: 1});
            return false;
        });
    ", CClientScript::POS_READY); ?>
    <div id="habla">
        <a href="#" id="close"></a>
        <a href="#" id="expand"></a>
        <a href="#" id="olark_enabler">Нажмите для получения поддержки в режиме Онлайн</a>
    </div>
<? endif; ?>