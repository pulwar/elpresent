<ul class="pagination">
	<li>
		<a href="<?= $this->getPageUrl(1); ?>"><span
				aria-hidden="true">Первая</span><span
				class="sr-only">Первая</span></a>
	</li>
	<?php for (; $startFrom < $startTo; $startFrom++): ?>
		<li class="<?= $startFrom === $pager->currentPage ? 'active' : '' ?>">
			<?php if ($startFrom === $pager->currentPage): ?>
				<span><?= $startFrom + 1; ?></span>
			<? else: ?>
				<a href="<?= $this->getPageUrl($startFrom + 1); ?>"><?=
					$startFrom + 1; ?></a>
			<? endif; ?>
		</li>
	<? endfor; ?>
	<? if ($pager->currentPage < $pager->pageCount) : ?>
		<li><a href="<?= $this->getPageUrl($pager->currentPage + 2); ?>"><span
					aria-hidden="true">Следущая</span><span class="sr-only">Следущая</span></a>
		</li>
	<? endif; ?>
</ul>