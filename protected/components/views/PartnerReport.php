<? foreach ( $aData as $iKey => $aDataObjects ) : ?>

<table class="table table-condensed table-striped table-bordered">
    <tr>
        <td colspan="6"><h2><?= $aDataObjects[2]->item->partner->name ?></h2></td>
    </tr>
    <tr>
        <td>Название подарка</td>
        <td>Дата оплаты</td>
        <td>Время оплаты</td>
        <td>Цена</td>
    </tr>
    <? 
    $iTotalSum = 0;
    foreach ( $aDataObjects as $aDataObject ) : ?>
    <? $iTotalSum += (int) $aDataObject->item->partner_price ?>
        <tr>
<!--            <td><? //echo $aDataObject->item->partner->name; ?></td>-->
            <td><?= $aDataObject->item->title ?></td>
            <td><?
                if ((int) strtotime($aDataObject->is_paid_date) > 0)
                {
                    echo date("d/m/Y", strtotime($aDataObject->is_paid_date));
                }
                else
                {
                    echo '&nbsp;';
                }
                ?>
            </td>
            <td><?
                if ((int) strtotime($aDataObject->is_paid_date) > 0)
                {
                    echo date("h:m", strtotime($aDataObject->is_paid_date));
                }
                 else
                 {
                     echo '&nbsp;';
                 }
                ?>
            </td>
            <td><?= number_format  ($aDataObject->item->partner_price) ?></td>
            
        </tr>
    <? endforeach; ?>
        <tr>
            <td><h3>Сумма</h3></td>
            <td colspan="5"><h3><?= number_format($iTotalSum) ?></h3></td>
        </tr>
</table>
<br />
<br />
<? endforeach; ?>
