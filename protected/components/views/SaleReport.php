<br />
<br />
<table class="table table-condensed table-striped table-bordered">

<tr>
    <td>Название</td>
    <td>Номер карты</td>
    <td>Дата оплаты</td>
    <td>Прайс</td>
    <td>Цена факт.</td>
    <td>Скидка</td>
</tr>
<? foreach ( $aData as $k => $aDataItem ): ?>

        <? if($k): ?>
        <tr>
            <td colspan="6">&nbsp;</td>
        </tr>
        <? endif; ?>

        <tr>
            <td><?= $aDataItem['order']->item->title; ?></td>
            <td><?= $aDataItem['order']->code; ?></td>
            <td><?
                if ((int) strtotime($aDataItem['order']->is_paid_date) > 0)
                {
                    echo date("d/m/Y", strtotime($aDataItem['order']->is_paid_date));
                }
                else
                {
                    echo '&nbsp;';
                }
                ?>
            </td>
            <td><?= number_format($aDataItem['price']); ?></td>
            <td><?= number_format($aDataItem['priceDisc']); ?></td>
            <td><?= $aDataItem['order']->discount; ?>%</td>
        </tr>

        <? if($aDataItem['order']->flower_quantity): ?>
        <tr>
            <td>Цветы: <?= $aDataItem['order']->flower->flower_title; ?></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td><?= number_format($aDataItem['flower']); ?></td>
            <td><?= number_format($aDataItem['flowerDisc']); ?></td>
            <td><?= $aDataItem['order']->discount; ?>%</td>
        </tr>
        <? endif; ?>

        <? if($aDataItem['order']->card_quantity): ?>
        <tr>
            <td>Открытка: <?= $aDataItem['order']->card->card_title; ?></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td><?= number_format($aDataItem['card']); ?></td>
            <td><?= number_format($aDataItem['cardDisc']); ?></td>
            <td><?= $aDataItem['order']->discount; ?>%</td>
        </tr>
        <? endif; ?>

        <? if($aDataItem['order']->is_express_delivery): ?>
        <tr>
            <td>Экспресс доставка</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td><?= number_format($aDataItem['deliv']); ?></td>
            <td><?= number_format($aDataItem['deliv']); ?></td>
            <td>&nbsp;</td>
        </tr>
        <? endif; ?>

        <? if($aDataItem['order']->is_thematic_delivery): ?>
        <tr>
            <td>Тематическая доставка</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td><?= number_format($aDataItem['thematicDeliv']); ?></td>
            <td><?= number_format($aDataItem['thematicDeliv']); ?></td>
            <td>&nbsp;</td>
        </tr>
        <? endif; ?>

        <? if($aDataItem['order']->is_express_delivery_recipient): ?>
        <tr >
            <td>Вручить лично получателю подарка</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td><?= number_format($aDataItem['giftDeliv']); ?></td>
            <td><?= number_format($aDataItem['giftDeliv']); ?></td>
            <td>&nbsp;</td>
        </tr>
        <? endif; ?>

        <? if($aDataItem['price'] != $aDataItem['total']): ?>
        <tr style="background: #cccccc;">
            <td colspan="3">Итого:</td>
            <td><?= number_format($aDataItem['total']); ?></td>
            <td><?= number_format($aDataItem['totalDisc']); ?></td>
            <td>&nbsp;</td>
        </tr>
        <? endif; ?>

 <? endforeach; ?>

        <tr style="background: #cccccc;">
            <td colspan="3">Итого:</td>
            <td><?= number_format($aData[0]['totalSum']); ?></td>
            <td><?= number_format($aData[0]['totalSumDisc']); ?></td>
            <td>&nbsp;</td>
        </tr>
</table>
