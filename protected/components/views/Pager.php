<div class='clear'></div>
<table style="margin: 20px auto 50px;" class="main-paginator">
    <tbody><tr>
        <td width="110" style="text-align: right; padding-right: 10px;">
        <? if($this->getPager()->getCurrentPage() > 1) : ?>
            <div>
            	<a href="<?= $this->getPager()->getPageUrl($this->getPager()->getCurrentPage() - 1); ?>"><span>←</span> Предыдущая</a>
			</div>
		<? endif ?>
            <div style="padding-top: 5px;">
                <a href="<?= $this->getPager()->getPageUrl(1); ?>" style="font-size: 11px;">Первая</a>
			</div>
        </td>
        <td>
        	<div class="paginator" id='paginator'>
			</div>
		</td>
        <td width="110" style="text-align: left; padding-left: 20px;">
        <? if($this->getPager()->getCurrentPage() < $this->getPager()->getTotalPages()) : ?>
            <div>
                <a href="<?= $this->getPager()->getPageUrl($this->getPager()->getCurrentPage() + 1); ?>">Следующая <span>→</span> </a>
			</div>
		<? endif;?>
            <div style="padding-top: 5px;">
                <a href="<?= $this->getPager()->getPageUrl($this->getPager()->getTotalPages()); ?>" style="font-size: 11px;">Последняя</a>
			</div>
        </td>
    </tr>
</tbody></table>
<script type="text/javascript">
	$(function() {
	    paginator_example = new Paginator(
	        "paginator",
	        <?= $this->getPager()->getTotalPages() ?>,
	        <?= $this->getPager()->getPagesPerPage() ?>,
	        <?= $this->getPager()->getCurrentPage() ?>,
	        "<?= $this->getPager()->getUrl(); ?>"
	    );
	});
</script>
