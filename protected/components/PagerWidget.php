<?php

class PagerWidget extends CWidget
{
    /**
     * @var CPagination
     */
    public $pager;

    /**
     * @var CCustomUrlManager
     */
    private $urlManager;

	public function init()
	{
        $this->urlManager = Yii::app()->urlManager;
	}
	
	protected function getView()
	{
		return 'pager_new';
	}
	
	public function getPager()
	{
		return $this->pager;
	}

    public function getPageUrl($page)
    {
        return $this->urlManager->createUrlFromCurrent(['page' => $page]);
    }
	
	public function run()
	{
        $pager = $this->pager;
        if ($pager instanceof AbstractPager) {
            if ($pager->getTotalPages() <= 1) {
                return;
            }

            $startFrom = max(0, $pager->getCurrentPage(false) - 4);
            $startTo = min($pager->getTotalPages(), $startFrom + 10);
            $startFrom = max(0, $startTo - 10);
        } else {
            if ($pager->pageCount <= 1) {
                return;
            }

            $startFrom = max(0, $pager->currentPage - 4);
            $startTo = min($pager->pageCount, $startFrom + 10);
            $startFrom = max(0, $startTo - 10);
        }


        $this->render($this->getView(), [
            'pager' => $pager,
            'startFrom' => $startFrom,
            'startTo' => $startTo
        ]);
	}
	
}
