<?php

class FeatureSwitch extends CApplicationComponent
{

    public $features;

    protected $user;



    public function isEnabled($feature)
    {
        if (!Yii::app()->user->getId()) {
            return YII_DEBUG;
        }

        $user = Yii::app()->user->getUser();

        return YII_DEBUG || (!empty($user->feature_switches) && in_array($feature, $user->feature_switches));
    }

    public function switchFeature($feature, $enabled = false)
    {
        $features = $this->user->enabled_features;
        $available_features = array_keys($this->features);
        // remove old features from list
        $features = array_intersect($features, $available_features);
        // remove or append new feature
        if(($key = array_search($feature, $features)) !== false) {
            unset($features[$key]);
        }
        if ($enabled) {
            $features[] = $feature;
        }

        $user->enabled_features = $features;
    }

    public function getAvailableSwitchers()
    {
        return $this->features;
    }

}