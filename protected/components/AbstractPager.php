<?php

abstract class AbstractPager implements IteratorAggregate
{
	abstract public function getList();	
	protected
		$model,
		$totalRows,
		$itemsPerPage,
		$pagesPerPage;
	public function getPagesPerPage()
	{
		return $this->pagesPerPage;
	}
	
	protected function getItemsPerPage()
	{
		return $this->itemsPerPage;
	}
	
	public function getTotalRows()
	{
		return $this->totalRows;
	}
	
	protected function getOffset()
	{
		return ($this->getCurrentPage() - 1) * $this->getItemsPerPage();
	}
	
	public function getIterator()
	{
		static $iterator;
		if(is_null($iterator))
			$iterator = new ArrayIterator($this->getList());
		return $iterator;
	}
	
	public function getUrl()
	{
		return $this->getPageUrl('@PAGE');
	}
	
	public function getPageUrl($page)
	{
		return Yii::app()->urlManager->createUrlFromCurrent(array('page' => $page));
	}
	
	public function getTotalPages()
	{
		return ceil($this->getTotalRows() / $this->getItemsPerPage());
	}
	
	public function getCurrentPage()
	{
		return Yii::app()->request->getParam('page', 1);
	}

	public function getStartPage()
	{
		$offset = round($this->getPagesPerPage() / 2);
		$offseted = $this->getCurrentPage() - $offset;
		if($offseted <= 1)
			return 1;
		return $offseted;
	}
	
	public function getEndPage()
	{
		$end = $this->getStartPage() + $this->getPagesPerPage();
		if($end > $this->getTotalPages())
			return $this->getTotalPages();
		return $end;
	}
	
	public function getDisplayingPagesRange()
	{
		return range($this->getStartPage(),$this->getEndPage(),1);
	}
}
