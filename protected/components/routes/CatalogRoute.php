<?php

class CatalogRoute extends CBaseUrlRule
{

    public function createUrl($manager, $route, $params, $ampersand)
    {
        if (!preg_match('/^catalog\//Ui', $route)) {
            return false;
        }

        $city = Yii::app()->request->getQuery('_city', null);
        if (isset($params['city']) && $params['city'] instanceof City) {
            $city = $params['city'];
            unset($params['city']);
        }

        $url = '/' . ($city->isDefault ? '' : $city->name);
        if ('catalog/item' === $route) {
            if (!isset($params['perm_link'])) {
                return false;
            }

            $url .= '/' . $params['perm_link'];
            unset($params['perm_link']);
        }

        if ('catalog/search' === $route) {

            $url .= '/search';
        }

        if ('catalog/index' === $route) {
            $url .= '/';
        }

        if ('catalog/category' === $route) {
            if (!isset($params['slug'])) {
                return false;
            }

            $url .= (isset($params['parent']) ? '/'.$params['parent'] : '') . '/' . $params['slug'];
            if (isset($params['parent'])) unset($params['parent']);
            unset($params['slug']);
        }

        if (in_array($route, ['catalog/index', 'catalog/category', 'catalog/search']) && isset($params['page'])) {
            if ($params['page'] > 1) $url .= '/page' . $params['page'];
            unset($params['page']);
        }

        // build params
        $query = $manager->createPathInfo($params,'=',$ampersand);
        return ltrim($url, '/') . ($query ? '?' . $query : '');
    }

    public function parseUrl($manager, $request, $pathInfo, $rawPathInfo)
    {
        if (preg_match('/^(admin|order)/Ui', $pathInfo)) {
            return false;
        }

        $pathInfo = '/' . ltrim($pathInfo, '/');

        // strip from pages
        $params = [];
        if (preg_match('/\/page(\d+?)/Ui', $pathInfo, $matches)) {
            $_GET['page'] = intval($matches[1]);
            $pathInfo = '/' . ltrim(str_replace($matches[0], '', $pathInfo), '/');
        }

        if ("/" === $pathInfo) {
            return 'catalog/index';
        }

        if ("/search" === $pathInfo) {
            return 'catalog/search';
        }

        // check is it an item?
        $item = Item::model()->findByAttributes([
            'perm_link' => trim($pathInfo, '/')
        ]);

        if ($item) {
            $_GET['item'] = $item;
            $_GET['perm_link'] = trim($pathInfo, '/');

            return 'catalog/item';
        }

        // check is it a category?
        $path = explode('/', strtolower(trim($pathInfo, '/')));
        if (2 >= count($path)) {
            $path = array_reverse($path);
            $slug = $path[0];

            $category = Category::model()->resetScope()->findByAttributes([
                'slug' => $slug
            ]);

            if ($category) {
                $_GET['slug'] = $slug;
                return 'catalog/category';
            }
        }

        return false;
    }


}