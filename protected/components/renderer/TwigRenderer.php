<?php

class TwigRenderer extends CApplicationComponent implements IViewRenderer, \Elpresent\Yii\ViewRendererInterface
{

    public $fileExtension = '.twig';

    public $templatesDir = 'application.templates';

    public $options;

    /**
     * @var \Twig_Environment
     */
    protected $twig;


    public function init()
    {
        parent::init();

        $this->twig = new \Twig_Environment(new \Twig_Loader_Filesystem([
            Yii::getPathOfAlias($this->templatesDir)
        ]), $this->options);

        $this->twig->addExtension(new \Elpresent\Yii\Twig\Extension\YiiBridgeExtension(Yii::app()));
    }


    /**
     * Renders a view file.
     * @param CBaseController $context the controller or widget who is rendering the view file.
     * @param string $file the view file path
     * @param mixed $data the data to be passed to the view
     * @param boolean $return whether the rendering result should be returned
     * @return mixed the rendering result, or null if the rendering result is not needed.
     */
    public function renderFile($context, $file, $data, $return)
    {
        $content = $this->twig->render($file, $data);
        if (!$return) {
            echo $content;
            return '';
        }

        return $content;
    }

    public function render($template, $data)
    {
        return $this->twig->render($template, $data);
    }

    /**
     * @param $viewName
     * @return string|boolean
     */
    public function resolveViewFile($viewName)
    {
        return $viewName . $this->fileExtension;
    }


}