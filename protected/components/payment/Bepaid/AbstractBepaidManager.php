<?php

/**
 * Class AbstractBepaidManager
 */
abstract class AbstractBepaidManager extends CApplicationComponent
{
    /** Bepaid  */
    const PAYMENT_TYPE = null;
    /** @var  BepaidClient */
    private $client;
    /** @var  string */
    public $version;
    /** @var  string  */
    public $currency;

    /** @var  string */
    private $checkout;
    /** @var  string */
    private $endpoint;

    /**
     * @return BepaidClient
     */
    abstract protected function initClient();

    /**
     * @return BepaidClient
     */
    protected function getClient()
    {
        if ($this->client === null) {
            $this->client = $this->initClient();
        }

        return $this->client;
    }

    /**
     * @param string $tracking_id
     * @param Order $order
     * @return array
     */
    abstract protected function getRequest($tracking_id, Order $order);

    /**
     * @param Order $order
     * @return Bepaid
     */
    public function createInvoice(Order $order)
    {
        $invoice = new Bepaid();
        $invoice->type = static::PAYMENT_TYPE;
        $invoice->tracking_id = $this->generateTrackingId($order);
        $invoice->total = $order->price;
        $invoice->order_id = $order->id;

        $checkout = $this->getClient()->createPaymentToken('/ctp/api/checkouts',  $this->getRequest($invoice->tracking_id, $order));
        if ($checkout->checkout->token) {
            $invoice->token = $checkout->checkout->token;
            $invoice->save();

//            $this->checkout = $checkout->checkout;
            $this->endpoint = $checkout->checkout->redirect_url;
        }
        return null;
    }

    /**
     * @param string $token
     * @param string $uid
     * @return array|CActiveRecord|mixed|null
     */
    public function getInvoice($token, $uid)
    {
        $bepaid = Bepaid::model()->findByAttributes(['token' => $token]);
        if(empty($bepaid->uid) || empty($bepaid->status)){
            $checkout =  $this->getClient()->getPaymentDetails('/ctp/api/checkouts', $token);
            if($status = $checkout->checkout->status){
                return $this->processInvoice($token, $status, $uid);
            }
        } else {
            return $bepaid;
        }
    }

    /**
     * @param integer $token
     * @param string  $status
     * @param string  $uid
     * @return bool
     */
    public function processInvoice($token, $status, $uid)
    {
        $invoice = Bepaid::model()->findByAttributes(['token' => $token]);
        $invoice->status = $status;
        $invoice->uid = $uid;
        $invoice->save();
        if ($status == 'successful') {
            $order = Order::model()->findByPk($invoice->order_id);
            $order->payment_type_id = static::PAYMENT_TYPE;
            $order->is_paid = 1;
            $order->save();
        }
        return $invoice;
    }

    /**
     * @param Order $order
     * @return string
     */
    protected function orderItems(Order $order)
    {
        $names = '';
        foreach ($order->orderItems as $orderItem) {
            $names .= empty($names) ?  $orderItem->itemObj->title : ', '.$orderItem->itemObj->title;
        }

        return $names;
    }

    /**
     * @param Order $order
     * @return string
     */
    protected function generateTrackingId(Order $order)
    {
        return time() . 'ORDER' . $order->order_number . mt_rand(10, 99);
    }

    /**
     * @return string
     */
    public function getCheckout()
    {
        return $this->checkout;
    }

    /**
     * @return string
     */
    public function getEndpoint()
    {
        return $this->endpoint;
    }
}