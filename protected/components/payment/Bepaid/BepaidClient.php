<?php

class BepaidClient extends CApplicationComponent
{
    /** @var  string  */
    public $checkoutUrl;
    /** @var  string */
    public $shopId;
    /** @var  string  */
    public $shopKey;

    /**
     * @return string
     */
    protected function getAuthToken()
    {
        return $this->shopId . ':' . $this->shopKey;
    }

    /**
     * @return resource
     * @throws \Exception
     */
    private function openCurl()
    {
        $cUrl = curl_init();
        if ($cUrl !== false) {
            return $cUrl;
        }
        throw new \Exception('cURL error: The attempt to initialize cUrl failed.');
    }

    /**
     * @param string $url
     * @param array  $requestData
     * @return mixed
     * @throws \Exception
     */
    public function createPaymentToken($url, array $requestData)
    {
        $cUrl = $this->openCurl();
        $json = json_encode($requestData);
        //url and auth
        curl_setopt($cUrl, CURLOPT_URL, $this->checkoutUrl.$url);
        curl_setopt($cUrl, CURLOPT_USERPWD, $this->getAuthToken());
        //post data
        curl_setopt($cUrl, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Content-type: application/json'));
        curl_setopt($cUrl, CURLOPT_POST, 1);
        curl_setopt($cUrl, CURLOPT_POSTFIELDS, $json);
        //other
        curl_setopt($cUrl, CURLOPT_TIMEOUT, 30);
        curl_setopt($cUrl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cUrl, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($cUrl);
        $error = curl_error($cUrl);
        curl_close($cUrl);
        if ($response === false) {
            throw new \Exception("cURL error " . $error);
        }

        return json_decode($response);
    }

    /**
     * @param string $token
     * @param string $url
     * @return array|mixed
     * @throws Exception
     */
    public function getPaymentDetails($url, $token)
    {
        $cUrl = $this->openCurl();
        curl_setopt($cUrl, CURLOPT_URL, $this->addTokenToUrl($this->checkoutUrl.$url, $token));
        curl_setopt($cUrl, CURLOPT_USERPWD, $this->getAuthToken());
        curl_setopt($cUrl, CURLOPT_TIMEOUT, 30);
        curl_setopt($cUrl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cUrl, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($cUrl);
        $error = curl_error($cUrl);
        curl_close($cUrl);
        if ($response === false) {
            throw new \Exception("cURL error " . $error);
        }

        return json_decode($response);
    }

    /**
     * @param string $url
     * @param string $token
     * @return string
     */
    private function addTokenToUrl($url, $token)
    {
        return substr($url, -1) == '/' ? ($url.$token) : ($url.'/'.$token);
    }
}