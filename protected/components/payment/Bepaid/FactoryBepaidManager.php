<?php

class FactoryBepaidManager
{
    /**
     * @param $token
     * @return AbstractBepaidManager
     * @throws CHttpException
     */
    public static function getByToken($token)
    {
        $bepaid = Bepaid::model()->findByAttributes(['token' => $token]);
        if ($bepaid) {
            /** @var AbstractBepaidManager $manager */
            switch ($bepaid->type) {
                case Bepaid::TYPE_CARD:
                    return Yii::app()->getComponent('manager.bepaid.card');
                case Bepaid::TYPE_HALVA:
                    return Yii::app()->getComponent('manager.bepaid.halva');
            }
        }

        throw new CHttpException('404');
    }

}