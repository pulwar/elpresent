<?php

/**
 * Class CardBepaidManager
 */
class CardBepaidManager extends AbstractBepaidManager
{
    const PAYMENT_TYPE = Bepaid::TYPE_CARD;

    /**
     * @return BepaidClient
     */
    protected function initClient()
    {
        return Yii::app()->getComponent('client.bepaid.credit_card');
    }

    protected function getRequest($tracking_id, Order $order)
    {
        $request = [
            'checkout' => [
                'version' => $this->version,
                'transaction_type' => 'payment',
                'order' => [
                    'amount' => $order->price * 100,
                    'currency' => $this->currency,
                    'description' => $this->orderItems($order),
                    'tracking_id' => $tracking_id,
                ],
                'settings' => [
                    'success_url' => 'https://elpresent.by/order/bepaidcomplete',
                    'decline_url' => 'https://elpresent.by/order/bepaiddecline',
                    'fail_url' => 'https://elpresent.by/order/bepaiddecline',
                    'cancel_url' => 'https://elpresent.by/order/bepaidcancel',
                    'language' => 'ru',
                    'customer_fields' => [
                        'read_only' => ['first_name', 'last_name'],
                        'visible' => ['first_name', 'last_name', 'email']
                    ]
                ],
                'customer' => [
                    'email' => $order->email,
                    'first_name' => $order->name,
                    'last_name' => $order->lname,
                    'address' => $order->adr,
                    'phone' => $order->phone
                ]
            ]
        ];
        return $request;
    }
}