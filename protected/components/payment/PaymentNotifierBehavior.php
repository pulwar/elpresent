<?php

class PaymentNotifierBehavior extends CActiveRecordBehavior
{
    const MAX_USER_CASH = 1000;

    public function events()
    {
        return [
            'onOrderPaid' => 'orderPaid'
        ];
    }

    public function orderPaid(CModelEvent $event)
    {

        /** @var Order $order */
        $order = $event->sender;


        if (PaymentType::isDigitalPayment($order->payment_type_id)) {
            if ( $order->type == Order::TYPE_DIGITAL && empty($order->code) ) {

               $order->generateUniqCode();
//                foreach($order->orderItems as $orderItem) {
//                    /** @var OrderItems $orderItem */
//                    $orderItem->generateUniqCode();
//                    $orderItem->save();
//                }
//
                //$order->save();

                // send certificate
                $this->sendCertificate($order);
            }

        }

        // Если у заказа есть юзер, накапливаем для него баллы
        if (!empty($order->user_id) && ($order->is_paid_date == 0)) {
            $user = User::model()->findByPk($order->user_id);
            // списание накопленных gдолжно сразу происходить а не при подтверждении заказа
            //$user->cash = $user->cash - $order->user_cash;
            // накопление баллов от суммы только подарка
            $user_cash = $user->cash + Order::getCountPoint($order->price);
            // если больше чем надо
            if ($user_cash >= self::MAX_USER_CASH) {
                $user_cash = self::MAX_USER_CASH;
            }

            $user->cash = $user_cash;
            $user->save();
        }

        $order->is_paid_date = date('Y-m-d H:i:s');
        $order->save();
    }

    private function sendCertificate(Order $order)
    {
        /** @var PdfGenerator $pdfGenerator */
        $pdfGenerator = Yii::app()->pdfGenerator;
        $pdf = $pdfGenerator->generateDigitalCertificate($order);

        /** @var Mailer $mailer */
        $mailer = Yii::app()->mailer;
        $message = $mailer->createMessageFromView('Электронный сертификат',
            Yii::getPathOfAlias('application.views.email') . '/digital_certificate.php', [
                'order' => $order
            ]
        );
        $message->setTo($order->email);
        $message->attach($mailer->createAttachment($pdf)->setFilename('gift_certificate.pdf'));
        $mailer->send($message);

        // дублирование письма
        $mailer_double = Yii::app()->mailer;
        $message_double = $mailer_double->createMessageFromView('Электронный сертификат',
            Yii::getPathOfAlias('application.views.email') . '/digital_certificate.php', [
                'order' => $order
            ]
        );

        $message_double->setTo(Yii::app()->params['review']['emailTo']);
        $message_double->attach($mailer_double->createAttachment($pdf)->setFilename('gift_certificate.pdf'));
        $mailer_double->send($message_double);

    }
}