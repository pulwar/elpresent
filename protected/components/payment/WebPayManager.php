<?php


class WebPayManager extends CApplicationComponent
{

    public $debug;
    public $username;
    public $password;
    public $storeId;
    public $store;
    public $currencyId;
    public $secretKey;

    protected $endpoint = 'https://billing.webpay.by';
    protected $sandbox_endpoint = 'https://sandbox.webpay.by';

    public function getEndpoint()
    {
        return $this->debug ? $this->sandbox_endpoint : $this->endpoint;
    }

    public function getStoreId()
    {
        return $this->storeId;
    }

    public function getCurrencyId()
    {
        return $this->currencyId;
    }

    public function getStore()
    {
        return $this->store;
    }

    public function isDebug()
    {
        return $this->debug;
    }

    /**
     * @param Order $order
     * @return Webpay
     */
    public function createInvoice(Order $order)
    {
        $invoice = new Webpay();
        $invoice->wsb_order_num = time() . 'ORDER' . $order->order_number . mt_rand(10, 99);
        $invoice->wsb_seed = mt_rand(100000, 100000000);
        $invoice->wsb_total = $order->price;
        $invoice->wsb_signature = $this->generateSignature($invoice);

        $invoice->order_id = $order->id;
        $invoice->save();

		return $invoice;
    }

    /**
     * @param $orderId
     * @return Webpay
     */
    public function getInvoice($orderId)
    {
        return Webpay::model()->findByAttributes(['wsb_order_num' => $orderId]);
    }

    /**
     * @param Webpay $invoice
     * @param integer $transactionId
     * @return bool
     */
    public function processInvoice(Webpay $invoice, $transactionId)
    {
        $transactionDetails = $this->getTransactionDetails($transactionId);

        $data = array_slice($transactionDetails, 0, 8);
        if (!$this->validateTransaction($data, $transactionDetails['wsb_signature'])) {
            return false;
        }

        if (!in_array(intval($data['payment_type']), [1, 4])) {
            return false;
        }

        $invoice->wsb_tid = $transactionId;
        $invoice->save();

        return true;
    }

    /**
     * @param CHttpRequest $request
     * @return bool
     */
	public function validateRequest(CHttpRequest $request)
	{
		$data = array_map(function ($field) use ($request) {
			return $request->getParam($field);
		}, [
			'batch_timestamp',
			'currency_id',
			'amount',
			'payment_method',
			'order_id',
			'site_order_id',
			'transaction_id',
			'payment_type',
			'rrn'
		]);

		return $request->getParam('wsb_signature') === md5(implode('', $data) . $this->secretKey);
	}

    /**
     * @param array $data
     * @param string $signature
     * @return bool
     */
    public function validateTransaction(array $data, $signature)
    {
        return $signature === md5(implode('', $data).$this->secretKey);
    }

    /**
     * @param $transactionId
     * @return array
     */
    public function getTransactionDetails($transactionId)
    {
        $postData = '*API=&API_XML_REQUEST=' . urlencode('
             <?xml version="1.0" encoding="ISO-8859-1" ?>
             <wsb_api_request>
                       <command>get_transaction</command>
                       <authorization>
                               <username>' . $this->username . '</username>
                               <password>' . $this->password . '</password>
                       </authorization>
                       <fields>
                               <transaction_id>' . $transactionId . '
                               </transaction_id>
                       </fields>
             </wsb_api_request>');


        $curl = curl_init($this->debug ? $this->sandbox_endpoint : $this->endpoint);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        $response = simplexml_load_string(curl_exec($curl));
		//html::pr($response, 1);
        curl_close($curl);

        return [
            'transaction_id' => (string) $response->fields->transaction_id,
            'batch_timestamp' => (string) $response->fields->batch_timestamp,
            'currency_id' => (string) $response->fields->currency_id,
            'amount' => (string) $response->fields->amount,
            'payment_method' => (string) $response->fields->payment_method,
            'payment_type' => (string) $response->fields->payment_type,
            'order_id' => (string) $response->fields->order_id,
            'rrn' => (string) $response->fields->rrn,
            'wsb_signature' => (string) $response->fields->wsb_signature,
        ];
    }

    /**
     * @param WebPay $invoice
     * @return string
     */
    private function generateSignature(WebPay $invoice)
    {
        return md5(implode('', [
            $invoice->wsb_seed,
            $this->storeId,
            $invoice->wsb_order_num,
            $this->debug ? '1' : '0',
            $this->currencyId,
            $invoice->wsb_total,
            $this->secretKey
        ]));
    }

}