<? class SqlModelPager extends AbstractPager
{
	protected
		$list;
	public function __construct($sql,$model,$itemsPerPage = 10,$pagesPerPage = 10)
	{
		$this->model = $model;
		$this->itemsPerPage = $itemsPerPage;
		$this->pagesPerPage = $pagesPerPage;
		$this->list = $this->queryDb($this->prepareSql($sql));
	}
	
	protected function prepareSql($sql)
	{
		$offset = $this->getOffset();
		$limit = $this->getItemsPerPage();
		if(!preg_match('/^SELECT\w*?SQL_CALC_FOUND_ROWS/i',$sql))
			$sql = preg_replace('/^SELECT\w*?/i','SELECT SQL_CALC_FOUND_ROWS',$sql);
		
		return $sql . " LIMIT {$offset},{$limit} ";
	}
	
	protected function queryDb($sql)
	{
		static $res;
		if(is_null($res))
		{
			$res = $this->model->findAllBySql($sql);
			$this->totalRows = Yii::app()->db->createCommand("SELECT FOUND_ROWS() FROM DUAL")->queryScalar();
		}
		return $res;
	}
	
	public function getList()
	{
		return $this->list;
	}
}
