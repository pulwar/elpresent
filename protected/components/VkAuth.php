<?php

class VkAuth extends CApplicationComponent
{
	/**
	 * ID приложения
	 *
	 * @var string
	 */
	const client_id = '4976462';

	/**
	 * Защищённый ключ
	 *
	 * @var string
	 */
	const client_secret = '2c0f437cPy7WISyILVk0';

	/**
	 * Адрес метода авторизации на сайте
	 *
	 * @var string
	 */
	const redirect_uri = 'https://elpresent.by/profile/VkAuth';

	/**
	 *
	 */
	const url = 'http://oauth.vk.com/authorize';

	/**
	 * Информация о профайле юзера из контакта
	 *
	 * @var array
	 */
	private $_userInfo = [];

	/**
	 * @var array
	 */
	private static $_params = array(
		'client_id' => self::client_id,
		'redirect_uri' => self::redirect_uri,
		'response_type' => 'code',
		'scope' => 'email'
	);

	public function init()
	{
		$link = self::url . '?' . urldecode(http_build_query(self::$_params));

		if (!empty($_GET['code'])) {
			$result = false;
			$params = array(
				'client_id' => self::client_id,
				'client_secret' => self::client_secret,
				'code' => $_GET['code'],
				'redirect_uri' => self::redirect_uri,
			);

			$token = json_decode(file_get_contents('https://oauth.vk.com/access_token' . '?' . urldecode(http_build_query($params))), true);

			if (!empty($token['access_token'])) {
				$params = array(
					'uids' => $token['user_id'],
					'fields' => 'uid,first_name,last_name,screen_name,sex,bdate,photo_big,city,email',
					'access_token' => $token['access_token'],
				);
			}

			$userInfo = json_decode(file_get_contents('https://api.vk.com/method/users.get' . '?' . urldecode(http_build_query($params))), true);

			if (!empty($userInfo['response'][0]['uid'])) {
				$userInfo = $userInfo['response'][0];
				if (!empty($token['email'])) {
					$userInfo['email'] = $token['email'];
				}
				$result = true;
			}

			if ($result) {
				$this->_userInfo = $userInfo;
			}
		}else{
			header("Location: $link");
		}
	}

	/**
	 * @return array
	 */
	public function getUserInfo()
	{
		return $this->_userInfo;
	}
}