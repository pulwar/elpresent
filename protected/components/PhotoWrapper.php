<?php

class PhotoWrapperException extends Exception
{

}

class PhotoWrapper
{

    private $filepath;
    private $actual_path;
    private $thumb_path;
    private $ext;
    private $name;
    private $thumb;
    private $image;
    private $saved;

    static protected function getAllowedExts()
    {
        return array('jpg', 'png', 'gif', 'jpeg', 'bmp');
    }

    static public function getFileExt($file)
    {
        return pathinfo($file, PATHINFO_EXTENSION);
    }

    static public function isCorrectExt($image, $allowed = null)
    {
        if (is_null($allowed))
            $allowed = self::getAllowedExts();

        return in_array(strtolower(self::getFileExt($image['name'])), $allowed);
    }

    public function __construct($item_id)
    {
        Yii::app()->setImport(array('application.extensions.image.*'));
        $this->name = $item_id . '_' . uniqid();
    }

    public function prepare($image)
    {
        if (!$image)
        {
            throw new PhotoWrapperException(_('no imagepath given'));
        }
        if (!is_array($image) && !$image['tmp_name'])
        {
            throw new PhotoWrapperException(strtr(_("file {filepath} does not exists"), array('{filepath}' => $image['tmp_name'])));
        }
        if (!self::isCorrectExt($image))
            throw new PhotoWrapperException(_('not allowed image extension ' . self::getFileExt($image)));
        $this->ext = self::getFileExt($image['name']);
        $filepath = Yii::getPathOfAlias('webroot') . "/images/item/photo/original/{$this->name}.{$this->ext}";
        if (move_uploaded_file($image['tmp_name'], $filepath))
            $this->filepath = $filepath;
        return true;
    }

    public function process()
    {
        $this->thumb_path = Yii::getPathOfAlias('webroot') . "/images/item/photo/thumb/{$this->name}.{$this->ext}";
        $this->thumb = new Image($this->filepath);
        if (!file_exists($this->thumb_path))
        {
            $this->thumb->resize(100, 67, Image::AUTO)->crop(100, 67);
        }

        $this->actual_path = Yii::getPathOfAlias('webroot') . "/images/item/photo/{$this->name}.{$this->ext}";
        $this->image = new Image($this->filepath);
        if (!file_exists($this->actual_path))
        {
            $this->image->resize(500, 333, Image::AUTO)->crop(500, 333);
        }
    }

    public function save()
    {
        $this->thumb->save($this->thumb_path);
        $this->image->save($this->actual_path);
        $this->saved = true;
    }

    public function delete()
    {
        return true;
    }

    public function __toString()
    {
        return $this->filepath;
    }

    public function getPath()
    {
        return "{$this->name}.{$this->ext}";
    }

    public function __destruct()
    {
        if (!$this->saved)
        {
            if (file_exists($this->filepath))
                unlink($this->filepath);
            if (file_exists($this->actual_path))
                unlink($this->actual_path);
            if (file_exists($this->thumb_path))
                unlink($this->thumb_path);
        }
    }

}
?>
