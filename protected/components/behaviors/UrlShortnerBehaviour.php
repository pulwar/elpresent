<?php

class UrlShortnerBehaviour extends CActiveRecordBehavior
{

    public $prefix;

    /** @var  UrlShortner */
    protected $urlShortner;

    public function __construct()
    {
        $this->urlShortner = Yii::app()->urlShortner;
    }


    public function getPageHash()
    {
        return $this->urlShortner->encode($this->owner->primaryKey, 'i');
    }


}