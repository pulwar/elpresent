<?php

/**
 * Class TransliterationBehaviour
 */
class TransliterationBehaviour extends CBehavior
{
    /**
     * @param string $str
     * @return string
     */
    public function transliterate($str)
    {
        $dictionary = array(
            "А"=>"A","Б"=>"B","В"=>"V","Г"=>"G",
            "Д"=>"D","Е"=>"E","Ж"=>"J","З"=>"Z","И"=>"I",
            "Й"=>"Y","К"=>"K","Л"=>"L","М"=>"M","Н"=>"N",
            "О"=>"O","П"=>"P","Р"=>"R","С"=>"S","Т"=>"T",
            "У"=>"U","Ф"=>"F","Х"=>"H","Ц"=>"TS","Ч"=>"CH",
            "Ш"=>"SH","Щ"=>"SCH","Ъ"=>"","Ы"=>"YI","Ь"=>"",
            "Э"=>"E","Ю"=>"YU","Я"=>"YA","а"=>"a","б"=>"b",
            "в"=>"v","г"=>"g","д"=>"d","е"=>"e","ж"=>"j",
            "з"=>"z","и"=>"i","й"=>"y","к"=>"k","л"=>"l",
            "м"=>"m","н"=>"n","о"=>"o","п"=>"p","р"=>"r",
            "с"=>"s","т"=>"t","у"=>"u","ф"=>"f","х"=>"h",
            "ц"=>"ts","ч"=>"ch","ш"=>"sh","щ"=>"sch","ъ"=>"y",
            "ы"=>"yi","ь"=>"","э"=>"e","ю"=>"yu","я"=>"ya"
        );

        return strtr($str, $dictionary);
    }

    /**
     * @param string $str
     * @return string
     */
    public function normalizeUri($str)
    {
        $dictionary = array(
            '—' => '-', ' ' => '-', '–' => '-'
        );

        $normalizedStr = strtr(strtolower($this->transliterate(trim($str))), $dictionary);
        $normalizedStr = preg_replace('/([\-]+)/', '-', $normalizedStr);
        return preg_replace('/[^a-z0-9\-\_]/', '', $normalizedStr); //remove special characters
    }

}