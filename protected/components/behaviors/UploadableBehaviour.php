<?php

class UploadableBehaviour extends CActiveRecordBehavior
{

    public $fields;

    public $saveToDirectory;

    protected $originalValues = [];

    protected $webRoot;

    public function __construct()
    {
        Yii::import('application.extensions.image.Image');
        $this->webRoot = Yii::getPathOfAlias('webroot');
    }

    public function events()
    {
        return [
            'onBeforeValidate' => 'bind',
            'onAfterValidate' => 'processImages',
        ];
    }

    public function bind()
    {
        foreach ($this->fields as $field => $options) {
            $this->originalValues[$field] = $this->owner->$field; // save old value
        }
    }

    public function processImages()
    {
        $images = Yii::app()->request->getParam('Item')['image'] ?? [];
        foreach ($images as $imageType => $image) {
            if (empty($image)) {
                continue;
            }

            $imageData = base64_decode(str_replace('data:image/jpeg;base64,', '', $image));
            $imageName = uniqid() . '.jpg';
            $path = sprintf('%s/%s/%s', $this->webRoot, trim($this->saveToDirectory, DIRECTORY_SEPARATOR), ltrim($imageName, '/'));
            file_put_contents($path, $imageData);
            $this->owner->$imageType = substr(substr($path, strlen($this->webRoot)), 1);
        }
    }

}