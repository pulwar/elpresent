<?php

class LinkSaverBehaviour extends CActiveRecordBehavior
{

    public $binders;

    protected $relatedData = [];

    public function events()
    {
        return [
            'onAfterSave' => 'saveRelatedData'
        ];
    }

    public function saveRelatedData()
    {
        foreach ($this->relatedData as $binderName => $data) {
            if (!isset($this->binders[$binderName]) || !is_callable($this->binders[$binderName])) {
                continue;
            }

            $binder = $this->binders[$binderName];
            call_user_func($binder, $this->owner, $data);
        }
    }

    public function setRelatedData($relation, array $data = [])
    {
        $this->relatedData[$relation] = $data;
    }

}