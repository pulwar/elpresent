<?php

/**
 * Class CachePurger
 *
 *
 */
class CachePurger extends CActiveRecordBehavior
{
    /**
     * @var array
     */
    public $tags = [];

    public function events()
    {
        return [
            'onAfterSave' => 'purgeCache',
        ];
    }

    public function purgeCache()
    {
        $tags = is_callable($this->tags) ?
            call_user_func($this->tags, $this->owner) : $this->tags;

        if (empty($tags)) {
            return;
        }

        /** @var CCache $cache */
        $cache = Yii::app()->cache;
        foreach($tags as $tag) {
            $cache->delete($tag);
        }
    }

}