<?php

class WysiwygTidyBehaviour extends CBehavior
{

    /**
     * function obscured unused WYSIWYG tags
     */
    public static function clearWYSIWYG($string)
    {
        $allMatches = [];

        if (preg_match_all('#<!--.*?-->#ums', $string, $matches))
        {
            $allMatches = array_merge($allMatches, $matches[0]);
        }
        if (preg_match_all('#<xml>.*?</xml>#ums', $string, $matches))
        {
            $allMatches = array_merge($allMatches, $matches[0]);
        }
        if (preg_match_all('#<link\s+?href="file.+?/>#ums', $string, $matches))
        {
            $allMatches = array_merge($allMatches, $matches[0]);
        }

        if (count($allMatches))
        {
            $string = str_replace($allMatches, array_fill(0, count($allMatches), ''), $string, $i);
        }

        $string = str_replace("\n\r", '', $string);
        $string = str_replace("\n", '', $string);

        return $string;
    }

}