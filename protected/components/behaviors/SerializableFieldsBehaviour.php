<?php

class SerializableFieldsBehaviour extends CActiveRecordBehavior
{

    public $fields;

    protected $relatedData = [];

    public function events()
    {
        return [
            'onBeforeSave' => 'serializeData',
            'onAfterSave' => 'unserializeData',
            'onAfterFind' => 'unserializeData'
        ];
    }

    public function serializeData()
    {
        foreach ($this->fields as $field) {
            $this->owner->$field = serialize($this->owner->$field);
        }
    }

    public function unserializeData()
    {
        foreach ($this->fields as $field) {
            if (!empty($this->owner->$field)) {
                $this->owner->$field = unserialize($this->owner->$field);
                if (!$this->owner->$field) {
                    $i = 0;
                }
            } else {
                $j = 4;
            }
        }
    }

}