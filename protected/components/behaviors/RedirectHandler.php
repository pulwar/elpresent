<?php

/**
 * Class RedirectHandler
 */
class RedirectHandler extends CBehavior
{

    private $url;

    public function events()
    {
        return array(
            'onException' => 'checkRedirection',
        );
    }

    public function setUrl($url)
    {
        // remove suffix if we have one
        $suffix = Yii::app()->urlManager->urlSuffix;
        if (substr($url, -strlen($suffix)) == $suffix) {
            $url = substr($url, 0, -strlen($suffix));
        }

        $this->url = $url;
    }

    public function checkRedirection(CExceptionEvent $event)
    {

	return true;

        if (!in_array($event->exception->statusCode, array(400, 404))) {
            return true;
        }

        $request = Yii::app()->request;
        $uri = $request->getUrl();
        $this->setUrl($uri);

        if ($target = $this->handleInternalRedirection()) {
            Yii::app()->request->redirect($target, true, 301);
        }

        return true;
    }



    /**
     * After we changed structure we need to handle all this old links...
     */
    protected function handleInternalRedirection()
    {
        $url = $this->url;
        $params = [];

        // clear url
        if (preg_match('/\/page\/@page/Ui', $url, $matches)) {
            $url = str_replace($matches[0], '', $url);
        }

        $url = $this->handleParams($url, '/\/page\/(\d+?)/Ui',function ($value) use (&$params) {
            if ($value != 0) {
                $params['page'] = $value;
            }
        });
        $url = $this->handleParams($url, '/\/countPage\/(\d+?)/Ui', 'perPage', $params);
        $url = $this->handleParams($url, '/\/sortPrice\/(up|down)/Ui', function ($value) use (&$params) {
            $params['sort'] = 'up' == strtolower($value) ? 'asc' : 'desc';
        });

        if (preg_match('/^\/(catalog|site)\/index/Ui', $url)) {
            return Yii::app()->createUrl('catalog/index', $params);
        }

        // handle catalog
        if (preg_match('/^\/catalog\/category/Ui', $url)) {
            if (preg_match('/\/sub\/([^\/\.]+)/', $url, $matches)) {
                return $this->handleSubCategoryRedirection($matches[1], $params);
            }

            if (preg_match('/\/category_id\/(\d+)/', $url, $matches)) {
                return $this->handleCategoryRedirection(intval($matches[1]), $params);
            }
        }

        if (preg_match('/^\/catalog\/item\/item_id\/(\d+?)/Ui', $url, $matches)) {
            $item = Item::model()->resetScope()->findByPk($matches[1]);
            if (!$item) {
                return false;
            }
            return Yii::app()->urlManager->createUrl('catalog/item', ['perm_link' => $item->perm_link]);
        }

        // handle static pages
        if (preg_match('/pages\/([^\/]+?)/Ui', $url, $matches)) {
            return Yii::app()->urlManager->createUrl('pages/show', ['alias' => $matches[1]]);
        }

        if (preg_match('/\/site\/(\w+?)/Ui', $url, $matches)) {
            $action = $matches[1] === 'contact' ? 'contacts' : $matches[1];
            if (in_array($action, ['contacts', 'discount', 'faq', 'questionnaire'])) {
                $url =  Yii::app()->urlManager->createUrl('site/' . $action);
                return $url;
            }
        }

        if (preg_match('/\.html$/Ui', $url, $matches)) {
            return substr($url, 0, -5);
        }

        return false;
    }

    private function handleCategoryRedirection($id, array $params = [])
    {
        // get categories mapping
        if (!file_exists(Yii::app()->getRuntimePath() . '/categories_mapping.php')) {
            return false;
        }

        $mapping = require Yii::app()->getRuntimePath() . '/categories_mapping.php';

        // try to find new id
        if (!isset($mapping[$id])) {
            return false;
        }

        $category = Category::model()->findByPk($mapping[$id]);
        if (!$category) {
            return false;
        }

        return Yii::app()->urlManager->createUrl('catalog/category', array_merge($params, ['slug' => $category->slug]));
    }

    private function handleSubCategoryRedirection($link, array $params = [])
    {
        $category = Category::model()->resetScope()->findByAttributes(['slug' => $link]);
        if (!$category || !$category->parent) {
            return false;
        }

        return Yii::app()->urlManager->createUrl('catalog/category', array_merge($params, [
            'parent' => $category->parent->slug,
            'slug' => $category->slug
        ]));
    }

    private function handleParams($url, $regexp, $param, &$params=null) {
        if (preg_match_all($regexp, $url, $matches)) {
            // we need just strip this junk
            if (count($matches[0]) == 1) {
                if (is_callable($param)) {
                    $param($matches[1][0]);
                } elseif ($params !== null) {
                    $params[$param] = $param == 'perPage' && 40 == $matches[1][0] ? 'all' : $matches[1][0];
                }
            }

            foreach ($matches[0] as $search) {
                $url = str_replace($search, '', $url);
            }
        }

        return $url;
    }

}
