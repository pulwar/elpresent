<?php

class Mailer extends CApplicationComponent
{

    public $debug;

    public $spoolPath;

    /**
     * @var \Swift_Mailer
     */
    private $mailer;


    public function init()
    {
        parent::init();
        $transport = Swift_SendmailTransport::newInstance();

        $this->mailer = Swift_Mailer::newInstance($transport);
    }

    /**
     * @param string null $subject
     * @param string null $body
     * @param string null $contentType
     * @param string null $charset
     * @return Swift_Message
     */
    public function createMessage($subject = null, $body = null, $contentType = null, $charset = null)
    {
        $message = Swift_Message::newInstance($subject, $body, $contentType, $charset);
        $message->setFrom(Settings::getValueByAlias('site_name'), 'Elpresent.by');

        return $message;
    }

    /**
     * @param $subject
     * @param $view
     * @param null $data
     * @return Swift_Message
     * @throws CException
     */
    public function createMessageFromView($subject, $view, $data=null)
    {
        $html = Yii::app()->controller->renderFile($view, $data, true);


        return $this->createMessage($subject, $html, 'text/html');
    }

    /**
     * @param Swift_Mime_Message $message
     * @param array|null $failedRecipients An array of failures by-reference
     * @return integer
     */
    public function send(Swift_Mime_Message $message, &$failedRecipients = null)
    {
        return $this->mailer->send($message, $failedRecipients);
    }

    /**
     * @param $file
     * @return Swift_Mime_Attachment
     */
    public function createAttachment($file)
    {
        return Swift_Attachment::fromPath($file);
    }

}