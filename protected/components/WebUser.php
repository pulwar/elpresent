<?php

class WebUser extends CWebUser
{

    private $_model = null;
    private static $oUser;

    public function getRole()
    {
        if ($user = $this->getModel())
        {
            // в таблице User есть поле role
            return $user->user_role;
        }

        return '';
    }

    private function getModel()
    {
        if (!$this->isGuest && $this->_model === null)
        {
            $this->_model = User::model()->findByPk($this->id);
        }

        return $this->_model;
    }

    public static function instance()
    {
        if (is_null(self::$oUser))
        {
            self::$oUser = User::model()->findByPk(Yii::app()->user->getId());
        }

        return self::$oUser;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return self::instance();
    }

    public function getFullName()
    {
        return $this->getUser()->getFullName();
    }



}