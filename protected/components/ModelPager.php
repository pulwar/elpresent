<?php

class ModelPager extends AbstractPager
{

    public function __construct($model, $itemsPerPage = 10, $pagesPerPage = 10)
    {
        $this->model = $model;
        $this->itemsPerPage = $itemsPerPage;
        $this->pagesPerPage = $pagesPerPage;
        $dbCriteria = clone $model->getDbCriteria();
        $this->totalRows = $model->count();
        $model->getDbCriteria()->mergeWith($dbCriteria);
    }

    public function getList()
    {
        $criteria = $this->model->getDbCriteria();

        if (get_class($this->model) == 'Category')
        {
            $criteria->order = '`position` ASC';
        }
        $criteria->limit = $this->getItemsPerPage();
        $criteria->offset = $this->getOffset();
        return $this->model->findAll();
    }

}
