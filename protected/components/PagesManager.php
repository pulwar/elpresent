<?php

class PagesManager extends CApplicationComponent
{	
	public function getLink($alias, $noFollow = false)
	{
		$page = TextRecord::model()->findByAttributes(array('alias'=>$alias));
        if(!$page || !$page->isEnabled())
            return '';
        return $page->getLink($noFollow);
	}
	
	public function isActive($alias)
	{
		return Yii::app()->controller->id == 'pages' && Yii::app()->controller->action->id == $alias;
	}
}
