<?php

class ApcCache extends CApcCache
{
    /**
     * Initializes this application component.
     * This method is required by the {@link IApplicationComponent} interface.
     * It checks the availability of APC.
     * @throws CException if APC cache extension is not loaded or is disabled.
     */
    public function init()
    {
        if($this->keyPrefix===null)
            $this->keyPrefix=Yii::app()->getId();

        if(!extension_loaded('apc') && !extension_loaded('apcu'))
            throw new CException(Yii::t('yii','CApcCache requires PHP apc extension to be loaded.'));
    }
}
