<?php

/*
 * EasyPay Class
 */

class EasyPay
{
	protected static $mer_no = 'ok0711';
	protected static $pass = 't6RD97bVoM';
	protected static $wsdl = 'https://ssl.easypay.by/xml/easypay.wsdl';
	//protected static $wsdl = 'https://ssl.easypay.by/test/test.wsdl';

	public static function CreateInvoice(Order $order, $card)
	{
		$names = [];
		foreach ($order->orderItems as $orderItem) {
			$names[] = $orderItem->itemObj->title;
		}

		$title = str_replace (['"'], '', implode(',', $names));
		$title = substr(iconv('UTF-8', 'windows-1251', addslashes($title)), 0, 50);
		$params_EP_CreateInvoice = array(
			'mer_no' => self::$mer_no, //поставщик
			'pass' => self::$pass, //пароль
			'order' => $order->id, //номер счета
			'sum' => $order->price, //сумма в белорусских рублях
			'exp' => '4', //время жизни счета
			'card' => $card, //идентификатор электронного кошелька EasyPay
			'comment' => $title, //комментарий счета (до 50 симв.)
			//'info'      =>substr('Информация о счете', 0 , 2000), //подробный ком. (до 2000 с.)
			//'xml'       =>'<xml>Это xml-данные</xml>', //xml-данные
		);
		$client = new SoapClient(self::$wsdl, array('trace' => 1, 'encoding' => 'windows-1251'));

		try {
			//вызов функции EP_CreateInvoice
			$answer = $client->EP_CreateInvoice($params_EP_CreateInvoice);
		} catch (SoapFault $fault) {

		}

		if (200 == $answer->status->code) {
			return 200;
		} else {
			return iconv('windows-1251', 'UTF-8', $answer->status->message);
		}
	}

	public static function IsInvoicePaid($orderId)
	{
		$params_EP_IsInvoicePaid = array(
			'mer_no' => self::$mer_no, //поставщик
			'pass' => self::$pass, //пароль
			'order' => $orderId
		);
		$client = new SoapClient(self::$wsdl, array('trace' => 1, 'encoding' => 'windows-1251'));

		try {
			$answer = $client->EP_IsInvoicePaid($params_EP_IsInvoicePaid);
		} catch (SoapFault $fault) {

		}

		return $answer->status->code;

		/*switch ($answer->status->code) {
			case 200:
				$response = "счет оплачен";
				break;
			case 211:
				$response = "счет не оплачен";
				break;
			case 503:
				$response = "такой счет не существует в системе (Покупатель может самостоятельно удалить неоплаченный счет из системы)";
				break;
			case 506:
				$response = "счет просрочен";
				break;
			case 517:
				$response = "счет отменен";
				break;
			default:
				$response = $answer->status->code . ' - ' . iconv('windows-1251', 'UTF-8', $answer->status->message);
		}
		return $response;*/

	}

	public static function GetPaidInvoicesExt($orderId)
	{
		$params_EP_GetPaidInvoicesExt = array(
			'mer_no' => self::$mer_no, //Поставщик
			'pass' => self::$pass, //пароль
			'order' => $orderId //номер счета. 0 - для получения первых 100 оплаченных счетов.
		);

		$client = new SoapClient(self::$wsdl, array('trace' => 1, 'encoding' => 'windows-1251'));

		try {
			//вызов функции EP_CreateInvoice
			$answer = $client->EP_GetPaidInvoicesExt($params_EP_GetPaidInvoicesExt);
		} catch (SoapFault $fault) {
			print "ExeptionIsCatchedByUser!<br>faultcode: $fault->faultcode;
			faultstring: $fault->faultstring<br>";
		}
		return $answer->invoices_ext;

	}

}
