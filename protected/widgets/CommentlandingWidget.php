<?php
class CommentlandingWidget extends CWidget
{

    public function init()
    {

    }

    public function run()
    {

        //if(! $comments = Yii::app()->cache->get('comments_widget'))
        //{
            $criteria = new CDbCriteria();
            $criteria->condition = 'visibility = 1';
            $criteria->limit = 10;
            $criteria->order = 'contact_date DESC';
            $comments = Contact::model()->findAll($criteria);
           // Yii::app()->cache->set('comments_widget' ,$comments, 3600);
        //}

        if(empty($comments)){
            return;
        }

        $this->render('CommentlandingWidget', array('comments' => $comments));
    }
}