<?php

class SearchWordWidget extends CWidget
{
	protected $query;
                
    public function init()
	{                    
        $this->query = htmlspecialchars(Yii::app()->request->getParam('q'));
                        
	}
	
	public function run()
	{            		
        $this->render('SearchWordWidget',['query' => htmlspecialchars(Yii::app()->request->getParam('q'))]);
	}	
}
