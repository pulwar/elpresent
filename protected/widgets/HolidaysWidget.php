<? 
class HolidaysWidget extends CWidget
{
    protected
    $list;

    public function init()
    {
//        $this->list = HolidayDay::model()->findAllBySql("
//		SELECT DISTINCT hd.id, date, concat(YEAR(CURDATE()),SUBSTR(hd.date,5,6)), CONCAT(YEAR(CURDATE()),SUBSTR(hd.date,5,6)) FROM
//		el_holiday_day as hd JOIN el_holiday as h ON hd.`id` = h.`day_id`
//		JOIN el_holiday_birthday as hb ON h.`id` = hb.`day_id`
//		WHERE CONCAT(YEAR(CURDATE()), SUBSTR(hd.`date`,5,6)) >= NOW() AND SUBSTR(hd.date,1,4)='2010'
//		GROUP BY  hd.`id`
//		ORDER BY hd.`date`
//		LIMIT 3");

        $this->list = HolidayDay::model()->findAllBySql("
		SELECT hd.id, hd.date, CONCAT(YEAR(CURDATE()),SUBSTR(hd.date,5,6)), CONCAT(YEAR(CURDATE()),SUBSTR(hd.date,5,6))
                FROM el_holiday_day AS hd
                JOIN el_holiday AS h ON hd.`id` = h.`day_id`
                JOIN el_holiday_birthday AS hb ON hd.`id` = hb.`day_id`
                WHERE YEAR(hd.date)=YEAR(NOW()) AND UNIX_TIMESTAMP(CONCAT(YEAR(CURDATE()),SUBSTR(hd.`date`,5,6))) > UNIX_TIMESTAMP(NOW() -INTERVAL 1 DAY)
                GROUP BY hd.`id`
                ORDER BY hd.`date`
                LIMIT 3");
    }

    public function run()
    {
        $this->render('HolidaysWidget', array('list' => $this->list));
    }
}
