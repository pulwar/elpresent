<?php

class NewSearchWidget extends CWidget
{
	public $view;

	public function init()
	{

	}

	public function run()
	{
        $search_request = array(
            'q' =>  Yii::app()->request->getParam('q'),
            'prices' => Yii::app()->request->getQuery('prices', []),
            'features' => Yii::app()->request->getQuery('features', []),
            'options' => Yii::app()->request->getQuery('options', []),
            'is_new' =>  Yii::app()->request->getParam('is_new'),
            'off_price' =>  Yii::app()->request->getParam('off_price'),
        );

	    $reasons = FeatureValue::model()->findAll(array(
            'select'=>'*',
            'condition'=>'`available`=:available AND `feature_id`=:feature_id',
            'params'=>array(':available' => 1, ':feature_id' => 7),
            'order'=> '`feature_value_value`',
        ));

        $recipients = FeatureValue::model()->findAll(array(
            'select'=>'*',
            'condition'=>'`available`=:available AND `feature_id`=:feature_id',
            'params'=>array(':available' => 1, ':feature_id' => 6),
            'order'=> '`feature_value_value`',
        ));

        $this->render('NewSearchWidget',array(
            'reasons' => $reasons,
            'recipients' => $recipients,
            'type_view' => $this->view,
            'search_request'    => $search_request
        ));
	}
}
