<? 
class RecommendWidget extends CWidget
{
    public function init()
    {
            
    }
    
    public function run()
    {
        $list = new ModelPager(Item::model()->recommend(),10);
        
        $this->render('RecommendWidget', array
        (
            'list' => $list    
        ));
    }
}