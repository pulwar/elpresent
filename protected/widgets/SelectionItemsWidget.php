<?php

class SelectionItemsWidget extends CWidget
{
	public $type = 'order';
    /**
     * @var CHttpRequest
     */
    protected $request;

    /**
     * @var CCustomUrlManager
     */
    protected $urlManager;

	public function init()
	{
        $this->request = Yii::app()->request;
        $this->urlManager = Yii::app()->urlManager;
	}

    public function generatePerPageLink($perPage)
    {
        return $this->urlManager->createUrlFromCurrent([
            'perPage' => $perPage
        ]);
    }

	public function run()
	{
        $activePageCount = $this->request->getQuery('perPage', isFeatureEnabled('side-seo-text') ? 20 : 10);
        $activeSortPage = $this->request->getQuery('sort');
        $sortPriceLink = $this->urlManager->createUrlFromCurrent([
            'sort' => $activeSortPage == 'desc' ? 'asc' : 'desc'
        ]);

        $activAvailablePage = $this->request->getQuery('available');


        if ($activAvailablePage == 'all') {
            unset($_GET['available']);
            $availableLink = $this->urlManager->createUrlFromCurrent([]);
        } else {
            $availableLink = $this->urlManager->createUrlFromCurrent(['available' => 'all']);
        }


        $this->render('SelectionItemsWidget', [
            'availableLink' => $availableLink,
            'activAvailablePage' => $activAvailablePage,
            'linkPrice'=>$sortPriceLink,
            'activePageCount' => $activePageCount,
            'activeSortPage'=>$activeSortPage,
			'type' => $this->type,
        ]);
	}	
}
