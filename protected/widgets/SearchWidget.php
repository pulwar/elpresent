<?php

class SearchWidget extends CWidget
{
	public $view;

	public function init()
	{

	}

	public function run()
	{
        $cookie = Yii::app()->request->cookies;
        $hint = !($cookie->contains('search-hint') || $cookie['search-hint']->value);
		$this->render('SearchWidget',array(
            'hint' => $hint,
			'type_view' => $this->view
        ));
	}

	public function createUrl()
	{
		$args = func_get_args();
		return call_user_func_array(array(Yii::app()->urlManager,'createUrl'),$args);
	}
}
