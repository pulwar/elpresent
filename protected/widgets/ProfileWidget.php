<?php

class ProfileWidget extends CWidget
{
    /**
     * @var WebUser
     */
    protected $user;

    public function init()
    {
        $this->user = Yii::app()->user;
    }

    public function run()
    {
        if ($this->user->isGuest) {
            $this->render('profile/guest');
        } else {
            $this->render('profile/profile', ['user' => $this->user->getUser()]);
        }
    }

}
