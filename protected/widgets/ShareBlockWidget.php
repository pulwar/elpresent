<?php

class ShareBlockWidget extends CWidget
{

    public $link;

    public $title;

    public $description;


    public function run()
    {
        $this->render('ShareBlockWidget', [
            'link' => $this->link,
            'title' => $this->title,
            'description' => $this->description
        ]);
    }

}