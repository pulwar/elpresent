<?php

class AnoncerWidget extends CWidget
{
	private $_dir;

	public function init(){
		$this->_dir = Yii::getPathOfAlias('webroot.assets');
	}

	public function run()
	{
		$file_path = $this->_dir . '/anoncer.txt';
		$txt = null;

		if(is_file($file_path)){
			$txt = file_get_contents($file_path);
		}

		$this->render('AnoncerWidget',['txt' => $txt]);
	}
}