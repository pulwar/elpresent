<?php

class PaymentWidget extends CWidget
{
	/** @var  Order */
	public $order;
	public $result;

	/**
	 * @throws CException
	 * @throws CHttpException
	 */
	public function run()
	{
		if (!$this->order->is_paid) {
			switch ($this->order->payment_type_id) {
				case PaymentType::WEBPAY: /** Todo: outdated*/
					$webpay = Yii::app()->webpay;
					$invoice = $this->order->webpay;
					$this->render('payment/webpay', [
						'invoice' => $invoice,
						'order' => $this->order,
						'webpay' => $webpay
					]);
					break;
				case PaymentType::BEPAID_CARD:
                    $this->render('payment/bepaid', [
                        'invoice' => $this->order->bepaid,
                        'order' => $this->order,
                        'bepaid' => Yii::app()->getComponent('manager.bepaid.card')
                    ]);
				    break;
				case PaymentType::BEPAID_HALVA:
					$this->render('payment/bepaid', [
						'invoice' => $this->order->bepaid,
						'order' => $this->order,
						'bepaid' => Yii::app()->getComponent('manager.bepaid.halva')
					]);
					break;
				case PaymentType::EASYPAY: /** outdated */
					$this->render('payment/easypay', [
						'order' => $this->order,
						'result' => $this->result
					]);
					break;
				case PaymentType::COURIER:
				default:
					throw new CHttpException('404');
					//$this->render('payment/submit_form', ['order' => $this->order]);
			}
		} else {
			$this->render('payment/already_paid');
		}

	}
}