<?php
class CommentWidget extends CWidget
{

    public function init()
    {

    }

    public function run()
    {
        //$criteria = new CDbCriteria();
        //$criteria->order ='RAND()';
        //$criteria->condition = 'visibility = 1';
        //$criteria->limit = 1;
        //$comments = Contact::model()->findAll($criteria);

        //d::pr($comment,1);


        //if(! $comments = Yii::app()->cache->get('comments_widget'))
        //{
            $criteria = new CDbCriteria();
            $criteria->condition = 'visibility = 1';
            $criteria->limit = 10;
            $criteria->order = 'contact_date DESC';
            $comments = Contact::model()->findAll($criteria);
            //Yii::app()->cache->set('comments_widget' ,$comments, 3600);
        //}

        if(empty($comments)){
            return;
        }

        $this->render('CommentWidget', array('comments' => $comments));
    }
}