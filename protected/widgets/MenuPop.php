<?php

class MenuPop extends CWidget
{
	public $view;

    protected $tree;
    /**
     * @var CHttpRequest
     */
    protected $request;

    public function init()
    {
        //
    }

    public function run()
    {
        $this->render('MenuPop' . $this->view, ['fast_search_type' => Yii::app()->request->getParam('fast_search_type')]);
    }


}
