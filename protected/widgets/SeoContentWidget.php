<?php

class SeoContentWidget extends CWidget
{
	protected $seoContent;

	public function init()
	{
		$this->seoContent = '';
		if (!empty(Yii::app()->controller->seoText)) {
			$this->seoContent = Yii::app()->controller->seoText;
		} else {
			// только первый пункт пагинации
			if (!preg_match("/\/page\d$/i", $_SERVER['REQUEST_URI'])) {
				$slug = Yii::app()->request->getParam('slug');
				$category = Category::model()->findByAttributes(['slug' => $slug]);
				if ($slug && $category) {
					$this->seoContent = empty($category->seo_text) ? '' : $category->seo_text;
				}
			}
		}

	}

	public function run()
	{
		$this->render('SeoContentWidget', array('seoContent' => $this->seoContent));
	}
}

?>
