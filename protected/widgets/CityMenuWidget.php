<?php

class CityMenuWidget extends CWidget
{

	public function run()
	{
        $cities = City::getCityList();
        $currentCity = Yii::app()->request->getQuery('_city', null);

		$this->render('CityMenuWidget', compact('cities', 'currentCity'));
	}

    public function createCityUrl(City $city)
    {
        if (Yii::app()->controller->id !== 'catalog') {
            return Yii::app()->urlManager->createUrl('catalog/index', [
                'city' => $city
            ]);
        }

        return Yii::app()->urlManager->createUrlFromCurrent([
            'city' => $city
        ]);
    }
}
