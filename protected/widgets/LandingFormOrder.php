<?php

class LandingFormOrder extends CWidget
{

    public function init()
    {

    }

    public function run()
    {
        $landingOrderForm = new LandingOrderForm();

        $this->render('LandingFormOrder',array(
            'landingOrderForm' => $landingOrderForm
        ));
    }
}
