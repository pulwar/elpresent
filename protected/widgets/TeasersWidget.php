<?php

class TeasersWidget extends CWidget
{
    public function run()
    {
        /** @var $teasers Teaser[] */
        $cache = Yii::app()->cache;
        $content = $cache->get('teasers-cache');

        if (!$content) {
            $teasers = Teaser::model()->findAll();

            $content = $this->render('teasers', compact('teasers'), true);
            $cache->set('teasers-cache', $content, 3600 * 8);
        }

        echo $content;
    }
}