<?php

class CategoriesWidget extends CWidget
{
	public $view;

    protected $tree;
    /**
     * @var CHttpRequest
     */
    protected $request;

    public function init()
    {
        $this->tree = static::filterTree(Category::getTree(true, true));
        $this->request = Yii::app()->request;
    }

    public function run()
    {
        $this->render('CategoriesWidget' . $this->view,['tree' => $this->tree]);
    }

    protected function isActive($category) {
        $active = $category['slug'] == $this->request->getQuery('slug');

        if ($category['hasParent']) {
            return $active;
        }

        foreach($category['children'] as $children) {
            if ($active) {
                return $active;
            }

            $active = $children['slug'] == $this->request->getQuery('slug');
        }

        return $active;
    }

    protected static function filterTree($tree)
    {
        return array_filter($tree, function ($node) {

            return $node['itemsCount'] > 0;
        });
    }

}
