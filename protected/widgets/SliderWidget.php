<?php

class SliderWidget extends CWidget
{
    public function init()
    {

    }

    public function run()
    {
        $slides = Slider::model()->findAll(array(
            'select'=>'*',
            'condition'=>'`show`=:show AND `type_id`=:type_id',
            'params'=>array(':show'=>1, ':type_id' =>1),
            'order'=> '`order`',
        ));

        $this->render('SliderWidget',['slides' => $slides]);
    }
}