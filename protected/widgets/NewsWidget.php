<?php

class NewsWidget extends CWidget
{
    public function run()
    {
        Yii::beginProfile('news-widget', 'widgets');
        $cache = Yii::app()->cache;
        $content = $cache->get('news-widget');
        if (!$content) {
            $cDbCriteria = new CDbCriteria();
            $cDbCriteria->order = '`date` DESC';
            $oNews = News::model()->findAll($cDbCriteria);

            $content = $this->render('NewsWidget', array (
                'news' => $oNews
            ), true);

            // cache widget for 8h
            $cache->set('news-widget', $content, 3600 * 8);
        }

        echo $content;
        Yii::endProfile('news-widget', 'widgets');
    }
}