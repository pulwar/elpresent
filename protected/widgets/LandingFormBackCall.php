<?php

class LandingFormBackCall extends CWidget
{

    public function init()
    {

    }

    public function run()
    {
        $landingBackCallForm = new LandingBackCallForm();

        $this->render('LandingFormBackCall',array(
            'landingBackCallForm' => $landingBackCallForm
        ));
    }
}
