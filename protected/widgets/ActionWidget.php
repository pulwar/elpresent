<?php

class ActionWidget extends CWidget
{

    public function run()
    {
        $user_id = Yii::app()->user->getId();

        $json_file = 'assets/action.json';
        $json_data = file_get_contents($json_file);
        $data = json_decode($json_data, true);
        $count = empty($data['count']) ? 0 : (int)$data['count'];

        $this->render('ActionWidget', ['user_id' => $user_id, 'count' => $count]);
    }

}