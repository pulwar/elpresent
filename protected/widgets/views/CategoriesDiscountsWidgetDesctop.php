<ul class="discounts-categories nav-catalog nav-catalog-regular list-unstyled">
    <? foreach($categories as $node):?>
        <li>
            <a href="/skidki/<?=$node->slug?>" class="nav-catalog-link <?= url::active($node->slug)?>">
                <!--<span class="hover"></span>-->
                <span class="nav-catalog-link-content">
                   <span class="nav-catalog-image">
                       <img src="/images/category/<?=$node['icon']?>" alt="<?= $node['title']; ?>">
                   </span>
                   <?= $node['title']; ?>
                </span>
            </a>
        </li>
    <? endforeach; ?>
</ul>