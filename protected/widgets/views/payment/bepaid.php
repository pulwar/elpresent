<?php
/** @var BepaidManager $bepaid */
/** @var Order $order */
/** @var Bepaid $invoice */
?>

    

    <?php
    $discountTitle  = '';
    $discountPrice = 0;
    foreach ($order->orderItems as $orderItem) {
        $option = Option::model()->findAllByPk($orderItem->option_id);
        $itemName = htmlspecialchars($orderItem->item_title.$option->name);

        if ($orderItem->discount_price) {
            $discountTitle .= "Скидка:  {$orderItem->discount_percent}% на позицию \"$itemName\" \n";
            $discountPrice += $orderItem->discount_price;
        }
        ?>

    <?php }
        if($order->user_cash) {
            $discountTitle .= "Баллов вычтенных из суммы: - {$order->user_cash} руб.";
            $discountPrice += $order->user_cash;
        }

//        if($discountPrice) {
//            echo "<input type='hidden' name='be_discount_name' value='$discountTitle'>";
//            echo "<input type='hidden' name='be_discount_price' value='$discountPrice'>";
//        }

        if ($order->type != Order::TYPE_DIGITAL) {
            $deliveryPrice = $order->getPriceForDelivery($order->deliveryType);
            $deliveryType = '';
            switch (true) {
                case (bool) $order->is_thematic_delivery:
                    $deliveryPrice = $order->getPriceForDelivery($order->thematic_delivery_name);
                    $deliveryType = "Тематическая доставка: {$order->thematic_delivery_price}руб.";
                    break;
                case (bool) $order->is_express_delivery:
                    $deliveryPrice = $order->getPriceForDelivery(2);
                    $deliveryType = "Экспресс доставка: ".Order::DELIVERY_EXPRESS_PRICE." руб.";
                    break;
                case (bool) $order->is_delivery_general:
                    $deliveryType = "Обычная доставка: ";
                    $deliveryPrice = $order->getPriceForDelivery($order->deliveryType);
                    if ($order->price < Order::PRICE_FOR_FREE_DELIVERY) {
                        $deliveryType .= Order::DELIVERY_GENERAL_PRICE." руб.";
                    } else {
                        $deliveryType .= "бесплатно (Цена выше ".Order::PRICE_FOR_FREE_DELIVERY." руб.)";
                    }
                    echo "</li>";
                    break;
                default:
                    echo "Самовывоз: ".Order::DELIVERY_FREE_PRICE." руб.</span></li>";
                    $deliveryType = 'Самовывоз';
                    $deliveryPrice = Order::DELIVERY_FREE_PRICE;
            }
        }
    ?>

 <a class="btn btn-primary btn-sm btn-mid" href="<?= $bepaid->getEndpoint(); ?>">Перейти к оплате</a>
	


