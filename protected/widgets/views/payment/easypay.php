<hr>
<? if(!empty($result)):?>
	<?=$result;?>
<? endif;?>
<form action="" method="post" id="easypay" data-payment-form="easypay">
	<p>
		<label>Номер EasyPay кошелька:</label><br>
		<input type="text" name="card" required="required">
		<button type="submit" class="btn btn-primary btn-sm btn-mid" data-pay-method="easypay">Выставить счет</button>
	</p>
</form>

<? if($order->type == Order::TYPE_DIGITAL):?>
	<form action="" method="post" id="get_sertificat">
		<p><span class="label label-danger">Внимание!</span> Подтвердите платеж в личном кабинете EasyPay и нажмите на кнопку, чтобы получить серитификат на ваш email <?=$order->email?></p>
		<input type="submit" name="get_sertificat" class="btn btn-primary btn-sm btn-mid" value="Выслать сертификат"/>
	</form>
<? endif;?>

