<?php
/** @var WebPayManager $webpay */
/** @var Order $order */
/** @var Webpay $invoice */
?>
<form action="<?= $webpay->getEndpoint(); ?>" method="post" id="webpay" data-payment-form="webpay">
    <input type="hidden" name="*scart">
    <input type="hidden" name="wsb_language_id" value="russian">
    <input type="hidden" name="wsb_storeid" value="<?= $webpay->getStoreId(); ?>" >
    <input type="hidden" name="wsb_store" value="<?= $webpay->getStore(); ?>" >
    <input type="hidden" name="wsb_order_num" value="<?= $invoice->wsb_order_num; ?>" >
    <input type="hidden" name="wsb_test" value="<?= $webpay->isDebug() ? '1' : '0'; ?>" >
    <input type="hidden" name="wsb_currency_id" value="<?= $webpay->getCurrencyId(); ?>" >
    <input type="hidden" name="wsb_seed" value="<?= $invoice->wsb_seed; ?>">

    <input type="hidden" name="wsb_return_url" value="<?= path('order/wsbcomplete', [], true); ?>">
    <input type="hidden" name="wsb_cancel_return_url" value="<?= path('order/wsbcancel', [], true); ?>">
    <input type="hidden" name="wsb_notify_url" value="<?= path('order/wsbnotify', [], true); ?>">

    <input type="hidden" name="wsb_email" value="<?= htmlspecialchars($order->email); ?>" >
    <input type="hidden" name="wsb_phone" value="<?= htmlspecialchars($order->phone); ?>" >


    <?
    $discountTitle  = '';
    $discountPrice = 0;
    foreach ($order->orderItems as $orderItem) {
        $option = Option::model()->findAllByPk($orderItem->option_id);
        $itemName = htmlspecialchars($orderItem->item_title.$option->name);

        if ($orderItem->discount_price) {
            $discountTitle .= "Скидка:  {$orderItem->discount_percent}% на позицию \"$itemName\" \n";
            $discountPrice += $orderItem->discount_price;
        }
        ?>
        <input type="hidden" name="wsb_invoice_item_name[]" value="<?=$itemName?>">
        <input type="hidden" name="wsb_invoice_item_quantity[]" value="1">
        <input type="hidden" name="wsb_invoice_item_price[]" value="<?= $orderItem->item_price; ?>">
    <? }
        if($order->user_cash) {
            $discountTitle .= "Баллов вычтенных из суммы: - {$order->user_cash} руб.";
            $discountPrice += $order->user_cash;
        }

        if($discountPrice) {
            echo "<input type='hidden' name='wsb_discount_name' value='$discountTitle'>";
            echo "<input type='hidden' name='wsb_discount_price' value='$discountPrice'>";
        }

        if ($order->type != Order::TYPE_DIGITAL) {
            $deliveryPrice = $order->getPriceForDelivery($order->deliveryType);
            $deliveryType = '';
            switch (true) {
                case (bool) $order->is_thematic_delivery:
                    $deliveryPrice = $order->getPriceForDelivery($order->thematic_delivery_name);
                    $deliveryType = "Тематическая доставка: {$order->thematic_delivery_price}руб.";
                    break;
                case (bool) $order->is_express_delivery:
                    $deliveryPrice = $order->getPriceForDelivery(2);
                    $deliveryType = "Экспресс доставка: ".Order::DELIVERY_EXPRESS_PRICE." руб.";
                    break;
                case (bool) $order->is_delivery_general:
                    $deliveryType = "Обычная доставка: ";
                    $deliveryPrice = $order->getPriceForDelivery($order->deliveryType);
                    if ($order->price < Order::PRICE_FOR_FREE_DELIVERY) {
                        $deliveryType .= Order::DELIVERY_GENERAL_PRICE." руб.";
                    } else {
                        $deliveryType .= "бесплатно (Цена выше ".Order::PRICE_FOR_FREE_DELIVERY." руб.)";
                    }
                    echo "</li>";
                    break;
                default:
                    echo "Самовывоз: ".Order::DELIVERY_FREE_PRICE." руб.</span></li>";
                    $deliveryType = 'Самовывоз';
                    $deliveryPrice = Order::DELIVERY_FREE_PRICE;
            }
        }
    ?>
    <input type="hidden" name="wsb_shipping_name" value="<?=$deliveryType?>" >
    <input type="hidden" name="wsb_shipping_price" value="<?=$deliveryPrice?>">
    <input type="hidden" name="wsb_total" value="<?= $invoice->wsb_total; ?>" >
    <input type="hidden" name="wsb_signature" value="<?= $invoice->wsb_signature; ?>" >
	<br>
	<button type="submit" class="btn btn-primary btn-sm btn-mid" data-pay-method="webpay">Оплатить</button>
</form>

