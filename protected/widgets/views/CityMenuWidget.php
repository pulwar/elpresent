<div class="dropdown change-city-list">
    <div class="dropdown-box" id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">
        <?= $currentCity->title; ?>
        <span class="drop_arr"></span>
    </div>
    <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
        <? foreach ($cities as $city):?>
            <li>
                <a href="<?= $this->createCityUrl($city); ?>"><?= $city->title;?></a>
            </li>
       <? endforeach;?>
    </ul>
</div>