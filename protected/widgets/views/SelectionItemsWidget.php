<? if ($type == 'selectionIems'): ?>
    <div class="BoxSortAvailable" style="display: none">
        <div class="sort-price-div">
            <a href="<?= $linkPrice; ?>" class="sort-item-price">
                Сортировать по цене<?= $activeSortPage ? ($activeSortPage == 'desc'?'<span>&#9660;</span>':'<span>&#9650;</span>') : '<span>&#9660</span>' ?>
            </a>
        </div>
            <div id="availableInput">
                <label for="availableCheck">Показывать позиции, которые есть в наличии</label>
                <input data-link="<?=$availableLink?>" <?= $activAvailablePage == 'all' ? 'checked' : ''?> type="checkbox" value="all" name="available" id="availableCheck">
                <script>
                    $(document).ready(function () {
                        $(document).on('click', '#availableCheck', function () {
                            window.location = $(this).attr('data-link');
                        } )
                    })
                </script>
            </div>
    </div>
<? endif; ?>