<script>
    $(function() {
        $('#foo').carouFredSel({
            prev: '#prev',
            next: '#next',
            pagination: false,
            auto: false,
            responsive: true,
            direction: 'left'
        });
    });
</script>

<div class="comment-content">
    <h4>Отзывы наших клиентов</h4>
    <div class="list_carousel">
        <ul id="foo">
            <? foreach($comments as $comment):?>
                <li>
                    <div class="contact-name">
                        <?=htmlspecialchars($comment->contact_name)?>
                    </div>
                    <div class="contact-subject">
                        <?=htmlspecialchars($comment->contact_subject)?>
                    </div>
                    <div class="testimonials">
                        <div class="stars">
                            <?
                            for ($i = 1; $i <= 5; $i++) {
                                echo '<i' . ($i <= $comment->contact_rating ? ' class="act"' : '') .'>*</i>';
                            }
                            ?>
                        </div>
                    </div>
                    <div class="contact_body">
                        <?=text::limitWords(htmlspecialchars($comment->contact_body), 25); ?>
                    </div>
                </li>
            <? endforeach;?>
        </ul>
        <div class="clearfix"></div>
        <div class="link-bar">
            <a id="prev" class="prev foo-buttons" href="#">&lt; Предыдущий отзыв</a>
            <a id="next" class="next foo-buttons" href="#">Следующий отзыв &gt;</a>
            <noindex><a href="<?= path('site/contact'); ?>#comments" title="Все отзывы" rel="nofollow">Далее</a></noindex>
        </div>
    </div>
</div>


