<? if (!Yii::app()->user->getId()) : ?>
    <script type="text/javascript" src="/js/ui.achtung-min.js"></script>
    <div class="registration-box">
        <a class="registration-box-close" href="javascript: void(0);"></a>
        <p class="registration-box-text">Добро пожаловать!
            <span>Введите ваши данные и пароль для регистрации</span></p>
        <input type="text" id="reg" class="email" name="registration-box-email" value="" tabindex="6" /><br />
        <input type="password" class="password1" name="registration-box-password1" value="" tabindex="7" /><br />
        <input type="password" class="password2" name="registration-box-password2" value="" tabindex="8" /><br />
        <input type="button" class="registration-box-button" value="" tabindex="9" /><br />
    </div>
    <script type="text/javascript" language="javascript">
        $(document).ready(function()
        {
            $('.login-register').click(function()
            {
                $('.registration-box').slideToggle(200);
                return false;
            });

            $('.registration-box-close').click(function()
            {
                $('.registration-box').slideToggle(80);
                return false;    
            });

            $('.registration-box-button').click(function()
            {
                var email           = $('.email#reg').val();
                var password        = $('.password1').val();
                var passwordConfirm = $('.password2').val();

                if (email.length && email.search(/^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/) >=0)
                {
                    if (password && passwordConfirm && password == passwordConfirm && password != '123456')
                    {
                        $.ajax(
                        {
                            type : 'POST',
                            url  : '/ajax_register',
                            data : {'user_email' : email, 'user_password' : password, 'user_password_confirm' : passwordConfirm},
                            success : function(data)
                            {
                                if ( data == 'success' )
                                {
                                    $('.registration-box').hide();
                                    location.href = '/catalog/all.html';
                                    return;
                                }
                                else
                                {
                                    $.achtung(
                                    {
                                        className : 'achtungFailRegister',
                                        message   : data,
                                        timeout   : 7
                                    });

                                    return;
                                }
                            }
                        });
                    }
                    else
                    {
                        $.achtung(
                        {
                            className : 'achtungFailRegister',
                            message   : 'Пароли не введены или не совпадают',
                            timeout   : 5
                        });
                    }
                }
                else
                {
                    $.achtung(
                    {
                        className : 'achtungFailRegister',
                        message   : 'Введите правильный email',
                        timeout   : 5
                    });
                }
            });

            $('.email#reg').defaultValue('Ваш E-mail');
            $('.password1').defaultValue('123456');
            $('.password2').defaultValue('123456');
        });
    </script>
<? endif; ?>