<script type="text/javascript" >
$(document).ready(function()
{
    $('._subscribe_button').click(function()
    {
        $('._subscribe_form').slideDown('fast');
    });
});
</script>

<div id="subscribe_info" user_id="<? if ($data['user']) echo $data['user']->id; else echo '0'; ?>" user_ip="<?= $data['subscrIp']; ?>" user_email="<? if ($data['user']) echo $data['user']->user_email; else echo ''; ?>" style="display: none;"><!-- --></div>

<div class="_subscribe_container">
    <div class="_subscribe_button">&nbsp;</div>
    <div class="_subscribe_form">
        <div class="_subscribe-container-inner">
            <a class="_subscribe-close-button" href="javascript:void(0);" onclick="subscribeHide();"></a>
            <p>Добро пожаловать!<br />
                <span id="email_input">Введите Ваш Email для оформления подписки.</span>
                <span id="email_false">Неверный Email</span>
            </p>
            <form id="itemform" action="">
                <input class="subscribe-email" name="subscribe_email" type="text" value="<? if ($data['user']) echo $data['user']->user_email; else echo 'Email'; ?>" maxlength="255" onfocus="if(this.value == 'Email') this.value=''" onblur="if (this.value==''){this.value='Email'}" />
                <input id="form_submit" type="button" value="" class="subscribe-submit" rel="<?= $subscrUrl; ?>" onclick="subscribe();" />
            </form>
            <div class="clear"></div>
        </div>
    </div>
</div>

<script type="text/javascript" language="javascript">

    $('input[name=subscribe_email]')
    .keypress(function(e)
    {
        if(e.which == 13)
        {
            e.preventDefault();
            $('#form_submit').trigger('click');
            return false;
        }
    });


    function subscribe()
    {
        var subscribe_email = $('input[name=subscribe_email]').val();
        var subscribe_url   = $('#form_submit').attr('rel');
        var user_id = $('#subscribe_info').attr('user_id');
        var user_ip = $('#subscribe_info').attr('user_ip');

        if ('Email' == subscribe_email || !subscribe_email )
        {
            return false;
        }

        $.post
        (
            subscribe_url,
            {
                'subscribe_email': subscribe_email,
                'user_id': user_id,
                'user_ip': user_ip
            },
            function(data)
            {
                if ('undefined' != typeof data.message && '200' == data.message)
                {
                    $('._subscribe_button').hide(300);
                    /* email inserted */
                }
                else if ('undefined' != typeof data.message && '403' == data.message)
                {
                    $('#email_input').hide();
                    $('#email_false').show();
                    return false;
                }
                subscribeHide();
            },
            'json'
        );
        return false;
    }

    function subscribeHide()
    {
        var email = $('#subscribe_info').attr('user_email');
        email     = $.trim(email) ? email : 'Email';
        $('._subscribe_form').slideUp('fast');
        $('#email_false').hide();
        $('#email_input').show();
        $('input[name=subscribe_email]').val(email);
        return false;
    }

</script>
        