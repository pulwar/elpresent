<div class="feedback-small-widget">
    <hr class="border-small">
    <h3 class="header-with__link">Отзывы
        <noindex><a href="<?= path('site/contact'); ?>#comments" rel="nofollow">Далее</a></noindex>
    </h3>

    <div class="slides">
        <div id="carousel-small-feedback" class="carousel carousel-feedback-small slide" data-ride="carousel" data-interval=5000>

            <!-- Indicators -->
            <ol class="carousel-indicators">
                <? foreach($comments as $k => $slide):?>
                    <li data-target="#carousel-small-feedback" data-slide-to="<?=$k?>" class="<?= ($k == 0 ? 'active' : '')?>"></li>
                <? endforeach;?>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <? foreach ($comments as $k => $comment): ?>
                    <div class="item <?= ($k == 0 ? 'active' : '')?>">
                        <div>
                            <h4 class="feedback-small-title">
                                <?=htmlspecialchars($comment->contact_name)?> -
                            <span class="rating">
                                <? for ($i = 1; $i <= 5; $i++) :?>
                                    <? if($i <= $comment->contact_rating):?>
                                        <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                                    <?endif;?>
                                <? endfor;?>
                            </span>
                            </h4>
                            <p class="feedback-small-content"><?=text::limitWords(htmlspecialchars($comment->contact_body), 25); ?>
                                <noindex><a href="<?= path('site/contact'); ?>#comments" rel="nofollow">Далее</a></noindex>
                            </p>
                        </div>
                    </div>
                <? endforeach;?>
            </div>

        </div>
    </div>

    <div class="feedback-small-button">
	    <a href="#" class="btn btn-default" data-toggle="modal" data-target="#myModal">Оставить отзыв</a>
    </div>
</div>

