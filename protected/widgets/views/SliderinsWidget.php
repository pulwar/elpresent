<? if(!empty($slides)): ?>
    <div class="main_slider" style="overflow: hidden">
        <? foreach($slides as $k => $slide):?>
            <div><a href="<?=$slide->link?>" title="<?=$slide->title?>"><img src="/images/slider/<?=$slide->img?>" alt="<?=$slide->title?>"></a></div>
        <? endforeach;?>
    </div>
<? endif;?>


