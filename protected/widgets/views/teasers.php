<ul class="teasers">
    <?php foreach($teasers as $teaser) { ?>
        <li><?php if ($teaser->noIndex) { ?> <noindex> <?php } ?>
            <a class="teaser" href="<?= $teaser->link; ?>">
                <img src="/<?= $teaser->getImageUri(); ?>" alt="<?= $teaser->description; ?>" />
                <span class="caption"><?= $teaser->description; ?></span>
            </a>
            <?php if ($teaser->noIndex) { ?> </noindex> <?php } ?>
        </li>
    <?php } ?>
</ul>