<? $action = Yii::app()->controller->action->id; ?>
<div class="profile-info">
    <div class="profile-info-name"><a href="/profile"><?= (Yii::app()->user->getFullName()) ? Yii::app()->user->getFullName() : Yii::app()->user->getUser()->user_email ?></a></div>
    <div class="profile-info-menu">
        <div class="profile-menu-item">
            <a <? if ( $action == 'settings' ) : ?>class="active"<? endif; ?> href="/profile/settings">Настройки</a>
            <div class="profile-menu-item-r <? if ( $action == 'settings' ) : ?>active<? endif; ?>"></div>
        </div>
        <div class="profile-menu-item">
            <a <? if ( $action == 'orders' && !Yii::app()->request->getParam('history') ) : ?>class="active"<? endif; ?> href="/profile/orders">Ваша корзина</a>
            <div class="profile-menu-item-r <? if ( $action == 'orders' && !Yii::app()->request->getParam('history') ) : ?>active<? endif; ?>"></div>
        </div>
        <div class="profile-menu-item">
            <a <? if ( $action == 'orders' && Yii::app()->request->getParam('history') ) : ?>class="active"<? endif; ?> href="/profile/orders?history=1">История заказов: <span style="color: #fffc00;"><?= $ordersCnt; ?></span></a>
            <div class="profile-menu-item-r <? if ( $action == 'orders' && Yii::app()->request->getParam('history') ) : ?>active<? endif; ?>"></div>
        </div>
        <div class="profile-menu-item">
            <a <? if ( $action == 'calendar' ) : ?>class="active"<? endif; ?> href="/profile/calendar">Календарь событий</a>
            <div class="profile-menu-item-r <? if ( $action == 'calendar' ) : ?>active<? endif; ?>"></div>
        </div>
    </div>
    <div class="profile-info-logout"><a href="/logout">Выйти</a></div>
</div>
<div style="clear: both;"></div>
<br /><br /><br /><br /><br />