<div class='main-inner'>
<div class="calendar-title"><!-- --></div>
<? foreach($list as $each) : ?>
<? //var_dump($each); ?>
<? //var_dump($each->widget_date); ?>
	<div class="dotted-line"><!-- --></div>
	<? foreach($each->holidays as $key => $h) : ?>
	<? if ( $key > 0 ) continue; ?>
	<div class="days">
		<div class="date">
			<span>
				<?= date_format($each->date,"d"); ?>
			</span>
			<i>
				<? 
                                  $translation = array(
                                      "January" => "Января",
                                      "February" => "Февраля",
                                      "March" => "Марта",
                                      "April" => "Апреля",
                                      "May" => "Мая",
                                      "June" => "Июня",
                                      "July" => "Июля",
                                      "August" => "Августа",
                                      "September" => "Сентября",
                                      "October" => "Октября",
                                      "November" => "Ноября",
                                      "December" => "Декабря",
                                  );

                                echo strtr(_(date_format($each->date,"F")), $translation); ?>
			</i>
		</div>
		<div class="info">
			<a href="<?= Yii::app()->urlManager->createUrl('holiday/id',array('id' => $h->id)); ?>"><?= $h->title ?></a>
			<br />
			<strong>Именинники:</strong>&nbsp;<?= $each->births->title; ?>
		</div>
	</div>
	<? endforeach; ?>
<? endforeach; ?>
<? /* <div class="days-all"><a href="<?= $this->createUrl('calendar/index'); ?>">Весь календарь</a></div> */ ?>
								<div class="top">
	<div class="left"><!-- --></div>
	<div class="center"><!-- --></div>
	<div class="right"><!-- --></div>
</div>
<div class="bottom">
	<div class="left"><!-- --></div>
	<div class="center"><!-- --></div>
	<div class="right"><!-- --></div>
</div>
<div class="right-side">
	<div class="inner-top"><!-- --></div>
	<div class="inner"><!-- --></div>
</div>
<div class="left-side">
	<div class="inner-top"><!-- --></div>
	<div class="inner"><!-- --></div>
</div>
<div class="kruki">
	<div><!-- --></div>
	<div><!-- --></div>
	<div><!-- --></div>
	<div><!-- --></div>
	<div><!-- --></div>
	<div><!-- --></div>
	<div><!-- --></div>
	<div><!-- --></div>
	<div><!-- --></div>
	<div><!-- --></div>
</div>
</div>
