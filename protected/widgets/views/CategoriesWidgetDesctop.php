<? if (!empty($tree)): ?>
    <ul class="nav-catalog nav-catalog-regular list-unstyled">

        <? foreach ($tree as $node): ?>
            <li class="<?= $this->isActive($node) ? 'active current' : ''; ?>">
                <a href="<?= Yii::app()->controller->createUrl('catalog/category',
                    array('slug' => $node['slug'])) ?>" class="nav-catalog-link">
                    <!--<span class="hover"></span>-->
                <span class="nav-catalog-link-content">
                  <span class="nav-catalog-image">
                      <? $img = text::doPruningToSymbol($node['icon'], '.') ?>
                      <?
                      $prefix = $this->isActive($node) ? 'w-' : '';
                      if (is_file('images/category/' . $img . '.svg') && is_file('images/category/' . $prefix . $img . '.svg')) {
                          $img = '/images/category/' . $prefix . $img . '.svg';
                      } else {
                          $img = '/images/category/' . $node['icon'];
                      }
                      ?>
                      <img src="<?= $img ?>" alt="<?= $node['title']; ?>">
                  </span>
                    <?= $node['title']; ?>
                    <span class="badge"><?= $node['itemsCount']; ?></span>
                </span>
                </a>
                <?/* if (!empty($node['children'])) : ?>
                    <ul class="nav-catalog list-unstyled">
                        <? foreach ($node['children'] as $children): ?>
                            <li>
                                <a href="<?= Yii::app()->controller->createUrl('catalog/category', ['slug' => $children['slug'], 'parent' => $node['slug']])?>" class="nav-catalog-link">
                                    <span class="hover"></span>
                                    <span class="nav-catalog-link-content"><?= $children['title']; ?></span>
                                </a>
                            </li>
                        <? endforeach; ?>
                    </ul>
                <? endif */?>
            </li>
        <? endforeach; ?>
    </ul>
<? else: ?>
    <ul class="nav-catalog nav-catalog-regular list-unstyled">
        <li>
            <span class="nav-catalog-link-content">Каталог пуст</span>
        </li>
    </ul>
<? endif; ?>