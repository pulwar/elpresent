<div class="landing-form-order">
    <? $form = $this->beginWidget('CActiveForm',array(
        'enableAjaxValidation'=>true,
        'action'=> CHtml::normalizeUrl(array("/site/landing")),
        'enableClientValidation'=>false,

        'clientOptions'=>array(
            'validateOnChange' =>true,
            'validateOnSubmit'=>true,
            'afterValidate'=>'js:function(form,data,hasError){
                if(!hasError)
                {
                  $.ajax(
                  {
                    "type":"POST",
                    "url":"'.CHtml::normalizeUrl(array("/site/landing")).'",
                    "data":form.serialize(),
                    "success":function(data)
                        {
                           //alert(data);
                           var response = jQuery.parseJSON (data);
                           if(response.status){
                              $(form)[0].reset();
                              $(form).hide();
                              $(form).siblings(".landing-form-order-success").html("Ваша заявка отправлена!");
                           }
                        },
                  });
                }
        }'),
    ));
    ?>

    <div>
        <?=$form->textField($landingOrderForm,'name', ['placeholder' => $landingOrderForm->attributeLabels()['name']]); ?>
        <? $form->error($landingOrderForm,'name'); ?>
    </div>

    <div>
        <?=$form->textField($landingOrderForm,'phone', ['placeholder' => $landingOrderForm->attributeLabels()['phone']]);?>
        <? $form->error($landingOrderForm,'phone'); ?>
    </div>

    <div>
        <?=$form->emailField($landingOrderForm,'email', ['placeholder' => $landingOrderForm->attributeLabels()['email']]); ?>
        <? $form->error($landingOrderForm,'email'); ?>
    </div>

    <div>
        <?=$form->textArea($landingOrderForm,'message', ['placeholder' => $landingOrderForm->attributeLabels()['message']]); ?>
        <? $form->error($landingOrderForm,'message'); ?>
    </div>

    <? echo CHtml::submitButton('Оставить заявку'); ?>
    <? $this->endWidget(); ?>

    <div class="landing-form-order-success"></div>
</div>
