<div class="comment_wrap">
    <div class="page_width">
        <div class="slider_comment">
            <? foreach ($comments as $k => $comment): ?>
                <div class="slider_comment_item">
                    <div class="slider_comment_text">
                        <div class="slider_comment_text_fixed">
                            <?=htmlspecialchars($comment->contact_body)?>
                        </div>
                        <?if(strlen($comment->contact_body)>200):?>
                            <div><span class="show_text">Читать весь отзыв</span></div>
                        <?endif;?>
                    </div>
                    <div class="slider_comment_info">
                        <div class="slider_comment_photo" style="background-image: url('../img/front_bootstrap/userDef.svg')"></div>
                        <div class="slider_comment_name">
                            <p><?=htmlspecialchars($comment->contact_name)?></p>
                        </div>
                    </div>
                </div>
            <? endforeach;?>
        </div>
    </div>
</div>