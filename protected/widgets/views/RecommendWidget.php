<br />

<? 

$oCurlManager = new CUrlManager;

?>

<div class="item-title bg-recommend"><!-- --></div>
<div class="main-gift">
<? foreach($list as $each) : ?>
    <div class="section<?= $each->off_price ? " off":"";?>">
        <div class="main-block">
        	<div class="left">
            <?php if(!$each->off_price ) { ?>
            <? if($each->isNew()) : ?>
                <div class="new-label" style='z-index:5;'> </div>
                <? endif; ?>
            <? if ( $each->is_weekend ) : ?><div class="weekend-label" style='z-index:5;'> </div><? endif; ?>
            <? if ( !$each->available ) : ?><div class="available-label-cat" style='z-index:5;'> </div><? endif; ?>
            <?php } ?>
            <? if($each->off_price > 0) : ?>
            <?php
            $offClass = "big";
            if($each->off_price < 30)
                $offClass = "middle";
            if($each->off_price < 20)
                $offClass = "small";

            ?>
            <div class="off-label" style='z-index:5;'>
                <span class="off-value <?=$offClass;?>">-<?=round($each->off_price);?>%</span>
                <div class="new-price">
                    <div class="price-label">
                        Новая цена:
                        <span><?= (round($each->price-($each->price*($each->off_price/100)))); ?> руб.</span>
                    </div>
                </div>
            </div>
            <? endif; ?>
        	<? if( isset($each->preview_img) ) : ?>
			    <div class="img">
                    <table style="width: 100%; height: 112px;">
                        <tr>
                            <td style="vertical-align: middle; text-align: center;">
							    <a href="/catalog/item/item_id/<?= $each->id; ?>/category_id/<?= $each->category_id ?>">
                                    <img src="/<?= $each->preview_img; ?>" alt="" />
                                </a>
							</td>
                        </tr>
                    </table>
        		</div>
        	<? endif; ?>
				<div class="price">
					<?= ($each->price); ?> руб.
				</div>
        	</div>
            <div class="right">
                <a class="title" href="/catalog/item/item_id/<?= $each->id; ?>/category_id/<?= $each->category_id ?>"><?= $each->title ?></a>
				<span><?= $each->desc ?></span>
                <div class="opacity-text"><!-- --></div>
			</div>
        </div>
		<div class="right-block">
			<a href="/catalog/item/item_id/<?= $each->id; ?>/category_id/<?= $each->category_id ?>" class="gift-comments">
			<?= count($each->comments); ?>
			</a>
		</div>
		<a href="/catalog/item/item_id/<?= $each->id; ?>/category_id/<?= $each->category_id ?>" class="link">Подробнее</a>
    </div>
<? endforeach; ?>			                
</div>
<? $this->widget('PagerWidget',array('pager' => $list)); ?>