<!-- Fixed navbar -->
<nav class="navbar navbar-default container-pale-orange navbar-fixed-top" style="display: none">
	<div class="container ">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<p>Каталог</p>
			</button>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
			<? if (!empty($tree)): ?>
			<ul class="nav navbar-nav navbar-nav-cat">
				<? foreach ($tree as $node): ?>
					<? if (empty($node['children'])) : ?>
						<li>
							<a href="<?= Yii::app()->controller->createUrl('catalog/category', ['slug' => $node['slug']]) ?>"><?= $node['title']; ?></a>
						</li>
					<? else:?>
						<li class="dropdown">
							<a href="<?= Yii::app()->controller->createUrl('catalog/category', ['slug' => $node['slug']]) ?>" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
								<?= $node['title']; ?><span class="caret"></span>
							</a>
							<ul class="dropdown-menu" role="menu">
								<? foreach ($node['children'] as $children): ?>
								<li>
									<a href="<?= Yii::app()->controller->createUrl('catalog/category',
										[
											'slug' => $children['slug'],
											'parent' => $node['slug']
										]) ?>"><?= $children['title']; ?></a>
								</li>
								<?endforeach;?>
								<li><a href="<?= Yii::app()->controller->createUrl('catalog/category', ['slug' => $node['slug']]) ?>">Все подарки категории</a></li>
							</ul>
						</li>
					<? endif;?>
				<? endforeach; ?>
			</ul>
			<? endif ?>
		</div>
		<!--/.nav-collapse -->
	</div>
</nav>