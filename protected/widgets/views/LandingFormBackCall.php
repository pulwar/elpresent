<? $form = $this->beginWidget('CActiveForm', array(
    'enableAjaxValidation' => true,
    'action' => CHtml::normalizeUrl(array("/site/landing")),
    'enableClientValidation' => false,
    'htmlOptions' => array("class" => "main-form frigh"),
    'clientOptions' => array(
        'validateOnChange' => true,
        'validateOnSubmit' => true,
        'afterValidate' => 'js:function(form,data,hasError){
                if(!hasError)
                {
                  $.ajax(
                  {
                    "type":"POST",
                    "url":"' . CHtml::normalizeUrl(array("/site/landing")) . '",
                    "data":form.serialize(),
                    "success":function(data)
                        {
                           var response = jQuery.parseJSON (data);
                           if(response.status){
                              $(form)[0].reset();
//                              $(form).hide();
//                              $(form).siblings(".landing-form-back-call-success").html("<br/><p class=\'alert alert-success\'>Ваша заявка отправлена!</p>");
                              $(".popup-wrap").show();
                           }
                        },
                    });
                }
      }'
    ),
));
?>
<div class="form-group box">
    <?= $form->textField($landingBackCallForm, 'name', [
        'placeholder' => $landingBackCallForm->attributeLabels()['name'],
        'class' => 'form-control',
        'required' => 'required',
    ]); ?>
    <? $form->error($landingBackCallForm, 'name'); ?>
</div>

<div class="form-group box">
    <? $this->widget('CMaskedTextField', array(
        'model' => $landingBackCallForm,
        'attribute' => 'phone',
        'mask' => '+375-99-99-99-999',
        'placeholder' => '*',
        'htmlOptions' => array(
            'size' => '40',
            'class' => 'form-control',
            'required' => 'required',
            'placeholder' => $landingBackCallForm->attributeLabels()['phone']
        ),
    )); ?>
</div>

<div class="form-group box">
    <?= $form->emailField($landingBackCallForm, 'email', [
        'placeholder' => $landingBackCallForm->attributeLabels()['email'],
        'class' => 'form-control',
        'required' => 'required',
    ]); ?>
    <? $form->error($landingBackCallForm, 'email'); ?>
</div>
<!--<p class="callback-alert">Ваши данные не будут переданы третьим лицам</p>-->
<div class="button btn-margin-top box">
    <?= CHtml::submitButton('Отправить', ['class' => 'btn btn-primary']); ?>
</div>

<? $this->endWidget(); ?>

<div class="landing-form-back-call-success"></div>


