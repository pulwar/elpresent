<script type="text/javascript">
$(document).ready(function() {
	$('#mycarousel').jcarousel( {
	    visible: 5
	});
});
</script>
<noindex>
    <div class="new-block clearfix ribbon ribbon-sm purple">
        <div class="news-label">Новости: </div>
        <div class="news-swiper">
            <ul id="mycarousel" class="jcarousel-skin-tango">
                <? foreach ( $news as $newsItem ) : ?>
                    <li>
                        <noindex><a href="<?= path('news/view', ['id'=>$newsItem->id]); ?>" rel="nofollow">
                                <?= htmlentities(mb_strlen($newsItem->title,'UTF-8')>22 ? mb_substr($newsItem->title,0,22,'UTF-8').'...' : $newsItem->title); ?>
                        </a></noindex>
                    </li>
                <? endforeach; ?>
            </ul>
        </div>
    </div>
</noindex><br/>