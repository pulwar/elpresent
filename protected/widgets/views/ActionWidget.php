<? if(!empty($user_id)):?>
<script>
    $(document).ready(function() {
        $('#action_widget_submit').click(function(){
            $('.action-block .count').addClass('clicked');
            $.ajax({
                type: 'POST',
                data:{'user_id': <?=$user_id?>},
                cache: false,
                url: '/actions/click',
                success: function (data) {
                    var arr = JSON.parse(data);
                    alert(arr.msg);
                    $('.action-block .count').text(arr.count);
                    $('.action-block .count').removeClass('clicked');
                }
            });

        });
    });
</script>
<? endif;?>
<div class="action-block">
    <h3>Внимание! Акция!</h3>
    <div class="top">
        <div class="count">
            <?=$count?>
            <!--Уже кликнули: <span class="label label-danger count-info"><? //$count?></span> раз(а) из <span class="label label-success">500</span>-->
        </div>
        <? if(!empty($user_id)):?>
            <form action="/" method="post">
                <input type="button" id="action_widget_submit" name="action_submit" value="Кликнуть" class="btn btn-default">
            </form>
        <? endif;?>
    </div>
    <div class="bottom">
        <p>Кликни 500-м и получи сертификат! <a href="/priz-za-klik" target="_blank">Подробнее</a></p>
        <? if(empty($user_id)):?>
            <p>Для участия авторизуйтесь!</p>
        <? endif;?>
    </div>
</div>
