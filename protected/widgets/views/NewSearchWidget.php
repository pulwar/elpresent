<div class="h_inner">
        <div class="page_width">
        <a class="h_logo" href="/">
            <img class="logo-main-img"
                 src="/img/front_bootstrap/h_logo1.svg" alt="El Present">
        </a>
        <form action="/search">
            <div class="search_wrap">
                <input class="search_field" type="text" placeholder="Какой подарок вы ищете?" name="q" <?if($search_request['q']):?>value="<?= htmlentities($search_request['q'])?>"<?endif;?>>
                <input class="search_btn" type="submit" value="Найти">
            </div>
        </form>
        <form action="/search">
            <div class="mob_btn_filter">Расширенный поиск <i></i></div>
            <div class="h_filter">
                <div class="h_filter_cell">
                    <div class="h_filter_label">повод</div>
                    <div class="h_filter_select">
                        <select class="chosen-select" name="options[0]">
                            <option value="0">Все поводы</option>
                            <?foreach ($reasons as $reason):?>
                                <option value="<?=$reason->feature_value_id?>" <?if($search_request['options']['0']==$reason->feature_value_id && !$search_request['options']['1']):?>selected="true"<?endif;?>><?=$reason->feature_value_value?></option>
                            <?endforeach;?>
                        </select>
                    </div>
                </div>
                <div class="h_filter_cell">
                    <div class="h_filter_label">кому подарок</div>
                    <div class="h_filter_select">
                        <select class="chosen-select" name="features[0]">
                            <option value="0">Все подарки</option>
                            <?foreach ($recipients as $recipient):?>
                                <option value="<?=$recipient->feature_value_id?>" <?if($search_request['features']['0']==$recipient->feature_value_id):?>selected="true"<?endif;?>><?=$recipient->feature_value_value?></option>
                            <?endforeach;?>

                        </select>
                    </div>
                </div>
                <div class="h_filter_cell">
                    <div class="h_filter_label">стоимость подарка</div>
                    <div class="h_filter_numb">
                        <span>от:</span>
                        <input type="text"  pattern="[0-9]{,5}" name="prices[0]" <?if($search_request['prices']['0']):?>value="<?=$search_request['prices']['0']?>"<?endif;?>>
                        <span>до:</span>
                        <input type="text" pattern="[0-9]{,5}" name="prices[1]" <?if($search_request['prices']['1']):?>value="<?=$search_request['prices']['1']?>"<?endif;?>>
                    </div>
                </div>
                <button  type="submit" class="h_filter_btn">Показать</button>
            </div>
        </form>
        <input type="hidden" name="is_new" <?if($search_request['is_new']):?>value="1"<?endif;?>>
        <input type="hidden" name="off_price" <?if($search_request['off_price']):?>value="1"<?endif;?>>
    </div>

</div>