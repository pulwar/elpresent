<? if(!empty($slides)): ?>
    <div class="widget slider">
        <ul>
            <? foreach($slides as $k => $slide):?>
                <? if(!empty($slide->show)):?>
                    <li class="item<?=$k?> <?=$k==0 ? ' show' : ''?>">
                        <? if(empty($slide->link)):?>
                            <img src="/images/slider/<?=$slide->img?>" alt="<?=$slide->title?>"/>
                        <? else:?>
                            <a href="<?=$slide->link?>" title="<?=$slide->title?>">
                                <img src="/images/slider/<?=$slide->img?>" alt="<?=$slide->title?>"/>
                            </a>
                        <? endif;?>
                    </li>
                <? endif;?>
            <? endforeach;?>
        </ul>
    </div>
<? endif;?>