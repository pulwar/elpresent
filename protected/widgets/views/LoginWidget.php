<? if (!Yii::app()->user->getId()) : ?>
<? Yii::app()->clientScript->registerScriptFile('/js/ui.achtung-min.js', CClientScript::POS_END); ?>
    <div class="authorization-box">
        <form id="authorization-form">
            <a class="authorization-box-close" href="javascript: void(0);"></a>
            <p class="authorization-box-text">
                <span class="auth-welcome">Добро пожаловать!</span>
                    	Введите регистрационный E-mail и пароль
            </p>
            <input type="text" class="email" id="log" name="authorization-box-email" value="" /><br />
            <input type="password" class="password" name="authorization-box-password" value="" /><br />
            <a href="/forgot_password" class="lostpassword">Забыли пароль?</a>
            <input type="submit" class="authorization-box-button" value="" />
        </form>
    </div>
    <script type="text/javascript">
        $(document).ready(function()
        {
            /* sending form on enter press */
           $('input#log,input[name=authorization-box-password]')
            .keypress(function(e)
            {
                if(e.which == 13 && $('input#log').val() && $('input[name=authorization-box-password]').val())
                {
                    /*$('.authorization-box-button[type=submit]').trigger('click');*/

                    $('#authorization-form').submit();

                    e.preventDefault();
                    
                    return false;
                }
            });


            $('.login-button').click(function()
            {
                $('.authorization-box').slideToggle(200, function(){$('input#log').focus();});

                return false;
            });

            $('.authorization-box-close').click(function()
            {
                $('.authorization-box').slideToggle(80);

                return false;
            });

            $('#authorization-form').submit(submitFunc);

            
            function submitFunc()
            {
                var userEmail    = $('.authorization-box .email#log').val();
                var userPassword = $('.authorization-box .password').val();

                $.ajax(
                {
                    type : 'POST',
                    url  : '/ajax_login',
                    data : {'user_email' : userEmail, 'user_password' : userPassword},
                    success : function(data)
                    {
                        if ( data.substr(-4) == 'fail' )
                        {
                            $.achtung(
                            {
                                className : 'achtungFail',
                                message   : 'Неверный E-mail либо пароль!',
                                timeout   : 5
                            });
                            return;
                        }

                        location.href = '/catalog/all.html';
                    }
                });
                return false;
            }


            $('.authorization-box .email#log').defaultValue('Ваш E-mail');
            $('.authorization-box .password').defaultValue('Пароль');
        });
    </script>    
<? endif; ?>