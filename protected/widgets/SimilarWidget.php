<? 
class RecommendWidget extends CWidget
{
    public function init()
    {
            
    }
    
    public function run()
    {
        $list = new ModelPager(Item::model()->similar(),10);
        
        $this->render('SimilarWidget', array
        (
            'list' => $list    
        ));
    }
}