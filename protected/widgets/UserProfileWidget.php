<? 
class UserProfileWidget extends CWidget
{
    private $_cartItemsCnt = 0;
    private $_ordersCnt    = 0;
    
    public function init()
    {
        $userId = Yii::app()->user->getId();

        $this->_cartItemsCnt = Cart::model()->count('user_id = '.$userId);
        $this->_ordersCnt    = Order::model()->count('user_id = '.$userId.' AND visibility = '.Order::VISIBLE, array());
    }
    
    public function run()
    {
        $this->render('UserProfileWidget', array
        (
            'cartItemsCnt' => $this->_cartItemsCnt,
            'ordersCnt'    => $this->_ordersCnt
        ));
    }
}