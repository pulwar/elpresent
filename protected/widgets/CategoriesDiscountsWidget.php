<?php

class CategoriesDiscountsWidget extends CWidget
{
    public $view;

    public function run()
    {
        $categories = Discounts::model()->getCategoriesByIds('object');
        $this->render('CategoriesDiscountsWidget' . $this->view, ['categories' => $categories]);
    }

}
