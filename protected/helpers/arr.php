<?php

class Arr{
	/**
	 * @param        $arr
	 * @param bool   $item
	 * @param string $glue
	 *
	 * @return string
	 */
	public static function arrInStr($arr,  $item = false, $glue = ', ') {
		if($item !== false && !empty($arr[$item])){
			$str = $arr[$item];
		}elseif(!empty($arr) && is_array($arr)){
			$str = implode($glue, $arr);
		}else{
			$str = $arr;
		}
		return $str;
	}
}
