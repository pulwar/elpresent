<?php

class Url
{
    /**
     * Текущий url
     *
     * @param string $long
     * @param string $http
     * @param bool   $clear
     *
     * @return mixed|string
     */
    public static function curr($long = 'total', $http = 'http', $clear = false)
    {
        if ($long == 'total') {
            $url = $http . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        } else {
            $url = $_SERVER['REQUEST_URI'];
        }
        if ($clear == true) {
            $query = strstr($url, '?');
            $url = str_replace($query, '', $url);
        }
        return $url;
    }

    /**
     * Нажата кнопка пагинации любая кроме первой
     *
     * @return bool
     */
    public static function is_pagination()
    {
        $url = trim($_SERVER['REQUEST_URI'], '/');
        if (preg_match('/.*page\d+/i', $url)) {
            return true;
        }
        return false;
    }

    /**
     * Это главная?
     */
    public static function is_front($mode = 'soft')
    {
        $url = trim($_SERVER['REQUEST_URI'], '/');
        $strpos = strpos($url, '?');

        if ($strpos !== false && $mode == 'hard') {
            return false;
        }

        if (empty($url) || $url == 'brest' || $url == 'gomel'
            || $url == 'grodno'
            || $url == 'mogilev'
            || $url == 'vitebsk'
        ) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $city
     *
     * @return bool
     */
    public static function is_city($city)
    {
        $city = trim(strtolower($city));
        $url = trim($_SERVER['REQUEST_URI'], '/');
        if ($city == 'minsk' && empty($url)) {
            return true;
        } elseif (strpos($url, $city) !== false) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Сравнение текущей страницы с $page
     */
    public static function is_page($page)
    {
        return strpos(self::curr(), $page) !== false ? true : false;
    }

    /**
     *
     * Добавляет ксласс current к активным ссылкам;
     * если передан только $url_current, ищется совпадение url c контроллером
     * если два параметра, ищется совпадения текущего url с url пункта меню
     *
     * @param string $match
     * @return string
     */
    public static function active($match = 'NULL')
    {
        if (empty($match)) {
            return '';
        }

        $url_current = self::curr('short', 'http', true);
        if ($url_current == '/' && $match == 'main') {
            return ' current';
        } else {
            return strpos($url_current, $match) !== false ? 'active' : '';
        }
    }

    public static function is_localhost()
    {
        if ($_SERVER["REMOTE_ADDR"] == '127.0.0.1') {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Получить url без пагинации (page1, page2 и т.д.)
     *
     * @param $url
     *
     * @return string
     */
    public function get_url_without_pagination($url)
    {
        return preg_replace('/page\d+/', '', $url);
    }
}
