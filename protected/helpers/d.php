<?php

class D{
    public static function pr($str, $die = null) {
        echo "<pre style='background: #FFDDDD; color:#000'>";
        print_r($str);
        echo '</pre>';
        if ($die != null)
            die('Сарботал die');
    }

    public static function get_function_info($function_name){
        $func = new ReflectionFunction($function_name);
        // Печать документации
        printf("<pre> %s </pre>", str_replace("'",'',var_export($func->getDocComment(), 1)));
        // Вывод основной информации
        printf(
            "<i>%s</i>
             <b>%s</b>".
            "<br>%s".
            "<br>lines %d to %d",
            $func->isInternal() ? 'core-function' : 'user-function',
            $func->getName(),
            $func->getFileName(),
            $func->getStartLine(),
            $func->getEndline()
        );

        // Вывод статических переменных
        if ($statics = $func->getStaticVariables())
        {
            printf("<br>Статические переменные: %s", var_export($statics, 1));
        }
    }


    public static function get_class_info($class_name){
        $r = new ReflectionClass($class_name);
        $parents= [];

        $constatnts = $r->getConstants();
        $properties = $r->getProperties();
        $methods = $r->getMethods();

        $default_properties = $r->getDefaultProperties();

        //while ($parent = $r->getParentClass()) {
            //$parents[] = $parent->getName();
        //}
        echo  '<div style="background:#fff">';

        echo $r->getFileName().'<br>';

        echo '<pre>'.$r->getDocComment(). '</pre><br>';

        //$end_line = $r->getEndLine();



        echo '<pre>';
        echo '<h4>constatnts</h4>';
        var_dump($constatnts);
        echo '<h4>default_properties</h4>';
        var_dump($default_properties);
        echo '<h4>properties</h4>';
        var_dump($properties);
        echo '<h4>methods</h4>';
        var_dump($methods);
        echo '</pre>';

        echo '</div>';
    }
}
