<?php

class Text{
    /**
     * Создает рандомную строку на латинице
     * @param $length Количество символов
     * @param $chartypes Я предавал всегда all
     * @return string
     */
    public static function random_string($length, $chartypes) {
        $chartypes_array=explode(",", $chartypes);
        // задаем строки символов.
        //Здесь вы можете редактировать наборы символов при необходимости
        $lower = 'abcdefghijklmnopqrstuvwxyz'; // lowercase
        $upper = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'; // uppercase
        $numbers = '1234567890'; // numbers
        $special = '^@*+-+%()!?'; //special characters
        $chars = "";
        // определяем на основе полученных параметров,
        //из чего будет сгенерирована наша строка.
        if(in_array('all', $chartypes_array)){
            $chars = $lower.$upper.$numbers.$special;
        }
        else{
            if(in_array('lower', $chartypes_array))
                $chars = $lower;
            if(in_array('upper', $chartypes_array))
                $chars .= $upper;
            if(in_array('numbers', $chartypes_array))
                $chars .= $numbers;
            if(in_array('special', $chartypes_array))
                $chars .= $special;
        }
        // длина строки с символами
        $chars_length = (strlen($chars) - 1);
        // создаем нашу строку,
        //извлекаем из строки $chars символ со случайным
        //номером от 0 до длины самой строки
        $string = $chars{rand(0, $chars_length)};
        // генерируем нашу строку
        for ($i = 1; $i < $length; $i = strlen($string)){
            // выбираем случайный элемент из строки с допустимыми символами
            $random = $chars{rand(0, $chars_length)};
            // убеждаемся в том, что два символа не будут идти подряд
            if ($random != $string{$i - 1}) $string .= $random;
        }
        // возвращаем результат
        return $string;
    }

    /**
     * Получение алиаса по строке
     * @param $str
     * @return mixed
     */
    public static function getAliasByStr($str)
    {
        $rus = ['А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П','Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я','а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п','р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я',' '];
        $lat = ['a','b','v','g','d','e','e','gh','z','i','y','k','l','m','n','o','p','r','s','t','u','f','h','c','ch','sh','sch','y','y','y','e','yu','ya','a','b','v','g','d','e','e','gh','z','i','y','k','l','m','n','o','p','r','s','t','u','f','h','c','ch','sh','sch','y','y','y','e','yu','ya',' '];
        $str = str_replace($rus, $lat, $str); // перевеодим на английский
        $str = str_replace('-', '', $str); // удаляем все исходные "-"
        $slag = preg_replace('/[^A-Za-z0-9-]+/', '-', $str); // заменяет все символы и пробелы на "-"
        return $slag;
    }

    /**
     * Умная обрезка строки
     * $str = 'Исходная строка для усечения';
     * echo cutStr($str, 10);
     * Получим "Исходная ..."
     * @param string $str - исходная строка
     * @param int $lenght - желаемая длина результирующей строки
     * @param string $end - завершение длинной строки
     * @param string $charset - кодировка
     * @param string $token - символ усечения
     * @return string - обрезанная строка
     */
    public static function setCutStr($str, $lenght = 100, $end = '&nbsp;&hellip;', $charset = 'UTF-8', $token = '~') {
        $str = strip_tags($str);
        if (mb_strlen($str, $charset) >= $lenght) {
            $wrap = wordwrap($str, $lenght, $token);
            $str_cut = mb_substr($wrap, 0, mb_strpos($wrap, $token, 0, $charset), $charset);
            return $str_cut .= $end;
        } else {
            return $str;
        }
    }

    /**
     * Limits a phrase to a given number of words.
     *
     *     $text = Text::limit_words($text);
     *
     * @param   string  $str        phrase to limit words of
     * @param   integer $limit      number of words to limit to
     * @param   string  $end_char   end character or entity
     * @return  string
     */
    public static function limitWords($str, $limit = 100, $end_char = NULL)
    {
        $limit = (int) $limit;
        $end_char = ($end_char === NULL) ? '…' : $end_char;

        if (trim($str) === '')
            return $str;

        if ($limit <= 0)
            return $end_char;

        preg_match('/^\s*+(?:\S++\s*+){1,'.$limit.'}/u', $str, $matches);

        // Only attach the end character if the matched string is shorter
        // than the starting string.
        return rtrim($matches[0]).((strlen($matches[0]) === strlen($str)) ? '' : $end_char);
    }

    /**
     * Обрезка слова от начала до сивола $symbol
     * @param $str
     * @param $symbol
     * @return string
     */
    public static function doPruningToSymbol($str, $symbol){
        return substr($str, 0, strpos($str, $symbol));
    }
}
