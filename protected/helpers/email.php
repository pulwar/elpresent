<?php

class Email
{

    /**
     * smtp - обертка для класса EMailer
     *
     * @param EMailer $mailer
     * @param $email
     * @param $subject
     * @param $body
     * @param array $config
     * @param array $attach $attach[0] - path, $attach[1] - name
     * @return bool
     */
    public static function EMailerSmtpSend(EMailer $mailer, $email, $subject, $body, array $config, array $attach = [])
    {
        $mailer->Host = 'smtp.yandex.ru';
        $mailer->IsSMTP();
        $mailer->isHTML();
        $mailer->CharSet = 'UTF-8';
        $mailer->SMTPDebug = 1;
        $mailer->SMTPAuth = true;
        $mailer->From = 'elpwork@yandex.ru';
        $mailer->Port = 465;
        $mailer->SMTPSecure = 'ssl';
        $mailer->Username = $config['username'];
        $mailer->Password = $config['password'];
        $mailer->FromName = 'elpwork@yandex.ru';
        $mailer->AddAddress($email);
        $mailer->Subject = $subject;
        $mailer->Body = $body;
        if(!empty($attach)){
            $mailer->addAttachment($attach[0], $attach[1]);
        }

        if (!$mailer->Send()) {
            return false;
        }

        return true;
    }
}