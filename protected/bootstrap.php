<?php

function build_config($__target='web', $__env = 'dev')
{
    extract(require __DIR__ . '/config/parameters.php');

    // merge config
    $__config = require __DIR__ .'/config/config.php';
    $__configEnv = require __DIR__ .'/config/config_'.$__env.'.php';
    $__configTarget = require __DIR__ .'/config/'.$__target.'.php';

    return CMap::mergeArray(CMap::mergeArray($__config, $__configEnv), $__configTarget);
}


defined('EL_ENV') or define('EL_ENV', 'dev');

$yii = __DIR__ . '/../framework/framework/' . (YII_DEBUG ? 'yii' : 'yiilite') . '.php';
$config=dirname(__FILE__).'/config/_main.php';

// setting up locale settings
date_default_timezone_set('Europe/Minsk');

// autoloading
// load composer autoloader
$autoload = require __DIR__ . '/../vendor/autoload.php';
require_once($yii);

// shortcuts
require __DIR__ . '/shortcuts.php';

$config = build_config(PHP_SAPI === 'cli' ? 'console' : 'web', EL_ENV);
if (PHP_SAPI === 'cli') {
    $app = Yii::createConsoleApplication($config);
} else {
    $app = Yii::createWebApplication($config);
}

return $app;