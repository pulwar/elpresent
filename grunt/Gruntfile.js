module.exports = function (grunt) {
    grunt.initConfig({

		watch: { //описываем работу плагина слежки за файлами.
            scripts1: {
				files: [
					'D:/php/elpresent.loc/css/front_bootstrap/**/*.css', 
					'D:/php/elpresent.loc/js/front_bootstrap/**/*.js',
					'D:/php/elpresent.loc/js/functions.js',
				],  //следить за всеми js файлами в папке src
                tasks: ['concat']  //при их изменении запускать следующие задачи
            },
			
			scripts3: {
                files: [
					'D:/php/elpresent.loc/css/front_bootstrap/prodaction.min.css',
				],
                tasks: ['cssmin']
            },
			
			scripts2: {
                files: [
					'D:/php/elpresent.loc/js/front_bootstrap/prodaction.min.js',
				],
                tasks: ['uglify']
            },

        },
		
		concat: {
			options: {
			  separator: ';',
			},
			javascripts: {
			  src: [
					'D:/php/elpresent.loc/js/functions.js',
                    'D:/php/elpresent.loc/js/front_bootstrap/bootstrap.js',
                    'D:/php/elpresent.loc/js/front_bootstrap/bootstrap-datepicker.js',
                    'D:/php/elpresent.loc/js/front_bootstrap/star-rating.min.js',
                    'D:/php/elpresent.loc/js/front_bootstrap/jquery.groupinputs.min.js',
                    'D:/php/elpresent.loc/js/front_bootstrap/app.js',
                    'D:/php/elpresent.loc/js/front_bootstrap/lightbox.min.js',
			  ],
			   dest: 'D:/php/elpresent.loc/js/front_bootstrap/prodaction.min.js'
			},
			
			styles: {
			  src: [
					'D:/php/elpresent.loc/css/front_bootstrap/custom.css',
					'D:/php/elpresent.loc/css/front_bootstrap/star-rating.css',
					'D:/php/elpresent.loc/css/front_bootstrap/bootstrap-datepicker.css',
					'D:/php/elpresent.loc/css/front_bootstrap/lightbox.css',
			  ],
			   dest: 'D:/php/elpresent.loc/css/front_bootstrap/prodaction.min.css'
			}
		},
		
		cssmin: {
		  options: {
			shorthandCompacting: false,
			roundingPrecision: -1
		  },
		  target: {
			files: {
			  'D:/php/elpresent.loc/css/front_bootstrap/prodaction.min.css': ['D:/php/elpresent.loc/css/front_bootstrap/prodaction.min.css']
			}
		  }
		},

        uglify: {
            javascripts: {
                src: 'D:/php/elpresent.loc/js/front_bootstrap/prodaction.min.js',
                dest: 'D:/php/elpresent.loc/js/front_bootstrap/prodaction.min.js'
            },
        }

    });

	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-watch');
	//grunt.loadNpmTasks('grunt-reload');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');

    grunt.registerTask('default', [
        'concat',
        'uglify',
		'cssmin',
		'watch',
    ]);
};
