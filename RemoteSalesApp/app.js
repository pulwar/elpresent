Ext.application({

	name: 'MVC',

	controllers: [
		'MVC.controller.Login',
		'MVC.controller.Main'
	],

	/*
	* Нужно только лишь для подключения /view/Login.js
	* Можно заменить подключением в index.html
	*/
	views: [
		'MVC.view.Login',
		'MVC.view.Main',
		'MVC.view.GiftView',
		'MVC.view.GiftsTopTb',
		'MVC.view.OrdersTopTb',
		'MVC.view.GiftsGrid',
		'MVC.view.OrdersGrid',
		'MVC.view.ScoresGrid',
		'MVC.view.UsersGrid',
		'MVC.view.UsersTopTb',
		'MVC.view.PickupTopTb',
		'MVC.view.PickupGrid'
	],

	stores: [
		'MVC.store.GiftsStore',
		'MVC.store.OrdersStore',
		'MVC.store.ScoresStore',
		'MVC.store.UsersStore',
		'MVC.store.PickupStore'
	],

	models: [
		'MVC.model.Gifts',
		'MVC.model.Orders',
		'MVC.model.GiftView',
		'MVC.model.Scores',
		'MVC.model.Users',
		'MVC.model.Pickup'
	],

	launch: function ()
	{
		var loggedIn = Ext.util.Cookies.get('LoggedIn');
		Ext.widget(loggedIn ? 'main' : 'login').show();
	}
});