function Helpers(Ext){

	this.Paginator = function(store) {
		this.xtype = 'pagingtoolbar';
		this.dock = 'bottom';
		this.displayInfo = true;
		this.beforePageText = 'Страница';
		this.afterPageText = 'из {0}';
		this.displayMsg = 'Штук {0} - {1} из {2}';
		this.store = store;
	}

	/**
	 * Отбображение текстовых блоков во вкладке очков
	 * @returns {*[]}
	 */
	this.getScoreDockedItems = function () {
		return [
			{
				xtype: 'label',
				dock: 'bottom',
				text: LABEL_BOTTOM_IN_SCORES_GRID,
				margin: '20'
			},
			{
				xtype: 'label',
				dock: 'top',
				text: LABEL_TOP1_IN_SCORES_GRID,
				margin: '20'
			},
			{
				xtype: 'label',
				dock: 'top',
				text: LABEL_TOP2_IN_SCORES_GRID,
				margin: '0 20 20 20'
			},
			{
				xtype: 'label',
				dock: 'top',
				text: LABEL_TOP3_IN_SCORES_GRID,
				margin: '0 20 20 20'
			},
		]
	}

	/**
	 * CSS для надписи справа от логотипа
	 * @type {{paddingLeft: string, color: string, textShadow: string}}
	 */
	this.CssLoginLabel = {
		paddingLeft: '200px',
		color:'gray',
		textShadow: '1px 1px 1px white'
	}
	/**
	 * Получение значение id по контексту из другой ячейки
	 * @param context
	 * @returns {string|innerText|*}
	 */
	this.gitIdByRowGrid = function(context, id){
		//var grid = Ext.getCmp('myOrdersViewId');
		var grid = Ext.getCmp(id);
		var row = grid.getView().getRow(context.rowIdx);
		var cell = Ext.get(row).first();
		var id = cell.dom.innerText;
		return id;
	}
	/**
	 * Получение авторизованного юзера из куков
	 * @returns {*}
	 */
	this.getAuth = function(){
		var loggedIn = Ext.util.Cookies.get('LoggedIn');
		var loggedIn_obj = JSON.parse(loggedIn);
		return loggedIn_obj;
	}

	this.isAdmin = function(){
		var loggedIn = Ext.util.Cookies.get('LoggedIn');
		var loggedIn_obj = JSON.parse(loggedIn);
		if(loggedIn_obj.email == 'admin' || loggedIn_obj.email == 'elpresent'){
			return true;
		}
		return false;
	}
	/**
	 * Получить дату активации подарка
	 * @param val
	 * @param eOpts
	 * @returns {string}
	 */
	this.getDateActivation = function(val, eOpts){
		var title = eOpts.record.data.item_title;
		if (in_array(title, ITEMS_FOR_6_MONTH)){
			val = parseInt(IN_6_MONTH) + parseInt(val);
		}else{
			val = parseInt(IN_3_MONTH) + parseInt(val);
		}
		return getDateByUnixtTime(val);
	}
	/**
	 * Получить массив из строки типа 10000,20000 для создания выпадающего списка
	 * @param context
	 * @returns {Array}
	 */
	this.getArrByStr = function(context){
		var arr = context.value.split(',');
		var data = [];
		for(var i = 0; i < arr.length; i++){
			data[i] = {
				'name': arr[i],
				'value': arr[i]
			}
		}
		return data;
	}
}
