var helpers = new Helpers(Ext);
Ext.define('MVC.view.Main', {
	extend: 'Ext.container.Container',
	plugins: 'viewport',
	alias: 'widget.main',
	xtype: 'main',
	initComponent: function(){
		var that = this;
		Ext.apply(that, {
		});

		this.callParent(arguments)
	},
	layout: {
		type: 'border'
	},
	items: [
		{
			region: 'north',
			xtype: 'component',
			padding: 10,
			height: 40,
			id: 'logo',
			viewModel: {
				formulas: {
					login: function (get){
						return helpers.getAuth().email
					},
					city: function(get){
						var city_id = helpers.getAuth().city;
						return city_id
					}
				}
			},
			bind: {
				html: 'Ваш логин: <span style="color:black">{login}</span> Id города <span style="color:black">{city}</span>',
				style: helpers.CssLoginLabel
			}
		},
		{
			region: 'center',
			xtype: 'tabpanel',
			listeners: {
				afterrender: function(){
					var loggedIn = Ext.util.Cookies.get('LoggedIn');
					for (var i = 0; i < this.items.items.length; i++){
						if(this.items.items[i].title == 'Баллы' && helpers.getAuth().score_hide){
							this.items.items[i].destroy()
						}
						if(this.items.items[i].title == 'Юзеры' && !helpers.isAdmin()){
							this.items.items[i].destroy()
						}
					}
				}
			},
			items: [
				{
					title: 'Впечатления',
					tbar: {
						xtype: 'GiftsTopTb'
					},
					layout: 'fit',
					items: [{
						xtype: 'GiftsGrid'
					}]
				},
				{
					title: 'Отчетность',
					tbar: {
						xtype: 'OrdersTopTb'
					},
					layout: 'fit',
					items: [{
						xtype: 'OrdersGrid'
					}]
				},
				{
					title: 'Баллы',
					layout: 'fit',
					items: [{
						xtype: 'ScoresGrid'
					}]
				},
				{
					title: 'Юзеры',
					tbar: {
						xtype: 'UsersTopTb'
					},
					layout: 'fit',
					items: [{
						xtype: 'UsersGrid'
					}]
				},
				{
					title: 'Самовывоз',
					tbar: {
						xtype: 'PickupTopTb'
					},
					layout: 'fit',
					items: [{
						xtype: 'PickupGrid'
					}]
				}
			]
		}
	]
});