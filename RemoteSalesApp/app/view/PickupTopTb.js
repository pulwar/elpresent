Ext.define('MVC.view.PickupTopTb' ,{
	extend: 'Ext.toolbar.Toolbar',
	alias: 'widget.PickupTopTb',
	dock: 'bottom',
	items: [
		{
			xtype: 'textfield',
			width: 50,
			readOnly: true,
			itemId: 'PickupIdValue',
			id: 'PickupIdValue',
			style: {
				display: 'none'
			}
		},
		{
			xtype: 'button',
			itemId: 'SavePickup',
			listeners: {
				beforerender: function (i)
				{
					// для юзеров "на связи"
					var loggedIn = Ext.util.Cookies.get('LoggedIn');
					var loggedIn_obj = JSON.parse(loggedIn);
					if(loggedIn_obj.email.search(/ns_/i) != -1){
						i.disable();
					}
				}
			},
			iconCls: 'dollar',
			style: {
				boxShadow: '1px 1px 3px red',
				margin: '5px'
			},
			tooltip: {
				text: 'Регистрация покупки',
				anchor: 'top'
			}
		}
		//{
		//	xtype: 'button',
		//	itemId: 'PickupPrint',
		//	iconCls: 'printer',
		//	style: {
		//		margin: '5px'
		//	},
		//	tooltip: {
		//		text: 'Печать',
		//		anchor: 'top'
		//	}
		//},
	]
});