Ext.define('MVC.view.GiftsGrid' , {
	extend: 'Ext.grid.Panel',
	alias: 'widget.GiftsGrid',
	id: 'myGiftsViewId',
	title: 'Впечатления',
	store: 'MVC.store.GiftsStore',
	plugins: [
		new Ext.grid.plugin.CellEditing({clicksToEdit: 1}),
		'gridfilters'
	],
	listeners: {
		beforeedit: function (editor, context){
			var store = Ext.create('Ext.data.Store', {
				queryMode: 'local',
				storeId: 'columnStore',
				fields: ['name', 'value'],
				data: helpers.getArrByStr(context)
			});
			Ext.getCmp('myComboBox').bindStore(store);
		},
		edit: function(editor, context){
			var price = context.value;
			Ext.ComponentQuery.query('tabpanel #IdSum')[0].setValue(price);
		}
	},
	dockedItems: [
		new helpers.Paginator('MVC.store.GiftsStore')
	],
	columns: [
		{
			text: 'id',
			dataIndex: 'id',
			width: 50
		},
		{
			text: 'Название',
			dataIndex: 'title',
			filter: 'string',
			width: 350
		},
		{
			text: 'Ссылка',
			dataIndex: 'perm_link',
			filter: 'string',
			width: 250,
			renderer: function(val) {
				return '<a style="color:#3892D3" target="_blank" href="http://elpresent.by/' + val + '">'+val+'</a>';
			}
		},
		{
			text: 'Цена',
			dataIndex: 'price',
			filter: 'number',
			width: 200,
			renderer: function(val) {
				return val;
			},
			editor: {
				xtype: 'combobox',
				id: 'myComboBox',
				displayField: 'name',
				valueField: 'value',
				store:  Ext.create('Ext.data.Store', {
					queryMode: 'local',
					storeId: 'columnStore',
					fields: ['name', 'value'],
					data: []
				})
			}
		},
		{
			text: 'Категория',
			sortable: false,
			dataIndex: 'cats',
			width: 350
		}
	]
});
