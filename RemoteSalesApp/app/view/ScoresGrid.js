Ext.define('MVC.view.ScoresGrid' ,{
	extend: 'Ext.grid.Panel',
	alias: 'widget.ScoresGrid',
	id: 'myScoresViewId',
	title: 'Бонусная программа',
	store: 'MVC.store.ScoresStore',
	plugins: [
		new Ext.grid.plugin.CellEditing({clicksToEdit: 1}),
	],
	listeners: {
		edit: function(editor, context){
			if(helpers.isAdmin()){
				Ext.Ajax.request({
					url: '/extjs/score/edit',
					params: {
						'originalValue': context.originalValue,
						'value': context.value,
						'part' : context.colIdx == 1 ? 0 : 1
					},
					success: function(response){
						var data = Ext.decode(response.responseText);
						if(data.success){
							Ext.getCmp('myScoresViewId').getStore().reload();
						}
						else{
							Ext.getCmp('myScoresViewId').getStore().reload();
							Ext.Msg.alert('Ошибка','Не удалось обновить данные');
						}
					}
				});
			}else{
				Ext.getCmp('myScoresViewId').getStore().reload();
				Ext.Msg.alert('Ошибка','Вы не имеете прав на эту операцию');
			}
		}
	},
	dockedItems: helpers.getScoreDockedItems(),
	columns: [
		{
			text: 'Очки',
			dataIndex: 'score',
			width: 150
		},
		{
			text: 'Сумма в рублях ОТ',
			dataIndex: 'from',
			width: 150,
			editor: {
				xtype: 'textfield',
				displayField: 'name',
				valueField: 'value'
			}
		},
		{
			text: 'Сумма в рублях ДО',
			dataIndex: 'to',
			width: 150,
			editor: {
				xtype: 'textfield',
				displayField: 'name',
				valueField: 'value'
			}
		}
	]
});

