Ext.define('MVC.view.UsersGrid' , {
	extend: 'Ext.grid.Panel',
	alias: 'widget.UsersGrid',
	id: 'myUsersViewId',
	title: 'Пользователи',
	store: 'MVC.store.UsersStore',
	plugins: [
		new Ext.grid.plugin.CellEditing({clicksToEdit: 1}),
	],
	listeners: {
		edit: function(editor, context){

			if(helpers.isAdmin()){
				console.log(context);
				var field = context.field;
				var value = context.value;
				var rowIdx = context.rowIdx;
				var rows = Ext.getCmp('myUsersViewId').getStore().getRange();
				var user = rows[rowIdx].data.user;

				Ext.Ajax.request({
					url: '/extjs/user/edit',
					params: {
						'field': field,
						'value': value,
						'user': user
					},
					success: function(response){
						var data = Ext.decode(response.responseText);
						if(data.success){
							Ext.getCmp('myUsersViewId').getStore().reload();
						}
						else{
							Ext.getCmp('myUsersViewId').getStore().reload();
							Ext.Msg.alert('Ошибка','Не удалось обновить данные');
						}
					}
				});
			}else{
				Ext.getCmp('myUsersViewId').getStore().reload();
				Ext.Msg.alert('Ошибка','Вы не имеете прав на эту операцию');
			}
		}
	},
	dockedItems: [
		new helpers.Paginator('MVC.store.UsersStore')
	],
	columns: [
		{
			text: 'Логин',
			dataIndex: 'user',
			width: 150,
			editor: {
				xtype: 'textfield',
				displayField: 'user',
				valueField: 'value'
			}
		},
		{
			text: 'Пароль',
			dataIndex: 'password',
			width: 150,
			editor: {
				xtype: 'textfield',
				displayField: 'password',
				valueField: 'value'
			}
		},
		{
			text: 'Фио',
			dataIndex: 'fio',
			width: 200,
			editor: {
				xtype: 'textfield',
				displayField: 'fio',
				valueField: 'value'
			}
		},
		{
			text: 'Название компании',
			dataIndex: 'company_name',
			width: 200,
			editor: {
				xtype: 'textfield',
				displayField: 'company_name',
				valueField: 'value'
			}
		},
		{
			text: 'Адрес компании',
			dataIndex: 'company_address',
			width: 200,
			editor: {
				xtype: 'textfield',
				displayField: 'company_address',
				valueField: 'value'
			}
		},
		{
			text: 'Скрывать очки?',
			dataIndex: 'score_hide',
			width: 120,
			editor: {
				xtype: 'checkbox',
				displayField: 'score_hide',
				valueField: 'value'
			}
		},
		{
			text: 'Скрывать оплачено?',
			dataIndex: 'paid_hide',
			width: 135,
			editor: {
				xtype: 'checkbox',
				displayField: 'paid_hide',
				valueField: 'value'
			}
		}
	]
});

