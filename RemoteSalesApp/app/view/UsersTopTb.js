Ext.define('MVC.view.UsersTopTb' ,{
	extend: 'Ext.toolbar.Toolbar',
	alias: 'widget.UsersTopTb',
	dock: 'bottom',
	items: [
		{
			xtype: 'textfield',
			width: 140,
			itemId: 'IdUser',
			id: 'user'
		},
		{
			xtype: 'textfield',
			width: 140,
			itemId: 'IdPassword',
			id: 'password'
		},
		{
			xtype: 'textfield',
			width: 190,
			itemId: 'IdFio',
			id: 'fio'
		},
		{
			xtype: 'textfield',
			width: 190,
			itemId: 'IdCompany_name',
			id: 'company_name'
		},
		{
			xtype: 'textfield',
			width: 195,
			itemId: 'IdCompany_address',
			id: 'company_address'
		},
		{
			xtype: 'checkboxfield',
			width: 120,
			itemId: 'IdScore_hide',
			id: 'score_hide'
		},
		{
			xtype: 'checkboxfield',
			width: 130,
			itemId: 'IdPaid_hide',
			id: 'paid_hide'
		},
		{
			xtype: 'button',
			width: 90,
			itemId: 'IdUserSave',
			text: 'Сохранить'
		},
		{
			xtype: 'textfield',
			width: 1,
			itemId: 'UserItemSelected',
			id: 'user_selected',
			style: {
				display: 'none'
			}
		},
		{
			xtype: 'button',
			itemId: 'DeleteUser',
			iconCls: 'del',
			style: {
				margin: '5px'
			},
			tooltip: {
				text: 'Удалить',
				anchor: 'top'
			}
		},
	]
});