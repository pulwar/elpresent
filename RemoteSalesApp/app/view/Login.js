Ext.define('MVC.view.Login', {
	extend: 'Ext.window.Window',
	xtype: 'login',
	alias: 'widget.login',

	items: [{
		// экземпляр компонента (Ext.form.Panel)
		url: '/extjs/default/login',
		xtype: 'form',
		reference: 'form-view',
		items: [{
			xtype: 'textfield',
			name: 'email',
			fieldLabel: 'Email',
			allowBlank: false
		}, {
			xtype: 'textfield',
			name: 'password',
			inputType: 'password',
			fieldLabel: 'Password',
			allowBlank: false
		}, {
			xtype: 'displayfield',
			hideEmptyLabel: false,
			value: 'Enter any non-blank password'
		}, {
			xtype: 'combobox',
			id: 'cities',
			fieldLabel: 'Города',
			name: 'city',
			store: Ext.create('Ext.data.Store', {
				id: 'city_store',
				autoLoad: true,
				fields: ['id','title'],
				proxy: {
					type: 'ajax',
					url: '/extjs/default/cities'
				}
			}),
			value: 'Минск',
			valueField:'id',
			displayField:'title',
			queryMode:'local'
		}
		],

		buttons: [{
			text: 'Login',
			//This means that the button will not be clickable until the two input fields contain values.
			formBind: true,
			itemId: 'SaveRecord'
		}]
	}],
	title: 'Авторизация'
});