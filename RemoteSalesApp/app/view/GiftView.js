Ext.define('MVC.view.GiftView', {
	extend: 'Ext.window.Window',
	alias: 'widget.giftview',
	title: 'Впечатение',
	layout: 'fit',
	autoShow: true,

	initComponent: function () {
		this.items = [
			{
				xtype: 'form',
				items: [
					{
						name: 'preview_img',
						fieldLabel: 'image',
						width: 235,
						height: 189,
						xtype: 'image',
						style: {
							margin: '10px auto',
							display: 'block',
							boxShadow: '1px 1px 5px black'
						}
					},
					{
						name: 'title',
						xtype: 'textfield',
						readOnly: true,
						width: 335,
						height: 10,
						style:{
							margin: '5px'
						}
					},
					{
						name: 'desc',
						xtype: 'textareafield',
						readOnly: true,
						width: 335,
						height: 50,
						style:{
							margin: '5px'
						}
					},
					{
						name: 'options',
						xtype: 'textareafield',
						readOnly: true,
						width: 335,
						height: 50,
						style:{
							margin: '5px'
						}
					}
				]
			}
		];

		this.callParent(arguments);
	}
});