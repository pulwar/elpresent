Ext.define('MVC.view.PickupGrid' , {
	extend: 'Ext.grid.Panel',
	alias: 'widget.PickupGrid',
	title: 'Самовывоз',
	id:'myPickupViewId',
	store: 'MVC.store.PickupStore',
	plugins: [
		'gridfilters',
		new Ext.grid.plugin.CellEditing({clicksToEdit: 1})
	],
	listeners: {
		beforerender: function(){

		},
		edit: function(editor, context){
			var id = (helpers.gitIdByRowGrid(context, 'myPickupViewId'));
			console.log(context);
			Ext.Ajax.request({
				url: '/extjs/pickup/' + context.field,
				params: {
					'id': id,
					'value': context.value
				},
				success: function(response){
					var data = Ext.decode(response.responseText);
					if(data.success){
						Ext.getCmp('myPickupViewId').getStore().reload();
					}
					else{
						Ext.getCmp('myPickupViewId').getStore().reload();
						Ext.Msg.alert('Обновление','Не удалось обновить данные');
					}
				}
			});
		}
	},
	dockedItems: [
		new helpers.Paginator('MVC.store.PickupStore')
	],
	columns: [
		{
			text: 'ID',
			dataIndex: 'id',
			width: 100
		},
		{
			text: 'Название',
			filter: 'string',
			dataIndex: 'item_title',
			width: 250
		},
		{
			text: 'Id подарка',
			dataIndex: 'item_id',
			width: 150
		},
		{
			text: 'Цена',
			dataIndex: 'price_pickup',
			width: 100
		},
		{
			text: 'Дата заказ',
			dataIndex: 'order_date',
			width: 160
		},
		{
			text: 'Имя',
			dataIndex: 'name',
			width: 120
		},
		{
			text: 'Фамилия',
			dataIndex: 'lname',
			width: 120
		},
		{
			text: 'Телефон',
			filter: 'string',
			dataIndex: 'phone',
			width: 150
		},
		{
			text: 'Номер заказа',
			filter: 'string',
			dataIndex: 'order_number',
			width: 120
		},
		{
			text: 'E-mail',
			filter: 'string',
			dataIndex: 'email',
			width: 120
		},
		{
			text: 'Оплачен',
			dataIndex: 'is_paid',
			width: 90,
			renderer: function(val) {
				return val == 0 ? 'Нет' : 'Да';
			},
			editor: {
				xtype: 'combobox',
				id: 'activated',
				displayField: 'name',
				valueField: 'value',
				store:  Ext.create('Ext.data.Store', {
					queryMode: 'local',
					storeId: 'columnStore',
					fields: ['name', 'value'],
					data: [
						{
							'name': 'Нет',
							'value': 0
						},
						{
							'name': 'Да',
							'value': 1
						}
					]
				})
			}
		}
	]
});