Ext.define('MVC.view.GiftsTopTb' ,{
	extend: 'Ext.toolbar.Toolbar',
	alias: 'widget.GiftsTopTb',
	dock: 'bottom',
	items: [
		{
			itemId: 'Logout',
			iconCls: 'exit',
			style: {
				margin: '5px'
			},
			tooltip: {
				text: 'Выйти из системы',
				anchor: 'top'
			}
		},
		{
			xtype: 'textfield',
			width: 50,
			readOnly: true,
			itemId: 'IdItem',
			id: 'myid',
			style: {
				display: 'none'
			}
		},
		{
			xtype: 'textfield',
			width: 50,
			readOnly: true,
			itemId: 'IdSum',
			id: 'sum',
			style: {
				display: 'none'
			}
		},
		{
			xtype: 'button',
			itemId: 'view',
			iconCls: 'eye',
			style: {
				margin: '5px'
			},
			tooltip: {
				text: 'Смотреть подробнее',
				anchor: 'top'
			}
		},
		{
			xtype: 'button',
			itemId: 'Save',
			listeners: {
				beforerender: function (i)
				{
					// для юзеров "на связи"
					var loggedIn = Ext.util.Cookies.get('LoggedIn');
					var loggedIn_obj = JSON.parse(loggedIn);
					if(loggedIn_obj.email.search(/ns_/i) != -1){
						i.disable();
					}
				}
			},
			iconCls: 'dollar',
			style: {
				boxShadow: '1px 1px 3px red',
				margin: '5px'
			},
			tooltip: {
				text: 'Регистрация покупки',
				anchor: 'top'
			}
		},
		{
			xtype: 'combobox',
			id: 'sale',
			fieldLabel: 'Скидка',
			style: {
				width: '200px',
				paddingLeft: '25px'
			},
			store: Ext.create('Ext.data.Store', {
				autoLoad: true,
				fields: ['val'],
				proxy: {
					type: 'ajax',
					url: '/extjs/default/sales'
				}
			}),
			value: 0,
			valueField:'val',
			displayField:'val',
			queryMode:'local'
		}
	]
});