Ext.define('MVC.view.OrdersTopTb' ,{
	extend: 'Ext.toolbar.Toolbar',
	alias: 'widget.OrdersTopTb',
	dock: 'bottom',
	items: [
		{
			xtype: 'button',
			itemId: 'ClearScore',
			iconCls: 'clear',
			style: {
				margin: '5px'
			},
			tooltip: {
				text: 'Очистить очки',
				anchor: 'top'
			}
		},
		{
			xtype: 'textfield',
			width: 170,
			readOnly: true,
			itemId: 'IdScore',
			id: 'score',
			listeners: {
				beforerender: function (i)
				{
					var loggedIn = Ext.util.Cookies.get('LoggedIn');
					var loggedIn_obj = JSON.parse(loggedIn);
					if(loggedIn_obj.score_hide) {
						i.hide()
					}
				}
			}
		},
		{
			xtype: 'button',
			itemId: 'SaveOrder',
			iconCls: 'printer',
			style: {
				margin: '5px'
			},
			tooltip: {
				text: 'Печать',
				anchor: 'top'
			}
		},
		{
			xtype: 'button',
			itemId: 'DeleteOrder',
			iconCls: 'del',
			style: {
				margin: '5px'
			},
			tooltip: {
				text: 'Удалить',
				anchor: 'top'
			}
		},
		{
			xtype: 'button',
			itemId: 'Excel',
			iconCls: 'excel',
			style: {
				margin: '5px'
			},
			tooltip: {
				text: 'Скачать отчет в Excel. Макс.' + EXCEL_COUNT + ' строк',
				anchor: 'top'
			}
		},
		{
			xtype: 'textfield',
			width: 50,
			readOnly: true,
			itemId: 'IdItemOrder',
			id: 'myidOrder',
			style: {
				display: 'none'
			}
		}
	]
});
