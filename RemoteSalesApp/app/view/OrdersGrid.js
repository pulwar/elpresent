Ext.define('MVC.view.OrdersGrid' , {
	extend: 'Ext.grid.Panel',
	alias: 'widget.OrdersGrid',
	title: 'Отчетность',
	id:'myOrdersViewId',
	store: 'MVC.store.OrdersStore',
	plugins: [
		'gridfilters',
		new Ext.grid.plugin.CellEditing({clicksToEdit: 1})
	],
	listeners: {
		beforerender: function(){
			var grid = Ext.getCmp('myOrdersViewId');
			var orders_fields = grid.headerCt.gridDataColumns;

			if(helpers.getAuth().score_hide == true || helpers.getAuth().score_hide == 'true'){
				// номер поля с начала
				orders_fields[12].setVisible(false);
			}
			if(helpers.getAuth().paid_hide == true || helpers.getAuth().paid_hide == 'true'){
				orders_fields[5].setVisible(false);
			}
		},
		edit: function(editor, context){
			var id = (helpers.gitIdByRowGrid(context,'myOrdersViewId'));
			console.log(context);
			Ext.Ajax.request({
				url: '/extjs/order/' + context.field,
				params: {
					'id': id,
					'value': context.value
				},
				success: function(response){
					var data = Ext.decode(response.responseText);
					if(data.success){
						Ext.getCmp('myOrdersViewId').getStore().reload();
					}
					else{
						Ext.getCmp('myOrdersViewId').getStore().reload();
						Ext.Msg.alert('Обновление','Не удалось обновить данные');
					}
				}
			});
		}
	},
	dockedItems: [
		new helpers.Paginator('MVC.store.OrdersStore')
	],
	columns: [
		{
			text: 'id',
			dataIndex: 'id',
			width: 50
		},
		{
			text: 'Название',
			filter: 'string',
			dataIndex: 'item_title',
			width: 250
		},
		{
			text: 'Дата продажи',
			filter: 'date',
			dataIndex: 'date',
			width: 120,
			renderer: function(val) {
				return getDateByUnixtTime(val);
			}
		},
		{
			text: 'Активировать до',
			filter: 'date',
			dataIndex: 'date',
			width: 130,
			renderer: function(val, eOpts) {
				return helpers.getDateActivation(val, eOpts);
			}
		},
		{
			text: 'Код',
			filter: 'string',
			dataIndex: 'code',
			width: 120
		},
		{
			text: 'Оплачен?',
			dataIndex: 'activated',
			filter: 'number',
			width: 70,
			renderer: function(val) {
				return val == 0 ? 'Нет' : 'Да';
			},
			/*editor: {
			 id: 'activated',
			 displayField: 'name',
			 valueField: 'value'
			 }*/
			editor: {
				xtype: 'combobox',
				id: 'activated',
				displayField: 'name',
				valueField: 'value',
				store:  Ext.create('Ext.data.Store', {
					queryMode: 'local',
					storeId: 'columnStore',
					fields: ['name', 'value'],
					data: [
						{
							'name': 'Нет',
							'value': 0
						},
						{
							'name': 'Да',
							'value': 1
						}
					]
				})
			}
		},
		{
			text: 'Логин',
			filter: 'string',
			dataIndex: 'user',
			width: 120,
			renderer: function(val) {
				/*var obj = JSON.parse(val);
				 var str = 'Компания: '+obj.user_info.company +'; логин: '+obj.email;
				 return str;*/
				return val;
			}
		},
		{
			text: 'Фио',
			filter: 'string',
			dataIndex: 'fio',
			width: 120
		},
		{
			text: 'Компания',
			filter: 'string',
			dataIndex: 'company',
			width: 120
		},
		{
			text: 'Сумма',
			filter: 'number',
			dataIndex: 'sum',
			width: 100
		},
		{
			text: 'Адрес',
			filter: 'string',
			dataIndex: 'address',
			width: 150
		},
		{
			text: 'Номер дисконтной карты',
			filter: 'number',
			dataIndex: 'discont',
			width: 180,
			editor: {
				xtype: 'textfield',
				displayField: 'name',
				valueField: 'value'
			}
		},
		{
			text: 'Очки',
			filter: 'number',
			dataIndex: 'score',
			width: 50
		},
	]
});