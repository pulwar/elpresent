Ext.define('MVC.model.Scores', {
	extend: 'Ext.data.Model',
	fields: [
		{
			name: 'score',
			type: 'int'
		},
		{
			name: 'from',
			type: 'int'
		},
		{
			name: 'to',
			type: 'int'
		}
	]
});