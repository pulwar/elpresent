Ext.define('MVC.model.Gifts', {
	extend: 'Ext.data.Model',
	fields: [
		{
			name: 'id',
			type: 'int'
		},
		{
			name: 'title',
			type: 'string'
		},
		{
			name: 'perm_link',
			type: 'string'
		},
		{
			name: 'price',
			type: 'string'
		},
		{
			name: 'cats',
			type: 'string'
		}
	]
});