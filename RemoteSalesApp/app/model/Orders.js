Ext.define('MVC.model.Orders', {
	extend: 'Ext.data.Model',
	fields: [
		{
			name: 'id', type: 'int'
		},
		{
			name: 'item_title', type: 'string'
		},
		{
			name: 'date', type: 'string'
		},
		{
			name: 'user', type: 'string'
		},
		{
			name: 'fio', type: 'string'
		},
		{
			name: 'company', type: 'string'
		},
		{
			name: 'sum', type: 'float'
		},
		{
			name: 'address', type: 'string'
		},
		{
			name: 'score', type: 'int'
		},
		{
			name: 'code', type: 'string'
		},
		{
			name: 'activated', type: 'string'
		},
		{
			name: 'discont', type: 'float'
		}
	]
});