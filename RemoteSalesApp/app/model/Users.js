Ext.define('MVC.model.Users', {
	extend: 'Ext.data.Model',
	fields: [
		{
			name: 'user',
			type: 'string'
		},
		{
			name: 'password',
			type: 'string'
		},
		{
			name: 'fio',
			type: 'string'
		},
		{
			name: 'company_name',
			type: 'string'
		},
		{
			name: 'company_address',
			type: 'string'
		},
		{
			name: 'score_hide',
			type: 'string'
		},
		{
			name: 'paid_hide',
			type: 'string'
		}
	]
});