Ext.define('MVC.model.Pickup', {
	extend: 'Ext.data.Model',
	fields: [
		{
			name: 'id',
			type: 'int'
		},
		{
			name: 'item_title',
			type: 'string'
		},
		{
			name: 'item_id',
			type: 'int'
		},
		{
			name: 'price_pickup',
			type: 'float'
		},
		{
			name: 'phone',
			type: 'string'
		},
		{
			name: 'order_date',
			type: 'string'
		},
		{
			name: 'order_number',
			type: 'string'
		},
		{
			name: 'name',
			type: 'string'
		},
		{
			name: 'lname',
			type: 'string'
		},
		{
			name: 'email',
			type: 'string'
		},
		{
			name: 'is_paid',
			type: 'int'
		}
	]
});