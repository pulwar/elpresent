Ext.define('MVC.model.GiftView', {
	extend: 'Ext.data.Model',
	fields: [
		{
			xtype: 'image',
			name: 'preview_img'
		},
		{
			name : 'title',
			type : 'string'
		},
		{
			name : 'desc',
			type : 'string'
		},
		{
			name : 'options',
			type : 'string'
		}
	]
});