Ext.define('MVC.store.ScoresStore', {
	extend: 'Ext.data.Store',
	model: 'MVC.model.Scores',
	autoLoad: true,
	pageSize: 10,
	storeId: 'OrdersStore',
	remoteSort: false,
	remoteFilter: false,
	filterOnLoad: false,
	proxy: {
		type: 'ajax',
		url: '/extjs/score',
		reader: {
			type: 'json',
			rootProperty: 'scores',
			successProperty: 'success'
		}
	}
});