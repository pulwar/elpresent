Ext.define('MVC.store.GiftsStore', {
	extend: 'Ext.data.Store',
	model: 'MVC.model.Gifts',
	autoLoad: true,
	pageSize: COUNT_ITEMS_GIFTS,
	remoteSort: true,
	remoteFilter: true,
	filterOnLoad: true,
	proxy: {
		type: 'ajax',
		url: '/extjs/gift',
		reader: {
			type: 'json',
			rootProperty: 'gifts',
			successProperty: 'success'
		}
	}
});