Ext.define('MVC.store.PickupStore', {
	extend: 'Ext.data.Store',
	model: 'MVC.model.Scores',
	autoLoad: true,
	pageSize:50,
	storeId: 'PickupStore',
	remoteSort: true,
	remoteFilter: true,
	filterOnLoad: true,
	proxy: {
		type: 'ajax',
		url: '/extjs/pickup',
		reader: {
			type: 'json',
			rootProperty: 'pickup',
			successProperty: 'success'
		}
	}
});