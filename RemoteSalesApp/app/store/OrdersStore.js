Ext.define('MVC.store.OrdersStore', {
	extend: 'Ext.data.Store',
	model: 'MVC.model.Orders',
	autoLoad: true,
	pageSize: COUNT_ITEMS_ORDERS,
	storeId: 'OrdersStore',
	remoteSort: true,
	remoteFilter: true,
	filterOnLoad: true,
	proxy: {
		type: 'ajax',
		url: '/extjs/order',
		reader: {
			type: 'json',
			rootProperty: 'orders',
			successProperty: 'success'
		}
	}
});