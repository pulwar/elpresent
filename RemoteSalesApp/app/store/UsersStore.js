Ext.define('MVC.store.UsersStore', {
	extend: 'Ext.data.Store',
	model: 'MVC.model.Scores',
	autoLoad: true,
	pageSize: 120,
	storeId: 'UsersStore',
	remoteSort: false,
	remoteFilter: false,
	filterOnLoad: false,
	proxy: {
		type: 'ajax',
		url: '/extjs/user',
		reader: {
			type: 'json',
			rootProperty: 'users',
			successProperty: 'success'
		}
	}
});