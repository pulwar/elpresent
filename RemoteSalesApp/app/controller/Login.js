Ext.define('MVC.controller.Login', {
	extend : 'Ext.app.Controller',

	init: function() {
		var controller = this;
		this.control({
			'login button#SaveRecord' : {
				click : this.onSaveButtonClick
			}
		});
	},

	onSaveButtonClick : function(btn) {
		var win = btn.up('window');
		var form = btn.up('form');

		form.submit({
			success: function (form, action) {
				var user = action.result.user;
				win.close();

				var now = new Date();
				var expiry = new Date(now.getTime() + 60 * 60 * 1000);
				Ext.util.Cookies.set('LoggedIn', user, expiry);

				Ext.widget('main').show();
			},
			failure: function (form, action) {
				Ext.MessageBox.alert('Ошибка авторизации. ', action.result.errors.reason);
			}
		});
	}
});