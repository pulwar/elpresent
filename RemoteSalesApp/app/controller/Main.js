Ext.define('MVC.controller.Main', {
	extend: 'Ext.app.Controller',
	init: function () {
		var controller = this;
		var loggedIn = Ext.util.Cookies.get('LoggedIn');
		var loggedIn_obj = JSON.parse(loggedIn);

		if(loggedIn != null){
			Ext.Ajax.request({
				url: '/extjs/score/count',
				params: {
					'user': loggedIn
				},
				success: function(response){
					var data = Ext.decode(response.responseText);
					Ext.ComponentQuery.query('tabpanel #IdScore')[0].setValue('Очков накоплено: ' + data.count);
				}
			});
		}

		this.control({
			'#IdUserSave':{
				click: this.UserSave
			},
			'#ClearScore':{
				click: this.ClearScore
			},
			'#myGiftsViewId': {
				select: this.onGridGiftsSelect
			},
			'#myOrdersViewId': {
				select: this.onGridOrdersSelect
			},
			'#myPickupViewId': {
				select: this.onPickupSelect
			},
			'#view': {
				click: this.viewItem
			},
			'#Save': {
				click: this.SaveRecord
			},
			'#SavePickup': {
				click: this.SavePickupRecord
			},
			'#SaveOrder': {
				click: this.SaveOrderRecord
			},
			'#PickupPrint':{
				click: this.PrintPickupRecord
			},
			'#Excel': {
				click: this.Excel
			},
			'#DeleteOrder': {
				click: this.DeleteOrderRecord
			},
			'#Logout' : {
				click: this.Logout
			},
			'#myUsersViewId': {
				select: this.onUserSelect
			},
			'#DeleteUser': {
				click: this.DeleteUserRecord
			}
		});
	},
	UserSave: function(grid, record) {
		var user = Ext.ComponentQuery.query('tabpanel #IdUser')[0].getValue();
		var password = Ext.ComponentQuery.query('tabpanel #IdPassword')[0].getValue();
		var fio = Ext.ComponentQuery.query('tabpanel #IdFio')[0].getValue();
		var company_name = Ext.ComponentQuery.query('tabpanel #IdCompany_name')[0].getValue();
		var company_address = Ext.ComponentQuery.query('tabpanel #IdCompany_address')[0].getValue();
		var score_hide = Ext.ComponentQuery.query('tabpanel #IdScore_hide')[0].getValue();
		var paid_hide = Ext.ComponentQuery.query('tabpanel #IdPaid_hide')[0].getValue();

		if(user == '' || password == ''){
			Ext.Msg.alert('Ошибка','Не все поля заполнены!');
		}else{
			Ext.Ajax.request({
				url: '/extjs/user/save',
				params: {
					'user': user,
					'password': password,
					'fio': fio,
					'company_name': company_name,
					'company_address': company_address,
					'score_hide': score_hide,
					'paid_hide': paid_hide
				},
				success: function(response){
					var data = Ext.decode(response.responseText);
					if(data.success){
						Ext.Msg.alert('Ура!',user + ' добавлен!');
						Ext.getCmp('myUsersViewId').getStore().reload();
					}
					else{
						Ext.Msg.alert('Ошибка', data.error);
					}
				}
			});
		}
	},
	DeleteUserRecord:function (btn){
		var item_id = Ext.ComponentQuery.query('tabpanel #UserItemSelected')[0].getValue();
		if(item_id == '') {
			Ext.MessageBox.alert('Ошибка', 'Не выбран итем');
		}else{
			Ext.Ajax.request({
				url: '/extjs/user/delete',
				params: {
					'item_id': item_id
				},
				success: function(response){
					var data = Ext.decode(response.responseText);
					if(data.success){
						// перегруэаем стор с юзерами
						Ext.getCmp('myUsersViewId').getStore().reload();
					}
					else{
						Ext.Msg.alert('Ошибка','Не удалось обновить данные');
					}
				}
			});
		}
	},
	viewItem: function (grid, record) {
		var item_id = Ext.ComponentQuery.query('tabpanel #IdItem')[0].getValue();
		if(item_id == '') {
			Ext.MessageBox.alert('Ошибка', 'Выберите подарок, который нужно просмотреть');
		}else{
			Ext.Ajax.request({
				url: '/extjs/gift/view',
				params: {
					'id': item_id
				},
				success: function(response){
					var data = Ext.decode(response.responseText);
					if(data.success){
						var view = Ext.widget('giftview');
						var form = view.down('form').getForm();
						var img = view.down('form').items.first();
						img.setSrc('/' + data.item.preview_img);

						form.setValues({
							'desc': data.item.desc,
							'title': data.item.title,
							'options': data.item.options
						});

					}
					else{
						Ext.Msg.alert('Ошибка','Не удалось обновить данные');
					}
				}
			});
		}
	},
	ClearScore: function(){
		Ext.Ajax.request({
			url: '/extjs/score/clear',
			success: function(response){
				var data = Ext.decode(response.responseText);
				if(data.success){
					Ext.ComponentQuery.query('tabpanel #IdScore')[0].setValue('Очков накоплено: ' + 0);
					Ext.Msg.alert('Ура!','Очки сброшены');
					Ext.getCmp('myOrdersViewId').getStore().reload();
				}
				else{
					Ext.Msg.alert('Ошибка','Не удалось обновить данные');
				}
			}
		});
	},
	onGridGiftsSelect: function (grid, record, index, eOpt) {
		if (typeof record.raw.price !== 'undefined') {
			var arr_price = [];
			var price = record.raw.price;
			arr_price = price.split(',');
			if (arr_price.length > 1){
				price = arr_price[0];
			}
			Ext.ComponentQuery.query('tabpanel #IdSum')[0].setValue(price);
		}
		Ext.ComponentQuery.query('tabpanel #IdItem')[0].setValue(record.id);
	},
	onGridOrdersSelect: function (grid, record, index, eOpt) {
		Ext.ComponentQuery.query('tabpanel #IdItemOrder')[0].setValue(record.id);
	},
	onUserSelect: function (grid, record, index, eOpt) {
		Ext.ComponentQuery.query('tabpanel #UserItemSelected')[0].setValue(record.raw.user);
	},
	onPickupSelect: function (grid, record, index, eOpt){
		Ext.ComponentQuery.query('tabpanel #PickupIdValue')[0].setValue(record.id);
	},
	SaveOrderRecord: function (btn){
		var item_id = Ext.ComponentQuery.query('tabpanel #IdItemOrder')[0].getValue();
		if(item_id == '') {
			Ext.MessageBox.alert('Ошибка', 'Не выбран итем');
		}else{
			window.open('/order/print?id=' + item_id);
		}
	},
	SavePickupRecord: function(btn){
		var id = Ext.ComponentQuery.query('tabpanel #PickupIdValue')[0].getValue();
		if(id == ''){
			Ext.MessageBox.alert('Ошибка', 'Выберите подарок, который регистрируется');
		}else{
			var grid = Ext.getCmp('myPickupViewId');
			var item_id = grid.getSelectionModel().getSelection()[0].get('item_id');
			var order_id = grid.getSelectionModel().getSelection()[0].get('id');
			var sum = grid.getSelectionModel().getSelection()[0].get('price_pickup');
			Ext.Ajax.request({
				url: '/extjs/default/buy',
				params: {
					'item_id': item_id,
					'sum': sum,
					'order_id' : order_id
				},
				success: function(response){
					var data = Ext.decode(response.responseText);
					if(data.success){
						// перегруэаем стор с заказми
						//Ext.getCmp('myOrdersViewId').getStore().reload();
						Ext.MessageBox.show({
							title:'Регистрация покупки',
							buttons: Ext.MessageBox.OK,
							msg: 'Перейти к распечатке?',
							fn: function(btn) {
								if(btn == 'ok'){
									window.open('/order/print?id=' + id + '&site=' + id);
								}
							}
						})
					}
					else{
						Ext.Msg.alert('Ошибка','Не удалось обновить данные');
					}
				}
			});
		}
	},
	DeleteOrderRecord: function (btn){
		var item_id = Ext.ComponentQuery.query('tabpanel #IdItemOrder')[0].getValue();
		if(item_id == '') {
			Ext.MessageBox.alert('Ошибка', 'Не выбран итем');
		}else{
			Ext.Ajax.request({
				url: '/extjs/order/delete',
				params: {
					'item_id': item_id
				},
				success: function(response){
					var data = Ext.decode(response.responseText);
					if(data.success){
						var id = data.id;
						Ext.Ajax.request({
							url: '/extjs/score/count',
							params: {
								'user': Ext.util.Cookies.get('LoggedIn')
							},
							success: function(response){
								var data = Ext.decode(response.responseText);
								Ext.ComponentQuery.query('tabpanel #IdScore')[0].setValue('Очков накоплено: ' + data.count);
							}
						});

						// перегруэаем стор с заказми
						Ext.getCmp('myOrdersViewId').getStore().reload();
					}
					else{
						Ext.Msg.alert('Ошибка','Не удалось обновить данные');
					}
				}
			});
		}
	},
	Excel: function (btn){
		var store = Ext.getCmp('myOrdersViewId').getStore();
		var items = store.data.items;
		var data = [];
		if(items.length > EXCEL_COUNT){
			Ext.Msg.alert('Ошибка','Слишком большой объём данных. Сузьте параметры поиска.');
		}else{
			for(var i = 0; items.length > i; i++){
				data[i] = items[i].raw;
			}
			var dataJson = Ext.encode(data);

			Ext.Ajax.request({
				url: '/extjs/default/excel',
				params: {
					'data_json': dataJson
				},
				success: function(response){
					//var link = Ext.decode(response);
					var data = Ext.decode(response.responseText);
					var link = data.link;
					window.location = '/' + link;
				}
			});
		}
	},

	/**
	 * Заказ
	 * @param btn
	 * @constructor
	 */
	SaveRecord: function (btn) {
	    btn.disable();
		var item_id = Ext.ComponentQuery.query('tabpanel #IdItem')[0].getValue();

		if(item_id == ''){
			Ext.MessageBox.alert('Ошибка', 'Выберите подарок, который регистрируется');
			btn.enable();
		}else{
			var sum = parseFloat(Ext.ComponentQuery.query('tabpanel #IdSum')[0].getValue(), 10);
			var sale = parseFloat(Ext.ComponentQuery.query('tabpanel #sale')[0].getValue(), 10);
			if (sale > 0){
				sum = sum - sum * sale / 100;
			}
			Ext.Ajax.request({
				url: '/extjs/default/buy',
				params: {
					'item_id': item_id,
					'sum': sum
				},
				success: function(response){
					var data = Ext.decode(response.responseText);
					if(data.success){
						var id = data.id;
						Ext.Ajax.request({
							url: '/extjs/score/count',
							params: {
								'user': Ext.util.Cookies.get('LoggedIn')
							},
							success: function(response){
								var data = Ext.decode(response.responseText);
								Ext.ComponentQuery.query('tabpanel #IdScore')[0].setValue('Очков накоплено: ' + data.count);
								//Ext.ComponentQuery.query('tabpanel #sale')[0].setValue(0);
							}
						});

						// перегруэаем стор с заказми
						Ext.getCmp('myOrdersViewId').getStore().reload();
						Ext.MessageBox.show({
							title:'Регистрация покупки',
							buttons: Ext.MessageBox.OK,
							msg: 'Перейти к распечатке?',
							fn: function(btn) {
								if(btn == 'ok'){
									window.open('/order/print?id=' + id);
								}
							}
						})
					}
					else{
						Ext.Msg.alert('Ошибка','Не удалось обновить данные');
					}
				}
			});
			setTimeout(function () {
               btn.enable();
            }, 2000);


		}
	},
	Logout: function(btn) {
		Ext.util.Cookies.clear('LoggedIn');
		Ext.ComponentQuery.query('main')[0].destroy();
		Ext.widget('login').show();
	}
});