/**
 * Срок активации через 3 месяца
 * @type {number}
 */
var IN_3_MONTH = 3600 * 24 * 30 * 3;

/**
 * Срок активации через 6 месяцев
 * @type {number}
 */
var IN_6_MONTH = 3600 * 24 * 30 * 6;

/**
 * Подарки для которых нужно применить 6 месяцев
 * @type {string[]}
 */
var ITEMS_FOR_6_MONTH = [
	'Двухэтапный мастер-класс «Город»',
	'Курс «Штурман: боевое крещение»',
	'Курс «Штурман: раллийный драйв»',
	'Курс начинающих водителей',
	'Урок вождения: маневрирование',
	'Мастер-класс вождения на выбор',
	'Парковочный курс',
	'Правильная парковка',
	'Программа контраварийного вождения',
	'Раллийный день',
	'Раллийный день  для двоих',
	'Раллийный день для компании',
	'Урок контраварийного вождения',
	'Урок контраварийного вождения для двоих',
	'Универсальная подарочная карта'
];

/**
 * Количество подарков на страницу
 * @type {number}
 */
var COUNT_ITEMS_GIFTS = 100;

/**
 * Количество заказов на страницу
 * @type {number}
 */
var COUNT_ITEMS_ORDERS = 100;

var LABEL_BOTTOM_IN_SCORES_GRID = 'Данные по сумме проданных сертификатов и начисленных баллов заносятся в таблицу в программа дилерских продаж. Посмотреть результаты может только сам продавец, зайдя под личным номером/фамилией и наш менеджер.';
var LABEL_TOP1_IN_SCORES_GRID = 'Компания ElPresent.by в рамках дилерского сотрудничества предлагает бонусную программу для продавцов. За каждый проданный сертификат продавец получает баллы, которые потом суммируются и которые можно обменять на любой сертификат в нашем интернет-магазине.';
var LABEL_TOP2_IN_SCORES_GRID = '1 балл = 1000 руб.';
var LABEL_TOP3_IN_SCORES_GRID = 'Ниже приведена таблица баллов и их значение';

var EXCEL_COUNT = COUNT_ITEMS_ORDERS;

var PICKUP_TOP_LABEL = 'Всегда спрашивайте номер заказа у клиента!';