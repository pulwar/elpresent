Refactoring roadmap
=============================

## Database Refactoring

* Split Order into smaller entities

## Service Layer

* Move webpay specific functions from model to service
* Move easypay specific functions from model to service

## PDF Generation