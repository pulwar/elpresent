$(document).ready(function(){
    $('.scale').click(function(){
       var change = false;
       var ids = this.id.split('_');
       var pointer = $('#'+ids[0]+'_pointer');
       $(pointer).appendTo(this);
       /*$('#'+ids[0]+'mood img').attr('src','/upload/moods/'+moods[ids[0]][ids[1]].image);*/
       $('#'+ids[0]+'mood').html(moods[ids[0]][ids[1]].image);
       $('#'+ids[0]+'mood-text').html(moods[ids[0]][ids[1]].mood);
       $('#'+ids[0]+'mood-items').html(moods[ids[0]][ids[1]].items);
       var full = moods[ids[0]].length;
       var separator = full/3;
       if(!change){
           change = true;
           $('#'+ids[0]+'table').find('.mood-text-content1').removeClass('mood-text-content1');
           $('#'+ids[0]+'table').find('.mood-text1').removeClass('mood-text1');
           $('#'+ids[0]+'table').find('.mood-text-bottom1').removeClass('mood-text-bottom1');
           $('#'+ids[0]+'mood-text').css('font-weight','bold');
       }
       if(0 == ids[1]){
           $(this).parent().removeClass('pointer').addClass('pointer-1');
       }else{
           $(this).parent().removeClass('pointer-1').addClass('pointer');
       }
       if(full - separator >= ids[1] && ids[1] > separator) {

           $(pointer).attr('src','/images/moods/pointer-yellow.png');           
       }else if(full - separator > ids[1]) {
           $(pointer).attr('src','/images/moods/pointer-green.png');

       }else{
           $(pointer).attr('src','/images/moods/pointer-red.png');

       }
    });
    $('.scale-pointer').bind('mousedown', function(){
        var ids = this.id.split('_');
        var pointer = this;
        $('.scale'+ids[0]).bind('mouseover',function(){
            $(this).click();
        })
        return false;
    });
    $('.scale-pointer').bind('mouseup', function(){
        var ids = this.id.split('_');
        $('.scale'+ids[0]).unbind('mouseover')
        return false;
    });
})
