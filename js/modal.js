(function ($) {

    $(function () {
        $('[data-toggle=modal]').on('click', function (e) {
            var target = $(this).data('target'),
                $modal = $('#' + target);
            if ($modal.length !== 1) {
                return;
            }

            $modal.removeClass('hide');
            e.stopPropagation();
        });

        $(document).on('click', function () {
            $('.modal-window').addClass('hide');
        });

        $('.modal-window').delegate('click', '[data-dismiss]', function (e) {
            $(this).closest('.modal-window').addClass('hide');
            e.stopPropagation();
        }).on('click', function (e) {
            e.stopPropagation();
        });

        $('[data-submit=want-for-present]').on('submit', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('[type=submit]', this).attr('disabled', 'disabled');
            var errMsg = $(this).closest('.modal-window').find('.error-msg'),
                successMsg = $(this).closest('.modal-window').find('.success-msg'),
                $this = $(this);

            $([errMsg, successMsg]).addClass('hide');

            $.ajax({
                url: $this.attr('action'),
                data: $this.serialize(),
                type: 'POST',
                success: function () {
                    setTimeout(function () {
                        $this.addClass('hide');
                        successMsg.removeClass('hide');
                    }, 250);
                },
                error: function () {
                    setTimeout(function () {
                        errMsg.removeClass('hide');
                    }, 250);
                }
            });

            return false;
        })
    });

}(window.jQuery));