/**
 * Корректный url - alias
 * @param string str
 * @returns string Создает корректный url
 */
function aliasCorrect(str) {
    str = str.toLowerCase();
    var newstr = [];
    var rus = ['Ё', 'Ё', 'Ж', 'Ж', 'Ч', 'Ч', 'Щ', 'Щ', 'Щ', 'Ш', 'Ш', 'Э', 'Э', 'Ю',
        'Ю', 'Я', 'Я', 'ч', 'ш', 'щ', 'э', 'ю', 'я', 'ё', 'ж', 'A', 'Б', 'В', 'Г',
        'Д', 'E', 'З', 'И', 'Й ', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У',
        'Ф', 'Х', 'Ц', 'Ы', 'а', 'б', 'в', 'г', 'д', 'е', 'з', 'и', 'й', 'к', 'л',
        'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х ', 'ц', 'ы', 'Ъ', 'ъ', 'Ь',
        'ь', '-'];
    var eng = ['YO', 'Yo', 'ZH', 'Zh', 'CH', 'Ch', 'SHC', 'SHc', 'Shc', 'SH',
        'Sh', 'YE', 'Ye', 'YU', 'Yu', 'YA', 'Ya', 'ch', 'sh', 'shc', 'ye', 'yu',
        'ya', 'yo', 'zh', 'A', 'B', 'V', 'G', 'D', 'E', 'Z', 'I', 'Y', 'K', 'L',
        'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', 'C', 'X', 'a', 'b', 'v',
        'g', 'd', 'e', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't',
        'u', 'f', 'h', 'c', 'i', '', '', '', '', '-'];

    for (var i = 0; i < str.length; i++) {
        newstr[i] = str[i].replace(/([^a-zA-Z0-9])/, '-');
        for (var j = 0; j < rus.length; j++) {
            if (str[i] == rus[j] || str[i] == eng[j]) {
                newstr[i] = str[i].replace(rus[j], eng[j]);
            }
        }
    }
    var alias = newstr.join('');
    alias = alias.replace(/\-+/, '-');

    return alias;
}

/**
 * Добавление пробелов в больших числах
 * @param string nStr
 * @returns string
 */
function getNamberWithSpace(prixe){
    prixe = prixe + '';
    return prixe.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1,');
}

/**
 * Yii. Добавляет атрибут required к input, исходя если класс required у label
 * @param string contextClass Контекст (css-класс)
 */
function setYiiRequiredInputByLabelRequired(contextClass){

    contextClass = typeof contextClass ==="undefined" ? '' :  contextClass+' ';

    $(contextClass+'form label.required').each(function(index, element) {
        $(element).siblings('input').attr('required', 'required');
    });
}

/**
 * Bootstrap. Запоминание в куках табов
 * @param string cssClass, нарпимер  .nav.nav-tabs (css-класс с точками)
 */
function setBootstrapCoockieTab(cssClass){

    cssClass = typeof contextClass ==="undefined" ? '.nav.nav-tabs ' :  '.'+cssClass+' ';

    $(cssClass + 'a').click(function(e){
        e.preventDefault();
        $.cookie('cookie_tab', $(this).attr('href'));
    })

    var cookie_tab = $.cookie('cookie_tab');

    $(cssClass + "a[href*='"+cookie_tab+"']").tab('show');
}

/**
 * Получить дату по метке времени; формат d.m.Y H:i
 *
 * @param int time Метка времени в секундах
 * @returns {string}
 */
function getDateByUnixtTime(time)
{
	var date = new Date(time * 1000);
	var d = date.getDate() >= 10 ? date.getDate() : '0'+ date.getDate();
	var m = date.getMonth() > 8 ? (date.getMonth() + 1) : '0' + (date.getMonth() + 1);
	var h = date.getHours() >= 10 ? date.getHours() : '0' + date.getHours();
	var i = date.getMinutes() >= 10 ? date.getMinutes() : '0' + date.getMinutes();
	return d  + '.' + m + '.' + date.getFullYear() + ' ' + h + ':' + i;
}

/**
 * Есть ли элемент в массиве
 *
 * @param needle
 * @param haystack
 * @param strict
 * @returns {boolean}
 */
function in_array(needle, haystack, strict)
{
	var found = false, key, strict = !!strict;
	for (key in haystack) {
		if ((strict && haystack[key] === needle) || (!strict && haystack[key] == needle)) {
			found = true;
			break;
		}
	}
	return found;
}
