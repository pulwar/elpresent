$(function () {
    $('.reviews-slider').slick({
        slidesToShow: 1,
        infinite: true,
        fade: true,
        dots: true
    });

    function changeWidth() {
        let viewportWidth = $(window).width();
        if (viewportWidth > 1100) {
            $('.clients-wrap').filter('.slick-initialized').slick('unslick');
            $('.catalog-slider').filter('.slick-initialized').slick('unslick');
        }
        if (viewportWidth <= 1100) {
            $('.clients-wrap').not('.slick-initialized').slick({
                arrows: false,
                centerMode: true,
                infinite: false,
                variableWidth: true,
                initialSlide: 3,
                slidesToShow: 1
            });
            $('.catalog-slider').slick({
                initialSlide: 1,
                slidesToShow: 1,
                arrows: false,
                infinite: false,
                dots: true
            });
        }
        if (viewportWidth > 767) {
            $('.advantage-slider').filter('.slick-initialized').slick('unslick');
        }
        if (viewportWidth <= 767) {
            $('.advantage-slider').not('.slick-initialized').slick({
                arrows: false,
                dots: true,
                infinite: false,
                slidesToShow: 1
            });
        }
    };

    $(window).on('load resize', changeWidth);

    $('.nav-mob').on('click', function(e){
        $('.nav-landing-mob').stop().stop().toggleClass('open');
        $('.menu-wrap').stop().stop().toggleClass('open');
    });

    $(document).click(function(event) {
        let $target = $(event.target);
        let viewportWidth = $(window).width();
        if(viewportWidth < 1201 && !$target.closest('.nav-mob').length && $('.nav-landing-mob').is(":visible")) {
            $('.nav-landing-mob').removeClass('open');
            $('.menu-wrap').removeClass('open');
        }
    });


    $('.close').on('click', function(e){
        $('.popup-wrap').hide();
    });

});