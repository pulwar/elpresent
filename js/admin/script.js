$(document).ready(function() {
    //setYiiRequiredInputByLabelRequired();
    // запомниание в куках табов
    setBootstrapCoockieTab('div.option .nav.nav-tabs');

    // init datepicker
    $('#div-datepicker').datetimepicker();

    // init start date block
    show_hide_datepicker();

    // on change select
    $('body').on('change', 'select#kind_validity', function(){
        show_hide_datepicker();
    });
});

function show_hide_datepicker() {
    if($('select#kind_validity').val()==0) {
        $('div.start_season_div').hide();
    } else {
        $('div.start_season_div').show();
    }
}