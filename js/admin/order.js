$(function(){
    $(document).ready(function(){
        $('.items div.item').first().click();

        // изменение значения селекта
        $('body').on('change', 'select.active_impression', function(){
            var option_value = $(this).find(':selected').attr('data-option-id');
            if($('select.option_impression').length) {
                $('select.option_impression').val(option_value);
            }
        })
    });

    var createViewUrl = function(id) {
        return '/admin/order/view?id=' + id;
    };

    $(document)
        .on('click', '.save', function() {
            if($(this).hasClass('disabled')) {
                return false;
            }
            var form = $(this).closest('form');
            $(this).addClass('disabled');
            var self = $(this);
            $.ajax({
                url: form.attr('action'),
                type: 'POST',
                dataType: 'json',
                data: $(form).serialize(),
                success: function(response) {
                    if(!response.success) {
                        alert('Произошла ошибка, проверьте введенные данные.');
                    }
                },
                complete: function() {
                    self.removeClass('disabled');
                    console.log('test');
                    getOrder(self.data('id'));
                },
                error: function(){
                    alert('Что то пошло не так!');
                }
            });
        }).on('click', '.nav-tabs a',  function (e) {
            e.preventDefault();
            $(this).tab('show');
        }).on('click', '.order-list .item', function(){
            console.log('dfadsfasd');
            if ($(this).hasClass('active'))
                return false;
            // disable active element
            $('.order-list .item').not(this).removeClass('active');
            $(this).addClass('active');
            // get data
            getOrder($(this).data('id'));
        });

    var getOrder = function(id) {
        var container = $('.order-info-wrap');
        $.ajax({
            'url': createViewUrl(id),
            'dataType': 'html',
            'beforeSend': function (data) {
                $('*', $('.order-info',container)).hide();
                $('.ajaxLoader', container).show();
            },
            'success': function(result){
                $('*', $('.order-info',container)).remove();
                $('.ajaxLoader', container).hide();
                $('.order-info',container).append(result);
                $('.datepicker', container).bdatepicker({});
            },
            error: function(){
                alert('Что то пошло не так!');
            }
        });
    };
});