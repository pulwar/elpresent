(function($){
	$(function(){
		var self = this;
		var timeOut;
		
		var searchValue;
		var emptyTemplate = '<input name="Items[hint][]" type="text" placeholder="Введите заголовок" />';


        var uniqid = function () {
            var text = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

            for (var i = 0; i < 13; i++)
                text += possible.charAt(Math.floor(Math.random() * possible.length));

            return text;
        };


        var getDefaultCityIndex  = function (cityList) {
            var defaultIndex = 0;
            for(var i=0; i < cityList.length; i++) {
                if(cityList[i].default) {
                    defaultIndex = i;
                    break;
                }
            }

            return defaultIndex;
        };

        $(document).on('change', 'select.city-select', function() {
            var selectedCity = $(this).val();
        	var itemWrapEl = $(this).parents('.item-wrap');
        	var jsonPrices = $(itemWrapEl).attr('data-prices');
        	var prices = JSON.parse(jsonPrices);
        	var cityPrices = prices[selectedCity];
            var priceSelect = renderPrices(cityPrices, $(itemWrapEl).find('.item-id').attr('data-uniq'));
            $(itemWrapEl).find('.price-wrap').html(priceSelect);
		});

		var renderCities = function (cityList, uniqidStr) {
            var defaultCityIndex = getDefaultCityIndex(cityList);
			var output = '<select class="city-select" name="Items['+uniqidStr+'][city]" '+(cityList.length === 1 ? 'disabled' : '')+'>';
			for (var i=0; i<cityList.length; i++) {
				var selected = i === defaultCityIndex?'selected="selected"':'';
                output += '<option ' +selected +' value="'+cityList[i].id+'">'
					+cityList[i].title
					+'</option>';
			}
            output += '</select>';
			if (cityList.length === 1) {
                output += '<input type="hidden" name="Items['+uniqidStr+'][city]" value="'+cityList[defaultCityIndex].id+'">';
			}
			return output;
		};

        /**
         * @param cityList
         * @param prices
         * @returns array
         */
		var getPrices = function (cityList, prices) {
            var defaultCityIndex = getDefaultCityIndex(cityList);
            var defaultCityId = cityList[defaultCityIndex].id;
            var cityPrices = prices[defaultCityId];
            if (!(cityPrices instanceof Array) && !(cityPrices instanceof Object)) {
                var cityPrices = JSON.parse("[\"" + cityPrices + "\"]");
            }

            return cityPrices;
		};

		var renderPrices = function (pricesList, uniqidStr) {
            var output = '<select '  + 'name="Items['+uniqidStr+'][option]"' +(pricesList=== undefined || pricesList.length === 1 ? 'disabled' : '')+'>';
			var counter = 0;
            var firstKey = 0;
            for(var key in pricesList) {
            	var selected = counter === 0?'selected="selected"':'';
            	var firstKey = counter++ === 0?key:firstKey;
                output += '<option ' + selected +' value="'+key+'">'
							+pricesList[key]
							+'</option>';
            }
            output += '</select>';
            if (pricesList=== undefined || pricesList.length === 1) {
                output += '<input type="hidden" name="Items['+uniqidStr+'][option]" value="'+firstKey+'">';
            }

            return output;
		};

		var getBlock = function( id, container ) {
			$.ajax({
				"url": "/admin/superitem/getItemThumb",
				"type": "post",
				"data": { "id": id },
				"dataType": "json",
				"success": function(response) {
                    var jsonPrices = JSON.stringify(response.allPrices);
                    var jsonCities = JSON.stringify(response.cities);
                    var uniqidStr = uniqid();
					var template =
						'<div class="item-wrap" data-prices=\''+jsonPrices+'\' data-cities=\''+jsonCities+'\'>'
							+"<div class=\"span2\"><img src=\"/{preview_img}\" alt=\"\" /></div>"
							+ "<div class=\"span3\">"
							+ "<strong>{title}</strong>"
							+ '<input data-uniq="'+uniqidStr+'" class="item-id" type="hidden" value="{id}" name="Items['+uniqidStr+'][id]" />'
							+ '<div class="city-wrap">'+renderCities(response.cities, uniqidStr) + '</div>'
							+ '<div class="price-wrap">'+renderPrices(getPrices(response.cities, response.allPrices), uniqidStr) + '</div>'
							+ '<a href="#" class="replace">[Убрать]</a>'
							+ '</div>'
						+'</div>'
					;

					for(var key in response) {
						var searchPattern = '{' + key + '}';
						template = template.replace(searchPattern, response[key]);
					}
					
					template = $(template);
					
					$(container).html(template);
				}
			});
		};
		
		var doSearch = function( value, container ) {
			
			$.ajax({
				"url": "/admin/superitem/search",
				"dataType": "json",
				"type": "post",
				"data": { "query": value },
				"success": function( answer ) {
					
					var newHintList;
					$('.hint-list').remove();
					
					if( answer.length > 0 ) {
						newHintList = $('<ul class="hint-list"/>');
						for( var key in answer ) {

							var item = $('<li/>').text(answer[key].title).data('item-id', answer[key].id);
							
							item.bind('click', function(e) {
								getBlock( $(this).data('item-id'), container );
								$(this).parents('ul').remove();
							});
							newHintList.append( item );
						}
						
						$(container).append(newHintList);
					}
					
					
				}
			});
			
		}
		
		$('.items-list input[type=text]').live('keyup', function() {
			clearTimeout(timeOut);
			
			searchValue = this.value;
			var container = $(this).parent();
			if( searchValue && searchValue.length > 2 ) {
				timeOut = setTimeout( function() {
					doSearch( searchValue, container );
				}, 250);
			}
			else 
				$('.hint-list').remove();

		}).bind('blur', function(){
			$(this).removeClass('correct empty');
			
			if( $(this).parent().find('input[type=hidden]').val() == "")
				$(this).addClass('correct');
			else
				$(this).addClass('empty');
		});
		
		$('.items-list .item a.replace').live('click', function(){			
			$(this).parent().html(emptyTemplate);
			return false;
		});
		
		$('form').bind('submit', function(){
			
			var empty = true;
			
			$('.items-list input[type=hidden]').each(function(e, el) {
				empty = !(!empty || el.value != "");
			});
			
			if( empty )
				alert('Выберите хотя бы одно впечатление');
				
			return !empty;
		});
		
	});
})(jQuery);