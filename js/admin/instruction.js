(function (window, $, storage, showdown) {


    var sharedDataPrefix,
        converter = showdown ? new showdown.converter() : undefined,
        processors = {
            basic: contentProcessor,
            markdown: markdownProcessor,
            attr: attrProcessor
        },
        segmentsMap = {};

    $(function () {
        $('[data-shared]').each(initSharedData);
    })

    function initSharedData() {
        // get type and prefix
        var prefix = $(this).data('shared'),
            type = $(this).data('shared-type')
        // if we have no prefix, so do not handle it
        if (!prefix || !type) {
            return;
        }
        sharedDataPrefix = prefix;

        // bind listeners

        if ('source' === type) {
            $('[data-shared-source]', this).on('change', applyNewData).trigger('change');
        } else {
            $('[data-shared-bind]', this).each(function () {
                var name = $(this).data('shared-bind');
                if (!segmentsMap.hasOwnProperty(name)) {
                    segmentsMap[name] = $([this]);
                } else {
                    segmentsMap[name] = segmentsMap[name].add(this);
                    console.log('segmentsMap[name]', segmentsMap[name]);
                }
                applyData(name, storage[prefix + '_' + name]);
            });
            // bind changes notifier
            window.addEventListener('storage', dataBinder, true)
        }
    }

    function dataBinder(e) {
        // check prefix
        if (0 !== e.key.indexOf(sharedDataPrefix)) {
            return;
        }

        var name = e.key.substring(sharedDataPrefix.length + 1);
        console.log('bind', name, e);
        applyData(name, e.newValue);
    }

    function applyData(name, value) {
        if (segmentsMap.hasOwnProperty(name)) {
            // check is there any processing needed
            var processor = segmentsMap[name].data('shared-bind-processor');
            console.log(name, processor);
            if (!processors.hasOwnProperty(processor)) {
                processor = 'basic';
            }
            processors[processor](value, segmentsMap[name]);
        }
    }

    function applyNewData() {
        var name = $(this).data('shared-source'),
            attr = $(this).data('shared-attr') || 'value',
            useAttr = !!attr;
        storage.setItem(sharedDataPrefix + '_' + name, this[attr])
    }

    function contentProcessor (value, $el) {
        $el.html(value);
    }

    function markdownProcessor(value, $el) {
        var html = value;
        if (converter) {
            html = converter.makeHtml(value);
        }
        $el.html(html);
    }

    function attrProcessor(value, $el) {
        var attr = $el.data('shared-attr');
        console.log('process attr', value, attr);
        $el.attr(attr, value);
    }

    function clearForPrefix(prefix) {
        for(var i in storage) {
            if (!storage.hasOwnProperty(i)) continue;
            if (0 === i.indexOf(prefix)) {
                storage.removeItem(i);
            }
        }
    }

}(window, window.jQuery, window.localStorage, window.Showdown))