/**
 * goper 25 июля 2014
 * Инициализация плагинов и скрипты
 */

/**
 * Plugins Init
 */
$(document).ready(function() {
    // ротатор отзывов в шапке сайта
    $('.list_carousel ul#foo').carouFredSel({
        prev: '#prev',
        next: '#next',
        pagination: false,
        auto: false,
        responsive: true,
        direction: 'left'
    });
    // внутренний слайдер
    $('.widget.sliderins .bxslider').bxSlider({
        auto: true,
        pause: 5000
    });
});


/**
 * Scripts
 */
$(document).ready(function() {

    /**
     * Ротатор баннеров в самом верху
     */
    if( $('.widget.slider ul li').size() > 1 ){
        theRotator();
    }

    /**
     *  Управление подарками с опциями
     */

    // если нажата кнопка Купить - всегда обнуляем корзину
    $('body').on('click', 'a.buy-button', function(e){
        $.cookie('kit_ids', null, { path: '/' });
    });
    $('body').on('click', '.catalog-item .el-btn-bottom a', function(e){
        $.cookie('kit_ids', null, { path: '/' });
    });
    // выбираем чекбокс если есть в куках
    var coockie_item = JSON.parse($.cookie('option_by_city_value'));
    //console.log(coockie_item);
    if (typeof coockie_item !== "undefined" && coockie_item != null) {
        $('.price_choose td.option-'+coockie_item.val+' input').click();
    }
    else{
        $('.catalog-item .price_choose input:first').click();
    }

    $('.price_choose input').click(function(){

        $(this).parents('.option-hover').hide().addClass('no-show');

        var val = $(this).val();
        var id = $(this).attr('id');

        var coockie = {
            'id' : id,
            'val' : val
        }
        //console.log(JSON.stringify(coockie));
        $.cookie('option_by_city_value', JSON.stringify(coockie), { path: '/' });
        $('.catalog-item .price').text($(this).parent().siblings(".td-third").text());

        // если существует корзина, обновляем ее
        if( $('#cart').length > 0 ){
            if(JSON.parse($.cookie('kit_ids')) != null){
                var coockie_items = JSON.parse($.cookie('kit_ids'));

                var myData = {
                    kits: JSON.stringify(coockie_items)
                }
                ajaxCartReload("http://" + document.domain +"/catalog/cartadd", myData);
            }
        }
    });


    /**
     * Управление впечатлениями и корзиной
     */

    // $('.gift.list .price_choose input').click(function(){
    //     $(this).parents('.item').find('a.add-to-kit-cart.enable').click();
    // });

    // $('body').on('click', 'a.add-to-kit-cart.disable', function(e){
    //     $(this).parents('.item').find('.option-hover').show();
    //     $(this).removeClass('disable').addClass('enable');
    // });

    var coockie_items = [];

    // $('body').on('click', 'a.add-to-kit-cart.enable', function(e){
    //
    //     $(this).parents('.item').find('.option-hover').hide();
    //
    //     var id = $(this).data('id');
    //
    //     if(JSON.parse($.cookie('kit_ids')) != null){
    //         coockie_items = JSON.parse($.cookie('kit_ids'));
    //     }
    //     coockie_items.push(id);
    //
    //     coockie_items =  coockie_items.filter(function (value, index, self) {
    //         return self.indexOf(value) === index;
    //     });
    //
    //     var myData = {
    //         kits: JSON.stringify(coockie_items)
    //     }
    //     ajaxCartReload("http://" + document.domain +"/catalog/cartadd", myData);
    //
    //     $.cookie('kit_ids', JSON.stringify(coockie_items), { path: '/' });
    //
    //     //console.log(JSON.parse($.cookie('kit_ids')));
    // });

    $('body').on('click', '#cart .cart-delete-item', function(e){
        var myData = {
            item_from_del_id: $(this).data('item_from_del_id')
        }
        ajaxCartReload("http://"+document.domain +"/catalog/cartdel", myData);
    });

});

/**
 * Обновление корзины
 * @param url
 * @param myData
 */
function ajaxCartReload(url, myData){
    $.ajax({
        type: 'POST',
        data: myData,
        cache: false,
        url: url,
        success: function (data) {
            var response = jQuery.parseJSON (data);
            console.log(data);
            $('#cart').html(response.cart);
        }
    });
}

/**
 * Ротация слайдера
 */
function theRotator() {
    $('.widget.slider ul li').css({opacity: 0.0});
    $('.widget.slider ul li:first').css({opacity: 1.0});
    setInterval('rotate()',5000);
}
function rotate() {
    var current = ($('.widget.slider ul li.show') ?  $('.widget.slider ul li.show') : $('.widget.slider ul li:first'));
    var next = ((current.next().length) ? ((current.next().hasClass('show')) ? $('.widget.slider ul li:first') :current.next()) : $('.widget.slider ul li:first'));
    next.css({opacity: 0.0}).addClass('show').animate({opacity: 1.0}, 1000);
    current.animate({opacity: 0.0}, 1000).removeClass('show');
};

/**
 * Кнопка наверх
 */
$("#back-top").hide();
$(window).scroll(function() {
    if ($(this).scrollTop() > 50) {
        $('#back-top').fadeIn();
    } else {
        $('#back-top').fadeOut();
    }
});
$('#back-top a').click(function() {
    $('body,html').animate({
        scrollTop: 0
    }, 800);
    return false;
});


