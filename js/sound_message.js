$(document).ready(function() {
	$('#orderInfo a.close').click(function(){
		$(this).parent().hide();
	});
});
function checkOrder() {
	$('#orderInfo').hide();
	$.ajax({
		type: 'POST',
		cache: false,
		url: '/admin/order/check',
		success: function (data) {
			var txt = '';
			var response = $.parseJSON(data);
			if(response.status){
				if($.cookie('str_ids_hash') == null || $.cookie('str_ids_hash') != response.str_ids_hash){
					var audio = new Audio("/upload/an.mp3");
					audio.play();
					var ids = response.ids;
					ids.forEach(function(item){
						txt = txt + '<p>Новый заказ №: ' + item + '</p>';
					});
					$('#orderInfo').show();
					$('#orderInfo section').html(txt);

					$.cookie('str_ids_hash', response.str_ids_hash, { path: '/' });
				}
			}
		}
	});
}
checkOrder();
setInterval(checkOrder, 60000);