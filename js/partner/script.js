jQuery(document).ready(function() {
    $('#partnerInfo a.close').click(function(){
        $(this).parent().parent().hide();
    });

    function resetForm() {
        $('.activationDateLabel').hide();
        $('#activateDate').hide();
        $('form#formPartnerCheckCert #activateCert').attr('disabled', true);
        $('form#formPartnerCheckCert #checkCert').attr('disabled', false);
    }

    function parseQueryString( queryString ) {
        var params = {}, queries, temp, i, l;
        // Split into key/value pairs
        queries = queryString.split("&");
        // Convert the array of strings into an object
        for ( i = 0, l = queries.length; i < l; i++ ) {
            temp = queries[i].split('=');
            params[temp[0]] = temp[1];
        }
        return params;
    }


    function getUrl(uri) {
        var search = window.location.search;
        if ('?' === search.substring(0, 1)) {
            uri += search.substring(1).length > 0 ? '?'+search.substring(1) : '';
        }

        return uri;
    }

    function resetGrid() {
        $.ajax({
            type: 'POST',
            url: getUrl('/partner/certificate/resetGrid'),
            // data: {code: codeField.val(), activateDate: $('input[name=activateDate]').val()},
            beforeSend: function() {

            },
            success: function (data) {
                $('.order-list').html(data);
            },
            error: function () {
                showMessage('Произошла ошибка сервера! Таблица не обновлена.');
            }
        });
    }

    function getQueryString()
    {
        var search = window.location.search;
        if ('?' === search.substring(0, 1) && search.substring(1).length > 0) {
            return decodeURIComponent(search.substring(1));
        }

        return '';
    }

    /**
     * @returns {Date}
     */
    function getStartDate()
    {
        var now = new Date();
        now = new Date(now.setDate(1));
        now = new Date(now.setHours(0,0,0,0));
        var date = new Date(now);

        var query = getQueryString();
        if ('' !== query) {
            var parseQuery = parseQueryString(query);
            if ('' !== parseQuery['filter[date][from]']) {
                return new Date(parseQuery['filter[date][from]']);
            }
        }

        return date;
    }

    /**
     * @returns {Date}
     */
    function getEndDate()
    {
        var date = new Date();

        var query = getQueryString();
        if ('' !== query) {
            var parseQuery = parseQueryString(query);
            if ('' !== parseQuery['filter[date][to]']) {
                return new Date(parseQuery['filter[date][to]']);
            }
        }

        return date;
    }

    /**
     * Returns true if it goes beyond the interval
     * @param date
     * @returns {boolean}
     */
    function checkFilterInterval(date)
    {
        return date.getTime() < getStartDate().getTime() || getEndDate().getTime() < date.getTime();
    }

    /**
     * @returns {string}
     */
    function getProcessedFilterPeriod(activateDate)
    {
        var aDate = new Date(activateDate);
        var startDate = new Date(activateDate);
        startDate = new Date(startDate.setDate(1));
        startDate = new Date(startDate.setHours(0,0,0,0));
        startDate = new Date(startDate);

        var endDate = new Date(aDate.getFullYear(), aDate.getMonth() + 1, aDate.getDate()+1);
        if (endDate.getTime() > (new Date()).getTime()) {
            endDate  = new Date();
            endDate = new Date(endDate.setHours(0,0,0,0));
            endDate = new Date(endDate.setDate(endDate.getDate()+1));
        }

        return formateDate(startDate) + ' - ' + formateDate(endDate);
    }

    /**
     *
     * @param {Date} date
     * @returns {string}
     */
    function formateDate(date)
    {
        return date.getFullYear() + '.' + formateDateNumber(date.getMonth() + 1) + '.' + formateDateNumber(date.getDate());
    }

    function formateDateNumber(number)
    {
        return ('0' + number).slice(-2);
    }

    function restrictionInputCode(elem) {
        var elemVal =  elem.val().toString();
        elemVal = elemVal.replace(/[^0-9]/g, '');
        if(elemVal.length > 12) {
            elemVal = elemVal.substr(0, 11);
        }
        elem.val(elemVal);
    }

    $('form#formPartnerCheckCert #codeCert').keyup(function(e) {
        restrictionInputCode($(this));
        resetForm();
    });
    $('form#formPartnerCheckCert #codeCert').change(function() {
        restrictionInputCode($(this));
        resetForm();
    });

    function showMessage(msg, title) {
        var audio = new Audio("/upload/an.mp3");
        audio.play();
        $('#partnerInfo').show();
        if(title){
            $('#partnerInfo span.label').addClass('success');
        } else {
            $('#partnerInfo span.label').addClass('error');
        }
        $('#partnerInfo section').html(msg);
    }
    $('body').on('click', 'form#formPartnerCheckCert #activateCert', function(e) {
        e.preventDefault();
        var thisButton = $(this);
        var codeField = $('form#formPartnerCheckCert input[name=cert_code]');
        var codeVal = codeField.val();
        var activatedItem = $('select#activation_popup_activated_item').val();

        if(codeVal.length != 0 && thisButton.attr('disabled') != 'disabled') {
            var activateDate = $('input[name=activateDate]').val();
            $.ajax({
                type: 'POST',
                url: '/partner/certificate/activate/',
                data: {
                    code: codeVal,
                    activateDate: activateDate,
                    activated_item: activatedItem
                },
                success: function (data) {
                    var jsonData = JSON.parse(data);
                    switch (jsonData.code) {
                        case 200:
                            showMessage(jsonData.message, true);

                            $('#codeCert').trigger('change');
                            $( "#checkCert" ).trigger("click");
                            if (jsonData.status) {
                                if (checkFilterInterval(new Date(activateDate))) {
                                    $('#activateInfo #certNum').html(codeVal);
                                    $('#activateInfo #certActivDate').html(activateDate);
                                    $('#activateInfo #proposedPeriod').html('('+getProcessedFilterPeriod(activateDate)+')');
                                    $('#activateInfo').show();

                                }
                            }
                            resetGrid();

                            break;
                        default:
                            showMessage(jsonData.message, false);
                            resetForm();

                    }
                },
                error: function () {
                    resetForm();
                    var message = 'Произошла ошибка сервера!';
                    var content = '<div class="tab-content"><div style="text-align: center; font-weight: bold;">'+message+'</div></div>';
                    $('.certificate-info').html(content);
                }
            });
        }
    });

    $(document).on('click', 'form#formPartnerCheckCert #checkCert', function(e) {
        e.preventDefault();
        $('.activationDateLabel').hide();
        $('#activateDate').hide();
        var thisButton = $(this);
        var codeField = $('form#formPartnerCheckCert input[name=cert_code]');
        if (codeField.val().length != 0 && thisButton.attr('disabled') != 'disabled') {
            thisButton.attr('disabled', true);
            codeField.removeClass('error');
            $.ajax({
                type: 'GET',
                url: '/partner/certificate/check/',
                data: {code: codeField.val()},
                success: function (data) {
                    var jsonData = JSON.parse(data);
                    //console.log(Object.keys(jsonData.items).length)
                    switch (jsonData.code) {
                        case 200:
                            $('.certificate-info').html(jsonData.message);
                            $('form#formPartnerCheckCert #activateCert').attr('disabled', false);
                            $('.activationDateLabel').show();
                            //
                            if(Object.keys(jsonData.items).length > 0 ) {
                                $('.selectActiveImpression select').html('');
                                $.each(jsonData.items, function(key, value) {
                                     $('.selectActiveImpression select')
                                         .append($("<option></option>")
                                                    .attr("value",key)
                                                    .text(value));
                                });
                                $('.selectActiveImpression').show();
                                $(".selectActiveImpression select").chosen({
                                    width: "276px",
                                    disable_search: true
                                });
                            }
                            $('#activateDate').show();
                            break;
                        case 404:
                            $('.certificate-info').html(jsonData.message);
                            thisButton.attr('disabled', false);
                            $('.activationDateLabel').hide();
                            $('.selectActiveImpression').hide();
                            $('#activateDate').hide();
                            $("selectActiveImpression select").chosen("destroy");
                            break;
                    }
                },
                error: function () {
                    resetForm();
                    var message = 'Произошла ошибка сервера!';
                    var content = '<div class="tab-content"><div style="text-align: center; font-weight: bold;">'+message+'</div></div>';
                    $('.certificate-info').html(content);
                }
            });
        } else {
            codeField.addClass('error');
        }
    });

    $('#fromDate').datetimepicker({
        pickTime: false,
        language: 'ru',
        autoclose: true,
    }).on("show", ()=> {
        $('#fromDate').addClass('active');
    }).on('hide', ()=> {
        $('#fromDate').removeClass('active');
    });
    $('#toDate').datetimepicker({
        pickTime: false,
        language: 'ru',
        autoclose: true,
    }).on("show", ()=> {
        $('#toDate').addClass('active');
    }).on('hide', ()=> {
        $('#toDate').removeClass('active');
    });
    var now = new Date();
    now.setDate(now.getDate() - 4);
    $('#activateDate').datetimepicker(
        {
            startDate: now,
            endDate:  new Date(),
            pickTime: false,
            language: 'ru',
            autoclose: true,
        }
    ).on("show", ()=> {
        $('#activateDate').addClass('active');
    }).on('hide', ()=> {
        $('#activateDate').removeClass('active');
    });

    $('.user-info').on('click', function(e){
        $('.user-menu').stop().stop().slideToggle();
        $(this).stop().stop().toggleClass('active');
    });


    $(document).click(function(event) {
        var $target = $(event.target);
        if(!$target.closest('.user-menu').length && !$target.closest('.user-info').length && $('.user-menu').is(":visible")) {
            $('.user-menu').slideUp();
            $('.user-info').removeClass('active');
        }
    });

    $('.nav-medium').on('click', function(e){
        $('.header-nav-mob').stop().stop().slideToggle();
    });

    $('.nav-close').on('click', function(e){
        $('.header-nav-mob').slideUp();
    });

    $(document).click(function(event) {
        var $target = $(event.target);
        if(!$target.closest('.header-nav-mob').length && !$target.closest('.nav-medium').length && $('.header-nav-mob').is(":visible")) {
            $('.header-nav-mob').slideUp();
        }
    });

    $('.certificate-info').on('click', function(e){
        if (e.target.className === 'no-found-icon') {
            $('.certificate-info .no-found-note').stop().stop().show();
            e.target.classList.add('active');
        }
    });

    $('.certificate-info').on('click', function(e){
        if (e.target.className === 'no-found-close') {
            $('.no-found-note').hide();
            $('.no-found-icon').removeClass('active');
        }
    });

    $(document).click(function(event) {
        var $target = $(event.target);
        if(!$target.closest('.no-found-note').length && !$target.closest('.no-found-icon').length && $('.no-found-note').is(":visible")) {
            $('.no-found-note').hide();
            $('.no-found-icon').removeClass('active');
        }
    });

    // tab
    let $tabItem = $('.tab-item');
    let $tabContent = $('.tab-content');
    let selectedStyle = 'selected';

    (function () {
        let ind = 0;
        $tabItem.each(function () {
            if ($(this).hasClass(selectedStyle)) {
                ind = $(this).index();
            }
        });
        $tabContent.hide();
        $tabContent.eq(ind).show();
    })();

    $tabItem.click(function () {
        let _ = $(this);
        let ind = _.index();

        if (!_.hasClass(selectedStyle)) {
            $tabItem.removeClass(selectedStyle);
            _.addClass(selectedStyle);
            $tabContent.hide();
            $tabContent.eq(ind).show();
        }
    });

    $(document).change(function(event) {
        if(event.target.tagName === 'SELECT') {
            if(event.target.value === '0') {
                event.target.classList.remove("selectNotEmptyField");
            } else {
                event.target.classList.add('selectNotEmptyField');
            }
        }
    });

});