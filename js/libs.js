(function($) {
	var maskList = null;
	var getCodes = function(callback){
		if(maskList)
			callback();
		else
			$.ajax({url:"/templates/qz/phonemask/phone-codes.json",
				dataType:"json",
				success:function(t){
					maskList = $.masksSort(t, ['#'], /[0-9]|#/, "mask");
					callback();
				}
			});
	};

	$.fn.phonemask = function()
	{
		if(!this.length)
			return;
		var elem = this;

		getCodes(function(){
			elem.inputmasks({
				inputmask: {
					definitions: {
						'#': {
							validator: "[0-9]",
							cardinality: 1
						}
					},
					//clearIncomplete: true,
					showMaskOnHover: false,
					autoUnmask: false
				},
				match: /[0-9]/,
				replace: '#',
				list: maskList,
				listKey: "mask"
			});

			elem.focus(function(){
				if(!this.value)
					$(this).inputmask("setvalue", '+375 (');
			});
		});
	}
}(jQuery));