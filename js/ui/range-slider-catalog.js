    $(document).ready(function () {
        function formatNumber(num, offset){
            nstr = num.toString();
            nstr = nstr.replace(/[\s]+/g, '');
            nstr = nstr.replace(/[^0-9]/g, '');
            return nstr; // TODO
            ostr = '';
            for (i = (nstr.length - 1), j = offset; i >= 0; i--, j++)
            {
                if ((j % 3 == 0) && (j > 0))
                    ostr = ' ' + ostr;
                ostr = nstr.substr(i, 1) + ostr;
            }
            return ostr;
        }

        jQuery("input#minCost").val(formatNumber(jQuery("input#minCost").val(), 0));
        jQuery("input#maxCost").val(formatNumber(jQuery("input#maxCost").val(), 0));

        jQuery("#slider-range").slider({
            min: 0,
            max: 2000,
            step: 5,
            values: [minRangeSliderVal, maxRangeSliderVal],
            range: true,

            slide: function (event, ui) {
                jQuery("input#minCost").val(formatNumber(jQuery("#slider-range").slider("values", 0), 0));
                jQuery("input#maxCost").val(formatNumber(jQuery("#slider-range").slider("values", 1), 0));
            },
            change: function (event, ui) {
                jQuery("input#minCost").change();
                jQuery("input#maxCost").change();
            }
        });

        function inInt(number){
            number=number.replace(/[\s]+/g, '');
            number=number.replace(/[^0-9]/g, '');
            number = parseInt(number, 10);
            return number;
        }

        jQuery("input#minCost").keyup(function (eventObject) {
            var e = eventObject.which;
            if(e == 38 || e ==37 || e ==39 || e ==40 || e ==46 || e ==8){
                return;
            }
            jQuery("input#minCost").val(formatNumber(inInt(jQuery("input#minCost").val()), 0));
            jQuery("#slider-range").slider({
                values: [inInt(jQuery("input#minCost").val()), inInt(jQuery("input#maxCost").val())]
            });
        });

        jQuery("input#maxCost").keyup(function (eventObject) {
            var e = eventObject.which;
            if (e == 38 || e == 37 || e == 39 || e == 40 || e == 46 || e == 8) {
                return;
            }
            jQuery("input#maxCost").val(formatNumber(inInt(jQuery("input#maxCost").val()), 0));
            if(inInt(jQuery("input#maxCost").val())>50000000){
                jQuery("input#maxCost").val(formatNumber('50000000', 0));
            }
            jQuery("#slider-range").slider({
                values: [inInt(jQuery("input#minCost").val()), inInt(jQuery("input#maxCost").val())]
            });
        });

        $('input#maxCost').focusout(function(){
            jQuery(this).val(formatNumber(jQuery(this).val(), 0));

        });


        $('input#minCost').focusout(function(){
            jQuery(this).val(formatNumber(jQuery(this).val(), 0));

        });
    });