(function ($, undefined) {



    function Rotator(container) {
        this.$c = container;
        this.current = 0;
        this.slides = $('.slide', container).detach();
        this.length = this.slides.length;
        this.duration = parseInt(container.data('slide-rotator-duration')) * 1000;
        this.interval = undefined;
        this.cs = undefined;
        this.ps = undefined;

        this.next = function () {

            if (this.ps) {
                this.ps.remove();
            }

            // hide current slide
            if (this.cs) {
                this.cs.addClass('hide');
            }
            this.ps = this.cs;

            this.cs = $(this.slides[this.current]).clone();
            this.$c.append(this.cs);
            this.current = (this.current + 1 >= this.length ? 0 : this.current+1);
            setTimeout($.proxy(function () {
                this.cs.removeClass('hide');
            }, this));
        };
        this.next();

        this.interval = setInterval($.proxy(this.next, this), this.duration);
    }

    $(function () {
        $('[data-slide-rotator]').each(function () {
            var rotator = new Rotator($(this));
        })
    })

}(window.jQuery))