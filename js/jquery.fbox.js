/*
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */
(function($) 
{
	var bindFbox = function(el)
	{
		//factory returns fbox object
		//according to "this" element's type
		//"this" can be <form> or <a>
		//finaly we bind necessary events to fbox handlers 
		return Fbox.Factory(el).bind();
	};
	var parseContent = function($content)
	{
		$('*[rel~=fbox]',$content).fbox();
		return $content;
	};
	//you can use this function by $(selector).fbox()
	//it is bind fbox onclick handler to element
	$(function() { parseContent($(document.body)); });
	$.fn.fbox = function()
	{
		return this.each(function(i,el) { return bindFbox(el); });
	};
	var wrapContent = function($content)
	{
		$content = $("<div class='fbox' id='fboxWindow'></div").append($content);
		$content.bind('click',function() { return false });
		return $content;
	};
	var Fbox = function(element)
	{
		return {
			currentLocation: null,
			element: element,
			location: function(newLocation)
			{
				this.currentLocation = newLocation;
				this.getContent(newLocation,this.appendContent);
				$.fbox = this;
			},
			appendContent: function(html)
			{
				var $content = $(html);
				$content = parseContent($content);
				$content = wrapContent($content);
				if($('#fboxWindow').length == 0)
					$(document.body).append($content);
			},
			close: function()
			{
				return $('#fboxWindow').remove();
			},
			refresh: function()
			{
				this.location(this.currentLocation);
			},
			append: function($smth)
			{
				$('#fboxWindow').append($smth);
			},
			prepend: function($smth)
			{
				$('#fboxWindow').prepend($smth);
			},
			errors: function(errors)
			{
				$('div.errorCss').remove();
				this.prepend($('<div class="errorCss"></div>').append(errors));
			}
		}
	}
	
	var FboxA = function(element)
	{
		var base = Fbox(element);
		var getHiddenEl = function(id,callback)
		{
			callback($(id).html());
		};
		
		var getUrl = function(url,callback)
		{
			$.get(url,{fbox: true},callback);
		}
		
		base.bind = function()
		{
			base.element.onclick = function()
			{
				base.close();
				base.location(this.href);
				return false;
			}
			return this;
		}
		
		base.getContent = function(href,cl)
		{
			if(href[0] == '#')
				return getHiddenEl(href,cl);
			return getUrl(href,cl);
		}
		
		return base;
	}
	
	var FboxForm = function(element)
	{
		var base = Fbox(element);
		base.getContent = function(location,callback)
		{
			var data = $(base.element).serialize();
			data = decodeURIComponent(data);
			$[base.element.method.toLowerCase()](location,data,callback);
		}
		
		base.bind = function()
		{
			base.element.submit = function()
			{
				base.location(this.action);
				return false;
			}
		}
		return base;
	}
	
	Fbox.Factory = function(element)
	{
		if(element.tagName.toLowerCase() == 'form')
			return FboxForm(element);
		
		if(element.tagName.toLowerCase() == 'a')
			return FboxA(element);
		
		throw new exception('incorrect fbox element');
	}
	
	$(document.body).bind('click',function()
	{
		if($.fbox && $.fbox.close)
			$.fbox.close();
	});
	
})(jQuery);
