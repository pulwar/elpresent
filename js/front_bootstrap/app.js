window.partner = {
	lat:0,
	lon:0
};
$(document).ready(function () {

	// отображение текста в способах оплаты
	$('#Order_paymentType_0').click(function(){
		$('p.Order_paymentType_1').show();
		$('p.Order_paymentType_2').hide();
	});

	$('#Order_paymentType_1').click(function(){
		$('p.Order_paymentType_1').hide();
		$('p.Order_paymentType_2').show();
	});

	$('#Order_paymentType_2').click(function(){
		$('p.Order_paymentType_1').hide();
		$('p.Order_paymentType_2').show();
	});

	/**
	 * Инициализация датапикера
	*/
    $('.datepicker').datepicker({
        //format: 'dd/mm/yyyy'
		format: 'yyyy-mm-dd'
    });


	/**
	 * Открытие и закррытие расширенной формы поиска
	 */
    $('.advanced_search').on('click', 'a', function () {
        var content = $('.search-body');
        if (content.is(':hidden')){
            $(content).show();
        } else {
            $(content).hide();
        }
    });

	/**
	 * Отправка поиска
	 */
    $('form.search-form input').bind('change', function () {
        $('form .search-results').html("<img src='/img/loading.gif' style='position: relative; left:45%;'>");
        $.ajax({
            url: '/catalog/ajaxsearch',
            dataType: 'json',
            type: 'POST',
            data: $('form.search-form').serialize(),
            success: function (data) {
                $('form .search-results').html(data.content);
            }
        });
    });

	/**
	 * Быстрый заказ
	 */
	$('.fast-order form').on('click', 'button', function(){
		var form = $(this).parent();

        $(form).find('button').prop('disabled', true);

		var valid = true;
		$('input[type=text], input[type=email]', form).each(function(i, e){
			if($(this).val() == '') {
				$(this).addClass('error');
				valid = false;
			} else {
                $(this).removeClass('error');
			}
		});

        $('input[type=email]', form).each(function(i, e){
            if(!(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test($(this).val()))) {
                $(this).addClass('error');
                valid = false;
			} else {
                $(this).removeClass('error');
            }
        });

		if(!valid) {
		    $(form).find('button').prop('disabled', false);
			return false;
		}

		var requestData = form.serialize();
		if($('.item-variant .price_choose')) {
			var checked = $('.item-variant .price_choose input[type=radio]:checked');
			var val = $(checked).val();
			if(val) {
				requestData = requestData + '&optionId='+val;
			}
		}

		$.ajax({
			type: 'POST',
			url: '/order/makeRequest',
			data: requestData,
			success:function(result){
				if(result.ok) {
					form.hide();
					$('[data-show-on-success]').show();
				}
				reachGoal('quick_order');
			},
			dataType: 'json'
		});

		return false;
	});

	/**
	 * Хочу в подарок
	 */
	$('[data-submit=want-for-present]').on('submit', function (e) {
		e.preventDefault();
		e.stopPropagation();
		$('[type=submit]', this).attr('disabled', 'disabled');
		var errMsg = $(this).closest('.modal-window').find('.error-msg'),
			successMsg = $(this).closest('.modal-window').find('.success-msg'),
			$this = $(this);

		$([errMsg, successMsg]).addClass('hide');

		$.ajax({
			url: $this.attr('action'),
			data: $this.serialize(),
			type: 'POST',
			success: function () {
				setTimeout(function () {
					$this.addClass('hide');
					successMsg.removeClass('hide');
				}, 250);
			},
			error: function () {
				setTimeout(function () {
					errMsg.removeClass('hide');
				}, 250);
			}
		});

		return false;
	});

    // если нажата кнопка купить в попапе опций
    $('body').on('click', '.buy-quickly', function(e){
		var modelWindow = $(this).parents('div.modal-content');
		var checkedOption = modelWindow.find('input:radio:checked');
		var id = $(this).data('id');
		var option = $(checkedOption).val() == null ? '' : $(checkedOption).val();
		var params = {itemId: id, optionId: option};
		var queryParam = $.param(params);
		var href = $(this).data('href');

		window.document.location = href + '?' + queryParam;
    });

	/**
	 * Add and remove item from cart
	 */
	$(document)
		.on('click', '.add-to-kit-cart', function(e){
		$('.modal.fade.option').modal('hide');
		var checkedPrice = $(this).parents('.modal-content').find('input:radio:checked');
		var id = $(this).data('id');
		var option = $(checkedPrice).val() == null ? '' : $(checkedPrice).val();

			var modalSelector = $(this).attr('data-item-modal');
			var price = $(checkedPrice).attr('data-price');
		$.ajax({
			type: 'POST',
			data: {'addItem': {'id':id, 'option':option}},
			cache: false,
			url: '/cart/add',
			success: function (data) {
				var response = jQuery.parseJSON(data);
				//$('#cart').html(response.cart);
				updateCartIcon(response.countItems, response.countPrice);
				if (price) {
					$(modalSelector).find('.product-item-price').each(function () {
						$(this).html(price);
					});
				}
				$(modalSelector).modal();

			},
			error: function(data) {

			}
		});
	}).on('click', '.add-to-kit-cart.item-page', function(e){
		var id = $(this).data('id');
		var checkedPrice = $('.item-variant').find('input:radio:checked');

		var option = $(checkedPrice).val() == null ? '' : $(checkedPrice).val();

		var modalSelector = $(this).attr('data-item-modal');
		var price = $(checkedPrice).attr('data-price');

		$.ajax({
			type: 'POST',
			data: {'addItem': {'id':id, 'option':option}},
			cache: false,
			url: '/cart/add',
			success: function (data) {
				var response = jQuery.parseJSON(data);
				updateCartIcon(response.countItems, response.countPrice);

				if (price) {
					$(modalSelector).find('.product-item-price').each(function () {
						$(this).html(price);
					});
				}
				$(modalSelector).modal();
			},
			error: function(data) {

			}
		});
	}).on('click', '#cart .cart-delete-item', function(e){
		$.ajax({
			type: 'POST',
			data: {'removeItemId': $(this).data('item_from_del_id')},
			cache: false,
			url: '/cart/del',
			success: function (data) {
				var response = jQuery.parseJSON(data);
				$('#cart').html(response.cart);
				updateCartIcon(response.countItems, response.sumPrice);
			},
			error: function(data) {

			}
		});

	});

	/**
	 * Update header cart icon
	 * @param countItems
	 * @param sumPrice
     */
	function updateCartIcon(countItems, sumPrice) {
		$('#cartItemsCount').html(countItems);
		$('#cartPriceSum').html(sumPrice);
	}

	/**
	 * Добавление рейтинга для отзывов
	 */
    $('input.rating').on('rating.change', function(event, value, caption) {
        $('#comment-rating').val(value);
    });

	/**
	 * Добавление отзывов для подарков
	 */
	$('.input-7-xs').on('rating.change', function(event, value, caption) {
		var item_id = $('.rating-loading-id-item').val();

		var item_id_coockie = $.cookie('rating');

		if(item_id_coockie = item_id){
			var myData = {
				item_id: item_id,
				val: value
			};

			$.ajax({
				type: 'POST',
				data: myData,
				cache: false,
				url: '/catalog/rating',
				success: function (data) {
					var response = jQuery.parseJSON (data);
					var val = response.value;
					var result = response.result;
					$('.input-7-xs').rating('update', val).val();

					if(result){
						alert('Ваш голос принят!');
						$.cookie('rating', item_id, { path: '/' });
					}else{
						alert('Вы уже голосовали! Пытаясь проголосовать вновь, вы совершаете нравственное преступление!');
					}


				}
			});
		}else{
			alert('Вы уже голосовали! Пытаясь проголосовать вновь, вы совершаете нравственное преступление!');
			$('.input-7-xs').rating('reset').val()
		}
	})
});

/**
 * Карта для впечатлений
 */
function loadmap() {
	var map = new google.maps.Map(document.getElementById('activation-map'), {
		zoom:14,
		center:new google.maps.LatLng(window.partner.lat, window.partner.lon),
		mapTypeId:google.maps.MapTypeId.ROADMAP
	});
	var marker = new google.maps.Marker({
		position:new google.maps.LatLng(window.partner.lat, window.partner.lon),
		visible:true,
		map:map
	});
}
/**
 * Отображение результатов впечатления
 * @param button
 */
function submitPopup(button) {
	$('#error_activation,#ok_activation').hide();
	$('#error_activation').empty();
	var data = $('#activation_popup_form').serialize();

	$.getJSON('/order/activation?' + data, {}, function (data) {
		if (data.error) {
			$('#error_activation').html(data.error).show();
		}
		else {
			$(button).hide();
			$('#activation-partner-place').html(data.adress);
			$('#activation-partner-address').html(data.address);
			$('#activation-partner-contact').html(data.contact_person + ', ' + data.phone);
			$('#ok_activation').show();
			$('#activation-info').show();
			$('.activation-instruction').show();
			window.partner.lat = Number(data.lat);
			window.partner.lon = Number(data.lon);
			var script = window.document.createElement("script");
			script.type = 'text/javascript';
			script.src = 'http://maps.google.com/maps/api/js?sensor=false&language=ru&&callback=loadmap';
			window.document.body.appendChild(script);
		}
	});
}

function reachGoal(goal) {
    var map = {
        quick_order: ["quick-order", "quick_order"],
        aktivation: ["aktivization", "aktivation"],
        otzivi: ["otzivi", "otzivi"],
        vopros: ["vopros", "vopros"],
        zakaz: ["zakaz", null]
    };
    if (!map.hasOwnProperty(goal)) {
        return
    }
    var yaCounter = yaCounter17198173 || {
            reachGoal: function () {
            }
        };
    var _gaq = _gaq || [];
    var goalValue = map[goal];
    if (goalValue[1]) {
        yaCounter.reachGoal(goalValue[1])
    }
    if (goalValue[0]) {
        _gaq.push(["_trackPageview", "/" + goalValue[0]])
    }
}

/**
 *
 */
function showActionPopup() {
    //var popupHeight = $("body").height();
    //$(".activation-popup").find("#error_activation,#ok_activation").hide();
    //$(".activation-popup").css({height: popupHeight + 20}).show()
	$('#myModalFour').modal('hide')
	$('#myModalEight').modal('show');
}

/**
 * Активация шаг 2
 * @param title
 * @param href
 * @param code
 * @param items
 */
function setActivationPopupData(title, href, code, items) {
    if (items == undefined) {
        $(".superimpressions").hide()
    } else {
        $("#activation_popup_activated_item").html("");
        $.each(items, function (id, title) {
            $("#activation_popup_activated_item").append("<option value=" + id + ">" + title + "</option>")
        })
    }
    $("#item_title_activation_popup").text(title).attr("href", href);
    $("#item_code_activation_popup").text(code)
}

/**
 * Активация подарка
 */
function activateItem() {
    //reachGoal("aktivation");
    $("#answer_true,#answer_false").hide().empty();
    var form = [];
    $("#itemform input").each(function () {
        form.push($(this).attr("name") + "=" + encodeURIComponent($(this).val()))
    });
    form = form.join("&");
    $.getJSON("/order/code" + "?" + form, {}, function (data) {
        if (data.error) {
            $("#answer_false").show().html(data.error);
        } else {
            setActivationPopupData(data.title, data.href, data.code, data.items);
            $("#activation_popup_code").val(data.rawCode);
            showActionPopup();
            //$(".activation-button").click()
        }
    })
}

/* Numbers */

$('.number').groupinputs();

function numbersonly(myfield, e, dec) {
	var key;
	var keychar;

	if (window.event)
		key = window.event.keyCode;
	else if (e)
		key = e.which;
	else
		return true;
	keychar = String.fromCharCode(key);

// control keys
	if ((key == null) || (key == 0) || (key == 8) ||
		(key == 9) || (key == 13) || (key == 27))
		return true;

// numbers
	else if ((("0123456789").indexOf(keychar) > -1))
		return true;

// decimal point jump
	else if (dec && (keychar == ".")) {
		myfield.form.elements[dec].focus();
		return false;
	}
	else
		return false;
}

(function($, undefined){
	$.extend({
		/**
		 * Lockfixed initiated
		 * @param {Element} el - a jquery element, DOM node or selector string
		 * @param {Object} config - offset - forcemargin
		 */
		"lockfixed": function(el, config){
			if (config && config.offset) {
				config.offset.bottom = parseInt(config.offset.bottom,10);
				config.offset.top = parseInt(config.offset.top,10);
			}else{
				config.offset = {bottom: 100, top: 0};
			}
			var el = $(el);
			if(el && el.offset()){
				var el_position = el.css("position"),
					el_margin_top = parseInt(el.css("marginTop"),10),
					el_position_top = el.css("top"),
					el_top = el.offset().top,
					pos_not_fixed = false;

				/*
				 * We prefer feature testing, too much hassle for the upside
				 * while prettier to use position: fixed (less jitter when scrolling)
				 * iOS 5+ + Android has fixed support, but issue with toggeling between fixed and not and zoomed view
				 */
				if (config.forcemargin === true || navigator.userAgent.match(/\bMSIE (4|5|6)\./) || navigator.userAgent.match(/\bOS ([0-9])_/) || navigator.userAgent.match(/\bAndroid ([0-9])\./i)){
					pos_not_fixed = true;
				}

				/*
				 // adds throttle to position calc; modern browsers should handle resize event fine
				 $(window).bind('scroll resize orientationchange load lockfixed:pageupdate',el,function(e){

				 window.setTimeout(function(){
				 $(document).trigger('lockfixed:pageupdate:async');
				 });
				 });
				 */

				$(window).bind('scroll resize orientationchange load lockfixed:pageupdate',el,function(e){
					// if we have a input focus don't change this (for smaller screens)
					if(pos_not_fixed && document.activeElement && document.activeElement.nodeName === "INPUT"){
						return;
					}

					var top = 0,
						el_height = el.outerHeight(),
						el_width = el.outerWidth(),
						max_height = $(document).height() - config.offset.bottom,
						scroll_top = $(window).scrollTop();

					// if element is not currently fixed position, reset measurements ( this handles DOM changes in dynamic pages )
					if (el.css("position") !== "fixed" && !pos_not_fixed) {
						el_top = el.offset().top;
						el_position_top = el.css("top");
					}

					if (scroll_top >= (el_top-(el_margin_top ? el_margin_top : 0)-config.offset.top)){

						if(max_height < (scroll_top + el_height + el_margin_top + config.offset.top)){
							top = (scroll_top + el_height + el_margin_top + config.offset.top) - max_height;
						}else{
							top = 0;
						}

						if (pos_not_fixed){
							el.css({'marginTop': (parseInt(scroll_top - el_top - top,10) + (2 * config.offset.top))+'px'});
						}else{
							el.css({'position': 'fixed','top':(config.offset.top-top)+'px','width':el_width +"px"});
						}
					}else{
						el.css({'position': el_position,'top': el_position_top, 'width':el_width +"px", 'marginTop': (el_margin_top && !pos_not_fixed ? el_margin_top : 0)+"px"});
					}
				});
			}
		}
	});
})(jQuery);

$(document).ready(function() {
	$.lockfixed("#myTabContent",{offset: {top: 10, bottom: 100}});

	//делаем кликабельной картинку со скидками
	$('.gift.list .discount-img').click(function(){
		var href = $(this).siblings('a').attr('href');
		window.location = href;
	});
});