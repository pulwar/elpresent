(function($){
	$(window).load(function(){

		var datavalueDefault = $(".image-click.active").attr("data-value");
		$(".texture-selector").val(datavalueDefault);

		var datavalueDefaultBow = $(".strip-click.active").attr("data-value");
		$(".bow-selector").val(datavalueDefaultBow);
	});

	$(".image-click").on("click", function() {
		var dataimage = $(this).attr("data-image");
		$(".image").hide();
		$(".image[data-image=" + dataimage + "]").show();
		$(".image-click").removeClass("active");
		$(this).addClass("active");

		var datavalue =  $(this).attr("data-value");
		$(".texture-selector").val(datavalue);
	});

	$(".strip-click").on("click", function() {
		var dataimage = $(this).attr("data-image");
		$(".strip").hide();
		$(".strip[data-image=" + dataimage + "]").show();
		$(".strip-click").removeClass("active");
		$(this).addClass("active");

		var datavalue =  $(this).attr("data-value");
		$(".bow-selector").val(datavalue);
	});

	$(".card-click").on("click", function() {
		var dataimage = $(this).attr("data-image");
		$(".cardd").hide();
		$(".cardd[data-image=" + dataimage + "]").show();
		$(".card-click").removeClass("active");
		$(this).addClass("active");

		var dataprice = $(this).attr("data-price");

		$('#card-price').html(dataprice);
		$('#card-price').next().show();
		summ();

		var datavalue =  $(this).attr("data-value");
		$("#card-value").val(datavalue);
	});

	$(".bouquet-click").on("click", function() {
		var dataimage = $(this).attr("data-image");
		$(".bouquet").hide();
		$(".bouquet[data-image=" + dataimage + "]").show();
		$(".bouquet-click").removeClass("active");
		$(this).addClass("active");

		var dataprice = $(this).attr("data-price");
		$('#bouquet-price').html(dataprice);
		$('#bouquet-price').next().show();
		summ();

		var databouquet =  $(this).attr("data-value");
		$("#flower-value").val(databouquet);

	});

	$(".toy-click").on("click", function() {
		var dataimage = $(this).attr("data-image");
		$(".toy").hide();
		$(".toy[data-image=" + dataimage + "]").show();
		$(".toy-click").removeClass("active");
		$(this).addClass("active");

		var dataprice = $(this).attr("data-price");
		$('#toy-price').html(dataprice);
		$('#toy-price').next().show();
		summ();

		var datatoy =  $(this).attr("data-value");
		$("#toy-value").val(datatoy );

	});


	$("#hide-bouquet").click(function(){
		$(".show-bouquet").hide();

		$('#bouquet-price').next().hide();
		$('#bouquet-price').empty();
		$(".add_bouquet").show();
		$("#bouquet-value").val(0);
		summ();
	});

	$("#show-bouquet").click(function(){
		$(".show-bouquet").show();
		$(".add_bouquet").hide();
	});

	$("#hide-card").click(function(){
		$(".show-card").hide();

		$('#card-price').next().hide();
		$('#card-price').empty();
		$(".add_card").show();
		$("#card-value").val(0);
		summ();
	});

	$("#show-card").click(function(){
		$(".show-card").show();
		$(".add_card").hide();
	});

	$("#hide-toy").click(function(){
		$(".show-toy").hide();

		$('#toy-price').next().hide();
		$('#toy-price').empty();
		$(".add_toy").show();
		$("#toy-value").val(0);
		summ();
	});

	$("#show-toy").click(function(){
		$(".show-toy").show();
		$(".add_toy").hide();
	});

	var giftprice = $("#giftprice").attr("data-price");
	$('#giftprice').html(giftprice + ' руб.');


	var summ = function(){
		var total = parseInt($('#giftprice').attr("data-price")),
			bouqet = !!$('#bouquet-price').text() ? parseInt($('#bouquet-price').text()) : 0,
			card = !!$('#card-price').text() ? parseInt($('#card-price').text()) : 0,
			toy = !!$('#toy-price').text() ? parseInt($('#toy-price').text()) : 0;
		$('#total-price').html(getNamberWithSpace(total + bouqet + card + toy));

	};
})( jQuery );