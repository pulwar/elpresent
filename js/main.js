$( document ).ready(function() {

     $(function() {
          $('.lazy').lazy({
              beforeLoad: function(element){
    			  console.log('image is about to be loaded');
              },
    		  afterLoad: function(element) {

    			  console.log('image was loaded successfully');
    		  },
    		  onError: function(element) {

    			  console.log('image could not be loaded');
    		  },
    		  onFinishedAll: function() {
    		      console.log('finished loading elements');
    		      console.log('lazy instance is about to be destroyed')
    		  }
          });
      });
    // console.log('222222222');

    // $('#phone_mask').phonemask();

    // $("body").swipe( {
    //     swipeLeft:function(event, direction, distance, duration, fingerCount) {
    //         $('.h_menu_sm_wrap').slideUp();
    //         console.log('свернуть');
    //         $('.h_menu_sm_btn').removeClass('active');
    //     }
    // });

    // $("body").swipe( {
    //     swipeRight:function(event, direction, distance, duration, fingerCount) {
    //         $('.h_menu_sm_wrap').slideDown();
    //         $('.h_menu_sm_btn').addClass('active');
    //         console.log('развернуть');
    //     }
    // });


    $('.sort_select_single').on('click', function () {
        $(this).parent('.sort_select_wrap').find('.sort_select_inner').toggleClass('dropOpen');
    });

    $("body").click(function(e) {
        if($(e.target).closest(".sort_select_wrap").length==0){
            $('.sort_select_inner').removeClass('dropOpen');
        }
    });

    $('.h_menu_sm_btn').on('click', function () {
        $(this).toggleClass('active');
        $('.h_menu_sm_wrap').slideToggle();
    });

    $('.h_menu_sub_item').on('click', function () {
        $(this).toggleClass('active');
        $(this).next('.sub_menu').slideToggle();
    });

    $("body").click(function(e) {
        if($(e.target).closest(".h_menu_sm").length==0){
            $(".h_menu_sm_wrap").slideUp();
            $('.h_menu_sm_btn').removeClass('active');
        }
    });

    $('.mob_btn_filter').on('click', function () {
        $(this).toggleClass('filter_open');
        $('.h_filter').slideToggle();
    });

    $('.show_text').on('click', function () {
        $(this).hide();
        $(this).parents('.slider_comment_text').find('.slider_comment_text_fixed').css({'height' : 'auto'});
    });

    $('.slider_comment').on('beforeChange', function(event, slick, currentSlide, nextSlide){
        $('.show_text').show();
        $('.slider_comment_text_fixed').css({'height' : '84px'});
    });

    $('.slider_comment').slick({
        slidesToShow: 1,
        dots: false,
        adaptiveHeight: false,
        slidesToScroll: 1,
        autoplay: false,
        autoplaySpeed: 5000,
        arrows: true,
        infinite: true,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    adaptiveHeight: true,
                    autoplay: false
                }
            }
        ]
    });

    $(window).scroll(function () {
        if ($(this).scrollTop() > 0) {
            $('.btn_top').fadeIn();
        } else {
            $('.btn_top').fadeOut();
        }
    });

    $('.btn_top').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 400);
        return false;
    });

    $(".chosen-select").chosen({
        disable_search: true
    });

    $('.team_inner').slick({
        slidesToShow: 5,
        dots: false,
        adaptiveHeight: true,
        slidesToScroll: 1,
        autoplay: false,
        // autoplaySpeed: 5000,
        arrows: true,
        infinite: true,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    dots: false,
                    adaptiveHeight: true,
                    slidesToScroll: 1,
                    autoplay: false,
                    arrows: true,
                    infinite: true
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                    dots: false,
                    adaptiveHeight: true,
                    slidesToScroll: 1,
                    autoplay: false,
                    // autoplaySpeed: 5000,
                    arrows: true,
                    infinite: true
                    // settings: "unslick"
                }
            }
        ]
    });

    $('.main_slider').slick({
        slidesToShow: 1,
        dots: true,
        adaptiveHeight: false,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 5000,
        arrows: false,
        infinite: true,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    adaptiveHeight: false
                }
            }
        ]
    });

    $('.main_slider_sm').slick({
        slidesToShow: 1,
        dots: true,
        adaptiveHeight: false,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 10000,
        arrows: false,
        fade: true,
        infinite: true
    });

    $('.menu_item_btn').on('click', function () {
        $(this).toggleClass('active');
        $('.h_menu_inner').slideToggle();
    });

    $(document).on('click', function (e) {
        if ($(".menu_item_btn").has(event.target).length === 0){
            $(".h_menu_inner").slideUp();
            $('.menu_item_btn').removeClass('active');
        }
    });

    $('.menu_item_btn').on('click', function () {
        if ($(".menu_mob_pop").hasClass('active')){
            $('.menu_mob_pop').removeClass('active');
            $('.menu_pop_wrap').slideUp();
        }
    });

    $("body").click(function(e) {
        if($(e.target).closest(".menu_item_btn").length==0){
            $(".h_menu_inner").slideUp();
            $('.menu_item_btn').removeClass('active');
        }
    });


    $('.menu_mob_pop').on('click', function () {
        $(this).toggleClass('active');
        $('.menu_pop_wrap').slideToggle();
    });

    // phone
    $('.h_phone').on('click', function () {
        $(this).toggleClass('active');
        $('.h_phone_all').stop(true, true).slideToggle();
    });

    $("body").click(function(e) {
        if($(e.target).closest(".h_phone").length==0){
            $(".h_phone_all").slideUp();
            $('.h_phone').removeClass('active');
        }
    });

    // login
    $('.login_cell').on('click', function () {
        $(this).toggleClass('active');
        $('.login_all').stop(true, true).slideToggle();
    });

    $("body").click(function(e) {
        if($(e.target).closest(".login_cell").length==0){
            $(".login_all").slideUp();
            $('.login_cell').removeClass('active');
        }
    });



    // resize

    // function windowSize() {
    //     var windowWidth = $(window).width();
    //     if (windowWidth < '901') {
            // $('.menu_pop_wrap').hide();
            //
            // $(document).on('click', function (e) {
            //     if ($(".menu_mob_pop").has(event.target).length === 0){
            //         $(".menu_pop_wrap").slideUp();
            //         $('.menu_mob_pop').removeClass('active');
            //     }
            // });
        // }
        // else {
            // $('.menu_pop_wrap').css({'display' : 'inline-block'});
    //     }
    // }
    // $(window).on('load resize', windowSize);
    var createViewUrl = function(id) {
        return '/catalog/list/page' + id;
    };

    // ajax подгрузка
    $("body").on('click', 'div.btn_add', function(e) {

        console.log('load more');

        var count_elements = $('.main_list div.col_4').length
        var total = $('.search_note span').text();
        if(count_elements == total) {
            //$(this).hide()
        } else {
            var id =  Math.floor(count_elements/12) + 1;
            var container = $('.main_list div.row_custom');
            var priceMin = $('input[name="prices[0]"]').val();
            var priceMax = $('input[name="prices[1]"]').val();
            var options = $('select[name="options[0]"]').val();
            var features = $('select[name="features[0]"]').val();
            var categoryId = $('input[name="category_id"]').val();
            var q = $('input[name="q"]').val();
            var is_new = $('input[name="is_new"]').val();
            var off_price = $('input[name="off_price"]').val();

            $.ajax({
                'url': createViewUrl(id),
                'dataType': 'html',
                'method': "POST",
                data: {
                    'sortBy' : $('div.sort_select_single').attr('data-sort'),
                    'priceMin' : priceMin,
                    'priceMax' : priceMax,
                    'options' : options,
                    'features' : features,
                    'q' : q,
                    'category_id' : categoryId,
                    'is_new' : is_new,
                    'off_price' : off_price
                 },
                'beforeSend': function (data) {
                    $('.btn_add').hide();
                },
                'success': function(result){
                     if(count_elements+12 < total){
                         $('.btn_add').show();
                     }
                    $(container).append(result);
                    $('.lazy').lazy();
                },
                error: function(){
                    alert('Что то пошло не так!');
                }
            });
        }


    });

    $('.sort_select_item').on('click', function () {
            $('.sort_select_item').removeClass('selectActive');
            $(this).addClass('selectActive');
            $('.sort_select_inner').removeClass('dropOpen');
            var sortText = $(this).text()
            $( ".sort_select_single span" ).text(sortText);

            var sortBy = $(this).attr('data-sort');
            var priceMin = $('input[name="prices[0]"]').val();
            var priceMax = $('input[name="prices[1]"]').val();
            var options = $('select[name="options[0]"]').val();
            var features = $('select[name="features[0]"]').val();
            var q = $('input[name="q"]').val();
            var categoryId = $('input[name="category_id"]').val();
            var is_new = $('input[name="is_new"]').val();
            var off_price = $('input[name="off_price"]').val();
            console.log(sortBy);

            $('div.sort_select_single').attr('data-sort', sortBy);

            var container = $('.main_list div.row_custom');

            // подгрузка
            $.ajax({
                'url': createViewUrl(1),
                'dataType': 'html',
                'method': "POST",
                data: {
                    'sortBy' : sortBy,
                    'priceMin' : priceMin,
                    'priceMax' : priceMax,
                    'options' : options,
                    'features' : features,
                    'q' : q,
                    'category_id' : categoryId,
                    'is_new' : is_new,
                    'off_price' : off_price
                 },
                'beforeSend': function (data) {
                    // какой-нибудь прелоадер
                    //$('.btn_add').hide();
                },
                'success': function(result){
                      $('.btn_add').show();
                      $(container).html(result);
                      $('.lazy').lazy();
                },
                error: function(){
                    alert('Что то пошло не так!');
                }
            });
    });

    $('form#formHelp').on('submit', function(){
        console.log('form submit');
        $.ajax({
            'url': '/site/landing',
            'dataType': 'json',
            'method': "POST",
            data: $(this).serialize(),
            'beforeSend': function (data) {
                // какой-нибудь прелоадер
                //$('.btn_add').hide();
            },
            'success': function(result){
                if(result.status==true){
                    $('form#formHelp').find("input, textarea").val("");
                    $('form#formHelp div.response_text').html('<p>Сообщение отправлено</p>')
                } else {
                    if(result.code==1) {
                        $('form#formHelp div.response_text').html('<p>Ошибка валидации</p>')
                    } else if(result.code==2) {
                        $('form#formHelp div.response_text').html('<p>Пожалуйста, подтвердите, что вы не робот</p>')
                    }
                }
                  //$('.btn_add').show();
                  //$(container).html(result);
            },
            error: function(){
                alert('Что то пошло не так!');
            }
        });
        return false;

    });

});