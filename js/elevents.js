(function ($) {

    function scrollTo(el, duration) {

        setTimeout(function () {
            $('html, body').animate({
                scrollTop: el.offset().top
            }, duration);
        }, 0);
    }

    $(function () {
        var $mainSection = $('[data-main-section]'),
            $eventList = $('.events-list').delegate('click', '[data-sub-section]', function (e) {
                $(this).closest('[data-section]').find('[data-sub-section]').not(this).removeClass('active');
                var id = $(this).toggleClass('active').data('sub-section'),
                    parentId = $(this).closest('[data-section]').data('section');
                $('[data-section-container='+parentId+']').find('[data-sub-section-container]').hide();
                handleMainSection();
                scrollTo($('[data-sub-section-container='+id+']').slideToggle(250), 250);
                e.stopPropagation();
                return false;
            }).delegate('click', '[data-section]', function () {
                $('.active[data-section]').not(this).trigger('click');
                var id = $(this).toggleClass('active').find('.active').removeClass('active').end().data('section'),
                    item = $('[data-section-container='+id+']');
                handleMainSection();
                scrollTo(item.slideToggle(250), 250);
                item.find('[data-sub-section-container]').hide();

            }).fixTo('.section-container');

        function handleMainSection () {
            setTimeout(function () {
                if ($eventList.find('.active').length) {
                    $mainSection.hide();
                } else {
                    $mainSection.show();
                }
            }, 0);
        }
    });

}(window.jQuery));