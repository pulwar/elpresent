if ($ && typeof $.fn.on !== "function") {
    $.fn.on = $.fn.bind;
}
if ($ && typeof $.fn.off !== "function") {
    $.fn.off = $.fn.unbind;
}
if ($ && typeof $.fn.prop !== "function") {
    $.fn.prop = function () {};
}