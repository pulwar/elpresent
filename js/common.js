window.partner = {
    lat:0,
    lon:0
};
function loadmap() {
    var map = new google.maps.Map(document.getElementById('activation-map'), {
        zoom:14,
        center:new google.maps.LatLng(window.partner.lat, window.partner.lon),
        mapTypeId:google.maps.MapTypeId.ROADMAP
    });
    var marker = new google.maps.Marker({
        position:new google.maps.LatLng(window.partner.lat, window.partner.lon),
        visible:true,
        map:map
    });
}
function submitPopup(button) {
    $('#error_activation,#ok_activation').hide();
    $('#error_activation').empty();
    var data = $('#activation_popup_form').serialize();
    $.getJSON('/order/activation?' + data, {}, function (data) {
        if (data.error) {
            $('#error_activation').html(data.error).show();
        }
        else {
            $(button).hide();
            $('#activation-partner-place').html(data.adress);
            $('#activation-partner-address').html(data.address);
            $('#activation-partner-contact').html(data.contact_person + ', ' + data.phone);
            $('#ok_activation').show();
            $('#activation-info').show();
            $('.activation-instruction').show();
            window.partner.lat = Number(data.lat);
            window.partner.lon = Number(data.lon);
            var script = window.document.createElement("script");
            script.type = 'text/javascript';
            script.src = 'http://maps.google.com/maps/api/js?sensor=false&language=ru&&callback=loadmap';
            window.document.body.appendChild(script);

        }
    });
}

function reachGoal(goal) {
    var map = {
        quick_order: ['quick-order', 'quick_order'],
        aktivation: ['aktivization', 'aktivation'],
        otzivi: ['otzivi', 'otzivi'],
        vopros: ['vopros', 'vopros'],
        zakaz: ['zakaz', null]
    };

    if (!map.hasOwnProperty(goal)) {
        return;
    }

    var yaCounter = yaCounter17198173 || {reachGoal: function () {}};
    var _gaq = _gaq || [];
    var goalValue = map[goal];
    // send
    if (goalValue[1]) {
        yaCounter.reachGoal(goalValue[1]);
    }
    if (goalValue[0]) {
        _gaq.push(['_trackPageview', '/' + goalValue[0]]);
    }
}

function showActionPopup()
{
    var popupHeight = $('body').height();
    $('.activation-popup').find('#error_activation,#ok_activation').hide();
    $('.activation-popup').css({height: popupHeight + 20}).show();
}
function setActivationPopupData(title,href,code,items)
{
    if(items == undefined){
        $('.window .superimpressions').hide();
    }
    else {
        $('#activation_popup_activated_item').html('');
        $.each(items, function(id, title) {
            $('#activation_popup_activated_item').append('<option value='+id+'>'+title+'</option>');
        });
    }
    $('#item_title_activation_popup').text(title).attr('href',href);
    $('#item_code_activation_popup').text(code);
}

function activateItem() {
    //reachGoal('aktivation');
    $('#answer_true,#answer_false').hide().empty();
    var form = [];
    $("#itemform input").each(function () {
        form.push($(this).attr('name') + '=' + encodeURIComponent($(this).val()));
    });
    form = form.join("&");
    $.getJSON('/order/code' + '?' + form, {}, function (data) {
        if (data.error)
            $('#answer_false').show().html(data.error);
        else {
            setActivationPopupData(data.title, data.href, data.code, data.items);
            $('#activation_popup_code').val(data.rawCode);
            showActionPopup();
            $('.activation-button').click();
        }
    });
}

$('[data-pincode-input] > input[type=text]').bind('keydown', function (e) {
    var keys = [8, 9, 19, 20,
        27, 33, 34, 35, 36, 37, 38, 39, 40, 45, 46, 144, 145,
        96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106
    ];

    console.log(e.which)

    if (e.which == 8 && this.value.length == 0) {
        $(this).prev('input[type=text]').focus();
    } else if (keys.indexOf(e.which) >= 0) {
        return true;
    } else if (this.value.length >= this.maxLength) {
        $(this).next('input[type=text]').focus();
        return false;
    } else if (e.shiftKey || e.which < 48 || e.which >= 58) {
        return false;
    }
}).bind('keyup', function (e) {
    if (this.value.length >= this.maxLength) {
        $(this).next('input[type=text]').focus();

        return false;
    }
});

$(document).ready(function () {
    alert('!');



    // fixme: sorry,...
    $('.change-city-list > .select-city').bind('click', function () {
        $('.change-city-list > ul').toggle();
    });

//    $(window).on('load', function () {
//        $('.gallery-view-list').masonry({
//            itemSelector: '.gallery-preview'
//        });
//    });


    $('body').bind('click', function (e) {
        if (!$(e.target).closest('.change-city-list').length) // ... nor a descendant of the container
        {
            $('.change-city-list > ul').hide();
        }
    });

    $('select').not('#activation_popup_activated_item').not('.no-fancy-select').selectbox();
    if ($.datepicker) {
        $('#datepicker').datepicker({dateFormat: 'yy-mm-dd', minDate: new Date()});
    }

    // superimpressions
    $('[placeholder]').placeholder();

    // super impressions
    $('.superimpressions .item .figure').bind('mouseover mouseout', function(e){
        var leave = e.type == "mouseout";
        $('img:first', this).stop().animate({"opacity": leave ? 1:0.2}, 250);
        $('.figcaption', this).stop().animate({"opacity": leave ? 0:1}, 250);
        return false;
    });

    $('.superimpressions .item .figure').bind('click', function(){
        location = $('.ribbon', $(this).closest('.item')).attr('href');
    });

    $('.superimpressions .item .figcaption img').bind('mouseover mouseout', function(e){
        var text = e.type == "mouseout" ? "":$(this).attr('alt');
        $(this).closest('.figcaption').find('.child-title').text(text);
    });

    $('.superimpression .items a.header').bind('click', function(){

        var target = this,
            hideModal = function() {
            $('.item-modal').remove();
            $('.modal-overlay').remove();
            $('html,body').animate({"scrollTop": $(target).offset().top - 50}, 500);
        };

        var modalBody = $('<div class="item-modal"><div class="ribbon green"></div><div class="description"></div></div>').bind('click', function(){
                return false;
            }),
            modalOverlay = $('<div class="modal-overlay"></div>').bind('click', hideModal);

        $(document).bind('keyup', function(e){
            $(this).unbind('keyup');
            if (e.keyCode == 27) {
                hideModal();
            }
        });

        // load data
        $.ajax({
            "url": $(this).attr('href'),
            "type": "get",
            "success": function(html) {
                var title=$('h1:first', html).text(),
                    description = $('.text-main .gray-bg', html).html(),
                    script = $('.text-main .gift-info', html);

                $('.ribbon', modalBody).text(title);
                var descriptionContainer = $('.description', modalBody);
                if (description.length) {
                    descriptionContainer.append('<h2>О подарке</h2>').append(description);
                }
                if (script.length) {
                    descriptionContainer.append('<h2>Сценарий</h2>').append(script);
                }
                $('html,body').animate({"scrollTop": $(modalBody).offset().top - 50}, 500);
            }
        });

        $('body').append(modalBody);
        $('body').append(modalOverlay);

        return false;
    });


    $('.activation-button').toggle(
        function () {
            $('.modal').slideDown(300);
        },
        function () {
            $('.modal').hide();
        }
    );

    var exp = $.browser.msie ? 'slide':'drop';


    // todo: WTF? REFACTORING needed!
    var l1width = $('.id-01').height();
    var l2width = 1851;
    var l3width = $('.id-03').height();

    $('.lay-button-01').click(function () {
        $('.id-01').hide(exp, {
            direction:'left',
            easing:'backinout'
        }, 600);
        $('.id-02').show('slide', {
            direction:'right',
            easing:'backinout'
        }, 1000);
        $.scrollTo({
            top:'320',
            left:''
        }, 500, {
            easing:'easein'
        });
        setTimeout(function () {
            $('.js-layout-outer').css({
                height:l2width
            });
        }, 400);
        $('.main-menu').parent().find('.active').removeClass();
        $('.main-menu').parent().find('.item-center:last').find('div').addClass('active');
    });

    $('.lay-button-02').click(function () {
        $('.id-02').hide('slide', {
            direction:'right',
            easing:'backinout'
        }, 600);
        $('.id-01').show('slide', {
            direction:'left',
            easing:'backinout'
        }, 1000);
        $.scrollTo({
            top:'320',
            left:''
        }, 500, {
            easing:'easein'
        });
        setTimeout(function () {
            $('.js-layout-outer').css({
                height:l1width
            });
        }, 400);
        $('.main-menu').parent().find('.active').removeClass();
        $('.main-menu').parent().find('.item-center:first').find('div').addClass('active');
    });

    $('.lay-button-03').click(function () {
        $('.id-02').hide(exp, {
            direction:'left',
            easing:'backinout'
        }, 600);
        $('.id-03').show('slide', {
            direction:'right',
            easing:'backinout'
        }, 1000);
        $.scrollTo({
            top:'320',
            left:''
        }, 500, {
            easing:'easein'
        });
        setTimeout(function () {
            $('.js-layout-outer').css({
                height:l3width
            });
        }, 400);
        $('.main-menu').parent().find('.active').removeClass();
        $('.main-menu').parent().find('.item-right').find('div').addClass('active');
    });

    $('.lay-button-04').click(function () {
        $('.id-03').hide('slide', {
            direction:'right',
            easing:'backinout'
        }, 600);
        $('.id-02').show('slide', {
            direction:'left',
            easing:'backinout'
        }, 1000);
        $.scrollTo({
            top:'320',
            left:''
        }, 500, {
            easing:'easein'
        });
        setTimeout(function () {
            $('.js-layout-outer').css({
                height:l2width
            });
        }, 400);
        $('.main-menu').parent().find('.active').removeClass();
        $('.main-menu').parent().find('.item-center:last').find('div').addClass('active');
    });

    // todo: select boxes fixes? For Colors? What colors?
    // todo: REFACTOR THIS!
    var jsColorsHeight = ($('.js-colors').height() + 0);
    var jsSelectHeight = ($('.js-select').height() + 220);
    var bowOn = 1;
    $('.fix').css({
        height:jsSelectHeight
    });

    function toggleColorsOn() {
        $('.js-select').hide('slide', {
            direction:'left',
            easing:'backinout'
        }, 500);
        if (bowOn == 1) {
            $('.js-bow-select').hide('slide', {
                direction:'left',
                easing:'backinout'
            }, 500);
        }
        $('.js-colors').show('slide', {
            direction:'right',
            easing:'backinout'
        }, 800);
        $('.fix').animate({
            height:jsColorsHeight
        }, 500);
        $('.bow').hide();
        $('.pack-button-small').hide('slide', {
            direction:'left',
            easing:'backinout'
        }, 500);
    }



    function toggleColorsOff() {
        $('.js-colors').hide('slide', {
            direction:'left',
            easing:'backinout'
        }, 500);
        $('.js-select').show('slide', {
            direction:'right',
            easing:'backinout'
        }, 800);
        $('.fix').animate({
            height:jsSelectHeight
        }, 500);
        if (bowOn == 1) {
            setTimeout(function () {
                $('.js-bow-select').show('slide', {
                    direction:'right',
                    easing:'backinout'
                }, 800);
            }, 300);
        }
        $('.pack-button-small').show('slide', {
            direction:'right',
            easing:'backinout'
        }, 800);
    }



    $('.box-select').click(toggleColorsOn);
    $('.texture-button').click(toggleColorsOff);

    $('.bow .box-selector').click(function () {
        var lastBowClassName = $(this).parent().find('.box-selector').attr('class').split(' ').slice(-1);

        $('.bow .js-scroller').slideUp();
        $(this).parent().find('.js-scroller').show('clip', {}, 200);

        $('.box-left .img-bow').fadeOut(200);
        setTimeout(function () {
            $('.box-left .img-bow:first').remove();
        }, 200);
        $('.box-left .container').append('<div class="img-bow ' + lastBowClassName + '"><!-- --></div>');
        $('.box-left .img-bow:last').fadeIn(500);

        $('.bow-select').find('.bg').removeClass().addClass('bg ' + lastBowClassName);
        $('.bow-selector').attr('value', lastBowClassName);
        setTimeout(function () {
            $('.img-bow').ifixpng();
        }, 1000);
    });

    $('.js-colors .box-selector').click(function () {
        var lastClassName = $(this).parent().find('.box-selector').attr('class').split(' ').slice(-1);

        $('.js-colors .js-scroller').slideUp();
        $(this).parent().find('.js-scroller').show('clip', {}, 200);

        $('.box-left .img-outer').fadeOut(200);
        setTimeout(function () {
            $('.box-left .img-outer:first').remove();
        }, 200);
        $('.box-left .container').append('<div class="img-outer ' + lastClassName + '"><!-- --></div>');
        $('.box-left .img-outer:last').fadeIn(500);

        $('.box-select').find('.bg').removeClass().addClass('bg ' + lastClassName);
        $('.texture-selector').attr('value', lastClassName);
        setTimeout(function () {
            $('.img-outer').ifixpng();
        }, 1000);
    });

    $('.add-bow a').click(function () {
        bowOn = 1;
        $('.add-bow').hide('slide', {
            direction:'left',
            easing:'backinout'
        }, 500);
        $('.js-bow-select').show('slide', {
            direction:'right',
            easing:'backinout'
        }, 800);
        $('.bow-selector').attr('value', 'bow-bg-01');
        $('.box-left .img-bow').fadeOut(200);
        setTimeout(function () {
            $('.box-left .img-bow:first').remove();
        }, 200);
        $('.box-left .container').append('<div class="img-bow bow-bg-01"><!-- --></div>');
        $('.box-left .img-bow:last').fadeIn(500);

        $('.bow').find('.js-scroller:first').show();
        $('.bow-select').find('.bg').removeClass().addClass('bg bow-bg-01');
    });

    $('.delete-bow a').click(function () {
        bowOn = 0;
        $('.js-bow-select').hide('slide', {
            direction:'left',
            easing:'backinout'
        }, 500);
        $('.add-bow').show('slide', {
            direction:'right',
            easing:'backinout'
        }, 800);
        $('.bow-selector').attr('value', 'none');
        $('.box-left .img-bow:first').fadeOut(500);
        $('.bow .js-scroller').hide();
    });

    $('.bow-select').click(function () {
        $('.js-select, .js-bow-select, .pack-button-small').hide('slide', {
            direction:'left',
            easing:'backinout'
        }, 500);
        $('.bow').show('slide', {
            direction:'right',
            easing:'backinout'
        }, 800);
        $('.fix').animate({
            height:240
        }, 500);
    });

    $('.bow-button').click(function () {
        $('.bow').hide('slide', {
            direction:'left',
            easing:'backinout'
        }, 500);
        $('.js-select, .js-bow-select, .pack-button-small').show('slide', {
            direction:'right',
            easing:'backinout'
        }, 800);
        $('.fix').animate({
            height:351
        }, 500);
    });

    $('.write-button').click(function () {
        $('._testimonials_popup').show();
        var winLeft = (( $(window).width() / 2) - 300);
        $('.testimonials-popup .window').css({
            left:winLeft
        });
    });


    $('a._contact_comment').click(function () {
        $('._contact_comment_popup').show();
        var winLeft = (( $(window).width() / 2) - 300);
        $('.testimonials-popup .window').css({
            left:winLeft
        });
    });

    $('a._reply_comment').click(function () {
        var parent_id = parseInt($(this).attr('rcomment'));

        if (parent_id) {
            $('#parent_id').val(parent_id);
        }

        $('._reply_comment_popup').show();
        var winLeft = (( $(window).width() / 2) - 300);
        $('.testimonials-popup .window').css({
            left:winLeft
        });
    });

    $('a._delete_comment').click(function () {
        var id = parseInt($(this).attr('dcomment'));

        if (id) {
            $('#comment_id').val(id);
        }

        $('._delete_comment_popup').show();
        var winLeft = (( $(window).width() / 2) - 300);
        $('.testimonials-popup .window').css({
            left:winLeft
        });
    });


    $('.item-button-add-photo').click(function () {
        $('.photos-popup').show();
        var winLeft = (( $(window).width() / 2) - 300);
        $('.photos-popup .window').css({
            left:winLeft
        });
    });

    var startPrice = $('#total-price').html();


    $('#flowers-select').change(function () {
        var flower = document.getElementById('flower-value');
        flower.value = this.value;
    });

    $('#cards-select').change(function () {
        var card = document.getElementById('card-value');
        card.value = this.value;
    });

    function addDiscount() {
        $('#resultPrice').css(
            {
                textDecoration:'line-through'
            });

        $('#discount-container').show();
    }

    function removeDiscount() {
        $('#resultPrice').css(
            {
                textDecoration:'none'
            });

        $('#discount-container').hide();
    }

    $("#focus").focus();

    // attach carousel and tooltips
    $('.submit-order .submit').tipsy({
        "html":true,
        "title" : function () {
            return $(this).parent().find('.help').html();
        },
        gravity:'w',
        "fade":true,
        "offset":5,
        "opacity":1,
        "live":true,
        "className": 'help-box'
    }).bind('click', function(){
        $(this).closest('form').trigger('submit');
    });
    $('[data-tipsy]').tipsy({
        gravity:'w',
        "fade":true,
        "offset":-25,
        "opacity":1,
        "className": 'help-box'
    });

    $('.submit-order form').bind('submit', function(){
        var valid = true;
        $('input[type=text]', this).each(function(){
            if(this.required == true && this.value == '') {
                $(this).addClass('error');
                valid = false;
            }
        });
        // send if valid

        if(!valid) {
            return false;
        }

        $.ajax({
            type: 'POST',
            url: this.action,
            data: $(this).serialize(),
            success:$.proxy(function(result){
                if(result.ok) {
                    $(this).hide();
                    $('[data-show-on-success]', $(this).parent()).show();
                }
                reachGoal('quick_order');
            }, this),
            dataType: 'json'
        });

        return false;
    });

    $('.like li a').tipsy({
        "html":true,
        "title":function () {
            return $(this).parent().find('.figcaption').html();
        },
        gravity:'w',
        "fade":true,
        "offset":5,
        "opacity":1,
        "live":true
    });

    $('.like .carousel').jcarousel({
        vertical:true,
        scroll:1
    });

    $('.search-head').bind('click', function(){
        $('#search-hint').hide();
        $(this).unbind('mouseover').unbind('mouseout');
        var content = $('.search-body');
        if (content.is(':visible')) {
            $(content).hide(400);
        } else {
            $(content).show(400);
        }
    });
    $('#search-form input').bind('change', function(){
        $.ajax({
            url: '/catalog/ajaxsearch',
            dataType: 'json',
            type: 'POST',
            data: $('#search-form').serialize(),
            success: function(data) {
                $('#search-results-header').html(data.header);
                $('#search-results').html(data.content);
            }
        });
    });
    $('#search-hint').show(500);

    $('#search-hint a').bind('click', function(){
        var expires = new Date();
        expires.setTime(expires.getTime() + 360 * 24 * 60 * 60 * 1000);
        document.cookie = 'search-hint='+'1'+  ';path=/' + '; expires=' + expires.toGMTString();
        $('#search-hint').hide();
    })

    window.scrollTo(window.scrollX, window.scrollY);
});

(function ($) {
    $.fn.defaultValue = function (defaultValue) {
        this.val(defaultValue);
        this.focus(function () {
            if ($(this).val() == defaultValue) {
                $(this).val('');
            }
        });
        this.blur(function () {
            if ($(this).val() == '') {
                $(this).val(defaultValue);
            }
        });
    }
}(window.jQuery));