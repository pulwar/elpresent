<?php
if ((!empty($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] == '127.0.0.1') || ($_SERVER['REMOTE_ADDR'] == '::1')) {
    error_reporting(1);
    define('YII_DEBUG', true);
    define('EL_ENV', 'dev');
}else{
    error_reporting(0);
    define('YII_DEBUG', false);
    define('EL_ENV', 'prod');
}

// load bootstrap code
/** @var CWebApplication $app */
$app = require 'protected/bootstrap.php';
$app->run();